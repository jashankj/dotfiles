# -*- mode: perl -*-

=head1 configuration for latexmk

... unfortunately, latexmk isn't quite flexible enough.
much of this is lifted from the example configuration files.

=head2 see also

~texlive/current/texmf-dist/doc/support/latexmk/example_rcfiles/

=cut

use strict;
use warnings;

use constant {
	dvi_none		=> 0,
	dvi_using_pdftex	=> 1,
	dvi_using_luatex	=> 2,
	xdv_none		=> 0,
	xdv_enable		=> 1,
	hnt_none		=> 0,
	hnt_enable		=> 1,
	pdf_none		=> 0,
	pdf_using_pdftex	=> 1,
	pdf_using_ps2pdf	=> 2,
	pdf_using_dvipdf	=> 3,
	pdf_using_luatex	=> 4,
	pdf_using_xetex		=> 5,
};

# set_tex_cmds '-synctex=1 --shell-escape %O %S';
# @default_files = qw{ cvfull.tex cvbrief.tex };
# $pdf_mode = pdf_using_luatex;
# $dvi_mode = 0;

########################################################################
# from `asymptote_latexmkrc' ...

if (defined $ENV{'LATEXMK_ASYMPTOTE'}) {
	add_cus_dep('asy', 'eps', 0, 'asy2eps');
	add_cus_dep('asy', 'pdf', 0, 'asy2pdf');
	add_cus_dep('asy', 'tex', 0, 'asy2tex');
}

sub asy2eps { return asy2x($_[0], 'eps'); }
sub asy2pdf { return asy2x($_[0], 'pdf'); }
sub asy2tex { return asy2x($_[0], 'tex'); }
sub asy2x {
	my ($base, $fmt) = @_;
	my $log_file = "$base.log";
	my $cmd = "asy -vv -noV -f \"$fmt\" -o \"$base.$fmt\" \"$base\" > '$log_file' 2>&1";
	print "asy2x: Running '$cmd'\n";
	my $ret = system($cmd);
	my $FH = undef;
	if (! open($FH, "<", $log_file)) {
		warn "asy2x: Couldn't read log file '$log_file':\n $!";
		return $ret;
	}

	my %imports = ("$base.asy" => 1);
	while (<$FH>) {
		s/\s*$//;
		if (/^(Including|Loading) .* from (.*)$/) {
			my $import = $2;
			# Convert MSWin directory separator to /
			$import =~ s(\\)(/)g;
			$imports{$import} = 1;
		} elsif (
			/^error/ ||
			/^.*\.asy: \d/
		) {
			warn "==Message from asy: $_\n";
			$ret = 1;
		} elsif (
			/^kpsewhich / ||
			/^Processing / ||
			/^Using / ||
			/^Welcome / ||
			/^Wrote /||
			/^cd /||
			/^gs /
		) {
		} else {
			# warn "==Message from asy: $_\n";
		}
	}
	close $FH;
	show_hash('', \%imports);
	rdb_set_source($rule, keys %imports);
	return $ret;
}

########################################################################
# from `glossaries_latexmkrc' ...

if (defined $ENV{'LATEXMK_GLOSSARIES'}) {
	add_cus_dep('acn', 'acr', 0, 'makeglossaries');
	add_cus_dep('glo', 'gls', 0, 'makeglossaries');
	$clean_ext .= ' acr acn alg glo gls glg';
}

sub makeglossaries {
	my @args = (
		'-s', "$_[0].ist",
		'-t', "$$Psource.ilg",
		'-o', $$Pdest, $$Psource
	);
	unshift @args, '-q' if $silent;
	return system 'makeglossaries', @args;
}

########################################################################
# from `graphviz_latexmkrc'

if (defined $ENV{'LATEXMK_GRAPHVIZ'}) {
	push @file_not_found, 'runsystem\(dot -Tpdf -o ([^ ]+) ';
	add_cus_dep('dot', 'pdf', 0, 'dot2pdf');
}

sub dot2pdf {
	my $file = $_[0];
	return system 'dot', '-Tpdf', '-o', "${file}.pdf", "${file}.dot";
}
