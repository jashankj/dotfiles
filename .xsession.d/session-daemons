# -*- shell-script -*-
# PROVIDE: session-daemons
# REQUIRE: ENVIRONMENT
#
# Session services and daemons described via xdg-autostart.

# Suppress a bunch of services that shouldn't be running.
deny_xdg_autostart=(
	blueman.desktop			# started elsewhere
	firewall-applet.desktop		# started elsewhere
	nm-applet.desktop		# started elsewhere
	pulseaudio.desktop		# started elsewhere
	xfsettingsd.desktop		# started elsewhere

	xfce4-notifyd.desktop		# prefer Dunst
	xscreensaver.desktop		# prefer xsecurelock
	xscreensaver-autostart.desktop	# prefer xsecurelock

	# Prefer, specifically, the MATE agent.
	polkit-gnome-authentication-agent-1.desktop
	polkit-kde-authentication-agent-1.desktop

	# We already have Workrave or xwrits.
	rsibreak_autostart.desktop

	# Don't use the GNOME keyring services.
	gnome-keyring-pkcs11.desktop
	gnome-keyring-secrets.desktop
	gnome-keyring-ssh.desktop

	# Suppress GNOME-intrinsic services.
	baloo_file.desktop
	gnome-shell-overrides-migration.desktop

	# Suppress KDE-intrinsic services.
	gmenudbusmenuproxy.desktop
	kaccess.desktop
	org.kde.plasmashell.desktop
	org.kde.discover.notifier.desktop
	pam_kwallet_init.desktop
	xembedsniproxy.desktop
	xapp-sn-watcher.desktop

	# Suppress Cinnamon-intrinsic services.
	cinnamon-notifier.desktop
	cinnamon-settings-daemon-a11y-keyboard.desktop
	cinnamon-settings-daemon-a11y-settings.desktop
	cinnamon-settings-daemon-automount.desktop
	cinnamon-settings-daemon-background.desktop
	cinnamon-settings-daemon-clipboard.desktop
	cinnamon-settings-daemon-color.desktop
	cinnamon-settings-daemon-cursor.desktop
	cinnamon-settings-daemon-housekeeping.desktop
	cinnamon-settings-daemon-keyboard.desktop
	cinnamon-settings-daemon-media-keys.desktop
	cinnamon-settings-daemon-mouse.desktop
	cinnamon-settings-daemon-orientation.desktop
	cinnamon-settings-daemon-power.desktop
	cinnamon-settings-daemon-screensaver-proxy.desktop
	cinnamon-settings-daemon-smartcard.desktop
	cinnamon-settings-daemon-sound.desktop
	cinnamon-settings-daemon-wacom.desktop
	cinnamon-settings-daemon-xrandr.desktop
	cinnamon-settings-daemon-xsettings.desktop
	monitor-cinnamon-screensaver.desktop

	# Please don't just start speaking to me.
	orca-autostart.desktop

	# Suppress on-screen keyboards at startup (?!).
	onboard-autostart.desktop
	caribou-autostart.desktop
)

xdg_autostart_dir=
if [ -e /etc/xdg/autostart ]
then
	xdg_autostart_dir=/etc/xdg/autostart
elif [ -e /usr/local/etc/xdg/autostart ]
then
	xdg_autostart_dir=/usr/local/etc/xdg/autostart
fi

if [ ! -z "${xdg_autostart_dir}" ]
then
	services=($(ls -1 "${xdg_autostart_dir}" | grep '^.*\.desktop$'))
	for x in ${services:|deny_xdg_autostart}
	do
		echo "${x}:"
		eval "exec dex -w -v ${xdg_autostart_dir}/$x" &
	done
fi
