# Backtraces proceed beyond `main' up to `_start'.
set backtrace past-main on
set backtrace past-entry on

# Always create a breakpoint on the ASan error reporting hook.
set breakpoint pending on
break __asan::ReportGenericError
set breakpoint pending auto

# Attach to a Valgrind session.
define vgdb
  target remote | /usr/bin/vgdb
end

# Allow `.gdbinit' files in...
add-auto-load-safe-path ~/cs3231/root
add-auto-load-safe-path /home/cs3231

add-auto-load-safe-path /src/gnu/emacs
add-auto-load-safe-path ~/src/gnu/emacs

# Connect a student's session.
define connect-3231
  dir ~/cs3231/$arg0/kern/compile/$arg1
  target remote unix:.sockets/gdb
  break panic
end
document connect-3231
connect-3231 SOURCE-DIR KERNEL-CONFIG
    connects to a System/161 session running in the specified source
    directory using the specified kernel configuration file.
end

define reconnect-3231
  target remote unix:.sockets/gdb
end

# allow debuginfo
set debuginfod enabled on
