;; -*- mode: common-lisp; slime-default-lisp: cmucl -*-

;;; Load common Common Lisp initialisation code.
(load (merge-pathnames ".common.lisp" (user-homedir-pathname)))
