define(`_hostrule',
`+ ifelse(`$3', `', `ifelse(`$2',`',`jashank',`$2')@', `$3$2@')$1		Exec exec xterm -T "ifelse(`$3', `', `ifelse(`$2',`',`jashank',`$2')@', `$3$2@')$1 : connecting..." -e "ssh -A ifelse(`$2', `', `', `-l $2 ')$1"')

define(`hostgroup',
`AddToMenu RemoteLogins "...$1" Title
AddToMenu RemoteRoots "...$1" Title')

define(`host',
`AddToMenu RemoteLogins
_hostrule($1)
AddToMenu RemoteRoots
_hostrule($1,root)')

DestroyMenu RemoteLogins
AddToMenu RemoteLogins "Remote Logins" Title

DestroyMenu RemoteRoots
AddToMenu RemoteRoots "Remote Roots" Title

hostgroup(rulingia.com)
host(elspeth)
host(innle)
host(elaria)
host(kaylee)
host(beckett)
host(antoinette)
host(bekkielee)
host(cesca)
host(inara)
host(alyzon)
host(ravek)
host(meredith)
host(menolly)
hostgroup(proutility.com.au)
AddToMenu RemoteLogins
_hostrule(zed,,jashankj)
AddToMenu RemoteRoot
_hostrule(zed,root)
hostgroup(pondr.net)
AddToMenu RemoteLogins
_hostrule(pondr)


