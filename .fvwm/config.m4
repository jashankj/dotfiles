############################################################################
############################################################################
##                                                                        ##
##  ######## ##     ##  ##       ##  ###     ###                          ##
##  #######  ##     ##  ##       ##  ####   ####   fvwm2 config 2013.09   ##
##  ##       ##     ##  ##   #   ##  ## ## ## ##  <jashank@rulingia.com>  ##
##  #####    ##     ##  ##  ###  ##  ##  ###  ##                          ##
##  ####      ##   ##   ## ## ## ##  ##   #   ##   built for fvwm 2.6.5   ##
##  ##         ## ##    ####   ####  ##       ##    inara.rulingia.com    ##
##  ##          ###     ###     ###  ##       ##                          ##
##                                                                        ##
############################################################################
############################################################################

ImagePath $HOME/.fvwm/images:+

DefaultFont     "xft:Liberation Sans:pixelsize=16:antialias=true"
Style		"*"   IconFont "xft:Liberation Sans:pixelsize=16:slant=italic:antialias=true"
MenuStyle	"*"   font "xft:Liberation Sans:pixelsize=12:antialias=true"

CursorStyle ROOT    left_ptr
CursorStyle TITLE   left_ptr
CursorStyle DEFAULT left_ptr
CursorStyle SYS     left_ptr
CursorStyle MENU    left_ptr

Style		"*"	HilightFore #3f8fd2, HilightBack #000000
Style		"*"     ForeColor #007929, BackColor #000000

Style		"*"     !Borders

TitleStyle      LeftJustified
TitleStyle	-- flat
ButtonState     InactiveDown    False
ButtonStyle     Reset
ButtonStyle     all     -- UseTitleStyle

MenuStyle       "*"     win
MenuStyle       "*"     Foreground grey80
MenuStyle       "*"     Background grey10
MenuStyle       "*"     Greyed grey60
MenuStyle       "*"     ActiveFore White
MenuStyle       "*"     HilightBack #007929
MenuStyle       "*"     Hilight3DOff
MenuStyle       "*"     BorderWidth 0
CopyMenuStyle   "*"     wops
MenuStyle       "wops"  TitleWarpOff

Emulate fvwm
HideGeometryWindow Never
OpaqueMoveSize 100

Style		"*"	FirmBorder
Style		"*"	ResizeOpaque
Style		"*"     WindowShadeSteps 20
Style		"*"     SnapAttraction 5 SameType ScreenAll

Style		"*"     ClickToFocus
Style		"*"     MouseFocusClickRaises
Style		"*"     MinOverlapPlacement
Style		"*"     GrabFocusOff
Style		"*"     !UsePPosition

DesktopSize     9x1
EdgeResistance  -1
EdgeScroll      0 0
EdgeThickness   0
Style		"*"     EdgeMoveDelay 200
Style		"*"     EdgeMoveResistance 0
DesktopName	0	inara

Style		"*"	MWMFunctions
Style		"*"	MWMDecor
Style		"*"	HintOverride
Style		"*"	OLDecor
BusyCursor DynamicMenu True, Read True
Style		"*"	DecorateTransient
Style		"*"	RaiseTransient

Style		"*"	EWMHUseStrutHints
EwmhBaseStruts  0 0 0 14

DestroyMenu "window_ops_func"
AddToFunc "window_ops_func" "C" PopUp Window-Ops2
+              "M" PopUp Window-Ops2
+              "D" Delete

# XXX menus
Mouse 1		R	A	Menu Utilities mouse -1p -1p
Mouse 2		R	A	Menu Window mouse -1p -1p
Mouse 3		R	A	WindowList mouse -1p -1p
Mouse 3		R	C	WindowList mouse -1p -1p OnlyListSkip

Mouse 1		FST	C	Menu Window-Ops2
Mouse 2		FST	A	Menu Move-Window
Mouse 3		FST	A	Menu Window-Ops2

DestroyMenu FvwmCSAMenu
AddToMenu FvwmCSAMenu
+ "inara"		Title
+ "(&q) Previous page"	GotoPage -1p +0p
+ "(&j) Next page"	GotoPage +1p +0p
+ ""			Nop
+ "(&a) Move"		Move
+ "(&o) Size"		Resize
+ "(&e) Iconify"	Move
+ ""			Nop
+ "(&t) Terminal"	Exec exec gnome-terminal

# Press Ctrl-Shift-A and then "x" or "c"
Key Tab A C Menu FvwmCSAMenu

DestroyFunc my_dbltab2
AddToFunc   my_dbltab2
+ I WindowListFunc
DestroyFunc my_dbltab_prev
AddToFunc   my_dbltab_prev
+ I Prev my_dbltab2

Key KP_Enter	A C	WindowList Root c c CurrentDesk CurrentAtEnd, IconifiedAtEnd, NoGeometry, SelectOnRelease Alt_L my_dbltab2
Key KP_Add	A C	WindowList Root c c CurrentDesk CurrentAtEnd, IconifiedAtEnd, NoGeometry, SelectOnRelease Alt_L my_dbltab_prev

# XXX fix this
DestroyMenu Remotes
AddToMenu Remotes "Remotes..." Title
+ "Remote &Host..." Popup RemoteLogins
+ "Remote &Root..." Popup RemoteRoots

DestroyMenu Applications
AddToMenu Applications "Applications" Title
+ "&Emacs%mini.edit.xpm%" Exec exec ec
+ "&Mail%mini.mail.xpm%" Exec exec xterm -g 80x40 -e 'mutt'
+ "" Nop
+ "&Finder%mini.filemgr.xpm%" Exec open -a Finder
+ "&Chromium%mini.nscape.xpm%" Exec open -a Chromium
+ "i&Tunes%mini.sound.xpm%" Exec open -a iTunes
+ "iC&al%mini.calendar.xpm%" Exec open -a iCal

DestroyMenu Modules
AddToMenu Modules "Modules" Title
+ "&Console%mini.xterm.xpm%"            Module FvwmConsole -sb
+ "B&anner%mini.ray.xpm%"               RestartModule FvwmBanner
+ "BarB&uttons%mini.pencil.xpm%"        RestartModule FvwmButtons BarButtons
+ "" Nop
+ "Unconfigured" Title
+ "&Autoraise%mini.raise.xpm%"          RestartModule FvwmAuto 500
+ "A&nimate%mini.iconify.xpm%"          RestartModule FvwmAnimate
+ "&Backer%mini.rainbow.xpm%"           RestartModule FvwmBacker
+ "&Debug%mini.bug2.xpm%"               RestartModule FvwmDebug
+ "&IconBox%mini.icons.xpm%"            RestartModule FvwmIconBox
+ "I&conMan%mini.run.xpm%"              RestartModule FvwmIconMan
+ "Identif&y%mini.question.xpm%"        RestartModule FvwmIdent
+ "Sound &effect%mini.sound.xpm%"       RestartModule FvwmEvent
+ "Task&Bar%mini.exp.xpm%"              RestartModule FvwmTaskBar
+ "Window &List%mini.windows.xpm%"      RestartModule FvwmWinList
+ "&Wharf%mini.pencil.xpm%"             RestartModule FvwmWharf
+ "" Nop
+ "Kill modules..." Popup KillModules

DestroyMenu KillModules
AddToMenu KillModules
+ "BarB&uttons%mini.pencil.xpm%"        All (BarButtons) Close
+ "B&anner%mini.ray.xpm%"               KillModule FvwmBanner
+ "&Console%mini.xterm.xpm%"            KillModule FvwmConsole
+ "" Nop
+ "Unconfigured" Title
+ "&Autoraise%mini.raise.xpm%"          KillModule FvwmAuto
+ "A&nimate%mini.iconify.xpm%"          KillModule FvwmAnimate
+ "&Backer%mini.rainbow.xpm%"           KillModule FvwmBacker
+ "&Mini button bar%mini.pencil.xpm%"   All (MiniButtons) Close
+ "&WMaker buttons%mini.pencil.xpm%"    All (WinMakerApp*) Close
+ "&Debug%mini.bug2.xpm%"               KillModule FvwmDebug
+ "&IconBox%mini.icons.xpm%"            KillModule FvwmIconBox
+ "I&conMan%mini.run.xpm%"              KillModule FvwmIconMan
+ "Identif&y%mini.question.xpm%"        KillModule FvwmIdent
+ "Desker &Panel%mini.pager.xpm%"       All (DeskerPanelButtons) Close
+ "Sound &effect%mini.sound.xpm%"       KillModule FvwmEvent
+ "Task&Bar%mini.exp.xpm%"              KillModule FvwmTaskBar
+ "Window &List%mini.windows.xpm%"      KillModule FvwmWinList
+ "&Wharf%mini.pencil.xpm%"             KillModule FvwmWharf


DestroyMenu Utilities
AddToMenu Utilities "Home" Title
+ "X&term%mini.term.xpm%" Exec exec xterm
+ "r&oot%mini.term.xpm%" Exec exec xterm -T "root@elspeth : connecting" -e 'sudo -s'
+ "&ssh...%mini.connect.xpm%" Popup Remotes
+ "&Apps...%mini.desktop.xpm%" Popup Applications
+ "&Mods...%mini.modules.xpm%" Popup Modules
+ "" Nop
+ "&Restart%mini.turn.xpm%" Restart
+ "Slee&p%mini.display.xpm%" Exec exec pmset sleepnow
+ "Quit" Nop

DestroyMenu "Window"
AddToMenu "Window" "Window Operations" Title
+ "&Move%mini.move1.xpm%"               Move
+ "&Resize%mini.resize3.xpm%"           Resize
+ "(De)&Iconify%mini.iconify1.xpm%"     Iconify
+ "(Un)M&aximize%mini.maximize1.xpm%"   Maximize 100 100
+ "(Un)Max&wide%mini.maximize-horiz1.xpm%"	Maximize 100 0
+ "(Un)Max&tall%mini.maximize-vert1.xpm%"	Maximize   0 100
+ "(Un)&Shade%mini.shade1.xpm%"         WindowShade
+ "(Un)S&tick%mini.stick1.xpm%"         Stick
+ "R&aise%mini.raise2.xpm%"             Raise
+ "&Lower%mini.lower2.xpm%"             Lower
+ ""				Nop
+ "&Delete%mini.cross.xpm%"		Delete
+ "&Close%mini.delete.xpm%"		Close
+ "&Destroy%mini.bomb.xpm%"		Destroy
+ ""				Nop
+ "Move to &Page%mini.move1.xpm%"	Popup Move-window
+ "&Group Ops%mini.windows.xpm%"	Popup WindowGroupOps
+ "&Window Style%mini.window.xpm%"	Popup WindowStyle
+ "&Rearrange/Scroll%mini.windows.xpm%"	Popup WinRearrange
+ ""				Nop
+ "&Identify%mini.question.xpm%"	Module FvwmIdent
+ "Switch &to...%mini.windows.xpm%"	WindowList

#------------------------------------------------------------------------------
# A trimmed down version of "Window Ops", good for binding to decorations

DestroyMenu "Window-Ops2"
AddToMenu "Window-Ops2"
+ "&Move%mini.move1.xpm%"			Move
+ "&Resize%mini.resize3.xpm%"			Resize
+ "(De)&Iconify%mini.iconify1.xpm%"		Iconify
+ "(Un)M&aximize%mini.maximize1.xpm%"		Maximize 100 100
+ "(Un)Max&wide%mini.maximize-horiz1.xpm%"	Maximize 100 0
+ "(Un)Max&tall%mini.maximize-vert1.xpm%"	Maximize   0 100
+ "(Un)&Shade%mini.shade1.xpm%"			WindowShade
+ "(Un)S&tick%mini.stick1.xpm%"			Stick
#+ "R&aiseLower%mini.raise2.xpm%"		RaiseLower
+ ""			Nop
+ "&Close%mini.delete.xpm%"			Close
+ ""			Nop
+ "&Identify%mini.question.xpm%"		Module FvwmIdent
+ "More&...%mini.window.xpm%"			Menu Window This 0 0

#------------------------------------------------------------------------------
# A trimmed down version of "Window Ops", good for binding to TaskBar,
# WinList ... buttons

DestroyMenu "Window-Ops3"
AddToMenu "Window-Ops3"
+ "Move to &Page%mini.move1.xpm%"		Popup Move-window
+ "&Group Ops%mini.windows.xpm%"		Popup WindowGroupOps
+ ""			Nop
+ "(De)&Iconify%mini.iconify1.xpm%"		Iconify
+ "(Un)&Shade%mini.shade1.xpm%"			WindowShade
+ "(Un)S&tick%mini.stick1.xpm%"			Stick
+ "(Un)M&aximize%mini.maximize1.xpm%"		Maximize 95 95
+ "%mini.window.xpm%Scroll&Bar (75%% scr)"	Module FvwmScroll 75p 75p
+ ""			Nop
+ "&Close%mini.delete.xpm%"			Close
+ ""			Nop
+ "&Identify%mini.question.xpm%"		Module FvwmIdent
+ "More&...%mini.window.xpm%"			Menu Window This 0 0


#------------------------------------------------------------------------------
# A "Move Window" menu

DestroyMenu "Move-Window"
AddToMenu   "Move-Window" "Move to ..." Title
+ '&1 home%mini.move1.xpm%'	MoveToDesk 0 0
+ '&2 plat%mini.move1.xpm%'	MoveToDesk 0 1
+ '&3 net@%mini.move1.xpm%'	MoveToDesk 0 2
+ '&4 dev1%mini.move1.xpm%'	MoveToDesk 0 3
+ '&5 dev2%mini.move1.xpm%'	MoveToDesk 0 4
+ '&6 dev3%mini.move1.xpm%'	MoveToDesk 0 5
+ '&7 dev4%mini.move1.xpm%'	MoveToDesk 0 6
+ '&8 mail%mini.move1.xpm%'	MoveToDesk 0 7
+ '&9 @sch%mini.move1.xpm%'	MoveToDesk 0 8

#------------------------------------------------------------------------------
# A group version of WindowOps

DestroyMenu WindowGroupOps
AddToMenu   WindowGroupOps "Group Ops" Title
+ "&Iconify%mini.iconify1.xpm%"   Pick All ($c) Iconify on
+ "&DeIconify%mini.iconify1.xpm%" Pick All ($c) Iconify off
+ "&Shade%mini.shade1.xpm%"       Pick All ($c) WindowShade on
+ "&UnShade%mini.shade1.xpm%"     Pick All ($c) WindowShade off
+ "R&aise%mini.raise2.xpm%"       Pick All ($c) Raise
+ "&Lower%mini.lower2.xpm%"       Pick All ($c) Lower
+ "" Nop
+ "&Delete%mini.cross.xpm%"	Pick Delete
+ "&Close%mini.delete.xpm%"	Pick Close
+ "&Destroy%mini.bomb.xpm%"	Pick Destroy
+ "&Quick move to ..." Title
+ 'Desk &1%mini.move1.xpm%'	MoveToDesk 0 0
+ 'Desk &2%mini.move1.xpm%'	MoveToDesk 0 1
+ 'Desk &3%mini.move1.xpm%'	MoveToDesk 0 2
+ 'Desk &4%mini.move1.xpm%'	MoveToDesk 0 3
+ 'Desk &5%mini.move1.xpm%'	MoveToDesk 0 4
+ 'Desk &6%mini.move1.xpm%'	MoveToDesk 0 5
+ 'Desk &7%mini.move1.xpm%'	MoveToDesk 0 6
+ 'Desk &8%mini.move1.xpm%'	MoveToDesk 0 7
+ 'Desk &9%mini.move1.xpm%'	MoveToDesk 0 8

DestroyMenu WindowStyle
AddToMenu   WindowStyle
+ "%mini.window.xpm%Title&AtTop" Pick (CirculateHit) Style $[w.name] TitleAtTop
+ "%mini.window.xpm%&NoTitle"	 Pick (CirculateHit) Style $[w.name] !Title
+ "%mini.window.xpm%&Title"	 Pick (CirculateHit) Style $[w.name] !Title
+ "%mini.window.xpm%TitleAt&Bottom" Pick (CirculateHit) Style $[w.name] TitleAtBottom
+ "" Nop
+ "%mini.raise2.xpm%StaysOn&Top" Pick (CirculateHit) StyleAndRecapture $[w.name] StaysOnTop
+ "%mini.window.xpm%Stays&Put"   Pick (CirculateHit) StyleAndRecapture $[w.name] StaysPut
+ "%mini.lower2.xpm%StaysOn&Bottom"  Pick (CirculateHit) StyleAndRecapture $[w.name] StaysOnBottom
+ "" Nop
+ "%mini.window.xpm%&FvwmBorder"	Pick (CirculateHit) Style $[w.name] FvwmBorder
+ "%mini.window.xpm%&MWMBorder"		Pick (CirculateHit) Style $[w.name] MWMBorder
+ "%mini.window.xpm%&DepressableBorder"	Pick (CirculateHit) Style $[w.name] DepressableBorder
+ "%mini.window.xpm%&FirmBorder"	Pick (CirculateHit) Style $[w.name] FirmBorder
+ "%mini.window.xpm%&Handles"		Pick (CirculateHit) Style $[w.name] Handles
+ "%mini.window.xpm%&NoHandles"		Pick (CirculateHit) Style $[w.name] !Handles
+ "%mini.window.xpm%BorderWidth &0" Pick (CirculateHit) Style $[w.name] BorderWidth 0
+ "%mini.window.xpm%BorderWidth &5" Pick (CirculateHit) Style $[w.name] BorderWidth 5
+ "%mini.window.xpm%HandleWidth &0" Pick (CirculateHit) Style $[w.name] HandleWidth 0
+ "%mini.window.xpm%HandleWidth &7" Pick (CirculateHit) Style $[w.name] HandleWidth 7
+ "" Nop
+ "%mini.resize3.xpm%&ResizeOpaque"  Pick (CirculateHit) Style $[w.name] ResizeOpaque
+ "%mini.resize3.xpm%Resize&Outline" Pick (CirculateHit) Style $[w.name] ResizeOutline
+ "%mini.mouse.xpm%Sloppy&Focus"    Pick (CirculateHit) Style $[w.name] SloppyFocus
+ "%mini.mouse.xpm%&ClickToFocus"   Pick (CirculateHit) Style $[w.name] ClickToFocus
+ "%mini.mouse.xpm%&MouseFocus"     Pick (CirculateHit) Style $[w.name] MouseFocus
+ "%mini.mouse.xpm%&NeverFocus"     Pick (CirculateHit) Style $[w.name] NeverFocus

DestroyMenu WinRearrange
AddToMenu   WinRearrange
+ "&Cascade%mini.windows.xpm%" FvwmRearrange -cascade -m 3 3 -incx 2
+ "Tile &horizontally%mini.windows.xpm%" FvwmRearrange -tile -h -m 2 2 98 98
+ "Tile &vertically%mini.windows.xpm%" FvwmRearrange -tile -m 2 2 98 98
+ "" Nop
+ "&ScrollBar (1/2 app)%mini.window.xpm%"  Module FvwmScroll 2 2
+ "%mini.window.xpm%&Scroll&Bar (75%% scr)" Module FvwmScroll 75p 75p
+ "" Nop
+ "&Arrange Icons%mini.icons.xpm%" All (CurrentDesk Iconic) RecaptureWindow
+ "&Refresh Screen%mini.ray.xpm%" Refresh

ChangeMenuStyle wops Window Window-Ops2 Window-Ops3 Move-Window WindowGroupOps WindowStyle WinRearrange

Style "FvwmDesker" !Title, !Handles, Sticky, WindowListSkip, CirculateSkip
DestroyModuleConfig FvwmDesker: *
*FvwmDesker: UseSkipList
*FvwmDesker: Back black
*FvwmDesker: Fore white
*FvwmDesker: Hilight #530d00
*FvwmDesker: Geometry 792x44+5000+5000
*FvwmDesker: MiniIcons
*FvwmDesker: Balloons
*FvwmDesker: BalloonFore #007929
*FvwmDesker: BalloonBack black
*FvwmDesker: Font "xft:Liberation Sans:pixelsize=12:antialias=true"
*FvwmDesker: WindowColors grey20 #007929 grey80 #3f8fd2

Style BarButtons !Title, !Handles, Sticky, WindowListSkip, BorderWidth 0, CirculateSkip
DestroyModuleConfig BarButtons: *
*BarButtons: Font "xft:Liberation Sans:pixelsize=12:antialias=true"
*BarButtons: Fore white
*BarButtons: Back grey40
*BarButtons: Geometry 1012x46+0-14
*BarButtons: Rows 1
*BarButtons: (1x1, Title "sys", Swallow "xclock" "Exec exec xclock -padding 0 -update 1 -geometry -1500-1500", Action Popup Utilities)
*BarButtons: (1x1, Title term, Icon mini.term.xpm, Action 'Exec exec xterm')
*BarButtons: (1x1, Title edit, Icon mini.edit.xpm, Action 'Exec exec ec')
*BarButtons: (1x1, Title wins, Icon mini.pager.xpm, Action WindowList mouse -1p -1p)
*BarButtons: (3x1, Padding 0 0, Container(Padding 0 0, Frame 2, Rows 2))
*BarButtons: (Icon mini.move1.xpm, Action Move)
*BarButtons: (Icon mini.resize3.xpm, Action Resize)
*BarButtons: (Icon mini.iconify1.xpm, Action Iconify)
*BarButtons: (Icon mini.shade1.xpm, Action WindowShade)
*BarButtons: (Icon mini.stick1.xpm, Action Stick)
*BarButtons: (Icon mini.cross.xpm, Action Delete)
*BarButtons: (Icon mini.bomb.xpm, Action Destroy)
*BarButtons: (Icon mini.question.xpm, Action Module FvwmIdent)
*BarButtons: (End)
*BarButtons: (3x1, Title "load", Swallow "xload" "Exec exec nice -16 \
                xload -nolabel -update 1 -bg grey40 -hl grey70 \
                -geometry -1500-1500")
*BarButtons: (18x1, Swallow "FvwmDesker" 'FvwmPager FvwmDesker 0 0')

Module FvwmButtons BarButtons
#Module FvwmBanner

