default-cache-ttl 300
max-cache-ttl 999999
allow-emacs-pinentry
allow-loopback-pinentry
pinentry-program esyscmd(`which pinentry | tr -d "\n"')
