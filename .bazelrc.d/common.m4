divert(-1)dnl
########################################################################
# common Bazel configuration across my machines


########################################################################
# Host JVM tuning

define(HostJVM_SetHeap, `
startup		--host_jvm_args=-Xms$1
startup		--host_jvm_args=-Xmx$2')
define(HostJVM_TuneGC, `
startup		--host_jvm_args=-XX:-UseParallelGC')

# Linux-only: tell the kernel to batch/nice/ionice the build.
define(HostJVM_CPUNice, dnl
ifelse(__sysname__, Linux, `
startup		--batch_cpu_scheduling', `'))

define(HostJVM_IONice, dnl
ifelse(__sysname__, Linux, `
startup		--io_nice_level=7', `'))

# Persist Bazel buildservers for $1 seconds.
define(HostJVM_PersistentSec, `
startup		--max_idle_secs=$1')


########################################################################
# UI tuning:

define(UIDefaults, `
build		--progress_in_terminal_title
build		--show_timestamps')

# UI: make UI updates more useful
define(UISpeed, `
build:ui-xs	--curses=no
build:ui-s	--ui_actions_shown=24 --show_progress_rate_limit=1
build:ui-m	--ui_actions_shown=12 --show_progress_rate_limit=0.1
build:ui-l	--ui_actions_shown=6  --show_progress_rate_limit=0
build:ui-xl	--ui_actions_shown=24 --show_progress_rate_limit=0
build		--config=ui-$1')


########################################################################
# Build queue:

# Build queue: prepare and enqueue many jobs.
define(Build_Queued, `
build:queue-$1	--jobs=$2')
define(Build_QueueSize, `
Build_Queued(`xl', `10000')
Build_Queued(`l',  `1000')
Build_Queued(`m',  `100')
Build_Queued(`s',  `10')
Build_Queued(`xs', `1')')

# Build queue: run with many CPUs.
define(Build_LocalCPUs, `
build:jobs-$1	--local_resources=cpu=$1')
define(Build_NLocalCPUs, `
Build_LocalCPUs(`1')
Build_LocalCPUs(`2')
Build_LocalCPUs(`4')
Build_LocalCPUs(`8')
Build_LocalCPUs(`12')
Build_LocalCPUs(`16')')

########################################################################
# Build behaviour:

define(BuildBehaviour, `
# Try and make forward progress
build		--keep_going
# Be noisy when things break
build		--verbose_failures
# Provide build information
build		--stamp')

define(TestBehaviour, `
# Tests should be noisy.
test		--test_output=errors
test		--test_summary=detailed
test		--build_tests_only')

common		--generate_json_trace_profile
common		--profiles_to_retain=25

########################################################################
# Toolchains:

define(`ActionEnv', `--action_env=$1=$2 --host_action_env=$1=$2')

# Prefer LLVM toolchains.
define(Toolchain_Clang, `
build:llvm	ActionEnv(`BAZEL_COMPILER', `llvm')
build:llvm	ActionEnv(`CC', `clang')
build:llvm	ActionEnv(`CXX', `clang++')
build:llvm	--linkopt=-fuse-ld=lld')

define(Toolchain_CCacheClang, `
build:ccache	--sandbox_writable_path=__HOME__/.ccache
build:ccache	ActionEnv(`CC', __HOME__`/.ccache/bin/clang')
build:ccache	ActionEnv(`CXX', __HOME__`/.ccache/bin/clang++')')

define(`CXXOption', `ActionEnv(`BAZEL_CXXOPTS', `$1') --cxxopt=$1 --host_cxxopt=$1')

define(Language_CXX, `
build:cxx23	CXXOption(`-std=c++23')
build:cxx20	CXXOption(`-std=c++20')
build:cxx17	CXXOption(`-std=c++17')')

define(WithLibCXX, `
build:libc++	ActionEnv(`CXXFLAGS', `-stdlib=libc++')
build:libc++	ActionEnv(`LDFLAGS', `-stdlib=libc++')
build:libc++	ActionEnv(`BAZEL_CXXOPTS', `-stdlib=libc++')
build:libc++	ActionEnv(`BAZEL_LINKLIBS', `-l%:$1:-l%:$2')
build:libc++	ActionEnv(`BAZEL_LINKOPTS', `-lm:-pthread')')

define(Language_C, `
build:cgnu2x	--copt=-std=gnu2x --host_copt=-std=gnu2x
build:c2x	--copt=-std=c2x --host_copt=-std=c2x
build:c17	--copt=-std=c17 --host_copt=-std=c17')

define(WithPIC, `
build:fpic	--copt=-fPIC')

define(WithFission, `
build:fission	--fission=dbg,opt
build:fission	--features=per_object_debug_info')

define(SanitizerFlags_Common, `
build:san	--copt=-fsanitize-stats
build:san	--copt=-fno-omit-frame-pointer
build:san	--copt=-fno-optimize-sibling-calls
build:san	--copt=-fno-common
build:san	--copt=-g
build:san	--linkopt=--rtlib=compiler-rt')

define(SanitizerFlags_ASan, `
build:asan	--config=san
build:asan	--copt=-fsanitize=address
build:asan	--copt=-fsanitize-address-use-after-scope
build:asan	--linkopt=-fsanitize=address')

define(SanitizerFlags_UBSan, `
build:ubsan	--config=san
build:ubsan	--copt=-fsanitize=undefined
build:ubsan	--linkopt=-fsanitize=undefined')

# Kythe extractor configuration:
define(Flags_Kythe, `
build:kythe	--bazelrc=$KYTHE_RELEASE/extractors.bazelrc
build:kythe	--override_repository=kythe_release=$KYTHE_RELEASE
build:kythe	--experimental_extra_action_top_level_only=false')

# Allow Leiningen to see a JDK:
define(Flags_Leiningen, `
build:lein	--action_env=LEIN_JAVA_CMD=/usr/lib/jvm/jre-1.8.0/bin/java')

# Use dynamic shared objects.
define(Flags_Dyn, `
build:dyn	--dynamic_mode=fully')

# Enable the address sanitizer:
# build		--config=asan
