include(`.bazelrc.d/common.m4')
divert(0)dnl

HostJVM_SetHeap(`8G', `8G')
HostJVM_TuneGC
HostJVM_CPUNice
HostJVM_IONice
HostJVM_PersistentSec(`1800')

UIDefaults
UISpeed(`xl')

Build_QueueSize
Build_NLocalCPUs

BuildBehaviour
TestBehaviour

Toolchain_Clang
Toolchain_CCacheClang
# build		--config=llvm
# build		--config=ccache

Language_CXX
WithLibCXX(`libc++.so', `libc++abi.so')
Language_C

# build		--config=cxx20
# build		--config=cxx17

# build		--config=

WithPIC
# build		--config=fpic

WithFission
# build		--config=fission

SanitizerFlags_Common
SanitizerFlags_ASan
SanitizerFlags_UBSan

Flags_Kythe
Flags_Leiningen
Flags_Dyn
