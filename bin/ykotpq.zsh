#!/usr/bin/env zsh
# ykotpq --- query and copy a YubiKey OTP

set -e

ykman oath accounts list |
rofi -dmenu |
read x

ykman oath accounts code -s "${x}" |
tee >(xclip -in -selection clipboard) |
xargs -I% \
notify-send \
	--transient \
	--urgency=normal \
	--expire-time=$((5*1000)) \
	"ykman %" \
	"${x}"
