#!/usr/bin/env zsh

#
# /
#   dists/
#     ${distribution}/
#       Release			describes the whole suite
#       ${component}/
#         Contents-...		awk-style map, file to package
#           ...-all
#           ...-source
#           ...-<arch>
#         binary-<arch>/
#           Release
#           Packages
#

#DPKG_ARCH="$(dpkg --print-architecture)"
DPKG_ARCH=i386
ARCHIVE_ROOT="$1";
DISTRIBUTION="$2";
COMPONENT="$3";

fetch() {
	local that="$1"
#	[ -e "$that" ] && return
#	echo "${ARCHIVE_ROOT}/${that}"
	mkdir -p "$(dirname "$that")"
#	echo curl -o "$that" "${ARCHIVE_ROOT}/${that}"
	printf "%s/%s\n" \
		"${ARCHIVE_ROOT}" \
		"${that}"
	printf "\tout=%s\n" \
		"${that}"
	printf "\tallow-overwrite=true\n"
}

fetchmany() {
	while read f
	do
		fetch "$f"
	done
}

deb822() {
	local klass="$1"; shift;
	local file="$1"; shift;
	{
		echo "import os, sys, debian.deb822";
		echo "this = debian.deb822.${klass}(open(sys.argv[1]))";
		echo "$@"
	} | python3 - "$file"
}

prefix_with_distribution() {
	sed -e "s@^@dists/${DISTRIBUTION}/@"
}

fetch_distrib_Release() {
	fetch "dists/${DISTRIBUTION}/Release"
}

list_distrib_Release_files() {
	deb822 Release "dists/${DISTRIBUTION}/Release" \
		'[print(l["name"]) for l in this["SHA256"]]'
}

fetch_distrib_suite_Contents() {
	list_distrib_Release_files \
	| grep "^${COMPONENT}/binary-.*/Contents\.gz$" \
	| prefix_with_distribution \
	| fetchmany
}

fetch_distrib_suite_binary_Release() {
	list_distrib_Release_files \
	| grep "^${COMPONENT}/binary-.*/Release$" \
	| prefix_with_distribution \
	| fetchmany
}

fetch_distrib_suite_binary_Packages() {
	list_distrib_Release_files \
	| grep "^${COMPONENT}/binary-.*/Packages\.xz$" \
	| prefix_with_distribution \
	| fetchmany
}

fetch_distrib_suite_dep11_Components() {
	list_distrib_Release_files \
	| grep "^${COMPONENT}/dep11/Components-.*\.yml\.xz$" \
	| prefix_with_distribution \
	| fetchmany
}

fetch_distrib_suite_i18n_Translation() {
	list_distrib_Release_files \
	| grep "^${COMPONENT}/i18n/Translation-.*\.bz2$" \
	| prefix_with_distribution \
	| fetchmany
}

fetch_distrib_suite_source_Release() {
	list_distrib_Release_files \
	| grep "^${COMPONENT}/source/Release$" \
	| prefix_with_distribution \
	| fetchmany
}

fetch_distrib_suite_source_Sources() {
	list_distrib_Release_files \
	| grep "^${COMPONENT}/source/Sources\.xz$" \
	| prefix_with_distribution \
	| fetchmany
}

select_package() {
	local field="$1"
	local value="$2"
	local pkgfile
	printf "dists/%s/%s/binary-%s/%s" \
		"${DISTRIBUTION}" \
		"${COMPONENT}" \
		"${DPKG_ARCH}" \
		"Packages" |
	read pkgfile
	[ -e "${pkgfile}" ] || gunzip -k "${pkgfile}.gz"

	{
		echo "import os, sys, debian.deb822";
		echo "this = list(debian.deb822.Packages.iter_paragraphs(open(sys.argv[1])))";
		echo -n "[print(debian.deb822.Packages.dump(it))"
		echo -n " for it in this"
		echo    " if it.get(\"${field}\", None) == \"${value}\"]"
	} | python3 - "${pkgfile}"
}

main() {
	fetch_distrib_Release
	fetch_distrib_suite_Contents
	fetch_distrib_suite_binary_Release
	fetch_distrib_suite_binary_Packages
	fetch_distrib_suite_dep11_Components
	fetch_distrib_suite_i18n_Translation
	fetch_distrib_suite_source_Release
	fetch_distrib_suite_source_Sources

	select_package Source nvidia-graphics-drivers \
	| awk '/Filename:/ { print $2 }' \
	| fetchmany
}

# main
