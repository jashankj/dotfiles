#!/usr/bin/env zsh
# NAME: localize --- create a directory on the local system for here.
# SYNOPSIS: localize <directory>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

set -e

print -P '%~' | cut -d/ -f2- | tr / . | read localdir
dirname="$1"
mkdir -p "${HOME}/${dirname}s/${localdir}"
ln -svf "${HOME}/${dirname}s/${localdir}" "${dirname}"
