#!/usr/bin/env zsh
# NAME: pkg-enqueue-build --- enqueue a package in Poudriere
# SYNOPSIS: pkg enqueue-build [poudriere flags]
#
# 'poudriere queue' only talks to 'poudriered' which simply doesn't work.
# This script is compatible with 'poudriere daemon'.

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# -ge 1 ] || usage

echo "mainq       POUDRIERE_ARGS: bulk -j 11amd64 -p default $@" \
	| nc -U /var/run/poudriered.sock
