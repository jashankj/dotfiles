#!/usr/bin/env python3

from typing import Any, Iterable

import asyncio
import contextlib
import functools
import json
import os
import pathlib
import shutil
import subprocess
import sys
import urllib.parse

import aiohttp
import aiosqlite
import debian.deb822
import jsonrpc_websocket
import re2


_CREATE_TABLE_Release = '''
CREATE TABLE IF NOT EXISTS Release (
	release_id  INTEGER PRIMARY KEY,
	archive_root        TEXT NOT NULL,
	distribution        TEXT NOT NULL,
	component   TEXT NOT NULL,
	Origin      TEXT,
	Label       TEXT,
	Version     TEXT,
	Suite       TEXT,
	Codename    TEXT,
	Changelogs  TEXT,
	Date        TEXT,
	Valid_Until TEXT,
	NotAutomatic        TEXT,
	ButAutomaticUpgrades        TEXT,
	Snapshots   TEXT,
	Acquire_By_Hash     TEXT,
	No_Support_for_Architecture_all     TEXT,
	Architectures       TEXT,
	Components  TEXT,
	Description TEXT
);'''

async def INSERT_INTO_Release(
		dbh: aiosqlite.Connection,
		archive_root: str,
		distribution: str,
		component: str,
		Origin: str | None = None,
		Label: str | None = None,
		Version: str | None = None,
		Suite: str | None = None,
		Codename: str | None = None,
		Changelogs: str | None = None,
		Date: str | None = None,
		Valid_Until: str | None = None,
		NotAutomatic: str | None = None,
		ButAutomaticUpgrades: str | None = None,
		Snapshots: str | None = None,
		Acquire_By_Hash: str | None = None,
		No_Support_for_Architecture_all: str | None = None,
		Architectures: str | None = None,
		Components: str | None = None,
		Description: str | None = None
):
	return await dbh.execute_insert(
		'''
		INSERT INTO Release
			(archive_root, distribution, component,
			Origin, Label, Version, Suite, Codename, Changelogs, Date,
			Valid_Until, NotAutomatic, ButAutomaticUpgrades, Snapshots,
			Acquire_By_Hash, No_Support_for_Architecture_all,
			Architectures, Components, Description)
		VALUES (?, ?, ?,
			?, ?, ?, ?, ?, ?, ?,
			?, ?, ?, ?,
			?, ?,
			?, ?, ?);
		''',
		[archive_root, distribution, component,
		 Origin, Label, Version, Suite, Codename, Changelogs, Date,
		 Valid_Until, NotAutomatic, ButAutomaticUpgrades, Snapshots,
		 Acquire_By_Hash, No_Support_for_Architecture_all,
		 Architectures, Components, Description],
	)


_CREATE_TABLE_Release_files = '''
CREATE TABLE IF NOT EXISTS Release_files (
	release_id  INTEGER NOT NULL,
	name        TEXT NOT NULL,
	size        INTEGER,
	md5sum      TEXT,
	sha1sum     TEXT,
	sha256sum   TEXT,
	PRIMARY KEY (release_id, name),
	FOREIGN KEY (release_id) REFERENCES Release(release_id)
		ON DELETE CASCADE,
	CHECK (size >= 0)
);'''

async def INSERT_INTO_Release_files(
		dbh: aiosqlite.Connection,
		release_id: int,
		name: str,
		size: int,
		md5sum: str | None = None,
		sha1sum: str | None = None,
		sha256sum: str | None = None,
):
	return await dbh.execute_insert(
		'''
		INSERT INTO Release_files
			(release_id, name, size, md5sum, sha1sum, sha256sum)
		VALUES (?, ?, ?, ?, ?, ?);
		''',
		[release_id, name, size, md5sum, sha1sum, sha256sum],
	)

async def INSERT_MANY_INTO_Release_files(
		dbh: aiosqlite.Connection,
		release_id: int,
		items: Iterable[Iterable[Any]]
):
	return await dbh.executemany(
		'''
		INSERT INTO Release_files
			(release_id, name, size, md5sum, sha1sum, sha256sum)
		VALUES (?, ?, ?, ?, ?, ?);
		''',
		[(release_id, *rest) for rest in items]
	)


_CREATE_TABLE_Basket = '''
CREATE TABLE IF NOT EXISTS Basket (
	release_id   INTEGER NOT NULL,
	basket_id    INTEGER PRIMARY KEY,
	name         TEXT NOT NULL,
	FOREIGN KEY (release_id) REFERENCES Release(release_id)
		ON DELETE CASCADE
);'''

async def INSERT_INTO_Basket(
		dbh: aiosqlite.Connection,
		release_id: int,
		name: str
) -> int:
	return await dbh.execute_insert(
		'''
		INSERT INTO Basket (release_id, name)
		VALUES (?, ?);
		''',
		[release_id, name],
	)


_CREATE_TABLE_Packages = '''
CREATE TABLE IF NOT EXISTS Packages (
	basket_id   INTEGER NOT NULL,
	package_id  INTEGER PRIMARY KEY,
	Package     TEXT NOT NULL,
	Source      TEXT,
	Version     TEXT NOT NULL,
	Section     TEXT,
	Priority    TEXT,
	Architecture TEXT NOT NULL,
	Essential   TEXT,
	Installed_Size INTEGER,
	Maintainer  TEXT NOT NULL,
	Description TEXT NOT NULL,
	Description_md5 TEXT,
	Homepage    TEXT,
	Built_Using TEXT,
	Filename    TEXT NOT NULL,
	Size        INTEGER NOT NULL,
	md5sum      TEXT,
	sha1sum     TEXT,
	sha256sum   TEXT,
	Multi_Arch  TEXT,
	FOREIGN KEY (basket_id) REFERENCES Basket(basket_id)
		ON DELETE CASCADE,
	CHECK (Size >= 0),
	CHECK (Installed_Size >= 0)
);'''


_CREATE_TABLE_Packages_tag = '''
CREATE TABLE IF NOT EXISTS Packages_tag (
	package_id  INTEGER NOT NULL,
	tag         TEXT NOT NULL
);'''

_CREATE_INDEX_Packages_tag_tag = '''
CREATE INDEX IF NOT EXISTS Packages_tag_tag
	ON Packages_tag (tag);'''


_CREATE_TABLE_Dependencies = '''
CREATE TABLE IF NOT EXISTS Dependencies (
	from_package_id INTEGER NOT NULL,
	upon_expr       TEXT NOT NULL,
	nature TEXT,
	UNIQUE (from_package_id, upon_expr)
	FOREIGN KEY (from_package_id) REFERENCES Packages(package_id)
		ON DELETE CASCADE,
	CHECK (nature IN ('Depends', 'Pre-Depends', 'Recommends',
			'Suggests', 'Enhances', 'Breaks', 'Conflicts'))
);'''

_CREATE_TABLE_Contents = '''
CREATE TABLE IF NOT EXISTS Contents (
	basket_id	INTEGER NOT NULL,
	pathname    TEXT NOT NULL,
	provider    TEXT NOT NULL,
	FOREIGN KEY (basket_id) REFERENCES Basket(basket_id)
		ON DELETE CASCADE
);'''

_CREATE_INDEX_Contents_pathnames = '''
CREATE INDEX IF NOT EXISTS Contents_pathnames
	ON Contents (pathname);'''
_CREATE_INDEX_Contents_providers = '''
CREATE INDEX IF NOT EXISTS Contents_providers
	ON Contents (provider);'''


@contextlib.asynccontextmanager
async def StartAria2():
	aria2_proc = await asyncio.create_subprocess_exec(
		shutil.which('aria2c'),
		'--enable-rpc=true',
		'--file-allocation=falloc',
		'--enable-mmap=true',
	)
	try:
		# Allow some time to start and settle:
		await asyncio.sleep(1)
		yield aria2_proc
	finally:
		await aria2_proc.wait()


@contextlib.asynccontextmanager
async def Aria2RPCChannel():
	aria2 = jsonrpc_websocket.Server('ws://localhost:6800/jsonrpc')
	await aria2.ws_connect()
	try:
		yield aria2
	finally:
		await aria2.aria2.shutdown()
		await aria2.close()


#
# /
#   dists/
#     ${distribution}/
#       Release             describes the whole suite
#       ${component}/
#         Contents-...      awk-style map, file to package
#           ...-all
#           ...-source
#           ...-<arch>
#         binary-<arch>/
#           Release
#           Packages
#


def Once(): return asyncio.Queue(maxsize = 1)

_p = pathlib.Path()


class A2:
	def _on_download_complete(self, event):
		if not 'gid' in event:
			print(f"bad event from aria2c: no gid: {event}")
			return
		if not event['gid'] in self._ongoing:
			print(f"bad event from aria2c: unknown gid: {event}")
			return
		self._ongoing[event['gid']].put_nowait(event)

	def __init__(self, aria2):
		self._ongoing = {}
		if not aria2: return
		self._aria2 = aria2
		self._aria2.aria2.onDownloadComplete = \
			self._on_download_complete

	async def Fetch(self, url, **kwargs):
		once = Once()
		gid = await self._aria2.aria2.addUri([url], kwargs)
		self._ongoing[gid] = once
		await once.get()


class Deb:
	def __init__(
			self,
			archive_root: str,
			distribution: str,
			component: str
	):
		if archive_root[-1] != '/': archive_root += '/'
		self.archive_root = archive_root
		self.distribution = distribution
		self.component = component

	def URL(self, path):
		return urllib.parse.urljoin(self.archive_root, str(path))

	@functools.cached_property
	def path_dists_dist_Release(self):
		return _p / 'dists' / self.distribution / 'Release'

	@functools.cached_property
	def dists_dist_Release(self):
		return debian.deb822.Release(
			open(self.path_dists_dist_Release))

	async def get_dists_dist_Release(self, aria2):
		await aria2.Fetch(
			self.URL(self.path_dists_dist_Release),
			**{
				'out': str(self.path_dists_dist_Release),
				'allow-overwrite': True
			})
		return self.dists_dist_Release

async def insert_Release(dbh, deb):
	release = deb.dists_dist_Release
	(release_id,) = await INSERT_INTO_Release(
		dbh,
		deb.archive_root,
		deb.distribution,
		deb.component,
		release.get('Origin', None),
		release.get('Label', None),
		release.get('Version', None),
		release.get('Suite', None),
		release.get('Codename', None),
		release.get('Changelogs', None),
		release.get('Date', None),
		release.get('Valid-Until', None),
		release.get('NotAutomatic', None),
		release.get('ButAutomaticUpgrades', None),
		release.get('Snapshots', None),
		release.get('Acquire-By-Hash', None),
		release.get('No-Support-for-Architecture-all', None),
		release.get('Architectures', None),
		release.get('Components', None),
		release.get('Description', None),
	)
	return release_id


async def insert_Release_files(dbh, deb, release_id):
	rfiles = {}
	for (hashkey, hashsrc, hashtgt) in [
			('MD5Sum', 'md5sum', 'md5sum'),
			('SHA1',   'sha1',   'sha1sum'),
			('SHA256', 'sha256', 'sha256sum')
	]:
		for rfile in deb.dists_dist_Release.get(hashkey, []):
			rfiles[rfile['name']] = \
				rfiles.get(rfile['name'], {}) | \
				{'name': rfile['name'],
				 'size': int(rfile['size']),
				 hashtgt: rfile[hashsrc]}

	await INSERT_MANY_INTO_Release_files(
		dbh,
		release_id,
		[[
			rfile['name'],
			rfile['size'],
			rfile.get('md5sum', None),
			rfile.get('sha1sum', None),
			rfile.get('sha256sum', None)
		] for rfile in rfiles.values()]
	)


async def Main(
		aria2,
		dbh: aiosqlite.Connection,
		archive_root: str,
		distribution: str,
		component: str
) -> int:
	await dbh.execute(_CREATE_TABLE_Release)
	await dbh.execute(_CREATE_TABLE_Release_files)
	await dbh.execute(_CREATE_TABLE_Basket)
	await dbh.execute(_CREATE_TABLE_Packages)
	await dbh.execute(_CREATE_TABLE_Packages_tag)
	await dbh.execute(_CREATE_INDEX_Packages_tag_tag)
	await dbh.execute(_CREATE_TABLE_Dependencies)
	await dbh.execute(_CREATE_TABLE_Contents)
	await dbh.execute(_CREATE_INDEX_Contents_pathnames)
	await dbh.execute(_CREATE_INDEX_Contents_providers)

	deb = Deb(archive_root, distribution, component)

	# await deb.get_dists_dist_Release(aria2)
	release = deb.dists_dist_Release

	release_id = await insert_Release(dbh, deb)
	await insert_Release_files(dbh, deb, release_id)

	architectures = release['Architectures'].split(' ')

	async with asyncio.TaskGroup() as tg:
		[
			pathlib.Path(rfile['name'])
			for rfile in release.get(
					'SHA256', release.get(
						'SHA1', release.get('MD5Sum', {})))
		]

	await dbh.commit()

	return 0


async def Main0(args: list[str]) -> int:
#	async with StartAria2() as aria2_proc:
#		async with Aria2RPCChannel() as aria2:
			aria2 = None
			async with aiosqlite.connect('release.db') as dbh:
				_argv0, archive_root, distribution, component = args
				return await Main(
					A2(aria2), dbh,
					archive_root, distribution, component)

if __name__ == '__main__':
	sys.exit(asyncio.get_event_loop().run_until_complete(Main0(sys.argv)))
