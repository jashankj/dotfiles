#!/usr/bin/env zsh

pathdir="$(dirname "$(realpath "$0")")"
path=(${path:#${pathdir}})

if command -v ionice >/dev/null
then
	exec \
	ionice -c3 \
	"$(basename "$0")" "$@"
else
	exec \
	"$(basename "$0")" "$@"
fi
