#!/usr/bin/env zsh
# NAME: git-fpush --- sensible force push
# SYNOPSIS: git fpush <remote> <ref>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# -ne 2 ] && usage

remote=$1
ref=$2

exec git push --force-with-lease=$ref:$remote/$ref $remote $ref
