#!/usr/bin/env zsh
# NAME: ossify --- fossil to git sync tool
# SYNOPSIS: ossify <repo>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

case "$1" in
dotfiles)
	fossil_repo=/src/local/_fossil/dotfiles.fossil
	fossil_marks=$HOME/.git/dotfiles.fossil.marks
	git_marks=$HOME/.git/dotfiles.git.marks
	git_repo=$HOME/.git
	;;

*)
	usage
	;;
esac

cd $git_repo/..
fossil export \
	--git \
	--export-marks $fossil_marks \
	--import-marks $fossil_marks \
	$fossil_repo \
| git fast-import \
	--export-marks=$git_marks \
	--import-marks=$git_marks

git fast-export --all \
	--export-marks=$git_marks \
	--import-marks=$git_marks \
| fossil import \
	--git --incremental \
	--export-marks $fossil_marks \
	--import-marks $fossil_marks \
	$fossil_repo
