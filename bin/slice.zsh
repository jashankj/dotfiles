#!/usr/bin/env zsh
# NAME: slice --- extract lines in range
# SYNOPSIS: slice <head> <tail>
#
# Given zero-indexed line numbers <head> and <tail> in input, print all
# lines read from stdin in that range, inclusively.  Early exit.

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# -eq 2 ] || usage

head="$1"
tail="$2"

awk \
	-v "head=$head" \
	-v "tail=$tail" \
'
	(head <= (NR - 1) && (NR - 1) <= tail) { print };
	(! ((NR - 1) <= tail)) { exit };
'
