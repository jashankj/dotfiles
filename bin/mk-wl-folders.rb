#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: true

# NAME: mk-wl-folders --- dump out .folders for Wanderlust.
# SYNOPSIS: mk-wl-folders

require 'erb'
require 'set'

def unlines(args)
  args.join("\n")
end

MAILDIR  = File.join ENV['HOME'], 'Maildir'
TEMPLATE = File.join ENV['HOME'], '.folders.erb'
Dir.chdir MAILDIR
maildirs  = Dir['.*'].map { |i| i.gsub(/^\./, '%') }
maildirs  = Set.new maildirs
maildirs -= ['%', '%.']
source    = File.read(TEMPLATE)
template  = ERB.new source, trim_mode: '-'
puts template.result(binding)
