#!/usr/bin/env zsh
# NAME: cpanm-depends --- expand the entire dependency tree in Cpanm
# SYNOPSIS: cpanm-depends [package...]
#
# Expanding outwards until the result converges.
#
# Some hints from `zshexpn(1)' about what's going on:
#  * Parameter Expansion:
#  ** Parameter Expansion Flags:
#     `o':  Sort the resulting words in ascending order
#     `u':  Expand only the first occurrence of each unique word.
#  * Filename Generation:
#  ** Globbing Flags:
#     `s', `e':   more or less, `^' and `$'.

set -o err_exit
set -o extended_glob
# set -o xtrace

odeps=()
deps=( "$@" )

# Packages we've seen.
typeset -A seen

# Packages which can't ever be resolved
typeset -A ignored
ignored+=([perl]=1 [Config]=1)

# If there were no new dependencies, we're done.
# If there are now new dependencies, expand them too.
while [[ ${deps} != ${odeps} ]]
do
	odeps=( ${deps} )

	# Add all hard dependencies of the current dependency set.
	for dep in ${odeps}
	do
		[ ! -z "${seen[$dep]}" ] && continue
		seen[$dep]=1

		cpanm --showdeps ${dep} \
		| grep -v '^skipping ' \
		| grep -v '^--> Working on ' \
		| grep -v '^Fetching ' \
		| grep -v '^Configuring ' \
		| cut -d'~' -f1 \
		| sort | uniq \
		| while read p
		do
			[ ! -z "${ignored[$p]}" ] && continue
			printf '"%s" -> "%s";\n' "$dep" "$p"
			deps+=("$p")
		done
	done

	# |> sort |> uniq
	deps=( ${(ou)deps} )
done

print ${deps}
