#!/usr/bin/env zsh
# NAME: tv --- turn the TV screen on alyzon on and off
# SYNOPSIS: tv (on|off)

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"


export DISPLAY=:0.1

case "X$1" in
	Xon)
		xrandr --output HDMI1 --auto --primary
		sleep 1
		xrandr --output VGA1  --off
		;;

	Xoff)
		xrandr --output VGA1  --auto --primary
		sleep 1
		xrandr --output HDMI1 --off
		;;

	*)	usage ;;
esac
