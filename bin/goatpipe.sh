#!/bin/sh
# NAME: goatpipe --- run `goat' in a pipeline
# SYNOPSIS: ... | goatpipe

exec goat -- /dev/stdin
