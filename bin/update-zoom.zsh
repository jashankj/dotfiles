#!/usr/bin/env zsh

curl -s "https://zoom.us/rest/download?os=linux" \
	| jq -r '.result.downloadVO.zoom.version' \
	| read V
sudo dnf install "https://cdn.zoom.us/prod/${V}/zoom_x86_64.rpm"
