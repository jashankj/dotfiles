#!/usr/bin/env zsh
# NAME: zfs-cleanup --- ZFS snapshot cleanup tool
# SYNOPSIS: zfs-cleanup <dataset-name> [<daily-limit> [<hourly-limit>]]
# <options>
# 	dataset-name	dataset to clean up
# 	daily-limit	number of daily snapshots to keep (default: 60)
# 	hourly-limit	number of hourly snapshots to keep (default: 160)
# </options>

set -e

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

if [ "X$1" = "X" ]
then
	usage
fi

dataset="$1"
pool="${dataset/\/*/}"
daily_limit="${2-$((  9 *  7 ))}"
hourly_limit="${3-$(( 7 * 24 ))}"

TMPDIR="${TMPDIR-/tmp}"

echo "running snapshot cleanup (hourly=${hourly_limit}; daily=${daily_limit})"

#echo "taking weekly snapshot ($(date +%Y-%V))"
#zfs snapshot -r ${dataset}@$(date +%Y-%V)

zpool list -Hp "${pool}" |
read opool osize oalloc ofree \
	oxpand ofrag orcap odedup \
	ohealth oaltroot

output="$TMPDIR/zfs-cleanup.$$"
output_h="${output}.h"
output_d="${output}.d"

p=
[ "$HOST" = "alyzon" ] &&
p="-P$(getconf _NPROCESSORS_ONLN)"

echo "identifying snapshots for deletion"
zfs list -H -r -d 1 -t snapshot -o name "${dataset}" |
sort |
awk \
	-v dataset="${dataset}" \
	-v output_h="${output_h}" \
	-v output_d="${output_d}" \
	-v daily_limit="${daily_limit}" \
	-v hourly_limit="${hourly_limit}" \
'
	BEGIN {
		nh = 0; dh = dataset "@h_";
		nd = 0; dd = dataset "@d_";
	};
	match($0, dh) == 1 { h[++nh] = $0; next };
	match($0, dd) == 1 { d[++nd] = $0; next };
	END {
		hn = nh - hourly_limit;
		for (i = hn; i > 0; i--) print h[hn-i+1];
		print hn > output_h;
		dn = nd - daily_limit;
		for (i = dn; i > 0; i--) print d[dn-i+1];
		print dn > output_d;
	}
' |
xargs -t -L1 ${p} zfs destroy -dr 2>&1

echo "snapshot deletions sent, committing..."
sync; sleep 5; sync;
zpool list -Hp "${pool}" |
read npool nsize nalloc nfree \
	nxpand nfrag nrcap ndedup \
	nhealth naltroot

cat "${output_h}" | read hn
cat "${output_d}" | read dn

awk \
	-v hn=$hn \
	-v dn=$dn \
	-v oalloc=$oalloc \
	-v nalloc=$nalloc \
'
	BEGIN {
		printf( \
			"destroyed %d snapshots; reclaimed ~%d kB\n", \
			(hn + dn), \
			(oalloc / 1024) - (nalloc / 1024) \
		);
		exit
	}
'

rm -f "${output}.h"
rm -f "${output}.d"

#echo "kicking off 'zpool scrub'"
#exec zpool scrub ${dataset}
