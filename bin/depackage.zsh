#!/usr/bin/env zsh
# NAME: depackage --- dpkg(1) unroller and installer
# SYNOPSIS: depackage <.deb-package>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# -ne 1 ] && usage
[[ ! ( $1 =~ '\.deb$' ) ]] &&
    errx EX_USAGE "$1: not a .deb?"
[ -z $DEPKG_HOME ] &&
    errx EX_USAGE "\$DEPKG_HOME not set"

# DEPKG_HOME=$HOME/.depackage
mkdir -p ${DEPKG_HOME}

DEPKG_DB=${DEPKG_HOME}/etc/depkgdb
mkdir -p ${DEPKG_DB}

DEPKG_TMP=${DEPKG_HOME}/tmp
mkdir -p ${DEPKG_TMP}

pkg_name=$(dpkg-deb --showformat='${Package}\n' -W $1)
pkg_raw=${DEPKG_TMP}/${pkg_name}

dpkg-deb -e $1 ${pkg_raw}
for i in ${pkg_raw}/*
do
	mv ${i} ${DEPKG_DB}/${pkg_name}.$(basename ${i})
done
rmdir ${pkg_raw}

dpkg-deb -x $1 ${DEPKG_HOME}
