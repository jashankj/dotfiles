#!/bin/sh
# NAME: ldapsearch-ad --- wire up :man:`ldapsearch(1)` for AD LDAP search.
# SYNOPSIS: ldapsearch-ad <ldapsearch options...>
#
# ENVIRONMENT VARIABLES:
#     $AD_DOMAIN
#         the name of the Active Directory domain.
#     $AD_BASEDN
#         the base distinguished name of the Active Directory DIT.

[ ! -z "${AD_DOMAIN}" ] || exit 65 # EX_DATAERR
[ ! -z "${AD_BASEDN}" ] || exit 65 # EX_DATAERR

#
# 2021-03-31, jashankj:
#   A query directed at ``-H ldap://$DOMAIN/`` will no longer succeed if
#   GSSAPI is used.  But a query directed at a specific DC *does* work,
#   so we must first pick a DC.
#
#   An AD domain has a stack of DNS SRV records for DC services at magic
#   names --- e.g., _ldap._tcp.$DOMAIN --- and, for a domain with many
#   DCs, we can rely on DNS round-robin to rotate the load over multiple
#   DCs.  We can use :man:`host(1)` to retrieve these.
#
#   -T
#       use a TCP connection when querying the name server.
#       in a sufficiently-large cluster of DCs, there are enough SRV
#       records to hit the UDP-response limit, which would ordinarily
#       trigger a retry anyway, so forestall that.
#
#   -t TYPE
#       query for records of type `TYPE`; we want SRV records.
#
#   From :man:`host(1)`, those SRV records have the form::
#
#     _ldap._tcp.$DOMAIN has SRV record 0 100 389 infpwdc011.$DOMAIN.
#
#   So we pick out column 8, and that's our LDAP server.
#
# 2021-09-02, jashankj:
#   ... something's broken in UNSW's SRV DNS service, so this is broken.
#   Instead, just use round-robin DNS and pick a server, I guess.  Using
#   DNS-over-TCP seems to trigger something really badly behaved. 

#ldap_server=$( \
#	host -T -t SRV _ldap._tcp.${AD_DOMAIN} \
#	| awk '{ print $8; exit }'
#)

ldap_server=$( \
	host -t A ${AD_DOMAIN} \
	| awk '{ print $4; exit }'
)

#
# 2021-05-31, jashankj:
#   Okay, fine, this :man:`ldapsearch(1)` incantation takes these flags:
#
#   -H LDAP-URI
#       specifies LDAP server(s) by providing a comma- or whitespace-
#       separated list of URIs, for which only protocol, host, and port
#       fields are allowed.
#
#   -Y SASL-MECHANISM
#       specifies the SASL mechanism to be used for authentication; we
#       use GSSAPI as we are assuming a Kerberised world.
#
#   -LLL
#       display results in plain LDIFv1 without comments or version.
#
#   -o OPT[=OPT-PARAM]
#       where `OPT' may be `ldif-wrap', which accepts a width in columns
#       or "no" to disable text wrapping.
#
#   -s {base|one|sub|children}
#       specifies the scope of the search, choosing either a base
#       object, one-level, sub-tree, or children search.
#
#   -b SEARCH-BASE-DN
#       specifies the starting point for the search.
#
#   All other command-line arguments are passed directly.  The first
#   non-flag argument is an LDAP search filter, per RFC 4515.  Any
#   following arguments list the attributes to be returned.
#
exec ldapsearch \
	-H ldap://${ldap_server}/ -Y GSSAPI \
	-LLL -o ldif-wrap=no \
	-s sub -b "${AD_BASEDN}" \
	"$@"
