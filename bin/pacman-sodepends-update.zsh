#!/usr/bin/env zsh
# NAME: pacman-sodepends-update --- (badly) list binaries which have
#     shared-library dependencies on any presenly-outdated packages
# SYNOPSIS:
#     $ find bin/ -type f | xargs pacman-sodepends-update

join \
	<(pacman -Qu \
	| awk '{ print $1 }' \
	| xargs pacman -Ql \
	| awk '
		/\/$/ { next };
		{ print $2 "\t" $1}
	' \
	| sort -k 1b,1 \
	| uniq) \
	<(file "$@" \
	| grep 'ELF ' \
	| sed 's@:.*@@' \
	| xargs ldd \
	| awk '
		/:$/ { bin = $1; };
		/=>/ { libs[$3] = libs[$3] " " bin; };
		END {
			for (lib in libs) {
				print lib "\t" libs[lib];
			}
		}
	' \
	| sed 's@:@@g' \
	| sort -k 1b,1)
