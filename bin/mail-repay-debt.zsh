#!/usr/bin/env zsh
# NAME: mail-repay-debt --- move emails out of debt folders
# SYNOPSIS: mail-repay-debt

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

typeset -A debts
typeset -A repayments

debts=(
	[unsw]=6452         # (46.49%)
	[gmail]=2999        # (21.61%)
	[csiro]=1247        # ( 8.99%)
	[emeralfel]=3180    # (22.91%)
)
total_debt=13878

#
#for n in {5..30}
#do
#	:| awk '
#	END {
#		nn = n * 4;
#		dd = debt / nn;
#		printf( \
#			"debt=%5d    Δ=%4d/day  ==>  %3d days (%.01f%% year)\n", \
#			debt, -nn, dd, dd / 365.0 * 100.0 \
#		);
#		exit
#	}' \
#		   n="$n" \
#		   debt="${total_debt}"
#done
#
# debt=13878    Δ= -20/day  ==>  693 days (190.1% year)
# debt=13878    Δ= -24/day  ==>  578 days (158.4% year)
# debt=13878    Δ= -28/day  ==>  495 days (135.8% year)
# debt=13878    Δ= -32/day  ==>  433 days (118.8% year)
# debt=13878    Δ= -36/day  ==>  385 days (105.6% year)
# debt=13878    Δ= -40/day  ==>  346 days (95.1% year)
# debt=13878    Δ= -44/day  ==>  315 days (86.4% year)
# debt=13878    Δ= -48/day  ==>  289 days (79.2% year)
# debt=13878    Δ= -52/day  ==>  266 days (73.1% year)
# debt=13878    Δ= -56/day  ==>  247 days (67.9% year)
# debt=13878    Δ= -60/day  ==>  231 days (63.4% year)  <<<<<
# debt=13878    Δ= -64/day  ==>  216 days (59.4% year)
# debt=13878    Δ= -68/day  ==>  204 days (55.9% year)
# debt=13878    Δ= -72/day  ==>  192 days (52.8% year)
# debt=13878    Δ= -76/day  ==>  182 days (50.0% year)
# debt=13878    Δ= -80/day  ==>  173 days (47.5% year)
# debt=13878    Δ= -84/day  ==>  165 days (45.3% year)
# debt=13878    Δ= -88/day  ==>  157 days (43.2% year)
# debt=13878    Δ= -92/day  ==>  150 days (41.3% year)
# debt=13878    Δ= -96/day  ==>  144 days (39.6% year)
# debt=13878    Δ=-100/day  ==>  138 days (38.0% year)
# debt=13878    Δ=-104/day  ==>  133 days (36.6% year)
# debt=13878    Δ=-108/day  ==>  128 days (35.2% year)
# debt=13878    Δ=-112/day  ==>  123 days (33.9% year)
# debt=13878    Δ=-116/day  ==>  119 days (32.8% year)
# debt=13878    Δ=-120/day  ==>  115 days (31.7% year)
#

repayment_sum=60
for k in ${(k)debts}
do
	repayments+=([$k]=$(\
		:| awk 'END { printf("%d\n", sum * (this / total)); }' \
		sum="${repayment_sum}" \
		this="${debts[$k]}" \
		total="${total_debt}" \
	))
done

n_inboxes=0
for repo in $(ccat ~/etc/mail.repos)
do
	[ $repo = conglomerate ] && continue
	inbox="${repo}.INBOX"
	n_inbox="$(doveadm search mailbox "${inbox}" 2>/dev/null | wc -l)"
	n_inboxes=$(( n_inboxes + n_inbox ))
done

[ ${n_inboxes} -gt 1024 ] && exit 1


for repo in $(ccat ~/etc/mail.repos)
do
	[ $repo = conglomerate ] && continue
	inbox="${repo}.INBOX"
	case "$repo" in
		(emeralfel|gmail|unsw)
			archive="${repo}.Debt"    ;;
		*)	archive="${repo}.Archive" ;;
	esac
#	n_inbox="$(doveadm search mailbox "${inbox}"   2>/dev/null | wc -l)"
#	n_debt="$( doveadm search mailbox "${archive}" 2>/dev/null | wc -l)"

	doveadm -v move -u jashank \
		"${inbox}" \
		1:"${repayments[$repo]}" mailbox "${archive}"
done
