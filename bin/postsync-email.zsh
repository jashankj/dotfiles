#!/usr/bin/env zsh
# NAME: postsync-email --- run email synchronisation hooks
# SYNOPSIS: postsync-email <repository>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

set -e

[ ! -z "${1}" ] || usage
repo="${1}"

mail_root="$HOME/Mail"
mail_repo="${mail_root}/${repo}"
[ -d "${mail_repo}" ] ||
	errx EX_USAGE "unknown repository '${repo}'"

cd "${mail_repo}"

if [ -e ".git" ]
then
	git add -A ||:
	git commit --no-gpg-sign \
		-m "offlineimap $(date +'%Y-%m-%d %H:%M:%S')" ||:
fi

notmuch_config="${mail_root}/${repo}.notmuch"
if [ -e "${notmuch_config}" ]
then
	notmuch --config="${notmuch_config}" new
fi

repo_count="${mail_root}/.stats/${repo}.count"
if [ -e "${notmuch_config}" ]
then
	printf "%d %d\n" \
		"$(date +%s)" \
		"$(notmuch --config="${notmuch_config}" count folder:INBOX)" \
	>> "${repo_count}"
else
	printf "%d %d\n" \
		"$(date +%s)" \
		"$( (ls INBOX/cur; ls INBOX/new) | wc -l)" \
	>> "${repo_count}"
fi

