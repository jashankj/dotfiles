#!/bin/sh
# NAME: cpumask --- run `command' bound to `ncpus' randomly-selected CPUs.
# SYNOPSIS: cpumask <ncpus> <command...>
#
# Uses `cpupick' for the computation.

NCPUS="$1"; shift
exec taskset "$(cpupick "$NCPUS")" "$@"
