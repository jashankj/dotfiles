#!/usr/bin/env perl
#-
# Copyright (c) 1993-2018 John Shepherd <jas@cse.unsw.edu.au>
#

#
# Convert file in .slides format to HTML notes
#

#
# Special directories
#
$HTMLib = "/home/jas/lib/html";

#
# Colours ... for tailoring easily
#
$notesBG = "white";

#
#
# Get the date
#
@now = localtime(time());
@m = ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
$date = sprintf("%d %s %d", $now[3], $m[$now[4]], 1900+$now[5]);

#
# Subroutine to apply standard in-line conversions
#
sub line_filter
{
	$_[0] =~ s/<\$>/<i>/g;
	$_[0] =~ s/<\/\$>/<\/i>/g;
	$_[0] =~ s/<@>/<large><code>/g;
	$_[0] =~ s/<@@>/<large><font color="#008800"><b><code>/g;
	$_[0] =~ s/<\/@>/<\/code><\/large>/g;
	$_[0] =~ s/<\/@@>/<\/code><\/b><\/font><\/large>/g;
	$_[0] =~ s/<tilde>/~/g;
	$_[0] =~ s/<at>/@/g;
	$_[0] =~ s/<floor>/<big>&lfloor;<\/big>/g;
	$_[0] =~ s/<\/floor>/<big>&rfloor;<\/big>/g;
	$_[0] =~ s/<ceil>/<big>&lceil;<\/big>/g;
	$_[0] =~ s/<\/ceil>/<big>&rceil;<\/big>/g;
	$_[0] =~ s/<hspace>/\&nbsp;/g;
	$_[0] =~ s/<\~>/\&nbsp;/g;
	$_[0] =~ s/<\~\~>/\&nbsp;\&nbsp;/g;
	$_[0] =~ s/<\~\~\~>/\&nbsp;\&nbsp;\&nbsp;/g;
	$_[0] =~ s/<\~\~\~\~>/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g;
	$_[0] =~ s/<dash>/\&#8212;/g;
	$_[0] =~ s/<q>/"/g;
	$_[0] =~ s/<\/q>/"/g;
	$_[0] =~ s/<dollar>/\$/g;
	$_[0] =~ s/<left\(>/(/g;
	$_[0] =~ s/<right\)>/)/g;
	$_[0] =~ s/<Rightarrow>/\&rArr;/g;
	$_[0] =~ s/<Lightarrow>/\&lArr;/g;
	$_[0] =~ s/<notRightarrow>/\&#8655;/g;
	$_[0] =~ s/<rightarrow>/\&rarr;/g;
	$_[0] =~ s/<notrightarrow>/\&#8603;/g;
	$_[0] =~ s/<leftarrow>/\&larr;/g;
	$_[0] =~ s/<leftrightarrow>/\&harr;/g;
	$_[0] =~ s/<approx>/\&cong;/g;
	$_[0] =~ s/<and>/\&and;/g;
	$_[0] =~ s/<or>/\&or;/g;
	$_[0] =~ s/<not><elem>/\&notin;/g;
	$_[0] =~ s/<not><in>/\&notin;/g;
	$_[0] =~ s/<not>/\&not;/g;
	$_[0] =~ s/<in>/\&isin;/g;
	$_[0] =~ s/<elem>/\&isin;/g;
	$_[0] =~ s/<leq>/\&le;/g;
	$_[0] =~ s/<geq>/\&ge;/g;
	$_[0] =~ s/<neq>/\&ne;/g;
	$_[0] =~ s/<sum>/\&sum;/g;
	$_[0] =~ s/<prod>/\&prod;/g;
	$_[0] =~ s/<sqrt>/\&radic;/g;
	$_[0] =~ s/<\/sqrt>//g;
	$_[0] =~ s/<plusmn>/\&plusmn;/g;
	$_[0] =~ s/<sel>/\&sigma;/g;
	$_[0] =~ s/<proj>/\&pi;/g;
	$_[0] =~ s/<join>/\&#8904;/g;
	$_[0] =~ s/<renam>/<img src="sym\/renam.gif">/g;
	$_[0] =~ s/<subset>/\&sub;/g;
	$_[0] =~ s/<subseteq>/\&sube;/g;
	$_[0] =~ s/<times>/\&times;/g;
	$_[0] =~ s/<union>/\&cup;/g;
	$_[0] =~ s/<intersect>/\&cap;/g;
	$_[0] =~ s/<exists>/\&exist;/g;
	$_[0] =~ s/<forall>/\&forall;/g;
	$_[0] =~ s/<delta>/\&delta;/g;
	$_[0] =~ s/<infty>/\&infin;/g;
	$_[0] =~ s/<empty>/\&empty;/g;
	$_[0] =~ s/<oplus>/\&oplus;/g;
	$_[0] =~ s/<bigoplus>/<big>&oplus;<\/big>>/g;
	$_[0] =~ s/<ll>/\&ll;/g;
	$_[0] =~ s/<gg>/\&gg;/g;
	$_[0] =~ s/<log>/log/g;
	$_[0] =~ s/<dollar>/\$/g;
	$_[0] =~ s/<frac>//g;
	$_[0] =~ s/<divides>/\//g;
	$_[0] =~ s/<over>/\//g;
	$_[0] =~ s/<\/frac>//g;
	$_[0] =~ s/<progdef>/<font color='#996600'><b><large><code>/g;
	$_[0] =~ s/<\/progdef>/<\/code><\/large><\/b><\/font>/g;
	$_[0] =~ s/<brown>/<font color='#996600'>/g;
	$_[0] =~ s/<\/brown>/<\/font>/g;
	$_[0] =~ s/<navy>/<font color='#000099'>/g;
	$_[0] =~ s/<\/navy>/<\/font>/g;
	$_[0] =~ s/<red>/<font color='#CC0000'>/g;
	$_[0] =~ s/<\/red>/<\/font>/g;
	$_[0] =~ s/<blue>/<font color='#0000CC'>/g;
	$_[0] =~ s/<\/blue>/<\/font>/g;
	$_[0] =~ s/<orange>/<font color='#CC9900'>/g;
	$_[0] =~ s/<\/orange>/<\/font>/g;
	$_[0] =~ s/<green>/<font color='#009900'>/g;
	$_[0] =~ s/<\/green>/<\/font>/g;
	$_[0] =~ s/<gray>/<font color='#999999'>/g;
	$_[0] =~ s/<gb>/<font color="green"><b>/g;
	$_[0] =~ s/<\/gb>/<\/b><\/font>/g;
	$_[0] =~ s/<rb>/<font color="red"><i>/g;
	$_[0] =~ s/<\/rb>/<\/i><\/font>/g;
	$_[0] =~ s/<\/gray>/<\/font>/g;
	$_[0] =~ s/<href=([^>]+)>/<a href="$1">$1<\/a>/;
	$_[0] =~ s/<</\&lt;/g;
	$_[0] =~ s/>>>/>\&gt;/g;
	$_[0] =~ s/>>/\&gt;/g;
	$_[0] =~ s/<nerd>/<span style='color:#000099;font-weight:bold;font-size:110%'>/g;
	$_[0] =~ s/<\/nerd>/<\/span>/g;
	if ($indeftable || $intable) {
		$_[0] =~ s/<row>/<tr valign=top>/g;
		$_[0] =~ s/<\/row>/<\/tr>/g;
		$_[0] =~ s/<col1>/<td><nobr>/g;
		$_[0] =~ s/<col[2-9]>/<td><\/td><td>/g;
		$_[0] =~ s/<\/col[0-9]>/<\/td>/g;
	}
	elsif ($inreltable) {
		$_[0] =~ s/<row>/<tr align=center>/g;
		$_[0] =~ s/<\/row>/<\/tr>/g;
		$_[0] =~ s/<col[0-9]>/<td>/g;
		$_[0] =~ s/<\/col[0-9]>/<\/td>/g;
		$_[0] =~ s/<attr[0-9]>/<td>\&nbsp;\&nbsp;<b>/g;
		$_[0] =~ s/<\/attr[0-9]>/<\/b>\&nbsp;\&nbsp;<\/td>/g;
	}
	if ($_[0] =~ /<image ([^>]*)>/) # <image file-name>
	{
		$image = $1;
		if ($image !~ /\./) { $image = "$image.png" }
		$image =~ s/\./-small./;
		$_[0] =~ s/<image [^>]*>/<img src="$image">/
	}
}

#
# Check args and open .slides file
#
if ($#ARGV != 0 || $ARGV[0] !~ /\.slides$/) { die "Usage: $0 xyz.slides"; }
open(SLIDES, "<$ARGV[0]") || die "$0: Can't open slides file $ARGV[0]";

$title = <SLIDES>;
chop($title);
if ($title !~ /^<title>/)
	{ die "$0: Can't find title in $ARGV[0]"; }
else
	{ $title =~ s/<title>//; }

#
# Count slides
# (so we know when we reach last on the second (main) pass)
#
$max_slide = 0;
$inslide = 0;
$line = 1;  ## we read <title> line already
while (<SLIDES>)
{
	$line++;
	if (/^<slide>/)
	{
		if ($inslide == 1)
			{ die "Missing </slide> near line $line"; }
		$max_slide++;
		$inslide = 1;
	}
	elsif (/^<\/slide>/)
	{
		if ($inslide == 0)
			{ die "Too many </slide>'s at line $line"; }
		$inslide = 0;
	}
}
if ($inslide == 1)
	{ die "Missing </slide> near line $line"; }

#
# Rewind slides file for second pass
#
seek(SLIDES, 0, 0);

#
# Make symbols directory
#
if (! -d "sym")
{
	mkdir("sym",0755) || die "Can't make symbols directory";
	system "cp $HTMLib/sym/*.gif ./sym";
}

#
# Copy notes.css if needed
#
if (! -f "notes.css")
{
	system "cp $HTMLib/notes.css ./notes.css";
}

#
# Open notes file
#
open(NOTES, ">notes.html") || die "$0: Can't open notes file";
print NOTES "<html>\n";
print NOTES "<head>\n<title>$title</title>\n";
print NOTES "<link href='notes.css' rel='stylesheet' type='text/css'>\n";
print NOTES "</head>\n<body>\n";
print NOTES "<p><span class='title'>$title</span><p>\n";

#
# Read the .slides file and produce slides and notes
#
$snum = 1;
$eqnum = 0;
$inslide = 0;
while (<SLIDES>)
{
	chop;
	if (/^%/)
	{
	}
	elsif (/^<slide>/)
	{
		$inslide = 1;
		print NOTES "<p><hr><p>\n";
	}
	elsif (/^<\/slide>/)
	{
		$snum++;
		$inslide = 0;
	}
	elsif (/^<section>/)
	{
		($heading = $_) =~ s/<section>//;
		&line_filter($heading);
		print NOTES "<table width='100%' cellpadding='0'>\n".
				"<tr valign='top'><td align='left'>".
				"<span class='section'>$heading</span>".
				"</td><td align='right'>".
				"</td></tr></table>\n<p>\n";
	}
	elsif (/^<heading>/)
	{
		($heading = $_) =~ s/<heading>//;
		&line_filter($heading);
		$curHeading = $heading;
		print NOTES "<table width='100%' cellpadding='0'>\n".
				"<tr valign='top'><td align='left'>".
				"<span class='heading'>$heading</span>".
				"</td><td align='right'>".
				"<small>$snum/$max_slide</small>".
				"</td></tr></table>\n<p>\n";
	}
	elsif (/^<subheading>/)
	{
		($heading = $_) =~ s/<subheading>//;
		$heading =~ s/<\/subheading>//;
		&line_filter($heading);
		$heading =~ s/<br>/\&nbsp;/g;
		print NOTES "<h4>$heading</h4>\n";
	}
	elsif (/^<exercise>/)
	{
		($heading = $_) =~ s/<exercise>//;
		$nex++;
		&line_filter($heading);
		$heading = "Exercise $nex: $heading";
		        print NOTES "<table width='100%' cellpadding='0'>\n".
                "<tr valign='top'><td align='left'>".
                "<span class='heading'>$heading</span>".
                "</td><td align='right'>".
                "<small>$snum/$max_slide</small>".
                "</td></tr></table>\n<p>\n";
	}
	elsif (/^<continued>/)
	{
		$heading = $curHeading;
		print NOTES "<table width='100%' cellpadding='0'>\n".
				"<tr valign='top'><td align='left'>".
				"<span class='cont'>... $heading</span>".
				"</td><td align='right'>".
				"<small>$snum/$max_slide</small>".
				"</td></tr></table>\n<p>\n";
	}
	elsif (/^<diagram>/) # <diagram>file-name
	{
		($image = $_) =~ s/<diagram>//;
		if ($image !~ /\./) { $image = "$image.png" }
		$image =~ s/\./-small./;
		print NOTES "<p><div class='center'>\n<img alt=\"[Diagram:$image]\"";
		print NOTES " src=\"$image\">\n</div><p>\n";
	}
	elsif (/^\<citation /) # <citation tag>authors
	{
		($line = $_) =~ s/^<citation //;
		($tag = $line) =~ s/>.*$//;
		($cite = $line) =~ s/.*>//;
		print NOTES "[<a href=\"#$tag\">$cite</a>]\n";
	}
	elsif (/^<pp>/)
	{
		print NOTES "<p>\n";
	}
	elsif (/^<vspace /)
	{
		($n = $_) =~ s/<vspace //;
		$n =~ s/>.*$//;
		for ($i = 1; $i < $n; $i++)
		{
			print NOTES "<p><br>";
		}
		print NOTES "<p>\n";
	}
	elsif (/^<\$\$>/)
	{	print NOTES "<p><div class='center'><i>\n"; 
	}
	elsif (/^<\/\$\$>/)
	{	print NOTES "</i></div><p>\n"; 
	}
	elsif (/^<eqn>/)
	{
		($eqn = $_) =~ s/<eqn>//;
		$eqn_file = sprintf("eqn/%03d.gif", $eqnum);
		system("/home/jas/bin/scripts/e2g '$eqn' $eqn_file 2>&1 > /dev/null");
		$eqnum++;
		print NOTES "<img alt='$eqn' src='$eqn_file'>\n";
	}
	elsif (/^<center>/)
	{	print NOTES "<div class='center'>\n";
	}
	elsif (/^<\/center>/)
	{	print NOTES "<\/div>\n";
	}
	elsif (/^<indent>/)
	{	print NOTES "<dl><dd>\n";
	}
	elsif (/^<\/indent>/)
	{	print NOTES "<\/dd><\/dl>\n";
	}
	elsif (/^<itemize>/)
	{	print NOTES "<ul>\n"; $nitems = 0;
	}
	elsif (/^<\/itemize!>/)
	{	print NOTES "</ul>\n";
	}
	elsif (/^<\/itemize>/)
	{	print NOTES "</ul>\n";
	}
	elsif (/^<enumerate>/)
	{	print NOTES "<ol>\n";
	}
	elsif (/^<\/enumerate>/)
	{	print NOTES "</ol>\n";
	}
	elsif (/^<item>/ || /^<sitem>/)
	{
		if (/^<sitem>/) { $small=1 } else { $small=0 }
		($line = $_) =~ s/<s?item>/<li>/;
		&line_filter($line);
		print NOTES "$line\n";
		$nitems++;
	}
	elsif (/^<program>/ || /^<session>/ || /^<syntax>/)
	{	print NOTES "<p><pre>\n";
	}
	elsif (/^<prog>/)
	{	print NOTES "<p><pre>\n";
	}
	elsif (/^<sprogram>/ || /^<ssession>/)
	{	print NOTES "<p><pre><small>\n";
	}
	elsif (/^<\/program>/ || /^<\/session>/ || /^<\/syntax>/)
	{	print NOTES "</pre><p>\n";
	}
	elsif (/^<\/prog>/)
	{	print NOTES "</pre><p>\n";
	}
	elsif (/^<\/sprogram>/ || /^<\/ssession>/)
	{	print NOTES "</small></pre><p>\n";
	}
	elsif (/^<deftable>/ || /^<deftable [0-9]>/)
	{	print NOTES "<p><table border='0' cellpadding='6'>\n";
		$indeftable = 1;
	}
	elsif (/^<reltable [0-9]>/)
	{	print NOTES "<p><table border='1' cellpadding='2'>\n";
		$inreltable = 1;
	}
	elsif (/^<table ([0-9])>/)
	{	print NOTES "<table border='0' cellpadding='4'>\n";
		$intable = 1; $ncols = $1;
	}
	elsif (/^<\/deftable>/)
	{	print NOTES "</table><p>\n";
		$indeftable = 0;
	}
	elsif (/^<\/reltable>/)
	{	print NOTES "</table><p>\n";
		$inreltable = 0;
	}
	elsif (/^<\/table>/)
	{	print NOTES "</table>\n";
		$intable = 0; $ncols = 0;
	}
	elsif (/^<url /)
	{
		($url = $_) =~ s/<url //;
		$url =~ s/>.*$//;
		($txt = $_) =~ s/^<url [^>]*>//;
		&line_filter($txt);
		print NOTES "<a target=demo href=\"http://$url\">$txt</a>\n";
	}
	else {
		if ($inslide == 1)
		{
			$line = $_;
			&line_filter($line);
			print NOTES "$line\n";
		}
	}
}

#
# Finish off the notes file
#

print NOTES "<p><hr><p>\n";
print NOTES "<small><small>Produced: $date</small></small>\n";
print NOTES "</body>\n</html>\n";
close(NOTES);
