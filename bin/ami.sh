#!/bin/sh

case "$#" in
	(1)	;;
	(*)
		echo "usage: ami <hostname>"
		exit 64 # EX_USAGE
	;;
esac

. "$HOME/.zsh/functions/ami"
ami "$@"
