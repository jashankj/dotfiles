#!/bin/sh
exec wkhtmltopdf -q -O Landscape -s A4 "$1" "${1%.html}.pdf"
