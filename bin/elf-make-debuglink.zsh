#!/usr/bin/env zsh
# NAME: elf-make-debuglink --- extract debugging symbols
# SYNOPSIS: elf-make-debuglink [files...]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# -lt 1 ] && usage

for i
do
	full="${i}.full"
	syms="${i}.debug"
	orig="${i}"

	mv "${i}" "${full}" && \
	objcopy --only-keep-debug "${full}" "${syms}" && \
	objcopy --strip-debug --add-gnu-debuglink="${syms}" "${full}" "${orig}"
done
