#!/usr/bin/env zsh
# NAME: sleepuntil --- spin until a time is true.
# SYNPOSIS: sleepuntil <date-spec>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ "X$1" = "X" ] && usage

# Really, really terrible brute force.
until [ $(( $(date +%s) - $(date --date="${1}" +%s) )) -ge 0 ]
do
	sleep 1
done
