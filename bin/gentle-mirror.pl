#!/usr/bin/env perl

=encoding utf8

=head1 NAME

gentle-mirror -- mirror a website

=head1 SYNOPSIS

  gentle-mirror <mirror.db> <url>

=head1 DESCRIPTION

somewhat like

  wget --mirror --no-parent --timestamping --continue --wait=30s

but without the memory loss

=head1 EXAMPLES

=head1 SEE ALSO

=cut

use strict;
use warnings FATAL => qw(uninitialized);
use feature 'say';

use constant VERSION => '0.3';

use Carp;
use Data::Dumper;
use Getopt::Long;
use IO::File;
use IO::Handle;
use Time::HiRes qw{ gettimeofday tv_interval };

use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration::ISO8601;
use Devel::Confess 'color';
use HTTP::CookieJar::LWP;
use LMDB_File;
use LWP::UserAgent;
use Progress::Any;
#	use Progress::Any::Output 'TermMessage';
#	use Progress::Any::Output 'TermSpin';
	use Progress::Any::Output 'TermProgressBarColor';
use URI;

Progress::Any::Output->set(
	'TermProgressBarColor',
	wide => 1,
	freq => -0.1,
	template => (join('', (
		'%9P/%9T (%5.01p%%) %9m (%8r)',
		'<color 00cc00>%b</color>',
	))),
);

# use LWP::ConsoleLogger::Everywhere;
# LWP::ConsoleLogger::Everywhere->set(dump_content => 1);

$|++;

my $DRYRUN      = 0;
my $VERBOSE     = 1;

my $BASEURL;
my $DATABASE;

GetOptions(
	"baseurl=s"         => \$BASEURL,
	"database=s"        => \$DATABASE,
	"dryrun"            => \$DRYRUN,
	"verbose!"          => \$VERBOSE,
) or croak "usage: gentle-mirror [options]\n";

tie our %DB, 'LMDB_File', $DATABASE;

die "bad database $DATABASE: _BASEURL_ mismatch"
	if exists $DB{'_BASEURL_'} and $DB{'_BASEURL_'} ne $BASEURL;
$DB{'_BASEURL_'} = $BASEURL;

sub K_uri_have { shift->canonical."|have" }
sub K_uri_want { shift->canonical."|want" }

sub enqueue {
	my $URI = shift;
	return if $DB{K_uri_have($URI)};
	$DB{K_uri_want($URI)}++;
	$DB{_QSIZE_}++;
}

sub dequeue {
	my $URI = shift;
	$DB{K_uri_have($URI)}++;
}

my $AGENT     = sprintf(
	'gentle-mirror/%s libwww-perl/%s (jashankj@)',
	VERSION,
	$LWP::UserAgent::VERSION,
);

my $LWP  = LWP::UserAgent->new(
	agent               => $AGENT,
	cookie_jar          => HTTP::CookieJar::LWP->new,
	keep_alive          => 64,
	protocols_allowed   => ['https'],
	timeout             => 60,
);
$LWP->env_proxy;

#	use LWP::ConsoleLogger;
#	my $LOGGER = LWP::ConsoleLogger->new(
#		dump_content => 1,
#		dump_cookies => 1,
#		dump_headers => 1,
#		dump_params => 1,
#		dump_status => 1,
#		dump_text => 1,
#		dump_title => 1,
#		dump_uri => 1,
#	);
#	$LWP->add_handler('response_done', sub { $LOGGER->response_callback(@_) });
#	$LWP->add_handler('request_send',  sub { $LOGGER->request_callback(@_) });
#	$LWP->add_handler('response_data', sub { say Dumper \@_; });

sub main {
	enqueue($BASEURL);
	
}

main;

__END__
