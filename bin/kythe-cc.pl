#!/usr/bin/env perl

use strict;
use warnings;

my $KYTHE_ROOT_DIRECTORY = $ENV{'KYTHE_ROOT_DIRECTORY'} || '/opt/kythe';

unless (grep { m{\A-print-} or m{\A-shared\Z} } @ARGV) {
	system $KYTHE_ROOT_DIRECTORY."/extractors/cxx_extractor", @ARGV;
}

exec "clang", @ARGV
