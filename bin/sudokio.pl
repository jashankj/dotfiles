#!/usr/bin/env perl
# -*- mode: perl; buffer-file-coding-system: utf-8-unix -*-
#
# Sudoku "solver". Features:
#   + uses a very big z3-shaped hammer,
#   + mostly avoids open3 deadlocks,
#   + has passable pretty-printing,
#   + has *two* pretty-printer impls.,
#   + actually parses the output model.
#
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#

use strict;
use warnings;

use utf8;
binmode STDOUT, ':utf8';

use Carp;
use Data::Dumper;
use IO::Handle;
use IO::String;
use List::Util 'zip';
use Symbol;

use FindBin;
use lib "$FindBin::Bin/../lib/perl";
use J::Proc;

use Regexp::Common;
use Data::SExpression;

our $VERBOSE_Z3 = 0;
$J::Proc::VERBOSE_IO = $VERBOSE_Z3;

sub make_grid
{
	my $X = [];
	foreach my $i (0..8) { $X->[$i] = []; }
	$X
}

sub is_digit
{
	shift =~ /^[123456789]$/
}

sub read_sudoku_data
{
	my $I = make_grid;
	my @F = ();
	while (<>) { chomp; push @F, split /\s+/; }

	foreach my $i (1..81) {
		my $k = shift @F;
		$k = ' ' unless defined($k) and is_digit($k);
		$I->[($i-1)/9]->[($i-1)%9] = $k;
	}

	$I
}

########################################################################

sub cell_ref
{
	my ($i, $j) = @_; sprintf 'c%d%d', $i, $j
}

sub z3_setup_options
{
	my $z3 = shift;
	$z3->send($_."\n") foreach (
		"(set-option :produce-assignments true)",
		"(set-option :produce-models true)",
		"(set-option :produce-proofs true)",
		"(set-option :produce-unsat-cores true)",
	);
	$z3
}

sub z3_setup_types
{
	my $z3 = shift;
	$z3->send($_."\n") foreach (
		"(declare-datatype Cell ((n1) (n2) (n3) (n4) (n5) (n6) (n7) (n8) (n9)))",
	);
	$z3
}

sub z3_setup_objects
{
	my $z3 = shift;
	foreach my $i (0..8) {
		foreach my $j (0..8) {
			$z3->send(
				sprintf("(declare-const \%s Cell)\n", cell_ref($i, $j))
			)
		}
	}
	$z3
}

sub z3_assert_distinct
{
	my $z3 = shift;
	$z3->send('(assert (distinct '.join(' ', @_).'))'."\n");
	$z3;
}

sub z3_setup_axioms
{
	my $z3 = shift;

	# Distinct rows:
	foreach my $i (0..8) {
		z3_assert_distinct($z3, map { cell_ref $i, $_ } (0..8));
	}

	# Distinct columns:
	foreach my $j (0..8) {
		z3_assert_distinct($z3, map { cell_ref $_, $j } (0..8));
	}

	# Distinct blocks:
	foreach my $n (0..2) {
		foreach my $m (0..2) {
			z3_assert_distinct(
				$z3,
				map {
					cell_ref(
						($n * 3) + ($_ / 3),
						($m * 3) + ($_ % 3)
					)
				} (0..8)
			)
		}
	}

	$z3
}

sub z3_setup
{
	my $z3 = J::Proc->spawn('z3', '-in');
	$z3 = z3_setup_options $z3;
	$z3 = z3_setup_types $z3;
	$z3 = z3_setup_objects $z3;
	$z3 = z3_setup_axioms $z3;
	$z3
}

sub z3_setup_model
{
	my $z3 = shift;
	my $I = shift;
	foreach my $i (0..8) {
		foreach my $j (0..8) {
			my $x = $I->[$i]->[$j];
			next unless is_digit($x);
			$z3->send(
				sprintf(
					"(assert (= c\%d\%d n\%d))\n",
					$i, $j, $x
				)
			);
		}
	}
	$z3
}

sub z3_setup_with_input
{
	my $I = shift;
	my $z3 = z3_setup;
	$z3 = z3_setup_model $z3, $I;
	$z3
}

sub z3_check_sat
{
	my $z3 = shift;
	$z3->send("(check-sat)\n");
	my $sat = $z3->recv;
	chomp $sat;
	($z3, $sat)
}

sub z3_get_model
{
	my $z3 = shift;
	$z3->send("(get-model)\n");
	$z3
}

sub z3_read_model
{
	my $z3 = shift;
	my $O = make_grid;

	my ($i, $j);
	my $o = '';
	$o = $o . $z3->recv
		until $o =~ m[ ^ $RE{balanced}{-parens=>'()'} $ ]xs;

	my $sexp = Data::SExpression->new({ fold_dashes => 1 });
	my $oo = $sexp->read($o);
#	print Dumper $oo;
	foreach (@$oo) {
		next unless *{$_->[0]} eq "*main::define_fun";
		my $ij = substr *{$_->[1]}, -2;   # *main::c67
		($i, $j) = split //, $ij, 2;
		my $n = substr *{$_->[4]}, -1;    # *main::n2
		$O->[$i]->[$j] = $n;
	}

	($z3, $O)
}

sub z3_exit
{
	my $z3 = shift;
	$z3->send("(exit)\n");
}

sub z3_run
{
	my ($o, $O);
	my $I     = shift;
	my $z3    = z3_setup_with_input $I;
	($z3, $o) = z3_check_sat $z3;
	$z3       = z3_get_model $z3;
	($z3, $O) = z3_read_model $z3;

	z3_exit $z3;
	$O
}

########################################################################

our %Q = (
	HH => '━', hh => '─',
	VV => '┃', vv => '│',
	TL => '┏', Tc => '┯', TC => '┳', TR => '┓',
	rL => '┠', rc => '┼', rC => '╂', rR => '┨',
	RL => '┣', Rc => '┿', RC => '╋', RR => '┫',
	BL => '┗', Bc => '┷', BC => '┻', BR => '┛',
);

sub unlines { join "\n", @_ }
sub untoken { map { $Q{$_} } @_ }
sub unruled { join shift, untoken @_ }

sub pp_row_inner {
	my $a = shift;
	my @b = untoken qw{ vv vv VV vv vv VV vv vv };
	join '', map { ($_->[0] || '') . ($_->[1] || '') } zip($a, \@b)
}

sub pp_row_normal_inner
{
	my @a = map { ' '.$_.' ' } @_;
	pp_row_inner \@a
}

sub pp_row_normal
{
	my $i = shift;
	my $O = shift;
	($Q{VV} . pp_row_normal_inner(@{$O->[$i]}) . $Q{VV})
}

sub pp_row_answer_inner
{
	my @a = ();
	foreach (@_) {
		my $is = ($_->[0] || '') eq ($_->[1] || '');
		my $s = $_->[0];
		$s = ' ' . $s . ' ' ;
		$s = sprintf("\033[1;3\%om\%s\033[0m", rgb1(1, 0, 0), $s)
			unless $is;
		push @a, $s;
	}
	pp_row_inner \@a
}

sub pp_row_answer
{
	my $i = shift;
	my $O = shift;
	my $I = shift;
	($Q{VV} . pp_row_answer_inner(zip($O->[$i], $I->[$i])) . $Q{VV})
}

sub pp_rule_header
{
	unruled(($Q{HH} x 3), qw{ TL Tc Tc TC Tc Tc TC Tc Tc TR })
}

sub pp_rule_minor
{
	unruled(($Q{hh} x 3), qw{ rL rc rc rC rc rc rC rc rc rR })
}

sub pp_rule_major
{
	unruled(($Q{HH} x 3), qw{ RL Rc Rc RC Rc Rc RC Rc Rc RR })
}

sub pp_rule_footer
{
	unruled(($Q{HH} x 3), qw{ BL Bc Bc BC Bc Bc BC Bc Bc BR })
}

sub rgbw($$$$)
{
	my ($r, $g, $b, $width) = @_;
	my $max = (1 << $width) - 1;
	die "rgbw(r=$r, g=$g, b=$b): invalid argument"
		unless (0 <= $r and $r <= $max)
			and (0 <= $g and $g <= $max)
			and (0 <= $b and $b <= $max);
	(($r & $max) << (0 * $width)) |
	(($g & $max) << (1 * $width)) |
	(($b & $max) << (2 * $width))
}
sub rgb1($$$) { my ($r, $g, $b) = @_; rgbw $r, $g, $b, 1 }

sub print_pretty_sudoku
{
	my $O = shift;
	my $I = shift;

	my $pp_row = defined $I ? \&pp_row_answer : \&pp_row_normal;

	my $x = unlines (
		pp_rule_header,

		&$pp_row(0, $O, $I),
		pp_rule_minor,
		&$pp_row(1, $O, $I),
		pp_rule_minor,
		&$pp_row(2, $O, $I),

		pp_rule_major,

		&$pp_row(3, $O, $I),
		pp_rule_minor,
		&$pp_row(4, $O, $I),
		pp_rule_minor,
 		&$pp_row(5, $O, $I),

		pp_rule_major,

		&$pp_row(6, $O, $I),
		pp_rule_minor,
		&$pp_row(7, $O, $I),
		pp_rule_minor,
 		&$pp_row(8, $O, $I),

		pp_rule_footer
	);
	printf "\%s\n", $x;
}

sub pretty_format_sudoku
{
	my $O = shift;
	format SUDOKU =
┏━━━┯━━━┯━━━┳━━━┯━━━┯━━━┳━━━┯━━━┯━━━┓
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[0]}
┠───┼───┼───╂───┼───┼───╂───┼───┼───┨
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[1]}
┠───┼───┼───╂───┼───┼───╂───┼───┼───┨
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[2]}
┣━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┫
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[3]}
┠───┼───┼───╂───┼───┼───╂───┼───┼───┨
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[4]}
┠───┼───┼───╂───┼───┼───╂───┼───┼───┨
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[5]}
┣━━━┿━━━┿━━━╋━━━┿━━━┿━━━╋━━━┿━━━┿━━━┫
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[6]}
┠───┼───┼───╂───┼───┼───╂───┼───┼───┨
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[7]}
┠───┼───┼───╂───┼───┼───╂───┼───┼───┨
┃@||│@||│@||┃@||│@||│@||┃@||│@||│@||┃
@{$O->[8]}
┗━━━┷━━━┷━━━┻━━━┷━━━┷━━━┻━━━┷━━━┷━━━┛
.
	$~ = 'SUDOKU';
	write;
}

########################################################################

my $i = read_sudoku_data;
my $o = z3_run $i;
print_pretty_sudoku $o, $i;

# 8 6 5 2 4 9 1 3 7 9 1 3 7 5 6 4 8 2 4 2 7 3 1 8 6 9 5 3 5 2 9 7 4 8 6 1 7 4 1 6 8 2 9 5 3 6 8 9 5 3 1 2 7 4 1 7 8 4 6 3 5 2 9 5 9 4 8 2 7 3 1 6 2 3 6 1 9 5 4 7 8
# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


__DATA__;

;;	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
;;	;;   ⁰ ¹ ²   ³ ⁴ ⁵   ⁶ ⁷ ⁸
;;	;; ⁰ . . . | . 1 . | . . .
;;	;; ¹ . 3 4 | 2 . . | . . .
;;	;; ² . . . | 7 5 . | . 6 .
;;	;;   ------+-------+------
;;	;; ³ 5 1 . | . . 8 | . 3 .
;;	;; ⁴ . 6 . | . . . | 7 1 .
;;	;; ⁵ . . 9 | . . . | . . .
;;	;;   ------+-------+------
;;	;; ⁶ 9 . . | 4 . 6 | 3 . .
;;	;; ⁷ . . 1 | . 3 . | . . .
;;	;; ⁸ . 2 . | . . . | 9 5 .
;;	(assert
;;	 (and
;;	  (= c04 n1)
;;	  (= c11 n3) (= c12 n4) (= c13 n2)
;;	  (= c23 n7) (= c24 n5) (= c27 n6)
;;	  (= c30 n5) (= c31 n1) (= c35 n8) (= c37 n3)
;;	  (= c41 n6) (= c46 n7) (= c47 n1)
;;	  (= c52 n9)
;;	  (= c60 n9) (= c63 n4) (= c65 n6) (= c66 n3)
;;	  (= c72 n1) (= c74 n3)
;;	  (= c81 n2) (= c86 n9) (= c87 n5)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

. . . . 1 . . . .
. 3 4 2 . . . . .
. . . 7 5 . . 6 .
5 1 . . . 8 . 3 .
. 6 . . . . 7 1 .
. . 9 . . . . . .
9 . . 4 . 6 3 . .
. . 1 . 3 . . . .
. 2 . . . . 9 5 .

