#!/usr/bin/env zsh
# NAME: iemacs --- start my Isabelle/Emacs session
# SYNOPSIS: iemacs

exec with-emacs-session isabelle "$@"
