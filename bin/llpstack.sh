#!/bin/sh
# NAME: llpstack --- print a process' stack using LLDB

exec lldb \
	--one-line 'thread backtrace --extended true all' \
	--one-line 'exit' \
	"$@"
