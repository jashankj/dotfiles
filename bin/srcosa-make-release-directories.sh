#!/bin/sh
#
# srcosa-make-release-directories:
#   given a release table, and a set of release files acquired by
#   `srcosa-get-release-info`, generate directories for each release,
#   and link in the release index page as ``RELEASE.html``, as would be
#   useful to `srcosa-fetch-release-tarballs`.

cat index.html \
| sed -Ene '
	/release\// {
		s/^.*\"\/release\/(.*).html\">(.*)<\/.*/\1:\2/;
		s/^((mac-os-x-|os-x|macos).*):/&macOS-/;
		s/^((ios|iphone)-.*):/&iOS-/;
		s/^(developer-tools-.*):/&SDK-/;
		p;
	}' \
| while IFS=: read rel v
do
	mkdir -p ../${v}
	ln -v ${rel}.html ../${v}/RELEASE.html
done
