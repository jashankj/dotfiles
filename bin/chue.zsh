#!/usr/bin/env zsh
# NAME: chue --- cycle v4l hue control
# SYNOPSIS: chue

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"

for i in $(seq 1 180) $(seq -180 1)
do
	v4l2-ctl --set-ctrl hue=$i "$@"
	sleep 0.01
done
