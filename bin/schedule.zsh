#!/usr/bin/env zsh
# NAME: schedule --- print my schedule
# SYNOPSIS: schedule

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

( toggl ls | head -n15
; echo
; yes '-' | head -n164 | xargs | tr -d ' '
; echo
; todoist --header --namespace list --filter 'overdue | today'
)
