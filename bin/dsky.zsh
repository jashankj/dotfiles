#!/usr/bin/env zsh
# NAME: dsky --- display keys
# SYNOPSIS: dsky
#
# Runs 'screenkey' with some nice flags to show what's going on.

flags=(
	# Show modifiers as I would expect (C-x, C-S-c, M-x):
	--mods-mode=emacs
	--vis-shift

	# Fonts.
	#
	# Font size is calculated proportional to both ``font-size`` and
	# determined window geometry; for the exact mechanism, see both
	#  + :py:meth:`Screenkey.update_font`,
	#  + :py:meth:`Screenkey.update_geometry`.
	#
	--font='source code pro'
	--font-size=small

	# Get rid of the window after a reasonable delay.
	--timeout=1.1 # 0.75
)

# Place the window somewhere useful.

typeset -A geometries
geometries=(
)

hostenv="$(hostenv)"
if [ ! -z "${geometries[${hostenv}]}" ]
then
	flags+=(--geometry="${geometries[${hostenv}]}")
else
	flags+=(--position=bottom) # top
fi

exec screenkey "${flags[@]}"
