#!/bin/sh

set -e
prefix="$HOME"

etar="$(ls -r "${prefix}/src/gnu/emacs/"emacs-*.tar.zst | head -n1)"
echo "# extracting $(basename "${etar}")..."

tar -C "${prefix}" \
	--extract \
	--file "${etar}" \
	--acls --xattrs --selinux \
	--keep-directory-symlink

mtree -e -C -f "${prefix}/etc/emacs.manifest" \
| awk '$0 !~ / type=dir/ { print $1 }' \
| sed -e 's@^\./@/@' \
> ~/.gitignore.d/local-emacs

emacs --batch --eval "(message (emacs-version))"
