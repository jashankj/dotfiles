#!/bin/sh
# NAME: timestamped --- add a timestamp to streaming messages.
# SYNOPSIS: timestamped

exec stdbuf -oL gawk '{print strftime("%F %T", systime()), "\t", $0 }'
