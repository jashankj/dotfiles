#!/bin/sh
# NAME: adsearch --- do a quick search for a particular CN in AD.
# SYNOPSIS: adsearch <cn>
#
# SEE ALSO: ``ldapsearch-ad``.

exec ldapsearch-ad "(cn=$1)"
