#!/bin/sh

gt=xfce4-terminal

########################################################################
lh_19t1() {
	W=/src/local/www/cse.jashankj.space
	S=/src/local/CS
	i3-msg split h
	$gt	--working-directory="$W"

	$gt	--working-directory="$W/19t1/tCOMP2521"
	i3-msg focus left && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg focus right && i3-msg split v && i3-msg layout tabbed
	i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="$S/19t1/t2521"

	$gt	--working-directory="$W/19t1/ARTS1690"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="$W/19t1/ARTS1690"

	$gt	--working-directory="$W/19t1/COMP3331"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="$S/19t1/3331"
}
lh_19t1
exit


########################################################################
lh_18s1() {
	W=/src/local/www/cse.jashankj.space
	S=/src/local/CS
	i3-msg split h
	$gt	--working-directory="$W"

	$gt	--working-directory="$W/18s1/COMP3141"

	#read

	i3-msg focus left && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg focus right && i3-msg split v && i3-msg layout tabbed
	i3-msg split v && i3-msg layout stacking

	#read

	$gt	--working-directory="$S/18s1/3141"

	$gt	--working-directory="$W/18s1/tCOMP1521"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="$S/18s1/t1521"

	$gt	--working-directory="$W/18s1/COMP3311"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="$S/18s1/3311"

	$gt	--working-directory="$W/18s1/COMP9243"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="$S/18s1/9243"

	$gt	--working-directory="$W/18s1"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="$S/18s1/t1511"
	$gt	--working-directory="$S/18s1/hs"
}
lh_18s1
exit


########################################################################
lh_17s2() {
	i3-msg split h
	$gt	--working-directory="$HOME/LH/UNSW"

	$gt	--working-directory="$HOME/LH/UNSW/17s2/tCOMP1511"

	#read

	i3-msg focus left && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg focus right && i3-msg split v && i3-msg layout tabbed
	i3-msg split v && i3-msg layout stacking

	#read

	$gt	--working-directory="/src/CS/17s2/t1511"

#	 $gt	--working-directory="$HOME/LH/UNSW/17s2/tCOMP3421"
#	 i3-msg move right && i3-msg split v && i3-msg layout stacking
#	 $gt	--working-directory="/src/CS/17s2/t3421"

	$gt	--working-directory="$HOME/LH/UNSW/17s2/COMP9242"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt	--working-directory="/src/CS/17s2/9242"
}
lh_17s2
exit

########################################################################
lh_17s1() {
	i3-msg split h
	$gt	--working-directory="$HOME/LH/UNSW"

	$gt	--working-directory="$HOME/LH/UNSW/17s1/tCOMP1911"

	#read

	i3-msg focus left && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg resize shrink width 10 px or 10 ppt && \
		i3-msg focus right && i3-msg split v && i3-msg layout tabbed
	i3-msg split v && i3-msg layout stacking

	#read

	$gt	--working-directory="/src/CS/17s1/t1911"

	$gt	--working-directory="$HOME/LH/UNSW/17s1/COMP3891"
	$gt	--working-directory="/src/CS/17s1/3891"
	i3-msg move right && i3-msg split v && i3-msg layout stacking

	$gt	--working-directory="$HOME/LH/UNSW/17s1/COMP6841"
	$gt	--working-directory="/src/CS/17s1/6841"
	i3-msg move right && i3-msg split v && i3-msg layout stacking

	$gt	--working-directory="$HOME/LH/UNSW/17s1/COMP2911"
	$gt	--working-directory="/src/CS/17s1/2911"
	$gt	--working-directory="$HOME/LH/UNSW/17s1/pCOMP2911"
	$gt	--working-directory="/src/CS/17s1/p2911"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
}
lh_17s1
exit
