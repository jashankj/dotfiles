#!/bin/sh
# NAME: ruhttpd --- serve the current directory via HTTP
# SYNOPSIS: ruhttpd [port]
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>

exec \
ruby -run -e httpd -- \
	--port="${1-10000}"
