#!/bin/sh

exec \
env \
	LD_PRELOAD="$(pkg-config --variable=libdir jemalloc)/libjemalloc.so.2" \
"$@"
