#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: false

# map is a Map-backed radix tree of twelve-bit cadence
map = {}
max = 0x0

def split_addr addr
  [
    (addr >> 60) & 0xfff,
    (addr >> 48) & 0xfff,
    (addr >> 36) & 0xfff,
    (addr >> 24) & 0xfff,
    (addr >> 12) & 0xfff,
    (addr)       & 0xfff,
  ]
end

go = false
ARGF.each_line do |line|
  if line =~ /^# +pos/ then go = true; next; end
  next unless go

  pos, size, status = line.split(/\s+/, 3)
  pos = pos.to_i
  size = size.to_i
  max = [max, pos + size].max

  lo_addr = pos
  hi_addr = pos + size - 1

  # base radix
  br0 = (pos >> 40) & 0xff
  br1 = (pos >> 32) & 0xff
  br2 = (pos >> 24) & 0xff
  br3 = (pos >> 16) & 0xff
  br4 = (pos >>  8) & 0xff
  br5 = (pos)       & 0xff

  # Fill out the tree.
  map[r0]                     ||= {}
  map[r0][r1]                 ||= {}
  map[r0][r1][r2]             ||= {}
  map[r0][r1][r2][r3]         ||= {}
  map[r0][r1][r2][r3][r4]     ||= {}
  map[r0][r1][r2][r3][r4][r5]
end

__END__
# Mapfile. Created by GNU ddrescue version 1.26
# Command line: ddrescue --idirect /dev/sdc3 drive.img drive.log
# Start time:   2022-06-02 19:50:39
# Current time: 2022-06-03 15:37:08
# Copying non-tried blocks... Pass 1 (forwards)
# current_pos  current_status  current_pass
0x21C4D0000     ?               1
#      pos        size  status
0x00000000  0x00380000  +
0x00380000  0x00010000  *
0x00390000  0x00280000  ?

39 70 C2 00 00
