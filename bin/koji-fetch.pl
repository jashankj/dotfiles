#!/usr/bin/env perl

=encoding utf8

=head1 NAME

koji-fetch -- Download a package from Koji

=head1 SYNOPSIS

  koji-fetch <options>

=head1 DESCRIPTION

C<koji-fetch> searches and retrieves
build artefacts from the Koji build service.
Koji is used by Fedora Linux,
and some other Linux distributions,
as the orchestration hub for package building.

L<< C<koji> | https://pagure.io/koji/ >>
is the official command-line interface;
L<< C<koji-tool> | https://github.com/juhp/koji-tool >>
is a nice alternative command-line interface.
Neither were quite appropriate for my needs.

C<koji-fetch> will either search for
a specific name, version, and release provided,
or for any recent builds of the named package.
It will then retrieve them into a directory hierarchy:

	⟨name⟩/
	  ⟨version⟩-⟨release⟩.⟨osrelease⟩/
	    *.rpm


=head1 OPTIONS

=over 4

=item C<--name=NAME>

The name of the package to retrieve.

Default C<kernel>
(appropriate for my no-rawhide-kernels policy).

=item C<--version=VERSION>

=item C<--release=RELEASE>

Specify both C<--version> and C<--release>
to search for a package's build
at a specific version and release;
otherwise, searches for recent builds.

=item C<--since=DURATION>

Bounds the earliest time to search for
an acceptable build of the specified package,
as an S<ISO 8601> duration.

Default C<P1W>, which looks back 1 week.

=item C<--osrelease=OSRELEASE>

The OS release to retrieve.

Default C<fc38>
(appropriate for my no-rawhide-kernels policy).

=item C<--arch=ARCH>

The RPM architecture to retrieve.
Note that C<noarch> artefacts are always retrieved.

Default C<x86_64>.

=item C<--dryrun>

Print URLs, but don't download anything.

=item C<--koji-rpc-url=URL>

The Koji XML-RPC API endpoint.

Default C<https://koji.fedoraproject.org/kojihub>
(appropriate for the Fedora Project's Koji instance).

=item C<--kojipkgs-url=URL>

The URL of the place where Koji artifacts may be found.

Default C<https://kojipkgs.fedoraproject.org>
(appropriate for the Fedora Project's Koji instance).

=back

=head1 EXAMPLES

To see a list of URLs of RPMs for a current kernel:

	% koji-fetch --dryrun
	[Koji alive.]
	['kernel' => packageID:8]
	[searching builds between @1678501085 .. @1679105885]
		kernel-6.2.5-300.fc38
		kernel-6.2.6-300.fc38
		kernel-6.2.7-300.fc38
	[buildID:2171722 => 'kernel-6.2.7-300.fc38']
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-core-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-devel-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-devel-matched-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/noarch/kernel-doc-6.2.7-300.fc38.noarch.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-modules-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-modules-core-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-modules-extra-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-modules-internal-6.2.7-300.fc38.x86_64.rpm
	https://kojipkgs.fedoraproject.org/packages/kernel/6.2.7/300.fc38/x86_64/kernel-uki-virt-6.2.7-300.fc38.x86_64.rpm

=head1 SEE ALSO

L<DateTime::Format::Duration::ISO8601>
L<LWP::UserAgent>
L<RPC::XML>

L<https://pagure.io/koji/>
L<https://github.com/juhp/koji-tool>

=cut

use strict;
use warnings FATAL => qw(uninitialized);
use feature 'say';

use constant VERSION => '0.3';

use Carp;
use Data::Dumper;
use Getopt::Long;
use IO::File;
use IO::Handle;
use Time::HiRes qw{ gettimeofday tv_interval };

use DateTime;
use DateTime::Duration;
use DateTime::Format::Duration::ISO8601;
use Devel::Confess 'color';
use HTTP::CookieJar::LWP;
use LWP::UserAgent;
use Progress::Any;
#	use Progress::Any::Output 'TermMessage';
#	use Progress::Any::Output 'TermSpin';
	use Progress::Any::Output 'TermProgressBarColor';
use RPC::XML::Client;
use RPC::XML;

Progress::Any::Output->set(
	'TermProgressBarColor',
	wide => 1,
	freq => -0.1,
	template => (join('', (
		'%9P/%9T (%5.01p%%) %9m (%8r)',
		'<color 00cc00>%b</color>',
	))),
);

# use LWP::ConsoleLogger::Everywhere;
# LWP::ConsoleLogger::Everywhere->set(dump_content => 1);

$|++;

my $KOJI_RPC_URL = 'https://koji.fedoraproject.org/kojihub';
my $KOJIPKGS_URL = 'https://kojipkgs.fedoraproject.org';

my $NAME         = 'kernel';
my $VERSION;
my $RELEASE;
my $OSRELEASE    = 'fc39';
my $ARCH         = 'x86_64';
my $SINCE        = 'P1W';
my $DRYRUN       = 0;
my $VERBOSE      = 1;

GetOptions(
	"koji-rpc-url=s"    => \$KOJI_RPC_URL,
	"kojipkgs-url=s"    => \$KOJIPKGS_URL,
	"name=s"            => \$NAME,
	"version=s"         => \$VERSION,
	"release=s"         => \$RELEASE,
	"osrelease=s"       => \$OSRELEASE,
	"arch=s"            => \$ARCH,
	"since=s"           => \$SINCE,
	"dryrun"            => \$DRYRUN,
	"verbose!"          => \$VERBOSE,
) or croak "usage: koji-fetch-kernel [options]\n";

my $AGENT     = sprintf(
	'koji-fetch/%s RPC::XML::Client/%s libwww-perl/%s (u/jashankj)',
	VERSION,
	$RPC::XML::Client::VERSION,
	$LWP::UserAgent::VERSION,
);

my $Koji = RPC::XML::Client->new($KOJI_RPC_URL);
my $LWP  = $Koji->useragent;
$LWP->agent(
	$AGENT,
	cookie_jar          => HTTP::CookieJar::LWP->new,
	keep_alive          => 64,
	protocols_allowed   => ['https'],
	timeout             => 60,
);
$LWP->env_proxy;

#	use LWP::ConsoleLogger;
#	my $LOGGER = LWP::ConsoleLogger->new(
#		dump_content => 1,
#		dump_cookies => 1,
#		dump_headers => 1,
#		dump_params => 1,
#		dump_status => 1,
#		dump_text => 1,
#		dump_title => 1,
#		dump_uri => 1,
#	);
#	$LWP->add_handler('response_done', sub { $LOGGER->response_callback(@_) });
#	$LWP->add_handler('request_send',  sub { $LOGGER->request_callback(@_) });
#	$LWP->add_handler('response_data', sub { say Dumper \@_; });


sub get_api_version;
sub get_package_id;
sub list_builds;
sub get_build;
sub list_rpms;
sub rpm_name;
sub rpm_url;
sub calc_rpm_files;
sub download_one;

sub nvr_of {
	my ($n, $v, $r) = @_;
	sprintf '%s-%s-%s', $n, $v, $r
}

sub nvr_of_build {
	my $build = shift;
	nvr_of $build->{'name'}, $build->{'version'}, $build->{'release'}
}

sub main {
	my $apivers = get_api_version;
	croak "bad Koji apivers: ".$apivers->as_string
	  unless $apivers->type eq 'int' and $apivers->value eq 1;
	say '[Koji alive.]' if $VERBOSE;

	my $package_id  = get_package_id $NAME;
	say sprintf "['\%s' => packageID:\%d]", $NAME, $package_id if $VERBOSE;

	my $build;
	my $build_id;
	if (defined $VERSION and defined $RELEASE) {
		my $nvr = nvr_of $NAME, $VERSION, $RELEASE;
		$build       = get_build $NAME, $VERSION, $RELEASE;
		$build_id    = $build->{'build_id'};
		say sprintf "['\%s' => buildID:\%d]", $nvr, $build_id if $VERBOSE;

	} else {
		my $now   = DateTime->now();
		my $since = DateTime::Format::Duration::ISO8601->parse_duration($SINCE);
		my $newerthan = $now->clone()->subtract_duration($since);
		say sprintf "[searching builds between \@\%d .. \@\%d]",
			$newerthan->epoch, $now->epoch if $VERBOSE;
		my $builds = list_builds $package_id, "*.$OSRELEASE", $newerthan->epoch * 1.0;

		croak "no matching builds found, aborting." unless @$builds;
		if ($VERBOSE) {
			say sprintf("\t\%s", nvr_of_build($_)) foreach (@$builds);
		}

		   $build       = $builds->[scalar @$builds - 1];
		   $VERSION     = $build->{'version'};
		   $RELEASE     = $build->{'release'};
		my $nvr         = nvr_of $NAME, $VERSION, $RELEASE;
		   $build_id    = $build->{'build_id'};
		say sprintf "[buildID:\%d => '\%s']", $build_id, $nvr if $VERBOSE;
	}

	my $rpms    = list_rpms $build_id;
	my $files   = calc_rpm_files $build, $rpms;

	unless (-d $NAME) { mkdir $NAME or carp "mkdir $NAME: $!"; }
	my $dir = sprintf '%s/%s-%s', $NAME, $VERSION, $RELEASE;
	unless (-d $dir)  { mkdir $dir  or carp "mkdir $dir: $!"; }

	my @files = sort keys %$files;

	my $progress = {};
	$progress->{$_} = Progress::Any->get_indicator(
		task  => 'download',
		title => $_
	)
		foreach @files;

	foreach my $file (@files) {
		my $out  = sprintf '%s/%s', $dir, $file;
		my $info = $files->{$file};
#		printf "$file\n";

		printf "\%s\n", $info->{url} if $DRYRUN;
		next if $DRYRUN;

		my $prog = $progress->{$file};
		$prog->target($info->{size});

		download_one $prog, $file, $out, $info->{url}
			unless -e $out && (-s $out) == $info->{size};

		$prog->update(pos => $info->{size});
		$prog->finish;
		say '';

		system('rpm', '-qp', $out) == 0 or die "verification failed";
	}
}

sub get_api_version {
	my $res = $Koji->send_request('getAPIVersion');
	croak "error: $res" unless ref $res;
	return $res;
}

sub get_package_id {
	my $name = shift;
	my $res = $Koji->send_request('getPackageID', $name);
	croak "error: $res" unless ref $res;
	return $res->value;
}

sub list_builds {
	my $package_id      = shift;
	my $pattern         = shift;
	my $complete_after  = shift;

	my $res = $Koji->send_request(
		'listBuilds',
		{
			packageID     => $package_id,
			pattern       => $pattern,
			completeAfter => RPC::XML::double->new($complete_after),
			__starstar    => RPC::XML::boolean->new(1),
		}
	);

	croak "error: $res" unless ref $res;
	return [
		sort { $a->{completion_ts} <=> $b->{completion_ts} }
		@{$res->value}
	]
}

sub get_build {
	my ($name, $version, $release) = @_;
	my $nvr = nvr_of $name, $version, $release;
	my $res = $Koji->send_request('getBuild', $nvr);
	croak "error: $res" unless ref $res;
	return $res->value;
}

sub list_rpms {
	my $build_id = shift;
	my $res = $Koji->send_request('listRPMs', $build_id);
	croak "error: $res" unless ref $res;
	return $res->value;
}

sub rpm_name {
	my $blob = shift;
	return sprintf(
		'%s.%s.rpm',
		nvr_of($blob->{name}, $blob->{version}, $blob->{release}),
		$blob->{arch}
	);
}

sub pathinfo_build {
	my $blob = shift;
	# via koji.PathInfo#build
	return sprintf(
		'packages/%s/%s/%s',
		$blob->{name},
		$blob->{version},
		$blob->{release},
	);
}

sub pathinfo_rpm {
	my $blob = shift;
	# via koji.PathInfo#rpm
	return sprintf(
		'%s/%s.%s.rpm',
		$blob->{arch},
		nvr_of($blob->{name}, $blob->{version}, $blob->{release}),
		$blob->{arch}
	);
}

sub calc_rpm_files {
	my $build = shift;
	my $rpms  = shift;
	my $files = {};

	foreach my $rpmdata (@$rpms) {
		my $rpm = rpm_name $rpmdata;
		my $url = sprintf(
			'%s/%s/%s',
			$KOJIPKGS_URL,
			pathinfo_build($build),
			pathinfo_rpm($rpmdata)
		);

		my $rpm_rel = $rpmdata->{release};
		next unless $rpm_rel =~ m/\.${OSRELEASE}\Z/;

		my $rpm_arch = $rpmdata->{arch};
		next unless $rpm_arch eq $ARCH or $rpm_arch eq 'noarch';

		my $rpm_name = $rpmdata->{name};
		next if $rpm_name =~ m/-debug/;

		$files->{$rpm} = {
			url  => $url,
			size => $rpmdata->{'size'},
			hash => $rpmdata->{'payloadhash'},
		};
	}
	return $files;
}

sub download_one {
	my $prog = shift;
	my $file = shift;
	my $out  = shift;
	my $url  = shift;

	my $outf = IO::File->new($out, 'w');
	unless (defined $outf) { carp "open: $out: $!"; next; }
	$prog->start;

	my $t0 = [gettimeofday];
	$LWP->get(
		$url,
		':content_cb' => sub {
			my ($data, $ua, $proto) = @_;

			$outf->write($data);

			my $have = $outf->tell * 1.0;
			my $time = tv_interval($t0, [gettimeofday]);
			my $rate = ($have / 1024.0) / $time;
			$prog->update(
				pos     => $have,
				message => sprintf('%4d kB/s', $rate),
			);
		}
	);
}

main;

__END__

2022-07-01 13:00:33,799 [DEBUG] koji: data: "
<?xml version='1.0'?>
<methodCall>
<methodName>listBuilds</methodName>
<params>
<param>
<value><struct>
<member>
<name>pattern</name>
<value><string>*.fc37</string></value>
</member>
<member>
<name>packageID</name>
<value><int>813</int></value>
</member>
<member>
<name>completeAfter</name>
<value><double>1655647200.0</double></value>
</member>
<member>
<name>__starstar</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
</param>
</params>
</methodCall>
"


__END__

2022-05-24 12:11:49,910 [DEBUG] koji: data: "
<?xml version='1.0'?>
<methodCall>
<methodName>listBuilds</methodName>
<params>
<param>
<value><struct>
<member>
<name>pattern</name>
<value><string>*.fc37</string></value>
</member>
<member>
<name>packageID</name>
<value><int>8</int></value>
</member>
<member>
<name>completeAfter</name>
<value><double>1652973825.0</double></value>
</member>
<member>
<name>__starstar</name>
<value><boolean>1</boolean></value>
</member>
</struct></value>
</param>
</params>
</methodCall>

"


	$koji list-builds \
		--package=kernel \
		--after='2022-05-11 23:01:51' \
		--pattern="*.${r}" \
	| $awk '
		{ nvr = $1; };
		END { print nvr; }
	' \
	| xargs \
	  $koji buildinfo \


__END__

#			'%s/%s/%s/%s/%s/%s/%s',
#			$kojipkgs_url, $F[3], $F[4], $F[5], $F[6], $F[7], $F[8]

sprintf(
	'%s/packages/%s/%s/%s/%s/%s.%s.rpm'
	$kojipkgs_url, $name, $version, $release, $arch, $nvr, $arch
);

            {
              arch     => 'src',
              build_id => 1969515,
              buildroot_id
                       => 35608605,
              buildtime
                       => 1653313855,
              epoch    => undef,
              external_repo_id
                       => 0,
              external_repo_name
                       => 'INTERNAL',
              extra    => undef,
              id       => 30457157,
              metadata_only
                       => 0,
              name     => 'kernel',
              nvr      => 'kernel-5.18.0-60.fc37',
              payloadhash
                       => 'ab22d64e43b8e7ef2a07462e5ca71bb6',
              release  => '60.fc37',
              size     => 133205256,
              version  => '5.18.0'
            },


__END__

#	while (<>) {
#		tr/\t/\//;
#		my @F = split '/';
#		next
#		unless $F[0] eq ""
#		   and $F[1] eq "mnt"
#		   and $F[2] eq "koji"
#		   and $F[3] eq "packages"
#		   and $F[4] eq $name
#		   and $F[6] =~ m/\.${osrelease}\Z/
#		   and ($F[7] eq $arch or $F[7] eq 'noarch')
#		   and (not $F[8] =~ m/-debug/);
#		say sprintf(
#			'%s/%s/%s/%s/%s/%s/%s',
#			$kojipkgs_url, $F[3], $F[4], $F[5], $F[6], $F[7], $F[8]
#		);
#	}

__END__

	&& ($4 == "packages") \
	&& ($5 == n) \
	&& ($7 ~ ("\\." r)) \
	&& (($8 == a) || ($8 == "noarch")) \
	&& ($9 !~ "-debug") \
	) {
	 	printf( \
			"%s/%s/%s/%s/%s/%s/%s\n", \
			kojiurl, $4, $5, $6, $7, $8, $9 \
		);
	}

__END__

	$koji list-builds \
		--package=kernel \
		--after='2022-05-11 23:01:51' \
		--pattern="*.${r}" \
	| $awk '
		{ nvr = $1; };
		END { print nvr; }
	' \
	| xargs \
	  $koji buildinfo \

__END__

	$awk \
		--field-separator '/' \
		--assign kojiurl="https://kojipkgs.fedoraproject.org" \
		--assign n="${n}" \
		--assign r="${r}" \
		--assign a="${a}" \
		'
		(  ($1 == "") \
		&& ($2 == "mnt") \
		&& ($3 == "koji") \
		&& ($4 == "packages") \
		&& ($5 == n) \
		&& ($7 ~ ("\\." r)) \
		&& (($8 == a) || ($8 == "noarch")) \
		&& ($9 !~ "-debug") \
		) {
			printf( \
				"%s/%s/%s/%s/%s/%s/%s\n", \
				kojiurl, $4, $5, $6, $7, $8, $9 \
			);
		}
	'

__END__

# 1	/
# 2	mnt/
# 3	koji/
# 4	packages/
# 5	kernel/
# 6	5.18.0/
# 7	60.fc37/
# 8	x86_64/
# 9	kernel-modules-internal-5.18.0-60.fc37.x86_64.rpm/
# 10	Signatures: 5323552a
#
#	($1 ~ ("\\." r "\\." a)) && ($1 !~ "debug") { print $1 }
# | sed 's@^/mnt/koji/@https://kojipkgs.fedoraproject.org/@'
