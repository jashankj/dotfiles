#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: false

require 'date'
require 'erb'
require 'json'
require 'yaml'
require 'shellwords'

require 'rubygems'
require 'cicphash'
require 'inifile'
require 'subprocess'

########################################################################
#### CICPHash monkeypatch:

class CICPHash
  class << self
    def from_hash(input)
      result = CICPHash.new
      input.each { |k, v| result[k] = v }
      result
    end
  end
end

########################################################################
#### Subprocess shims:

def explain(cmd)
  $stderr.puts "+ #{Shellwords.join(cmd)}"
end

def subprocess_run(cmd, opts = {}, &blk)
  opts[:stdout] = Subprocess::PIPE
  child = Subprocess::Process.new(cmd, opts, &blk)
  output, _ = child.communicate()
  child.wait
  output
end

def system(cmd)
  begin
    explain(cmd)
    o = Subprocess.check_output(cmd)
  rescue Subprocess::NonZeroExit => e
    [o, e.status]
  end
  o
end

def lines(out)
  out.lines.map(&:chomp)
end

def system_lines(cmd)
  lines(system(cmd))
end

def as_root(cmd)
  ['sudo'] + cmd
end

########################################################################
#### Filenames and filepaths.

def prefix_for_disk(serial)
  "#{serial}."
end

def prefix_for_disk_part(serial, device)
  "#{prefix_for_disk(serial)}#{File.basename(device)}."
end

def lsblk_file_for(serial)
  "#{prefix_for_disk(serial)}lsblk.data"
end

def smartctl_file_for(serial)
  "#{prefix_for_disk(serial)}smartctl.json"
end

def partition_file_for(serial)
  "#{prefix_for_disk(serial)}partitions.yaml"
end

########################################################################
#### Invoke `smartctl':

def smartctl_invocation(device)
  as_root(['smartctl', '--xall', '--json', device])
end

def run_smartctl(device)
  invocation = smartctl_invocation(device)
  explain smartctl_invocation(device)
  subprocess_run invocation
end

def load_smartctl_output(datum)
  JSON.parse datum
end

########################################################################
#### Invoke `lsblk':

def lsblk_invocation(device)
  as_root(['lsblk', '--pairs', '--shell', '--output-all', '--bytes', device])
end

def run_lsblk(device)
  invocation = lsblk_invocation(device)
  system invocation
end

def load_lsblk_one(line)
  Shellwords
    .split(line)
    .map { |w| w.split('=', 2) }
    .map { |xs| [(xs[0] || '').downcase.to_sym, xs[1]] }
    .to_h
end

def load_lsblk_output(output)
  lines(output).map { |x| load_lsblk_one(x) }
end

########################################################################
#### Shim for `ntfsls':

def ntfsls(
      device,
      all: false,
      classify: false,
      dos: false,
      inode: false,
      long: false,
      path: nil,
      recursive: false,
      system: false,
      verbose: false
    )
  ['ntfsls'] +
    (all ? ['--all'] : []) +
    (classify ? ['--classify'] : []) +
    (dos ? ['--dos'] : []) +
    (inode ? ['--inode'] : []) +
    (long ? ['--long'] : []) +
    (path ? ['--path', path] : []) +
    (recursive ? ['--recursive'] : []) +
    (system ? ['--system'] : []) +
    (verbose ? ['--verbose'] : []) +
    [device]
end

########################################################################
#### Interact with the Windows registry:

##### Find registry hives:
def walk_to_registry_path(dev)
  look = lambda { |device, path, pattern|
    system_lines(as_root(ntfsls(device, all: true, path: path)))
      .grep(pattern) }

  path = ['']
  [/\Awindows\Z/i, /\Asystem32\Z/i, /\Aconfig\Z/i].each do |pattern|
    seen = look.call(dev, File.join('', *path), pattern)
    extend, = seen
    path.push extend
  end

  File.join(path)
end

def resulting_hive_file(serial, device, hive)
  "#{prefix_for_disk_part(serial, device)}#{hive}"
end

##### Retrieve registry hives:
def fetch_registry_hive(serial, device, hive_path, hive)
  cmd = as_root(['ntfscat', device, File.join(hive_path, hive)])
  explain(cmd)
  outf = resulting_hive_file(serial, device, hive)
  Subprocess.check_call(cmd, stdout: outf)
  outf
end

##### Load registry hive `HIVE' at specific node `PATH';
# returns a Hash of of the values at that specific node
def load_from_registry_hivexget(hive, path)
  system_lines(['hivexget', hive, path])
    .map { |x| x.split('=', 2) }
    .map { |x| [ Shellwords.split(x[0])[0],
                 Shellwords.split(x[1])[0] ] }
    .sort
end

def hivexregedit_read_value(v)
  case v
  when /hex\([13]\):([0-9a-f,]*)$/ then
    $1
      .split(/,/)
      .map { |x| x.to_i(16) }
      .map(&:chr)
      .join
  else
    v
  end
end

##### Load registry hive `HIVE' recursively at path `PATH';
# returns a Hash of node-name to Hash of value-name to value.
# _VERY_ slow and inefficient!
def load_from_registry_hivexregedit(hive, path)
  result = {}
  this_node = nil
  system_lines(['hivexregedit', '--export', hive, path])
    .drop(2)
    .each do |l|
    case l
    when /^\[(.*)\]$/ then # section
      this_node = $1.gsub("\\", '/')
      result[this_node] ||= {}
    when /^"([^"]+)"=(.*)$/, /^([^=]+)=(.*)$/ then
      result[this_node][$1] = hivexregedit_read_value $2
    end
  end
  result
end

########################################################################
#### A class for reasoning about Windows' SIDs

# derived from <https://renenyffenegger.ch/notes/Windows/security/SID/index>
#
# Explode a SID in human-readable form
# e.g., "S-1-5-21-489056535-1467421822-2524099697-1231"
#
#   489056535-1467421822-2524099697– this is the unique identifier of the domain that issued the SID (this part will be the same for all objects in the same domain):
#   1231 – the object’s relative security identifier (RID). It starts at 1000 and increases by 1 for each new object. Issued by a domain controller with FSMO role RID Master.
class SID
  AUTHORITIES = {
    NULL_SID: 0,
    WORLD_SID: 1,
    LOCAL_SID: 2,
    CREATOR_SID: 3,
    NT: 5,
    RESOURCE_MANAGER: 9,
    MANDATORY_LABEL: 16
  }

  attr_accessor :header, :revision, :authority, :subauthority, :identity

  protected
  def initialize(header: true, revision: 1, authority: [], subauthority: [], identity: nil)
    @header       = header
    @revision     = revision
    @authority    = authority
    @subauthority = subauthority
    @identity     = identity
  end

  public
  def to_s
    format 'S-%d-%d%s%d',
           @revision,
           SID::AUTHORITIES[@authority],
           (@subauthority.empty? ? [] : ([''] + @subauthority))
             .map(&:to_s).join('-') + '-',
           @identity
  end

  class << self
    public
    def from_str(sid)
      raise "invalid argument: need String" unless sid.is_a?(String)
      begin
        SID.from_str_array sid.split('-')
      rescue => e
        raise "invalid SID #{sid.inspect}: #{e.message}"
      end
    end

    protected
    def from_str_array(bits)
      inter = {}

      raise "needs 'S'" unless bits[0] == 'S'
      inter[:header] = true

      raise "bad SID revision" unless bits[1] == '1'
      inter[:revision] = bits[1].to_i

      case bits[2]
      when '0' then # the owner of the null account SID and manages one SID: S-1-0-0.
        raise "malformed SID for NULL authority" \
          unless bits.length == 4
        raise "bad identity for NULL authority" \
          unless bits[3] == '0'
        SID.from_str_array_simple_sid(inter, bits, :NULL_SID)

      when '1' then # everyone group, also has one SID only: S-1-1-0.
        raise "malformed SID for WORLD authority" \
          unless bits.length == 4
        raise "bad identity for WORLD authority" \
          unless bits[3] == '0'
        SID.from_str_array_simple_sid(inter, bits, :WORLD_SID)

      when '2' then # local group also has one SID only: S-1-2-0.
        raise "malformed SID for LOCAL authority" \
          unless bits.length == 4
        raise "bad identity for LOCAL authority" \
          unless bits[3] == '0'
        SID.from_str_array_simple_sid(inter, bits, :LOCAL_SID)

      when '3' then # SIDs S-1-3-0 through S-1-3-5
        raise "malformed SID for CREATOR authority" \
          unless bits.length == 4
        raise "bad identity for CREATOR authority" \
          unless %w[0 1 2 3 4 5].include? bits[3]
        SID.from_str_array_simple_sid(inter, bits, :CREATOR_SID)

      when '5' then # owns accounts that are managed by the NT security subsystem.
        SID.from_str_array_mixed_sid(inter, bits, :NT)
      when '9' then
        SID.from_str_array_mixed_sid(inter, bits, :RESOURCE_MANAGER)
      when '16' then
        SID.from_str_array_mixed_sid(inter, bits, :MANDATORY_LABEL)
      else
        raise "no or unknown SID authority"
      end
    end

    protected
    def from_str_array_simple_sid(inter, bits, auth)
      inter[:authority] = auth
      inter[:subauthority] = []
      inter[:identity] = bits[3].to_i
      SID.new(**inter)
    end

    protected
    def from_str_array_mixed_sid(inter, bits, auth)
      subauth = bits.drop(3)
      inter[:authority] = auth
      inter[:subauthority] = subauth.take(subauth.length - 1).map(&:to_i)
      inter[:identity] = subauth.last.to_i
      SID.new(**inter)
    end
  end
end

########################################################################
#### Is this a Windows install? Get some info about it.

def identify_windows(serial, device)
  # assuming there's a Windows installation on `device',
  # try and identify what version of Windows it's running
  # by huffing HKLM/Software/Microsoft/Windows NT/CurrentVersion
  hive_path = walk_to_registry_path(device)
  hives = %w[SAM SOFTWARE SYSTEM].map do |hive|
    [
      hive,
      begin
        fetch_registry_hive(serial, device, hive_path, hive)
      rescue => e
        puts "#{serial} #{device}: #{hive}: sadness: #{e.message}"
        resulting_hive_file(serial, device, hive)
      end
    ]
  end.to_h

  {
    windows_nt_sam:
      begin load_sam_keys_from_registry_hive(hives)
      rescue => e
        puts "#{serial} #{device}: SAM: sadness: #{e.message}"; nil
      end,

    current_version:
      begin load_current_version_from_registry_hive(hives)
      rescue => e
        puts "#{serial} #{device}: SOFTWARE: sadness: #{e.message}"; nil
      end,

    tcpip_parameters:
      begin load_tcpip_parameters_from_registry_hive(hives)
      rescue => e
        puts "#{serial} #{device}: SYSTEM: sadness: #{e.message}"; nil
      end,
  }
end

def load_current_version_from_registry_hive(hives)
  load_from_registry_hivexget(
    hives['SOFTWARE'], 'Microsoft\\Windows NT\\CurrentVersion'
  )
    .reject { |x| x[0] =~ /\ADigitalProductId/i }
    .to_h
end

def load_tcpip_parameters_from_registry_hive(hives)
  load_from_registry_hivexget(
    hives['SYSTEM'], 'ControlSet001\\Services\\Tcpip\\Parameters'
  )
    .select { |x| x[0] =~ /\A(HostName|Domain)\Z/i }
    .to_h
end


INTERESTING_SAM_KEYS = %w[
  Surname
  GivenName
  InternetUserName
  InternetUID
  UserName
]

def extract_interesting_sam_data(hive)
  enc = ->(x) { x.force_encoding(Encoding::UTF_16LE).encode!(Encoding::UTF_8) }
  users = {}
  hive.keys.each do |hk|
    case hk
    when /\A\/SAM\/Domains\/Account\/Users\/([[:xdigit:]]+)\Z/ then
      uid = $1.to_i(16)
      users[uid] ||= {}
      users[uid].merge!(hive[hk])
      %w[Surname GivenName InternetUserName InternetUID].each do |k|
        enc.call(users[uid][k]) if users[uid].key? k
      end

    when /\A\/SAM\/Domains\/Account\/Users\/Names\/(.*)\Z/ then
      username = $1;
      raise "bad key #{hk.inspect}" \
        unless hive[hk]['@'] =~ /\Ahex\(([[:xdigit:]]+)\):\Z/
      uid = $1.to_i(16)
      users[uid] ||= {}
      users[uid]['UserName'] = username
    end
  end

  users.to_a.map do |kv|
    uid, h = kv
    [uid, h.select {|x| INTERESTING_SAM_KEYS.include? x }]
  end.to_h
end


def load_sam_keys_from_registry_hive(hives)
  samfile = hives['SAM']
  sam_i_am = {
    machine_sid:
      system_lines(['samusrgrp', '-s', samfile]).first,
    sam_users_table:
      system_lines(['sampasswd', '-l', samfile])
      .map { |x| x.split(/:/) },
    sam_usergroup_table:
      system_lines(['samusrgrp', '-L', samfile])
      .map { |x| x.split(/:/) },
  }

  # Don't persist the whole hive; just fish out things we want.
  # It'd be nice to do the previous steps too, instead of shelling out.
  hive = load_from_registry_hivexregedit(samfile, '\\')
  sam_i_am[:sam] = extract_interesting_sam_data(hive)
  sam_i_am
end

########################################################################

def taste_partition(serial, part)
  raise 'not a partition' unless part[:type] == 'part'
  partinfo = {}

  case part[:fstype]
  when /\Antfs\Z/i then
    path = part[:path]
    begin
      names_at_root =
        system as_root ntfsls(path, all: true)
      raise "didn't get any names" unless names_at_root
      names_at_root = lines(names_at_root)
      
      partinfo[:listing] =
        system_lines as_root ntfsls(path, all: true, long: true, verbose: true)

      # Is there a Windows in here?
      partinfo.merge! identify_windows(serial, path) \
        unless names_at_root.grep(/\Awindows\Z/i).empty?

#     # Take a full listing of the filesystem contents.
#     File.write(
#       "#{prefix_for_disk_part(serial, path)}ntfsls",
#       system(as_root(ntfsls(path, all: true, long: true, recursive: true)))
#     )

    rescue => e
      puts e.full_message
      puts "this disk is really unhappy... :/"
      partinfo[:sadness] = true
    end

    return partinfo

  else
    puts "don't know what to do with #{part[:fstype]}, in #{part[:path]}"
    return {}
  end
end

########################################################################

SI_FACTORS  = [ ['G',  10**9], ['M',  10**6], ['k',  10**3] ]
IEC_FACTORS = [ ['Gi', 2**30], ['Mi', 2**20], ['ki', 2**10] ]

def humanize_value(size_base, base_unit = 'B', factors = SI_FACTORS)
  for unit, mul in factors
    size_scaled = (size_base*1.0) / mul
    return [size_scaled, unit + base_unit] if size_scaled >= 1.0
  end

  return [size_base, base_unit]
end

########################################################################

def deepwalk(a, ks)
  return nil if a.nil? || !a.respond_to?(:[])
  return nil if ks.nil? || !ks.is_a?(Array) || ks.empty?
  return nil if a.is_a?(Hash) && !a.key?(ks[0])
  return nil if a.is_a?(Array) && ks[0] >= a.length
  ks.length == 1 ? a[ks[0]] : deepwalk(a[ks[0]], ks.drop(1))
end

HOURS_PER_YEAR = 8766.0
HOURS_PER_DAY  = 24.0

def explain_smartctl(smart)
  o = []
  o << 'SMART report: \\\\'

  on_hours = smart['power_on_time']['hours']
  on_years   =  on_hours / HOURS_PER_YEAR
  on_ydays   = (on_hours % HOURS_PER_YEAR) / HOURS_PER_DAY
  on_ydhours = (on_hours % HOURS_PER_YEAR) % HOURS_PER_DAY
  o << '\\hspace*{1em}'
  o << 'power-on time: '
  o << "#{on_hours} hours"
  o << "(#{format('%dy, %dd, %dh', on_years, on_ydays, on_ydhours)})."
  o << '\\\\'

  o << '\\hspace*{1em}'
  o << 'self-test reports:\\\\'
  o << '\\hspace*{2em}'
  o << '\\begin{tabular}{rll}'

  tests =
    deepwalk(smart, ['ata_smart_self_test_log', 'extended', 'table']) ||
    deepwalk(smart, ['ata_smart_self_test_log', 'standard', 'table']) ||
    []
  for test in tests.take([tests.length, 7].min)
    tested_at  = test['lifetime_hours']
    test_type  = test['type']['string'].downcase.delete_suffix("offline")
    test_state = test['status']['string'].downcase

    o << "{\\small #{test_type}} &"
    o << "{\\small @#{tested_at}h} &"
    o << "#{test_state}.\\\\"

    if not test['status']['passed']
      o << " & & {\\small"
      o << "#{100-test['status']['remaining_percent']}\\% done" \
        if test['status'].key?('remaining_percent')
      o << "--- \\textsc{lba} @#{test['lba']}" if test.key?('lba')
      o << "} \\\\"
    end
  end

  # report = deepwalk(smart, %w[ata_smart_data self_test status string])
  # o << "& & #{report}.\\\\"
  o << '\\end{tabular}'

  o.join("\n")
end

def explain_partition_windows_registry_props(o, part)
  current_version  = CICPHash.from_hash part[:current_version]
  tcpip_parameters = CICPHash.from_hash part[:tcpip_parameters]
  windows_nt_sam   = CICPHash.from_hash part[:windows_nt_sam]
  ok = ->(x) { !x.nil? && !x.empty? }
  [
    [
      'registered-owner:',
      -> { tex_safe current_version['RegisteredOwner'] },
      ok.call(current_version['RegisteredOwner'])
    ],

    [
      'registered-org:',
      -> { tex_safe current_version['RegisteredOrganization'] },
      ok.call(current_version['RegisteredOrganization'])
    ],

    [
      'installed-at:',
      -> { Time.at current_version['InstallDate'].split(':', 2)[1].to_i(16) },
      current_version['InstallDate']
    ],

    [
      'hostname:',
      -> { '\\texttt{' + tex_safe((tcpip_parameters['HostName'] || '')) + '}' },
      ok.call(tcpip_parameters['HostName'])
    ],

    [
      'on-domain:',
      -> { "\\texttt{#{tex_safe((tcpip_parameters['Domain'] || ''))}}" },
      ok.call(tcpip_parameters['Domain'])
    ],

    [
      'machine-sid:',
      -> { '{\\footnotesize ' +
          tex_safe(windows_nt_sam[:machine_sid]) + '}' },
      ok.call(windows_nt_sam[:machine_sid])
    ],
  ].each do |row|
    label, value, cond = row
    cond = value if cond.nil?
    if cond
      o << "#{label} &"
      if value.is_a? String then o << value
      elsif value.is_a? Proc then o << value.[]
      end
      o << '\\\\'
    end
  end
end

IGNORE_USERS = %w[Administrator DefaultAccount Guest HomeGroupUser$]

def explain_partition_windows_sam_list(o, part)
  windows_nt_sam   = CICPHash.from_hash part[:windows_nt_sam]
  return if windows_nt_sam[:sam].empty?
  o << 'users:'
  windows_nt_sam[:sam].each do |rid, hdata|
    username = hdata['UserName']
    next if IGNORE_USERS.include?(username)
    o << "& #{rid}: #{tex_safe(username)}"
    if hdata['GivenName'] and hdata['Surname']
      o << "{\\small (" +
           tex_safe(hdata['GivenName']) + " " +
           tex_safe(hdata['Surname']) + ")}"
    end
    o << '\\\\'
  end
end

def explain_partition(part)
  o = []
  if part[:current_version]
    current_version  = CICPHash.from_hash part[:current_version]
    tcpip_parameters = CICPHash.from_hash part[:tcpip_parameters]

    o << '\\\\'
    o << '... a'
    product_name = current_version['ProductName']
    edition_id   = current_version['EditionID']
    o << tex_safe(product_name)
    o << ' ' + tex_safe(edition_id) \
      unless product_name =~ /Windows 10 Pro/ ||
             product_name =~ /Windows 7/
    o << 'installation:'
    o << '\\par\\hspace*{-2em}'
    o << '\\begin{tabular}{rl}'
    explain_partition_windows_registry_props o, part
    explain_partition_windows_sam_list o, part
    o << '\\end{tabular}'
  end

  o.join("\n")
end

########################################################################

def disk_failing?(smart, parts)
  tests =
    deepwalk(smart, ['ata_smart_self_test_log', 'extended', 'table']) ||
    deepwalk(smart, ['ata_smart_self_test_log', 'standard', 'table']) || []

  return false if tests.empty?
  !tests[0]['status']['passed'] ||
  parts.any? { |kv| kv[1].has_key? :sadness }
end

########################################################################

def tex_safe(str)
  str.gsub('_', '\\_')
end

REPORT_TEMPLATE = <<'__EOF__'
\documentclass[english, 11pt]{article}

\usepackage{cochineal}
\usepackage{biolinum}
\usepackage{beramono}
\renewcommand{\familydefault}{\sfdefault}
\usepackage[T1]{fontenc}
\usepackage{babel}

\usepackage[latin9]{inputenc}

\usepackage{multicol}

% \usepackage{fancyhdr}
\pagestyle{empty}
\usepackage{geometry}
\geometry{
  landscape,
  a5paper,
  verbose,
  tmargin=0.5in,
  bmargin=0.5in,
  lmargin=0.5in,
  rmargin=0.5in}

\setlength{\parskip}{\smallskipamount}
\setlength{\parindent}{0pt}

\begin{document}

\begin{tabular}[t]{rl}
{\sffamily\scshape serial} &
{\ttfamily\bfseries\Huge
  <%= @smart['serial_number'] %>}
\\

<%- if @smart['wwn'] -%>
{\sffamily\scshape wwn} &
{\ttfamily\large
  <%= format '%01X\\,%06X\\,%010X', @smart['wwn']['naa'], @smart['wwn']['oui'], @smart['wwn']['id'] %>}
\\
<%- end -%>

{\sffamily\scshape size} &
{\sffamily\bfseries\large
  <%= format '%d\\,%s', *humanize_value(@size, unit = 'B', factors = SI_FACTORS) %>
  <%= format '(%.1f\\,%s)', *humanize_value(@size, unit = 'B', factors = IEC_FACTORS) %>
}
\\

{\sffamily\scshape model} &
{\sffamily\large
  <%= @smart['model_name'] %>}
\\

&
{\sffamily\large
  <%= @smart['model_family'] %>}
\\

\end{tabular}
<%- if disk_failing?(@smart, @parts) -%>
\hfill{\sffamily\bfseries\Huge FAILED}\hfill~
<%- end -%>

\vspace*{0.5em}
\begin{multicols}{2}
{\sffamily <%= explain_smartctl(@smart) %>}

\vspace*{0.5em}
{\sffamily
Found a <%= @lsblk_disk[:pttype].upcase %> partition table.
}

<%- @have_partition_env = false -%>
<%- for blk in @lsblk -%>
<%-   next unless blk[:type] == 'part' -%>
<%-   unless @have_partition_env -%>
\begin{description}
<%-     @have_partition_env = true -%>
<%-   end -%>
<%-   part_size = blk[:size].to_i -%>
<%-   portion   = Rational(part_size, @size) -%>
<%-   height_mm = [4.0, 150.0 * portion].max -%>
<%-   index = blk[:name].delete_prefix(@lsblk_disk[:name]) -%>
\par
\smallskip
% \rule[<%= format '%.02f', 3 - height_mm %>mm]{0.5ex}{<%= format '%.02f', height_mm %>mm}
% \hspace*{0.5ex}
% \begin{minipage}[t]{.9\linewidth}
\item[<%= index %>] {\sffamily <%= blk[:parttypename] %>
(\textsc{<%= blk[:fstype] %>},
  <%= format '%d\\,%s', *humanize_value(part_size, unit = 'B', factors = SI_FACTORS) %>)
% \hfill{\tiny\texttt{<%= blk[:partuuid] %>}}
<%-   if blk[:label] != '' -%>
\\
``<%= tex_safe(blk[:label]) %>''
<%-   end -%>
<%-   part = @parts[blk[:path]] -%>
<%-   if not (part.keys - [:listing]).empty? -%>
<%= explain_partition(part) %>
<%-   end -%>
}
<%- end -%>
<%- if @have_partition_env -%>
\end{description}
<%- end -%>
\vfill~
\end{multicols}
\vfill~
\pagebreak

<%- for blk in @lsblk -%>
<%-   next unless blk[:type] == 'part' -%>
<%-   part_size = blk[:size].to_i -%>
<%-   portion   = Rational(part_size, @size) -%>
<%-   height_mm = [4.0, 99.0 * portion].max -%>
<%-   index = blk[:name].delete_prefix(@lsblk_disk[:name]) -%>
\par
\rule[<%= format '%.02f', 3 - height_mm %>mm]{0.5ex}{<%= format '%.02f', height_mm %>mm}
\hspace*{0.5ex}
\begin{minipage}[t]{.9\linewidth}
\textbf{<%= index %>}: \textsf{<%= tex_safe(blk[:label]) %>}
  ({\small\textsc{<%= blk[:fstype] %>}, <%= blk[:parttypename] %>};
      <%= format '%.1f\\,%s', *humanize_value(part_size, unit = 'B', factors = SI_FACTORS) %>)
\end{minipage}
<%- end -%>

\end{document}
__EOF__

########################################################################

def export_main(device)
  # Load `lsblk' results *before* we write a copy of the file,
  # so we can get the serial number out.

  lsblk = run_lsblk device
  @lsblk  = load_lsblk_output(lsblk)
  @serial = @lsblk[0][:serial]
  File.write(lsblk_file_for(@serial), lsblk)
  File.write("#{@serial}.lsblk.yaml", YAML.dump(@lsblk))

  smartctl, smartctl_anger = run_smartctl device
  puts smartctl_anger.inspect
  File.write(smartctl_file_for(@serial), smartctl)
  @smart = load_smartctl_output(smartctl)

  @parts =
    @lsblk
      .filter {|x| x[:type] == 'part' }
      .map {|x| [x[:path], taste_partition(@serial, x)] }
      .to_h

  File.write(partition_file_for(@serial), YAML.dump(@parts))
  puts ["d=#{@serial}; ",
        'report-disk $d > $d.tex && ',
        'pdflatex $d && ',
        'evince $d.pdf'].join
end

def report_main(serial)
  @lsblk = load_lsblk_output    File.read lsblk_file_for(serial)
  @smart = load_smartctl_output File.read smartctl_file_for(serial)
  @parts = YAML.load File.read partition_file_for(serial)
  for path, part in @parts
    part
  end
  @lsblk_disk = @lsblk[0]
  @size = 1.0 * @smart['user_capacity']['bytes']

  # template = ERB.new REPORT_TEMPLATE.lines.take(38).join, trim_mode: '-'
  template = ERB.new REPORT_TEMPLATE, trim_mode: '-'
  puts template.result(binding)
end

case File.basename($PROGRAM_NAME)
when 'export-disk' then export_main ARGV[0]
when 'report-disk' then report_main ARGV[0]
end
