#!/usr/bin/env zsh
# NAME: z3i --- a primitive Z3 SMTLIB2 REPL
# SYNOPSIS: z3i

exec \
rlwrap -S 'z3> ' \
z3 -in
