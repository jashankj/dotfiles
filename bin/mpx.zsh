#!/usr/bin/env zsh
# NAME: mpx --- invoke mpv with our usual flags.
# SYNOPSIS: mpx [flags]
#
#	% mpx /alyzon/media/TV/Castle

mpv_flags=(
	--speed=1.3

	# Always try to preserve and resume playback locations.
	# This *should* mean, e.g., playing back a directory
	# will pick up from where we left off.
	--save-position-on-quit
	--playlist-start=auto

	# Video configuration: play out using GPU acceleration.
	--vo=gpu --hwdec=auto
	--video-sync=desync

	# Audio configuration: play out via PulseAudio.
	--ao=pulse

	# Try and turn subtitles on.
	--sub=auto --sub-auto=fuzzy

	# Crank brightness settings up.
	--gamma=10 --saturation=10 --contrast=10
)

exec mpv ${mpv_flags} "$@"
