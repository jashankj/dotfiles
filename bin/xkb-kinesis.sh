#!/bin/sh

xinput \
| perl -Mfeature=say -ne '
	if (/keyboard/ && /hid 05f3:0007/i && /id=(\d+)/) { say $1 }
	if (/keyboard/ && /hid 05ac:0204/i && /id=(\d+)/) { say $1 }
' \
| xargs -t -I% \
	setxkbmap \
		-device % \
		-layout us \
		-option ctrl:nocaps \
		-option compose:prsc
