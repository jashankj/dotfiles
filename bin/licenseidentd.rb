#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: false

require 'set'
require 'socket'

require 'rubygems'
require 'classifier-reborn'
require 'rugged'

require 'pry'

$lsi = ClassifierReborn::LSI.new auto_rebuild: false

def clean_name(name)
  name
    .delete_suffix('.txt')
    .delete_prefix('deprecated_')
end

def clean_text(text)
  text
    .delete(',/"')
    .split
    .map(&:downcase)
    .join(' ')
    .force_encoding(Encoding::UTF_8)
end

EXCLUSIONS = Set.new([
  'Net-SNMP'                  # collection of licenses
])

LLD_REPO = '/src/_github/spdx/license-list-data'
def load_licenses
  puts '# Loading licenses...'
  lld_git  = Rugged::Repository.new(LLD_REPO)
  lld_tree = lld_git.head.target.tree
  lld_text = Rugged::Tree.lookup(lld_git, lld_tree.path('text')[:oid])
  lld_text.walk_blobs do |_, entry|
    name = clean_name entry[:name]
    next if EXCLUSIONS.include? name

    blob = Rugged::Blob.lookup(lld_git, entry[:oid])
    text = clean_text blob.content

    puts "#  + #{name}"
    $lsi.add_item text, name
  end
end

def reindex(cutoff = 1)
  puts '# Reindexing...'
  $lsi.build_index(cutoff)
end

def report_related(lsi, doc, max_nearest = 10, &block)
  result =
    lsi \
      .proximity_array_for_content(doc, &block) \
      .reject { |pair| pair[0].eql? doc }
  result[0..max_nearest - 1] \
    .map { |pair| [lsi.categories_for(pair[0]), pair[1]] }
end

def unix_server
  socket = File.join(ENV['HOME'], '.cache', 'licenseid.sock')
  puts "# Waiting for connections to #{socket}"
  File.delete socket if File.exist? socket
  server = UNIXServer.new socket
  loop do
    Thread.start(server.accept) do |client|
      handle_request(client)
    end
  end
end

def handle_request(client)
  puts "# Spawning to accept #{client.inspect}"
  input = clean_text client.read
  maybe = report_related($lsi, input, 10)
  maybe = maybe
  puts "# Got matches for #{client.inspect}: #{maybe.inspect}"
  client.puts maybe.inspect
  client.close
end

load_licenses
reindex(cutoff = 0.6)
unix_server

binding.pry
