#!/bin/sh
# export a zsh array of Clang's understood languages

# Upstream:
llvm_src=https://raw.githubusercontent.com/llvm/llvm-project/main

# Local repository:
llvm_src=file:///src/llvm/src

{
echo "#define LANGSTANDARD(IDENT, NAME, LANG, DESC, FEATURES)" \
	"langstd+=([NAME]=IDENT);";
echo "#define LANGSTANDARD_ALIAS(IDENT, ALIAS)" \
	"langstd_alias+=([IDENT]=ALIAS);";
#echo "#define LANGSTANDARD_ALIAS_DEPR(IDENT, ALIAS)" \
#	"langstd_alias+=([IDENT]=ALIAS);";
curl ${llvm_src}/clang/include/clang/Basic/LangStandards.def
} |
cpp -traditional -nostdinc |
perl -Mfeature=say -ne '
	BEGIN {
		say "typeset -A langstd;";
		say "typeset -A langstd_alias;";
	}
	next if /^# /;
	s@[[:space:]]*//.*@@;
	s@[[:space:]]*@@g;
	s@^[[:space:]]*$@@;
	s@"@@g;
	next if $_ eq "";
	say;
'