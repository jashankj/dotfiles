#!/bin/sh

xinput \
| perl -Mfeature=say -ne '
	if (/keyboard/ && /zsa technology labs ergodox ez glow/i && /id=(\d+)/) { say $1 }
' \
| xargs -t -I% \
	setxkbmap \
		-device % \
		-layout us \
		-option ctrl:nocaps \
		-option compose:prsc
