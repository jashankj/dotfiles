#!/usr/bin/env zsh
# NAME: mkmailbox --- create a new mailbox visible via IMAP
# SYNOPSIS: mkmailbox <repository> <folder-name>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 2 ] || usage

repo="$1"
dir="$2"

[ -d "$HOME/Mail/${repo}" ] ||
    errx EX_SOFTWARE "unknown repository '${repo}'"
[ -d "$HOME/Mail/${repo}/${dir}" ] &&
    errx EX_SOFTWARE "${repo}/${dir} already exists?"
[ -e "$HOME/Maildir/.${repo}.${dir}" ] &&
    errx EX_SOFTWARE "%${repo}.${dir} already exists?"

mkmaildir "$HOME/Mail/${repo}/${dir}"
cd ~/Maildir
ln -sv ../"Mail/${repo}/${dir}" ".${repo}.${dir}"
