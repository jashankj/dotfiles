#!/bin/sh
# Akin to CSE's `newclass' script.

command="$(basename "$0")"

# on CSE? use newclass, not this script.
[ -x "/usr/local/bin/newclass" ] &&
exec "/usr/local/bin/${command}" "$@"

CTERM=21T3

# approximate ae(1)'s ${...:A} expansion
colon_A() {
	echo "$@" | tr "[:upper:]" "[:lower:]"
}

# approximate ae(1)'s ${...:tN} expansion
colon_tN() {
	n="$1"; shift
	echo "$@" | tail -c "$n"
}

case "${command}" in
	(109[1-2])               CCODE="DPST${command}" ;;
	([0-9][0-9][0-9][0-9])   CCODE="COMP${command}" ;;
	(cs[0-9][0-9][0-9][0-9]) CCODE="COMP${command}" ;;
	([A-Za-z][A-Za-z][A-Za-z][A-Za-z][0-9][0-9][0-9][0-9])
		CCODE="$(colon_A "${command}")"
		;;
	(*)
		echo >&2 "newclass: '$command' is not a valid class."
		exit 2
		;;
esac

CSUFFIX="$(colon_tN 4 "${CCODE}")"
case "$CCODE" in
	(COMP[0-9][0-9][0-9][0-9]) CACCT=cs${CSUFFIX} ;;
	(SENG[0-9][0-9][0-9][0-9]) CACCT=se${CSUFFIX} ;;
	(BINF[0-9][0-9][0-9][0-9]) CACCT=bi${CSUFFIX} ;;
	(DPST[0-9][0-9][0-9][0-9]) CACCT=dp${CSUFFIX} ;;
	(ENGG[0-9][0-9][0-9][0-9]) CACCT=en${CSUFFIX} ;;
	(GENE[0-9][0-9][0-9][0-9]) CACCT=ge${CSUFFIX} ;;
	(GSOE[0-9][0-9][0-9][0-9]) CACCT=gs${CSUFFIX} ;;
	(HSCH[0-9][0-9][0-9][0-9]) CACCT=hs${CSUFFIX} ;;
	(INFS[0-9][0-9][0-9][0-9]) CACCT=is${CSUFFIX} ;;
	(REGZ[0-9][0-9][0-9][0-9]) CACCT=rz${CSUFFIX} ;;
	(*) echo >&2 "newclass: could not derive account name for class $CCODE"
esac

case "$CCODE/$CTERM" in
	(COMP1511/*)
	;;
	(COMP1521/*)
	;;
	(COMP1911/*)
	;;
	(COMP1917/*)
	;;
	(COMP1927/*)
	;;
	(COMP2041/* | COMP9041/* | COMP9044/*)
	;;
	(COMP2521/19T0 | COMP2521/19x1)
		HOME2521=/home/cs2521
		RESOLVER=lts-9.21
		GHCVERS=8.0.2
		GHCARCH=x86_64-freebsd # GHCARCH=x86_64-linux-tinfo6
		STACKDIR="$HOME2521/cs2521.19x1.courseware/_tools/.stack-work"
		HSBIN="$STACKDIR/install/${GHCARCH}/${RESOLVER}/${GHCVERS}/bin"
		PATH="$HSBIN:$PATH"
		;;
	(COMP2521/*)
	;;
	(COMP3231/* | COMP3891/* | COMP9201/* | COMP9283/*)
	;;
	(COMP6443/* | COMP6843/*)
	;;
	(COMP9315/*)
	;;
esac

export      CLASS="$CCODE"
export  GIVECLASS="$CCODE"
export GIVEPERIOD="$CTERM"

PATH=.:$PATH
PATH=/usr/sbin:$PATH
PATH=/sbin:$PATH
PATH=/bin:$PATH
PATH=/usr/bin:$PATH
PATH=/usr/local/bin:$PATH
#PATH=/home/give/stable/bin:$PATH
#PATH=/home/class/bin:$PATH
PATH=/web/$CACCT/bin:$PATH
PATH=/web/$CACCT/$CTERM/private/scripts:$PATH
PATH=/home/$CACCT/bin:$PATH
export PATH

exec "$@"
