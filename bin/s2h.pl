#!/usr/bin/env perl
#-
# Copyright (c) 1993-2018 John Shepherd <jas@cse.unsw.edu.au>
#

#
# Convert file in .slides format to HTML slides and HTML notes
#

#
# Special directories
#
$HTMLib = "/home/jas/lib/html";

#
#
# Get the date
#
@now = localtime(time());
@m = ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
$date = sprintf("%d %s %d", $now[3], $m[$now[4]], 1900+$now[5]);

#
# Links at top and bottom of each slide
#
#<a href="index.html"><img border=0 alt="[index]" src="icon/up.gif"></a>
$SlideLinks=<<TEXT
<a href="XXX"><img border=0 alt="[prev]" src="icon/prev.gif"></a>
<a href="index.html"><span style="font-size:14pt;">ZZZ</span></a>
<a href="YYY"><img border=0 alt="[next]" src="icon/next.gif"></a>
TEXT
;
$SlideControl=<<TEXT
<script language="JavaScript">
<!--
function getKey(keyStroke) {
	isNetscape = (navigator.appName.indexOf('Netscape') == 0)
	eventChooser = (isNetscape) ? keyStroke.which : event.keyCode;
	which = String.fromCharCode(eventChooser);
	if (which == ' ') window.location = "YYY";
	if (which == 'n') window.location = "YYY";
	if (which == '\b') window.location = "XXX";
	if (which == 'b') window.location = "XXX";
}
document.onkeypress = getKey;
-->
</script>
TEXT
;

#
# Subroutine to apply standard in-line conversions
#
sub line_filter
{
	$_[0] =~ s/<\$>/<i>/g;
	$_[0] =~ s/<\/\$>/<\/i>/g;
	$_[0] =~ s/<@>/<large><code>/g;
	$_[0] =~ s/<@@>/<large><font color="#008800"><b><code>/g;
	$_[0] =~ s/<\/@>/<\/code><\/large>/g;
	$_[0] =~ s/<\/@@>/<\/code><\/b><\/font><\/large>/g;
	$_[0] =~ s/<tilde>/~/g;
	$_[0] =~ s/<at>/@/g;
	$_[0] =~ s/<floor>/floor(/g;
	$_[0] =~ s/<\/floor>/)/g;
	$_[0] =~ s/<ceil>/ceil(/g;
	$_[0] =~ s/<\/ceil>/)/g;
	$_[0] =~ s/<hspace>/\&nbsp;/g;
	$_[0] =~ s/<\~>/\&nbsp;/g;
	$_[0] =~ s/<\~\~>/\&nbsp;\&nbsp;/g;
	$_[0] =~ s/<\~\~\~>/\&nbsp;\&nbsp;\&nbsp;/g;
	$_[0] =~ s/<\~\~\~\~>/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g;
	$_[0] =~ s/<dash>/\&#8212;/g;
	$_[0] =~ s/<q>/"/g;
	$_[0] =~ s/<\/q>/"/g;
	$_[0] =~ s/<dollar>/\$/g;
	$_[0] =~ s/<left\(>/(/g;
	$_[0] =~ s/<right\)>/)/g;
	$_[0] =~ s/<Rightarrow>/\&rArr;/g;
	$_[0] =~ s/<Lightarrow>/\&lArr;/g;
	$_[0] =~ s/<notRightarrow>/\&#8655;/g;
	$_[0] =~ s/<rightarrow>/\&rarr;/g;
	$_[0] =~ s/<notrightarrow>/\&#8603;/g;
	$_[0] =~ s/<leftarrow>/\&larr;/g;
	$_[0] =~ s/<leftrightarrow>/\&harr;/g;
	$_[0] =~ s/<approx>/\&cong;/g;
	$_[0] =~ s/<and>/\&and;/g;
	$_[0] =~ s/<or>/\&or;/g;
	$_[0] =~ s/<not><elem>/\&notin;/g;
	$_[0] =~ s/<not><in>/\&notin;/g;
	$_[0] =~ s/<not>/\&not;/g;
	$_[0] =~ s/<in>/\&isin;/g;
	$_[0] =~ s/<elem>/\&isin;/g;
	$_[0] =~ s/<leq>/\&le;/g;
	$_[0] =~ s/<geq>/\&ge;/g;
	$_[0] =~ s/<neq>/\&ne;/g;
	$_[0] =~ s/<sum>/\&sum;/g;
	$_[0] =~ s/<prod>/\&prod;/g;
	$_[0] =~ s/<sqrt>/\&radic;/g;
	$_[0] =~ s/<\/sqrt>//g;
	$_[0] =~ s/<plusmn>/\&plusmn;/g;
	$_[0] =~ s/<sel>/\&sigma;/g;
	$_[0] =~ s/<proj>/\&pi;/g;
	$_[0] =~ s/<join>/\&#8904;/g;
	$_[0] =~ s/<renam>/<img src="sym\/renam.gif">/g;
	$_[0] =~ s/<subset>/\&sub;/g;
	$_[0] =~ s/<subseteq>/\&sube;/g;
	$_[0] =~ s/<times>/\&times;/g;
	$_[0] =~ s/<union>/\&cup;/g;
	$_[0] =~ s/<intersect>/\&cap;/g;
	$_[0] =~ s/<exists>/\&exist;/g;
	$_[0] =~ s/<forall>/\&forall;/g;
	$_[0] =~ s/<delta>/\&delta;/g;
	$_[0] =~ s/<infty>/\&infin;/g;
	$_[0] =~ s/<empty>/\&empty;/g;
	$_[0] =~ s/<oplus>/\&oplus;/g;
	$_[0] =~ s/<bigoplus>/<big>&oplus;<\/big>>/g;
	$_[0] =~ s/<ll>/\&ll;/g;
	$_[0] =~ s/<gg>/\&gg;/g;
	$_[0] =~ s/<log>/log/g;
	$_[0] =~ s/<dollar>/\$/g;
	$_[0] =~ s/<frac>//g;
	$_[0] =~ s/<divides>/\//g;
	$_[0] =~ s/<over>/\//g;
	$_[0] =~ s/<\/frac>//g;
	$_[0] =~ s/<progdef>/<font color='#996600'><b><large><code>/g;
	$_[0] =~ s/<\/progdef>/<\/code><\/large><\/b><\/font>/g;
	$_[0] =~ s/<brown>/<font color='#996600'>/g;
	$_[0] =~ s/<\/brown>/<\/font>/g;
	$_[0] =~ s/<navy>/<font color='#000099'>/g;
	$_[0] =~ s/<\/navy>/<\/font>/g;
	$_[0] =~ s/<red>/<font color='#CC0000'>/g;
	$_[0] =~ s/<\/red>/<\/font>/g;
	$_[0] =~ s/<blue>/<font color='#0000CC'>/g;
	$_[0] =~ s/<\/blue>/<\/font>/g;
	$_[0] =~ s/<orange>/<font color='#CC9900'>/g;
	$_[0] =~ s/<\/orange>/<\/font>/g;
	$_[0] =~ s/<green>/<font color='#009900'>/g;
	$_[0] =~ s/<\/green>/<\/font>/g;
	$_[0] =~ s/<gray>/<font color='#999999'>/g;
	$_[0] =~ s/<gb>/<font color="green"><b>/g;
	$_[0] =~ s/<\/gb>/<\/b><\/font>/g;
	$_[0] =~ s/<tiny>/<span style="font-size:67%">/g;
	$_[0] =~ s/<\/tiny>/<\/span>/g;
	$_[0] =~ s/<rb>/<font color="red"><i>/g;
	$_[0] =~ s/<\/rb>/<\/i><\/font>/g;
	$_[0] =~ s/<\/gray>/<\/font>/g;
	$_[0] =~ s/<href=([^>]+)>/<a href="$1">$1<\/a>/;
	$_[0] =~ s/<</\&lt;/g;
	$_[0] =~ s/>>>/>\&gt;/g;
	$_[0] =~ s/>>/\&gt;/g;
	$_[0] =~ s/<nerd>/<span class='nerd'>/g;
	$_[0] =~ s/<\/nerd>/<\/span>/g;
	if ($indeftable || $intable) {
		$_[0] =~ s/<row>/<tr valign=top>/g;
		$_[0] =~ s/<\/row>/<\/tr>/g;
		$_[0] =~ s/<col1>/<td><nobr>/g;
		$_[0] =~ s/<col[2-9]>/<td><\/td><td>/g;
		$_[0] =~ s/<\/col[0-9]>/<\/td>/g;
	}
	elsif ($inreltable) {
		$_[0] =~ s/<row>/<tr align=center>/g;
		$_[0] =~ s/<\/row>/<\/tr>/g;
		$_[0] =~ s/<col[0-9]>/<td>/g;
		$_[0] =~ s/<\/col[0-9]>/<\/td>/g;
		$_[0] =~ s/<attr[0-9]>/<td>\&nbsp;\&nbsp;<b>/g;
		$_[0] =~ s/<\/attr[0-9]>/<\/b>\&nbsp;\&nbsp;<\/td>/g;
	}
	if ($_[0] =~ /<image ([^>]*)>/) # <image file-name>
	{
		$image = $1;
		if ($image !~ /\./) { $image = "$image.png" }
		$_[0] =~ s/<image [^>]*>/<img src="$image">/
	}
}

sub slide_filter
{
	# do emphasis using color on slides

	$_[0] =~ s/<em>/<font color='#0000BB'>/g;
	$_[0] =~ s/<\/em>/<\/font>/g;
	$_[0] =~ s/<term>/<font color='#0000BB'>/g;
	$_[0] =~ s/<\/term>/<\/font>/g;
	$_[0] =~ s/<comment>/<span class='comment'>/g;
	$_[0] =~ s/<\/comment>/<\/span>/g;
	$_[0] =~ s/<hilite>/<font color='navy'>/g;
	$_[0] =~ s/<\/hilite>/<\/font>/g;
}

#
# Check args and open .slides file
#
if ($#ARGV != 0 || $ARGV[0] !~ /\.slides$/) { die "Usage: $0 xyz.slides"; }
open(SLIDES, "<$ARGV[0]") || die "$0: Can't open slides file $ARGV[0]";

$title = <SLIDES>;
chop($title);
if ($title !~ /^<title>/)
	{ die "$0: Can't find title in $ARGV[0]"; }
else
	{ $title =~ s/<title>//; }
$basedir = <SLIDES>;
chop($basedir);
if ($basedir !~ /^<base>/ && $basedir !~ /^$/)
	{ die "$0: Can't find base in $ARGV[0]"; }
else
	{ $basedir =~ s/<base>//; }

#
# Count slides
# (so we know when we reach last on the second (main) pass)
#
$max_slide = 0;
$inslide = 0;
$line = 1;  ## we read <title> line already
while (<SLIDES>)
{
	$line++;
	if (/^<slide>/)
	{
		if ($inslide == 1)
			{ die "Missing </slide> near line $line"; }
		$max_slide++;
		$inslide = 1;
	}
	elsif (/^<\/slide>/)
	{
		if ($inslide == 0)
			{ die "Too many </slide>'s at line $line"; }
		$inslide = 0;
	}
}
if ($inslide == 1)
	{ die "Missing </slide> near line $line"; }

#
# Assume that slide containing bibliography is always last
#
$ref_slide = sprintf("slide%03d.html", $max_slide);

#
# Rewind slides file for second pass
#
seek(SLIDES, 0, 0);

#
# Make icons directory
#
if (! -d "icon")
{
	mkdir("icon",0755) || die "Can't make icons directory";
	system "cp $HTMLib/icon/*.gif ./icon";
}

#
# Make symbols directory
#
if (! -d "sym")
{
	mkdir("sym",0755) || die "Can't make symbols directory";
	system "cp $HTMLib/sym/*.gif ./sym";
}

#
# Copy slides.css if needed
#
if (! -f "slides.css")
{
        system "cp $HTMLib/slides.css ./slides.css";
}

#
# Open slides index
#
$up = "<a href='../index.html'><img border=0 alt='[index]' src='icon/up.gif'></a>\n";
open(INDEX, ">index.html") || die "$0: Can't open index file";
print INDEX "<html>\n";
print INDEX "<head><title>$title (slides)</title>\n";
print INDEX "<link href='slides.css' rel='stylesheet' type='text/css'>\n";
print INDEX "</head>\n<body>\n<div style='padding:10px 5px 10px 5px'>$up</div>\n";
print INDEX "<center><h3>$title</h3></center>\n";
print INDEX "\n<center><table><tr><td>\n";
print INDEX "\n<small><ul>\n";

#
# Read the .slides file and produce slides and index
#
$snum = 1;
$eqnum = 0;
$inslide = 0;
while (<SLIDES>)
{
	chop;
	if (/^%/)
	{
	}
	elsif (/^<slide>/)
	{
		$inslide = 1;
		$this_slide = sprintf("slide%03d.html", $snum);
		if ($snum == 1)
			{ $prev_slide = "index.html"; }
		else
			{ $prev_slide = sprintf("slide%03d.html", $snum-1); }
		if ($snum == $max_slide)
			{ $next_slide = "index.html"; }
		else
			{ $next_slide = sprintf("slide%03d.html", $snum+1); }
		$links = $SlideLinks;
		$links =~ s/XXX/$prev_slide/;
		$links =~ s/YYY/$next_slide/;
		$links =~ s/ZZZ/$snum/;
		$control = $SlideControl;
		$control =~ s/XXX/$prev_slide/g;
		$control =~ s/YYY/$next_slide/g;
		open(SLIDE, ">$this_slide");
		print SLIDE "<html>\n";
		print SLIDE "<head><title>$title ($snum)</title>\n";
		print SLIDE "<link href='slides.css' rel='stylesheet' type='text/css'>\n";
		print SLIDE "$control\n</head>\n";
		print SLIDE "<body>\n";
		print SLIDE "$links";
		print SLIDE "\n<div align='center'><table height='95%' width='92%'><tr><td valign='top'>\n";
	}
	elsif (/^<\/slide>/)
	{
		print SLIDE "</td></tr></table></div>\n";
		print SLIDE "</body>\n</html>\n";
		close(SLIDE);
		$snum++;
		$inslide = 0;
	}
	elsif (/^<section>/)
	{
		($heading = $_) =~ s/<section>//;
		&line_filter($heading);
		print SLIDE "<br><br>";
		print SLIDE "<div align='center'><h2 style='color:green'>";
		print SLIDE "$heading";
		print SLIDE "</h2></div>\n";
		print INDEX "<li class=\"i\"> <a href=\"$this_slide\"><b>$heading</b></a>\n";
	}
	elsif (/^<heading>/)
	{
		($heading = $_) =~ s/<heading>//;
		&line_filter($heading);
		$curHeading = $heading;
		print SLIDE "<div align='center'><h3>";
		print SLIDE "$heading";
		print SLIDE "</h3></div>\n";
		print INDEX "<li class=\"i\"> <a href=\"$this_slide\">$heading</a>\n";
	}
	elsif (/^<subheading>/)
	{
		($heading = $_) =~ s/<subheading>//;
		$heading =~ s/<\/subheading>//;
		&line_filter($heading);
		print SLIDE "<h4>";
		print SLIDE "$heading\n";
		print SLIDE "</h4>\n";
		$heading =~ s/<br>/\&nbsp;/g;
	}
	elsif (/^<exercise>/)
	{
		$nex++;
		($xh = $_) =~ s/<exercise>//;
		&line_filter($xh);
		$h = "Exercise $nex: $xh";
		print SLIDE "<div align='center'><h3>$h</h3></div>\n";
		print INDEX "<li class=\"i\"> <a href=\"$this_slide\">Ex$nex: $xh</a>\n";
	}
	elsif (/^<continued>/)
	{
		$heading = $curHeading;
		print SLIDE "<center><h3>";
		print SLIDE "$heading ";
		print SLIDE "<span class=\"cont\">(cont)</span>\n";
		print SLIDE "</h3></center>\n";
	}
	elsif (/^<continuex>/)
	{
		$heading = $curHeading;
		print SLIDE "<center><h3>";
		print SLIDE "Ex $nex: $xh ";
		print SLIDE "<span class=\"cont\">(cont)</span>\n";
		print SLIDE "</h3></center>\n";
	}
	elsif (/^<diagram>/) # <diagram>file-name
	{
		($image = $_) =~ s/<diagram>//;
		if ($image !~ /\./) { $image = "$image.png" }
		print SLIDE "<p><center>\n<img alt=\"[Diagram:$image]\"";
		print SLIDE " src=\"$image\">\n</center><p>\n";
	}
	elsif (/^\<citation /) # <citation tag>authors
	{
		($line = $_) =~ s/^<citation //;
		($tag = $line) =~ s/>.*$//;
		($cite = $line) =~ s/.*>//;
		print SLIDE "[<a href=\"$ref_slide#$tag\">$cite</a>]\n";
	}
	elsif (/^<example>/) # <example>file-name
	{
		($fname = $_) =~ s/<example>//;
		print SLIDE "<a href=\"examples/$fname\">$basedir/examples/$fname</a>\n";
	}
	elsif (/^<pp>/)
	{
		print SLIDE "<p>&nbsp;<p>\n";
	}
	elsif (/^<vspace /)
	{
		($n = $_) =~ s/<vspace //;
		$n =~ s/>.*$//;
		for ($i = 1; $i < $n; $i++)
		{
			print SLIDE "<p><br>";
		}
		print SLIDE "<p>\n";
	}
	elsif (/^<\$\$>/)
	{	print SLIDE "<p><center><i>\n";
	}
	elsif (/^<\/\$\$>/)
	{	print SLIDE "</i></center><p>\n";
	}
	elsif (/^<eqn>/)
	{
		($eqn = $_) =~ s/<eqn>//;
		$eqn_file = sprintf("eqn/%03d.gif", $eqnum);
		system("/home/jas/bin/scripts/e2g '$eqn' $eqn_file 2>&1 > /dev/null");
		$eqnum++;
		print SLIDE "<img alt=\"$eqn\" src=\"$eqn_file\">\n";
	}
	elsif (/^<indent>/)
	{	print SLIDE "<dl><dd>\n";
	}
	elsif (/^<\/indent>/)
	{	print SLIDE "<\/dd><\/dl>\n";
	}
	elsif (/^<itemize>/)
	{	print SLIDE "<ul>\n";
	}
	elsif (/^<\/itemize!>/)
	{	print SLIDE "</ul>&nbsp;<br>\n";
	}
	elsif (/^<\/itemize>/)
	{	print SLIDE "</ul>\n";
	}
	elsif (/^<enumerate>/)
	{	print SLIDE "<p><ol>\n";
	}
	elsif (/^<\/enumerate>/)
	{	print SLIDE "</ol><p>\n";
	}
	elsif (/^<item>/ || /^<sitem>/)
	{
		if (/^<sitem>/)
			{ ($line = $_) =~ s/<sitem>/<li class="s">/; }
		else
			{ ($line = $_) =~ s/<item>/<li>/; }
		&line_filter($line);
		&slide_filter($line);
		print SLIDE "$line\n";
	}
	elsif (/^<program>/ || /^<session>/ || /^<syntax>/)
	{	print SLIDE "<p><center><table cellpadding='8'><tr><td valign='middle' bgcolor='#DDDDDD'><pre>\n";
	}
	elsif (/^<prog>/)
	{	print SLIDE "<p><table cellpadding='8'><tr><td valign='middle' bgcolor='#DDDDDD'><pre>\n";
	}
	elsif (/^<sprogram>/ || /^<ssession>/)
	{	print SLIDE "<p><center><table cellpadding='8'><tr><td valign='middle' bgcolor='#DDDDDD'><pre class='smaller'>";
	}
	elsif (/^<\/program>/ || /^<\/session>/ || /^<\/syntax>/)
	{	print SLIDE "</pre></td></tr></table></center><p>\n";
	}
	elsif (/^<\/prog>/)
	{	print SLIDE "</pre></td></tr></table><p>\n";
	}
	elsif (/^<\/sprogram>/ || /^<\/ssession>/)
	{	print SLIDE "</pre></td></tr></table></center><p>\n";
	}
	elsif (/^<deftable>/ || /^<deftable [0-9]>/)
	{	print SLIDE "<p><center><table border='0' cellpadding='12'>\n";
		$indeftable = 1;
	}
	elsif (/^<reltable [0-9]>/)
	{	print SLIDE "<p><center><table border=1 cellpadding=4>\n";
		$inreltable = 1;
	}
	elsif (/^<table ([0-9])>/)
	{	print SLIDE "<table border=0 cellpadding=6>\n";
		$intable = 1; $ncols = $1;
	}
	elsif (/^<\/reltable>/)
	{	print SLIDE "</table></center><p>\n";
		$inreltable = 0;
	}
	elsif (/^<\/deftable>/)
	{	print SLIDE "</table></center><p>\n";
		$indeftable = 0;
	}
	elsif (/^<\/table>/)
	{	print SLIDE "</table>\n";
		$intable = 0; $ncols = 0;
	}
	elsif (/^<url /)
	{
		($url = $_) =~ s/<url //;
		$url =~ s/>.*$//;
		($txt = $_) =~ s/^<url [^>]*>//;
		&line_filter($txt);
		print SLIDE "<a target=\"demo\" href=\"http://$url\">$txt</a>\n";
	}
	else {
		if ($inslide == 1)
		{
			$line = $_;
			&line_filter($line);
			&slide_filter($line);
			print SLIDE "$line\n";
		}
	}
}

#
# Finish off the index file and the notes file
#

print INDEX "</ul></small>\n</td></tr></table>\n";
print INDEX "<br><small><small>Produced: $date</small></small></center>\n";
print INDEX "</body>\n</html>\n";
close(INDEX);
