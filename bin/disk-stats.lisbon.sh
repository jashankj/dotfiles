#!/bin/sh
# NAME: disk-stats.lisbon --- get I/O statistics on lisbon

i3-msg 'append_layout ~/.i3/layout.lisbon.disk-stats.json'

xfce4-terminal \
	--title='iostat' \
	--command='disk-stats.lisbon.iostat'

xfce4-terminal \
	--title='arcstat' \
	--command='disk-stats.lisbon.arcstat'

xfce4-terminal \
	--title='zpool-iostat' \
	--command='zpool-iostat-neat 1'
