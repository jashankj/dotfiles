#!/usr/bin/env zsh
# NAME: attachment --- describe a series of file attachments
# SYNOPSIS: attachment [file...]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

if [ $# -lt 1 ]
then
	usage
fi

file_size()
{
	case $(uname) in
		FreeBSD) stat -f %z $1 ;;
		Linux)   stat -c %s $1 ;;
		*) errx EX_OSERR "how do you stat(1) on $(uname)?" ;;
	esac
}

for f
do
	echo " - ${f} ($(file_size ${f}) bytes; $(file -bi ${f}))"
done
