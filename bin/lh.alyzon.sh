#!/bin/sh

#gt=gnome-terminal
gt=xfce4-terminal

########################################################################
lh_18s1() {
	W=/src/local/www/cse.jashankj.space
	S=/src/local/CS

	$gt --working-directory="$W/18s1/COMP3141"
#	i3-msg focus left && \
#		i3-msg resize shrink width 10 px or 10 ppt && \
#		i3-msg resize shrink width 10 px or 10 ppt && \
#		i3-msg focus right && i3-msg split v
	i3-msg split v && i3-msg layout tabbed
	i3-msg split v && i3-msg layout stacking
	$gt --working-directory="$S/18s1/3141"

	$gt --working-directory="$W/18s1/tCOMP1521"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt --working-directory="$S/18s1/t1521"

	$gt --working-directory="$W/18s1/COMP3311"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt --working-directory="$S/18s1/3311"

	$gt --working-directory="$W/18s1/COMP9243"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt --working-directory="$S/18s1/9243"

	$gt --working-directory="$W/18s1"
	i3-msg move right && i3-msg split v && i3-msg layout stacking
	$gt --working-directory="$S/18s1/t1511"
	$gt --working-directory="$S/18s1/hs"
}
lh_18s1
exit


########################################################################
lh_17s2() {
#    i3-msg split h
#    $gt --window	--working-directory="$HOME/LH/UNSW"

    $gt --window	--working-directory="$HOME/LH/UNSW/17s2/tCOMP1511"

    #read

#    i3-msg focus left && \
#        i3-msg resize shrink width 10 px or 10 ppt && \
#        i3-msg resize shrink width 10 px or 10 ppt && \
#        i3-msg focus right && i3-msg split v && i3-msg layout tabbed
    i3-msg split v && i3-msg layout stacking

    #read

    $gt --window	--working-directory="/src/CS/17s2/t1511"

#    $gt --window	--working-directory="$HOME/LH/UNSW/17s2/tCOMP3421"
#    i3-msg move right && i3-msg split v && i3-msg layout stacking
#    $gt --window	--working-directory="/src/CS/17s2/t3421"

    $gt --window	--working-directory="$HOME/LH/UNSW/17s2/COMP9242"
    i3-msg move right && i3-msg split v && i3-msg layout stacking
    $gt --window	--working-directory="/src/CS/17s2/9242"
}
lh_17s2
exit


########################################################################
lh_17s1() {
    i3-msg split h
    $gt --window	--working-directory="$HOME/LH/UNSW"

    $gt --window	--working-directory="$HOME/LH/UNSW/17s1/tCOMP1911"

    #read

    i3-msg focus left && \
        i3-msg resize shrink width 10 px or 10 ppt && \
        i3-msg resize shrink width 10 px or 10 ppt && \
        i3-msg focus right && i3-msg split v && i3-msg layout tabbed
    i3-msg split v && i3-msg layout stacking

    #read

    $gt --window	--working-directory="/src/CS/17s1/t1911"

    $gt --window	--working-directory="$HOME/LH/UNSW/17s1/COMP3891"
    $gt --window	--working-directory="/src/CS/17s1/3891"
    i3-msg move right && i3-msg split v && i3-msg layout stacking

    $gt --window	--working-directory="$HOME/LH/UNSW/17s1/COMP6841"
    $gt --window	--working-directory="/src/CS/17s1/6841"
    i3-msg move right && i3-msg split v && i3-msg layout stacking

    $gt --window	--working-directory="$HOME/LH/UNSW/17s1/COMP2911"
    $gt --window	--working-directory="/src/CS/17s1/2911"
    $gt --window	--working-directory="$HOME/LH/UNSW/17s1/pCOMP2911"
    $gt --window	--working-directory="/src/CS/17s1/p2911"
    i3-msg move right && i3-msg split v && i3-msg layout stacking
}
lh_17s1
exit
