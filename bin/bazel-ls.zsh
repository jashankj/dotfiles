#!/usr/bin/env zsh
# NAME: bazel-ls --- list matching build targets
# SYNOPSIS: bazel-ls (<type> <target>|<target>|<type>)

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

. "${ZDOTDIR}/.zsh/functions/bzl"

case "$#" in
	( 2 )	type="$1";	target="$2"	;;
	( 1 )
		case "$1" in
			( //* | :* )	target="$1" ;;
			( * )		type="$1" ;;
		esac
		;;
	( 0 ) ;;
	( * )
		usage
	;;
esac

: ${type:="binary"}
: ${target:="..."}

bzl query --output=label_kind "${target}" \
| awk -v "type=${type}" '$1 ~ type { print }' \
| sort
