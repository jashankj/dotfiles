#!/usr/bin/env ruby
# frozen_string_literal: true
# rubocop:disable Naming/AccessorMethodName
# rubocop:disable Naming/MethodParameterName
# rubocop:disable Style/GlobalVars
# typed: false

# NAME: radar --- show a BoM radar image set
# SYNOPSIS: radar

require 'net/ftp'
require 'uri'

require 'rubygems'
require 'gtk3'
require 'lightly'
require 'xdg'

require 'pry'

xdg = XDG::Environment.new

CACHE_LIFE_MOMENTARY = '6m'
CACHE_LIFE_NORMAL    = '14d'
CACHE_LIFE_FOREVER   = '365d'
CACHE_DIR            = File.join xdg.cache_home, 'bom-radar'
$CACHE = Lightly.new dir: CACHE_DIR, life: CACHE_LIFE_NORMAL

def with_path(uri, path)
  uri = uri.clone
  uri.path = path
  uri
end

$FTP = Net::FTP.new 'ftp.bom.gov.au'
$FTP.login
$FTPCWD = nil

def RETR(path) # rubocop:disable Naming/MethodName
  puts "ftp://ftp.bom.gov.au#{path}"
  dir  = File.dirname path
  base = File.basename path
  $FTP.chdir dir unless $FTPCWD == dir
  $FTPCWD = dir
  $FTP.getbinaryfile base, nil
end

RADAR_LAYERS = '/anon/gen/radar_transparencies/'
RADAR_IMAGES = '/anon/gen/radar/'

def get_static(it)
  $CACHE.get it do
    path = RADAR_LAYERS + it
    RETR path
  end
end

def get_image(it)
  $CACHE.get it do
    path = RADAR_IMAGES + it
    RETR path
  end
end

GLOBAL_LAYER_NAMES = %w[
  IDR.copyright.png
  IDR.legend.0.png
  IDR.legend.1.png
  IDR.legend.2.gif
  IDR.legend.2.png
].freeze

def get_global_layer_images
  $CACHE.life = CACHE_LIFE_FOREVER
  GLOBAL_LAYER_NAMES.each { |it| get_static it }
  $CACHE.life = CACHE_LIFE_NORMAL

  GLOBAL_LAYER_NAMES
end

LAYER_NAMES = %w[
  background
  topography
  rail
  roads
  waterways
  catchments
  wthrDistricts
  range
  locations
].freeze

LAYERS_BELOW = [:background, :topography].freeze
LAYERS_ABOVE = [:range, :locations].freeze

Layer = Struct.new('Layer', :key, :next) do |klass|
  def pixbuf
    @pixbuf ||= load_cached_image key
  end

  def _image_new
    loader = GdkPixbuf::PixbufLoader.new
    loader.write $CACHE.get self.key
    loader.close
    img = Gtk::Image.new
    img.name = self.key
    img.from_pixbuf = loader.pixbuf
    img
  end

  def image
    @image ||= self._image_new
  end

  def _widget_new
    o = Gtk::Overlay.new
    o.set_child self.image
    o.add_overlay self.next.widget unless self.next.nil?
    o
  end

  def widget
    @widget ||= self._widget_new
  end
end

COPYRIGHT_LAYER   = Layer.new 'IDR.copyright.png', nil
RAIN_LEGEND_LAYER = Layer.new 'IDR.legend.0.png', nil

TYPEMAP = {
  radar_512km: '1', # Broad Scale 512km x 512km Composite Radar Image
  radar_256km: '2', # Mid Scale 256km x 256km Radar Image
  radar_128km: '3', # Local Scale 128km x 128km Radar Image
  radar_64km:  '4', # Local Scale 64km x 64km
  rain_6m:     'A', # Rainfields -- 6 min
  rain_1h:     'B', # Rainfields -- 1 hour
  rain_9am:    'C', # Rainfields -- since 9am
  rain_24h:    'D', # Rainfields -- 24 hour
  velocity:    'I', # Doppler velocity
}

def get_image_list
  $CACHE.life = CACHE_LIFE_MOMENTARY
  list = $CACHE.get 'image-list' do
    $FTP.chdir RADAR_IMAGES
    $FTP.list
  end
  $CACHE.life = CACHE_LIFE_NORMAL
  list
end

Radar = Struct.new('Radar', :site, :type) do |klass|
  def layers_at(time)
    [
      COPYRIGHT_LAYER,
      RAIN_LEGEND_LAYER
    ] + self.static_layers_below + [
      self.layer_at(time)
    ] + self.static_layers_above
  end

  def latest_layers
    self.layers_at "T."+self.list_latest_image
  end

  # List images for this radar/type.
  def list_images
    get_image_list
      .map {|l| l.split(/\s+/).last }
      .filter {|x| x.start_with? "#{self.name}." and x.end_with? '.png' }
      .sort
      .map {|x| x.split('.', 4)[2] }
  end

  def list_latest_image
    self.list_images.last
  end

  def latest_image_layer
    self.layer_at "T."+self.list_latest_image
  end

  def latest_image
    self.image_at "T."+self.list_latest_image
  end

  def layer_at(time)
    image_name = self.image_name_at time
    get_image image_name
    Layer.new image_name, nil
  end

  def image_at(time)
    get_image self.image_name_at time
  end

  def image_name_at(time)
    self._image_name time
  end

  def static_layers_below
    LAYERS_BELOW.map {|layer| self.static_layer layer }
  end

  def static_layers_above
    LAYERS_ABOVE.map {|layer| self.static_layer layer }
  end

  def static_layer(layer)
    layer_name = self.static_layer_name layer
    get_static layer_name
    Layer.new layer_name, nil
  end

  def static_layer_images
    $CACHE.life = CACHE_LIFE_FOREVER
    self.static_layers {|it| get_static it }
    $CACHE.life = CACHE_LIFE_NORMAL
    self.static_layers
  end

  def static_layer_name(layer)
    @layers ||= {}
    @layers[layer] ||= self._image_name layer
    @layers[layer]
  end

  def static_layers
    LAYER_NAMES.map {|layer| self.static_layer layer }
  end

  def _image_name(layer)
    format '%s.%s.png', self.name, layer.to_s
  end

  def _name
    format 'IDR%02d%1c', self.site, TYPEMAP[self.type]
  end

  def name
    @name ||= self._name
  end
end

SITE  = 71
SCALE = :radar_256km

def init_radar(site, scale)
  get_global_layer_images
  Radar.new site, scale
end

def init_gui(app)
  window = Gtk::ApplicationWindow.new app
  window.set_title "Bureau of Meteorology / Radar"
  window.set_default_size 512, (512+32)

  radar = init_radar SITE, SCALE
  layers = radar.latest_layers
  stackup = nil
  layers.reverse.each do |layer|
    layer = layer.clone
    layer.next = stackup
    stackup = layer
  end

  viewbox = Gtk::Box.new Gtk::Orientation::VERTICAL, 0

  statusbar = Gtk::Statusbar.new

  viewbox.add stackup.widget
  viewbox.add statusbar

  window.add viewbox
  window.show_all
end

app = Gtk::Application.new 'au.com.rulingia.radar', :flags_none
app.signal_connect('activate') do |app|
  init_gui app
end
app.run
