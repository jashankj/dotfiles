#!/usr/bin/env zsh
# NAME: create-lxcbr.jaenelle --- set up bridge for LXC hosts
# SYNOPSIS: create-lxcbr

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

set -e

brctl addbr lxcbr
ip addr add 192.168.74.74/24 dev lxcbr
ip link set lxcbr up
iptables -t nat -A POSTROUTING -o lxcbr -j MASQUERADE
