#!/usr/bin/env zsh

pathdir="$(dirname "$(realpath "$0")")"
path=(${path:#${pathdir}})
exec \
nice -n20 \
"$(basename "$0")" "$@"
