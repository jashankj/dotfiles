#!/bin/sh

exec \
xfce4-terminal \
	--drop-down \
	--font="Source Code Pro 16" \
	--color-text=white \
	--color-bg=black \
	"$@"
