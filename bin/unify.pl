#!/usr/bin/env perl
# NAME: unify --- use unison to sync $PWD to somewhere else
# SYNOPSIS: unify

use v5;
use strict;
use warnings;
use feature 'say';
use feature 'signatures'; no warnings 'experimental::signatures';

use Cwd;
use File::Basename;
use File::Spec;
use Net::Domain 'hostfqdn';

$0 = basename $0;

my $ROOTS = {
	'/src/local' => {
		-ignore => [
			# Don't sync site artifacts.
			'Path {www/_sites}',
			'Path {www/_build}',
			'Name {.sass-cache}',
			# Don't sync object build artifacts.
			'Name {.stack-work}',
			'Path {ts/*/obj.*}',
			'Name {node_modules}',
			'Path {*/target/}',
		],
	},

	'/home/cs1511' => {
		-ignore => [
			'Name {_ve}',
			'Path {GOURCE}',
		],
	},

	'/home/cs1521' => {
		-ignore => [
			'Path {public_html/bin/python3}',
			'Path {public_html/ve}',
			'Path {media}',
			'Path {markroot*}',
		],
	},

	'/home/cs2041' => {
		-ignore => [
			'Path {public_html/ve}',
		],
	},

	'/home/cs2521' => {
		-ignore => [
			'Path {old-hunts}',
		],
	},

	'/home/cs3231' => {},

	'/home/jashank/Pictures/Screenshots' => {},
	'/home/jashank/Finance' => {},
	'/home/jashank/.cargo/registry' => {
		-times => undef,
	},
	'/home/jashank/.gem/cache' => {},
	'/home/jashank/.m2' => {},
};

sub find_root($wd, $pwd) {
	# if it's a root we know about, we're interested,
	# otherwise try going up a directory and seeing if that's a root.
	while ($wd ne '/') {
		return $wd if exists $ROOTS->{$wd};
		$wd = dirname $wd;
	}

	# if we can't find a root at all, give up
	die "$0: can't sync $pwd: no root\n" if $wd eq '/';
}

sub find_remote($hostname) {
	if ($hostname =~ /\Aalyzon\./) {
		return 'lisbon.rulingia.com.au'
	} elsif ($hostname =~ /\Ajaenelle\./) {
		return 'alyzon.rulingia.com.au'
		  if (`netctl is-active 507.co` =~ /\Aactive/);
		return 'alyzon-vpn.ri';
	} elsif ($hostname =~ /\Alisbon\./) {
		return 'alyzon.rulingia.com.au'
		  if (`nmcli connection show --active` =~ /507\.co/);
		return 'alyzon-vpn.ri';
	} else {
		die "$0: don't know how to sync on $hostname";
	}
}

sub merge_flags($global_flags, $root_flags) {
	foreach my $key (keys %$root_flags) {
		next unless exists $root_flags->{$key};
		my $opt = $root_flags->{$key};
		if (not defined $opt) {
			@$global_flags = grep { $_ ne $key } @$global_flags;
		} elsif (ref $opt eq 'ARRAY') {
			push @$global_flags, $key, $_ foreach @$opt;
		} else {
			push @$global_flags, $key, $opt;
		}
	}

	return $global_flags;
}

sub unify_one($wd, $pwd) {
	$wd = find_root($wd, $pwd);

	my $root_opts = $ROOTS->{$wd};

	# use FQDNs as Unison's hostnames
	my $hostname
		= (exists $ENV{'UNISONLOCALHOSTNAME'})
		? $ENV{'UNISONLOCALHOSTNAME'}
		: hostfqdn();
	$ENV{'UNISONLOCALHOSTNAME'} = $hostname;

	my $remote = find_remote($hostname);

	my @flags = (
		'-ui', 'text',				# Not interested in GUI.
		'-times',				# Sync modification times.
		'-ignore', 'Name {_ve.*-*}',
		'-ignore', 'Name {obj.*-*}',
		'-ignore', 'Name {.stack-work}',
	);

	# Run in batch mode if there's no terminal.
	push @flags, '-batch' if -t STDOUT;

	merge_flags \@flags, $root_opts;

	$remote = sprintf 'ssh://jashank@%s/%s', $remote, $wd;
	exec 'unison', @flags, $wd, $remote or
	exec 'unison-text', @flags, $wd, $remote or
	die "hm, can't find a Unison executable.";
}

sub unify_all() {
	my @roots = sort { $a cmp $b } keys %$ROOTS;
	foreach my $root (@roots) {
		say "\n[$root] unify";
		my $kid;
		if (not defined ($kid = fork)) {
			die "$0: fork: $!";
		} elsif ($kid == 0) {
			unify_one($root, $root)
		} else {
			1 while (waitpid($kid, 0) > 0);
		}
	}
}

unify_one(getcwd, $ENV{'PWD'}) if (@ARGV == 0);

# |@ARGV| >= 1
my $arg;
while ($arg = shift @ARGV) {
	if ($arg =~ /\A(-a|--all)\Z/) {
		unify_all;
	}
}
