#!/usr/bin/env zsh
# NAME: email --- start my email Emacs session
# SYNOPSIS: email

exec with-emacs-session mail "$@"
