#!/usr/bin/env zsh

rev_date="${1-24MAR2022}"
current_or_pending="${2-current}"

aip=https://www.airservicesaustralia.com/aip/${current_or_pending}

cd ~/Downloads/KEEP/AIP-AU/"${rev_date}"

urls_of_charts() {
	for chart in \
		erch/erch{1..5}_${rev_date}.pdf \
		ercl/ercl{1..8}_${rev_date}.pdf \
		sgfg/SGFG_${rev_date}.pdf \
		pca/PCA_{back,front}_${rev_date}.pdf \
		tac/tac{1..8}_${rev_date}.pdf
	do
		printf "%s/aipchart/%s\n" \
			"${aip}" "${chart}"
	done
}

urls_of_vnc_charts() {
	for vnc in \
		Adelaide \
		Brisbane \
		Bundaberg \
		Cairns \
		Darwin \
		Deniliquin \
		Hobart \
		Launceston \
		Melbourne \
		Newcastle \
		Perth \
		Rockhampton \
		Sydney \
		Tindal \
		Townsville
	do
		printf "%s/aipchart/vnc/%s_VNC_%s.pdf\n" \
			"${aip}" "${vnc}" "${rev_date}"
	done
}

urls_of_vtc_charts() {
	for vtc in \
		Adelaide \
		Albury \
		AliceSprings_Uluru \
		Brisbane_Sunshine \
		Broome \
		Cairns \
		Canberra \
		Coffs_Harbour \
		Darwin \
		Gold_Coast \
		Hobart \
		Horn_Island \
		Karratha \
		Launceston \
		Mackay \
		Melbourne \
		Mildura \
		Newcastle_Williamtown \
		Oakey_Brisbane \
		perth_legend \
		Perth \
		Rockhampton \
		Sydney \
		Tamworth \
		Townsville \
		Whitsunday
	do
		printf "%s/aipchart/vtc/%s_VTC_%s.pdf\n" \
			"${aip}" "${vtc}" "${rev_date}"
	done
}

construct_dap_url() {
	gawk --assign="aip=${aip}" -- '{ print aip "/dap/" $1 }'
}

extract_dap_files() {
	perl -ne 'if (/<a href="([^"]+.pdf)">/) { print $1, "\n" }' |
	sort |
	uniq
}

fetch() {
	for toc in SpecNotMan Checklist LegendInfoTables AeroProcCharts
	do
		printf "%s/dap/%sTOC.htm\n" "${aip}" "${toc}"
	done | xargs wget

	{
		printf "%s/aip/complete_%s.pdf\n" "${aip}" "${rev_date}"
		urls_of_charts
		urls_of_vnc_charts
		urls_of_vtc_charts

		cat SpecNotManTOC.htm       | extract_dap_files | construct_dap_url
		cat ChecklistTOC.htm        | extract_dap_files | construct_dap_url
		cat LegendInfoTablesTOC.htm | extract_dap_files | construct_dap_url
		cat AeroProcChartsTOC.htm   | extract_dap_files | grep '^SSY' | construct_dap_url

	} | xargs wget
}

fetch

# wget \
# 	--mirror \
# 	--no-parent \
# 	--cut-dirs=4 \
# 	--continue \
# 	${aip}/dap/SpecNotManTOC.htm
