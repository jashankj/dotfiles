#!/bin/sh
# NAME: objexamine --- dump out an executable to explore
# SYNOPSIS: objexamine [object]

exec "${llvm_objdump:=llvm-objdump}" \
	--all-headers \
	--syms \
	--disassemble \
	--source \
	--demangle \
	"$@"
