#!/usr/bin/env zsh
# NAME: um4l --- render a UML diagram to a PDF
# SYNOPSIS: um4l <file.um4l>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

if [ "X$1" = "X" ]
then
	usage
fi

m4 ${__libdir__}/m4/uml.dot.m4 "$1" | dot -Tpdf -o "${1%.um4l}.pdf"
