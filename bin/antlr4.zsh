#!/usr/bin/env zsh
# NAME: antlr4, antlr4rig --- run antlr4
# SYNOPSIS: antlr4 <...>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

JAVAFLAGS=(-Xmx512m -Xms8m)

case "$0" in
*/antlr4 | antlr4)
	c=org.antlr.v4.Tool ;;
*/antlr4rig | antlr4rig)
	c=org.antlr.v4.gui.TestRig ;;
*/antlr4javac | antlr4javac)
	JAVAFLAGS+=(--add-modules ALL-DEFAULT -m)
	c=jdk.compiler/com.sun.tools.javac.Main ;;
*)
	usage ;;
esac

function _antlr4_classpath() {
	{
		m2get org.antlr antlr4         4.13.1
		m2get org.antlr antlr4-runtime 4.13.1
		m2get org.antlr antlr-runtime  3.5.3
		m2get org.antlr ST4            4.3.4
		m2get org.abego.treelayout org.abego.treelayout.core 1.0.3
		m2get org.glassfish javax.json 1.0.4
		m2get com.ibm.icu   icu4j      72.1
	} | tr '\n' ':'
}

exec \
env \
	CLASSPATH=${CLASSPATH}:$(_antlr4_classpath) \
java "${JAVAFLAGS[@]}" \
	"$c" \
	"$@"
