#!/usr/bin/env zsh
# NAME: disk-stats.lisbon.arcstat --- run arcstat on lisbon

fields=(
	time
	read
	miss miss%
	dread
	dmis dm% pmis pm% mmis mm%
	mfu mru mfug mrug
	eskip mtxmis
	size
	c
	avail
)

exec arcstat -f ${(j.,.)fields} 1
