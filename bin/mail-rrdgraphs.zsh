#!/usr/bin/env zsh
# NAME: mail-rrdgraphs --- generate RRDtool graphs from mail counts
# SYNOPSIS: rrdgraphs

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

graph_duration=${1-14d}

typeset -A solarized
solarized=(
	[base03]=002b36
	[base02]=073642
	[base01]=586e75
	[base00]=657b83
	[base0]=839496
	[base1]=93a1a1
	[base2]=eee8d5
	[base3]=fdf6e3
	[yellow]=b58900
	[orange]=cb4b16
	[red]=dc322f
	[magenta]=d33682
	[violet]=6c71c4
	[blue]=268bd2
	[cyan]=2aa198
	[green]=859900
)

typeset -a repos
repos=($(ccat ~/etc/mail.repos))

typeset -A colors
colors=(
	[conglomerate]=${solarized[yellow]}
	[gmail]=${solarized[blue]}
	[csiro]=${solarized[green]}
	[emeralfel]=${solarized[magenta]}
	[unsw]=${solarized[yellow]}
)

typeset -A names
names=(
	[conglomerate]=old
	[emeralfel]=RI
	[gmail]=Gmail
	[unsw]=UNSW
	[csiro]=CSIRO
)

color_back=${solarized[base02]}
color_canvas=${solarized[base03]}
color_shadea=${solarized[base03]}
color_shadeb=${solarized[base03]}
color_axis=${solarized[base1]}
color_frame=${solarized[base1]}
color_grid=${solarized[base01]}
color_mgrid=${solarized[base01]}
color_font=${solarized[base0]}

typeset -a rrdgraph_flags
rrdgraph_flags=(
	--start "now - ${graph_duration}"
	--end   "now"
#	--lower-limit 0
	--vertical-label "messages"
	--color BACK#${color_back}
	--color CANVAS#${color_canvas}
	--color SHADEA#${color_shadea}
	--color SHADEB#${color_shadeb}
	--color AXIS#${color_axis}
	--color FRAME#${color_frame}
	--color GRID#${color_grid}
	--color MGRID#${color_mgrid}
	--color FONT#${color_font}
)

typeset -a combined_graph
combined_graph=(
	COMMENT:"$(printf "%-16s%11s%10s%10s%10s" '' 'Cur.' 'Min.' 'Avg.' 'Max.')\n"
)

for repo in ${repos}
do
	echo -n "${repo}... "
	rrd_db=".rrd.${repo}"
	name="${names[${repo}]}"
	color="${colors[${repo}]}"
	rrdtool graph "${repo}.png" \
		${rrdgraph_flags} \
		--width 512 --height 192 \
		--title "jashank's email counts\n${name} inbox, last ${graph_duration}" \
		DEF:"messages_${repo}_avg=${rrd_db}:messages:AVERAGE" \
		DEF:"messages_${repo}_max=${rrd_db}:messages:MAX" \
		DEF:"messages_${repo}_min=${rrd_db}:messages:MIN" \
		COMMENT:"$(printf "%-16s %10s%10s%10s%10s" '' 'Cur.' 'Min.' 'Avg.' 'Max.')\n" \
		AREA:"messages_${repo}_avg#${color}:$(printf "%-16s" ${name})" \
		GPRINT:"messages_${repo}_avg:LAST:%6.0lf\t" \
		GPRINT:"messages_${repo}_min:MIN:%6.0lf\t" \
		GPRINT:"messages_${repo}_avg:AVERAGE:%6.0lf\t" \
		GPRINT:"messages_${repo}_max:MAX:%6.0lf\t" \
		COMMENT:"\n"

	[ "$repo" = conglomerate ] && continue
	[ "$repo" = csiro ] && continue
	combined_graph+=(
		DEF:"messages_${repo}_avg=${rrd_db}:messages:AVERAGE"
		DEF:"messages_${repo}_max=${rrd_db}:messages:MAX"
		DEF:"messages_${repo}_min=${rrd_db}:messages:MIN"
		AREA:"messages_${repo}_avg#${color}:$(printf "%-16s" ${name}):STACK"
		GPRINT:"messages_${repo}_avg:LAST:%6.0lf\t"
		GPRINT:"messages_${repo}_min:MIN:%6.0lf\t"
		GPRINT:"messages_${repo}_avg:AVERAGE:%6.0lf\t"
		GPRINT:"messages_${repo}_max:MAX:%6.0lf\t"
		COMMENT:"\n"
	)
done

echo -n "combined... "
rrdtool graph "counts.png" \
	${rrdgraph_flags} \
	--lower-limit 0 \
	--width 640 --height 256 \
	--title "jashank's email counts\ncombined inbox, last ${graph_duration}\n" \
	${combined_graph}

rsync -a *.png *.html /www/apache24/data/counts/
