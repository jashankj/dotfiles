#!/usr/bin/env zsh
# NAME: pacman-sizeof --- calculate entire footprint of a package
# SYNOPSIS: pacman-sizeof <package-name>...

pacman_repo=/var/lib/pacman/local

cd ${pacman_repo}
pacman-depends "$@" \
| xargs pacman -Q \
| tr ' ' '-' \
| sed 's@$@/desc@' \
| xargs grep -h -A 1 '%SIZE%' \
| grep '[[:digit:]]' \
| datamash sum 1
