#!/bin/sh
# Plan 9 wrapper: add $PATH, $MANPATH.

[ -z "${PLAN9}" ] && exit 64 # EX_USAGE
export PATH="$PLAN9/bin":"$PATH"
export MANPATH="$PLAN9/man":"$MANPATH"
exec "$@"
