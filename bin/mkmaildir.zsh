#!/usr/bin/env zsh
# NAME: mkmaildir --- make a new Maildir
# SYNOPSIS: mkmaildir <dirname>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 1 ] || usage

umask 077
exec mkdir ${1}{,/{cur,new,tmp}}
