#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: strict

require 'socket'
socket = File.join(ENV['HOME'], '.cache', 'licenseid.sock')
socket = UNIXSocket.new socket
socket.write ARGF.read
socket.close_write
puts socket.read
socket.close
