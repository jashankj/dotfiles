#!/usr/bin/env zsh
# NAME: mk-mutt-mailboxes --- update Mutt mailboxes list
# SYNOPSIS: mk-mutt-mailboxes

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

mailboxes=~/.mutt.mailboxes

exec > ${mailboxes}

echo "unmailboxes *"
#echo "mailboxes =conglomerate"
echo "mailboxes =csiro"
echo "mailboxes =emeralfel"
echo "mailboxes =gmail"
echo "mailboxes =unsw"

(cd ~/Maildir && \
 ls -a \
 | egrep -v '^.{1,2}$' \
 | egrep -v '^.conglomerate' \
 | egrep -v '\[' \
 | sed -e 's/^./mailboxes =/')
