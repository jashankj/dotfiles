#!/usr/bin/env zsh
# NAME: ghcup-of --- use `ghcup' to update a package.
# SYNOPSIS: ghcup-of (ghc|cabal|hls|stack)

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 1 ] || usage

set -e

WHAT="$1"
OLDV="$(
	ghcup list --raw-format --show-criteria installed \
	| grep "^$WHAT" \
	| awk '{ print $2 }'
)"

ghcup install "$WHAT" recommended
ghcup rm      "$WHAT" "$OLDV"
