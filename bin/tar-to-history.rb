#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: false

# tar-to-history: convert a collection of tarballs to a Git repo.
#
# 2021-12-04	Jashank Jeremy <jashank.jeremy@unsw.edu.au>
#     Initial implementation; clean up for addition to Git.
# 2021-12-11	Jashank Jeremy <jashank.jeremy@unsw.edu.au>
#     Improve support for tarballs with bad permissions.
# 2021-12-15	Jashank Jeremy <jashank.jeremy@unsw.edu.au>
#     Add extension stripping for things other than tarballs.
#     Add conversion for CRLF line endings.
# 2025-01-18	Jashank Jeremy <jashank at rulingia.com.au>
#     Substantial refactoring.
#     Support excluding files (e.g., Autotools noise).

require 'optparse'
require 'set'

require 'rubygems'
require 'ffi-libarchive'
require 'rugged'

########################################################################

opts = {}
OptionParser.new do |p|
  p.banner = 'Usage: tar-to-history [options] <git-repo> [<tarball>...]'

  p.on('--author-name=NAME', 'Git author name') do |n|
    opts[:author_name] = n
  end

  p.on('--author-email=EMAIL', 'Git author email') do |n|
    opts[:author_email] = n
  end

  p.on('--committer-name=NAME', 'Git committer name') do |n|
    opts[:committer_name] = n
  end

  p.on('--committer-email=EMAIL', 'Git committer email') do |n|
    opts[:committer_email] = n
  end

  p.on('--message=MESSAGE', 'message template') do |n|
    opts[:message] = n
  end

  p.on('--dos2unix', 'replace CRLF line endings with LF line endings') do |n|
    opts[:dos2unix] = n
  end

  p.on('--create-tags', 'generate Git tags') do |v|
    opts[:create_tags] = v
  end

  p.on('--exclude=REGEXP', Regexp, 'pattern matching files to reject') do |v|
    opts[:exclude] = v
  end
end.parse!

########################################################################

AUTHOR_IDENT = {
  name:  opts[:author_name]  || opts[:committer_name]  || nil,
  email: opts[:author_email] || opts[:committer_email] || nil
}.freeze

COMMITTER_IDENT = {
  name:  opts[:committer_name]  || opts[:author_name]  || nil,
  email: opts[:committer_email] || opts[:author_email] || nil
}.freeze

########################################################################

def stat(msg)
  $stdout.puts msg; return
  $stdout.write "\033[2K\r"
  $stdout.write msg
  $stdout.flush
end

########################################################################

UP, UR, UW, UX = 0o700, 0o400, 0o200, 0o100
GP, GR, GW, GX = 0o070, 0o040, 0o020, 0o010
OP, OR, OW, OX = 0o007, 0o004, 0o002, 0o001

# Fix up weird permissions --- aim for something like 0644 or 0755.
def fix_mode(mode)
  fmt_bits = mode & ~(UP|GP|OP)
  perm_bits =
    # (UR | UW | UX | GR | GW | GX | OR | OW | OX)
    if mode & UX == UX
      (UR | UW | UX | GR      | GX | OR      | OX)
    else
      (UR | UW      | GR      | OR)
    end
  fmt_bits | perm_bits
end

########################################################################

STRIP_EXTENSIONS = %w[
  .tar
  .tar.z    .tz
  .tar.gz   .tgz
  .tar.bz2  .tbz .tbz2
  .tar.xz   .txz
  .tar.lzma
  .tar.lz4
  .tar.lzo
  .tar.zstd
  .tar.br   .tbr
  .pax .cpio .shar .iso .zip .a .7z .cab .lha .rar .warc .xar
].freeze

def strip_extension(file)
  STRIP_EXTENSIONS.each do |ext|
    return File.basename file, ext if file.downcase.end_with? ext
  end
  file
end

########################################################################

BINARY_EXTENSIONS = %w[
  .bin .lib .dll .exe
  .ico .png
  .img
].freeze

# really, really bad "binary-ness" check.
def maybe_binary?(file)
  BINARY_EXTENSIONS.any? { |ext| file.downcase.end_with? ext }
end

########################################################################

def main(opts, args)
  repo_dir = args.shift
  tarballs = args

  repo =
    if Dir.exist?(repo_dir)
      Rugged::Repository.new     repo_dir
    else
      Rugged::Repository.init_at repo_dir, true
    end

  # Load any existing tree.
  index = repo.index
  index.read_tree(repo.head.target.tree) unless repo.empty?

  # Load tag set.
  tags = repo.tags

  tarballs.each do |tarball|
    puts "#{tarball}"
    prefix           = strip_extension tarball
    project, version = prefix.split('-', 2)
    archive          = Archive::Reader.open_filename(tarball)

    this_set = Set[]
    that_set = index.entries.to_set { |e| e[:path] }

    # Use the topmost directory modification time for the commit timestamp.
    firste      = archive.next_header
    first_mtime = firste.mtime

    archive.each_entry do |entry|
      next if entry.directory?

      pathname = entry.pathname
      pathsegs = pathname.split('/')
      raise unless pathsegs.length > 1

      path = pathsegs[1..].join('/')

      filedata = archive.read_data
      filedata = '' if filedata == 0
      raise unless entry.size == filedata.length

      # Really, really bad CRLF -> LF conversion.
      filedata.gsub! "\015\012", "\n" if opts[:dos2unix] && !maybe_binary?(path)

      if opts[:exclude]&.match?(path)
        stat "  . #{path}"
        next
      end

      stat "  + #{path}"

      file_oid = repo.write(filedata, :blob)

      mode = nil
      if entry.symbolic_link?
        mode     = 0o120000
        filedata = entry.symlink
      else
        mode = fix_mode entry.mode
      end

      index_opts = {
        oid:   file_oid,
        path:  path,
        mode:  mode,
        ctime: entry.ctime,
        mtime: entry.mtime,
        size:  entry.size,
        uid:   entry.uid,
        gid:   entry.gid,
      }

      index.add index_opts
      this_set << path
    end

    # Remove entries now gone.
    (that_set - this_set).each do |e|
      stat "  - #{e}"
      index.remove e
    end

    added   = (this_set - that_set).length
    removed = (that_set - this_set).length
    stat "  +#{added} -#{removed}"
    puts

    tree_oid = index.write_tree(repo)
    puts "  : tree #{tree_oid}"

    message =
      opts[:message]
        .gsub('{prefix}', prefix)
        .gsub('{project}', project)
        .gsub('{version}', version)
        .gsub('{tarball}', tarball)

    commit_opts = {
      tree:       tree_oid,
      author:     AUTHOR_IDENT   .merge(time: first_mtime),
      committer:  COMMITTER_IDENT.merge(time: first_mtime),
      message:    message,
      parents:    repo.empty? ? [] : [repo.head.target].compact,
      update_ref: 'HEAD'
    }
    commit_oid = Rugged::Commit.create repo, commit_opts
    puts "  : commit #{commit_oid}"

    if opts[:create_tags]
      tag = tags.create(prefix, commit_oid)
      puts "  : tagged #{prefix}"
    end

    archive.close
    # system "git -C #{repo_dir} gc"
    # system "git -C #{repo_dir} repack -d --no-write-bitmap-index"
  end
end

main(opts, ARGV)
