#!/bin/sh

if [ "X$1" = "X" ]
then
	echo "usage: $(basename $0) (alyzon_left|alyzon_right|jaenelle|menolly|wedge)"
	exit 64 #EX_USAGE
fi
what="$1"

echo '{"version":1}['
while true
do
	echo "$what" \
	| socat unix-connect:$HOME/.cache/statusbard.sock stdio
	# | socat tcp:localhost:2018 stdio
	# | nc -U ~/.cache/statusbard.sock
	# | nc localhost 2018
	# | json2yaml; echo
	sleep 1
done
echo ']'
