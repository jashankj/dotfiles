#!/usr/bin/env perl

=encoding utf8

=head1 NAME

bazel-gen-target-list -- generate a map of headers to targets

=head1 SYNOPSIS

  bazel-gen-target-list <bazel-query>

=head1 EXAMPLE

  % bazel-gen-target-list 'kind("cc_library", ... except //third_party/...)'

=head1 SEE ALSO

L<https://bazel.build/query/guide>

=cut

use v5;
use strict;
use warnings;

use IO::Handle;
use IPC::Open2;

use XML::LibXML;

die "must set environment \$BAZEL"
	unless exists $ENV{'BAZEL'};
my $bazel = $ENV{'BAZEL'};

my $pid = open2(
	my $out,
	my $in,
	$bazel, 'query', '--output=xml', @ARGV
);

# 'kind("cc_library", ... except //third_party/...)'

$in->close;
my $xml = XML::LibXML->load_xml(IO => $out);
$out->close;
waitpid($pid, 0);

my @nodes = $xml->findnodes('//rule/list[@name="hdrs"]/label');
for my $node (@nodes) {
	my $label = $node->findvalue('../../@name');
	# ==> "//base:base"
	my $header = $node->findvalue('./@value');
	# ==> "//base:io-funcs.h"
	$header =~ s@^//@@;
	# ==> "base:io-funcs.h"
	$header =~ s@:@/@g;
	# ==> "base/io-funcs.h"
	print "$header\t$label\n";
}
