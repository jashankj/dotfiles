#!/usr/bin/env zsh
# NAME: heldkeys --- report on control keys being held in X11
# SYNOPSIS: heldkeys

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"


for i in $(xinput list --id-only)
do
	xinput list-props ${i}
	xinput query-state ${i}
done | grep '=down'
