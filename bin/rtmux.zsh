#!/usr/bin/env zsh
# NAME: rtmux --- remote tmux wrapper
# SYNOPSIS: rtmux <host> [tmux-options...]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ -z $1 ] && usage

rhost="$1"
shift
tmuxcmd='tmux'

case "${rhost}" in
	zed)
		tmuxcmd='tmux -S /tmp/jashankj/tmux-1000/default'
		;;

	alyzon-vpn)
		rhost='alyzon-vpn.ri'
		;;

	cassy-vpn)
		rhost='-J alyzon-vpn.ri cassy'
		;;

	cse)
		rhost='cse-wi'
		tmuxcmd='bin.linux-i386/tmux -S /tmp/tmux-28159/default'
		;;
esac

printf "\033]0;rtmux!%s\a" "${rhost}"
ssh -A -t ${rhost} ${tmuxcmd} "$@"
