#!/usr/bin/env perl
# NAME: modparam --- set a linux kernel module parameter
#
#	# modparam zfs.zfs_txg_timeout
#	/sys/module/zfs/parameters/zfs_txg_timeout: 5
#	# modparam zfs.zfs_txg_timeout=60
#	/sys/module/zfs/parameters/zfs_txg_timeout: 5 -> 60
#	# modparam zfs.zfs_txg_timeout
#	/sys/module/zfs/parameters/zfs_txg_timeout: 60
#

use strict;
use warnings;
use Fcntl qw(SEEK_SET);

die "usage: modparam <mod>.<param>[=<value>]\n" unless @ARGV == 1;

sub mkf {
	my ($mod, $param) = @_;
	sprintf '/sys/module/%s/parameters/%s', $mod, ($param =~ s@\.@/@r)
}

my $x = shift @ARGV;
my ($f, $v);
my $mode = '<';
if ($x =~ m{ ^ ([^.]+) . ([^=]+) = (.+) $ }x) {
	$f = mkf($1, $2); $v = $3; $mode = '+<';
} elsif ($x =~ m{ ^ ([^.]+) . ([^=]+) $ }x) {
	$f = mkf($1, $2);
}

open my $rh, '<', $f or die "open $f: $!";
my $xv = '';
$rh->sysread($xv, 1024)		or die "read $f: $!";  # ... and hope it's big enough.
$rh->close 			or die "close $f: $!";

if ($mode eq '+<') {
	open my $wh, '>', $f	or die "open $f: $!";
	$wh->syswrite($v)	or die "write $f: \"$v\": $!";
	$wh->close		or die "close $f: $!";
}

chomp $xv;
printf "\%s: \%s\n", $f, ($mode eq '+<' ? "$xv -> $v" : $xv);
