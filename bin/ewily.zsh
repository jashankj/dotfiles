#!/usr/bin/env zsh
# NAME: ewily --- open an Emacs client onto ~/.wilyguide
# SYNOPSIS: eview <file>
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2023 Jashank Jeremy <jashank at rulingia dot com dot au>
#

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/emacs"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

. "${ZDOTDIR}/.zsh/functions/emacsc"

exec \
emacsc \
--suppress-output \
--create-frame \
	"${HOME?}/.wilyguide"

