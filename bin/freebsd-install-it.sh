#!/bin/sh

if [ $# != 1 ] && [ $# != 2 ] && [ $# != 3 ]
then
	echo "usage: install-it <tree> <config> [-jN]" >&2
	exit 64 #EX_USAGE
fi

tree=$1
config=$2
jn=${3--j$(getconf _NPROCESSORS_ONLN)}

ccat() {
	sed -E -e 's/#.*//g; /^[[:space:]]*$/d;'
}

treedir=/src/freebsd/src/${tree}
echo $tree | grep -q '^/' && \
	treedir=$tree

env -i \
env TERM=$TERM \
    PATH=/usr/bin:/usr/sbin:/bin:/sbin:. \
    MAKEOBJDIRPREFIX=/var/obj/${config} \
    make ${jn} -C ${treedir} \
	installkernel installworld \
	$(ccat < env.${config})

env -i \
env TERM=$TERM \
    PATH=/usr/bin:/usr/sbin:/bin:/sbin:. \
    $(ccat < env.${config}) \
    mergemaster -F -m ${treedir} \
      -D $(ccat < env.${config} | grep '^DESTDIR=' | cut -d= -f2)
