#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: true

require 'erb'
require 'yaml'

CONFIG_FILE = -'/root/master.passwd'
CONFIG      = YAML.load_file CONFIG_FILE, symbolize_names: true

FILES   = [:passwd, :shadow, :group, :gshadow]
DISTROS =
  begin
    root = 0; shadow = 42;
    {
      # -rw-r--r--. 1 root root 3273 Apr 11 16:11 /lisbon/arch/etc/passwd
      # -r--------. 1 root root 2632 Apr 11 16:08 /lisbon/arch/etc/shadow
      # -rw-r--r--. 1 root root 1479 Mar  9 18:01 /lisbon/arch/etc/group
      # -r--------. 1 root root 1314 Mar  9 18:01 /lisbon/arch/etc/gshadow
      arch: {
        passwd:   [ root, root, 0o644 ],
        shadow:   [ root, root, 0o400 ],
        group:    [ root, root, 0o644 ],
        gshadow:  [ root, root, 0o400 ],
        sysusers: [ root, root, 0o644 ],
      },

      # -rw-r--r--. 1 root root   1299 Apr  9 13:16 /lisbon/debian/etc/group
      # -rw-r-----. 1 root shadow 1066 Apr  9 13:16 /lisbon/debian/etc/gshadow
      # -rw-r--r--. 1 root root   3198 Apr 11 16:12 /lisbon/debian/etc/passwd
      # -rw-r-----. 1 root shadow 2632 Apr 11 16:08 /lisbon/debian/etc/shadow
      debian: {
        passwd:   [ root, root,   0o644 ],
        shadow:   [ root, shadow, 0o640 ],
        group:    [ root, root,   0o644 ],
        gshadow:  [ root, shadow, 0o640 ],
        sysusers: [ root, root,   0o644 ],
      },

      # -rw-r--r--. 1 root root  906 Apr 11 15:18 /lisbon/fedora/etc/group
      # ----------. 1 root root  740 Apr 11 15:18 /lisbon/fedora/etc/gshadow
      # -rw-r--r--. 1 root root 2879 Apr 11 16:11 /lisbon/fedora/etc/passwd
      # ----------. 1 root root 2632 Apr 11 16:08 /lisbon/fedora/etc/shadow
      fedora: {
        passwd:   [ root, root, 0o644 ],
        shadow:   [ root, root, 0o000 ],
        group:    [ root, root, 0o644 ],
        gshadow:  [ root, root, 0o000 ],
        sysusers: [ root, root, 0o644 ],
      },
    }
  end

NOLOGIN = {
  arch:    '/usr/bin/nologin',
  debian:  '/usr/sbin/nologin',
  fedora:  '/sbin/nologin',
  freebsd: '/usr/sbin/nologin',
}

$distro = :fedora

########################################################################

def hash_of(kvs, h = {})
  return h if kvs.respond_to? :empty? and kvs.empty?
  if kvs.respond_to? :size
    case kvs.size
    when 0               then return h
    when Float::INFINITY then raise ArgumentError.new("illegal operand")
    end
  end

  k, v = kvs.first
  h[k] ||= v
  hash_of(kvs.drop(1), h)
end

UNAME_TO_UID =
  begin
    hash_of(
      [CONFIG[:users], CONFIG[:system_users]].map do |users|
        users.map { |uname, user| [uname.to_sym, user[:uid]] }
      end.flatten.each_slice(2)
    )
  end

GNAME_TO_GID =
  begin
    hash_of(
      [CONFIG[:groups], CONFIG[:system_groups]].map do |groups|
        groups.map { |gname, gid| [gname.to_sym, gid] }
      end.flatten.each_slice(2)
    )
  end

UID_TO_UNAMES =
  begin
    map = {}
    [CONFIG[:users], CONFIG[:system_users]].each do |users|
      users.each do |uname, user|
        uid = user[:uid]
        map[uid] ||= []
        map[uid].append uname.to_sym
      end
    end
    map
  end

UID_TO_UNAME =
  UID_TO_UNAMES.map { |k,v| [k, v[0]] }.to_h

GID_TO_GNAMES =
  begin
    map = {}
    [CONFIG[:groups], CONFIG[:system_groups]].each do |groups|
      groups.each do |gname, gid|
        map[gid] ||= []
        map[gid].append gname.to_sym
      end
    end
    map
  end

GID_TO_GNAME =
  GID_TO_GNAMES.map { |k,v| [k, v[0]] }.to_h

GROUP_MEMBERS =
  begin
    groups = {}
    [CONFIG[:users], CONFIG[:system_users]].each do |users|
      users.each do |uname, u|
        members = (u[:memberof] || []).map(&:to_sym)
        primary = GID_TO_GNAME[u[:gid] || u[:uid]]
        members.unshift primary unless members.include? primary
        members.each do |g|
          groups[g] ||= []
          groups[g] << uname.to_sym
        end
      end
    end
    groups.map do |gname, gmembers|
      [gname, gmembers.uniq {|g| UNAME_TO_UID[g] }]
    end.to_h
  end


########################################################################

TEMPLATE_PASSWD = <<__EOF__
<%  for username, user in CONFIG[:users] -%>
<%=   user_to_passwd username, user %>
<%  end -%>
<%  for username, user in CONFIG[:system_users] -%>
<%=     user_to_passwd username, user %>
<%    end -%>
__EOF__
# <%    if user.key? $distro -%>
# <%    end -%>

def user_to_passwd(username, user)
  # user = user.merge(user[$distro]) if user.key? $distro
  # jashank:x:1000:1000:Jashank Jeremy:/home/jashank:/bin/zsh
  [
    username,
    'x',
    user[:uid],
    user[:gid]   || user[:uid],
    user[:gecos] || username,
    user[:home]  || '/nonexistent',
    user[:shell] || NOLOGIN[$distro]
  ].map(&:to_s).join(':')
end

########################################################################

TEMPLATE_SHADOW = <<__EOF__
<%  for username, user in CONFIG[:users] -%>
<%=   user_to_shadow username, user %>
<%  end -%>
<%  for username, user in CONFIG[:system_users] -%>
<%=   user_to_shadow username, user %>
<%  end -%>
__EOF__

def user_to_shadow(username, user)
  # avahi:!*:18617::::::
  change, age_min, age_max, warn, grace = user[:pass_dates]
  [
    username,
    user[:passwd] || '!*',
    change        || '',
    age_min       || '',
    age_max       || '',
    warn          || '',
    grace         || '',
    user[:expiry] || '',
                     ''
  ].map(&:to_s).join(':')
end

########################################################################

TEMPLATE_GROUP = <<__EOF__
<%  for groupname, gid in CONFIG[:groups] -%>
<%=   group_to_group groupname, gid %>
<%  end -%>
<%  for groupname, gid in CONFIG[:system_groups] -%>
<%=   group_to_group groupname, gid %>
<%  end -%>
__EOF__

def group_to_group(groupname, gid)
  # {group_name}:{password}:{GID}:{user_list}
  # daemon:x:1:
  [
    groupname,
    'x',
    gid,
    (GROUP_MEMBERS[groupname.to_sym] || []).join(',')
  ].map(&:to_s).join(':')
end

########################################################################

TEMPLATE_GSHADOW = <<__EOF__
<%  for groupname, gid in CONFIG[:groups] -%>
<%=   group_to_gshadow groupname, gid %>
<%  end -%>
<%  for groupname, gid in CONFIG[:system_groups] -%>
<%=   group_to_gshadow groupname, gid %>
<%  end -%>
__EOF__

def group_to_gshadow(groupname, gid)
  # {groupname}:{password}:{administrators}:{members}
  [
    groupname,
    '*',
    [].join(','),
    [].join(','),
  ].map(&:to_s).join(':')
end

########################################################################
#
# ``systemd-sysusers`` allocates system users and groups.
# Files in /etc/sysusers.d override shipped config files.
#
# Handy links:
#   file:/lib/sysusers.d/
#   file:/lisbon/arch/lib/sysusers.d/
#   file:/lisbon/debian/lib/sysusers.d/
#   file:/lisbon/fedora/lib/sysusers.d/
#
# Documentation:
#   man:systemd-sysusers(8)
#   man:sysusers.d(5)
#
# Config-Test:
#   $ systemd-analyze cat-config sysusers.d
#
# Quick-Reference:
#   ``u``  name  [id | ``-``]  gecos  [homedir | ``/``] [shell | ``/usr/sbin/nologin``]
#   ``g``  name  [id | ``-``]
#   ``m``  uname  gname...
#   ``r``  ``-``  lowest ``-`` highest
#

TEMPLATE_SYSUSERS_CONF = <<__EOF__
<%  for groupname, group in CONFIG[:system_groups] -%>
<%=   group_to_sysusers_conf groupname, group %>
<%  end -%>
<%  for username, user in CONFIG[:system_users] -%>
<%=   user_to_sysusers_conf username, user %>
<%  end -%>
__EOF__

def group_to_sysusers_conf(groupname, group)
  [
    'g',
    groupname,
    group # group[:gid]
  ].map(&:to_s).join("\t")
end

def user_to_sysusers_conf(username, user)
  uidgid =
    if user[:uid] and user[:gid] then
      format "%d:%d", user[:uid], user[:gid]
    elsif user[:uid] then
      format "%d", user[:uid]
    else
      '-'
    end
  result = [
    'u',
    username,
    uidgid,
    (user[:gecos] || username).to_s.inspect,
    user[:home],
    user[:shell] || ''
  ].map(&:to_s).join("\t")

  if user.include? :memberof
    result +=
      "\n" +
      [
        'm',
        username,
        *user[:memberof]
      ].map(&:to_s).join(' ')
  end

  result
end

########################################################################

def generate(file, output, template)
  template = ERB.new template, trim_mode: '-'
  output = File.join ENV['DESTDIR'], output if ENV.include? 'DESTDIR'
  uid, gid, perms = DISTROS[$distro][file]
  File.open(output, 'w') do |f|
    f.chown uid, gid
    f.chmod perms
    f.write template.result
  end
  real_output = output.delete_suffix '.new'
  puts sprintf(
         'install -m %03o -o %d -g %d %s %s',
         perms, uid, gid, output, real_output
       )
end

generate :passwd,   '/etc/passwd.new',  TEMPLATE_PASSWD
generate :shadow,   '/etc/shadow.new',  TEMPLATE_SHADOW
generate :group,    '/etc/group.new',   TEMPLATE_GROUP
generate :gshadow,  '/etc/gshadow.new', TEMPLATE_GSHADOW
# generate :sysusers, '/etc/sysusers.d/lisbon.conf.new', TEMPLATE_SYSUSERS_CONF
# generate :tmpfiles, '/etc/tmpfiles.d/lisbon.conf.new', TEMPLATE_TMPFILES_CONF

__END__

# `/root/master.passwd' looks like:

########################################################### -*- yaml -*-
#
# Using ``/root/mkpasswd``, this file is the source of truth for:
#   /etc/passwd
#   /etc/shadow
#   /etc/group
#   /etc/gshadow
#   /etc/sysusers.d/*.conf
#   (for user home directories, /etc/tmpfiles.d/*.conf)
#
# Schema:
#   users, system_users:
#
#     name          User's login name.
#     passwd        User's encrypted password.
#     uid           User's id.
#     gid           User's login group id.
#     pass_dates
#       change      Date of last password change
#       age_min     Minimum password age; empty/0 mean none
#       age_max     Maximum password age; empty/0 mean none
#       warn        Password warning period; empty/0 mean none
#       grace       Password inactivity period; empty mean none
#     expiry        Account expiry
#     gecos         General information about the user.
#     home          User's home directory.
#     shell         User's login shell.
#     memberof      List of groups
#
#   groups, system_groups:
#
#     group-name  =>  gid
#
#
#   XXX groups, system_groups:
#     name
#     passwd
#     gid
#     admins
#     members
#
# Generators:
#     ccat /etc/passwd \
#     | awk -F: '{
#             print $1 ":";
#             print "  passwd: " $2;
#             print "  uid: " $3;
#             if ($4 != $3)
#                     print "  gid: " $4;
#             if ($5 != $1)
#                     print "  gecos: " $5;
#             print "  home: " $6;
#             if ($7 !~ ".*/nologin")
#                     print "  shell: " $7;
#             print ""
#     }'
#     ccat /etc/shadow \
#     | awk -F: '{
#             print $1 ":";
#             print "  passwd: \"" $2 "\"";
#             print "  pass_dates: [" $3 ", " $4 ", " $5 ", " $6 ", " $7 "]";
#             print "  expiry: " $8;
#             print "";
#     }'
#
#     passwd[2] = "x", usually
#
########################################################################

users:
groups:
system_users:
system_groups:
