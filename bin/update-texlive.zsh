#!/usr/bin/env zsh

texlive=/usr/local/texlive

# CTAN mirrors:
ctan=https://mirror.ctan.org
#ctan=https://mirror.cse.unsw.edu.au/pub/CTAN
#ctan=https://mirror.aarnet.edu.au/pub/CTAN
#ctan=https://au.mirrors.cicku.me/ctan

case "$(hostname -s)" in
lisbon )
	tlmgr update \
		--repository="${ctan?}/systems/texlive/tlnet" \
		--self \
		--all
	;;

alyzon | cassy | jaenelle )
	rsync -Pai --delete lisbon:${texlive}/ ${texlive}
	;;
esac

# tail -n5 ~/bin/update-texlive | sh -ex

mktexlsr
updmap-sys --quiet --nohash
fmtutil-sys --all \
	| grep --line-buffered '^fmtutil \[INFO\]:'
mtxrun --generate
