#!/usr/bin/env zsh
# stolen with love from andrewt

wilybinary=$(which -a wily | grep -v 'bin/wily')

PATH="$HOME/lib/wily/bin:$HOME/lib/wily/html/bin:$PATH"
HISTORY=/dev/null
SHELL=$HOME/lib/wily/wily-sh
h="$HOME"

export wily_start_time=`date '+%s'`
export wily_process_id=$$

export HISTORY PATH WILYFIFO SHELL h
fn="-misc-fixed-medium-*-*-*-20-*-*-*-*-*-*-*"

case `whoami` in
root)
        files=(/etc /var/log);;
*)
        files=($HOME/.wilyguide .)
esac

wilyflags=(-c1 -p9font '' -p9fixed '' -fn "$fn" -rv)

trap '' HUP
case $1 in
/tmp/mutt*)
        exec setsid $wilybinary ${wilyflags} "$@" >/dev/null;;
/tmp/view*)
        exec setsid $wilybinary ${wilyflags} "$@" >/dev/null;;
*)
        exec setsid $wilybinary ${wilyflags} $files "$@"
        exec setsid $wilybinary ${wilyflags} $files "$@" >/dev/null </dev/null 2>&1;;
esac
