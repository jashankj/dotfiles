#!/usr/bin/env ruby
# frozen_string_literal: true
# rubocop:disable Layout/EmptyLineAfterGuardClause
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/BlockLength
# rubocop:disable Metrics/MethodLength
# rubocop:disable Naming/MethodParameterName
# rubocop:disable Style/FormatStringToken
# rubocop:disable Style/MethodDefParentheses
# rubocop:disable Style/SignalException
# typed: false

# NAME: age-q --- queries about the age of a file
#
# initially stolen from my journal logic, specifically within
#   $RI-SVN-Id: mk-weather-yssy 5250 2022-06-04 01:53:51Z jashank $
# and eventually rewritten in Ruby because Perl doesn't actually have
# enough support for time objects.

require 'time'
require 'optparse'

require 'rubygems'
require 'chronic'

def main args
  opts, args = getopt args
  case opts[:operation]
  when :newer, :older then compare       opts, *args
  when :epoch         then output_epoch  opts, *args
  when :format        then output_format opts, *args
  end
end

def get_timestamp opts, file
  case opts[:timestamp] || :mtime
  when :ctime then  File.ctime(file)
  when :mtime then  File.mtime(file)
  when :atime then  File.atime(file)
  end
end

def file_timestamp opts, f
  return get_timestamp opts, f if File.exist? f
  Chronic.parse(f, guess: true, context: :past) or
    fail "Could not parse #{f.inspect}"
end

def compare opts, a, b
  # puts "#{operation}: File(#{a.inspect}) <=> File(#{b.inspect})"
  ta, tb = [a, b].map { |f| file_timestamp opts, f }
  cmp = ta <=> tb
  exit (
    (opts[:operation] == :newer and cmp.positive?) or
    (opts[:operation] == :older and cmp.negative?)
  )
end

def output_epoch opts, f
  t = get_timestamp opts, f
  printf "%d.%09d\n", t.to_i, t.nsec
end

def output_format opts, f, fmt
  t = get_timestamp opts, f
  puts t.strftime fmt
end

def getopt(arguments)
  options = {}

  OptionParser.new do |parser|
    parser.banner = 'Usage: age-q [options]'
    parser.summary_width = 20

    parser.separator ''
    parser.separator 'Operations:'
    parser.on '--epoch',
              'Print timestamp in UNIX epoch-seconds',
              '(e.g., "age-q FILE --epoch")' do
      options[:operation] = :epoch
    end

    parser.on '--format', '--strftime',
              'Print timestamp with specified format',
              '(e.g., "age-q FILE --format \'%FT%T\'")' do
      options[:operation] = :format
    end

    parser.on '-o', '--older', '--older-than',
              'ARGS[0] is older-than ARGS[1]',
              '(e.g., "age-q FILE-A --older-than FILE-B")',
              '(e.g., "age-q FILE-A --older-than \'6 months ago\'")' do
      options[:operation] = :older
    end

    parser.on '-n', '--newer', '--newer-than',
              'ARGS[0] is newer-than ARGS[1]',
              '(e.g., "age-q FILE-A --newer-than FILE-B")',
              '(e.g., "age-q FILE-A --newer-than \'3 days ago\'")' do
      options[:operation] = :newer
    end

    parser.separator ''
    parser.separator 'Timestamp:'

    parser.on '--atime', '--access', 'Use last-file-access time' do
      options[:timestamp] = :atime
    end

    parser.on '--mtime', '--modify', 'Use last-data-modification time' do
      options[:timestamp] = :mtime
    end

    parser.on '--ctime', '--change', 'Use last-metadata-change time' do
      options[:timestamp] = :ctime
    end

    parser.separator ''
    parser.separator 'Common options:'
    parser.on '-h', '--help', 'Prints this help' do
      puts parser
      exit 64 # EX_USAGE
    end
  end.parse!(arguments)
  [options, arguments]
end

main(ARGV)
