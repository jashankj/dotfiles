#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: strict
# $Id: git--rcskw-expand 2425 2022-07-23 20:54:46 +1000 jashank@lisbon.rulingia.com.au $

require 'sorbet-runtime'; extend T::Sig

require 'stringio'
require 'logger'

require 'rubygems'
require 'rugged'

$LOG = Logger.new STDERR, level: Logger::INFO

module Pktline; end
module Handshake; end

########################################################################

module Pktline::Send
  extend T::Sig

  sig { params(io: IO, head: T.untyped, rest: T.untyped).void }
  def self._send io, head, rest = nil
    $LOG.debug "send: writing #{head.inspect}"; io.write head
    if rest
      $LOG.debug "send: writing  .... #{rest.inspect}"
      io.write rest
    end
    io.flush
  end

  sig { params(io: IO, bytes: T.untyped).void }
  def self.send io, bytes
    Pktline::Send._send io, format('%04x', 4 + bytes.length), bytes
  end

  sig { params(io: IO).void }
  def self.send_flush io
    $LOG.debug "send: writing flush-pkt"
    Pktline::Send._send io, '0000'
  end

  sig { params(io: IO).void }
  def self.send_delimiter io
    $LOG.debug "send: writing delim-pkt"
    Pktline::Send._send io, '0001'
  end

  sig { params(io: IO).void }
  def self.send_response_end io
    $LOG.debug "send: writing response-end-pkt"
    Pktline::Send._send io, '0002'
  end

  sig { params(io: IO, tag: String, val: String).void }
  def self.send_tagged io, tag, val
    Pktline::Send.send io, "#{tag}=#{val}"
  end

  sig { params(io: IO, lines: T::Array[String]).void }
  def self.send_lines io, lines
    lines.each { |line| Pktline::Send.send io, line }
  end
end

########################################################################

module Pktline::Recv
  extend T::Sig

  sig { params(io: IO).returns(T.any(Symbol, T.nilable(String))) }
  def self.recv io
    lenx = io.read(4);
    return nil if lenx.nil?
    fail "invalid #{lenx.inspect}" unless lenx =~ /^[[:xdigit:]]+$/i
    len = lenx.to_i(16)
    $LOG.debug "recv: #{lenx.inspect} => #{len}"
    case len
    when 0 then $LOG.debug "recv: flush-pkt";        :flush
    when 1 then $LOG.debug "recv: delim-pkt";        :delim
    when 2 then $LOG.debug "recv: response-end-pkt"; :response_end
    when 4 ... 65_516 then
      received = io.read(len - 4)
      fail "read failed" if received.nil?
      $LOG.debug "recv:  .... #{received.inspect}"
      received
    else fail "invalid packet header #{lenx.inspect}"
    end
  end

  sig { params(io: IO)
          .returns(T.nilable(T.any(Symbol, T::Array[String]))) }
  def self.recv_tagged io
    case (line = Pktline::Recv.recv io)
    when nil, Symbol then line
    else
      xtag, xvalue = line.split('=', 2)
      [T.must(xtag), T.must(xvalue)]
    end
  end

  ##
  # Receive lines until a ``:flush`` arrives.
  #
  sig { params(io: IO).returns(T::Array[String]) }
  def self.recv_lines io
    lines = []

    loop do
      case (line = Pktline::Recv.recv io)
      when nil    then fail "couldn't read from Git"
      when :flush then return lines
      else lines << T.cast(line, String)
      end
    end
  end
end

########################################################################

module Pktline
  extend T::Sig

  sig { void }
  def self._test
    [
      ["0006a\n",         "a\n"],
      ["0005a",           "a"],
      ["000bfoobar\n",    "foobar\n"],
      ["0004",            ""],
    ].each do |xy|
      input, expect = xy
      io = StringIO.new input, 'r'
      actual = Pktline::Recv.recv io
      fail unless expect == actual

      expect, input = xy
      io = StringIO.new
      Pktline::Send.send io, input
      actual = io.string
      fail unless expect == actual
    end
  end
end

########################################################################

module Handshake::Send
  extend T::Sig

  sig { params(io: IO, tag: String, vals: T::Array[String]).void }
  def self._send_tagged io, tag, vals
    vals.each { |val| Pktline::Send.send_tagged io, tag, val }
  end

  sig { params(io: IO, protocol: String, versions: T::Array[String])
          .void }
  def self.send_greeting io, protocol, versions
    Pktline::Send.send io, protocol
    Handshake::Send._send_tagged io, 'version', versions
    Pktline::Send.send_flush io
  end

  sig { params(io: IO, caps: T::Array[String]).void }
  def self.send_capabilities io, caps
    Handshake::Send._send_tagged io, 'capability', caps
    Pktline::Send.send_flush io
  end
end

########################################################################

module Handshake::Recv
  extend T::Sig

  sig { params(io: IO, tag: String).returns(T::Array[String]) }
  def self._recv_tagged_lines io, tag
    Pktline::Recv.recv_lines(io).map do |line|
      ltag, lval = line.chomp.split('=', 2)
      fail unless T.must(ltag) == tag
      T.must lval
    end
  end

  sig { params(io: IO).returns([String, T::Array[String]]) }
  def self.recv_greeting io
    protocol =
      case (protocol = Pktline::Recv.recv io)
      when nil    then fail "couldn't read from Git"
      when Symbol then fail "unexpected: #{protocol.inspect}"
      when String then protocol.chomp
      end
    versions = Handshake::Recv._recv_tagged_lines(io, 'version')
    [protocol, versions]
  end

  sig { params(io: IO).returns(T::Array[String]) }
  def self.recv_capabilities io
    Handshake::Recv._recv_tagged_lines(io, 'capability')
  end
end

########################################################################

module Filter
  extend T::Sig

  FilterFn = T.type_alias {
    T.proc
      .params(command: T::Hash[Symbol, String], content: T::Array[String])
      .returns([T::Hash[Symbol, String], T::Array[String]])
  }

  sig { params(
          io_in:   IO,
          io_out:  IO,
          smudger: FilterFn,
          cleaner: FilterFn,
        ).void }
  def self.main io_in, io_out, smudger, cleaner
    Filter.greet_git io_in, io_out

    loop do
      cmd = Filter.recv_command io_in
      command = T.must(cmd[:command]).chomp
      fail unless command == 'smudge' or command == 'clean'
      content = Filter.recv_content io_in

      result =
        case command
        when 'smudge' then smudger.call(cmd, content)
        when 'clean'  then cleaner.call(cmd, content)
        else fail "unknown command #{command.inspect}"
        end
      status, result = result
      Filter.send_result io_out, status, result
    end
  end

  sig { params(io_in: IO, io_out: IO).void }
  def self.greet_git io_in, io_out
    protocol, versions = Handshake::Recv.recv_greeting io_in
    fail unless protocol == 'git-filter-client'
    fail unless versions.include? '2'

    Handshake::Send.send_greeting io_out, 'git-filter-server', %w{ 2 }

    caps = Handshake::Recv.recv_capabilities io_in
    fail unless caps.include? 'clean'
    fail unless caps.include? 'smudge'

    Handshake::Send.send_capabilities io_out, %w{ clean smudge }
  end

  sig { params(io: IO).returns(T::Hash[Symbol, String]) }
  def self.recv_command io
    cmd = {}
    loop do
      line = Pktline::Recv.recv_tagged io
      exit 0 if line == nil
      return cmd if line == :flush
      fail "unexpected #{line.inspect}" \
        unless line.is_a? Array and line.length == 2
      tag, val = line
      cmd[T.must(tag).to_sym] = T.must(val).chomp
    end
  end

  sig { params(io: IO).returns(T::Array[String]) }
  def self.recv_content io
    Pktline::Recv.recv_lines io
  end

  sig { params(
          io: IO,
          status: T::Hash[Symbol, String],
          result: T::Array[String]
        ).void }
  def self.send_result io, status, result
    Pktline::Send.send_tagged io, 'status', T.must(status[:status])
    status.each do |k, v|
      Pktline::Send.send_tagged io, k.to_s, v unless k == :status
    end
    Pktline::Send.send_flush io

    Pktline::Send.send_lines io, result
    Pktline::Send.send_flush io
    Pktline::Send.send_flush io
  end
end

########################################################################

module RCSKeyword
  extend T::Sig

  KEYWORDS = T.let(%w{
    Author  LastChangedBy
    Date    LastChangedDate  AuthorDate
    Revision
    Blob
    Tree
    Commit
    Committer  LastCommittedBy
    CommitDate LastCommittedDate
    Id
    Header
  }, T::Array[String])

  KEYWORDS_RX   = T.let(Regexp.union(KEYWORDS), Regexp)
  UNEXPANDED_RX =
    T.let(%r{ \$ (?<KEYWORD> #{KEYWORDS_RX}) \$ }x, Regexp)
  EXPANDED_RX   =
    T.let(%r{ \$ (?<KEYWORD> #{KEYWORDS_RX}):\ [^$]+\$ }x, Regexp)
  MAYBE_EXP_RX  =
    T.let(Regexp.union(UNEXPANDED_RX, EXPANDED_RX), Regexp)

  $GIT = Rugged::Repository.discover Dir.pwd

  # Hash[hash: String, Hash[path: String, oid: String]]
  $CACHE = {}

  $CACHEREVS = {}

  ######################################################################

  sig { params(repo: Rugged::Repository, head: T.untyped, path: String)
          .returns(Rugged::Commit) }
  def self.last_commit_do repo, head, path
    commit = T.let(nil, T.nilable(Rugged::Commit))
    $LOG.debug "Walking in repo #{repo.path}, starting at #{head}."
    Rugged::Walker.walk repo, show: head, sort: Rugged::SORT_TOPO do |c|
      c = T.cast(c, Rugged::Commit)
      diffs = c.diff(paths: [path])
      next  if diffs.size == 0
      $LOG.debug "Changeset touching #{path}: #{c.oid}"
      commit = c; break
    end
    T.must(commit)
  end

  sig { params(repo: Rugged::Repository, head: T.untyped, path: String)
          .returns(Rugged::Commit) }
  def self.last_commit repo, head, path
    if $CACHE.key? head and $CACHE[head].key? path
      $LOG.debug("cache hit: #{head} #{path} => #{$CACHE[head][path]}")
      repo.lookup($CACHE[head][path])
    else
      $CACHE[head] ||= {}
      c = last_commit_do repo, head, path
      $CACHE[head][path] = c.oid
      $LOG.debug("cache miss: #{head} #{path} => #{$CACHE[head][path]}")
      c
    end
  end

  ######################################################################

  sig { params(repo: Rugged::Repository, head: T.untyped)
          .returns(Integer) }
  def self.count_revisions repo, head
    if $CACHEREVS.key? head
      $CACHEREVS[head]
    else
      $CACHEREVS[head] = count_revisions_do repo, head
    end
  end

  sig { params(repo: Rugged::Repository, head: T.untyped)
          .returns(Integer) }
  def self.count_revisions_do repo, head
    n = 0
    $LOG.debug "Walking in repo #{repo.path}, starting at #{head}."
    Rugged::Walker.walk repo, show: head, sort: Rugged::SORT_TOPO do |_|
      n += 1
    end
    n
  end

  ######################################################################

  sig { params(command: T::Hash[Symbol, String], kw: String)
          .returns(T.nilable(String)) }
  def self.expand_one command, kw
    pathname = T.must command[:pathname]
    ref      = command[:ref] || $GIT.head.name
    commit   = RCSKeyword.last_commit $GIT, ref, pathname

    get_oid_of   = ->(f) { commit.tree.path(f)[:oid] }
    fmt_user_id  = ->(x) { format '%s <%s>', x[:name], x[:email] }
    fmt_datetime = ->(x) { x[:time].strftime('%F %T %z (%a, %d %b %Y)') }

    value =
      case kw
      when 'Commit' then
        commit.oid
      when 'Tree' then
        case (dir = File.dirname pathname)
        when '.' then commit.tree.oid
        else get_oid_of[dir]
        end

      when 'Blob' then
        get_oid_of.call pathname

      when 'Committer',  'LastCommittedBy' then
        fmt_user_id.call commit.committer
      when 'CommitDate', 'LastCommittedDate' then
        fmt_datetime.call commit.committer
      when 'Author', 'LastChangedBy' then
        fmt_user_id.call commit.author
      when 'Date',   'LastChangedDate', 'AuthorDate' then
        fmt_datetime.call commit.author

      when 'Revision' then
        count_revisions($GIT, commit.oid).to_s

      when 'Id' then
        format '%s %d %s %s',
               File.basename(pathname),
               count_revisions($GIT, commit.oid).to_s,
               commit.author[:time].strftime('%F %T %z'),
               commit.author[:email]

      when 'Header' then
        format '//%s %d %s %s',
               pathname,
               count_revisions($GIT, commit.oid).to_s,
               commit.author[:time].strftime('%F %T %z'),
               commit.author[:email]

      else
        $LOG.warn "attempt to expand #{kw.inspect}"
        return "$#{kw}$"
      end

    format '$%s: %s $', kw, value
  end

  ######################################################################

  sig { params(command: T::Hash[Symbol, String], line: String)
          .returns(String) }
  def self.expand_line command, line
    line.gsub(MAYBE_EXP_RX) do |_|
      match = T.must Regexp.last_match
      RCSKeyword.expand_one command, T.must(match.named_captures['KEYWORD'])
    end
  end

  ######################################################################

  # Smudger:
  sig { params(command: T::Hash[Symbol, String], content: T::Array[String])
          .returns([T::Hash[Symbol, String], T::Array[String]]) }
  def self.expand command, content
    $LOG.debug "Smudging #{command[:pathname]}."
    $LOG.debug "#{command.inspect}"
    [
      { status: 'success' },
      content.map { |line| RCSKeyword.expand_line command, line }
    ]
  end

  ######################################################################

  sig { params(line: String).returns(String) }
  def self.contract_line line
    line.gsub MAYBE_EXP_RX, '$\\k<KEYWORD>$'
  end

  # Cleaner:
  sig { params(command: T::Hash[Symbol, String], content: T::Array[String])
          .returns([T::Hash[Symbol, String], T::Array[String]]) }
  def self.contract command, content
    $LOG.debug "Cleaning #{command[:pathname]}."
    $LOG.debug "#{command.inspect}"
    [
      { status: 'success' },
      content.map { |line| RCSKeyword.contract_line line }
    ]
  end
end

########################################################################

Filter.main \
         $stdin,
         $stdout,
         proc { |c,k| RCSKeyword.expand c, k },
         proc { |c,k| RCSKeyword.contract c, k } \
  if __FILE__ == $PROGRAM_NAME

__END__

Subversion propset docs::

    svn:keywords   - Keywords to be expanded.  Valid keywords are:
      URL, HeadURL             - The URL for the head version of the file.
      Author, LastChangedBy    - The last person to modify the file.
      Date, LastChangedDate    - The date/time the file was last modified.
      Rev, Revision,           - The last revision the file changed.
	LastChangedRevision
      Id                       - A compressed summary of the previous four.
      Header                   - Similar to Id but includes the full URL.

      Custom keywords can be defined with a format string separated from
      the keyword name with '='. Valid format substitutions are:
	%a   - The author of the revision given by %r.
	%b   - The basename of the URL of the file.
	%d   - Short format of the date of the revision given by %r.
	%D   - Long format of the date of the revision given by %r.
	%P   - The file's path, relative to the repository root.
	%r   - The number of the revision which last changed the file.
	%R   - The URL to the root of the repository.
	%u   - The URL of the file.
	%_   - A space (keyword definitions cannot contain a literal space).
	%%   - A literal '%'.
	%H   - Equivalent to %P%_%r%_%d%_%a.
	%I   - Equivalent to %b%_%r%_%d%_%a.
      Example custom keyword definition: MyKeyword=%r%_%a%_%P
      Once a custom keyword has been defined for a file, it can be used
      within the file like any other keyword: $MyKeyword$

We probably want:

+ Very summary info:

  | $Id: git--rcskw-expand 2425 2022-07-23 20:54:46 +1000 jashank@lisbon.rulingia.com.au $

+ Less summary info:

  | $Header: //bin/git--rcskw-expand 2425 2022-07-23 20:54:46 +1000 jashank@lisbon.rulingia.com.au $

+ Most recent modification author:

  | $Author: Jashank Jeremy <jashank@lisbon.rulingia.com.au> $
  | $LastChangedBy: Jashank Jeremy <jashank@lisbon.rulingia.com.au> $

+ Most recent modification date/time:

  | $Date: 2022-07-23 20:54:46 +1000 (Sat, 23 Jul 2022) $
  | $AuthorDate: 2022-07-23 20:54:46 +1000 (Sat, 23 Jul 2022) $
  | $LastChangedDate: 2022-07-23 20:54:46 +1000 (Sat, 23 Jul 2022) $

+ Most recent committer:

  | $Committer: Jashank Jeremy <jashank@rulingia.com.au> $
  | $LastCommittedBy: Jashank Jeremy <jashank@rulingia.com.au> $

+ Most recent commit date/time:

  | $CommitDate: 2022-07-23 20:54:46 +1000 (Sat, 23 Jul 2022) $
  | $LastCommittedDate: 2022-07-23 20:54:46 +1000 (Sat, 23 Jul 2022) $

+ Enumeration of revisions to this file:

  | $Revision: 2425 $

+ Hashes of
  the blob of this file,
  the tree containing this file,
  and the commit at this point.

  | $Blob: 7e01909ecb6b7497ef80847954ffb15c320c58ff $
  | $Tree: 78cc1bd1ce442262dc2435aca5359acbe969a118 $
  | $Commit: 0235cd15b9509fd3fa9f5236a9eb84bf6984f143 $
