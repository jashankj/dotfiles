#!/bin/sh

exec \
i3-nagbar \
	-t warning \
	-m 'Really exit i3?' \
	-b 'Yes, exit i3' 'i3-msg exit'
