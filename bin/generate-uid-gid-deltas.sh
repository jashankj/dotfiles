#!/bin/sh

x() {
	awk -F: '{print $1 "\t" $3}' | LC_ALL=C sort -k 1b,1
}
sysroot="$1"
for ff in group passwd
do
	f=$sysroot/etc/$ff
	LC_ALL=C join \
		<(ccat $f       | x) \
		<(ccat ${f}.new | x) \
	| awk '($2 != $3) { print }' \
	> ch.${ff}.${sysroot#/lisbon/}
done
