#!/bin/sh

prefix=$HOME
manifest="${prefix}/etc/emacs.manifest"

cd "${prefix}"

if ! [ -e "${manifest}" ]
then
	echo "local-emacs-uninstall: ${manifest}: No such file or directory" >&2;
	exit 69 # EX_UNAVAILABLE
fi

mtree -e -C -f "${prefix}/etc/emacs.manifest" |
grep type=file |
sort |
grep -v "etc/emacs.manifest" |
cut -d' ' -f1 |
xargs -P$(nproc) rm

mtree -e -C -f "${prefix}/etc/emacs.manifest" |
grep type=dir |
sort |
cut -d' ' -f1 |
xargs -P$(nproc) rmdir --ignore-fail-on-non-empty

rm -fv "${prefix}/etc/emacs.manifest" 
