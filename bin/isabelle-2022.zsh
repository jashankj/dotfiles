#!/usr/bin/env zsh
# NAME: isabelle-2022 --- run Isabelle 2022
# SYNOPSIS: isabelle-2022

exec \
env ISABELLE_IDENTIFIER=Isabelle2022 \
/src/isabelle/Isabelle2022/bin/isabelle "$@"
