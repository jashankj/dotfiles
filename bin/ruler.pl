#!/usr/bin/env perl

use strict;
use warnings;

my $cols = `tput cols`;

my $NN = 10;
my $N  = 1;

my @out = ();

while (($cols / $N) % $NN > 0) {
	my $out = '';
	for (my $i = 0; $i < $cols; $i += $N) {
		$out .= sprintf '%d%s', ($i/$N) % $NN, ' 'x($N-1);
	}
	$N *= $NN;
	$out = substr $out, 0, $cols;
	unshift @out, $out;
}

print "$_\n" foreach @out;
