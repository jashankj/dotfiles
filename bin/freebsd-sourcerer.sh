#!/bin/sh
# Build against a FreeBSD source tree.
#
#   % sh freebsd-sourcerer /usr/src korra -j24 buildworld buildkernel
#
#   % config=lisbon; \
#   > ionice -c3 \
#   > nice -n20 \
#   > script -a -f world+kernel.$(date +%Y%m%d).$(hostname -s).${config}.log -c \
#   >   "env MAKE=bmake freebsd-sourcer /src/freebsd/src-current $config -j16"
#

if [ $# -lt 4 ]
then
	echo "usage: freebsd-sourcerer <tree> <config> <-jN> [targets...]" >&2
	exit 64 #EX_USAGE
fi

: "${MAKE:=make}"
: "${OBJROOT:=/var/obj}"
: "${SRCROOT:=/src/freebsd/src}"
: "${tree:="$1"}"
: "${config:="$2"}"
: "${jn:="$3"}"
shift; shift; shift

ccat() {
	sed -E -e 's/#.*//g; /^[[:space:]]*$/d;'
}

case "$tree" in
	( /* )	treedir="$tree" ;;
	(  * )	treedir="${SRCROOT}/${tree}" ;;
esac

exec \
nice -n 20 \
env -i \
env \
	TERM="$TERM" \
	PATH=/usr/bin:/usr/sbin:/bin:/sbin:. \
	MAKEOBJDIRPREFIX="${OBJROOT}/${config?}" \
${MAKE?} \
	"${jn?}" \
	-C "${treedir?}" \
	"$@" \
	$(ccat < "env.${config?}")
