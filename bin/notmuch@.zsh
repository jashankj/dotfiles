#!/usr/bin/env zsh
# NAME: notmuch@ --- run Notmuch for a particular repository
# SYNOPSIS: notmuch@<repository> ...
#
# Symlink me as, e.g.,
#     $ ln -sf notmuch@ notmuch@unsw

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

set -e

repo="$(basename "$0")"
repo="${repo#notmuch@}"

[ ! -z "${repo}" ] ||
	errx EX_USAGE "no repository"

mail_root="$HOME/Mail"
mail_repo="${mail_root}/${repo}"

[ -e "${mail_repo}" ] ||
	errx EX_USAGE "unknown repository '${repo}'"

notmuch_config="${mail_root}/${repo}.notmuch"

[ -e "${notmuch_config}" ] ||
	errx EX_USAGE "no Notmuch configuration for '${repo}'"

exec notmuch --config="${notmuch_config}" "$@"
