#!/usr/bin/env zsh

basearch=x86_64
releasever=37
yumcache=/var/cache/dnf

typeset -a repos
typeset -A repo_cachedirs
typeset -A repo_urls

repos=(
	rawhide
	# rawhide-debug
	# rawhide-source

	openh264-rawhide
	# openh264-rawhide-debug
	# openh264-rawhide-source

	rawhide-modular
	# rawhide-modular-debug
	# rawhide-modular-source

	rpmfusion-free-rawhide
	# rpmfusion-free-rawhide-debug
	# rpmfusion-free-rawhide-source

	rpmfusion-nonfree-rawhide
	# rpmfusion-nonfree-rawhide-debug
	# rpmfusion-nonfree-rawhide-source

	rpmfusion-nonfree-steam-rawhide
	# rpmfusion-nonfree-steam-rawhide-debug
	# rpmfusion-nonfree-steam-rawhide-source

	openzfs
	# openzfs-source

	copr-jashankj-localpkgs
	copr-luminoso-Signal-Desktop

	ms-teams
)

repo_cachedirs=(
	[copr-jashankj-localpkgs]="copr:jashankj:localpkgs-d75badeb5ef781f7"
	[copr-luminoso-Signal-Desktop]="copr:luminoso:Signal-Desktop-076318cc180ba151"
	[ms-teams]="ms-teams-638eae43ea94ba74"
	[openh264-rawhide]="fedora-cisco-openh264-4896e02bbb10d47b"
	[openh264-rawhide-debug]="fedora-cisco-openh264-debuginfo-1c7a15f859237b4d"
#	[openh264-rawhide-source]=
	[openzfs]="zfs-95bede206046d798"
	[openzfs-source]="zfs-source-038c8a1e9a88596d"
	[rawhide]="rawhide-2d95c80a1fa0a67d"
	[rawhide-debug]="rawhide-debuginfo-b0297fcd3e236bd4"
	[rawhide-source]="rawhide-source-41d702670dad1ff3"
	[rawhide-modular]="rawhide-modular-bcea058c95ea8a90"
	[rawhide-modular-debug]="rawhide-modular-debuginfo-58fd8883ada30d87"
	[rawhide-modular-source]="rawhide-modular-source-f79fa9c27beaa9d9"
	[rpmfusion-free-rawhide]="rpmfusion-free-rawhide-11a4e834e18e3a11"
	[rpmfusion-free-rawhide-debug]="rpmfusion-free-debuginfo-993863e3b049535f"
	[rpmfusion-free-rawhide-source]="rpmfusion-free-rawhide-source-1735bdcf5a3517d4"
	[rpmfusion-nonfree-rawhide]="rpmfusion-nonfree-rawhide-978ef37952204a2d"
	[rpmfusion-nonfree-rawhide-debug]="rpmfusion-nonfree-rawhide-debuginfo-86c496277c656274"
	[rpmfusion-nonfree-rawhide-source]="rpmfusion-nonfree-rawhide-source-b4e20ce18b4853eb"
	[rpmfusion-nonfree-steam-rawhide]="rpmfusion-nonfree-steam-9f9bc4d8b2e41b2e"
	[rpmfusion-nonfree-steam-rawhide-debug]="rpmfusion-nonfree-steam-debuginfo-efec5e535637de96"
	[rpmfusion-nonfree-steam-rawhide-source]="rpmfusion-nonfree-steam-source-ae377553e6b2d663"
)

repo_urls=(
	[copr-jashankj-localpkgs]=https://download.copr.fedorainfracloud.org/results/jashankj/localpkgs/fedora-rawhide-$basearch
	[copr-luminoso-Signal-Desktop]=https://download.copr.fedorainfracloud.org/results/luminoso/Signal-Desktop/fedora-rawhide-$basearch

	[openh264-rawhide]="metalink+https://mirrors.fedoraproject.org/metalink?repo=fedora-cisco-openh264-$releasever&arch=$basearch"
	# https://codecs.fedoraproject.org/openh264/$releasever/$basearch/os
	[openh264-rawhide-debug]="metalink+https://mirrors.fedoraproject.org/metalink?repo=fedora-cisco-openh264-debug-$releasever&arch=$basearch"
	# https://codecs.fedoraproject.org/openh264/$releasever/$basearch/debug/tree

	[rawhide]="metalink+https://mirrors.fedoraproject.org/metalink?repo=rawhide&arch=$basearch"
	# https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Everything/$basearch/os
	[rawhide-debug]="metalink+https://mirrors.fedoraproject.org/metalink?repo=rawhide-debug&arch=$basearch"
	# https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Everything/$basearch/debug/tree
	[rawhide-source]="metalink+https://mirrors.fedoraproject.org/metalink?repo=rawhide-source&arch=$basearch"
	# https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Everything/$basearch/source/tree

	[rawhide-modular]="metalink+https://mirrors.fedoraproject.org/metalink?repo=rawhide-modular&arch=$basearch"
	# https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Modular/$basearch/os
	[rawhide-modular-debug]="metalink+https://mirrors.fedoraproject.org/metalink?repo=rawhide-modular-debug&arch=$basearch"
	# https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Modular/$basearch/debug/tree
	[rawhide-modular-source]="metalink+https://mirrors.fedoraproject.org/metalink?repo=rawhide-modular-source&arch=$basearch"
	# https://dl.fedoraproject.org/pub/fedora/linux/development/rawhide/Modular/$basearch/source/tree

	[rpmfusion-free-rawhide]="metalink+https://mirrors.rpmfusion.org/metalink?repo=free-fedora-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/free/fedora/development/rawhide/Everything/$basearch/os
	[rpmfusion-free-rawhide-debug]="metalink+https://mirrors.rpmfusion.org/metalink?repo=free-fedora-debug-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/free/fedora/development/rawhide/Everything/$basearch/debug
	[rpmfusion-free-rawhide-source]="metalink+https://mirrors.rpmfusion.org/metalink?repo=free-fedora-source-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/free/fedora/development/rawhide/Everything/source/SRPMS

	[rpmfusion-nonfree-rawhide]="metalink+https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/nonfree/fedora/development/rawhide/Everything/$basearch/os
	[rpmfusion-nonfree-rawhide-debug]="metalink+https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-debug-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/nonfree/fedora/development/rawhide/Everything/$basearch/debug
	[rpmfusion-nonfree-rawhide-source]="metalink+https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-source-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/nonfree/fedora/development/rawhide/Everything/source/SRPMS

	[rpmfusion-nonfree-steam-rawhide]="metalink+https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-steam-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/nonfree/fedora/steam/$releasever/$basearch
	[rpmfusion-nonfree-steam-rawhide-debug]="metalink+https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-steam-debug-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/nonfree/fedora/steam/$releasever/$basearch/debug
	[rpmfusion-nonfree-steam-rawhide-source]="metalink+https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-steam-source-$releasever&arch=$basearch"
	# https://download1.rpmfusion.org/nonfree/fedora/steam/$releasever/SRPMS

	[ms-teams]=https://packages.microsoft.com/yumrepos/ms-teams

	[openzfs]=https://download.zfsonlinux.org/fedora/36/$basearch
	[openzfs-source]=https://download.zfsonlinux.org/fedora/36/SRPMS
)

list_repodata_files() {
	xmlstarlet select \
		--template \
		--value-of '//@href' --nl \
		repodata/repomd.xml \
	| sort -t- -k2
}

download_file_curl() {
	local out="$1"
	local url="$2"

	curl \
		--progress-bar \
		--output "${out}" \
		--etag-save "${out}.etag" \
		--etag-compare "${out}.etag" \
		--remote-time \
		"${url}"
}

download_file() {
	local out="$1"
	local url="$2"

	case "${out}" in
		(*.zck)
			local b="$(basename "${out}")"
			local d="$(dirname "${out}")"
			pushd "$d"
			printf "%s:\n" "${out}"
			if [ -r "$b" ]
			then zckdl           -s "$b" "${url}"
			else download_file_curl "$b" "${url}"
			fi
			popd
			;;

		(*.gz)
			printf "# ignoring %s\n" "${url}"
		#	printf "%s:\n" "${out}"
		#	download_file_curl "$out" "$url"	
		;;

		(*)
			printf "%s:\n" "${out}"
			download_file_curl "$out" "$url"
			;;
	esac
}

download_repometadata() {
	local repo="$1"
	local repo_url="${repo_urls[$repo]}"

	case "$repo_url" in
		( metalink+* )
			download_file "metalink.xml" "${repo_url#metalink+}"
			wget \
				--input-metalink=metalink.xml \
				--trust-server-names=on \
				--directory-prefix=repodata \
				--clobber --continue --xattr
			repo_url="$(
				getfattr \
					--name=user.xdg.origin.url \
					--only-values \
					repodata/repomd.xml
			)"
			repo_url="${repo_url%/repodata/repomd.xml}"
			echo "using $repo_url for $repo"
			repo_urls[$repo]="${repo_url}"
			;;
		( * )
			download_file "repodata/repomd.xml" "${repo_url}/repodata/repomd.xml"
			;;
	esac
}

download_repo_file() {
	local repo="$1"
	local file="$2"
	local repo_url="${repo_urls[$repo]}"
	local file_url="${repo_url}/${file}"

	case "${file}" in
		( *-primary.xml.* | *-filelists.xml.* | *-other.xml.* | *-comps-*.xml.* )
			download_file "${file}" "${file_url}"
			;;

	#	( *-primary.sqlite.* | *-filelists.sqlite.* | *-other.sqlite.* | *-comps-*.sqlite.* )
	#		download_file "${file}" "${file_url}"
	#		;;

		(*)
			;;
	esac
}

for repo in ${repos}
do
	echo "repo=$repo"
	mkdir -p "${yumcache}/${repo_cachedirs[$repo]}"
	cd "${yumcache}/${repo_cachedirs[$repo]}"
	mkdir -p repodata packages

	download_repometadata "$repo"

	list_repodata_files | tac |
	while read file
	do
		download_repo_file "$repo" "$file"
	done
done
