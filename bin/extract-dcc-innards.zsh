#!/usr/bin/env zsh
# NAME: extract-dcc-innards --- extract rolled-up innards from dcc'd binary
# SYNOPSIS: extract-dcc-innards file

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 1 ] || usage

f="$1"
innards="${f}.innards"
tar_out="${f}.inner.tar"
san2_out="${f}.inner.san2"

objcopy -j .data "$f" "$f.innards"
file_off=$(objdump -h "${innards}" | awk '/\.data/ { print "0x" $6 }')
vma_base=$(objdump -h "${innards}" | awk '/\.data/ { print "0x" $4 }')

objdump -t "${innards}" |
awk '
/sanitizer2_executable/ { san2_base = $1; san2_len = $5 }
/tar_data/              { tar_base  = $1; tar_len  = $5 }
END { print "0x"san2_base, "0x"san2_len, "0x"tar_base, "0x"tar_len }
' |
while read san2_base san2_len tar_base tar_len
do
dd bs=1 skip=$(( ${tar_base}  - ${vma_base} + ${file_off} )) count=$(( ${tar_len}  )) if="${innards}" of="${tar_out}"
dd bs=1 skip=$(( ${san2_base} - ${vma_base} + ${file_off} )) count=$(( ${san2_len} )) if="${innards}" of="${san2_out}"
done
