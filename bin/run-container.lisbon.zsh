#!/usr/bin/env zsh
# NAME: run-container.lisbon --- run a container config on lisbon

set -o extendedglob

img="$(basename "$0")"
img="${img#run-}"
img="${img%.lisbon}"

typeset -A container_opts
container_opts=(
	[lxc.include]=/usr/share/lxc/config/common.conf
	[lxc.arch]=x86_64

	# Networking configuration.
	[lxc.net.0.type]=veth
	[lxc.net.0.flags]=up
	[lxc.net.0.link]=lxcbr
	[lxc.net.0.name]=lxcnet0

	# With AppArmour, run unconfined:
	# [lxc.apparmor.profile]=unconfined
)

case "${img}" in
	alpine)
		container_opts+=([lxc.net.0.ipv4.address]=192.168.76.66/24)
		;;
	arch)
		container_opts+=([lxc.net.0.ipv4.address]=192.168.76.44/24)
		;;
	centos|fedora)
		container_opts+=([lxc.net.0.ipv4.address]=192.168.76.88/24)
		;;
	debian)
		container_opts+=([lxc.net.0.ipv4.address]=192.168.76.22/24)
		;;
esac

# Container specific configuration
container_opts+=([lxc.rootfs.path]="/lisbon/${img}")
container_opts+=([lxc.uts.name]="${img}.lisbon.ri")

typeset -a spawn_args
spawn_args=()

for k in ${(k)container_opts}
do
	spawn_args+=(--define "${k}=${container_opts[$k]}")
done

[ ! -z "$DEBUG" ] && set -x
exec \
lxc-start \
	--name="${img}" \
	--rcfile=/dev/null \
	--foreground \
	"${(@)spawn_args}" \
	"$@"
