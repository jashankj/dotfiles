#!/usr/bin/env zsh
# NAME: c-pretty --- generate formatted PDFs, to Carroll's taste
# SYNOPSIS: c-pretty files...
#
# based on
# $Id: default.c.pdf.do 1407 2019-06-13 07:36:19Z jashank $
# $Id: template.tex 1449 2019-07-01 14:41:07Z jashank $
#
# 2019-10-24	 Jashank Jeremy <jashank.jeremy@unsw.edu.au>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

for arg
do
	lang=none
	case "${arg}" in
	*.[ch])	lang=C		;;
	*.hs)	lang=haskell	;;
	*.sh)	lang=shell	;;
	esac

	cat - <<__EOF__ | lualatex -shell-escape -jobname="${arg}"
\\documentclass[10pt,british,a4paper]{article}
\\usepackage[T1]{fontenc}
\\usepackage{microtype}
\\usepackage{fontspec}
\\usepackage[proportional,osf]{cochineal}
% \\usepackage{ccfonts}
\\usepackage{helvet}
\\setmonofont[Contextuals={Alternate}]{Fira Code}

\\usepackage{geometry}
\\geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1.25in,rmargin=1in}
\\setlength{\\parskip}{\\medskipamount}
\\setlength{\\parindent}{0pt}
\\usepackage{multicol}

\\usepackage{mathtools}
\\usepackage{amsthm}
\\usepackage{amssymb}
\\usepackage{cancel}
\\usepackage{mathdots}
\\usepackage{stmaryrd}
\\usepackage{stackrel}
\\PassOptionsToPackage{version=3}{mhchem}
\\usepackage{mhchem}
\\usepackage{esint}

\\usepackage[unicode=true,pdfusetitle]{hyperref}
\\usepackage{minted}
\\usepackage{alltt}

\\title{${arg}}
% \\author{Jashank Jeremy <${zID}@cse.unsw.edu.au>}
\\begin{document}
\\inputminted[
    frame=leftline, linenos,
    escapeinside=@@,
    obeytabs, tabsize=4
]{${lang}}{${arg}}
\\end{document}

__EOF__
done
