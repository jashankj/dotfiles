#!/usr/bin/env zsh
# NAME: ergo --- set up my Kinesis keyboard on my laptop
# SYNOPSIS: ergo

paste \
	<(xinput list --id-only) \
	<(xinput list --name-only) \
| grep '05f3:0007' \
| while IFS=$'\t' read id dev
  do
	setxkbmap \
		-option ctrl:nocaps \
		-option compose:prsc \
		-device ${id} us
  done
