#!/usr/bin/env zsh
# NAME: pdf-explode-pages --- split a PDF into a PDF per page
# SYNOPSIS: pdf-explode-pages <file.pdf>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# -eq 1 ] || usage
file="$1"

pages="$(
	pdfinfo "$file" |
	awk '/^Pages:/ { print $2 }'
)"

for i in {1..$pages}
do
	out="$(printf '%s-%04d.pdf' "${file%.pdf}" $i)"
	pdfjam --outfile "$out" -- "$file" $i
done
