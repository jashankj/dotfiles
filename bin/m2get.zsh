#!/usr/bin/env zsh
# NAME: m2get --- get a specific jar from the maven repo cache
# SYNOPSIS: m2get <group-id> <artifact-id> <version>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]
then
	usage
fi

gID="$1"
aID="$2"
ver="$3"
maven_root=~/.m2/repository;
glob="$(\
	printf "%s/%s/%s/%s/%s-%s*.jar" \
		"${maven_root}" \
		"${gID//./\/}" \
		"${aID}" \
		"${ver}" \
		"${aID}" "${ver}" \
	)"
eval "ls $glob"
