#!/usr/bin/env zsh
# NAME: isabelle-2024 --- run Isabelle 2024
# SYNOPSIS: isabelle-2024

exec \
env ISABELLE_IDENTIFIER=Isabelle2024 \
/src/isabelle/Isabelle2024/bin/isabelle "$@"
