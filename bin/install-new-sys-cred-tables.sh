#!/bin/sh

# mkdir -p remaster
# cp -vp passwd shadow group gshadow remaster

T=$(date +%s)
for i in passwd shadow group gshadow
do
	cp -vp $i $i.$T
	for m in chmod chown chgrp
	do
		$m --verbose --reference=$i $i.new
	done
	mv --verbose ${i}.new $i
done
