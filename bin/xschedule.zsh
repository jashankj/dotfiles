#!/usr/bin/env zsh
# NAME: xschedule --- print my schedule
# SYNOPSIS: xschedule

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"


schedule \
| zenity --text-info --timeout=10 \
	--font='Source Code Pro 12' \
	--width=1680 --height=960 \

# | xmessage -file - -timeout 10 -center -fn 10x20

