#!/bin/sh
# NAME: hostenv --- get the current host and environment
#
# If ``~/.hostenv`` is a readable extant file,
# its contents are taken as the host/environment pair::
#
#   $ echo ri-lisbon > ~/.hostenv
#   $ hostenv
#   ri-lisbon
#
# If ``~/.hostenv`` is a symlink to a nonexistent file,
# its symlink target is taken as a host/environment pair::
#
#   $ ln -s ri-lisbon ~/.hostenv
#   $ hostenv
#   ri-lisbon
#
# If we're at CSE, the host+environment is always "cse".
# This is to handle a shared home directory better.
#
# Otherwise, the environment is always ``ri``::
#
#   $ hostname -s
#   lisbon
#   $ hostenv
#   ri-lisbon

[ -e "$HOME/.hostenv" ] && exec cat "$HOME/.hostenv"
[ -h "$HOME/.hostenv" ] && exec readlink "$HOME/.hostenv"

[ -e "/unsw" ] && [ "$USER" = "z5017851" ] &&
[ "$(dnsdomainname)" = "orchestra.cse.unsw.EDU.AU" ] &&
	exec echo cse

exec echo "ri-$(hostname -s)"
