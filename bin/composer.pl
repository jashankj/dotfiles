#!/usr/bin/perl -w
# NAME: composer --- generate an XCompose file
#
# 2018-05-08	Jashank Jeremy <jashank@rulingia.com.au>
# 2021-12-31	Jashank Jeremy <jashank@rulingia.com.au>
#     clean up; parse a configuration file ("~/.XCompose.toml").
#
# Debian-Depends:
#     perl, libio-compress-perl, libtoml-parser-perl,
#     x11proto-dev
# Debian-Suggests: emacs-el
#
# The configuration file is TOML.
# The top level is a map.
# The map-key describes a compose key-sequence.
# The corresponding map-value, if boolean false, removes any previous
# mapping (useful when removing RFC-1345 sequences).
# Otherwise, that map-value is a string giving the resulting character.
#
# Invoke as, e.g.,
#     perl ~/bin/composer --load-rfc1345 > ~/.XCompose

use v5;
use strict;
use warnings;
use charnames ();
use feature 'say';

use Encode 'decode';
use feature 'unicode_strings';
use utf8;
binmode STDOUT, ":utf8";

use Getopt::Long;
use IO::Uncompress::Gunzip qw{gunzip $GunzipError};

use TOML::Parser;

sub add_rule($$);
sub remove_rule($);
sub load_rfc1345_from_emacs($);
sub load_composer_toml($);
sub load_keysymdef($);
sub explode_rule($);
sub has_conflict($);
sub emit_all_rules();
sub emit_rule($$$$);


my $LOAD_RFC1345     = 0;
my $QUAIL_RFC1345_EL;
my $KEYSYMDEF_H      = '/usr/include/X11/keysymdef.h';
my $CONFIG_FILE      = "$ENV{HOME}/.XCompose.toml";

GetOptions (
	'load-rfc1345'       => \$LOAD_RFC1345,
	'quail-rfc1345-el=s' => \$QUAIL_RFC1345_EL,
	'keysymdef-h=s'      => \$KEYSYMDEF_H,
	'config-file=s'      => \$CONFIG_FILE
) or die "composer".
	" [--load-rfc1345]".
	" [--quail-rfc1345-el=PATH]".
	" [--keysymdef-h=PATH]".
	" [--config-file=PATH]\n";

$QUAIL_RFC1345_EL =
	glob </usr/share/emacs/*/lisp/leim/quail/rfc1345.el*>
	if $LOAD_RFC1345 and not defined $QUAIL_RFC1345_EL;


########################################################################

my $PREFIXES = [];
my $RULES = {};

sub add_rule($$) {
	my ($k, $v) = @_;
	$RULES->{$k} = $v;
	push @$PREFIXES, $k;
}

sub remove_rule($) {
	my ($k) = @_;
	delete $RULES->{$k};
	@$PREFIXES = grep { @_ ne $k } @$PREFIXES;
}

########################################################################
# snarf RFC 1345 translation table

sub load_rfc1345_from_emacs($) {
	my ($el) = @_;
	my $fh;
	if ($el =~ /\.el\.gz$/) {
		$fh = new IO::Uncompress::Gunzip $el or die "$el: $GunzipError\n";
	} else {
		$fh = IO::File->new($el, 'r') or die "$el: $!\n";
	}

	my $seen_qdr = 0;

	while (<$fh>) {
		$_ = decode 'utf-8', $_; # convert from legacy

		# kill comments
		next if /^;/;

		# snarf only (quail-define-rules [...])
		if (/^\(quail-define-rules/) { $seen_qdr = 1; next }
		next unless $seen_qdr;
		last if /^\)/;

		s/^\s*\((.*)\)$/$1/g; # lose parens
		@_ = split;
		$_[0] =~ s/^"(.*)"$/$1/g; # lose quotes
		$_[0] =~ s/\\"/"/g; # lose escaped quotes
		$_[0] =~ s/^&//g; # lose leading &
		$_[1] =~ s/^\?\\?//g; # lose leading ?\

		add_rule $_[0], $_[1];
	}

	$fh->close;
}

########################################################################
# load XCompose TOML

sub load_composer_toml($) {
	my ($config_file) = @_;

	my $parser = TOML::Parser->new;
	my $data   = $parser->parse_file($config_file);

	foreach my $key (sort keys %$data) {
		my $seq = $data->{$key};
		if ($seq eq 'false') {
			remove_rule $key;
		} else {
			add_rule $key, $seq;
		}
	}
}


########################################################################
# snarf keysymdef files

sub load_keysymdef($) {
	my ($keysymdef_h) = @_;
	open my $fh, '<', $keysymdef_h or die "$keysymdef_h: $!";

	# "Mnemonic names for keysyms are defined in this file with lines that
	# match one of these Perl regular expressions:"
	my $KS1 = qr/^\#define XK_([a-zA-Z_0-9]+)\s+0x([0-9a-f]+)\s*\/\* U+([0-9A-F]{4,6}) (.*) \*\/\s*$/;
	my $KS2 = qr/^\#define XK_([a-zA-Z_0-9]+)\s+0x([0-9a-f]+)\s*\/\*\(U+([0-9A-F]{4,6}) (.*)\)\*\/\s*$/;
	my $KS3 = qr/^\#define XK_([a-zA-Z_0-9]+)\s+0x([0-9a-f]+)\s*(\/\*\s*(.*)\s*\*\/)?\s*$/;

	my $keysymdef = {};
	while (<$fh>) {
		if (/$KS3/) {
			my $point = hex $2;
			next unless 0x0000 <= $point and $point <= 0x007f;
			$keysymdef->{pack('U', $point)} = $1;
		}
	}

	$fh->close;
	return $keysymdef;
}

my $KEYSYMDEF = load_keysymdef $KEYSYMDEF_H;

########################################################################

sub emit_all_rules() {
	foreach my $rule (sort @$PREFIXES) {
		my $char = $RULES->{$rule};
		my $code = ord $char;
		my $name = charnames::viacode $code;

		# drop cases where Perl derps out
		next if not defined $name;
		next if $name eq "NULL";
		# sometimes it just turns into "200".."237"
		next if $code < 0x7f;

		my $rule_ = explode_rule $rule;

		# is this a prefix code?
		# FIXME(jashankj): this is extremely terrible!
		$rule_ .= ' <space>' if has_conflict $rule;

		emit_rule $rule_, $char, $code, $name;
	}
}

sub explode_rule($) {
	join ' ', map { '<' . $KEYSYMDEF->{$_} . '>' } split //, shift
}

sub has_conflict($) {
	my $rule = shift;
	grep { $rule ne $_ and 0 == index $_, $rule } @$PREFIXES
}

sub emit_rule($$$$) {
	my ($rule, $char, $code, $name) = @_;

	say sprintf '<Multi_key> %s : "%s" # U+%04X %s',
		$rule, $char, $code, $name;
}

########################################################################

load_rfc1345_from_emacs $QUAIL_RFC1345_EL if $LOAD_RFC1345;
load_composer_toml $CONFIG_FILE;
emit_all_rules;
