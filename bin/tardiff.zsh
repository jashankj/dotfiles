#!/usr/bin/env zsh
# NAME: tardiff --- list changes between two tarballs
# SYNOPSIS: tardiff <file-1.tar> <file-2.tar>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 2 ] || usage

bsdtar tf "$1" | sort | sed -E 's@^[^/]+/@@g' > "$1".files &
bsdtar tf "$2" | sort | sed -E 's@^[^/]+/@@g' > "$2".files &
wait

comm -23 "$1".files "$2".files | sed 's@^@- @'
comm -13 "$1".files "$2".files | sed 's@^@+ @'

