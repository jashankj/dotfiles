#!/bin/sh
#
# srcosa-fetch-release-tarballs:
#   given a ``RELEASE.html``, works out what tarballs make up this
#   release, and fetches them.  uses the local tarball cache if shared
#   between versions.

cat RELEASE.html \
| grep -o "/tarballs.*\"" \
| tr -d '"' \
| sed 's@^@https://opensource.apple.com@' \
| while read url
do
	if [ ! -e "../tarballs/$(basename "${url}")" ]
	then
		wget -O "../tarballs/$(basename "${url}")" "${url}"
	fi
	ln -v "../tarballs/$(basename ${url})"
done
