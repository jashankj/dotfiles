#!/usr/bin/env zsh

zmodload zsh/stat
zmodload zsh/datetime

cached="$HOME/.cache/todoist-karma"
lifetime=7200

expiry=$(( $(strftime %s) - lifetime ))

[ -e "${cached}" ] &&
[ $(zstat +mtime "${cached}") -ge "${expiry}" ] &&
{
	cat "${cached}"
	exit
}

todoist completed get_stats \
| awk '
/^karma:/ { karma = $2 };
/^karma_last_update:/ { last_update = $2 < 0 ? (-1 * $2) : $2; };
/^karma_trend:/ { trend = $2 };
END {
	trend_str = trend == "up" ? "▴" : "▾";
	printf("%d (%s%d)\n", karma, trend_str, last_update);
}' \
| tee "${cached}"
