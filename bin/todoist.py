#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys
import fire
# import todoist
from todoist_api_python.api import TodoistAPI

fire.Fire(
	TodoistAPI(
		open(os.environ['HOME'] + '/.todoist-token')
			.read()
			.strip()
	),
	command=sys.argv[1:]
)
