#!/usr/bin/env zsh
# NAME: throttle --- control performance on jaenelle

[ $HOST = "jaenelle" ] || exit 1

clamp=0
freq=2.0g
fan=3

case "$1" in
	min)	freq=1.0g;	clamp=50;	fan=3 ;;
	back)	freq=2.0g;	clamp=50;	fan=3 ;;
	down)	freq=2.0g;	clamp=0;	fan=3 ;;
	up)	freq=2.6g;	clamp=0;	fan=7 ;;
	max)	freq=3.3g;	clamp=0;	fan=disengaged ;;

	*)	echo "usage: throttle (min|back|down|up|max)";
		exit 64 # EX_USAGE
		;;
esac

set -e
echo "throttle: limit ${freq}, clamp ${clamp}%, fans ${fan}"

sudo cpupower frequency-set -u ${freq} >/dev/null
powerclamp ${clamp}
fan ${fan}

