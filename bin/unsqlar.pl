#!/usr/bin/perl -w
# NAME: unsqlar.pl --- terrible perl script to extract sql archives
#
# foolishly I copied a sqlar to CSE, where sqlite3 ain't new enough to
# know what to do with it ... and instead of working around it, I opted
# to write a decompressor instead; in perl, because plain shell was
# getting too slow for our needs.

use strict;
use warnings;

use Compress::Zlib;
use File::Path 'make_path';

use DBD::SQLite;
use DBI;

die "usage: unsqlar <archive>\n" unless scalar @ARGV == 1;

my $ar = shift @ARGV;
my $dbh = DBI->connect("dbi:SQLite:dbname=$ar","","");

my $get = $dbh->prepare(q{
	SELECT name, mode, mtime, sz, quote(data) FROM sqlar
});

$get->execute;
my @row;
while (@row = $get->fetchrow_array) {
	my ($name, $mode, $mtime, $sz, $data) = @row;

	print "$name";
	if ($mode & 040000) {
		# directory
		make_path $name;
	} else {
		# assuming file
		my ($i, $o, $st);
		($i, $st) = inflateInit();
		($o, $st) = $i->inflate(pack "H*", substr $data, 2, -1);
		open my $f, '>', $name;
		$f->print($o);
		$f->close;
	}

	$mode  = int($mode) & 0777;
	$mtime = int($mtime);
	utime $mtime, $mtime, $name;
	chmod($mode, $name);
	print "\n";
}
