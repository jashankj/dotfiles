#!/usr/bin/env zsh
# NAME: update-signal --- fetch and install a new version of Signal
# SYNOPSIS: update-signal

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# -eq 0 ] || usage

set -e

pkg="signal-desktop"
arch="amd64"
deb_uri="https://updates.signal.org/desktop/apt"
deb_dist="xenial"
deb_comp="main"

f_release="${deb_uri}/dists/${deb_dist}/InRelease"
f_package="${deb_uri}/dists/${deb_dist}/${deb_comp}/binary-${arch}/Packages"

echo curl "${f_package}"
curl "${f_package}" |
grep '^Filename:' |
grep --fixed-strings "/${pkg}/" |
sort --reverse --version-sort |
head -n1 |
cut -d' ' -f2 |
read f_latest

file="${f_latest##*/}"
echo curl "https://updates.signal.org/desktop/apt/${f_latest}"
curl --remote-name "https://updates.signal.org/desktop/apt/${f_latest}"
bsdtar xOf "${file}" data.tar.xz |
sudo bsdtar -C / -xvf - opt/Signal
