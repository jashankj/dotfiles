#!/usr/bin/env perl
#
# NAME: yum-list-acceptable-updates --- filter list of package updates
# SYNOPSIS: yum-list-acceptable-updates
#
# Running Fedora I<rawhide> on a host requires care and attention whilst
# one reviews and installs package updates, due to a combination of a
# lot of package churn, and the subsequent occasional glitches with
# repository self-consistency (i.e., C<repoclosure> or similar).
#
# To this end, this tool provides a mechanism to filter out particular
# updates, either indefinitely or for a particular period of time until
# an upstream issue is resolved.
#
# For example, I<rawhide> ships Linux release-candidate kernels, which
# provides an ample proving-ground for kernel changes --- and, indeed,
# there are those who opt to take I<rawhide> kernels on a branched
# release, in order to get access to changes that haven't landed on a
# stable kernel yet.
#
# I, however, do not want this --- my experience is that a laptop wants
# to run the newest stable kernel, and OpenZFS usually only marks a
# kernel usable once it is released --- and thus I do the opposite of
# that 'backport-kernel-from-rawhide' strategy, and, via C<koji-fetch>,
# will run the latest kernel from the stable Fedora release.  This means
# I must filter out I<rawhide> kernel updates.
#
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#

use strict;
use warnings;
use feature 'say';

use local::lib;

use FindBin;
use lib "$FindBin::Bin/../lib/perl";
use J::Proc;
$J::Proc::VERBOSE_IO = 0;

my $yum = J::Proc->spawn(
	# Force dnf to consider a wide terminal.
	'env', 'COLUMNS=1024',
	exists($ENV{'YUM'}) ? $ENV{'YUM'} : 'dnf5',
	'check-update',
	'--quiet',
	'--cacheonly'
);
$yum->{iam} = 'yum';

our $ARCH = qr{
	( noarch
	| i[3456]86			# 1..31 (XXX)
	| x86_64			# 1..
	| ia64				# 9
	| mipsel | mips64el		# 11..13, 23..24
	| armv7hl | aarch64		# 20..
	| ppc64le			# 4..
	| ppc64				# 22..28
	| ppc				# 4..28
	| riscv64			# ?37..
	| s390x				# 14..
	| sparc64 | sparc		# 12B
	)
}nx;

our $KERNEL_PKGS = qr{
	\A
	( kernel
	| kernel-core
	| kernel-devel
	| kernel-devel-matched
	| kernel-doc
	| kernel-modules
	| kernel-modules-core
	| kernel-modules-extra
	| kernel-modules-internal
	| kernel-selftests-internal
#	| kernel-tools
#	| kernel-tools-libs
#	| kernel-tools-libs-devel
	| kernel-uki-virt
	| kernel-uki-virt-addons
#	| perf
#	| libperf
#	| libperf-devel
#	| python3-perf
#	| rtla
#	| rv
	) \. $ARCH
}nx;

our $FFMPEG_PKGS = qr{
	\A
	( ffmpeg
	| ffmpeg-libs
	| libavdevice
	| libavcodec-free
	| libavdevice-free
	| libavfilter-free
	| libavformat-free
	| libavutil-free
	| libpostproc-free
	| libswresample-free
	| libswscale-free
	) \. $ARCH
}nx;

our $OPENJDK_PKGS = qr{
	\A
	java-
	( latest		# java-latest-openjdk
	| 1(\.\d+)+		# java-1.8.0-openjdk
	| \d+ )			# java-17-openjdk
	-openjdk
	( # empty
	| -demo
	| -devel
	| -headless
	| -javadoc
	| -jmods
	| -src
	) \. $ARCH
}nx;

sub is_acceptable
{
	$_ = shift;

	#
	# Rules:
	#  * rules are always of the form C<< return 0 if PATTERN >>.
	#  * always head and tail patterns:
	#    C<< m{ \A >> ... C<< \. $ARCH }nx >>.
	#  * lexicographical sorting of package exclusions in a set.
	#  * always annotate Bodhi update ID(s) of broken update(s).
	#  * date and describe all acceptability changes.
	#

	# 2022-05-06--: Don't accept rawhide kernels.
	return 0 if m{ $KERNEL_PKGS }nx;

	# 2024-11-16: rpmfusion ffmpeg lagging rawhide
	return 0 if m{ $FFMPEG_PKGS }nx or
		m{ \A mupdf( | -libs) \. $ARCH }nx or
		m{ \A teem-(devel|libs) \. $ARCH }nx or
		m{ \A tesseract( | -libs) \. $ARCH }nx;

#	# 2024-03-30--: CVE-2024-3094
#	return 0 if m{
#		\A
#		( perl-Compress-Raw-Lzma
#		| perl-IO-Compress-Lzma
#		| xz
#		| xz-devel
#		| xz-libs
#		| xz-lzma-compat )
#		\. $ARCH
#		\Z
#	}nx;

	return 0 if m{
		\A
		ghostscript-tools-(fonts|printing)
		\. $ARCH
		\Z
	}nx;

#	# 2024-01-25--: wine 9.0-2 (FEDORA-2024-9d7655d67e) broken
#	return 0 if m{
#		\A
#		wine
#		( -alsa
#		| -cms
#		| -common
#		| -core
#		| -desktop
#		| -devel
#		| -filesystem
#		| -fonts
#		| -ldap
#		| -opencl
#		| -pulseaudio
#		| -systemd
#		| -twain
#		| -( arial | courier | fixedsys | marlett
#			| ms-sans-serif | small | symbol
#			| system | tahoma | times-new-roman
#			| webdings | wingdings )-fonts
#		| -( tahoma | times-new-roman | wingdings )-fonts-system
#		) ?
#		\. $ARCH
#	}nx;

	# 2023-11-19--: Emacs update conflicts?
	return 0 if m{
		\A
		emacs ( - (filesystem | common | gtk\+x11 ) )?
		\. $ARCH
	}nx;

#	# 2023-04-15: python-nbclassic picks up bad files FEDORA-2023-0067a3d818
#	return 0 if m{ \A python3-nbclassic \. $ARCH }nx;

#	# 2023-02-19: dpkg picks up bad "perl(at)" dep FEDORA-2023-49b272d6c2
#	# 2023-03-28: fixed FEDORA-2023-33e83438df
#	return 0 if m{ \A dpkg ( | -dev | -perl ) \. $ARCH }nx;

#	# 2023-01-29: java-latest-openjdk picks up bad FreeType dep FEDORA-2023-c7a65df61c
#	# 2023-02-12: fixed FEDORA-2023-81e277b630
#	# 2023-02-19: conflict java-{17,latest}-openjdk FEDORA-2023-f0c0c49567
#	# 2023-04-23: broken again
#	return 0 if m{ $OPENJDK_PKGS }nx;

	# https://fedoraproject.org/wiki/Changes/StrongCryptoSettings3
	# Fedora crypto policy updates mean some third-party packages
	# with insecure (well, SHA1) signatures cause pain: RHBZ#2149762
	#
	# 2022-08-15: crypto-policies-20220815-1.gite4ed860.fc38 (RSAMinSize)
	# 2022-08-24: crypto-policies-20220824-1.gitd4b71ab.fc38 (SHA1--)
	# 2022-08-24: crypto-policies-20220824-2.git2187e9c.fc38 (revert)
	# 2022-10-03: crypto-policies-20221003-1.gitcb1ad32.fc38 (RequiredRSASize)
	#
	# crbug.com/1398429:
#	return 0 if m{ \A google-chrome-(stable|beta|unstable) \. $ARCH }nx;
	# Citrix:
	return 0 if m{ \A ICAClient \. $ARCH }nx;
	# Microsoft Teams:
	return 0 if m{ \A teams \. $ARCH }nx;
	# Zoom:
	return 0 if m{ \A zoom \. $ARCH }nx;

#	# 2022-08-08: rpmfusion ffmpeg lagging rawhide
#	return 0 if m{ $FFMPEG_PKGS }nx or m{ \A libplacebo \. $ARCH }nx;

#	# 2022-07-10: rpmfusion ffmpeg lagging rawhide tesseract FEDORA-2022-03e7d2ea1d
#	# 2022-07-16? rpmfusion rebuilt ffmpeg
#	return 0 if m{ $FFMPEG_PKGS }nx or m{ \A ( tesseract ) \. $ARCH }nx;

#	# 2022-07-09: rpmfusion mpv lagging rawhide libplacebo FEDORA-2022-9cccddf07c
#	# 2022-07-16? rpmfusion rebuilt mpv
#	return 0 if m{ \A ( libplacebo | mpv ) \. $ARCH }nx;

#	# 2022-07-01: blueman 1:2.3-1.beta1 breaks world FEDORA-2022-5346b5edd3
#	# 2022-07-05: blueman 1:2.3-2.beta1 fixes world FEDORA-2022-825fe8f982
#	return 0 if m{ \A blueman \. $ARCH }nx;

#	# 2022-06-21: suppress ffmpeg updates
#	# 2022-06-21: rpmfusion ffmpeg lagging rawhide srt-libs
#		| srt-libs
#	# 2022-06-25: rpmfusion rebuilt ffmpeg
#	# 2022-06-25: rpmfusion ffmpeg lagging rawhide libdav1d and deps
#	# 2022-06-28: rpmfusion rebuild ffmpeg
#	return 0 if m{ $FFMPEG_PKGS }nx or m{
#		\A
#		( gd
#		| libavif
#		| libdav1d
#		| svt-av1-libs
#		) \. $ARCH
#       }nx;

#	# 2022-05-29: rpmfusion mpv lagging rawhide vapoursynth FEDORA-2022-930353a9c8
#	# 2022-06-25: rpmfusion rebuilt mpv
#	return 0 if m{ \A ( mpv | vapoursynth-libs ) \. $ARCH }nx;

#	# 2022-05-29: ilbc breaks the world RHBZ#1948234 RHBZ#2090764
#	# 2022-05-30: rawhide rebuilt ffmpeg
#	return 0 if m{ \A ( ilbc | ilbc-devel ) \. $ARCH }nx;

	################################################################

	return 1;
}


$yum->close_in;
while (defined($_ = $yum->getline)) {
	chomp;
	next if /^\s*$/;

	# Ignore obsoleting updates.
	last if /^Obsoleting/;

	my @F = split /\s+/;
	next unless is_acceptable $F[0];
	say $F[0];
}

# Local variables:
# mode: cperl
# smartparens-mode: t
# tab-width: 8
# indent-tabs-mode: t
# cperl-indent-level: 8
# eval: (jashankj/bug-reference-setup-fedora)
# End:
