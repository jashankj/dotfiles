#!/bin/sh
# NAME: disk-stats.lisbon.iostat --- run iostat on lisbon

disk="${1-nvme0n1}"

exec \
env S_COLORS=always \
stdbuf -i0 -o0 \
iostat -d -x 1 \
| gawk --assign disk="${disk}" \
'
	function p(x) {
		print strftime("%F %T", systime()), "\t", x
	};
	/^Device/ {
		if (n % 20 == 0)
			p($0);
		n = (n % 20) + 1;
	};
	$0 ~ disk { p($0) }
'
