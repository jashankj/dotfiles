#!/usr/bin/env perl

use strict;
use warnings;

use Carp;
use File::Copy;

foreach my $local_name (@ARGV) {
	unless (-l $local_name) {
		carp "$local_name: not a symbolic link";
		next;
	}

	my $link_path = readlink $local_name
		or croak "readlink($local_name): $!";

	unlink $local_name
		or croak "unlink($local_name): $!";

	printf "\%s -> \%s\n", $link_path, $local_name;
	copy $link_path, $local_name
		or croak "copy($link_path, $local_name): $!";
}
