#!/bin/sh
#
# srcosa-get-release-info:
#   get information about releases from the toplevel index.

osa=https://opensource.apple.com

[ -e index.html ] ||
	wget -O index.html "${osa}/"

cat index.html \
| sed -Ene '
	/release\// {
		s/^.*\"\/release\/(.*).html\">(.*)<\/.*/\1/;
		p;
	}' \
| while read x; do
	echo "${osa}/release/${x}.html"
done \
| xargs wget
