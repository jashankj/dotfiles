#!/bin/sh
# suppress-ibus-hotkeys --- reconfigure ibus to disable its hotkeys
#
# :See-Also:
#   + :file:`/usr/share/glib-2.0/schemas/org.freedesktop.ibus.gschema.xml`
#     which defines the configuration schema, and default key bindings.

gsettings_set() {
  echo + gsettings set "$1" "$2" "$3"
  gsettings set "$1" "$2" "$3"
}

for schema_key in \
	.general.hotkey/disable-unconditional \
	.general.hotkey/enable-unconditional \
	.general.hotkey/next-engine \
	.general.hotkey/next-engine-in-menu \
	.general.hotkey/prev-engine \
	.general.hotkey/previous-engine \
	.general.hotkey/trigger \
	.general.hotkey/triggers \
	.panel.emoji/hotkey \
	.panel.emoji/unicode-hotkey
do
	gschema="${schema_key%%/*}"
	gkey="${schema_key##*/}"
	gsettings_set "org.freedesktop.ibus${gschema}" "${gkey}" "[]"
done
