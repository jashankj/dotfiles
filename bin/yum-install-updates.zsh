#!/usr/bin/env zsh
#
# NAME: yum-install-updates --- install those updates we can accept
# SYNOPSIS: yum-install-updates
#
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#

exec \
${YUM-dnf5} update \
	--best --allowerasing \
	--disablerepo pgdg-common \
	$(yum-list-acceptable-updates) \
	"$@"
