#!/usr/bin/env zsh
# NAME: abbr --- print an abbreviation
# SYNOPSIS: abbr <term>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

if [ "X$1" = "X" ]
then
	usage
fi

exec grep -i "^$1" "${HOME}/.abbr"
