#!/bin/sh
# autotools build environment

autoswc_base="${HOME}/lib/autoswc"

: ${AUTOSWC_CACHE_NAME:="default"}
: ${AUTOSWC_CACHE_FILE:="${autoswc_base}/db/config.cache.${AUTOSWC_CACHE_NAME}"}
: ${AUTOSWC_CONFIG_SITE:="${autoswc_base}/config.site"}

exec \
ionice -c3 \
nice -n20 \
env \
	CONFIG_SITE="${AUTOSWC_CONFIG_SITE}" \
	AUTOSWC_CACHE_FILE="${AUTOSWC_CACHE_FILE}" \
	AUTOSWC_CACHE_NAME="${AUTOSWC_CACHE_NAME}" \
"$@"
