#!/usr/bin/env zsh
# NAME: pinentry --- attempt to invoke an appropriate pinentry service.
#
# !!!  This really should invoke something *much* simpler than zsh.  !!!

[ ! -z "${INSIDE_EMACS}" ] &&
[ -e "${TMPDIR}/emacs${UID}/pinentry" ] &&
command -v "$(which pinentry-emacs)" > /dev/null &&
	exec "$(which pinentry-emacs)" "$@"

( ldconfig -p | grep -q libgtk-x11-2.0.so.0 ) &&
command -v "$(which pinentry-gtk-2)" > /dev/null &&
	exec "$(which pinentry-gtk-2)" "$@"

( ldconfig -p | grep -q libQt5Gui.so.5 ) &&
command -v "$(which pinentry-qt)" > /dev/null &&
	exec "$(which pinentry-qt)" "$@"

( ldconfig -p | grep -q libgcr-base-3.so.1 ) &&
command -v "$(which pinentry-gnome3)" > /dev/null &&
	exec "$(which pinentry-gnome3)" "$@"

( ldconfig -p | grep -q libfltk.so.1.3 ) &&
command -v "$(which pinentry-fltk)" > /dev/null &&
	exec "$(which pinentry-fltk)" "$@"

tty >/dev/null 2>&1 &&
	exec "$(which pinentry-curses)" "$@"

tty >/dev/null 2>&1 &&
	exec "$(which pinentry-tty)" "$@"

# no idea how to display, abandon ship
exit 1
