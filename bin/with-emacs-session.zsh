#!/usr/bin/env zsh
# NAME: with-emacs-session --- set EMACS_SESSION under an emacs
# SYNOPSIS: with-emacs-session <session> [<emacs-args>]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ "X$1" = "X" ] && usage
emacs_session="$1"; shift

exec \
env EMACS_SESSION="${emacs_session}" \
emacs --debug-init "$@"
