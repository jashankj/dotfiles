#!/usr/bin/env zsh
# compile a C program with AddressSanitizer extensions
# derived from andrewt@'s cs1911 dcc,  May 2013
#
# 2015-04-09	Jashank Jeremy <jashankj@cse.unsw.edu.au>
# 2017-11-01	Jashank Jeremy <jashankj@cse.unsw.edu.au>
# 2018-01-22	Jashank Jeremy <jashankj@cse.unsw.edu.au>
# 	fix up compiler-rt
#
# 2018-04-28	Jashank Jeremy <jashankj@cse.unsw.edu.au>
#   portability hacks.
#
# 2018-11-25	Jashank Jeremy <jashankj@cse.unsw.edu.au>
#   support the really primitive leak2521 leak checker.  lsan doesn't
#   work at all, and other leak checkers don't work reliably on i386:
#   tcmalloc produces hideously useless output; Valgrind explodes.
#
# 2018-12-07	Jashank Jeremy <jashankj@cse.unsw.edu.au>
#   clean up warning management; add more information about the warnings
#   we disable (given we enable them all anyway).
#
# 2019-03-08	Jashank Jeremy <jashankj@cse.unsw.edu.au>
#   set up shared libasan, to avoid bloating output binaries with the
#   ~1.5MB ASan runtime.  disable the really primitive leak2521 leak
#   checker on non-32-bit userland.
#
# 2019-03-11	Jashank Jeremy <jashankj@cse.unsw.edu.au>
#   put shared libasan behind a feature flag, because apparently the
#   workstation environment doesn't match login servers.
#
# 2019-11-16	Jashank Jeremy <jashankj@cse.unsw.edu.au>
#   disable all GNU warnings; add more sanitizer checks; improved shared
#   libasan search by asking clang (based on `c_check' improvements).

#set -x

path=(
	/bin  /usr/bin  /usr/local/bin
	/sbin /usr/sbin /usr/local/sbin
)

# pick up my LLVM bundle
if [ "X${LLVM_ROOT}" != "X" ]
then
	clang=${LLVM_ROOT}/bin/clang
else
	# LLVM_ROOT=/usr/lib/llvm-3.8
	clang=${CLANG:-clang}
fi

c3path=$(dirname $(realpath "$0"))

#path+=(${LLVM_ROOT}/bin)

target="$(uname)"
userland_bitness="$(file $(which file) | awk '{print $3}')"

c3flags=()


########################################################################
# Warnings

# shhh, I know we're doing weird flags.
c3flags+=-Qunused-arguments

# Pick a reasonable default C standard.
c3flags+=-std=gnu11

# Turn on every possible warning in Clang; the `-Weverything' flag is a
# special case to do this.  We then explicitly turn off the warnings we
# aren't especially interested in.
c3flags+=(
	-Weverything

	# - padded_struct_field
	#   (Warning, DefaultIgnore, InGroup<Padded>)
	#   "padding {struct|interface|class} %1 "
	#   "with %2 {byte|bit}s to align %4"
	#
	# - padded_struct_anon_field
	#   (Warning, DefaultIgnore, InGroup<Padded>)
	#   "padding {struct|interface|class} %1 "
	#   "with %2 {byte|bit}s to align anonymous bit-field"
	#
	# - padded_struct_size
	#   (Warning, DefaultIgnore, InGroup<Padded>)
	#   "padding size of %0 with %1 %{byte|bit}s "
	#   "to alignment boundary"
	#
	# - unnecessary_packed
	#   (Warning, DefaultIgnore, InGroup<Padded>)
	#   "packed attribute is unnecessary for %0"
	-Wno-padded

	# - pp_macro_is_reserved_id
	#   (Warning, DefaultIgnore, InGroup<ReservedIdAsMacro>)
	#   "macro name is a reserved identifier"
	#
	#   Warns for `_' and `__' prefixes on macros, which some of our
	#   code (e.g., `test1511.h') uses.
	-Wno-reserved-id-macro

	# - typecheck_zero_array_size
	#   (Extension, InGroup<ZeroLengthArray>)
	#   "zero size arrays are an extension"
	-Wno-zero-length-array

	# - pp_macro_not_used
	#   (Warning, DefaultIgnore, InGroup<"unused-macros">)
	#   "macro is not used"
	#
	#   Disabled 2018-11-26.
	-Wno-unused-macros

	# - pp_disabled_macro_expansion
	#   (Warning, DefaultIgnore, InGroup<"disabled-macro-expansion">)
	#   "disabled expansion of recursive macro"
	#
	#   Disabled 2018-12-06 to deal with the "#define Item Item" hack.
	-Wno-disabled-macro-expansion

	# - warn_zero_size_struct_union_compat
	#   (Warning, DefaultIgnore, InGroup<CXXCompat>)
	#
	#   "{empty }{struct|union} has size 0 in C, "
	#   "{size 1|non-zero size} in C++"
	#
	#   Disabled 2019-01-04; 3c is unlikely to be used for code that
	#   cares about C++ ABI compatibility.
	-Wno-c++-compat

	# - GNU C extension warnings disabled en-masse.  We have enough
	#   strange shenanigans that we can validly disable the lot.
	#   Previously, we had disabled all of:
	#
	#     - gnu_empty_initializer
	#     - ext_empty_struct_union
	#     - ext_no_named_members_in_struct_union
	#     - gnu_statement_expr
	#     - ext_missing_varargs_arg
	#     - ext_paste_comma
	#
	#   Disabled 2019-11-16.
	-Wno-gnu
)


########################################################################
# Debugging

c3flags+=(
	-g

	# GCC hints not needed because we're flying clang
	#-ggdb3 -gdwarf-2

	# enable AddressSanitizer and UndefinedBehaviourSanitizer
	-fsanitize=address
	-fsanitize=undefined
	# Add some additional sanity checks.
	-fsanitize-address-use-after-scope
	-fsanitize-memory-track-origins
	-fsanitize-stats

	# ASan and UBSan can, when they error out, produce stack traces...
	# make those as useful as possible
	-fno-omit-frame-pointer
	-fno-optimize-sibling-calls

	# dcc used to have these ... do we still need them?
	#-fno-common
	#-funwind-tables
)

# SHARED_LIBASAN=1
if [ ! -z $SHARED_LIBASAN ]
then
	clang_rt_dir=$($clang -print-resource-dir)/lib/$(uname | tr A-Z a-z)
	c3flags+=(
		-shared-libasan -pthread
		-Wl,-rpath,$clang_rt_dir
	)
fi

########################################################################
# System-specific hackery.

[ $target = "Linux" ] &&
	c3flags+=(-ldl)

# CSEbian used to be badly broken: LLVM tools weren't set up correctly.
# With support for those tools maturing over the last few years, the
# amount of specific fixing we need has *radically* reduced.
if [ -d /var/conform ] && [ $userland_bitness = "32-bit" ]
then
	c3flags+=(
		# We must specify target and architecture because detection of
		# the 32-bit userland is rather flaky.
		-m32 -target i386-unknown-linux-gnu
	)
fi

# If GCC is available, clang falls back to using libgcc as the runtime
# support library.  We would prefer the (more complete) compiler-rt:
# c3flags+=(--rtlib=compiler-rt -lgcc -lgcc_s)

########################################################################
# Compiler environment setup -- -lflags, -Dflags
c3flags+=(
	# We prefer to avoid precise POSIX and irritating ISO C compliance,
	# so make available as much of the C implementation's features as we
	# possibly can.  expressivity > standards compliance.
	-D_DEFAULT_SOURCE
	-D_BSD_SOURCE
	-D_SVID_SOURCE
	-D_GNU_SOURCE

	# Everybody assumes `-lm' is around.
	-lm

	# "[Add] all symbols to the dynamic symbol table."
	-rdynamic
)


########################################################################
# Support the terrible leak checker (obsoleted by LeakSanitizer):

if [ "X$1" = "X+leak" ] && [ $userland_bitness = "32-bit" ]
then
	c3flags+=(
		-Wl,-wrap,{malloc,calloc,free}
		-Wl,-wrap,{realloc,reallocarray}
		-Wl,-wrap,{strdup,strndup}
		"${c3path}/../lib/3c/leak2521.c"
	)
	shift
fi


########################################################################
# Invoke the compiler!

[ "$(basename "$0")" = "3x" ] && echo ${clang} ${c3flags} "$@"
exec ${clang} ${c3flags} "$@"
