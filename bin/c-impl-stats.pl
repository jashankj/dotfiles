#!/usr/bin/env perl
# NAME: c-impl-stats --- get some simple C program statistics
# SYNOPSIS: c-impl-stats <file.c ...>

use strict;
use warnings;

use Data::Dumper;
use File::Temp 'tempfile';
use IPC::Open3;
use JSON;
use Symbol 'gensym';

my $clang_query = 'clang-query';
my $tokei       = 'tokei';
if (0) {
	$clang_query = 'clang-query-11';
	$tokei       = '/home/jashankj/bin.linux-x86_64/tokei';
}

my $source_file;

sub count_functions {
	my ($file) = @_;
	my $n_functions;
	my $pid = open3(
		my $cq_in,
		my $cq_out,
		my $cq_err = gensym,
		$clang_query,
		'--extra-arg="-fno-color-diagnostics"',
		'-c', 'set output diag',
		'-c', 'match functionDecl(allOf(isDefinition(), isExpansionInMainFile()))',
		$file
	);

	$cq_in->close;
	while (<$cq_out>) {
		if (/^([[:digit:]]+) matches.$/) { $n_functions = $1; }
	}
	$cq_out->close;
	$cq_err->close;
	waitpid($pid, 0);

	return $n_functions;
}

sub get_main_location {
	my ($file) = @_;
	my $main_location;
	my $pid = open3(
		my $cq_in,
		my $cq_out,
		my $cq_err = gensym,
		$clang_query,
		'--extra-arg="-fno-color-diagnostics"',
		'-c', 'set output dump',
		'-c', 'match functionDecl(isMain())',
		$file
	);

	$cq_in->close;
	while (<$cq_out>) {
		if (/^FunctionDecl 0x[0-9a-f]+ <[^:]+:(\d+):(\d+), line:(\d+):(\d+)>/) {
			# $1:$2 -- start line, column; $3:$4 -- end line, column
			$main_location = [$1, $2, $3, $4];
		}
	}
	$cq_out->close;
	$cq_err->close;
	waitpid($pid, 0);

	return $main_location;
}

sub run_tokei {
	my ($file) = @_;
	my $tokei_obj;
	my $pid = open3(
		my $t_in,
		my $t_out,
		my $t_err = gensym,
		$tokei,
		'-o', 'json',
		'-f',
		$file
	);
	$t_in->close;
	$tokei_obj = decode_json <$t_out>;
	$t_out->close;
	$t_err->close;
	waitpid($pid, 0);
	return $tokei_obj;
}

sub extract_main_to_tempfile {
	my $source_file = shift;

	my $main_location   = get_main_location $source_file;
	return unless defined $main_location;

	my $tmpf            = File::Temp->new(SUFFIX => '.c');
	open my $srcf, '<', $source_file or die "$?";
	$srcf->getline foreach (1 .. ($main_location->[0] - 1));
	$tmpf->print($srcf->getline)
		foreach ($main_location->[0] .. ($main_location->[2] - 1));
	$srcf->close;

	return $tmpf;
}

foreach my $source_file (@ARGV) {
	unless (-e $source_file) {
		warn "'$source_file' doesn't exist\n"; next }
	unless (-r $source_file) {
		warn "'$source_file' doesn't seem to be readable\n"; next }
	unless (-T $source_file) {
		warn "'$source_file' doesn't look like text\n"; next }

	my $n_functions     = count_functions $source_file;

	# {"blanks":166,"blobs":{},"code":627,"comments":202}
	my $tokei_obj       = run_tokei $source_file;
	my $tokei_stats     = $tokei_obj->{'C'}->{'reports'}->[0]->{'stats'};
	my $n_source_lines  = $tokei_stats->{'code'};
	my $n_comment_lines = $tokei_stats->{'comments'};
	my $n_blank_lines   = $tokei_stats->{'blanks'};
	my $n_total_lines   =
		$n_source_lines + $n_comment_lines + $n_blank_lines;

	my $tmpf                 = extract_main_to_tempfile $source_file;
	my $have_main            = defined $tmpf;
	my $n_main_source_lines;
	my $n_main_comment_lines;
	my $n_main_blank_lines;
	my $n_main_total_lines;
	if ($have_main) {
		my $tokei_main_obj    = run_tokei $tmpf->filename;
		my $tokei_main_stats  =
			$tokei_main_obj->{'C'}->{'reports'}->[0]->{'stats'};
		$n_main_source_lines  = $tokei_main_stats->{'code'};
		$n_main_comment_lines = $tokei_main_stats->{'comments'};
		$n_main_blank_lines   = $tokei_main_stats->{'blanks'};
		$n_main_total_lines   =
			$n_main_source_lines +
			$n_main_comment_lines +
			$n_main_blank_lines;
	}

	printf "=== $source_file ===\n";
	format STDOUT_lines =
functions:              @<<<<<
                        $n_functions
lines, source:          @<<<<<
                        $n_source_lines
lines, comments:        @<<<<<
                        $n_comment_lines
lines, blank:           @<<<<<
                        $n_blank_lines
lines:                  @<<<<<
                        $n_total_lines
.

	format STDOUT_main =
`main' lines, source:   @<<<<<
                        $n_main_source_lines
`main' lines, comments: @<<<<<
                        $n_main_comment_lines
`main' lines, blank:    @<<<<<
                        $n_main_blank_lines
`main' lines:           @<<<<<
                        $n_main_total_lines
.

	$~ = 'STDOUT_lines'; write STDOUT;
	if ($have_main) { $~ = 'STDOUT_main'; write STDOUT; }
}
