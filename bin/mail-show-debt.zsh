#!/usr/bin/env zsh
# NAME: mail-show-debt --- show the amount of mail in debt
# SYNOPSIS: mail-show-debt

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

old_total_debt=13878

total_inboxes=0
total_debt=0

echo -n $(date +%FT%T)
for repo in $(ccat ~/etc/mail.repos)
do
	[ "$repo" = conglomerate ] && continue
	inbox="${repo}.INBOX"
	case "$repo" in
		(emeralfel|gmail|unsw)
			archive="${repo}.Debt"    ;;
		*)	archive="${repo}.Archive" ;;
	esac
	n_inbox="$(doveadm search mailbox "${inbox}"   2>/dev/null | wc -l)"
	n_debt="$( doveadm search mailbox "${archive}" 2>/dev/null | wc -l)"

	printf "\t%-12s%8d%8d" "${repo}" "${n_inbox}" "${n_debt}"
	(( total_inboxes += n_inbox ))
	(( total_debt    += n_debt  ))
done
printf "\t%-12s%8d%8d (%5d)\n" "... totals:" "${total_inboxes}" "${total_debt}" "$(( total_debt-old_total_debt ))"
