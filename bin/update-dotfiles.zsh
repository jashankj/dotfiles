#!/usr/bin/env zsh
# NAME: update-dotfiles --- push dotfiles onto my systems
# SYNOPSIS: update-dotfiles [noremote] [noagent]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

SSHFLAGS="-A"
REMOTES=1

while [[ $# -gt 0 ]]
do
	case "-$1" in
		-noremote) REMOTES=0 ;;
		-noagent)  SSHFLAGS="" ;;
		*) usage ;;
	esac
	shift
done

HOSTS=()
HOSTS+=(alyzon jaenelle inara)
HOSTS+=(santiago) # temporary AOS dev system

if [[ $REMOTES = 1 ]]
then
#	HOSTS+=(cse-wi) # special effort on cse
#	HOSTS+=(zed) # zed no longer on the net
	HOSTS+=(fort)
fi

for i in $HOSTS
do
	echo "=== $i"
	# something weird is going on here...
	(ssh ${SSHFLAGS} ${i} git stash && \
	 ssh ${SSHFLAGS} ${i} git pull -r && \
	 ssh ${SSHFLAGS} ${i} git stash pop)
done
