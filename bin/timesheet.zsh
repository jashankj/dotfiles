#!/bin/zsh

tasks=`wc -l ~/.time/taskmgr-tasks | awk '{ print $1 }'`

cat ~/.time/taskmgr-tasks \
	| sed -e "s/	/ :: /g" \
	| dmenu -nf grey80 -fn 'Monospace-12' -i -l ${tasks} -p "Task:" \
	| sed -Ee 's/^(.*) :: (.*) :: (.*)$/'`date +%s`'	\1	\2	\3/' \
	>> ~/.time/taskmgr-log
