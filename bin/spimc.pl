#!/usr/bin/env perl
# NAME: spimc --- use Clang to output SPIM-friendly MIPS assembly
# SYNOPSIS: spimc [clang-flags]
# EXAMPLE:  spimc x.c > x.s && spim -file x.s

use v5;
use strict;
use warnings;

########################################################################
package spimc;

our @FLAGS = ();
# yes, we'll have MIPS code, thanks
push @FLAGS, '-target', 'mips', '-march=mips32';
# SPIM uses approximately this ABI
push @FLAGS, '-mabi=32';
# use as little as possible -- go -ffreestanding!
push @FLAGS, '-ffreestanding';
push @FLAGS, '-isystem', "$ENV{'HOME'}/lib/spim";

# turn off executable layout shenanigans: PLT, GOT, etc.
#[gcc]	push @FLAGS, '-mno-plt';
push @FLAGS, '-fno-plt';
push @FLAGS, '-mno-xgot';
#[gcc]	push @FLAGS, '-mno-shared';
push @FLAGS, '-mno-abicalls';

# turn off alternative uarch support: no microMIPS or MIPS16
#[gcc]	push @FLAGS, '-mno-interlink-compressed';
#[gcc]	push @FLAGS, '-mno-interlink-mips16';

# turn off soft-float; turn off unsupported features
push @FLAGS, '-mhard-float';	# no soft-FP
#[gcc]	push @FLAGS, '-mno-llse';	# no LL, SE, SYNC
#[gcc]	push @FLAGS, '-mno-synci';	# no SYNCI
#[gcc]	push @FLAGS, '-mno-split-addresses'; # no %HI/%LO

# turn off some unhelpful directives
push @FLAGS, '-mno-ginv';
push @FLAGS, '-mno-gpopt';	# no %gp_rel

# disable unsupported ASEs
push @FLAGS, '-mno-dsp';
push @FLAGS, '-mno-dspr2';
#[gcc]	push @FLAGS, '-mno-smartmips';
#[gcc]	push @FLAGS, '-mno-dmx';
#[gcc]	push @FLAGS, '-mno-mips3d';
push @FLAGS, '-mno-micromips';
push @FLAGS, '-mno-mt';
#[gcc]	push @FLAGS, '-mno-mcu';
#[gcc]	push @FLAGS, '-mno-eva';
push @FLAGS, '-mno-virt';
#[gcc]	push @FLAGS, '-mno-xpa';

########################################################################
package spimc::Node;

sub new {
	my $class = shift;
	my $location = shift;
	my $spelling = shift;
	my $self = {
		location => $location,
		spelling => $spelling
	};
	bless $self, $class;
	$self
}

sub parse {
	my $class = shift;
}

package spimc::Node::Comment;
push our @ISA, qw(spimc::Node);
package spimc::Node::Instruction;
push our @ISA, qw(spimc::Node);
package spimc::Node::Directive;
push our @ISA, qw(spimc::Node);

########################################################################
package spimc;

use IPC::Open2;

sub main {
	my ($clang0, $clang1);
	my $pid = open2 $clang1, $clang0,
		'clang', @spimc::FLAGS, '-o', '-', '-S', @_;

	close $clang0;
	my ($bad_section, $la_reg, $la_loc);

	my @insns = ();
	my $line = 0;
	while (<$clang1>) {

	##### and now, more tales of the evil mangler #####

	# bad directives, drop
	if (/\s\.(p2align|type|size|ident|comm)/)
		{ goto NOPRINT }

	# strings wind up in .rodata -- move them back to .data
	s@\.section\s*\.rodata.*@\.data@;
	# sometimes we wind up in .bss -- move back to .data
	s@\.bss@\.data@;

	# most .section markers (and their contents) are bad;
	# drop until the next good one
	if (/\s\.section/)
		{ $bad_section = 1
		; goto NOPRINT }
	if ($bad_section)
		{ if (/\s\.k?(text|data)/)
			{ $bad_section = 0 }
		  else
			{ goto NOPRINT }
		}

	# clang insists on emitting %hi and %lo sequences --
	# instead, do a hacky rewrite of a LUI/ADDIU pair into LA.
	# very fragile! looks like -O3 breaks this ^_^
	if (/lui\s*(\$\d+),\s*%hi\(([^\)]+)\)/) { $la_reg = $1; $la_loc = $2; next }
	if (/addiu\s*(\$\d+),\s*(\$\d+),\s*%lo\(([^\)]+)\)/)
		{ die "nope la_reg $la_reg != $2" unless $la_reg eq $2
		; die "nope la_loc $la_loc != $3" unless $la_loc eq $3
		; print "\tla\t$1, $la_loc\t\t",
			"# LUI + ADDIU rewritten\n"
		; next }

	# terms of the form
	#	lui	$1, %hi(pRows)
	#	lw	$4, %lo(pRows)($1)
	# can be turned into
	#	lw	$4, pRows

	# clang mis-spells directive .ASCIIZ
	s@\.asciz@.asciiz@g;

	# clang mis-spells directive .WORD
	s@\.4byte@.word@g;

	# ok ... probably safe to print now
	print; next;

	NOPRINT:
	print "#XXX#", $_;

	};

	close $clang1;
	waitpid $pid, 0;

	0
}

########################################################################

package main;
exit &spimc::main(@ARGV);
