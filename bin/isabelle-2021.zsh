#!/usr/bin/env zsh
# NAME: isabelle-2021 --- run Isabelle 2021
# SYNOPSIS: isabelle-2021

exec \
env ISABELLE_IDENTIFIER=Isabelle21 \
/src/isabelle/Isabelle2021/bin/isabelle "$@"
