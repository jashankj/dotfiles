#!/usr/bin/env zsh
# NAME: pacman-depends --- expand the entire dependency tree in Pacman
# SYNOPSIS: pacman-depends [package...]
#
# Expanding outwards until the result converges.
#
# Some hints from `zshexpn(1)' about what's going on:
#  * Parameter Expansion:
#  ** Parameter Expansion Flags:
#     `o':  Sort the resulting words in ascending order
#     `u':  Expand only the first occurrence of each unique word.
#  * Filename Generation:
#  ** Globbing Flags:
#     `s', `e':   more or less, `^' and `$'.

set -o err_exit
set -o extended_glob
# set -o xtrace

odeps=()
deps=( "$@" )

# If there were no new dependencies, we're done.
# If there are now new dependencies, expand them too.
while [[ ${deps} != ${odeps} ]]
do
	odeps=( ${deps} )
#	print $deps

	# Add all hard dependencies of the current dependency set.
	deps+=( $(
		pacman -Qi ${odeps} \
		| grep '^Depends' \
		| cut -d: -f2 \
		| sed -Ee 's@None@@g'
	) )

	# |> sort |> uniq
	deps=( ${(ou)deps} )
done

print ${deps}
