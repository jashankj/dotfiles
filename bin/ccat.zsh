#!/usr/bin/env zsh
# NAME: ccat --- cat with comments
# SYNOPSIS: ccat [files]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"

exec sed -E -e 's/[[:space:]]*#.*//g; /^[[:space:]]*$/d;' "$@"
