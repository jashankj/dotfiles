#!/bin/sh

if [ $# != 1 ] && [ $# != 2 ] && [ $# != 3 ]
then
	echo "usage: make-it <tree> <config> [-jN]" >&2
	exit 64 #EX_USAGE
fi

tree=$1
config=$2
jn=${3--j$(getconf _NPROCESSORS_ONLN)}

ccat() {
	sed -E -e 's/#.*//g; /^[[:space:]]*$/d;'
}

treedir=/src/freebsd/src/${tree}
echo $tree | grep -q '^/' && \
	treedir=$tree

exec \
nice -n 20 \
env -i \
env TERM=$TERM \
    PATH=/usr/bin:/usr/sbin:/bin:/sbin:. \
    MAKEOBJDIRPREFIX=/var/obj/${config} \
    ${MAKE-make} ${jn} -C ${treedir} \
	buildworld buildkernel \
	$(ccat < env.${config})
