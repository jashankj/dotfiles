#!/usr/bin/env zsh
# NAME: dpkg-rdepends --- list package dependencies, recursively
# SYNOPSIS: dpkg-rdepends <package...>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 0 ] && usage

apt-cache depends \
	--recurse \
	--no-recommends --no-suggests --no-enhances \
	--no-conflicts --no-breaks --no-replaces \
	--no-pre-depends "$@" \
| egrep -v '^[ \t]+' \
| egrep -v '^<[^>]+>$' \
| xargs dpkg-query -W -f='${binary:Package}\n' \
| sort | uniq
