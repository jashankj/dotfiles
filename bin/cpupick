#!/bin/bash
# NAME: cpupick --- randomly select `ncpus' processors, for use with `taskset(1)'.
# SYNOPSIS: cpupick <ncpus>
#
# Derived from plinich@'s `userresourcelimits.sh'.
#     <https://www.cse.unsw.edu.au/~plinich/>
# Meddled with to give fewer subshells, less temporary files,
# and and much less Shellcheck screaming.

CPUSMAX="$1"; shift

# Set PATH so nothing tricky can happen
export PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Get the list of CPUs available (as a hexadecimal mask)
cpulist="$(taskset -p $$)" || exit 1
case "${cpulist}" in
	("pid "*"'s current affinity mask: "*) ;;
	(*) exit 1 ;;
esac

# Extract the mask and convert it from hexadecimal to decimal.
cpumaskhex=${cpulist/#* }
cpumaskdec=$((0x$cpumaskhex + 0 ))

# Populate an array with one element per available CPU
declare -A cpus
i=0; count=0; m=$cpumaskdec
while [[ $m -gt 0 ]]
do
	(( f = m & 1 ))
	if [[ $f -ne 0 ]]
	then
		cpus[$count]=$i
		(( count++ ))
	fi
	(( i++ )); (( m >>= 1 ))
done

# Extract a requisite number of CPUs from random points in the array,
# and build up a mask as we go.
n=0; cpusreqd=$CPUSMAX
while [[ $count -gt 0 ]] && [[ $cpusreqd -gt 0 ]]
do
	# Pick a random CPU.
	(( i = (RANDOM * count) / 32768 ))
	s=${cpus[$i]}
#echo "Selected CPU $i ==> $s"

	(( n |= 1 << s ))
	(( count-- ))
	cpus[$i]=${cpurand[$count]}
	(( cpusreqd-- ))
done

# Print the mask that was calculated; in hex again, to feed taskset.
printf "%x\n" "$n"

# Done.
exit 0
