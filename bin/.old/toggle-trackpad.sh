#!/bin/sh

#state=$(xinput list-props 'SynPS/2 Synaptics TouchPad' | grep "(135)" | awk '{ print $4 }')
#
#if [ X$state = X1 ]
#then
#    echo -n "[disabling touchpad"
#    xinput disable 'SynPS/2 Synaptics TouchPad'
#    echo "]"
#else
#    echo -n "[enabling touchpad"
#    xinput enable 'SynPS/2 Synaptics TouchPad'
#    echo "]"
#fi

state=$(synclient | grep AreaBottomEdge | awk '{ print $3 }')
if [ X$state = X0 ]
then
    synclient AreaBottomEdge=2500
else
    synclient AreaBottomEdge=0
fi
