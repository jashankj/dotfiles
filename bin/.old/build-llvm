#!/usr/bin/env zsh
########################################################################
# NAME: build-llvm --- build a LLVM + Clang binary bundle on CSE
# SYNOPSIS: build-llvm [(cmake|fetch|build|pkg|clean)]
#
# Builds in /var/tmp/$(whoami); no quota or AMD to deal with there.
# LLVM is ~2.0 GB; build artefacts ~3 GB.  XZ'd bindist is ~200 MB.
#
# 2017-10-28	Jashank Jeremy <{jashankj,z5017851}@cse.unsw.edu.au>
# 	Initial version.
#
# 2018-01-14	Jashank Jeremy <{jashankj,z5017851}@cse.unsw.edu.au>
# 	As most binaries are statically linked, attempt to bring down
# 	artefact size by deduplicating using `BUILD_SHARED_LIBS'.
#
# 2018-02-07	Jashank Jeremy <{jashankj,z5017851}@cse.unsw.edu.au>
# 	Disable libFuzzer, which can't handle the 32/64-bit hybrid
# 	environment properly (it has custom? build scripts which make 
# 	bad assumptions about the target host).
#
# 2018-09-28	Jashank Jeremy <{jashankj,z5017851}@cse.unsw.edu.au>
# 	Run the build within a `setarch(8)'d environment (using the
# 	`i386' alias) to hopefully get more 32-bit sanity.

set -e

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

CMAKE_DIST_VERS=3.12.2
CMAKE_DIST_MAJOR=$(echo ${CMAKE_DIST_VERS} | cut -d. -f1,2)

unset CFLAGS CXXFLAGS

NCPU=8 # probably could calculate this better

main()
{
	print_warning
	enforce_warning

	TMPDIR=/var/tmp/$(whoami)
	SRC=$TMPDIR/llvm

	mkdir -p ${TMPDIR}
	cd ${TMPDIR}

	case ".$1" in
		.cmake)	cmake ;;
		.fetch)	fetch ;;
		.build)	build ;;
		.pkg)	pkg ;;
		.clean)	clean ;;
		.)	cmake && fetch && build && pkg && clean ;;

		*)	usage
	esac
}

print_warning()
{
print '\033[7m\033[1m'
cat <<EOF
    !!!!!!!!!!!!!!!                                   !!!!!!!!!!!!!!!    
    !!! WARNING !!!    Building LLVM is intensive!    !!! WARNING !!!    
    !!!!!!!!!!!!!!!                                   !!!!!!!!!!!!!!!    
EOF
print '\033[0m'

cat <<EOF
It will hog ~$NCPU cores and ~5 GB of disk for ~2 hours on this system.
Don't even think about doing this on a login or VLAB server!
EOF
}

enforce_warning()
{
	HOST=$(hostname -s)
	HOST=${HOST-localhost}

	if	[ "${HOST}" = "wagner" ] \
	||	[ "${HOST}" = "weill" ] \
	||	[ "${HOST}" = "weber" ] \
	||	[ "${HOST}" = "wilson" ] \
	||	[ "${HOST}" = "williams" ] \
	||	[ "${HOST}" = "vx0" ] \
	||	[ "${HOST}" = "vx1" ] \
	||	[ "${HOST}" = "vx2" ] \
	||	[ "${HOST}" = "vx3" ] \
	||	[ "${HOST}" = "vx4" ]
	then
		errx EX_SOFTWARE "cowardly refusing to build on ${HOST}"
	fi
}

cmake() {

########################################################################
# build CMake

curl -L https://cmake.org/files/v${CMAKE_DIST_MAJOR}/cmake-${CMAKE_DIST_VERS}.tar.gz \
	| zcat \
	| tar xf -
mv cmake-* cmake
( \
	cd cmake && \
	./bootstrap --parallel=$NCPU && \
	make -j$NCPU \
) || exit 1

}

fetch() {

########################################################################
# fetch LLVM

for i in \
	llvm: \
	libcxx:projects/libcxx \
	libcxxabi:projects/libcxxabi \
	compiler-rt:projects/compiler-rt \
	lld:tools/lld \
	lldb:tools/lldb \
	clang:tools/clang \
	clang-tools-extra:tools/clang/tools/extra
do
	echo ${i} \
		| tr ':' ' ' \
		| read proj dir
	git clone https://git.llvm.org/git/${proj}.git $SRC/${dir}
done

}

build() {

########################################################################
# build LLVM

#		-D CMAKE_C_COMPILER=clang-4.0 \
#		-D CMAKE_CXX_COMPILER=clang++-4.0 \
( \
	mkdir -p obj && \
	cd obj && \
	i386 ../cmake/bin/cmake \
		-D CMAKE_BUILD_TYPE=MinSizeRel \
		-D CMAKE_C_COMPILER=gcc-7 \
		-D CMAKE_CXX_COMPILER=g++-7 \
		-D LLDB_DISABLE_PYTHON=1 \
		-D LLDB_DISABLE_LIBEDIT=1 \
		-D COMPILER_RT_BUILD_LIBFUZZER=0 \
		-D BUILD_SHARED_LIBS=ON \
		-G Ninja \
		../llvm && \
	env TERM=dumb ninja -j$NCPU \
) || exit 1

}

pkg() {

########################################################################
# package LLVM
( \
	cd obj && \
	../cmake/bin/cpack -G TXZ && \
	xzcat LLVM-*-Linux.tar.xz \
		| xz -9cv > ~/llvm-bin.tar.xz \
) || exit 1

}

clean() {

########################################################################
# clean up

rm -rf $TMPDIR/cmake*
rm -rf $TMPDIR/llvm*
rm -rf $TMPDIR/obj*

}


main
