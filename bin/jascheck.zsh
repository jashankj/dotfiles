#!/usr/bin/env zsh
# NAME: jascheck --- jas' check script
# SYNOPSIS: jascheck

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

case $# in
0)	BIN=""	;;
1)	BIN="$1" ;;
*)	usage ;;;
esac

check_missing()
{
	if [ ! $1 $2 ]
	then
		errx 1 "no $2 here ... are you in the right directory?"
		exit 1
	fi
}

check_missing -d tests/

case "$BIN" in
*.c)
	check_missing -e "$BIN"
	BIN=$(basename "$BIN" .c)

	check_missing -e "Makefile"

	warnx "running make, just in case you didn't..."
	make "${BIN}" || exit 1

	check_missing -x "$BIN"
	;;

*.s)
	check_missing -e "$BIN"
	BIN=$(basename "$BIN" .s)
	;;

*)
	;;
esac

                   pattern=*.sh
[ ! -z "$BIN" ] && pattern=$BIN*.sh

setopt glob null_glob
tests=(tests/$~pattern)

if [ -z "$BIN" ]
then
	warnx "Running ${#tests} tests ..."
else
	warnx "Running ${#tests} tests on $BIN ..."
fi

ulimit -t 10
ulimit -f $(( 192 * 1024 * 1024 / 512 ))

npass=0
nfail=0
for tt in ${tests}
do
	t=$(basename $tt .sh)
	sh "$tt" "$BIN" 2>&1 | head -1000 > tests/$t.out
	if cmp -s tests/$t.exp tests/$t.out
	then
		echo Passed test $t
		(( npass = npass + 1 ))
	else
		echo Failed test $t
#		printf "%-35s%-35s\n\n" "Your Output" "Expected Output" > tests/$t.cmp
#		pr -m -t -w 70 tests/$t.out tests/$t.exp >> tests/$t.cmp
#		echo Check differences using \"cat tests/$t.cmp\"
		echo Check differences using \"diff tests/$t.exp tests/$t.out\"
		(( nfail = nfail + 1 ))
	fi
done

print -P "%B${#tests} test(s)%b:\t${npass} passed; ${nfail} failed."
[ ${nfail} -eq 0 ] && \
print -P "%B%F{green}All tests passed. You are awesome!%f%b"
