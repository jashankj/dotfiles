#!/bin/sh
# NAME: update-texlive-fonts --- link all TTF, OTF, PFB font files
# SYNOPSIS: update-texlive-fonts

cd /usr/local/texlive
mkdir -p fonts
cd fonts

find ../current/texmf-dist/fonts \
	-name \*.\[TtOo\]\[Tt\]\[Ff\] -o \
	-name \*.\[Pp\]\[Ff\]\[Bb\] \
| sort \
| xargs -L 1 -P 16 -t -I % ln -s %
