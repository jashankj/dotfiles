#!/usr/bin/env zsh
# NAME: dpkg-rprovides --- list files provided by package and dependencies
# SYNOPSIS: dpkg-rprovides <package...>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 0 ] && usage

dpkg-rdepends "$@" \
| xargs dpkg-query --listfiles \
| grep -v "does not contain any files|has no files" \
| sort | uniq
