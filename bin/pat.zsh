#!/usr/bin/env zsh

exec \
pygmentize \
	-f terminal16m \
	-O style=zenburn \
	-g \
	"$@" \
| less \
	--quit-at-eof --QUIT-AT-EOF \
	--quit-if-one-screen \
	--status-column \
	--no-init \
	--mouse
