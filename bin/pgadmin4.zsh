#!/usr/bin/env zsh
# NAME: pgadmin4 --- launch pgAdmin, fourth edition
# SYNOPSIS: pgadmin4 [...]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"

: ${PGADMIN_ROOT:=/usr/lib/pgadmin4}

exec \
env \
	PGADMIN_SERVER_MODE=OFF \
python3 "${PGADMIN_ROOT?}/pgAdmin4.py" "$@"
