#!/usr/bin/env zsh
# NAME: cmd-host ---- multicall to invoke <cmd>.<host>
# SYNOPSIS: <cmd> [args ...]

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

cmd="$(basename "$0")"
host="$(hostname -s)"

bin="$(dirname "$0")"

[ -x "${bin}/${cmd}.${host}" ] &&
exec "${bin}/${cmd}.${host}" "$@"

[ -x "$HOME/bin/${cmd}.${host}" ] &&
exec "${cmd}.${host}" "$@"

errx EX_USAGE "\`${cmd}' unavailable on ${host}"
