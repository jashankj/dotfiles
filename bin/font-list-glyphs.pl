#!/usr/bin/env perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib/perl";
use J::Proc;
# $J::Proc::VERBOSE_IO = 1;

use Encode;
use utf8;

my $proc = J::Proc->spawn('fc-query', "--format=\%{charset}", @ARGV);
my $line = $proc->recv;
my @F = split /\s+/, $line;

binmode STDOUT, ":utf8";
sub explain { my $x = shift; printf "\%x\t\%c\n", $x, $x; }
foreach my $F (@F) {
	my ($a, $b) = split /-/, $F, 2;
	$b = $a unless defined $b;
	$a = hex $a; $b = hex $b;
	foreach my $i ($a ... $b) { explain $i }
}
