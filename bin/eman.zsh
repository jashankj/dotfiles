#!/usr/bin/env zsh
# NAME: eman --- open Man pages in Emacs using `man'/`manual-entry'
# SYNOPSIS: eman <manpage-ref>
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/emacs"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

. "${ZDOTDIR}/.zsh/functions/emacsc"

case "$#" in
	(1)	x="(man \"$1\")" ;;
	(*)	usage ;;
esac

exec emacsc --suppress-output --eval "$x"
