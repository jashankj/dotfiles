#!/bin/sh
# NAME: poudstat --- janky poudriere status grabber tool
# SYNOPSIS: poudstat

oneshot=0
interval=10
rundir=latest

case "$#" in
	0) oneshot=1 ;;
	1) rundir="$1" ;;
	2) rundir="$1"
	   interval="$2"
	   ;;
esac


as_time() {
	echo $1 | awk '{ printf("%dh%02dm%02ds\n", $1/3600, ($1%3600/60), $1%60) }'
}

from_file_or_default() {
	file="$1"; shift
	if [ -e "$file" ]
	then
		cat "$file"
	else
		echo "$@"
	fi
}

global_status() {
	status=$( from_file_or_default .poudriere.status 'unknown:')
	now=$(    from_file_or_default .poudriere.snap_now      0)
	t_start=$(from_file_or_default .poudriere.started       0)
	queued=$( from_file_or_default .poudriere.stats_queued  0)
	built=$(  from_file_or_default .poudriere.stats_built   0)
	failed=$( from_file_or_default .poudriere.stats_failed  0)
	ignored=$(from_file_or_default .poudriere.stats_ignored 0)
	skipped=$(from_file_or_default .poudriere.stats_skipped 0)
	elapsed=$(( $(date +%s) - t_start ))

	[ $oneshot -eq 0 ] && tput ce
	printf '\033[1m%-24s\033[0m +%s | %d > %d !%d-%d*%d | %s\n' \
		$status $(as_time $elapsed) $queued $built $failed $ignored $skipped \
		"$(from_file_or_default .poudriere.snap_loadavg '[no load averages]')"
}


builder_status() {
	for builder in $(cat .poudriere.builders)
	do
		[ -e .poudriere.status.$builder ] || continue
		[ $oneshot -eq 0 ] && tput ce

		builder_id=${builder#0}

		cat .poudriere.status.$builder \
		| tr ':' '\t' \
		| while read b_status b_origin b_3 b_runtime b_5x
		  do # fucking hell, sh(1)

		runtime=''
		maybe_runtime=$(( now - b_runtime ))
		[ ${maybe_runtime} -ge 0 ] && \
			runtime="$(as_time ${maybe_runtime})"
		[ ${maybe_runtime} -lt 0 ] && \
			b_origin='' && b_status='idle'

		task_count=$(\
			wc -l .poudriere.status.$builder.journal% \
			| sed -e 's/^ *//' \
			| cut -d' ' -f1 )

		printf '[%02d:%8s] #%-5d %-16s%s\n' \
			${builder_id} "${runtime}" ${task_count} ${b_status} ${b_origin} \
		2>/dev/null

		done
	done
}

maybe_builder_status() {
	case "$status" in
		unknown:) ;;
		starting_builders:) ;;
		*) builder_status ;;
	esac
}

[ $oneshot -eq 0 ] && tput cl
while true
do
	(
		cd "$rundir" || {
			echo "$0: no such file or directory: $rundir" >&2
			exit 64 # EX_USAGE
		}

		[ $oneshot -eq 0 ] && tput cm 0 0

		{
			global_status
			[ -e .poudriere.builders ] &&
				maybe_builder_status
		} \
			| cut -b1-$(( $(tput cols) - 1 ))

		[ $oneshot -eq 1 ] && break
		sleep $interval
	)
done
