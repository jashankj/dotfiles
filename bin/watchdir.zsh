#!/usr/bin/env zsh

inotifywait --format="%e %w %f" --monitor -r "$@" |
while read e d f
do
	case "$e" in
		(CLOSE_*,CLOSE)
			echo "$d$f"
			;;
	esac
done
