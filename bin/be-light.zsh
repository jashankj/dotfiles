#!/usr/bin/env zsh

pathdir="$(dirname "$(realpath "$0")")"
path=(${path:#${pathdir}})
exec \
env GTK_THEME=Adwaita \
"$(basename "$0")" "$@"
