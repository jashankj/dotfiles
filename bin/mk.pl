#!/usr/bin/env perl

=head1 NAME

mk -- try to make something useful

=head1 SYNOPSIS

mk [target...]

=head1 DESCRIPTION

C<mk> identifies build systems that might be reasonable to invoke, and
invokes that build system.  It will recurse up a directory tree until it
finds an acceptable build system.

If the first flag has the form C<++builder>, only the I<builder>
requested will be enabled.

=cut

use v5;
use strict;
use warnings;
use feature 'say';
use feature 'signatures'; no warnings 'experimental::signatures';

use Config;
use Cwd;
use Data::Dumper;
use File::Basename;
use File::Spec;
use POSIX qw(PRIO_PROCESS);

sub do_that;
sub there_do;
sub there_do_that;

$0 = basename $0;

sub build_dir { return "obj." . $ENV{'OSARCH'} }

# Renice all our child processes down.
setpriority PRIO_PROCESS, 0, 19 or say "(not nice)";

#
# Preferred builder order.  A good ordering balances builders that are
# more likely to know that they're relevant and what they need to do,
# and how likely we are to encounter them.
#
my @ORDER = (
	'latexmk',
	'holmake',
	'make',
	'bazel',
	'cargo',
	'rake',
	'cmake',
	'meson',
	'ninja',
	'redo',
	'hpack', 'cabal', 'stack',
	'ant',
	'maven',
	'elm_make',
);

=head2 RECOGNISED BUILD SYSTEMS

A key part of C<mk> is being able to identify different build systems.

=over 4

=cut

my $BUILDERS = {};

########################################################################

=item L<ant(1)>

Apache Ant is a general-purpose build tool,
typically used for languages running on the Java VM.
C<mk> identifies it by the presence of
a file called C<build.xml>;
and passes all arguments on the command line to the build tool.
L<https://ant.apache.org/>

=cut

$BUILDERS->{'ant'} = {
	'do'  => sub($wd, $args) { there_do_that $wd, 'ant', @$args },
	'can' => sub($dir) { return (-e "$dir/build.xml") }
};


########################################################################

=item C<bazel>

Bazel is a general-purpose build tool
(though it has many hardwired opinions
which often limit its generality).
C<mk> identifies a workspace root
by the presence of any of the files
C<MODULE.bazel> or C<WORKSPACE.bazel>;
If command-line arguments are passed,
but they are only Bazel target names,
we invoke C<bazel build> on those targets;
otherwise, we pass all arguments to C<bazel>.
When no further arguments are passed,
C<bazel build ...> is invoked
I<in the current directory>.
L<https://bazel.build/>

=cut

# (no, I doubt this is actually correct...)
my $BAZEL_TARGET_RX = qr{
	\A
	(?:\@[^/]+)?		# alt-package (i.e., `@rules_cc+`)
	(?://[^:]+)?		# workspace path (i.e., `//beep/boop`)
	(?::.+)?			# target name (i.e., `:alembic`)
	\Z
}x;

$BUILDERS->{'bazel'} = {
	'do'  => sub($wd, $args) {
		there_do_that $ENV{'PWD'}, 'bazel', 'build', '...'
			if $#{$args} == -1;
		unshift @$args, 'build'
			unless grep { not m{$BAZEL_TARGET_RX} } @$args;
		there_do_that $wd, 'bazel', @$args
	},
	'can' => sub($dir) {
		-e "$dir/MODULE.bazel" ||
		-e "$dir/WORKSPACE.bazel"
	}
};


########################################################################

=item L<cabal(1)>

Cabal is a build tool for Haskell.
C<mk> identifies it by the presence of
either a file called C<cabal.project>, or
any file matching the glob C<*.cabal>.
If command-line arguments are passed,
they are all passed to the build tool;
otherwise, C<cabal build> is invoked.
L<https://cabal.readthedocs.io/>

=cut

$BUILDERS->{'cabal'} = {
	'do' => sub($wd, $args) {
		unshift @$args, 'build' if $#{$args} == -1;
		there_do_that $wd, 'cabal', @$args },
	'can' => sub($dir) {
		return (-e "$dir/cabal.project") || (() = glob "$dir/*.cabal");
	},
};


########################################################################

=item L<cargo(1)>

Cargo is a build tool for Rust.
C<mk> identifies it by the presence
of a file called C<Cargo.toml>;
If command-line arguments are passed,
they are all passed to the build tool;
otherwise, C<cargo build> is invoked.
L<https://doc.rust-lang.org/cargo/>

=cut

$BUILDERS->{'cargo'} = {
	'do' => sub($wd, $args) {
		unshift @$args, 'build' if $#{$args} == -1;
		there_do_that $wd, 'cargo', @$args },
	'can' => sub($dir) { return (-e "$dir/Cargo.toml") },
};


########################################################################

=item L<cmake(1)>

CMake is a general-purpose metabuild system,
typically used for C++ projects.
C<mk> identifies it by the presence
of a file called C<CMakeLists.txt>.
C<mk> runs its configuration pass
into the guessed build directory,
passing all command line arguments to it;
then invokes the build pass
on the build directory.
(This behaviour requires a recent CMake to be installed.)
L<https://cmake.org/>

=cut

$BUILDERS->{'cmake'} = {
	'do' => sub($wd, $args) {
		my $build_dir = build_dir;
		there_do      $wd, 'cmake', '-S', '.', '-B', $build_dir, @$args;
		there_do_that $wd, 'cmake', '--build', $build_dir },
	# TODO: look for top-level CMakeLists.txt
	'can' => sub($dir) { return (-e "$dir/CMakeLists.txt") },
};


########################################################################

=item C<elm make>

The Elm programming language uses
a tool called L<elm(1)> to orchestrate builds.
C<mk> identifies it by the presence of
a file called C<elm-package.json>;
and passes all arguments on the command line to the build tool.

=cut

$BUILDERS->{'elm_make'} = {
	'do' => sub($wd, $args) {
		there_do_that $wd, 'elm', 'make', @$args },
	'can' => sub($dir) { return (-e "$dir/elm-package.json") },
};


########################################################################

=item C<holmake>

Holmake is the build system for
the HOL4 logic environment.
C<mk> identifies it by the presence of
a file called C<Holmakefile>;
and passes all arguments on the command line to the build tool.
L<https://hol-theorem-prover.org/>

=cut

$BUILDERS->{'holmake'} = {
	'do' => sub($wd, $args) { there_do_that $wd, 'holmake', @$args },
	'can' => sub($dir) { return (-e "$dir/Holmakefile") },
};


########################################################################

=item C<hpack>

C<hpack> is a configuration generator for L<cabal(1)>.
C<mk> identifies it by the presence of
a file called C<package.yaml>;
and invokes C<hpack> to generate a C<*.cabal> file,
before degenerating to L<cabal(1)> described above.
L<https://github.com/sol/hpack>

=cut

$BUILDERS->{'hpack'} = {
	'do' => sub($wd, $args) {
		there_do $wd, 'hpack';
		&{$BUILDERS->{'cabal'}->{'do'}}($wd, $args) },
	'can' => sub($dir) { return (-e "$dir/package.yaml") },
};


########################################################################

=item C<latexmk>

C<latexmk> is a build automation system
for LaTeX documents.
C<mk> identifies it by the presence of
a file called C<.latexmkrc>;
and passes all arguments on the command line to the build tool.
L<https://ctan.org/pkg/latexmk>

=cut

$BUILDERS->{'latexmk'} = {
	'do' => sub($wd, $args) { there_do_that $wd, 'latexmk', @$args },
	'can' => sub($dir) { return -e "$dir/.latexmkrc" },
};


########################################################################

=item L<make(1)>

Make is a general-purpose build tool.
C<mk> identifies it by the presence of
files called C<makefile>, C<Makefile>, C<GNUmakefile>;
and uses L<file(1)> to attempt to identify the dialect,
before invoking L<make(1)>, L<gmake(1)>, or L<bmake(1)>,
passing all arguments on the command line to the build tool.
Note that flag C<-C> makes no sense to pass to Make here.
L<https://www.gnu.org/software/make/>;
L<https://www.crufty.net/help/sjg/bmake.html>

=cut

$BUILDERS->{'make'} = {
	'do' => sub($wd, $args) {
		# Taste the variant of Make we're interested in.
		my $make;
		foreach my $file (qw(makefile Makefile GNUmakefile)) {
			my $path = File::Spec->catfile($wd, $file);
			-e "$path" or next;
			my $type = `file -b "$path"`;
			$make = 'make';
			$make = 'bmake' if $type =~ /\ABSD makefile/;
			$make = 'gmake' if $type =~ /\AGNU makefile/;
			last;
		}

		# TODO: fix MAKEOBJDIR
#		if ($make eq 'bmake') {
#			$ENV{'MAKEOBJDIR'} = $ENV{'PWD'} . '/' . build_dir;
#		}

		if ($Config{osname} eq 'freebsd') {
			$make = ({
				'bmake' => 'make',
				'gmake' => 'gmake',
				'make'  => 'gmake'
			})->{$make};
		}

		there_do_that $wd, $make, @$args },

	'can' => sub($dir) {
		return (-e "$dir/makefile"
			or  -e "$dir/Makefile"
			or  -e "$dir/GNUmakefile") },
};


########################################################################

=item L<mvn(1)>

Apache Maven is a build tool for
languages running on the Java VM.
C<mk> identifies it by the presence of
a file called C<pom.xml>.
If command-line arguments are passed,
they are all passed to the build tool;
otherwise, C<mvn package> is invoked.
L<https://maven.apache.org/>

=cut

$BUILDERS->{'maven'} = {
	'do' => sub($wd, $args) {
		unshift @$args, 'package' if $#{$args} == -1;
		there_do_that $wd, 'mvn', @$args },
	'can' => sub($dir) { return (-e "$dir/pom.xml") },
};


########################################################################

=item L<meson(1)>

Meson is a general-purpose metabuild system.
C<mk> identifies it by the presence
of files called C<meson.build> I<and> C<meson_options.txt>:
both are required to find the root of the source tree.
C<mk> runs its configuration pass
into the guessed build directory,
passing all command line arguments to it;
then invokes the build pass
on the build directory.
L<https://mesonbuild.com/>

=cut

$BUILDERS->{'meson'} = {
	'do' => sub($src, $args) {
		my $obj = $src . '/' . build_dir;
		there_do $src, 'meson', 'setup', $obj, $src, @$args
			unless -d $obj;
		there_do_that $obj, 'meson', 'compile' },
	'can' => sub($dir) {
		return (-e "$dir/meson.build"
			and -e "$dir/meson_options.txt") },
};


########################################################################

=item L<ninja(1)>

Ninja is a general-purpose build system,
usually the target of a metabuild system:
L<cmake(1)> and L<meson(1)>, amongst others,
will choose to use L<ninja(1)> to execute the build.
C<mk> identifies it by the presence of
a file called C<build.ninja>;
and passes all arguments on the command line to the build tool.
L<https://ninja-build.org/>

=cut

$BUILDERS->{'ninja'} = {
	'do' => sub($wd, $args) { there_do_that $wd, 'ninja', @$args },
	'can' => sub($dir) { return (-e "$dir/build.ninja") },
};


########################################################################

=item L<rake(1)>

Rake is a general-purpose build system,
typically used for Ruby.
C<mk> identifies it by the presence of
a file called C<Rakefile>;
and passes all arguments on the command line to the build tool.
L<https://github.com/ruby/rake>

=cut

$BUILDERS->{'rake'} = {
	'do' => sub($wd, $args) { there_do_that $wd, 'rake', @$args },
	'can' => sub($dir) { return (-e "$dir/Rakefile") },
};


########################################################################

=item L<redo(1)>

Redo is a general-purpose build system.
C<mk> identifies it by the presence of
files matching the pattern C<*.do>;
and passes all arguments on the command line to the build tool.
L<https://redo.readthedocs.io/>

=cut

$BUILDERS->{'redo'} = {
	'do' => sub($wd, $args) { there_do_that $wd, 'redo', @$args },
	'can' => sub($dir) { return () = glob "$dir/*.do" },
};


########################################################################

=item L<stack(1)>

Stack is an environment manager
and build system orchestrator for Haskell.
C<mk> identifies it by the presence of
a file called C<stack.yaml>.
If command-line arguments are passed,
they are all passed to the build tool;
otherwise, C<stack build> is invoked.
L<https://www.haskellstack.org/>

=cut

$BUILDERS->{'stack'} = {
	'do' => sub($wd, $args) {
		unshift @$args, 'build' if $#{$args} == -1;
		there_do_that $wd, 'stack', @$args },
	'can' => sub($dir) { return (-e "$dir/stack.yaml") },
};


########################################################################

=back

=cut

sub main($argv) {
	# walk up the directory tree
	my $wd = $ENV{'PWD'}; # getcwd;
	my @DISORDER = @ORDER;
	if (defined $argv->[0] and $argv->[0] =~ m{\A\+\+(\w+)\Z} and
		exists $BUILDERS->{$1}) { @DISORDER = ($1); shift @$argv }

	do {
		for my $this (@DISORDER) {
			my $builder   = $BUILDERS->{$this};
			my $predicate = $builder->{'can'};
			my $executor  = $builder->{'do'};
			&$executor($wd, $argv) if &$predicate($wd);
		}
	} while (($wd = dirname $wd) ne "/");

	say "$0: nothing to mk";
	exit 1;
}

sub there_do($there, @that) {
	my $kid = fork;
	die "???" unless defined $kid;
	there_do_that $there, @that if $kid == 0;
	wait
}

sub there_do_that($there, @that) {
	chdir $there;
	do_that @that;
}

sub do_that(@that) {
	say "[", getcwd, "] @that";
	exec @that;
	die "???";
}

main(\@ARGV);

__END__;
