#!/usr/bin/env perl
# NAME: archpkgupd-notify --- notify on Arch package updates

use v5;
use strict;
use warnings;
use feature 'say';

use constant PKG_UPDATE_DB => $ENV{HOME}."/.cache/archpkg-info.db";
use constant PKG_RSS_FEED => 'https://archlinux.org/feeds/packages/';

use DBD::SQLite;
use DBI;
use DateTime::Format::ISO8601;
use DateTime::Format::Mail;
use DateTime;
use Glib::Object::Introspection;
use LWP::UserAgent;
use XML::RSS;

Glib::Object::Introspection->setup(
	basename => 'Notify',
	version  => '0.7',
	package  => 'Notify'
);
Notify->init;


sub initdb {
	my $dbh = DBI->connect('dbi:SQLite:dbname='.PKG_UPDATE_DB, "", "");

	$dbh->do('PRAGMA mmap_size    = '.(128 * 1024 * 1024));
	$dbh->do('PRAGMA page_size    = '.(64 * 1024));
	$dbh->do('PRAGMA journal_mode = WAL');

	$dbh->do(<<__EOF__);
		CREATE TABLE IF NOT EXISTS updates (
			guid        TEXT,
			title       TEXT,
			created_at  DATETIME,
			PRIMARY KEY (guid)
		);
__EOF__

	$dbh->do(<<__EOF__);
		CREATE TABLE IF NOT EXISTS packages (
			name        TEXT,
			version     TEXT,
			repository  TEXT,
			platform    TEXT,
			update_guid TEXT,
			PRIMARY KEY (repository, platform, name, version),
			FOREIGN KEY (update_guid) REFERENCES updates (guid)
		);
__EOF__

	$dbh->do(<<__EOF__);
		CREATE INDEX IF NOT EXISTS package_plat_repos
			ON packages (repository, platform, name);
__EOF__

	$dbh->do(<<__EOF__);
		CREATE INDEX IF NOT EXISTS package_repos
			ON packages (repository, name);
__EOF__

	$dbh
}

# Fetch RSS feed of package updates.
sub get_pkg_rss_feed {
	my $ua = LWP::UserAgent->new(timeout => 10);
	$ua->env_proxy;

	my $response = $ua->get(PKG_RSS_FEED);
	die $response->status_line unless ($response->is_success);

	my $rss = XML::RSS->new;
	$rss->parse($response->decoded_content);

	$rss
}

# Get a list of installed packages.
sub get_installed_packages {
	my %pkgs = split /\s/, `pacman -Q`;

	\%pkgs;
}


my $dbh  = initdb;
my $rss  = get_pkg_rss_feed;
my $pkgs = get_installed_packages;

# Prepare statements for selection/insertion.
our $check_sth = $dbh->prepare(<<__EOF__);
	SELECT created_at FROM updates WHERE guid = ?;
__EOF__

our $put_update_sth = $dbh->prepare(<<__EOF__);
	INSERT INTO updates (guid, title, created_at)
	VALUES (?, ?, ?);
__EOF__

our $put_version_sth = $dbh->prepare(<<__EOF__);
	INSERT INTO packages
		(name, version, repository, platform, update_guid)
	VALUES (?, ?, ?, ?, ?);
__EOF__


# transactions, Just In Case(tm).
$dbh->begin_work;

foreach my $item (@{$rss->{'items'}}) {
	my $guid    = $item->{'guid'};
	my $pubdate = $item->{'pubDate'};
	my $title   = $item->{'title'};
	my $cats    = $item->{'category'};

	$check_sth->execute($guid);
	# Fetch and immediately discard; we only want #rows.
	$check_sth->fetchall_arrayref;
	next if ($check_sth->rows > 0);

	my $dt = DateTime::Format::Mail->parse_datetime($pubdate);
	$put_update_sth->execute($guid, $title, $dt->iso8601);
	say "$guid\t$title\t$dt";

	# $title has the form "xterm 370-1 x86_64".
	my ($pkg, $newv, $plat) = split /\s/, $title;
	my ($repo) = grep { lc $_ ne $plat } @$cats; $repo = lc $repo;
	$put_version_sth->execute($pkg, $newv, $repo, $plat, $guid);

	# Skip testing and kde-unstable updates.
	next if grep { $_ =~ /testing|kde-unstable/i } @$cats;

	my $ntitle = sprintf '%s/%s %s', $repo, $pkg, $plat;

	# Mark up the current version.
	my $ndescr = "$newv";
	$ndescr .= "\n(have " . $pkgs->{$pkg} . ")"
		if exists $pkgs->{$pkg} and $pkgs->{$pkg} ne $newv;

	my $notify = Notify::Notification->new(
		$ntitle, $ndescr, 'software-update-available');
	$notify->set_urgency('low') unless exists $pkgs->{$pkg};
	$notify->set_timeout(30 * 1000);

	$notify->show;
}

$dbh->commit;

