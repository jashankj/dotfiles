#!/usr/bin/env zsh
# Sort all your labels in Todoist.

set -e

mktemp -d | read tx
zmodload zsh/datetime

call() {
	curl https://api.todoist.com/sync/v9/sync \
		-H "Authorization: Bearer $(cat $HOME/.todoist-token)" \
		"$@" \
	| tee "${tx?}/todoist.$EPOCHSECONDS.json"
}

call \
	--data sync_token='*' \
	--data resource_types='["labels"]' \
| jq -r '.labels | .[] | (.name + "\t" + .id)' \
| LC_ALL=C sort \
| awk -v uuid=$(uuidgen) '
	BEGIN {
		print "[";
		print   "{";
		print     "\"type\": \"label_update_orders\",";
		print     "\"uuid\": \""uuid"\",";
		print     "\"args\": {";
		print       "\"id_order_mapping\": {"
	};
	{
		print (NR > 1 ? "," : "") "\""$2"\": " (NR * 10)
	};
	END {
		print       "}";
		print     "}";
		print   "}";
		print "]"
	}' \
| tee "${tx?}/commands.json"

call \
	--data sync_token='*' \
	--data commands="$(cat "${tx?}/commands.json")"

rm -v -R "${tx?}"
