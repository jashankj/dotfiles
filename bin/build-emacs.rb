#!/usr/bin/env ruby
# frozen_string_literal: true
# rubocop:disable Layout/ArrayAlignment
# rubocop:disable Layout/SpaceInsideRangeLiteral
# rubocop:disable Style/GlobalVars
# rubocop:disable Style/IfUnlessModifier
# rubocop:disable Style/TrailingCommaInArrayLiteral
# typed: false

# NAME: build-emacs --- Build Emacs
# SYNOPSIS: build-emacs

require 'yaml'

$flags        = []
$with_opts    = []
$without_opts = []
$cflags       = []
$ldflags      = []
$libs         = []
$env_vars     = {}

$HOME   = ENV['HOME']
$HOST   = ENV['HOST']
$OSARCH = ENV['OSARCH']
$J = (ENV['J'] || `getconf _NPROCESSORS_ONLN`.strip).to_i

case $HOST
when 'elspeth', 'lisbon', 'yarn-ev'
  $prefix  =           $HOME
# $bindir  = File.join $HOME, "bin.#{$OSARCH}"
  $bindir  = File.join $HOME, 'bin'
  emacssrc = File.join $HOME, 'src', 'gnu', 'emacs'
  $srcdir  = File.join emacssrc, 'src'
  $objdir  = File.join emacssrc, 'obj'
  $staged  = File.join emacssrc, 'stage'
  $arcdir  =           emacssrc
else
  $prefix = File.join '', 'src', 'gnu', 'emacs'
  $srcdir = File.join prefix, 'src'
  $objdir = File.join prefix, 'obj'
  $staged = File.join prefix, 'stage'
  $bindir = File.join prefix, 'bin'
  $arcdir =           prefix
end

########################################################################

def remark(x); $stdout.puts "# #{x}"; end
def report(x); $stdout.puts ": #{x}"; end

########################################################################

Dir.chdir $srcdir
$v_str = File.open('README') { |f| f.each_line.to_a[4].split(/\s+/)[5] }
$v_major, $v_minor, $v_patch = $v_str.split('.').map(&:to_i)

$rev = `git -C #{$srcdir} rev-list --first-parent --count HEAD`.chomp.to_i
$revname = format('%dn%d', $v_major, $rev)
$arcfile = format('emacs-%s.tar.zst', $revname)
$archive = File.join($arcdir, $arcfile)
$staged += "-#{$rev}"

remark "building Emacs #{$v_major}.#{$v_minor}.#{$v_patch} [#{$revname}] in #{$srcdir}"

########################################################################

# Add a compiler.
$env_vars[:cc]  = 'ccache clang'
$env_vars[:cxx] = 'ccache clang++'

$flags += %W[
  -C
  --prefix=#{$prefix}
  --bindir=#{$bindir}
  --enable-check-lisp-object-type
]

$with_opts += [
  'wide-int',                   # Use 62-bit integers.
  'libgmp',                     # Use GMP for math.
  'modules',                    # Enable dynamic modules.
  'threads',                    # Enable threading.
  'native-compilation',         # Enable native-comp.

  # Interop flags:
  'xml2', 'json',               # Interop: XML, JSON
  'zlib',                       # Interop: compression.
  'gnutls',                     # Interop: native SSL.
  'dbus',                       # Interop: D-Bus
  'gsettings',                  # Interop: GNOME/GSettings API

  # Graphics flags ---
  'lcms2',                      # Colour management.
  'harfbuzz',                   # Font rendering.
  'cairo',                      # Rendering.

  # Image libraries.
  'xpm', 'jpeg', 'tiff', 'gif', 'png', 'rsvg',
]

$without_opts += [
  'imagemagick',                # No ImageMagick support.
  'gpm',                        # No GPM support.
  'selinux',                    # No SELinux support.
  'pop',                        # No POP3 support.
  'libsystemd',                 # No systemd support.
  'compress-install',           # Don't compress .el files.
]

$cflags += %w[
  -g
  -O3
  -mtune=native -march=native
  -flto=thin

  -Wno-unknown-warning-option

  -fno-builtin-malloc
  -fno-builtin-calloc
  -fno-builtin-realloc
  -fno-builtin-free
  -fno-builtin-strdup
  -fno-builtin-strndup
  -fno-builtin-aligned_alloc
  -fno-builtin-memalign
]

$ldflags += %w[
  -flto
]

case $OSARCH
when /\Afreebsd-/, /\Alinux-/, /\Asunos-/
  # If we want to do X:
  $with_opts += ['x-toolkit=athena'] # of { gtk, gtk2, gtk3, lucid, athena }
  $with_opts += %w[
    x xaw3d xft xim xdbe
    libotf m17n-flt
  ] # xinput2

when /\Adarwin-/
  # If we want to do Step:
  $with_opts += ['ns']          # Not Step?

when /\Awindows-/
  # If we want to do Win32:
  $with_opts += [
    'w32',                      # Pane.
    'native-image-api',         # GDI+, more or less.
  ]
end

$env_vars[:make] = $make =
  case $OSARCH
  when /\Afreebsd-/ then 'gmake'
  when /\Adarwin-/  then 'gmake'
  else 'make'
  end

unless $HOST == 'elspeth'
  $ldflags += [
    '-fuse-ld=lld',
    '-Wl,--gc-sections',
    '-Wl,--threads,16'
  ]

  $libs += [
    '-lprofiler',
    '-ljemalloc',
##  '-lmimalloc',
  ]

  $env_vars[:ar]     = 'llvm-ar'
  $env_vars[:ranlib] = 'llvm-ranlib'
  $env_vars[:nm]     = 'llvm-nm'
end

########################################################################

def incdir(x); '-I' + x; end
def libdir(x); '-L' + x; end

# Deal with Debian's obnoxious packaging for libgccjit.
#
# Inexplicably, the header files are buried within the compiler library
# tree (i.e., `/usr/lib/gcc/x86_64-linux-gnu/12/include/').
# I have thus created a directory, `/usr/include/libgccjit-12',
# in which I have placed symlinks to `libgccjit.h' (and `libgccjit++.h')
# and which therefore I can `-I' in here.
if File.exist? '/usr/include/libgccjit-12'
  $cflags += [incdir('/usr/include/libgccjit-12')]
end

# Deal with Debian's broken packaging for libprofiler.
if File.exist?(File.join($HOME, 'lib', $OSARCH, 'libprofiler.so'))
  $cflags  += [incdir(File.join($HOME, 'include'))]
  $ldflags += [libdir(File.join($HOME, 'lib', $OSARCH))]
end

########################################################################

# OK ... use those flags.
$env_vars[:cflags]  = $cflags.join(' ')
$env_vars[:ldflags] = [$ldflags, $libs].flatten.join(' ')

#
# Why not `$LIBS'?  Because emacs/src/Makefile says:
#
#     Don't use LIBS.  configure puts stuff in it that either shouldn't
#     be linked with Emacs or is duplicated by the other stuff below.
#
# This is obviously wrong, but I can't do anything about it.
#

########################################################################

def run_that(*args)
  args.flatten!
  report args.join(' ')
  system(*args) or raise 'subprocess failed, bailing out'
end

def chdir(dir)
  report "cd #{dir}"
  Dir.chdir dir
end

def mkdir(dir)
  report "mkdir -p #{dir}"
  Dir.mkdir dir rescue nil
end

def rm_f(that)
  report "rm -f #{that}"
  File.remove that rescue nil
end

def do_describe
  Dir.chdir $srcdir
  run_that %w[git rev-parse HEAD]
end

def do_autoreconf
  chdir $srcdir
  # run_that %w[ ./autogen.sh ]
  run_that %w[
    autoreconf
      --verbose
      --install --force
      --symlink
      -I m4
  ]
end

def configure_args
  [
    File.join($srcdir, 'configure'),
    *$flags,
    *($with_opts.map    { |x| '--with-'    + x }),
    *($without_opts.map { |x| '--without-' + x }),
    *($env_vars.map { |k, v| k.to_s.upcase + '=' + v })
  ]
end

def do_configure
  mkdir $objdir
  chdir $objdir
  run_that configure_args
end

def make_cmd(*args)
  [
    $make,
    '-Otarget',
    "-j#{$J}",
    'V=1',
    # 'NATIVE_FULL_AOT=1',
    *args
  ]
end

def do_build
  chdir $objdir
  run_that make_cmd 'all'
end

def do_stage
  chdir $objdir
  run_that make_cmd 'install', "DESTDIR=#{$staged}"

  # Compile refcards in advance.
  chdir "#{$staged}#{$prefix}/share/emacs/#{$v_str}/etc/refcards"
  run_that make_cmd 'all'
  run_that make_cmd 'clean'
  chdir $objdir

  case $OSARCH
  when /\Adarwin-/ then do_put_ns_bundle
  else                  do_bin_symlinks
  end
end

def do_bin_symlinks
  names = %W[
    emacs-#{$v_major}.#{$v_minor}.#{$v_patch}
    emacs-#{$v_major}.#{$v_minor}
    emacs-#{$v_major}
    emacs
  ]

  chdir File.join($staged, $bindir)
  names.each_cons(2).each do |a, b|
    run_that 'ln', '-vsf', a, b
  end
end

def do_put_ns_bundle
  do_that \
    'rsync',
      '-Pai', '--delete',
      File.join($objdir, 'nextstep', 'Emacs.app', ''),
      File.join($HOME, 'Applications', "Emacs-#{$revname}.app")
end

########################################################################

def manifest_header
  manifest_header_obj
    .to_yaml
    .lines
    .map {|x| "# " + x }
    .join
end

def manifest_header_obj
  {
    version:    "#{$v_major}.#{$v_minor}.#{$v_patch}",
    revname:    $revname,
    build:      manifest_build_info,
  }
end

def manifest_build_info
  {
    with:       $with_opts,
    without:    $without_opts,
    cflags:     $cflags,
    ldflags:    $ldflags,
    libs:       $libs,
    flags:      $flags,
    env:        $env_vars,
    host:       $HOST,
    osarch:     $OSARCH,
  }
end

MANIFEST_MTREE_INVOCATION = -'mtree -c -k size,sha256'
def run_mtree
  report MANIFEST_MTREE_INVOCATION
  `#{MANIFEST_MTREE_INVOCATION}`
end

$manifest_file = File.join('etc', 'emacs.manifest')

def write_manifest
  rm_f $manifest_file
  mtree_manifest = run_mtree
  mkdir 'etc'

  report ">> #{$manifest_file}"
  File.open($manifest_file, mode: 'w') do |f|
    f.puts manifest_header
    f.puts '#'
    f.puts mtree_manifest
  end
end

def create_archive
  chdir File.join($staged, $prefix)
  write_manifest
  run_that \
    'bsdtar',
    '--create',
    '--zstd',
      '--options', 'zstd:compression-level=1',
      '--options', 'zstd:threads=0',
    '--file', $archive,
    '.'
  remark "generated #{$archive}"
end

########################################################################

stampfiles =
  [DATA.stat] +
  [
    File.join($srcdir, 'configure.ac'),
    File.join($srcdir, 'configure'),
    File.join($objdir, 'config.log'),
    File.join($objdir, 'src', 'emacs'),
    File.join($staged, 'etc', 'emacs.manifest'),
    $archive,
  ]
stamps = stampfiles.map { |x| File::Stat.new(x) rescue nil }

steps = %i[
  do_describe
  do_autoreconf
  do_configure
  do_build
  do_stage
  create_archive
]

nn = 0
(1 ... stamps.length).to_a.reverse.each do |n|
  af, a = stampfiles[n - 1], stamps[n - 1]
  bf, b = stampfiles[n],     stamps[n]
  if a and b
    remark "#{af} #{a.mtime} <=> #{bf} #{b.mtime}"
    nn = n - 1 if a.mtime >= b.mtime
  else
    remark "#{af} #{a||'nil'} <=> #{bf} #{b||'nil'}"
  end
end

(nn .. steps.length).each do |n|
  next unless steps[n]
  remark "step #{n}: #{steps[n]}"
  send steps[n]
end

__END__
