#!/bin/sh
# NAME: nashorn --- start the Java JavaScript engine interactively
# SYNOPIS: nashorn
#
# Wikipedia sez ---
# > With the release of Java 11, Nashorn is deprecated, and has been
# > removed from JDK 15 onwards.  GraalJS from the GraalVM project was
# > suggested as a replacement.
#
# Long live Project Nashorn!

if [ -z "${JAVA_HOME}" ]
then
	java="$(which java)"
	real_java="$(realpath "${java}")"
	java_home_bin="$(dirname "${real_java}")"
	JAVA_HOME="$(dirname "${java_home_bin}")"
fi

exec "${JAVA_HOME}/bin/jjs" -scripting -fv -cp . "$@"
