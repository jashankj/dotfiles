#!/usr/bin/env zsh
# NAME: listens --- listen to a radio stream
# SYNOPSIS: listens <station>

typeset -A stations
typeset -A alternates
typeset -A descr

########################################################################
# Australian Broadcasting Corporation (ABC Australia) radio streams:

stations+=([abclocalsyd]="https://mediaserviceslive.akamaized.net/hls/live/2038302/localsydney/index.m3u8")
alternates+=([702]=abclocalsyd [2bl]=abclocalsyd [localsyd]=abclocalsyd)
descr+=([abclocalsyd]="702 ABC Sydney (ABC Local Radio) <https://www.abc.net.au/sydney/>")

stations+=([abcrn]="https://mediaserviceslive.akamaized.net/hls/live/2038318/rnnsw/index.m3u8")
alternates+=([2fc]=abcrn [radionational]=abcrn [rn]=abcrn)
descr+=([abcrn]="ABC Radio National <https://www.abc.net.au/rn/>")

stations+=([abcclassic]="https://mediaserviceslive.akamaized.net/hls/live/2038316/classicfmnsw/index.m3u8")
alternates+=([abcfm]=abcclassic [classic]=abcclassic [classicfm]=abcclassic)
descr+=([abcclassic]="ABC Classic FM <https://www.abc.net.au/classic/>")

stations+=([abcclassic2]="https://mediaserviceslive.akamaized.net/hls/live/2038317/classic2/index.m3u8")
alternates+=([abcfm2]=classic2 [classic2]=abcclassic2)
descr+=([abcclassic2]="ABC Classic 2 <https://www.abc.net.au/classic/>")

stations+=([abcjazz]="https://mediaserviceslive.akamaized.net/hls/live/2038319/abcjazz/index.m3u8")
descr+=([abcjazz]="ABC Jazz (DiG Jazz) <https://www.abc.net.au/jazz/>")

stations+=([abcdoublej]="https://mediaserviceslive.akamaized.net/hls/live/2038315/doublejnsw/index.m3u8")
alternates+=([2jj]=abcdoublej [doublej]=abcdoublej)
descr+=([abcdoublej]="ABC Double J <https://www.abc.net.au/doublej/>")

stations+=([abctriplej]="https://mediaserviceslive.akamaized.net/hls/live/2038308/triplejnsw/index.m3u8")
alternates+=([2jjj]=abctriplej [triplej]=[abctriplej])
descr+=([abctriplej]="ABC Triple J <https://www.abc.net.au/triplej/>")

stations+=([abcunearthed]="https://mediaserviceslive.akamaized.net/hls/live/2038305/triplejunearthed/index.m3u8")
alternates+=([2jjjj]=abcunearthed [unearthed]=abcunearthed)
descr+=([abcunearthed]="ABC Triple J Unearthed <https://www.abc.net.au/triplejunearthed/>")

stations+=([abcradioaus]="https://mediaserviceslive.akamaized.net/hls/live/2038266/ramulti/index.m3u8")
alternates+=([radioaus]=abcradioaus)
descr+=([abcradioaus]="ABC Radio Australia <https://www.abc.net.au/radioaustralia/>")

########################################################################
# Special Broadcasting Service (SBS) radio streams:

stations+=([sbsradio1]="https://sbs-hls.streamguys1.com/sbs-web/sbs1/playlist.m3u8")
alternates+=([sbs1]=sbsradio1)
descr+=([sbsradio1]="SBS Radio 1 (1107AM Sydney) <https://www.sbs.com.au/radio/>")
# "m3u8a": "https://sbs-hls.streamguys1.com/sbs-web/sbs1/playlist.m3u8",
# "mp3": "https://sbs-ice.streamguys1.com/sbs1-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbs1-sbs-web"

stations+=([sbsradio2]="https://sbs-hls.streamguys1.com/sbs-web/sbs2/playlist.m3u8")
alternates+=([sbs2]=sbsradio2)
descr+=([sbsradio2]="SBS Radio 2 (97.7FM Sydney) <https://www.sbs.com.au/radio/>")
# "m3u8a": "https://sbs-hls.streamguys1.com/sbs-web/sbs2/playlist.m3u8",
# "mp3": "https://sbs-ice.streamguys1.com/sbs2-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbs2-sbs-web"

stations+=([sbsradio3]="https://sbs-hls.streamguys1.com/sbs-web/sbs3/playlist.m3u8")
alternates+=([sbs3]=sbsradio3)
descr+=([sbsradio3]="SBS Radio 3 (BBC World Service) <https://www.sbs.com.au/radio/>")
# "m3u8a": "https://sbs-hls.streamguys1.com/sbs-web/sbs3/playlist.m3u8",
# "mp3": "https://sbs-ice.streamguys1.com/sbs3-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbs3-sbs-web"

stations+=([sbsarabic24]="https://sbs-hls.streamguys1.com/sbs-web/arabic/playlist.m3u8")
alternates+=([sbsarabic]=sbsarabic24)
descr+=([sbsarabic24]="SBS Arabic24 <https://www.sbs.com.au/language/arabic>")
# "m3u8a": "https://sbs-hls.streamguys1.com/sbs-web/arabic/playlist.m3u8",
# "mp3": "https://sbs-ice.streamguys1.com/sbs-arabic-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbs-arabic-sbs-web"

stations+=([sbschill]="https://sbs-hls.streamguys1.com/sbs-web/sbschill/playlist.m3u8")
descr+=([sbschill]="SBS Chill <https://www.sbs.com.au/radio/chill>")
# "m3u8a": "https://sbs-hls.streamguys1.com/sbs-web/sbschill/playlist.m3u8",
# "mp3": "https://sbs-ice.streamguys1.com/sbschill-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbschill-sbs-web"

stations+=([sbspopasia]="https://sbs-hls.streamguys1.com/sbs-web/popasia/playlist.m3u8")
alternates+=([popasia]=sbspopasia)
descr+=([sbspopasia]="SBS PopAsia <https://www.sbs.com.au/popasia/>")
# "m3u8a": "https://sbs-hls.streamguys1.com/sbs-web/popasia/playlist.m3u8",
# "mp3": "https://sbs-ice.streamguys1.com/sbs-popasia-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbs-popasia-sbs-web"

stations+=([sbspopdesi]="https://sbs-hls.streamguys1.com/sbs-web/popdesi/playlist.m3u8")
alternates+=([popdesi]=sbspopdesi)
descr+=([sbspopdesi]="SBS PopDesi <https://www.sbs.com.au/radio/popdesi>")
# "m3u8a": "https://sbs-hls.streamguys1.com/sbs-web/popdesi/playlist.m3u8",
# "mp3": "https://sbs-ice.streamguys1.com/sbs-popdesi-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbs-popdesi-sbs-web"

stations+=([sbsradio4]="https://sbs-hls.streamguys1.com/sbs-web/sbs4/playlist.m4u8")
alternates+=([sbs4]=sbsradio4)
descr+=([sbsradio4]="SBS Radio 4")
# "m4u8a": "https://sbs-hls.streamguys1.com/sbs-web/sbs4/playlist.m4u8",
# "mp4": "https://sbs-ice.streamguys1.com/sbs4-sbs-web",
# "m4a": "https://sbs-ice.streamguys1.com/sbs4-sbs-web"

########################################################################
# British Broadcasting Corporation (BBC) radio streams:
#
# BBC streams all need _something_ that mpv doesn't support.
# DASH fragments past the first return 404.

stations+=([bbcradio1]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/cfs/bbc_radio_one.mpd")
descr+=([bbcradio1]="BBC Radio 1 <https://www.bbc.co.uk/sounds/play/live:bbc_radio_one>")

stations+=([bbcradio1dance]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/cfs/bbc_radio_one_dance.mpd")
descr+=([bbcradio1dance]="BBC Radio 1 Dance <https://www.bbc.co.uk/sounds/play/live:bbc_radio_one_dance>")

stations+=([bbcradio1relax]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/cfs/bbc_radio_one_relax.mpd")
descr+=([bbcradio1relax]="BBC Radio 1 Relax <https://www.bbc.co.uk/sounds/play/live:bbc_radio_one_relax>")

stations+=([bbcradio1xtra]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/cfs/bbc_1xtra.mpd")
alternates+=([bbcradio1x]=bbcradio1xtra [bbc1xtra]=bbcradio1xtra)
descr+=([bbcradio1xtra]="BBC Radio 1xtra <https://www.bbc.co.uk/sounds/play/live:bbc_1xtra>")

stations+=([bbcradio2]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/aks/bbc_radio_two.mpd")
descr+=([bbcradio2]="BBC Radio Two <https://www.bbc.co.uk/sounds/play/live:bbc_radio_two>")

stations+=([bbcradio3]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/aks/bbc_radio_three.mpd")
descr+=([bbcradio3]="BBC Radio Three <https://www.bbc.co.uk/sounds/play/live:bbc_radio_three>")

stations+=([bbcradio4]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/aks/bbc_radio_fourfm.mpd")
descr+=([bbcradio4]="BBC Radio Four <https://www.bbc.co.uk/sounds/play/live:bbc_radio_fourfm>")

stations+=([bbcradio4extra]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/aks/bbc_radio_four_extra.mpd")
alternates+=([bbcradio4x]=bbcradio4extra)
descr+=([bbcradio4extra]="BBC Radio Four Extra <https://www.bbc.co.uk/sounds/play/live:bbc_radio_four_extra>")

stations+=([bbcradio5live]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/aks/bbc_radio_five_live.mpd")
descr+=([bbcradio5live]="BBC Radio 5live <https://www.bbc.co.uk/sounds/play/live:bbc_radio_five_live>")

stations+=([bbcradio6music]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/aks/bbc_radio_six_music.mpd")
descr+=([bbcradio6music]="BBC Radio 6music <https://www.bbc.co.uk/sounds/play/live:bbc_radio_six_music>")

stations+=([bbcworld]="https://a.files.bbci.co.uk/media/live/manifesto/audio/simulcast/dash/nonuk/dash_low/aks/bbc_world_service.mpd")
descr+=([bbcworld]="BBC World Service <https://www.bbc.co.uk/sounds/play/live:bbc_world_service>")

########################################################################
# East Side Radio, Sydney

stations+=([eastside]="https://stream.eastsidefm.org/stream")
alternates+=([2resfm]=eastside)
descr+=([eastside]="2RESFM Eastside Radio 89.7 <https://eastsidefm.org/>")

########################################################################
# LiveATC for Sydney Airport (YSSY)

stations+=([yssy_16l]="https://www.liveatc.net/play/yssy1_twr2.pls")
alternates+=([yssy_34r]=yssy_16l [124.7]=yssy_16l)
descr+=([yssy_16l]="YSSY Tower, 16L/34R, 124.7")

stations+=([yssy_16r]="https://www.liveatc.net/play/yssy1_twr1.pls")
alternates+=([yssy_34l]=yssy_16r [yssy_07]=yssy_16r [yssy_25]=yssy_16r [120.5]=yssy_16r)
descr+=([yssy_16r]="YSSY Tower, 16R/34L, 07/25, 120.5")

stations+=([yssy_twr]="https://www.liveatc.net/play/yssy1_twr.pls")
alternates+=([yssy_tower]=yssy_twr)
descr+=([yssy_twr]="YSSY Tower, 120.5 + 124.7")

stations+=([yssy_atis]="https://www.liveatc.net/play/yssy1_atis1.pls")
descr+=([yssy_atis]="YSSY ATIS 112.1/118.55/126.25/428")

stations+=([yssy_app_n]="https://www.liveatc.net/play/yssy1_app_n.pls")
alternates+=([124.4]=yssy_app_n)
descr+=([yssy_app_n]="YSSY Approach (45-10nmi) North/East, 124.4")

stations+=([yssy_app_s]="https://www.liveatc.net/play/yssy1_app_s.pls")
alternates+=([128.3]=yssy_app_s)
descr+=([yssy_app_s]="YSSY Approach (45-10nmi) South, 128.3")

stations+=([yssy_gnd]="https://www.liveatc.net/play/yssy1_del_gnd.pls")
alternates+=([133.8]=yssy_gnd [121.7]=yssy_gnd [126.5]=yssy_gnd)
descr+=([yssy_gnd]="YSSY Ground/Clearance, 133.8 + 121.7 + 126.5")

stations+=([yssy_dep_ne]="https://www.liveatc.net/play/yssy1_dep_ne.pls")
alternates+=([123.0]=yssy_dep_ne)
descr+=([yssy_dep_ne]="YSSY Departures North/East, 123.0")

stations+=([yssy_dep_sw]="https://www.liveatc.net/play/yssy1_dep_s.pls")
alternates+=([129.7]=yssy_dep_sw)
descr+=([yssy_dep_sw]="YSSY Departures South/West, 129.7")

stations+=([yssy_dir]="https://www.liveatc.net/play/yssy5_dir.pls")
alternates+=([125.3]=yssy_dir [126.1]=yssy_dir)
descr+=([yssy_dir]="YSSY Director, 125.3 (west) + 126.1 (east)")

stations+=([yssy_center_s]="https://www.liveatc.net/play/yssy1_ctr_s.pls")
alternates+=([124.55]=yssy_center_s [yssy_fia_s]=yssy_center_s)
descr+=([yssy_center_s]="YSSY Center/FIA South, 124.55")

for x in 124625 124950 125000 128600
do
	stations+=([ybbb_${x%%???}.${x##???}]="https://www.liveatc.net/play/yssy1_ctr_$x.pls")
	descr+=([ybbb_${x%%???}.${x##???}]="YBBB Brisbane Center, ${x%%???}.${x##???}")
done

########################################################################

usage() {
	echo "usage: listens <station>"
	echo
	echo "... some stations you might like:"
	for station in ${(k)stations}
	do
		printf '%16s: %s' $station ${descr[$station]}
		alts=${(k)alternates[(R)$station]}
		[ ! -z "$alts" ] && printf ' [%s]' $alts
		echo
	done | sort
	exit 64 # EX_USAGE
}

[ -z "$1" ] && usage

id="${alternates[$1]}"
id="${id:-$1}"

url="${stations[$id]}"

[ -z "${url}" ] && usage

case "${url}" in
*.mpd)	exec ffplay -hide_banner "${url}" ;;
*)	exec mpv "${url}" ;;
esac
