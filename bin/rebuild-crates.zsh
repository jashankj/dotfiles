#!/usr/bin/env zsh
# NAME: rebuild-crates --- rebuild cached crates in ~/.cargo/target
# SYNOPSIS: rebuild-crates

typeset -a crates
typeset -A build_cmds

crates=()

# crates+=(/src/rust/rust-analyzer)
# build_cmds+=([/src/rust/rust-analyzer]="cargo install --path crates/rust-analyzer --bin rust-analyzer")

projects=$HOME/projects
crates+=(
# $projects/askance/be
# $projects/cse-autotest
# $projects/rs-elekt
# $projects/rs-exampaper
# $projects/rs-gtkdemo
$projects/historyd

$projects/rs-dominion
$projects/rs-illegible
$projects/schemes/rs-scm
$projects/simv/simvintage
$projects/slewfs

$projects/restructuredtext-rs

$projects/rs-mailsync
$projects/rs-movemail
# $projects/rs-proto-exchange

$projects/rs-language-c
$projects/rs-language-verilog
$projects/rs-language-vhdl
$projects/rs-ngaraguun

$projects/voxwork/kaldigst
$projects/voxwork/lars
$projects/voxwork/readrec


~/src/local

~/sw/concern
# ~/sw/eek
# ~/sw/pijul/pijul
~/src/mips/mipsy # ~/sw/mipsy
# /web/cs1521/22t2/_tools
)

build_cargo_crates() {
	ccat ~/.packages.rust \
	| grep -v '^rust-analyzer' \
	| while read crate
	  do
		echo "===> crates.io:${crate}"
		cargo install ${crate}
	  done
}

build_local_crates() {
	for crate in ${crates}
	do
		(
		echo "===> ${crate}"
		cd ${crate}
		build_cmd="${build_cmds[${crate}]}"
#		build_cmd="${build_cmd:-cargo update}"
#		build_cmd="${build_cmd:-cargo build -j4}"
		build_cmd="${build_cmd:-cargo doc -j6}"
#		build_cmd="ls ${crate}/Cargo.lock"
		eval ${build_cmd}
		)
	done
}

# build_cargo_crates
build_local_crates
