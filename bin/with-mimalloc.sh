#!/bin/sh

exec \
env LD_PRELOAD="$(pkg-config --variable=libdir mimalloc)/libmimalloc.so.2.0" \
"$@"
