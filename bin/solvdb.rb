#!/usr/bin/env ruby
# frozen_string_literal: true
# rubocop:disable Layout/ExtraSpacing
# rubocop:disable Layout/FirstParameterIndentation
# rubocop:disable Layout/LineContinuationLeadingSpace
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/ClassLength
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/ParameterLists
# rubocop:disable Naming/MethodName
# rubocop:disable Style/CommentedKeyword
# rubocop:disable Style/FormatStringToken
# rubocop:disable Style/MethodDefParentheses
# rubocop:disable Style/ParallelAssignment
# rubocop:disable Style/TrailingCommaInArguments
# rubocop:disable Style/TrailingCommaInHashLiteral
# rubocop:todo Style/Documentation
# typed: true

require 'sorbet-runtime'; extend T::Sig

require 'benchmark'
require 'logger'

require 'rubygems'
require 'oj'
require 'sqlite3'
require 'subprocess'
require 'yajl/ffi'

require 'pry'

Oj.default_options = {
  cache_keys: true,
  symbol_keys: true,
}

# TODO: flag
RUNTIME_DIR = ENV['XDG_RUNTIME_DIR'] || ENV['TMPDIR'] || '/tmp'
DEFAULT_DATABASE_PATH = "#{RUNTIME_DIR}/solvdb/solvdb.sqlite3"
FileUtils.mkdir_p File.dirname DEFAULT_DATABASE_PATH

########################################################################

module SolvDB
  module SQL
    def CREATE_TABLE table, *columns
      @dbh.execute "CREATE TABLE IF NOT EXISTS #{table}" \
                   " (#{columns.join(', ')});"
    end

    def CREATE_INDEX index, on: nil, of: []
      table, columns = on, of
      @dbh.execute "CREATE INDEX IF NOT EXISTS #{index}" \
                   " ON #{table} (#{columns.join(', ')});"
    end

    def CREATE_VIEW view, as: nil
      @dbh.execute "CREATE VIEW IF NOT EXISTS #{view}" \
                   " AS #{as};"
    end

    def INSERT_INTO table, *columns
      columns_ = columns.join(', ')
      binds = columns.map { |_| '?' }.join(', ')
      @dbh.prepare "INSERT INTO #{table} (#{columns_})" \
                   " VALUES (#{binds});"
    end

    def CREATE_FTS5_TABLE(
          table,
          columns: [],
          tokenize: nil,
          prefix: nil,
          content: nil,
          rowid: nil,
          columnsize: nil,
          detail: nil
        )
      columns_ = columns.join(', ')
      flags = ''
      flags += ", tokenize = #{tokenize}" if tokenize
      flags += ", prefix = #{prefix}" if prefix
      flags += ", content = #{content}" if content
      flags += ", content_rowid = #{rowid}" if rowid
      flags += ", columnsize = #{columnsize}" if columnsize
      flags += ", detail = #{detail}" if detail

      # Create the FTS5 overlay:
      @dbh.execute <<-__SQL__
        CREATE VIRTUAL TABLE IF NOT EXISTS #{table}
        USING fts5 (#{columns_}#{flags});
      __SQL__

      # Triggers to keep the table up-to-date:

      # old_columns = columns.map {|c| "old.#{c}" }
      # new_columns = columns.map {|c| "new.#{c}" }

      # insert_new_fragment = <<-__SQL__
      #   INSERT INTO #{table} (rowid, #{columns_})
      #   VALUES (new.#{rowid}, #{new_columns.join(', ')})
      # __SQL__

      # delete_old_fragment = <<-__SQL__
      #   INSERT INTO #{table} (#{table}, rowid, #{columns_})
      #   VALUES ('delete', old.#{rowid}, #{old_columns.join(', ')})
      # __SQL__

      # @dbh.execute <<-__SQL__
      #   CREATE TRIGGER IF NOT EXISTS #{content}_fts_#{table}_ai
      #     AFTER INSERT ON #{content}
      #   BEGIN
      #     #{insert_new_fragment};
      #   END;
      # __SQL__

      # @dbh.execute <<-__SQL__
      #   CREATE TRIGGER IF NOT EXISTS #{content}_fts_#{table}_ad
      #     AFTER DELETE ON #{content}
      #   BEGIN
      #     #{delete_old_fragment};
      #   END;
      # __SQL__

      # @dbh.execute <<-__SQL__
      #   CREATE TRIGGER IF NOT EXISTS #{content}_fts_#{table}_au
      #     AFTER UPDATE ON #{content}
      #   BEGIN
      #     #{delete_old_fragment};
      #     #{insert_new_fragment};
      #   END;
      # __SQL__
    end

    def REBUILD_INDEX_solvable_fts
      @dbh.execute 'INSERT INTO solvable_fts (solvable_fts)' \
                   " VALUES ('rebuild');"
    end

    def PRAGMA_optimize
      @dbh.execute 'PRAGMA optimize;'
    end
  end  # module SolvDB::SQL

  ######################################################################

  module DB
    def db_connect database_file
      dbh = SQLite3::Database.new database_file
      dbh.foreign_keys = false
      dbh.journal_mode = 'memory'
      dbh.page_size    = (1 << 14)
      dbh.cache_size   = -(1 << 14)
      dbh.mmap_size    = -(1 << 12)
      dbh.synchronous  = 'off'
      dbh
    end
  end  # module SolvDB::DB

  ######################################################################

  module Schema
    module Raw
      include SolvDB::SQL

      def CREATE_TABLE_raw
        CREATE_TABLE 'raw',
                     'libsolv_db TEXT',
                     'solvjson JSONB'
      end

      def INSERT_INTO_raw libsolv_db: nil, solvjson: nil
        @insert_into_raw ||=
          @dbh.prepare 'INSERT INTO raw (libsolv_db, solvjson)' \
                       '  VALUES (?, JSONB(?))'
        @insert_into_raw.execute libsolv_db, solvjson
      end
    end  # module SolvDB::Schema::Raw

    ####################################################################

    module Repositories
      include SolvDB::SQL

      def CREATE_TABLE_repositories
        CREATE_TABLE 'repositories',
                     'repository_id INTEGER PRIMARY KEY',
                     'name TEXT',
                     'revision INTEGER',
                     'timestamp INTEGER'
      end

      def INSERT_INTO_repositories name: nil, revision: nil, timestamp: nil
        @insert_into_repositories ||=
          INSERT_INTO 'repositories', 'name', 'revision', 'timestamp'
        @insert_into_repositories.execute name, revision, timestamp
        @dbh.last_insert_row_id
      end
    end  # module SolvDB::Schema::Repositories

    ####################################################################

    module Repomds
      include SolvDB::SQL

      def CREATE_TABLE_repomds
        CREATE_TABLE 'repomds',
                     'repomd_id INTEGER PRIMARY KEY',
                     'repository_id INTEGER' \
                     '  REFERENCES repositories (repository_id)',
                     'mdtype TEXT',
                     'checksum_sha256 TEXT',
                     'location TEXT',
                     'timestamp INTEGER',
                     'size INTEGER'
      end

      def INSERT_INTO_repomds repository_id: nil, mdtype: nil,
                              checksum_sha256: nil, location: nil,
                              timestamp: nil, size: nil
        @insert_into_repomds ||=
          INSERT_INTO 'repomds',
                      'repository_id', 'mdtype', 'checksum_sha256',
                      'location', 'timestamp', 'size'
        @insert_into_repomds.execute repository_id, mdtype,
                                     checksum_sha256, location,
                                     timestamp, size
        @dbh.last_insert_row_id
      end
    end  # module SolvDB::Schema::Repomds

    ####################################################################

    module Vendors
      include SolvDB::SQL

      def CREATE_TABLE_vendors
        CREATE_TABLE 'vendors',
                     'vendor_id INTEGER PRIMARY KEY',
                     'vendor TEXT UNIQUE'
      end

      def INSERT_INTO_vendors vendor: nil
        @insert_or_ignore_into_vendors ||=
          @dbh.prepare <<-__SQL__
            INSERT OR IGNORE INTO vendors (vendor) VALUES (?);
          __SQL__
        @insert_or_ignore_into_vendors.execute vendor
        @dbh.last_insert_row_id
      end
    end  # module SolvDB::Schema::Vendors

    ####################################################################

    module Packagers
      include SolvDB::SQL

      def CREATE_TABLE_packagers
        CREATE_TABLE 'packagers',
                     'packager_id INTEGER PRIMARY KEY',
                     'packager TEXT UNIQUE'
      end

      def INSERT_INTO_packagers packager: nil
        @insert_or_ignore_into_packagers ||=
          @dbh.prepare <<-__SQL__
            INSERT OR IGNORE INTO packagers (packager) VALUES (?);
          __SQL__
        @insert_or_ignore_into_packagers.execute packager
        @dbh.last_insert_row_id
      end
    end  # module SolvDB::Schema::Packagers

    ####################################################################

    module Licenses
      include SolvDB::SQL

      def CREATE_TABLE_licenses
        CREATE_TABLE 'licenses',
                     'license_id INTEGER PRIMARY KEY',
                     'license TEXT UNIQUE'
      end

      def INSERT_INTO_licenses license: nil
        @insert_or_ignore_into_licenses ||=
          @dbh.prepare <<-__SQL__
            INSERT OR IGNORE INTO licenses (license) VALUES (?);
          __SQL__
        @insert_or_ignore_into_licenses.execute license
        @dbh.last_insert_row_id
      end
    end  # module SolvDB::Schema::Licenses

    ####################################################################

    module Solvables
      include SolvDB::SQL

      def CREATE_TABLE_solvables
        CREATE_TABLE 'solvables',
                     'solvable_id INTEGER PRIMARY KEY',
                     'repository_id INTEGER' \
                     '  REFERENCES repositories (repository_id)',
                     'name TEXT',
                     'arch TEXT',
                     'evr  TEXT',
                     'vendor_id INTEGER' \
                     '  REFERENCES vendors (vendor_id)',
                     'summary TEXT',
                     'description TEXT',
                     'packager_id INTEGER' \
                     '  REFERENCES packagers (packager_id)',
                     'url TEXT',
                     'buildtime INTEGER',
                     'installsize INTEGER',
                     'downloadsize INTEGER',
                     'mediadir TEXT',
                     'mediafile TEXT',
                     'license_id INTEGER ' \
                     '  REFERENCES licenses (license_id)',
                     'repogroup TEXT',
                     'buildhost TEXT',
                     'sourcename TEXT',
                     'sourceevr TEXT',
                     'sourcearch TEXT',
                     'headerend INTEGER'
      end

      def CREATE_INDEX_solvables_nevra
        CREATE_INDEX 'solvables_nevra',
                     on: 'solvables',
                     of: %w[name evr arch]
      end

      def CREATE_VIEW_nevra
        CREATE_VIEW 'nevra',
          as: <<-__SQL__  # rubocop:disable Layout/ArgumentAlignment
              SELECT
                s.solvable_id,
                FORMAT('%s-%s.%s', s.name, s.evr, s.arch) AS nevra
              FROM solvables AS s
          __SQL__
      end

      def CREATE_INDEX_solvables_group
        CREATE_INDEX 'solvables_group',
                     on: 'solvables',
                     of: %w[repogroup]
      end

      def CREATE_FTS5_TABLE_solvable_fts
        CREATE_FTS5_TABLE 'solvable_fts',
                          content: 'solvables',
                          rowid: 'solvable_id',
                          columns: %w[name arch summary
                                      description url repogroup]
      end

      def INSERT_INTO_solvables(repository_id: nil,
                                name: nil, arch: nil, evr: nil,
                                vendor_id: nil, summary: nil,
                                description: nil, packager_id: nil,
                                url: nil, buildtime: nil,
                                installsize: nil, downloadsize: nil,
                                mediadir: nil, mediafile: nil,
                                license_id: nil, repogroup: nil,
                                buildhost: nil, sourcename: nil,
                                sourceevr: nil, sourcearch: nil,
                                headerend: nil)
        @insert_into_solvables ||=
          INSERT_INTO 'solvables',
                      'repository_id',
                      'name', 'arch', 'evr',
                      'vendor_id', 'summary',
                      'description', 'packager_id',
                      'url', 'buildtime',
                      'installsize', 'downloadsize',
                      'mediadir', 'mediafile',
                      'license_id', 'repogroup',
                      'buildhost', 'sourcename',
                      'sourceevr', 'sourcearch',
                      'headerend'
        @insert_into_solvables.execute(
          repository_id, name, arch, evr, vendor_id, summary,
          description, packager_id, url, buildtime, installsize,
          downloadsize, mediadir, mediafile, license_id, repogroup,
          buildhost, sourcename, sourceevr, sourcearch, headerend
        )
        @dbh.last_insert_row_id
      end
    end  # module SolvDB::Schema::Solvables

    ####################################################################

    module Constraints
      include SolvDB::SQL

      def CREATE_TABLE_constraints
        CREATE_TABLE 'constraints',
                     'constraint_id INTEGER PRIMARY KEY',
                     'constrain TEXT UNIQUE'
      end

      def INSERT_INTO_constraints constraint: nil
        @insert_or_ignore_into_constraints ||=
          @dbh.prepare <<-__SQL__
            INSERT OR IGNORE INTO constraints (constrain) VALUES (?);
          __SQL__
        @insert_or_ignore_into_constraints.execute constraint
      end
    end  # module SolvDB::Schema::Constraints

    ####################################################################

    module Provides
      include SolvDB::SQL

      def CREATE_TABLE_provides
        CREATE_TABLE 'provides',
                     'solvable_id INTEGER' \
                     '  REFERENCES solvables (solvable_id)',
                     'constraint_id INTEGER' \
                     '  REFERENCES constraints (constraint_id)'
      end

      def INSERT_INTO_provides solvable_id: nil, provide: nil
        @insert_into_provides ||=
          @dbh.prepare <<-__SQL__
            INSERT OR IGNORE INTO provides (solvable_id, constraint_id)
              SELECT ? AS solvable_id, k.constraint_id
              FROM constraints AS k
              WHERE k.constrain = ?;
          __SQL__
        @insert_into_provides.execute solvable_id, provide
      end
    end  # module SolvDB::Schema::Provides

    ####################################################################

    module Requires
      include SolvDB::SQL

      def CREATE_TABLE_requires
        CREATE_TABLE 'requires',
                     'solvable_id INTEGER' \
                     '  REFERENCES solvables (solvable_id)',
                     'constraint_id INTEGER' \
                     '  REFERENCES constraints (constraint_id)'
      end

      def INSERT_INTO_requires solvable_id: nil, require: nil
        @insert_into_requires ||=
          @dbh.prepare <<-__SQL__
            INSERT OR IGNORE INTO requires (solvable_id, constraint_id)
              SELECT ? AS solvable_id, k.constraint_id
              FROM constraints AS k
              WHERE k.constrain = ?;
          __SQL__
        @insert_into_requires.execute solvable_id, require
      end
    end  # module SolvDB::Schema::Requires

    ####################################################################

    module Filelist
      include SolvDB::SQL

      def CREATE_TABLE_filelist
        CREATE_FTS5_TABLE 'filelist',
                          columns: ['file', 'solvable_id UNINDEXED'],
                          tokenize: "'trigram'"

        # CREATE_TABLE 'filelist',
        #              'solvable_id INTEGER' \
        #              '  REFERENCES solvables (solvable_id)',
        #              'file TEXT'
      end

      # def CREATE_INDEX_filelist_of
      #   CREATE_INDEX 'filelist_of', on: 'filelist', of: %w[file]
      # end

      def INSERT_INTO_filelist file: nil, solvable_id: nil
        @insert_into_filelist ||=
          INSERT_INTO 'filelist', 'file', 'solvable_id'
        @insert_into_filelist.execute file, solvable_id
        # @dbh.last_insert_row_id
      end
    end  # module SolvDB::Schema::Filelist

    ####################################################################

    module Misc
      include SolvDB::SQL

      def CREATE_VIEW_q_info
        CREATE_VIEW 'q_info',
          as: <<-__SQL__  # rubocop:disable Layout/ArgumentAlignment
            SELECT
              s.name AS Name,
              s.evr  AS EpochVersionRelease,
              s.arch AS Architecture,
              s.installsize AS InstalledSize,
              r.name AS FromRepository,
              s.summary AS Summary,
              s.url AS URL,
              l.license AS License,
              s.description AS Description,
              v.vendor AS Vendor
            FROM solvables AS s
            LEFT JOIN repositories AS r USING (repository_id)
            LEFT JOIN vendors AS v USING (vendor_id)
            LEFT JOIN licenses AS l USING (license_id)
          __SQL__
      end

      def CREATE_VIEW_q_whatprovides
        CREATE_VIEW 'q_whatprovides',
          as: <<-__SQL__  # rubocop:disable Layout/ArgumentAlignment
            SELECT p.provide, s.nevra
            FROM nevra AS s
            LEFT JOIN provides AS p USING (solvable_id)
          __SQL__
      end

      def CREATE_VIEW_q_whatrequires
        CREATE_VIEW 'q_whatrequires',
          as: <<-__SQL__  # rubocop:disable Layout/ArgumentAlignment
            SELECT r.require, s.nevra
            FROM nevra AS s
            LEFT JOIN requires AS r USING (solvable_id)
          __SQL__
      end
    end  # module SolvDB::Schema::Misc

    ####################################################################

    include SolvDB::Schema::Raw
    include SolvDB::Schema::Repositories
    include SolvDB::Schema::Repomds
    include SolvDB::Schema::Vendors
    include SolvDB::Schema::Packagers
    include SolvDB::Schema::Licenses
    include SolvDB::Schema::Solvables
    include SolvDB::Schema::Constraints
    include SolvDB::Schema::Provides
    include SolvDB::Schema::Requires
    include SolvDB::Schema::Filelist
    include SolvDB::Schema::Misc

    def CREATE_SCHEMA
      self.CREATE_TABLE_raw

      self.CREATE_TABLE_repositories

      self.CREATE_TABLE_repomds

      self.CREATE_TABLE_vendors

      self.CREATE_TABLE_packagers

      self.CREATE_TABLE_licenses

      self.CREATE_TABLE_solvables
      self.CREATE_INDEX_solvables_nevra
      self.CREATE_VIEW_nevra
      self.CREATE_INDEX_solvables_group
      self.CREATE_FTS5_TABLE_solvable_fts

      self.CREATE_TABLE_constraints

      self.CREATE_TABLE_provides

      self.CREATE_TABLE_requires

      self.CREATE_TABLE_filelist
      # self.CREATE_INDEX_filelist_of

      self.CREATE_VIEW_q_info
      self.CREATE_VIEW_q_whatprovides
      self.CREATE_VIEW_q_whatrequires
    end
  end  # module SolvDB::Schema

  ######################################################################

  module Command
    class FromJSON
      attr_reader :benches

      include SolvDB::SQL
      include SolvDB::Schema
      include SolvDB::DB

      def initialize database_file
        @benches = {}
        @database_file = database_file

        @dbh = db_connect @database_file
        self.CREATE_SCHEMA

        @vendors = {}
        @packagers = {}
        @licenses = {}
      end

      ##################################################################

      def load_solv label, json_text
        @benches[label] ||= []

        INSERT_INTO_raw libsolv_db: label, solvjson: json_text

        # @parse = []
        # @parser = Yajl::FFI::Parser.new

        # @parser.start_document do
        #   @parse.push [:document]
        #   puts "doc+ #{@parse.inspect}"
        # end

        # @parser.end_document do
        #   tag, = @parse.pop
        #   fail unless tag == :document
        #   puts "doc- #{@parse.inspect}"
        # end

        # @parser.start_object do
        #   @parse.push [:object, nil, {}]
        #   puts "obj+ #{@parse.inspect}"
        # end

        # @parser.end_object do
        #   tag, nk, h = @parse.pop
        #   fail unless tag == :object
        #   puts "obj- #{@parse.inspect}"
        # end

        # @parser.start_array do
        #   @parse.push [:array, []]
        #   puts "arr+ #{@parse.inspect}"
        # end

        # @parser.end_array do
        #   tag, xs = @parse.pop
        #   fail unless tag == :array
        #   puts "arr- #{@parse.inspect}"
        # end

        # @parser.key do |k|
        #   this = @parse.last
        #   fail unless this[0] == :object
        #   fail unless this[1].nil?
        #   this[1] = k.to_sym
        #   puts "key: #{@parse.inspect}"
        # end

        # @parser.value do |v|
        #   this = @parse.last
        #   case this[0]
        #   when :object then nextk = this[1]; this[1], this[2][nextk] = nil, v
        #   when :array then this[1] << v
        #   else fail "unknown #{this.inspect}"
        #   end
        #   puts ":val #{@parse.inspect}"
        # end

        # @parser << json_text

        obj = Oj.load json_text

        load_repositories label, obj
      end

      ##################################################################

      def load_repositories label, obj
        (obj[:repositories] || []).each do |repo|
          @dbh.transaction
          load_repository label, repo
          @dbh.commit
        end
      end

      def load_repository label, repo
        repository_id = load_repository_itself label, repo
        load_repomd label, repository_id, repo
        load_solvables label, repository_id, repo
      end

      def load_repository_itself label, repo
        repository_id = nil

        revision = repo[:'repository:revision'].to_i
        timestamp = repo[:'repository:timestamp']
        @benches[label] << Benchmark.measure('I_I_repositories') do
          repository_id = INSERT_INTO_repositories(
            name: label,
            revision: revision,
            timestamp: timestamp,
          )
        end

        repository_id
      end

      def load_repomd label, repository_id, repo
        @benches[label] << Benchmark.measure('I_I_repomds') do
          repo[:'repository:repomd'].each do |repomd|
            load_repomd_itself label, repository_id, repomd
          end
        end
      end

      def load_repomd_itself _label, repository_id, repomd
        mdtype = repomd[:'repository:repomd:type']
        checksum_sha256 = repomd[:'repository:repomd:checksum'][:value]
        location = repomd[:'repository:repomd:location']
        timestamp = repomd[:'repository:repomd:timestamp']
        size = repomd[:'repository:repomd:size']

        INSERT_INTO_repomds(
          repository_id: repository_id,
          mdtype: mdtype,
          checksum_sha256: checksum_sha256,
          location: location,
          timestamp: timestamp,
          size: size,
        )
      end

      def load_solvables label, repository_id, repo
        @benches[label] << Benchmark.measure('I_I_solvables') do
          repo[:solvables].each_with_index do |solv, n|
            load_solvable label, repository_id, solv
            $stdout.print '.' if (n % 32).zero?
          end
        end
      end

      def load_solvable label, repository_id, solv
        solvable_id = load_solvable_itself label, repository_id, solv
        load_provides label, solvable_id, solv
        load_requires label, solvable_id, solv
        load_filelist label, solvable_id, solv
      end

      def load_vendor vendor
        @vendors[vendor] ||=
          INSERT_INTO_vendors vendor: vendor
      end

      def load_packager packager
        @packagers[packager] ||=
          INSERT_INTO_packagers packager: packager
      end

      def load_license license
        @licenses[license] ||=
          INSERT_INTO_licenses license: license
      end

      def load_solvable_itself _label, repository_id, solv
        INSERT_INTO_solvables(
          repository_id: repository_id,
          name: solv[:'solvable:name'],
          arch: solv[:'solvable:arch'],
          evr: solv[:'solvable:evr'],
          vendor_id: load_vendor(solv[:'solvable:vendor']),
          summary: solv[:'solvable:summary'],
          description: solv[:'solvable:description'],
          packager_id: load_packager(solv[:'solvable:packager']),
          url: solv[:'solvable:url'],
          buildtime: solv[:'solvable:buildtime'].to_i,
          installsize: solv[:'solvable:installsize'].to_i,
          downloadsize: solv[:'solvable:downloadsize'].to_i,
          mediadir: solv[:'solvable:mediadir'],
          mediafile: solv[:'solvable:mediafile'],
          license_id: load_license(solv[:'solvable:license']),
          repogroup: solv[:'solvable:group'],
          buildhost: solv[:'solvable:buildhost'],
          sourcename: solv[:'solvable:sourcename'],
          sourceevr: solv[:'solvable:sourceevr'],
          sourcearch: solv[:'solvable:sourcearch'],
          headerend: solv[:'solvable:headerend'].to_i,
        )
      end

      def load_provides label, solvable_id, solv
        (solv[:'solvable:provides'] || []).each do |prov|
          load_provide_itself label, solvable_id, prov
        end
      end

      def load_provide_itself _label, solvable_id, prov
        INSERT_INTO_constraints constraint: prov
        INSERT_INTO_provides solvable_id: solvable_id, provide: prov
      end

      def load_requires label, solvable_id, solv
        (solv[:'solvable:requires'] || []).each do |req|
          load_require_itself label, solvable_id, req
        end
      end

      def load_require_itself _label, solvable_id, req
        INSERT_INTO_constraints constraint: req
        INSERT_INTO_requires solvable_id: solvable_id, require: req
      end

      def load_filelist label, solvable_id, solv
        (solv[:'solvable:filelist'] || []).each do |file|
          load_filelist_itself label, solvable_id, file
        end
      end

      def load_filelist_itself _label, solvable_id, file
        INSERT_INTO_filelist solvable_id: solvable_id, file: file
      end

      ######################################################################

      class << self
        def run *args
          database_path = DEFAULT_DATABASE_PATH
          db = SolvDB::Command::FromJSON.new database_path
          args.each do |solv|
            $stdout.print "#{solv} "
            solvdata = `dumpsolv -j #{solv}`
            db.load_solv File.basename(solv), solvdata
            $stdout.puts
          end

          db.REBUILD_INDEX_solvable_fts
          db.PRAGMA_optimize

          db.benches.each do |k, v|
            puts "#{k} ---"
            Benchmark.benchmark(Benchmark::CAPTION, 32, Benchmark::FORMAT) do
              vsum = v.inject { |acc, x| acc + x }
              [*v, vsum, vsum / v.length]
            end
          end
        end
      end
    end  # class SolvDB::Command::FromJSON

    ########################################################################

    class Search
      include SolvDB::SQL
      include SolvDB::Schema
      include SolvDB::DB

      def initialize database_file
        @database_file = database_file
        @dbh = db_connect @database_file
        self.CREATE_SCHEMA
      end

      def search terms
        @search ||=
          @dbh.prepare <<-__SQL__
            SELECT
              FORMAT('%s.%s: %s', s.name, s.arch, s.summary)
            FROM solvable_fts AS s
            WHERE name MATCH ?
               OR summary MATCH ?
          __SQL__
        terms = terms.join(' ')
        @search.execute terms, terms
      end

      class << self
        def run *args
          database_path = DEFAULT_DATABASE_PATH
          db = SolvDB::Command::Search.new database_path
          db.search(args).map { |x| x[0] }.each { |x| puts x }
        end
      end
    end  # class SolvDB::Command::Search

    ####################################################################

    class Info
      include SolvDB::SQL
      include SolvDB::Schema
      include SolvDB::DB

      def initialize database_file
        @database_file = database_file
        @dbh = db_connect @database_file
        self.CREATE_SCHEMA
      end

      def info package
        @info ||=
          @dbh.prepare <<-__SQL__
            SELECT * FROM q_info WHERE Name = ?
          __SQL__
        @info.execute package
      end

      class << self
        def run *args
          database_path = DEFAULT_DATABASE_PATH
          db = SolvDB::Command::Info.new database_path

          order = %w[
            Name EpochVersionRelease Architecture InstalledSize
            FromRepository Summary URL License Description Vendor
          ]
          db.info(args).each_hash do |h|
            order.each { |k| puts format '%-16s: %s', k, h[k].to_s }
          end
        end
      end
    end  # class SolvDB::Command::Info
  end  # module SolvDB::Command
end  # module SolvDB

########################################################################

case File.basename $PROGRAM_NAME
when 'solvdb-fromjson' then SolvDB::Command::FromJSON.run(*ARGV)
when 'solvdb-search'   then SolvDB::Command::Search.run(*ARGV)
when 'solvdb-info'     then SolvDB::Command::Info.run(*ARGV)
else raise "unknown: #{$PROGRAM_NAME}"
end
