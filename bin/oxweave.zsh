#!/usr/bin/env zsh
# NAME: oxweave --- weave an Org-mode file
# SYNOPSIS: oxweave <format> <source-file.org>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/emacs"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 2 ] || usage

emacs_evals=(
	"(require 'org)"
	"(add-to-list 'org-modules 'ol-eshell)"
	"(add-to-list 'org-modules 'ol-man)"
)
emacs_args+=("-L" "$HOME/.emacs.d/straight/build/org")

run-with-current-buffer() {
	printf "
		(with-current-buffer
		    (find-file-noselect \"%s\")
		  %s)
	" "$1" "$2"
}

requires() {
	for arg
	do
		case "$arg" in
			(htmlize)
				emacs_args+=("-L" "$HOME/.emacs.d/straight/build/htmlize")
			;;
		esac
		emacs_evals+=("(require '${arg})")
	done
}

ox-export-to() {
	local mod="$1"
	local out="${2-${mod}}"
	local async="nil"
	local subtreep="nil"
	local visibleonly="nil"
	local bodyonly="nil"
	local extplist="${3-'()}"
	printf "(org-%s-export-to-%s %s %s %s %s %s)" \
		"$mod" "$out" \
		"$async" "$subtreep" "$visibleonly" "$bodyonly" \
		"$extplist"
}

ox-ascii() {
	ox-export-to ascii ascii \
		"$(printf "'(:ascii-charset %s :ascii-text-width 80)" "$1")"
}

case "${(L)1}" in
	(ascii)
		requires ox-ascii
		exporter="$(ox-ascii "${(L)1}")"
	;;
	(latin1)
		requires ox-ascii
		exporter="$(ox-ascii "${(L)1}")"
	;;
	(utf8|utf-8|text)
		requires ox-ascii
		exporter="$(ox-ascii "utf-8")"
	;;

	(confluence)
		requires ox-confluence
		exporter="$(ox-export-to confluence)"
	;;

	(freemind)
		requires ox-freemind
		exporter="$(ox-export-to confluence)"
	;;

	(html|xhtml)
		requires ox-html htmlize
		exporter="$(ox-export-to html)"
	;;
	(md|markdown)
		requires ox-html htmlize ox-md
		exporter="$(ox-export-to md markdown)"
	;;
	(deckjs|deck)
		requires ox-html htmlize ox-deck
		exporter="$(ox-export-to deck html)"
	;;
	(s5js|s5)
		requires ox-html htmlize ox-s5
		exporter="$(ox-export-to s5 html)"
	;;

	(latex)
		requires ox-latex
		exporter="$(ox-export-to latex)"
	;;
	(beamer)
		requires ox-latex ox-beamer
		exporter="$(ox-export-to beamer latex)"
	;;
	(koma|scrlttr2)
		requires ox-latex ox-koma-letter
		exporter="$(ox-export-to koma-letter latex)"
	;;

	(groff|troff)
		requires ox-groff
		exporter="$(ox-export-to groff)"
	;;

	(man)
		requires ox-man
		exporter="$(ox-export-to man)"
	;;

	(odt|opendocument)
		requires ox-odt
		exporter="$(ox-export-to odt)"
	;;

	(org)
		requires ox-org
		exporter="$(ox-export-to org)"
	;;

	(rss)
		requires ox-rss
		exporter="$(ox-export-to rss)"
	;;

	(texi|texinfo)
		requires ox-texinfo
		exporter="$(ox-export-to texinfo)"
	;;
	(info)
		requires ox-texinfo
		exporter="$(ox-export-to texinfo info)"
	;;
esac

emacs_evals+=("$(run-with-current-buffer "$2" "$exporter")")

eval_arg=("--eval")
emacs_args+=("${eval_arg:^^emacs_evals}")
emacs-invoke "${emacs_args[@]}"
