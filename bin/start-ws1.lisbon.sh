#!/bin/sh

i3-msg 'append_layout ~/.i3/layout.lisbon.ws1.json'

sleep 1

xfce4-terminal &
sleep 0.1

xfce4-terminal \
	--title='emacs' &
sleep 0.1

xfce4-terminal \
	--command='bluetoothctl' \
	--title='bluetoothctl' &
sleep 0.1

xfce4-terminal \
	--title='wpa_cli' \
	--command='wpa_cli -i wlp3s0' &
sleep 0.1

xfce4-terminal \
	--title='radeontop' \
	--command='radeontop --ticks 12' &
sleep 0.1

xfce4-terminal \
	--title='top' \
	--command='top' &
sleep 0.1

pavucontrol &

xfce4-terminal \
	--title='journalctl' \
	--command='journalctl --boot=0 --lines=all --follow' &
sleep 0.1

xfce4-terminal &
