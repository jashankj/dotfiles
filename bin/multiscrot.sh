#!/bin/sh

time=`date +%Y%m%d.%H%M%S`
xdpyinfo -ext XINERAMA \
    | sed '/^  head #/!d;s///' \
    | while IFS=' :x@,' read i w h x y; do
          import -window root -crop ${w}x$h+$x+$y ~/Pictures/Screenshots/`hostname -s`.$i.$time.png
      done
