#!/usr/bin/env zsh
# Run `xss-lock' and `xsecurelock' for display blanking and locking.
# This supplants the xscreensaver frontend; but uses its hacks.

which xsecurelock || exit
xsecurelock_bin="$(which xsecurelock)"


for d in \
	"$(dirname "${xsecurelock_bin}")"/../lib/xsecurelock \
	"$(dirname "${xsecurelock_bin}")"/../libexec/xsecurelock
do
	[ -d "$d" ] || continue
	xsecurelock_lib="$d"
done

[ -z "$xsecurelock_lib" ] && exit

# Find where the `xscreensaver' hacks are.
# * Arch: `/usr/lib/xscreensaver'
# * Debian: `/usr/libexec/xscreensaver'
# * FreeBSD: `/usr/local/bin/xscreensaver-hacks'.
for d in \
	/usr/lib/xscreensaver \
	/usr/libexec/xscreensaver \
	/usr/local/bin/xscreensaver-hacks
do
	[ -d "$d" ] || continue
	xsecurelock_xscreensaver_path="$d"
done

xsecurelock_vars=(
	# Before locking, dim the screen.
	XSECURELOCK_ENABLE_DIMMER=true
	XSECURELOCK_DIM_TIME_MS=2000
	XSECURELOCK_WAIT_TIME_MS=8000

	# Force grabbing of keyboard/mouse on lock.
	XSECURELOCK_FORCE_GRAB=1

	# Hack around compositor misbehaviour.
	XSECURELOCK_COMPOSITE_OBSCURER=1

	# Turn monitor off 30 seconds after locking
	XSECURELOCK_BLANK_DPMS_STATE=off
	XSECURELOCK_BLANK_TIMEOUT=30

	# While locked, show XScreenSaver hacks.
	XSECURELOCK_SAVER=saver_xscreensaver
	XSECURELOCK_XSCREENSAVER_PATH="${xsecurelock_xscreensaver_path}"

	# When unlocking, don't swallow the first keystroke.
	XSECURELOCK_DISCARD_FIRST_KEYPRESS=0

	# Tweak the displayed UI to taste.
	XSECURELOCK_FONT='Fira Code-24'
	XSECURELOCK_PASSWORD_PROMPT=cursor
	XSECURELOCK_SHOW_DATETIME=1
	XSECURELOCK_SHOW_HOSTNAME=1
	XSECURELOCK_SHOW_USERNAME=1
)

exec \
env "${xsecurelock_vars[@]}" \
xss-lock \
	--notifier="${xsecurelock_lib}/dimmer" \
	--transfer-sleep-lock \
xsecurelock
