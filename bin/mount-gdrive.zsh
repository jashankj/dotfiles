#!/usr/bin/env zsh
# NAME: mount-gdrive --- mount my Google Drive as a FUSE filesystem

mountpoint=/run/user/1000/gdfuse

mkdir -p ${mountpoint}
exec \
google-drive-ocamlfuse ${mountpoint}
