#!/usr/bin/env zsh
# NAME: obtangle --- tangle an Org-mode Babel file
# SYNOPSIS: obtangle <source-file.org>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/emacs"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

[ $# = 1 ] || usage

emacs-invoke \
	--eval "(setq gc-cons-threshold most-positive-fixnum)" \
	--eval "(setq gc-cons-percentage 0.6)" \
	--eval "(require 'org)" \
	--eval "
		(defun jashankj/obtangle (file)
		  (with-current-buffer
		      (find-file-noselect file)
		    (mapc
		     (lambda (it)
		       (message \"%s\" it)
		       (with-current-buffer
			   (find-file-noselect it)
			 (delete-trailing-whitespace (point-min) (point-max))
			 (save-buffer)))
		     (org-babel-tangle))))
	" \
        --eval "$(printf "
			(jashankj/obtangle \"%s\")
			(kill-emacs)
		" "$1")"

#	--eval "(require 'ox)" \
#	--eval "(require 'ob-tangle)" \
#	--eval "
#		(defun jashankj/obtangle--with-include (orig &rest args)
#		  (org-export-with-buffer-copy
#		    (org-export-expand-include-keyword)
#		    (apply orig args)))
#	" \
#
# #		      (set-visited-file-name nil)
# #		      (set-visited-file-name (concat \".\" (buffer-name)))
# #		      (rename-buffer (buffer-name) t)
#	--eval "
#		(advice-add
#		 'org-babel-tangle
#		 :around
#		 #'jashankj/obtangle--with-include)" \
