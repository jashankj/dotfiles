#!/usr/bin/env zsh
# NAME: mail-rrdcounts --- generate RRDtool databases from mail counts
# SYNOPSIS: mail-rrdcounts

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

typeset -a repos
repos=($(ccat ~/etc/mail.repos))

echo -n "Updating RRDtool databases:"
for repo in ${repos}
do
	echo -n " ${repo}"
	rrd_db=".rrd.${repo}"
	if [ ! -e "${rrd_db}" ]
	then
		rrdtool create ${rrd_db} \
			--start 1600000000 \
			--step  60 \
			DS:messages:GAUGE:1d:0:U \
			RRA:AVERAGE:0.99:1m:28d \
			RRA:AVERAGE:0.99:1h:365d \
			RRA:AVERAGE:0.99:6h:10y
#			DS:delta:DERIVE:1d:U:U \
#			DS:actions:ABSOLUTE:1d:0:U \
	fi

	< ${repo}.count \
	| awk '{ print $1 ":" $2 }' \
	| xargs rrdtool update --skip-past-updates ${rrd_db} --
done
echo .
