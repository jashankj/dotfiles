#!/usr/bin/env ruby
# frozen_string_literal: true
# typed: false

require 'set'

def load_map(file)
  File.read(file)
      .lines
      .map(&:chomp)
      .map { |l| l.split(/[[:space:]]+/, 3) }
      .map { |l| l[1..2].map(&:to_i) }
      .to_h
end

UID_MAP_FILE = ARGV.shift
UID_MAP      = load_map UID_MAP_FILE

GID_MAP_FILE = ARGV.shift
GID_MAP      = load_map GID_MAP_FILE

def run_rewrite(line, name, pattern, map)
  return unless (match = pattern.match(line))
  id = match.captures[0].to_i
  idx = map[id] || return
  line.gsub!(pattern, " #{name}=#{idx} ")
  { id => idx }
end

seen = Set.new
ARGF.each_line do |line|
  line.chomp!
  u = run_rewrite line, 'uid', / uid=(\d+) /, UID_MAP
  g = run_rewrite line, 'gid', / gid=(\d+) /, GID_MAP
  line += " # #{u} #{g}" if $DEBUG && (u || g)
  puts line

  if line.start_with?('#') && !seen.include?(line)
    $stderr.puts line; seen.add line
  end
end
