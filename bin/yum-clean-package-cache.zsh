#!/usr/bin/env zsh

cachedir=/var/cache/libdnf5
wrkdir="${TMPDIR?}/dcpc"
mkdir -p "${wrkdir}"
export LC_ALL=C

N=$(nproc)
alias psort="sort --parallel=${N?}"

database="${wrkdir}/work.db"
alias DB="sqlite3 ${database}"

create_schema() {
	DB <<__EOF__
		PRAGMA journal_mode = MEMORY;
		PRAGMA page_size = $(( 1 << 14 ));
		PRAGMA mmap_size = $(( 1 << 28 ));
		PRAGMA cache_size = $(( 1 << 14 ));

		CREATE TABLE IF NOT EXISTS
			installed_raw (package TEXT);
		CREATE VIEW IF NOT EXISTS
			installed
		AS
			SELECT DISTINCT package
			FROM installed_raw;
		CREATE INDEX IF NOT EXISTS
			installed_raw_packages
			ON installed_raw (package);

		CREATE TABLE IF NOT EXISTS
			latestavail_raw (package TEXT);
		CREATE VIEW IF NOT EXISTS
			latestavail
		AS
			SELECT DISTINCT package
			FROM latestavail_raw;
		CREATE INDEX IF NOT EXISTS
			latestavail_raw_packages
			ON latestavail_raw (package);

		CREATE TABLE IF NOT EXISTS
			cached_raw (package TEXT, filename TEXT);
		CREATE VIEW IF NOT EXISTS
			cached
		AS
			SELECT DISTINCT package, filename
			FROM cached_raw;
		CREATE INDEX IF NOT EXISTS
			cached_raw_packages
			ON cached_raw (package);

		CREATE VIEW IF NOT EXISTS
			installed_or_available
		AS
			SELECT DISTINCT package
			FROM (
				SELECT * FROM installed
				UNION ALL
				SELECT * FROM latestavail
			);

		CREATE VIEW IF NOT EXISTS
			cached_but_irrelevant_files
		AS
			SELECT DISTINCT filename FROM (
				SELECT
					cached.filename AS filename,
					cached.package  AS c_package,
					installed_or_available.package AS ioa_packaged
				FROM cached
				FULL JOIN installed_or_available USING (package)
				WHERE installed_or_available.package IS NULL
			);

__EOF__
}

insert_installed() {
	rpm -qa \
	| awk '
		BEGIN	{ print "BEGIN;" };
		{ printf("INSERT INTO installed_raw (package)");
		  printf(" VALUES ('"'"'%s'"'"');\n", $1); }
		END	{ print "COMMIT;";
			  print "PRAGMA optimize;" };
	' \
	| DB
}

insert_latestavail() {
	# dnf5 removed `--all'; one must select available+installed.
	# dnf5 removed `--envra'; pilfering its definition gives
	dnf \
		--cacheonly \
	repoquery \
		--available --installed \
		--queryformat='%{epoch}:%{name}-%{version}-%{release}.%{arch}' \
	| cut -d: -f2- \
	| awk '
		BEGIN	{ print "BEGIN;" };
		{ printf("INSERT INTO latestavail_raw (package)");
		  printf(" VALUES ('"'"'%s'"'"');\n", $1); }
		END	{ print "COMMIT;";
			  print "PRAGMA optimize;" };
	' \
	| DB
}

insert_cached() {
	find */packages -type f \
	| nice -n20 \
	  ionice -c3 \
	  parallel --tag -j${N?} \
	  rpm -q \
	| awk '
		BEGIN	{ print "BEGIN;" };
		{ printf("INSERT INTO cached_raw (filename, package)");
		  printf(" VALUES ('"'"'%s'"'"', '"'"'%s'"'"');\n", $1, $2); }
		END	{ print "COMMIT;";
			  print "PRAGMA optimize;" };
	' \
	| DB
}

pushd ${cachedir}

echo "+ create_schema" >&2
create_schema

echo "+ insert_installed" >&2
insert_installed

echo "+ insert_latestavail" >&2
insert_latestavail

echo "+ insert_cached" >&2
insert_cached

DB <<__EOF__
	PRAGMA page_size = $(( 1 << 14 ));
	PRAGMA mmap_size = $(( 1 << 28 ));
	PRAGMA cache_size = $(( 1 << 14 ));
SELECT * FROM cached_but_irrelevant_files;
__EOF__
