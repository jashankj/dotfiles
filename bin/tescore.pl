#!/usr/bin/env perl

use strict;
use warnings;

use DBI;
use DBD::SQLite;

my $HOME = $ENV{'HOME'};
my $TEDB = $HOME . '/.tescores.db';
my $DBH = DBI->connect(
	"dbi:SQLite:dbname=$TEDB",
	'', '', { ReadOnly => 1 }
);

my $STHx = {};

my $MODES = {
	'connected_sr'		=> 'Conn',
	'connected_areas'	=> 'As',
	'zone_battle_sr'	=> 'ZB',
	'score_attack_sr'	=> 'SA',
	'classic_sa_sr'		=> 'CSA',
};

my @ORDER = qw{
	connected_sr connected_areas zone_battle_sr
	score_attack_sr classic_sa_sr
};

my $LAST_N_LEN = 10;
$STHx->{lastN} = $DBH->prepare(<<__EOF__);
	SELECT *
	FROM tescores
	ORDER BY created_at DESC
	LIMIT $LAST_N_LEN;
__EOF__

$STHx->{overall_range} = $DBH->prepare(<<__EOF__);
	SELECT MIN(overall_sr), MAX(overall_sr)
	FROM (
		SELECT overall_sr
		FROM tescores
		ORDER BY created_at DESC
		LIMIT $LAST_N_LEN
	);
__EOF__

foreach my $mode (@ORDER) {
	$STHx->{$mode} = $DBH->prepare(<<__EOF__);
		SELECT created_at, $mode
		FROM tescores
		WHERE $mode IS NOT NULL
		ORDER BY created_at DESC
		LIMIT 2;
__EOF__
}

print <<__EOF__;
<!DOCTYPE html>
<html lang="en" data-bs-theme="dark">
<head>
<meta charset="utf-8" />
<meta http-equiv="refresh" content="50" />
<meta name="generator" content="tesr+tescore" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap\@5.3.0-alpha3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
	crossorigin="anonymous" />
</head>
<body>
<div class="container">
<div class="row">
__EOF__

$STHx->{lastN}->execute or die "couldn't get last N games";
my $lastN = $STHx->{lastN}->fetchall_hashref('created_at');
my @lastN_timestamps = reverse sort keys %$lastN;

$STHx->{overall_range}->execute or die "couldn't get overall range: $!";
my ($overall_min, $overall_max) = $STHx->{overall_range}->fetchrow_array;
my $overall_range = $overall_max - $overall_min;

my $current_overall_sr = $lastN->{$lastN_timestamps[0]}->{overall_sr};
print "<div class=\"col-3 text-nowrap fs-5\">";
print "overall<br/>";
print "<strong>$current_overall_sr</strong>\n";

my $LAST_N_WIDTH = 5.0; # ex
sub BAR {
	my ($w, $h) = @_;
	return
		"<span style=\"" .
		"display: inline-block; " .
		"width: ".$w."ex; " .
		"height: 0; " .
		"padding-bottom: ".$h."ex; " .
		"background: var(--bs-secondary-bg);" .
	"\">&nbsp;</span>"
}

print "<span style=\"background: var(--bs-secondary-color);\">";
foreach my $game (@lastN_timestamps) {
	my $sr_in_range = $lastN->{$game}->{overall_sr} - $overall_min;
	my $sr_range_fraction = $sr_in_range / $overall_range;
	print BAR($LAST_N_WIDTH / $LAST_N_LEN, $sr_range_fraction);
}
print "</span></div>\n";

foreach my $mode (@ORDER) {
	$STHx->{$mode}->execute or die "couldn't fetch games for $mode: $!";
	my $games = $STHx->{$mode}->fetchall_arrayref;
	my ($newer_ctime, $newer_score) = @{$games->[0]};
	my ($older_ctime, $older_score) = @{$games->[1]};

	print "<div class=\"col text-center\">";
	print $MODES->{$mode};
	print "<br/><strong>";
	print $newer_score;
	print "</strong></div>\n";
}

print <<__EOF__;
</div> <!-- .row -->
</div> <!-- .container -->
</body>
</html>
__EOF__

