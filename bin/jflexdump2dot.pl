#!/usr/bin/env perl
#
#-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2020 Jashank Jeremy <jashankj cse unsw edu au>
# Copyright 2023 Jashank Jeremy <jashank rulingia com au>
#

use v5;
use strict;
use warnings;
use feature 'say';
use feature 'switch'; no warnings 'experimental::smartmatch';

use constant (INITIAL   => 0);
use constant (READ_NFA  => 1);
use constant (READ_DFA  => 2);
use constant (READ_MDFA => 3);

my $state = INITIAL;

my $aState;
while (<>) {
	chomp; study;
	my $input = $_;

	given ($state) {
		when (INITIAL) {
			given ($input) {
				when (/NFA is/)         {
					say "/* NFA is */";
					$state = READ_NFA; }
				when (/Miniminal DFA is/) {
					say "/* min.DFA is */";
					$state = READ_MDFA; }
				when (/DFA is/)         {
					say "/* DFA is */";
					$state = READ_DFA; }
			}
		}

		when (READ_NFA) {
			given ($input) {
				when (/^State(\[FINAL\])? (\d+)$/) {
					$aState = $2;
					say sprintf 's%d [style=bold];', $aState if defined $1;
				}
				when (/^  with (\d+|epsilon) in \{([^}]+)\}$/) {
					my $cclass = $1;
					$cclass = 'ε' if $cclass eq 'epsilon';
					foreach my $aNext (split /,/, $2) {
						say sprintf 's%d -> s%d [label="%s"];',
						  $aState, $aNext, $cclass;
					}
				}
				when (/^\d+ states in NFA/) { $state = INITIAL; }
			}
		}

		when ([READ_DFA, READ_MDFA]) {
			given ($input) {
				when (/^State( \[FINAL\])? (\d+):/) {
					$aState = $2;
					say sprintf 's%d [style=bold];', $aState if defined $1;
				}
				when (/^  with (\d+) in (\d+)/) {
					my $cclass = $1;
					my $aNext  = $2;
					say sprintf 's%d -> s%d [label="%s"];',
					  $aState, $aNext, $cclass;
				}
				when (/^\s*$/) { $state = INITIAL };
			}
		}
	}
}
