#!/usr/bin/env zsh
# NAME: pi-extract-between --- extract emails between two commits into a maildir
# SYNOPSIS: pi-extract-between <init> <fini>

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

case $# in
	2)
		init="${1}"
		fini="${2}"
		;;
	*)
		usage
		;;
esac

git log --format='%H %at' "${init}..${fini}" |
while read cid dt
do
	echo "$dt" $(git ls-tree "${cid}")
done |
awk '{print $1, $4}' |
while read dt oid
do
	f="../0/new/$oid"
	[ -e "$f" ] && continue
	echo "$oid"
	git cat-file blob "$oid" > "$f"
	touch -d "@$dt" "$f"
done
