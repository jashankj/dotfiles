#!/usr/bin/env perl
#-
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#
# Portions derived from GNU Emacs, which is copyright
# Copyright (C) Free Software Foundation, Inc.
# Contains constants defined in:
#   + //emacs/src/pdumper.c
#     Copyright (C) 2018-2022 Free Software Foundation, Inc.
#   + //emacs/src/pdumper.h
#     Copyright (C) 2016, 2018-2022 Free Software Foundation, Inc.
#

#
# Examine an Emacs portable-dumper file.
# Apparently there's no already-extant tool that dumps it!
#

########################################################################
# Various constants.

package pdump;
package pdump::sizeof;
use constant dump_off => 4;
use constant emacs_reloc => (dump_off + dump_off + dump_off);

########################################################################
# Helpers for reading types:

package pdump;
use strict;
use warnings;
use base qw(Exporter);
our @EXPORT = qw(
	dump_off_read field_read xread
	EARLY_RELOCS LATE_RELOCS VERY_LATE_RELOCS RELOC_NUM_PHASES
);
our @EXPORT_OK = ();

use Fcntl 'SEEK_SET';

use constant EARLY_RELOCS	=> 0;
use constant LATE_RELOCS	=> 1;
use constant VERY_LATE_RELOCS	=> 2;
use constant RELOC_NUM_PHASES	=> 3;


# typedef int_least32_t dump_off;
# #define DUMP_OFF_MIN INT_LEAST32_MIN
# #define DUMP_OFF_MAX INT_LEAST32_MAX
# #define PRIdDUMP_OFF PRIdLEAST32

sub dump_off_read($$) {
	my ($fp, $offset) = @_;
	my $result = xreadat($fp, $offset, pdump::sizeof::dump_off);
	unless (defined ($result)) {
		warn 'dump_off_read: while reading';
		return undef;
	}

	return unpack 'L', $result;
}

sub field_read($$$$$) {
	my ($fp, $struct, $field, $offset, $length) = @_;
	my $result = xreadat($fp, $offset, $length);
	unless (defined ($result)) {
		warn sprintf(
			'field_read: while reading %s.%s',
			$struct, $field
		);
		return undef;
	}
	return $result;
}

# Read LENGTH bytes at OFFSET from file FP.
# If OFFSET is undefined, keeps reading at the next location.
# If OFFSET is defined, seeks first to a specific location.
sub xreadat($$$) {
	my ($fp, $offset, $length) = @_;
	if (defined($offset)) {
		if ($fp->seek($offset, SEEK_SET) != 1) {
			warn sprintf(
				'xreadat: +%d: seeking to %d; %s',
				$fp->tell(), $offset, $!
			);
			return undef;
		}
	}

	my $result;
	my $read = $fp->read($result, $length);
	unless ($read == $length) {
		warn sprintf(
			'xreadat: +%d: read %d, want %d; %s',
			$fp->tell(), $read, $length, $!
		);
		return undef;
	}

	return $result;
}

sub xread($$) {
	my ($fp, $length) = @_;
	return xreadat($fp, undef, $length);
}

########################################################################

package pdump::readable;
use Moose::Role;
requires 'read';

package pdump::explicable;
use Moose::Role;
requires 'explain';

########################################################################

package pdump::dump_table_locator;
use Moose;

pdump->import;

# Offset in dump, in bytes, of the first entry in the dump table.
has 'offset' => ('is' => 'ro', 'isa' => 'Int');

# Number of entries in the dump table.  We need an explicit end
# indicator (as opposed to a special sentinel) so we can efficiently
# binary search over the relocation entries.
has 'nr_entries' => ('is' => 'ro', 'isa' => 'Int');

sub read {
	my ($class, $fp, $base) = @_;
	my $dtl = {};

	$dtl->{'offset'} = dump_off_read($fp, $base);
	return undef unless defined $dtl->{'offset'};

	$dtl->{'nr_entries'} = dump_off_read($fp, undef);
	return undef unless defined $dtl->{'nr_entries'};

	return $class->new(%$dtl);
}

sub explain {
	my $self = shift;
	sprintf '@%d (%d entries)',
		$self->offset,
		$self->nr_entries;
}

with 'pdump::readable', 'pdump::explicable';


########################################################################

package pdump::emacs_reloc;
use strict;
use warnings;
use Moose;
pdump->import;

# Copy raw bytes from the dump into Emacs.  The length field in the
# emacs_reloc is the number of bytes to copy.
use constant COPY_FROM_DUMP	=> 0;

# Set a piece of memory in Emacs to a value we store directly in
# this relocation.  The length field contains the number of bytes
# we actually copy into Emacs.
use constant IMMEDIATE		=> 1;

# Set an aligned pointer-sized object in Emacs to a pointer into
# the loaded dump at the given offset.  The length field is
# always the machine word size.
use constant DUMP_PTR_RAW	=> 2;

# Set an aligned pointer-sized object in Emacs to point to
# something also in Emacs.  The length field is always
# the machine word size.
use constant EMACS_PTR_RAW	=> 3;

# Set an aligned Lisp_Object in Emacs to point to a value in the
# dump.  The length field is the _tag type_ of the Lisp_Object,
# not a byte count!
use constant DUMP_LV		=> 4;

# Set an aligned Lisp_Object in Emacs to point to a value in the
# Emacs image.  The length field is the _tag type_ of the
# Lisp_Object, not a byte count!
use constant EMACS_LV		=> 5;


use constant TYPE_BITS    => 3;
use constant LENGTH_BITS  => (pdump::sizeof::dump_off * 8) - TYPE_BITS;


# bitfield, TYPE_BITS wide ---
has 'type' => (is => 'ro', isa => 'Num');

sub is_copy_from_dump	{ return shift->type == COPY_FROM_DUMP }
sub is_immediate	{ return shift->type == IMMEDIATE }
sub is_dump_ptr_raw	{ return shift->type == DUMP_PTR_RAW }
sub is_emacs_ptr_raw	{ return shift->type == EMACS_PTR_RAW }
sub is_dump_lv		{ return shift->type == DUMP_LV }
sub is_emacs_lv		{ return shift->type == EMACS_LV }

# bitfield, LENGTH_BITS wide ---
has 'length' => (is => 'ro', isa => 'Num');

has 'emacs_offset' => (is => 'ro', isa => 'Num');

# a less perfect union, or something
has '_u' => (is => 'ro', isa => 'Num');
sub dump_offset		{ return shift->_u }
sub emacs_offset2	{ return shift->_u }
sub immediate		{ return shift->_u }

sub read {
	my ($class, $fp, $offset) = @_;
	my $h = {};

	# We read three dump-off's.
	my ($o0, $o1, $o2);

	unless (defined($o0 = dump_off_read($fp, $offset))) {
		die "emacs_reloc::read: type|length"; }
	unless (defined($o1 = dump_off_read($fp, undef))) {
		die "emacs_reloc::read: offset"; }
	unless (defined($o2 = dump_off_read($fp, undef))) {
		die "emacs_reloc::read: u"; }
	printf "o0 == 0x\%08x, o1 == 0x\%08x, o2 == 0x\%08x\n", $o0, $o1, $o2;
	die;

	return $class->new(%$h);
}

sub explain {
	my $self = shift;
	my $out = '';
	$out .= "Object: emacs-reloc\n";
	$out .= sprintf "Type: \%s\n", $self->type_name;
	$out .= sprintf "Length: \%d\n", $self->length;

	return $out;
}

with 'pdump::readable', 'pdump::explicable';


########################################################################

package pdump::dump_reloc;
use strict;
use warnings;
use Moose;
pdump->import;

sub read {
	my ($class, $fp) = @_;
	my $h = {};

	return $class->new(%$h);
}

sub explain {
	my $self = shift;
	my $out = '';
	$out .= "Object: dump-reloc\n";

	return $out;
}

with 'pdump::readable', 'pdump::explicable';


########################################################################

package pdump::dump_header;
use strict;
use warnings;
use Moose;
pdump->import;

my @DUMP_MAGIC = (
	'D', 'U', 'M', 'P', 'E', 'D',
	'G', 'N', 'U',
	'E', 'M', 'A', 'C', 'S'
);

# File type magic.
has 'magic' => (is => 'ro', isa => 'Str');

# Associated Emacs binary.
has 'fingerprint' => (is => 'ro', isa => 'Str');

# Relocation table for the dump file.
# Each entry is a struct dump_reloc.
has 'dump_relocs_dtl' => (is => 'ro', isa => 'ArrayRef[pdump::dump_table_locator]');

# "Relocation" table we abuse to hold information about the location
# and type of each lisp object in the dump.  We need for
# pdumper_object_type and ultimately for conservative GC correctness.
has 'object_starts_dtl' => (is => 'ro', isa => 'pdump::dump_table_locator');

# Relocation table for Emacs.
# Each entry is a struct emacs_reloc.
has 'emacs_relocs_dtl' => (is => 'ro', isa => 'pdump::dump_table_locator');
has 'emacs_relocs' => (is => 'ro', isa => 'ArrayRef[pdump::emacs_reloc]');

# Start of sub-region of hot region that we can discard after load
# completes.  The discardable region ends at cold_start.
has 'discardable_start' => (is => 'ro', isa => 'Num');

# Start of the region that does not require relocations and that we
# expect never to be modified.  This region can be memory-mapped
# directly from the backing dump file with the reasonable expectation
# of taking few copy-on-write faults.
has 'cold_start' => (is => 'ro', isa => 'Num');

# Offset of a vector of the dumped hash tables.
has 'hash_list' => (is => 'ro', isa => 'Num');

sub read {
	my ($class, $fp, $offset) = @_;
	my $h = {};

	$h->{'magic'}       = field_read($fp, 'dump_header', 'magic', $offset, 16);
	die 'dump_header::read: magic' unless defined $h->{'magic'};

	$h->{'fingerprint'} = field_read($fp, 'dump_header', 'fingerprint', undef, 32);
	die 'dump_header::read: fingerprint' unless defined $h->{'fingerprint'};

	$h->{'dump_relocs_dtl'} = [];
	for (my $i = 0; $i < pdump::RELOC_NUM_PHASES; $i++) {
		my $dtl = pdump::dump_table_locator->read($fp, undef);
		die "dump_header::read: dump_relocs[$i]" unless defined $dtl;
		$h->{'dump_relocs_dtl'}->[$i] = $dtl;
	}
	# (struct dump_table_locator [RELOC_NUM_PHASES])

	my $osdtl = pdump::dump_table_locator->read($fp, undef);
	die "dump_header::read: object_starts_dtl" unless defined $osdtl;
	$h->{'object_starts_dtl'} = $osdtl; # (struct dump_table_locator)

	my $erdtl = pdump::dump_table_locator->read($fp, undef);
	die "dump_header::read: emacs_relocs dtl" unless defined $erdtl;
	$h->{'emacs_relocs_dtl'} = $erdtl; # (struct dump_table_locator)

	$h->{'emacs_relocs'} = [];
	for (my $i = 0; $i < $erdtl->nr_entries; $i++) {
		my $at = $erdtl->offset + ($i * pdump::sizeof::emacs_reloc);
		my $er = pdump::emacs_reloc->read($fp, $at);
		die "dump_header::read: emacs_relocs[$i]" unless defined $er;
		$h->{'emacs_relocs'}->[$i] = $er;
	}

	my $ds = dump_off_read($fp, undef);
	die "dump_header::read: discardable_start" unless defined $ds;
	$h->{'discardable_start'} = $ds; # (dump_off)

	my $cs = dump_off_read($fp, undef);
	die "dump_header::read: cold_start" unless defined $ds;
	$h->{'cold_start'} = $cs; # (dump_off)

	my $hl = dump_off_read($fp, undef);
	die "dump_header::read: hash_list" unless defined $ds;
	$h->{'hash_list'} = $hl; # (dump_off)

	return $class->new(%$h);
}

sub explain {
	my $self = shift;
	my $out = '';
	$out .= "Object: dump-header\n";

	$out .= sprintf "Magic: \"\%s\"\n", $self->magic;

	my $fp_str = join '',
		map { sprintf "%02x", ord $_ }
		split '', $self->fingerprint;
	$out .= sprintf "Fingerprint: \"%s\"\n", $fp_str;

	$out .= "Dump-relocs: \n";
	$out .= "\tEarly: "
		. $self->dump_relocs->[pdump::EARLY_RELOCS]->explain
		. "\n"
			if defined $self->dump_relocs->[pdump::EARLY_RELOCS];
	$out .= "\tLate: "
		. $self->dump_relocs->[pdump::LATE_RELOCS]->explain
		. "\n"
			if defined $self->dump_relocs->[pdump::LATE_RELOCS];
	$out .= "\tVery-Late: "
		. $self->dump_relocs->[pdump::VERY_LATE_RELOCS]->explain
		. "\n"
			if defined $self->dump_relocs->[pdump::VERY_LATE_RELOCS];

	$out .= "Object-starts: " . $self->object_starts->explain . "\n";
	$out .= "Emacs-relocs: " . $self->emacs_relocs->explain . "\n";

	$out .= sprintf "Discardable-start: \@\%d\n", $self->discardable_start;
	$out .= sprintf "Cold-start: \@\%d\n", $self->cold_start;
	$out .= sprintf "Hash-list: \@\%d\n", $self->hash_list;

	$out .= "\n";
	return $out;
}

with 'pdump::readable';
with 'pdump::explicable';

########################################################################

package main;

use feature 'say';

die "usage: $0 <emacs-dump-file.pdmp>\n" unless @ARGV == 1;

my ($dumpfile) = @ARGV;
open my $fp, '< :mmap', $dumpfile or die "$dumpfile: $!";
$fp->binmode;

say 'Emacs portable dumper file.';
say '';

my $dump_header = pdump::dump_header->read($fp, 0);
print $dump_header->explain;

close $fp;
