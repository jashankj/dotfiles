#!/usr/bin/env zsh

type_for=5:00
break_for=0:45

exec xwrits \
	typetime=${type_for} breaktime=${break_for} \
	clock breakclock \
	mouse idle \
	top multiply=0:04 maxhands=137
