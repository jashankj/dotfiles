#!/usr/bin/awk -nf

/^# +pos/	{ go = 1; next; };
(! go)		{ print; };
(go)		{ a[$3] += $2; };

END {
	for (x in a)
		sum += a[x];
	print "";
	for (x in a)
		printf("%s: %12d bytes (%9dkB, %6dMB; %5.01f%%)\n", \
			x, a[x], a[x]/1024.0, a[x]/1048576.0, \
			(a[x]*100.0)/sum);
}

