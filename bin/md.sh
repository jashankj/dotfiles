#!/bin/sh

TEMPLATE=""
if [ -e "$PWD/html.pandoc" ]
then
    TEMPLATE="--template=$PWD/html.pandoc"

elif [ -e "$HOME/share/html/html.pandoc" ]
then
    TEMPLATE="--template=$HOME/share/html/html.pandoc"

elif [ -e "/home/jashankj/share/html/html.pandoc" ]
then
    template="--template=/home/jashankj/share/html/html.pandoc"
    PATH=/home/jashankj/bin:$PATH

fi

exec pandoc \
    --standalone --mathjax \
    ${TEMPLATE} \
    "$@"
