#!/usr/bin/env zsh
# NAME: isabelle-2023 --- run Isabelle 2023
# SYNOPSIS: isabelle-2023

exec \
env ISABELLE_IDENTIFIER=Isabelle2023 \
/src/isabelle/Isabelle2023/bin/isabelle "$@"
