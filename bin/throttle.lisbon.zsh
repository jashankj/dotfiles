#!/usr/bin/env zsh
# NAME: throttle.lisbon --- control performance on lisbon

[ $HOST = "lisbon" ] || exit 1

case "$1" in
	min)	freq=1.4g; gov=conservative; gpu=low;  fan=auto ;;
	down)	freq=1.4g; gov=conservative; gpu=low;  fan=auto ;;
	mid)	freq=1.6g; gov=conservative; gpu=low;  fan=7 ;;
	up)	freq=1.7g; gov=conservative; gpu=low;  fan=7 ;;
	max)	freq=1.7g; gov=performance;  gpu=auto; fan=7 ;;

	*)	echo "usage: throttle (min|down|mid|up|max)";
		exit 64 # EX_USAGE
		;;
esac

set -e
echo "throttle: frequency ${freq}, governor ${gov}, gpu ${gpu}, fan ${fan}"

doas /usr/bin/cpupower frequency-set -g ${gov} -u ${freq} >/dev/null
amdgpuperf auto && amdgpuperf ${gpu}
fan ${fan}

