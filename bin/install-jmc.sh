#!/bin/sh
#
# NAME
#	install-jmc -- Install JDK Mission Control from an Adoptium tarball.
# SYNOPSIS
#	install-jmc <org.openjdk.jmc-*.tar.gz>
#

set -e

: ${JMC_PREFIX:=/opt/jmc}
: ${JMC_BIN_OWNER:=root}
: ${JMC_BIN_GROUP:=root}
: ${JMC_TARBALL:=${1?}}

unroll() {
	bsdtar \
		--directory "${JMC_PREFIX?}" \
		--uname "${JMC_BIN_OWNER?}" \
		--gname "${JMC_BIN_GROUP?}" \
		--extract \
		--file "${JMC_TARBALL?}" \
		"$@"
}

install -d -o "${JMC_BIN_OWNER?}" -g "${JMC_BIN_GROUP?}" "${JMC_PREFIX?}"
unroll	--strip-components 1 'JDK Mission Control'
unroll	legal
