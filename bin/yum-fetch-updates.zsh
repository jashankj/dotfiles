#!/usr/bin/env zsh
#
# NAME: yum-fetch-updates --- download those updates we can accept
# SYNOPSIS: yum-fetch-updates
#
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#

yum-list-acceptable-updates \
| xargs -t -r ${YUM-dnf5} update \
	--assumeyes \
	--downloadonly \
	--disablerepo pgdg-common \
	--best --allowerasing \
