#!/usr/bin/env zsh
# NAME: dock.lisbon --- docking operations on `lisbon'
# SYNOPSIS: dock (up L C R | down)

__script__="$(realpath "${(%):-%x}")"
__progname__="$(basename "${__script__}")"
__libdir__="$(dirname "${__script__}")/../lib"
. "${__libdir__}/sh/err"
. "${__libdir__}/sh/usage"

case "$1" in
	up)
		l=${2-4};
		c=${3-3};
		r=${4-2};
		xrandr \
			--output eDP                                      --rotate normal --auto \
			--output DisplayPort-$l --right-of eDP            --rotate left   --auto \
			--output DisplayPort-$c --right-of DisplayPort-$l --rotate left   --auto \
			--output DisplayPort-$r --right-of DisplayPort-$c --rotate normal --auto
		;;

	down)
		seq 0 7 \
		| sed 's@^@DisplayPort-@' \
		| xargs -I % echo --output % --off \
		| xargs -t xrandr
		;;

	*)
		usage
		;;
esac
