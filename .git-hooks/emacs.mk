FILES	+= ${HOME}/.emacs.d/init.el
EMACS	?= emacs

%.el:	%.org ${HOME}/bin/obtangle
	(cd $(dir $<) && ${HOME}/bin/obtangle $(notdir $<))
%.html:	%.org ${HOME}/bin/oxweave
	(cd $(dir $<) && ${HOME}/bin/oxweave html $(notdir $<))

${HOME}/.emacs.d/init.el: ${HOME}/.emacs.d/init.org
${HOME}/.emacs.d/init.html: ${HOME}/.emacs.d/init.org
