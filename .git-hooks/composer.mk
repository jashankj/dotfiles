FILES	+= ${HOME}/.XCompose

${HOME}/.XCompose: ${HOME}/.XCompose.toml ${HOME}/bin/composer
	perl ${HOME}/bin/composer --load-rfc1345 > $@.new && mv $@.new $@
