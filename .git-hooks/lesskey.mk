FILES	+= ${HOME}/.less

${HOME}/.less: ${HOME}/.lesskey
	lesskey -o $@.new -- $< && mv $@.new $@
