stylus		?= ${HOME}/.mozilla/stylus
FILES		+= ${stylus}/local.user.css
stylus_files	:= $(sort $(wildcard ${stylus}/*.ucss))

${stylus}/local.user.css: ${stylus}/mkstylus ${stylus_files}
	(cd ${stylus}; ./mkstylus > $@.new && mv $@.new $@)
