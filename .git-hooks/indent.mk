FILES	+= ${HOME}/.indent.pro
ccat	?= ${HOME}/bin/ccat

${HOME}/.indent.pro: ${HOME}/.indent.pro.in
	${ccat} $< > $@.new && mv $@.new $@
