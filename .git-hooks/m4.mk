FILES	+= ${HOME}/.bazelrc
FILES	+= ${HOME}/.gnupg/gpg-agent.conf
FILES	+= ${HOME}/.i3/config
FILES	+= ${HOME}/.ssh/config

M4	?= m4
%: %.m4
	${M4} \
		-I "$(dir $<)" \
		-D HOSTENV="$(shell hostenv)" \
		-D __HOME__="${HOME}" \
		-D __sysname__="$(shell uname -s)" \
		$< > $@.new \
	&& mv $@.new $@

${HOME}/.bazelrc: \
	${HOME}/.bazelrc.m4 \
	${HOME}/.bazelrc.d/common.m4 \
	$(wildcard ${HOME}/.bazelrc.d/config.*.m4)

${HOME}/.i3/config: \
	${HOME}/.i3/config.m4 \
	${HOME}/.i3/common.m4 \
	$(wildcard ${HOME}/.i3/config.*.m4)

${HOME}/.ssh/config: \
	${HOME}/.ssh/config.m4 \
	${HOME}/.ssh/common.m4 \
	$(wildcard ${HOME}/.ssh/config.*.m4)
