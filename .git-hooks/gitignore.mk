FILES		+= ${HOME}/.gitignore
ignore_files	!= find ${HOME}/.gitignore.d/ -type f | sort | uniq
ccat		?= ${HOME}/bin/ccat

${HOME}/.gitignore: ${ignore_files}
	( cd ${HOME}/.gitignore.d \
	; ${ccat} * | sort | uniq \
	) > $@.new && mv $@.new $@
