order_files	?=
order_files	+= ${HOME}/.zsh/env.d/.ORDER
order_files	+= ${HOME}/.zsh/login.d/.ORDER
order_files	+= ${HOME}/.zsh/logout.d/.ORDER
order_files	+= ${HOME}/.zsh/profile.d/.ORDER
order_files	+= ${HOME}/.zsh/rc.d/.ORDER
order_files	+= ${HOME}/.xsession.d/.ORDER

FILES		+= ${order_files}

define genorder
$1: $$(wildcard $(dir $1)/*)
	(cd $(dir $1) && rcorder *) > $$@.new && mv $$@.new $$@
endef

$(foreach f, ${order_files}, $(eval $(call genorder, ${f})))
