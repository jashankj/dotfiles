include(`../lib/m4/whereami.m4')dnl
divert(-1)dnl

#
# M4 diversions are output in the order they are defined, and content
# between diversions is appended.  This means we can lead with the
# default configuration.
#

divert(`9')
########################################################################
# DEFAULTS #############################################################
Host *

	# Enable session multiplexing except on CSE
ifelse(__hostenv__, `cse', `',
`	ControlMaster auto
	ControlPath ~/.ssh/.control-%r.%h.%p
	ControlPersist 10m')

	# Try and improve performance.
	Compression yes
	ServerAliveInterval 5
	ServerAliveCountMax 6
	TCPKeepAlive yes

	# Don't forward unless permitted.
	ForwardAgent no
	ForwardX11 no

	# Other options
	Port 22
