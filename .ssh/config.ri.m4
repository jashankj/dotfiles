########################################################################
# Rulingia infrastructure

Host emeralfel
	HostName emeralfel.rulingia.com.au
	IdentityFile ~/.ssh/aws-apse2-ri.pem
Host hfgamorr
	HostName hfgamorr.rulingia.com.au
	IdentityFile ~/.ssh/aws-apse2-ri-hf.pem

Host fort
	HostName fort.rulingia.com.au
	IdentityFile ~/.ssh/aws-apse2-ri.pem
Host ruatha
	HostName ruatha.rulingia.com.au
	IdentityFile ~/.ssh/aws-euc1-ri.pem
Host benden
	HostName benden.rulingia.com.au
	IdentityFile ~/.ssh/aws-use1-ri.pem
Host boll
	HostName boll.rulingia.com.au
	IdentityFile ~/.ssh/aws-usw2-ri.pem
