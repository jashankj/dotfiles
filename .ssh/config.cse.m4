########################################################################
# CSE

define(`AllowAncientSSH',
	`HostKeyAlgorithms +ssh-rsa
	PubkeyAcceptedAlgorithms +ssh-rsa')
define(`DisallowHostbasedAuth',
	`HostbasedAuthentication no')
define(`zID', `z5017851')
define(`CSEJump', ifelse(__hostenv__, `cse', `', `ProxyJump cse'))
define(`CSEUHost', `
Host cse-$1
	User $2
	HostName $1.cse.unsw.edu.au
	CSEJump
	AllowAncientSSH'
ifelse(__hostenv__, `cse', `DisallowHostbasedAuth', `')`
Host $1.cse.unsw.edu.au
	User $2
	CSEJump
	AllowAncientSSH'
ifelse(__hostenv__, `cse', `DisallowHostbasedAuth', `'))
define(`CSEHost', `CSEUHost($1, zID)')
define(`ClassAccount',
`Host '$1`
	User '$1)

Host cse
	User zID
	HostName weill.cse.unsw.edu.au
	AllowAncientSSH
	DynamicForward 1780
	LocalForward 16286 wolfram.lic.unsw.edu.au:16286

CSEHost(login)
CSEHost(williams)
CSEHost(wagner)
CSEHost(weaver)
CSEHost(weber)
CSEHost(weill)
CSEHost(wilson)
CSEHost(zappa)
CSEHost(corelli)
CSEHost(mandolin)
CSEUHost(ligeti, root)
CSEUHost(almondbread, andrewb)
CSEUHost(gitlab, gitlab)

# CSE course account aliases
ClassAccount(cs1511)
ClassAccount(cs1511ass)
ClassAccount(cs1511exam)
ClassAccount(cs1511vx)
ClassAccount(cs1521)
ClassAccount(cs1521exam)
ClassAccount(cs1521vx)
ClassAccount(cs1917)
ClassAccount(cs1917ass)
ClassAccount(cs1927)
ClassAccount(cs1927ass)
ClassAccount(cs1927exam)
ClassAccount(cs2041)
ClassAccount(cs2041cgi)
ClassAccount(cs2041ass)
ClassAccount(cs2521)
ClassAccount(cs2521ass)
ClassAccount(cs2521exam)
ClassAccount(cs2521vx)
ClassAccount(cs3231)
ClassAccount(cs3231cgi)
ClassAccount(cs3891)
ClassAccount(cs6443)
ClassAccount(cs6843)
ClassAccount(cs9024)
ClassAccount(cs9044)
ClassAccount(cs9201)
ClassAccount(cs9283)
ClassAccount(cs9315)

Host cs1511
	HostName chopin.cse.unsw.edu.au
Host cs1521
	HostName zappa.cse.unsw.edu.au
Host cs????vx
	HostName pastorius.cse.unsw.edu.au
Host cs???? cs????ass cs????exam cs????cgi cs????vx
	IdentityFile ~/.ssh/csersa
	HostName williams.cse.unsw.edu.au
	CSEJump
	AllowAncientSSH
	ifelse(__hostenv__, `cse', `DisallowHostbasedAuth', `')
