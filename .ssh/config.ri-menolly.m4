########################################################################
# OpenSSH client configuration --- menolly.rulingia.com.au
#
# :See-Also:
#	file:~/.ssh/config.m4
#	file:~/.ssh/common.m4
#	file:~/.ssh/config
divert(-1)dnl

include(`common.m4')
include(`config.ri.m4')
include(`config.cse.m4')
