########################################################################
# OpenSSH client configuration --- elspeth.rulingia.com.au
#
# :See-Also:
#	file:~/.ssh/config.m4
#	file:~/.ssh/common.m4
#	file:~/.ssh/config
divert(-1)dnl

include(`common.m4')
include(`config.ri.m4')
include(`config.cse.m4')
include(`config.ts.m4')
include(`config.pub.m4')
