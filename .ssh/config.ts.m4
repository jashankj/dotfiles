########################################################################
# Trustworthy Systems.

define(`TSJump', ifelse(__hostenv__, `ts', `', `ProxyJump ts'))
define(`TSHost', `
Host ts-$1
	HostName $1.keg.cse.unsw.edu.au
	TSJump
Host $1.keg.cse.unsw.edu.au
	TSJump
')

Host ts
	User jashank
	HostName login.trustworthy.systems
	DynamicForward 6180

dnl TSHost(wedge)  # rip wedge.
dnl TSHost(lemma)
TSHost(duvel)
TSHost(tftp)

Host *.csiro.au
	User	jer025
Host tsgit
	User git
	HostName bitbucket.ts.data61.csiro.au
	Port 7999

