I often get interesting questions
about my host naming scheme.
I'm one of increasingly few people
who both have many systems
and uniquely name most of them.

Unsurprisingly, there *is* a naming scheme:
physical machines tend to be named for
characters from universes I enjoy,
primarily science fiction or fantasy;
and other devices ---
virtual machines, phones, etc. ---
tend to get locations or non-human characters.


Currently-in-use names ---

alyzon:
   A Lenovo ThinkCentre M83 workstation; it runs FreeBSD/amd64.
   New in mid-2015, still going strong in 2022 ---
   but to be supplanted soon by korra.
   Named for the protagonist of
   Isobelle Carmody's novel *Alyzon Whitestarr*.
   (Recycled; initially a VM in which I explored Fedora 19.)

antoinette:
   A Compaq Presario v6107, running FreeBSD/amd64.
   My daily driver from 2006 to 2011;
   defunct around 2014-15, for a 8-year life.
   Named for a character in a short story written by a friend.

   *antoinette* was the oldest system
   I continuously used (barring *beckett*)
   for a very long time.
   It was new in 2006,
   and was a very nice piece of hardware.
   It's got an AMD Turion, a Clawhammer-class TL-50
   on 90 nm, running at 1.6 GHz on two cores for ~30 W TDP,
   and was one of the first multicore boxes I had.
   It has a 1280x800 LCD,
   driven by a GeForce 6150 on an nForce 4.
   I distinctly remember it being
   very, very fussy about FreeBSD support,
   requiring all manner of hacks and deviancies
   to get it to boot at all,
   so it ran Fedora for a long time.
   It spent several years decommissioned, though,
   waiting for me to come up with
   a good solution to making it stable again --
   it wouldn't reliably boot,
   the keyboard was dying,
   and the fan controller,
   thermal management and
   power management all died.

beckett:
   A physical system for a long time;
   then virtualised, then retired.
   Named for a character from the TV series *Castle*.

   *beckett* is the fourth iteration of the system I originally had,
   a truly ancient i486 system that died,
   and was replaced by an Athlon which aged out,
   and was replaced by a nice Core 2 Duo system,
   before it was replaced with a jail.

cassy:
   An IBM System/x 3850 M2 server;
   manufactured in 2008 or so, rescued in 2014,
   still chugging along happily (but getting a bit slow).
   It's a 4U quad-socket Xeon box with a pittance of memory,
   and mostly runs Poudriere (and detects cosmic rays).
   It runs FreeBSD/amd64.
   Named for a key character in
   Isobelle Carmody's *Obernewtyn* series.

elspeth:
   A MacBook Pro 8,2 (early-2011),
   got in late 2011 at a discount (gasp!)
   and slowly run into the ground ever since.
   My daily driver until 2014,
   not helped by a catastrophic hardware failure.
   Nowadays, gathering dust.
   Named for the protagonist of
   Isobelle Carmody's *Obernewtyn* series.

   In late 2011, I was contemplating replacing *inara*.
   Windows (especially locked-down)  was *really* annoying me,
   and I was very tempted to get another X-series ThinkPad.
   Then I happened across a MacBook Pro,
   and promptly bought it.
   I used Mac OS X as a day-to-day OS
   from late 2011 through to early 2014.
   I have my gripes about the OS, of course,
   but Darwin's nice BSD userland was
   wonderfully familiar.

   It was superbly powerful,
   and really appreciated getting another 12 GB of memory.
   Unfortunately, it had an Intel i7-2820QM (a Sandy Bridge)
   on the same heat-pipe as an AMD Radeon 6750M...
   and of course, the GPU died in a spectacular way.
   The motherboard got replaced, and it wasn't ever quite the same.

   *elspeth* had a very varied life ---
   Emacs in X11, side-by-side with InDesign and Illustrator
   is, I'm sure, an unusual pairing.
   I don't really like what happened to Mac OS X in the years since, either,
   and between the hardware flaking and the software being increasingly painful,
   that's pretty much where *elspeth*'s day-to-day life ended.

gates:
   A Dell Optiplex 9020, running FreeBSD/amd64;
   it runs as a firewall.
   Named for a character in the *Castle* TV series.

inara:
   An aged Lenovo ThinkPad X100e, rebadged as a "Mini 10".
   More or less the start of my ThinkPad habit.
   Named for a character in the *Firefly* TV series.

   The NSW Department of Education and Training (as it was at the time)
   got a fabulous quantity of money to give students one-to-one laptops.
   Except they thoroughly screwed up in doing so,
   and the S10e (the "red" one),
   X100e/Mini 10 (the "blue" one),
   X120e (the "green" one), and
   X121e (the "silver" one)
   are all testament to that fact.
   The X100e was a first-generation AMD Fusion system,
   but that was, apparently, too expensive,
   too power-hungry and/or too difficult to source *en masse*,
   so instead we got given a bastardised version of it:
   the *Mini 10*, an X100e with a smaller display
   (and about an inch of bezel!) and an Intel Atom N450.
   (The S10e had an Atom N270;
   with the X120e and X121e,
   they put in processors
   actually up to the task --
   mobile Celerons, as I recall)

   It initially ran Windows 7 Enterprise,
   although "ran" is possibly too strong a word
   for how slow these boxes were.
   I always labelled mine *beelzebub*
   prior to the name reshuffle,
   and *inara* or *cesca* after it
   (because Windows, for no good reason,
   got *very* upset about being given
   the same IP address on two disparate interfaces).
   Oh, and you couldn't configure the system at all,
   so good luck to you if you wanted to do static addressing.

   Within an hour of its release to me,
   *inara* ran Fedora, which is all I had on me at the time.
   Nowadays it runs FreeBSD, of course,
   but its battery is finally down to its last legs,
   and its hardware is getting a bit antique:
   its physical disk failed, and
   its (802.11n draft) wireless interface
   is nowadays just a firmware bug.
   Nonetheless, it's still going,
   diskless and wireless.

jaenelle:
   A Lenovo ThinkPad T440p ---
   my rugged, hefty, robust daily driver
   from early 2014 to late 2020,
   at which point *lisbon* took over that role.
   Runs Arch Linux, FreeBSD, Fedora, Debian.
   Named for a character in
   Anne Bishop's *Black Jewels* series.

kaylee:
   A Compaq Armada M700, running FreeBSD/i386.
   A very nice offsider since 2008
   (probably built in 2001 or thereabouts);
   defunct around 2014, for a 13-odd year service life.
   Named for a character in the *Firefly* TV series.

   For a long time,
   I really liked pre-HP Compaq hardware.
   (Nowadays, I reserve
   that level of hardware piety
   for ThinkPads.)
   I'd had a few Armadas (1580s and 1592s) before this M700,
   and they are reliable old beasts.
   I retired the ancient 1580s and 1592s
   because their screens were getting
   a little small to be usable (800x600 panels),
   they took PATA disks,
   and they used small, slow RAM ---
   I distinctly remember ``81920 KB OK`` on boot.

   *kaylee* is one of four M700s
   rescued from an IS department in 2008.
   They're all very nice systems;
   we had one as our firewall
   for the better part of six or seven years,
   and it was a very reliable beast.
   Eventually, it started exhibiting
   very bizarre failure symptoms
   and was replaced,
   but *kaylee* continued still humming away;
   it made 390 days of continuous uptime once.
   The keyboard is now, sadly, dying, as is its trackpoint,
   but it's still reliable and surprisingly powerful
   for a Pentium III (!) with 128 MB of memory,
   and an ATI Rage 128.

korra:
   Eventually,
   will replace *alyzon* as a home server and NAS, and
   will replace *cassy* as a lot of CPU and memory.
   Home-built --- has a Ryzen 9 3900X (and a spare GeForce 9500GT),
   and a healthy amount of memory and disk.
   It runs FreeBSD/amd64.
   Named for (would you believe) a character
   in the *Legend of Korra* TV series.
   (Recycled; initially a VM in which I spelunking Ubuntu LTS 12.04.)

lisbon:
   A Lenovo ThinkPad T14s Gen1 AMD:
   it's my current daily driver (as of late 2020),
   replacing *jaenelle*.
   It mainly runs Arch Linux.
   Named for a character in the TV series *The Mentalist*.

maruman:
   a GL.iNet AR150 router.
   It runs FreeBSD/mips, is on NIS,
   and mounts ``/home`` from ``alyzon:/home``.
   It's incredibly underpowered!  I'd like to do CI on it,
   but neither Gitlab's nor Jenkins' CI workers will run there.
   Named for a character in
   Isobelle Carmody's *Obernewtyn* series.

menolly:
   A HP Neoware e90 thin client
   (sometimes spelled Neoware CA21).
   Probably manufactured in 2008,
   rescued in 2014, still in good condition.
   Named for a character in
   Anne McCaffery's *Dragonriders of Pern* series.

   I got my paws on *menolly*
   after two HP Neoware thin-clients
   came across my desk at work, having "died".
   Opening them up, it was obvious why:
   *lots* of suspiciously bulging capacitors.
   Replacing them worked a charm,
   and it's a fantastic thin client,
   running X nicely and PXE-booting anything.
   I used it as an Arch Linux testbed for a while,
   but it's been mostly decommissioned
   for the last five-odd years.
   It spent some time as a monitor stand;
   and now hangs out behind my bed.

santiago:
   a Dell Optiplex 9020, running Arch Linux;
   it's got a very fast disk, very fast file system,
   and runs as a CI build slave and media player.
   Named for a character in the *Brooklyn Nine-Nine* TV series.

weatherwax:
   A Sun Fire V240 server,
   which runs Sun Solaris 10 for SPARC
   (but might end up running FreeBSD ---
   Oracle Solaris is a clusterfuck).
   Manufactured in probably 2006-07, rescued in 2017,
   still in serviceable condition ...
   but it has titchy fans running at high speed,
   so is a bit unbearable to leave running.
   Named for a character from
   Terry Pratchett's *Discworld* universe.



Names to be used or recycled soon ---

meredith:
   Named for a character in a short story written by a friend.
   (a Fedora development box; defunct)

surreal:
   Named for a key character in
   Anne Bishop's *Black Jewels* series.
   (A VM in which I experimented with Arch Linux.)

thirteen:
   Named for a character in the *House MD* TV series.


Names that won't be recycled soon ---

dorab, fayew:
   Named for characters in Jeph Jacques' *Questionable Content* webcomic.
   (VMs for Linux-from-Scratch experiments,
   from that time I nearly switched to Linux-from-Scratch.)

bella, anastasia:
   two Windows VMs for those occasions when I need Windows;
   XP SP3 and Server '08r2 (mainly for WDS).
   Named for characters in
   a crappily-written paranormal fantasy series and
   a published, semi-pornographic fan-fiction thereof,
   both of which I've read and regretted.


Phones ---

ravek:
   My first smartphone!  A Huawei U8150 "Ideos".
   Named for a moniker applied to a character in
   Isobelle Carmody's *Obernewtyn* series.

   It was, by far, the most amazing thing I'd ever seen at the time,
   and I wound up running it into the ground;
   its flash is a little bit extremely fried.
   It ran DroniX for a long time;
   I found the original firmware far too nasty to use.

serenity:
   a Galaxy Nexus.
   Named for a not-quite-human character in
   Isobelle Carmody's *Obernewtyn* series.

   After I got involved in the Android universe,
   I realised that the Nexus phones were the place to be,
   so I got the then-current third generation Nexus phone,
   the Samsung Galaxy Nexus.
   Annoyingly, Google don't believe in supporting hardware,
   so it was "unsupported" for much of its 14-month life.
   And then *its* flash started dying,
   causing nasty resets and boot-loops,
   and that was the end of that.

kinraide:
   a Nexus 5.
   Named for a location in
   Isobelle Carmody's *Obernewtyn* series.

   I skipped the Nexus 4,
   because *serenity* was still happy at that point ---
   until it promptly died, so I got a Nexus 5.
   I really liked it; it's very light-weight and fast,
   but I really disliked non-removable, non-replaceable batteries,
   especially having replaced one and,
   uh, accidentally blowing it up ... oops.

x11r5:
   a Nexus 5X.
   Very, very nice for a long time.
   Got dropped a few times,
   and eventually decommissioned
   when its battery started expanding.
   An odd aberration from my usual naming scheme.

gahltha:
   a Nokia 6.1.
   Named for a non-human character in
   Isobelle Carmody's *Obernewtyn* series.
   Google gave up on the Nexus line,
   so I jumped ship onto the logical continuation.
   It's an *excellent* phone.

darga:
   a Nokia 6.2 --- why yes,
   Nokia still make good phones.
   Named for a non-human character in
   Isobelle Carmody's *Obernewtyn* series.


Other hosts ---

emeralfel, gelfort:
   VPSes.
   Named for locations in
   Isobelle Carmody's *Obernewtyn* series.

fort, benden, boll, ruatha:
   VPSes.
   Named for locations in
   Anne McCaffery's *Dragonriders of Pern* series.
