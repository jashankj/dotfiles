************************************************************************
                           jashank's dotfiles
************************************************************************

I've been hacking on these dotfiles
continuously since my extreme youth;
nearly two decades.
In exactly the same way that
throwing a ball and riding a bike
are part of my instincts,
so are my zsh aliases and Emacs behaviours.

My config files are huge, ghastly,
and full of all sorts of weirdities.
So I decided to properly overhaul the whole lot
to make it a bit more comfortable to use:
my home directory is a Git checkout,
and all my dotfiles just sit there.

All happy computers are alike;
each unhappy computer is
an unhappy special snowflake in its own way.
There's lots of assorted patches to support differences
(e.g. screens, keyboards, OSes, etc.) ---
but everything should Just Work
on as many systems as possible.

For a while, I handled this with a set of per-host branches,
cherry-picking a common core onto ``master``,
and it was excruciating to push changes out
(and to pull stuff off branches once they'd mainlined).


Hosts
====================================

alyzon.rulingia.com.au:
   my ThinkCentre M83 workstation.
   It runs FreeBSD amd64.

lisbon.rulingia.com.au:
   my ThinkPad T14s G1,
   multibooting Arch, FreeBSD, and more;
   this presents (unsurprisingly)
   some interesting configuration challenges.

jaenelle.rulingia.com.au:
   my ThinkPad T440p,
   multibooting Arch, Fedora, FreeBSD, and Windows;
   this presents (unsurprisingly)
   some interesting configuration challenges.

cassy.rulingia.com.au:
   my IBM System/x 3850 M2 (oh baby!) server.
   It runs FreeBSD amd64, is on NIS,
   and mounts ``/home`` from ``alyzon:/home``.

weatherwax.rulingia.com.au:
   my Sun Fire V240 server.
   It runs Solaris 10 for SPARC, is on NIS,
   and mounts ``/home`` from ``alyzon:/home``.

inara.rulingia.com.au:
   my ThinkPad Mini10 netbook.
   It's been liberated from Windows 7 Enterprise;
   now it runs FreeBSD amd64, is on NIS,
   and is diskless and wireless.

elspeth.rulingia.com.au:
   my MacBook Pro from early 2011.
   It runs OS X, so there's a butt-ton of Darwin-specific hackery ---
   but it's been mainly unused since c. 2016,
   so I have no idea how much still works.

santiago.rulingia.com.au:
   a Dell Optiplex 9020, running Arch Linux;
   it's got a very fast disk, very fast file system,
   and runs as a CI build slave.

gates.rulingia.com.au:
   a Dell Optiplex 9020, running FreeBSD;
   it runs as a firewall.

maruman.rulingia.com.au:
   a GL.iNet AR150 router.
   It runs FreeBSD/mips, is on NIS,
   and mounts ``/home`` from ``alyzon:/home``.
   It's incredibly underpowered!  I'd like to do CI on it,
   but neither Gitlab's nor Jenkins' CI workers will run there.

gahltha.rulingia.com.au:
   my Nokia 6.1, running Termux on Android.

x11r5.rulingia.com.au:
   my Nexus 5X, running Termux on Android.

kaylee.rulingia.com.au:
   my Compaq Armada M700,
   a beautiful old FreeBSD/i386 box
   with a Pentium III clocking at 650 MHz,
   128 MB of RAM, and a tiny old disk.

antoinette.rulingia.com.au:
   my Compaq Presario v6107, running FreeBSD/amd64,
   my daily driver from 2006 to 2011.

beckett.rulingia.com.au:
   formerly several generations of physical boxes
   (an old i486, then an Athlon, then a Core 2 Duo).

menolly.rulingia.com.au:
   a Neoware CA21 thin client.


fort.rulingia.com.au:
   a "VPS", hosted on Amazon AWS in ap-se-2.
   It runs FreeBSD amd64.


cse.unsw.edu.au:
   the 'baroque' environment at UNSW CSE.
   It runs Debian Linux i386 and x86_64, versions 6, 7, 8, 9, and 10,
   and includes twenty-plus years of accumulated flotsam and jetsam
   from SunOS, Domain/OS, HP-UX, IRIX, and Linux.

wedge.ts.data61.csiro.au:
   my former Optiplex 7060 workstation at Data61.
   It ran Debian testing.

yarn-ev.nexus.csiro.au:
   my Latitude 7410 laptop at Data61.
   It runs Ubuntu "focal" (20.04 LTS).


Dependencies
====================================

Probably incomplete.

* ``src/password-store``, from https://git.zx2c4.com/password-store.git
* ``src/git-repo``, from https://gerrit.googlesource.com/git-repo/
  to manage ``.emacs.d/straight/repos`` for
  `straight.el <https://github.com/raxod502/straight.el>`_,


Setting Up
====================================

* in ``.emacs.d/straight/repos``:
   * create a default manifest in ``.repo/manifest.xml``;
   * symlink ``*.xml`` into ``.repo/manifests``,
     creating it if it doesn't exist;
   * clone Repo into ``.repo/repo``;
   * ``repo sync --nmu``





Credits
====================================

The code here is shamelessly stolen from absolutely _everywhere_;
some of the big sources are/were:

- `Peter Jeremy`_
- the Arch Linux wiki
- `Greg Lehey`_
- the `FreeBSD Developer's Handbook`_
- EmacsWiki
- github:cjohansen/.emacs.d
- github:magnars/.emacs.d
- github:purcell/emacs.d
- github:milkypostman/dotemacs

.. _`Peter Jeremy`: http://www.rulingia.com/~peter/
.. _`Greg Lehey`: http://www.lemis.com/grog/
.. _`FreeBSD Developer's Handbook`: http://www.freebsd.org/doc/en/books/developers-handbook/emacs.html
