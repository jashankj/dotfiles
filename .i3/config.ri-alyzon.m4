########################################################################
# i3 config file (v4) --- alyzon.rulingia.com.au
#
# :See-Also:
#	file:~/.i3/config.m4
#	file:~/.i3/config
divert(-1)dnl

define(`TERMINAL',	`xvt')
define(`OTERMINAL',	`bigxvt')

define(`FONT',		`pango:Roboto 8')
define(`BAR_FONT',	`pango:FontAwesome 10,Roboto 10')

define(`alyzon_L',	`DP1') dnl `DisplayPort-0')
define(`alyzon_R',	`DP2') dnl `DVI-0')

include(`common.m4')

########################################################################
# alyzon-specific setup

MoveToOutput(`F1', alyzon_L)
MoveToOutput(`F2', alyzon_R)

Bind(`$mod+Shift+N',	`exec xset s activate')

PrimaryBar(alyzon_L,   `alyzon_left',  BAR_FONT)
AuxiliaryBar(alyzon_R, `alyzon_right', BAR_FONT)
