########################################################################
# i3 config file (v4) --- inara.rulingia.com.au
#
# :See-Also:
#	file:~/.i3/config.m4
#	file:~/.i3/config
divert(-1)dnl

define(`TERMINAL',	`xvt')
define(`OTERMINAL',	`bigxvt')

define(`FONT',		`pango:Roboto 8')
define(`BAR_FONT',	`pango:FontAwesome 10,Roboto 10')

include(`common.m4')

########################################################################
# inara-specific setup

Bind(`$mod+Shift+N',	`exec xset s activate')
