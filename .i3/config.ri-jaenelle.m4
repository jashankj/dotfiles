########################################################################
# i3 config file (v4) --- jaenelle.rulingia.com.au
#
# :See-Also:
#	file:~/.i3/config.m4
#	file:~/.i3/config
divert(-1)dnl

define(`TERMINAL',	`xvt')
define(`OTERMINAL',	`bigxvt')

define(`FONT',		`pango:Roboto 8')
define(`BAR_FONT',	`pango:FontAwesome 8,Roboto 8')

include(`common.m4')

########################################################################
# jaenelle-specific setup

# Lock the screen.
Bind(`$mod+Shift+N',	`exec xset s activate')

# Start i3bar to display a workspace bar
PrimaryBar(`eDP1', `jaenelle', `BAR_FONT')

# bindcode 248		exec "sh ~jashank/bin/toggle-trackpad.sh"
# bindsym Control+KP_Enter exec "sh ~/bin/mxscroll"

Bind(`XF86MonBrightnessDown',	`exec "xbacklight -dec 10"')
Bind(`XF86MonBrightnessUp',	`exec "xbacklight -inc 10"')

Bind(`Control+XF86MonBrightnessDown',		`exec "xbacklight -set 20"')
Bind(`Control+XF86MonBrightnessUp',		`exec "xbacklight -set 50"')
Bind(`Control+Shift+XF86MonBrightnessDown',	`exec "xbacklight -set 0"')
Bind(`Control+Shift+XF86MonBrightnessUp',	`exec "xbacklight -set 100"')

# Bind(`XF86AudioMute',			`exec "pacmd set-sink-mute 5 1"')
# Bind(`Shift+XF86AudioMute',		`exec "pacmd set-sink-mute 5 0"')
# Bind(`XF86AudioMicMute',		`exec "pacmd set-source-mute 6 1"')
# Bind(`Shift+XF86AudioMicMute',	`exec "pacmd set-source-mute 6 0"')
# Bind(`Shift+XF86AudioRaiseVolume',	`exec "amixer set Master 4096+"')
# Bind(`Shift+XF86AudioLowerVolume',	`exec "amixer set Master 4096-"')
