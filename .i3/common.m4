divert(-1)dnl

#
# Since 2014, I've used the i3 tiling window manager.
#
# In 2018, I did an ad-hoc configuration cleanup, and introduced some
# primitive `m4' glue to do per-host configuration.  This never really
# felt like a satisfying arrangement, but worked well enough.
#
# In 2022, I turned the configuration upside-down: each host has its own
# configuration file that slurps in all the common configuration.
#

define(`Bind', `bindsym	$1	$2')

#
# MoveToOutput([keysym], [output])::
#	Generate bindings to move a workspace (by Mod+Shift+[KEYSYM]) or
#	container (by Mod+Control+Shift+[KEYSYM]) to a specific output.
#
define(`MoveToOutput', `
Bind(`$mod+Shift+$1',		`move workspace to output $2')
Bind(`$mod+Control+Shift+$1',	`move container to output $2')')

define(`__block__', `
$1 {
	$2
}
')

define(`__i3bar_bar_common', `
	status_command	$1
	position	$2
	font		$3
	i3bar_command	$4
	__block__(`colors', `$5')
')

#
# __i3bar_bar(
#     [output], [trayoutput], [statuscmd], [position], [font],
#     [i3bar_command], [colors]
# )::
#	Generate an i3bar invocation with common settings we touch.
#
define(`__i3bar_bar',
	__block__(`bar', `
		output		$1
		tray_output	$2
		__i3bar_bar_common($3, $4, $5, $6, $7)
	'))

define(`StatusCommandFor',
dnl	status_command	sh ~/bin/statusbar.sh $3
dnl	status_command ~/bin/statusbar.alyzon.sh left
dnl	status_command env GEM_HOME=/home/jashank/.gem/jruby jruby ~/bin/statusbar.alyzon.rb
dnl	status_command ~/Software/i3status-rs/target/release/i3status
	`sh ~/bin/statusbar.sh $1'
)

# PrimaryBar([output], [bartype], [font])
define(`PrimaryBar',
	__i3bar_bar($1, $1, StatusCommandFor(`$2'), `top', $3,
	`i3bar --transparency',
	`background	#00000099'))

# AuxiliaryBar([output], [bartype], [font])
define(`AuxiliaryBar',
	__i3bar_bar($1, `none', StatusCommandFor(`$2'), `top', $3,
	`i3bar --transparency',
	`background	#00000099'))

# These bindings trigger as soon as you enter the resize mode
# Pressing left will shrink the window’s width.
# Pressing right will grow the window’s width.
# Pressing up will shrink the window’s height.
# Pressing down will grow the window’s height.
define(`ResizeBindings', `
	Bind(`$1Left',	`resize shrink width  $2 px or $2 ppt')
	Bind(`$1Down',	`resize grow   height $2 px or $2 ppt')
	Bind(`$1Up',	`resize shrink height $2 px or $2 ppt')
	Bind(`$1Right',	`resize grow   width  $2 px or $2 ppt')
')

# switch to workspace
# move focused container to workspace
define(`WorkspaceBindings', `
	Bind(`$mod+$1',		`workspace $1')
	Bind(`$mod+Shift+$1',	`move container to workspace $1')
')

define(`TermsWorkspaceBindings', `
	Bind(`           $1',	`workspace 0.$1')
	Bind(`      $mod+$1',	`workspace 0.$1')
	Bind(`$mod+Shift+$1',	`move container to workspace 0.$1')
')


divert(0)

########################################################################
# Common configuration elements across all hosts.

# First, configure the modifier.
set	$mod	Mod4

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier	$mod

# Font for window titles.
font	FONT

# class                 border  backgr. text    indicator
client.focused		#280100	#340707	#958384	#340707
client.focused_inactive	#280100	#340707	#958384	#340707
client.unfocused	#280100	#280100	#958384	#340707
client.urgent		#280100	#268bd2	#958384	#340707

# client.focused          #958384 #340707 #958384 #586e75
# client.focused_inactive #073642 #657b83 #958384 #586e75
# client.unfocused        #073642 #073642 #340707 #586e75
# client.urgent           #073642 #dc322f #340707 #dc322f
# # client.placeholder      #000000 #0c0c0c #ffffff #000000

default_border		pixel 2
default_floating_border	normal

workspace_auto_back_and_forth	yes

########################################################################
# Configure default-floating and default-sticky windows.

define(`EnsureFloating', `for_window $1	floating enable')dnl
define(`EnsureSticky',   `for_window $1	sticky enable')dnl
define(`AlwaysOnTop',
`EnsureFloating($1)
EnsureSticky($1)')dnl

EnsureFloating(`[title="Event Tester"]')
EnsureFloating(`[title="MPlayer"]')
EnsureFloating(`[title="xclock"]')
EnsureFloating(`[title="xmessage"]')
EnsureFloating(`[title="ImageMagick: -"]')
EnsureFloating(`[title="from the XScreenSaver \d.\d+ distribution"]')
EnsureFloating(`[title="Microsoft Teams Notification"]')

# FFmpeg is pretty awful at creating identifiable windows.
# But when listening to DASH radio streams, this will do.
EnsureFloating(`[title="https?://.*\.mpd"]')

# Jitsi Meet creates an always-on-top window, but it's really bad at
# making it especially identifiable, so I guess this will do...
EnsureFloating(`[title="Jitsi Meet"]')

EnsureFloating(`[class="Xfce4-notifyd"]')
for_window [class="Xfce4-notifyd"] new_window pixel 0

AlwaysOnTop(`[title="Workrave"]')
AlwaysOnTop(`[title="gkrellm"]')
AlwaysOnTop(`[class="knotes"]')

########################################################################
# Bindings to start applications:

# Start terminals.
Bind(`$mod+Return',		`exec TERMINAL')
Bind(`$mod+Shift+Return',	`exec OTERMINAL')
Bind(`$mod+backslash',
	`exec env TMPDIR=esyscmd(`echo $TMPDIR | tr -d "\n"') emacsclient -n -c -q')
Bind(`$mod+Shift+backslash',
	`exec env TMPDIR=esyscmd(`echo $TMPDIR | tr -d "\n"') ewily')

# Kill focused window.
Bind(`$mod+Shift+quotedbl',	`kill')

#
# Rofi provides a nice selector menu.
#
# Rofi offers a set of {window,ssh,run} selectors; and I seem to use the
# `ssh' option often enough that I want to bind it.  given the ordering,
# I could bind the key between the `window' and `run' bindings.  on most
# normal keyboards, that would be ``', but on a Kinesis Advantage, the
# key between Tab and F1 is `='.  no harm in having both!
#
Bind(`$mod+Tab',	`exec rofi -show window')
Bind(`$mod+grave',	`exec rofi -show ssh')
Bind(`$mod+equal',	`exec rofi -show ssh')
Bind(`$mod+F1',		`exec rofi -show run')
Bind(`$mod+F2',		`exec rofi -show drun')

#
# Screen locker configuration is per-host.  Some suggested choices:
#
#	exec xscreensaver-command -lock
#	exec i3lock -f -c 000000
#	exec loginctl lock-session
#	exec xset s activate
#

# Reload configuration.
Bind(`$mod+Shift+J',	`reload')

# Restart in-place.
Bind(`$mod+Shift+P',	`restart')

# Exit i3 (logs you out of your X session)
Bind(`$mod+Shift+greater',	`exec i3-query-exit')

########################################################################
# Bindings to manage the window layout.

# change focus
Bind(`$mod+Left',	`focus left')
Bind(`$mod+Right',	`focus right')
Bind(`$mod+Up',		`focus up')
Bind(`$mod+Down',	`focus down')

# move focused window
Bind(`$mod+Shift+Left',		`move left')
Bind(`$mod+Shift+Down',		`move down')
Bind(`$mod+Shift+Up',		`move up')
Bind(`$mod+Shift+Right',	`move right')

# split container; change container layout
Bind(`$mod+d',		`split h')
Bind(`$mod+k',		`split v')
Bind(`$mod+o',		`layout stacking')
Bind(`$mod+comma',	`layout tabbed')
Bind(`$mod+period',	`layout toggle split')

Bind(`$mod+shift+t',	`border pixel 2')

# fullscreen focussed container
Bind(`$mod+u',		`fullscreen')

# float current window
Bind(`$mod+Shift+space',	`floating toggle')
# focus between tiled/floating
Bind(`$mod+space',		`focus mode_toggle')

# focus the parent container
Bind(`$mod+a',			`focus parent')
# focus the child container
Bind(`$mod+Shift+a',		`focus child')

# Scratchpad
Bind(`$mod+Shift+minus',	`move scratchpad')
Bind(`$mod+minus',		`scratchpad show')

# resize window (you can also use the mouse for that)
__block__(`mode "resize"', `
	ResizeBindings(`',	`10')
	ResizeBindings(`Alt+',	`5')
	ResizeBindings(`Shift+',`50')

	# back to normal: Enter or Escape
	Bind(`Return',	`mode "default"')
	Bind(`Escape',	`mode "default"')
')

Bind(`$mod+p',	`mode "resize"')

########################################################################
# Workspace shuffling

WorkspaceBindings(`1')
WorkspaceBindings(`2')
WorkspaceBindings(`3')
WorkspaceBindings(`4')
WorkspaceBindings(`5')
WorkspaceBindings(`6')
WorkspaceBindings(`7')
WorkspaceBindings(`8')
WorkspaceBindings(`9')

__block__(`mode "terms"', `
	TermsWorkspaceBindings(`1')
	TermsWorkspaceBindings(`2')
	TermsWorkspaceBindings(`3')
	TermsWorkspaceBindings(`4')
	TermsWorkspaceBindings(`5')
	TermsWorkspaceBindings(`6')
	TermsWorkspaceBindings(`7')
	TermsWorkspaceBindings(`8')
	TermsWorkspaceBindings(`9')

	# back to normal: Enter or Escape
	Bind(`Return',	`mode "default"')
	Bind(`Escape',	`mode "default"')
')

Bind(`$mod+0',	`mode "terms"')

########################################################################
# Miscellaneous bindings

Bind(`$mod+Control+1',	`exec ~/bin/screencap')
Bind(`$mod+Control+2',	`exec ~/bin/screencap -w')
Bind(`$mod+Control+3',	`exec ~/bin/screencap -a')
Bind(`$mod+Control+4',	`exec ~/bin/screencap -v')
Bind(`$mod+Control+5',	`exec ~/bin/screencap -vw')

# Bind(`$mod+t',	`exec "~jashank/bin/tock"')

Bind(`XF86Display',	`exec "xrandr | xmessage -file - -timeout 3 -nearmouse &"')

# Audio playback controls, using :man:`playerctl(1)` (speaking MPRIS)
Bind(`XF86AudioPlay',	`exec playerctl play-pause')
