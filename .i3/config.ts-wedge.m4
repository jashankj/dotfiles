########################################################################
# i3 config file (v4) --- wedge
#
# :See-Also:
#	file:~/.i3/config.m4
#	file:~/.i3/config
divert(-1)dnl

define(`TERMINAL',	`xvt')
define(`OTERMINAL',	`bigxvt')

define(`FONT',		`pango:Roboto 8')
define(`BAR_FONT',	`pango:FontAwesome 10,Roboto 10')

include(`common.m4')

########################################################################
# wedge-specific setup

PrimaryBar(`HDMI-1',	`wedge',	BAR_FONT)

# Lock the screen.
Bind(`$mod+Shift+N',	`exec loginctl lock-session')
