########################################################################
# i3 config file (v4) --- lisbon.rulingia.com.au
#
# :See-Also:
#	file:~/.i3/config.m4
#	file:~/.i3/config
divert(-1)dnl

define(`TERMINAL',	`xvt')
define(`OTERMINAL',	`bigxvt')

define(`FONT',		`pango:Roboto 8')
define(`BAR_FONT',	`pango:FontAwesome 8,Roboto 8')

include(`common.m4')

########################################################################
# lisbon-specific setup

define(`lisbon_SPKR', `alsa_output.pci-0000_06_00.6.analog-stereo')dnl
define(`lisbon_MIC',  `alsa_input.pci-0000_06_00.6.analog-stereo')dnl
dnl `alsa_input.pci-0000_06_00.5-platform-acp_pdm_mach.0.stereo-fallback'

# Start i3bar to display a workspace bar
bar {
	output		eDP
	output		DisplayPort-0
	output		DisplayPort-1
	output		DisplayPort-2
	output		DisplayPort-3
	output		DisplayPort-4
	output		DisplayPort-5
	output		DisplayPort-6
	output		DisplayPort-7
	tray_output	eDP
	position	top
	font		BAR_FONT
	status_command	sh ~/bin/statusbar.sh lisbon
}

Bind(`$mod+Shift+N',	`exec xset s activate')

# Backlight control via `acpilight' substitute of `xbacklight':
Bind(`XF86MonBrightnessDown',			`exec "light -U 10"')
Bind(`XF86MonBrightnessUp',			`exec "light -A 10"')

Bind(`Control+XF86MonBrightnessDown',		`exec "light -S 20"')
Bind(`Control+XF86MonBrightnessUp',		`exec "light -S 50"')
Bind(`Control+Shift+XF86MonBrightnessDown',	`exec "light -S 0"')
Bind(`Control+Shift+XF86MonBrightnessUp',	`exec "light -S 100"')

# Volume control.
Bind(`XF86AudioMute',		`exec "pactl set-sink-mute   lisbon_SPKR toggle"')
Bind(`XF86AudioLowerVolume',	`exec "pactl set-sink-volume lisbon_SPKR -3277"')
Bind(`XF86AudioRaiseVolume'	`exec "pactl set-sink-volume lisbon_SPKR +3277"')
Bind(`XF86AudioMicMute',	`exec "pactl set-source-mute lisbon_MIC  toggle"')

# Move containers to displays.
MoveToOutput(`F1',  `DisplayPort-0')
MoveToOutput(`F2',  `DisplayPort-1')
MoveToOutput(`F3',  `DisplayPort-2')
MoveToOutput(`F4',  `DisplayPort-3')
MoveToOutput(`F5',  `DisplayPort-4')
MoveToOutput(`F6',  `DisplayPort-5')
MoveToOutput(`F7',  `DisplayPort-6')
MoveToOutput(`F8',  `DisplayPort-7')
MoveToOutput(`F9',  `DisplayPort-8')
MoveToOutput(`F11', `HDMI-A-0')
MoveToOutput(`F12', `eDP')
