########################################################################
# i3 config file (v4) --- menolly.rulingia.com.au
#
# :See-Also:
#	file:~/.i3/config.m4
#	file:~/.i3/config
divert(-1)dnl

define(`TERMINAL',	`xvt')
define(`OTERMINAL',	`bigxvt')

define(`FONT',		`pango:Roboto 8')
define(`BAR_FONT',	`pango:FontAwesome 10,Roboto 10')

include(`common.m4')

########################################################################
# menolly-specific setup

PrimaryBar(`VGA-1',	`menolly',	BAR_FONT)

Bind(`$mod+Shift+N',	`exec xset s activate')
