########################################################################
# i3 config file (v4) --- yarn
#
# :See-Also:
#	file:~/.i3/config.m4
#	file:~/.i3/config
divert(-1)dnl

define(`TERMINAL',	`xvt')
define(`OTERMINAL',	`bigxvt')

define(`FONT',		`pango:Roboto 8')
define(`BAR_FONT',	`pango:FontAwesome 8,Roboto 8')

include(`common.m4')

########################################################################
# yarn-specific setup

PrimaryBar(`eDP-1',	`yarn',	BAR_FONT)

# Backlight controls run via `xfce4-power-manager'.
# Volume controls could be picked up, I guess.
