rstdoc_dir	?= ${HOME}/lib/doc
rstdoc_config	?= ${rstdoc_dir}/docutils.conf

rstflags.config	 = --config=${rstdoc_config}

rst2html	?= rst2html5
%.html:	%.rst ${rstdoc_config} ${rstdoc_dir}/docutils.css
	${rst2html} --record-dependencies=$<.d \
		${rstflags} \
		${rstflags.config} \
		$< $@

# rst2latex	?= rst2latex
rst2latex	?= rst2xetex
%.tex:	%.rst ${rstdoc_config}
	${rst2latex} --record-dependencies=$<.d \
		${rstflags} \
		${rstflags.config} \
		$< $@

latexmk		?= latexmk
latexmkflags	?= -synctex=1 -quiet
%.pdf.pdf:	%.tex %.rst ${rstdoc_config}
	${latexmk} -outdir=$(dir $@) -pdf    ${latexmkflags} $<
	cp $*.pdf $@
%.xe.pdf:	%.tex %.rst ${rstdoc_config}
	${latexmk} -outdir=$(dir $@) -pdfxe  ${latexmkflags} $<
	cp $*.pdf $@
%.lua.pdf:	%.tex %.rst ${rstdoc_config}
	${latexmk} -outdir=$(dir $@) -pdflua ${latexmkflags} $<
	cp $*.pdf $@
