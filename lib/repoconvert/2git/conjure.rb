#!/usr/bin/env ruby
# typed: strict
require 'sorbet-runtime'; extend T::Sig
$:.unshift File.join Dir.home, %w{ lib repoconvert 2git }
require 'gfi'; extend Gfi::Syntax
require 'date'

uid! :jashank,  'Jashank Jeremy', 'jashank@alyzon.rulingia.com.au'

(0..6).each do |i|
  Dir["r#{i}/*", "r#{i}/.*"].sort.each do |obj|
    next if File.directory? obj
    blob_of obj, obj
  end
end

files = %w[
  .dir-locals.el
  .gitattributes
  .gitignore
  conf.py
  GNUmakefile
  index.rst
]

xfiles = %w[
  BUILD
  cron-day
  fmt-mailstat
  mk-header
  mk-mailstat
  mk-month
  mk-weather-nzch
  mk-weather-yssy
  mk-year
  propset
]

tip = T.let ref!('refs/heads/tip'), Gfi::Ref
reset tip

(0..6).each do |r|
  commit tip do |c|
    c.author = c.committer = stamp! :jashank, DateTime.now
    c.message = File.read("r#{r}.descr")
    c.from = pred tip unless r == 0
    files.each  {|f| c.modify f, blob?("r#{r}/#{f}") }
    xfiles.each {|f| c.modify f, blob?("r#{r}/#{f}"), mode = Gfi::Mode::Exe }
  end
end

finish!
__END__
