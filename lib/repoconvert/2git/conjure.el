(defvar conjure-current 0)

(defvar conjure-munge-state nil)
(defun conjure-munge-byref (term ref Reference)
  (pcase term
    ((rx bos eol) (kill-line))

    ((rx bos "commit ")
     (setq conjure-munge-state 'commit)
     (setq conjure-current (1+ conjure-current))
     (kill-line)
     (insert (format "end\n\ncommit %s do |c| # %s/%04d" Reference ref conjure-current))
     (beginning-of-line 2))

    ((rx (or
	  (: bos "mark :" (1+ digit) eol)
	  (: bos "original-oid " (= 40 hex-digit) eol)
	  (: bos "committer ")))
     (kill-line 1))

    ((rx bos "author " (*? nonl) " <" (group (1+ (not (any ?@ ?>)))) (opt "@" (1+ (not ?>))) ">"
	 " " (group (>= 9 digit) " " (| ?+ ?-) (= 4 digit)))
     (let* ((uid (match-string-no-properties 1 term))
	    (dt  (match-string-no-properties 2 term)))
       (kill-line)
       (insert (format "  c.author = c.committer = stamp!(:%s, \"%s\")" uid dt))
       (beginning-of-line 2)))

    ((and (rx bos "data " (group (1+ digit)) eol)
	  (guard (eq conjure-munge-state 'commit)))
     (let* ((len  (string-to-number (match-string-no-properties 1 term))))
       (kill-line 1)
       (insert "  c.message = <<__EOF__\n")
       (forward-char len)
       (insert "__EOF__\n")))

    ((rx bos "from :" (1+ digit) eol)
     (kill-line)
     (insert (format "  c.from = pred %s" Reference))
     (beginning-of-line 2))

    ((rx bos "M " (| "100644" "100755") " :" (1+ digit) " " (group (1+ nonl)) eol)
     (let* ((file  (match-string-no-properties 1 term)))
       (kill-line)
       (insert (format "  c.modify %-17s blob?('%s@%s.%04d')"
		       (concat "'" file "',")
		       file ref
		       conjure-current))
       (beginning-of-line 2)))

    ((rx bos "D " (group (1+ nonl)) eol)
     (let* ((file  (match-string-no-properties 1 term)))
       (kill-line)
       (insert (format "  c.delete '%s'" file))
       (beginning-of-line 2)))

    ((rx bos "blob" eol)
     (setq conjure-munge-state 'blob)
     (kill-line 1))

    ((and (rx bos "data " (group (1+ digit)) eol)
	  (guard (eq conjure-munge-state 'blob)))
     (let* ((len  (string-to-number (match-string-no-properties 1 term))))
       (kill-line 1)
       (push-mark)
       (forward-char len)
       (delete-region (mark) (point))))

))


(defun conjure-munge-OpenSUSE (term)
  (conjure-munge-byref term "opensuse" "OpenSUSE"))
(defun conjure-munge-Debian (term)
  (conjure-munge-byref term "debian" "Debian"))
(defun conjure-munge-pkgsrc (term)
  (conjure-munge-byref term "pkgsrc" "Pkgsrc"))
(defun conjure-munge-NetBSD (term)
  (conjure-munge-byref term "netbsd" "NetBSD"))
(defun conjure-munge-FreeBSD (term)
  (conjure-munge-byref term "freebsd" "FreeBSD"))
(defun conjure-munge-CSRG (term)
  (conjure-munge-byref term "csrg" "CSRG"))

(defun conjure-munge ()
  (when (>= (point) (point-max))
    (signal 'end-of-buffer "End of buffer."))
  (let* ((window  80)
	 (range   (min (+ (point) window) (point-max)))
	 (here    (buffer-substring (point) range)))
    (conjure-munge-CSRG here)))

;; (dotimes-with-progress-reporter (n 1000) "(conjure-munge)..." (conjure-munge))
