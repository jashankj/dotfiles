# -*- ruby -*-
# typed: strict
require 'sorbet-runtime'; extend T::Sig
require 'date'

########################################################################

##
# A module for generating +git-fast-import+ inputs.
#
module Gfi

  ##
  # A thin wrapper around Git's user syntax.
  #
  #   user = Gfi::User.new name: 'User', email: 'user@example.net'
  #   user.to_s  #  ==> "User <user@example.net>"
  #
  class User < T::Struct
    extend T::Sig
    prop :name, T.nilable(String)
    prop :email, String

    sig { returns String }
    def to_s
      [@name, '<' + @email + '>'].reject(&:nil?).join(' ')
    end
  end

  ##
  # A thin wrapper around Git's user/date stamps for commits.
  #
  #   user   = Gfi::User.new name: 'User', email: 'user@example.net'
  #   time0  = DateTime.new 2001, 9, 17, 0, 0, 0, '+0'
  #   stamp0 = Gfi::Stamp.new user: user, time: time0
  #   stamp0.to_s  #  ==> "User <user@example.net> 1000684800 +0000"
  #
  class Stamp < T::Struct
    extend T::Sig
    prop :user, User
    prop :time, DateTime

    sig { returns String }
    def to_s
      format '%s %s', @user, @time.strftime('%s %z')
    end
  end

  ######################################################################

  ##
  # A representation of Git's references.
  #
  #   trunk = Gfi::Ref ref: 'refs/heads/trunk'
  #
  class Ref < T::Struct
    extend T::Sig
    prop :ref, String

    sig { returns String }
    def to_s
      @ref
    end
  end

  ##
  # A representation of +git-fast-import+'s mark syntax.
  #
  #   now = Gfi::Mark.new mark: 1
  #   now.to_s  #  ==> ":1"
  #
  class Mark < T::Struct
    extend T::Sig
    prop :mark, Integer

    sig { returns String }
    def to_s
      format ':%d', @mark
    end
  end

  ##
  # A representation of Git object-identifiers.
  #
  #   that = Gfi::Oid.new oid: '9dda461e5ada3d7f808050b56522899c50602c1e'
  #   dead = Gfi::Oid.dead
  #   dead.to_s  #  ==> "0000000000000000000000000000000000000000"
  #
  class Oid < T::Struct
    extend T::Sig
    prop :oid, String

    sig { returns String }
    def to_s
      format '%040s', @oid
    end

    sig { returns Gfi::Oid }
    def self.dead
      Oid.new oid: ('0' * 40)
    end
  end

  ##
  # A representation of +git-fast-import+'s inline-data objects.
  #
  #   datum = Gfi::Data.new data: "oh no.\n"
  #   datum.to_s  #  ==> "data 7\noh no.\n"
  #
  class Data < T::Struct
    extend T::Sig
    prop :data, String

    sig { returns String }
    def to_s
      format("data %d\n", @data.length) + @data
    end
  end

  ######################################################################

  ##
  # A representation of a data references in a +git-fast-import+ stream.
  #
  module Dataref
    class Mark < ::T::Struct
      extend ::T::Sig
      prop :mark, Gfi::Mark

      sig { returns String }
      def to_s
        @mark.to_s
      end
    end

    class Oid < ::T::Struct
      extend ::T::Sig
      prop :oid, Gfi::Oid

      sig { returns String }
      def to_s
        @oid.to_s
      end
    end

    class Inline < ::T::Struct
      extend ::T::Sig
      prop :data, Gfi::Data

      sig { returns String }
      def to_s
        "inline"
      end
    end

    T = ::T.type_alias { ::T.any Dataref::Mark, Dataref::Oid, Dataref::Inline }
  end

  ######################################################################

  ##
  # A representation of file modes in +git-fast-import+ streams.
  #
  class Mode < T::Enum
    extend T::Sig

    enums do
      File    = new
      Exe     = new
      Link    = new
      Gitlink = new
      Dir     = new
    end

    sig { returns Integer }
    def to_int
      case self
      when File    then 0o100644
      when Exe     then 0o100755
      when Link    then 0o120000
      when Gitlink then 0o160000
      when Dir     then 0o040000
      end
    end
  end

  ##
  # A representation of file actions in +git-fast-import+ streams.
  #
  module Action
    class FileModify < ::T::Struct
      extend ::T::Sig
      prop :mode,    Mode
      prop :dataref, Dataref::T
      prop :path,    String

      sig { returns String }
      def to_s
        if @dataref.is_a? Dataref::Inline
          format("M %06o inline %s\n", @mode.to_int, @path) +
            @dataref.data.to_s
        else
          format "M %06o %s %s", @mode.to_int, @dataref, @path
        end
      end
    end

    class FileDelete < ::T::Struct
      extend ::T::Sig
      prop :path, String

      sig { returns String }
      def to_s
        format "D %s", @path
      end
    end

    class FileCopy < ::T::Struct
      extend ::T::Sig
      prop :srcpath, String
      prop :dstpath, String

      sig { returns String }
      def to_s
        format "C %s %s", @srcpath, @dstpath
      end
    end

    class FileRename < ::T::Struct
      extend ::T::Sig
      prop :srcpath, String
      prop :dstpath, String

      sig { returns String }
      def to_s
        format "R %s %s", @srcpath, @dstpath
      end
    end

    class FileDeleteAll < ::T::Struct
      extend ::T::Sig

      sig { returns String }
      def to_s
        "deleteall"
      end
    end

    class NoteModify < ::T::Struct
      extend ::T::Sig
      prop :dataref, Dataref::T
      prop :commit,  Ref

      sig { returns String }
      def to_s
        if @dataref.is_a? Dataref::Inline
          format("N inline %s\n", @commit) +
            @dataref.data.to_s
        else
          format "N %s %s", @dataref, @commit
        end
      end
    end

    T = ::T.type_alias {
      ::T.any(
        FileModify,
        FileDelete,
        FileCopy,
        FileRename,
        FileDeleteAll,
        NoteModify
      )
    }
  end

  ######################################################################

  ##
  # Objects that can be substituted for commits in +git-fast-import+.
  #
  Commitish = T.type_alias { T.any Ref, Mark, Oid }

  ##
  # Possible commands in a +git-fast-import+ stream.
  #
  module Command
    class Commit < ::T::Struct
      extend ::T::Sig
      prop :ref,       Ref
      prop :mark,      ::T.nilable(Mark),      default: nil
      prop :orig_oid,  ::T.nilable(Oid),       default: nil
      prop :author,    ::T.nilable(Stamp),     default: nil
      prop :committer, Stamp
      prop :encoding,  ::T.nilable(String),    default: nil
      prop :data,      Data
      prop :from,      ::T.nilable(Commitish), default: nil
      prop :merge,     ::T::Array[Commitish],  default: []
      prop :actions,   ::T::Array[Action::T],  default: []

      sig { returns String }
      def to_s
        out = []
        out.push format 'commit %s',       @ref
        out.push format 'mark %s',         @mark      if @mark
        out.push format 'original-oid %s', @orig_oid  if @orig_oid
        out.push format 'author %s',       @author    if @author
        out.push format 'committer %s',    @committer
        out.push format 'encoding %s',     @encoding  if @encoding
        out.push                           @data
        out.push format 'from %s',         @from      if @from
        out += @merge.map { |x| format 'merge %s', x }
        out += @actions.map(&:to_s)
        out.join("\n") + "\n"
      end
    end

    class Tag < ::T::Struct
      extend ::T::Sig
      prop :name, String
      prop :mark, ::T.nilable(Mark)
      prop :from, ::T.nilable(Ref)
      prop :orig_oid, ::T.nilable(Oid)
      prop :tagger, Stamp
      prop :data, Data

      sig { returns String }
      def to_s
        out = []
        out.push format 'tag %s',          @name
        out.push format 'mark %s',         @mark     if @mark
        out.push format 'from %s',         @from     if @from
        out.push format 'original-oid %s', @orig_oid if @orig_oid
        out.push format 'tagger %s',       @tagger
        out.join("\n") + "\n" + @data.to_s + "\n"
      end
    end

    class Reset < ::T::Struct
      extend ::T::Sig
      prop :name, Ref
      prop :from, ::T.nilable(Ref)

      sig { returns String }
      def to_s
        out = []
        out.push format 'reset %s', @name
        out.push format 'from %s',  @from if @from
        out.join("\n") + "\n"
      end
    end

    class Blob < ::T::Struct
      extend ::T::Sig
      prop :mark,     ::T.nilable(Mark)
      prop :orig_oid, ::T.nilable(Oid)
      prop :data,     Data

      sig { returns String }
      def to_s
        out = ['blob']
        out.push format 'mark %s',         @mark if @mark
        out.push format 'original-oid %s', @orig_oid if @orig_oid
        out.join("\n") + "\n" + @data.to_s + "\n"
      end
    end

    class Alias < ::T::Struct
      extend ::T::Sig
      prop :from, Mark
      prop :to,   Ref

      sig { returns String }
      def to_s
        out = ['alias']
        out.push format 'mark %s', @from
        out.push format 'to %s',   @to
        out.join("\n") + "\n"
      end
    end

    class Checkpoint < ::T::Struct
      extend ::T::Sig

      sig { returns String }
      def to_s
        "checkpoint\n"
      end
    end

    class Progress < ::T::Struct
      extend ::T::Sig
      prop :message, String

      sig { returns String }
      def to_s
        format "progress %s\n", @message
      end
    end

    class GetMark < ::T::Struct
      extend ::T::Sig
      prop :mark, Mark

      sig { returns String }
      def to_s
        format "get-mark %s\n", @mark
      end
    end

    class CatBlob < ::T::Struct
      extend ::T::Sig
      prop :blob, Dataref

      sig { returns String }
      def to_s
        format "cat-blob %s\n", @blob
      end
    end

    class Ls < ::T::Struct
      extend ::T::Sig

      sig { returns String }
      def to_s
        "ls\n"
      end
    end

    class Feature < ::T::Struct
      extend ::T::Sig
      prop :feature, String

      sig { returns String }
      def to_s
        format "feature %s\n", @feature
      end
    end

    class Option < ::T::Struct
      extend ::T::Sig
      prop :option, String

      sig { returns String }
      def to_s
        format "option %s\n", @option
      end
    end

    class Done < ::T::Struct
      extend ::T::Sig

      sig { returns String }
      def to_s
        "done\n"
      end
    end

    T = ::T.type_alias {
      ::T.any(
        Commit,
        Tag,
        Reset,
        Blob,
        Alias,
        Checkpoint,
        Progress,
        GetMark,
        CatBlob,
        Ls,
        Feature,
        Option,
        Done,
      )
    }
  end

  ######################################################################

  ##
  # A DSL for expressing a +git-fast-import+ stream.
  #
  #   extend Gfi::Syntax
  #   # ... commands ...
  #   finish!
  #
  module Syntax
    extend T::Sig

    $stream  = T.let([], T::Array[Command::T])
    $blobmap = T.let({}, T::Hash[String, Mark])
    $refmap  = T.let({}, T::Hash[String, Ref])
    $markmap = T.let({}, T::Hash[Ref, T::Array[Mark]])
    $uidmap  = T.let({}, T::Hash[Symbol, User])
    $mark    = T.let(0,  Integer)

    ##
    # Create or retrieve a particular reference.
    #
    sig { params(ref: String).returns(Ref) }
    public
    def ref! ref
      if ref.is_a? String
        $refmap[ref] ||= Ref.new(ref: ref)
      end
    end

    ##
    # Ensure +ref+ is a Ref.
    #
    sig { params(ref: T.any(Ref, String)).returns(Ref) }
    public
    def as_ref ref
      ref = ref! ref unless ref.is_a? Ref
      ref
    end

    ##
    # Create a Data.
    #
    sig { params(data: String).returns(Data) }
    public
    def data! data
      Data.new(data: data)
    end

    ##
    # Ensure +data+ is a Data.
    sig { params(data: T.any(Data, String)).returns(Data) }
    public
    def as_data data
      data = data! data unless data.is_a? Data
      data
    end

    ##
    # Create the next Mark.
    #
    sig { returns Mark }
    public
    def mark!
      Mark.new(mark: $mark += 1)
    end

    ##
    # Get a particular blob's mark.
    #
    sig { params(label: String).returns(Mark) }
    public
    def blob? label
      $blobmap[label]
    end

    ##
    # Create a Gfi::Stamp from a user and a timestamp.
    #
    sig { params(
            uid: T.any(Symbol, User),
            dt: T.any(String, DateTime)
          ).returns(Stamp) }
    public
    def stamp! uid, dt
      user = case uid
             when Symbol then T.let($uidmap[uid], User)
             when User   then T.let(uid, User)
             end
      time = case dt
             when String   then DateTime.strptime(dt, "%s %z")
             when DateTime then dt
             end
      Stamp.new(user: user, time: time)
    end

    ##
    # Create or retrieve a Gfi::User.
    #
    sig { params(
            uid:   T.any(String, Symbol),
            name:  T.nilable(String),
            email: T.nilable(String)
          ).returns(User) }
    public
    def uid! uid, name = nil, email = nil
      uid = uid.to_sym
      $uidmap[uid] ||= User.new(name: T.must(name), email: T.must(email))
    end

    ##
    # Inject a +reset+ command into the stream.
    #
    sig { params(
            ref: T.any(Ref, String)
          ).returns(Command::Reset) }
    public
    def reset ref
      the = Command::Reset.new name: as_ref(ref)
      $stream.push the; the
    end

    ##
    # A helper for Gfi::Syntax::commit, providing the object passed to
    # the inner block with methods to set up specific file operations.
    #
    class CommitBuilder < T::Struct
      extend T::Sig

      prop :ref,       T.nilable(T.any(Ref, String)),   default: nil
      prop :mark,      T.nilable(T.any(Mark, Integer)), default: nil
      prop :orig_oid,  T.nilable(T.any(String, Oid)),   default: nil
      prop :author,    T.nilable(Stamp),                default: nil
      prop :committer, T.nilable(Stamp)
      prop :encoding,  T.nilable(String),               default: nil
      prop :message,   T.nilable(String)
      prop :from,      T.nilable(T.any(Commitish, String)), default: nil
      prop :merge,     T::Array[T.any(Commitish, String)], default: []
      prop :actions,   T::Array[Action::T],  default: []

      ##
      # Update a specific file.
      #
      #   commit(...) do |c|
      #     c.modify 'is-good', Gfi::Data.new(data: "oh no.\n")
      #     # ...
      #   end
      #
      sig { params(
              path:    String,
              blobref: T.any(Dataref::T, Mark, Oid, Data),
              mode:    Mode
            ).returns(Action::FileModify) }
      def modify path, blobref, mode = Mode::File
        dataref =
          case blobref
          when Dataref::Mark   then blobref
          when Mark            then Dataref::Mark.new mark: blobref
          when Dataref::Oid    then blobref
          when Oid             then Dataref::Oid.new oid: blobref
          when Dataref::Inline then blobref
          when Data            then Dataref::Inline.new data: blobref
          end
        the = Action::FileModify.new(mode: mode, dataref: dataref, path: path)
        @actions.push the; the
      end

      ##
      # Delete a file.
      #
      #   commit(...) do |c|
      #     c.delete 'let/it/go'
      #     # ...
      #   end
      #
      sig { params(path: String).returns(Action::FileDelete) }
      def delete path
        the = Action::FileDelete.new path: path
        @actions.push the; the
      end

      ##
      # Copy a file within a commit.
      #
      #   commit(...) do |c|
      #     c.copy 'path/to/this', 'path/to/that'
      #     # ...
      #   end
      #
      sig { params(src: String, dst: String).returns(Action::FileCopy) }
      def copy src, dst
        the = Action::FileCopy.new srcpath: src, dstpath: dst
        @actions.push the; the
      end

      ##
      # Rename a file within a commit.
      #
      #   commit(...) do |c|
      #     c.rename 'path/to/this', 'path/to/that'
      #     # ...
      #   end
      #
      sig { params(src: String, dst: String).returns(Action::FileRename) }
      def rename src, dst
        the = Action::FileRename.new srcpath: src, dstpath: dst
        @actions.push the; the
      end

      ##
      # Delete everything!
      #
      #   commit(...) do |c|
      #     c.delete_all!
      #     # ...
      #   end
      #
      sig { returns Action::FileDeleteAll }
      def delete_all!
        the = Action::FileDeleteAll.new
        @actions.push the; the
      end
    end

    ##
    # Create a commit on a ref.
    #
    #   user  = Gfi::User.new name: 'User', email: 'user$example.net'
    #   time0 = DateTime.new 2001, 9, 17, 0, 0, 0, '+0'
    #   HEAD  = Gfi::Ref ref: 'refs/HEAD'
    #   commit HEAD do |c|
    #     c.author = c.committer = stamp! user, time0
    #     c.message = "Initial commit."
    #     # ...
    #   end
    #
    sig { params(
            ref:  T.any(Ref, String),
            body: T.proc.params(c: CommitBuilder).void
          ).returns(Command::Commit) }
    public
    def commit(ref, &body)
      c = CommitBuilder.new
      c.ref = as_ref ref
      body.call(c)
      the = _commitbuilder_to_ccommit c
      $markmap[the.ref] ||= []
      $markmap[the.ref].push the.mark
      $stream.push the; the
    end

    ##
    # Get the previous mark on a reference.
    #
    #   old_head = pred HEAD
    #
    sig { params(ref: T.any(Ref, String)).returns(Mark) }
    public
    def pred ref
      ref   = as_ref ref
      marks = $markmap[ref] || []
      mark  = T.let(marks.last, T.nilable(Mark))
      T.must(mark)
    end

    ##
    # Fix up a CommitBuilder object.
    #
    sig { params(the: CommitBuilder).returns(Command::Commit) }
    private
    def _commitbuilder_to_ccommit the
      orig_oid = ::Gfi::Syntax._cbcc_orig_oid the
      from     = ::Gfi::Syntax._cbcc_from     the
      merge    = ::Gfi::Syntax._cbcc_merge    the
      Command::Commit.new(
        ref:       as_ref(T.must(the.ref)),
        mark:      mark!,
        orig_oid:  orig_oid,
        author:    T.let(the.author,    T.nilable(Stamp)),
        committer: T.let(T.must(the.committer),   Stamp),
        encoding:  the.encoding,
        from:      from,
        merge:     merge,
        data:      as_data(T.must(the.message).chomp),
        actions:   the.actions
      )
    end

    sig { params(the: CommitBuilder).returns(T.nilable(Oid)) }
    private
    def self._cbcc_orig_oid the
      case the.orig_oid
      when nil    then nil
      when Oid    then T.cast(the.orig_oid, Oid)
      when String then Oid.new oid: T.cast(T.must(the.orig_oid), String)
      end
    end

    sig { params(the: CommitBuilder).returns(T.nilable(Commitish)) }
    private
    def self._cbcc_from the
      case the.from
      when nil    then nil
      when Ref    then T.cast the.from, Ref
      when String then as_ref T.cast(the.from, String)
      when Mark   then T.cast the.from, Mark
      end
    end

    sig { params(the: CommitBuilder).returns(T::Array[Ref]) }
    private
    def self._cbcc_merge the
      the.merge.map do |x|
        case x
        when String then as_ref(x)
        when Ref then x
        when Oid then x
        when Mark then x
        end
      end
    end

    ##
    # Create a tag.
    #
    sig { params(
            name: String,
            from: T.any(Ref, String),
            tagger: Stamp,
            data: T.any(Data, String)
          ).returns(Command::Tag) }
    public
    def tag name, from, tagger, data
      the = Command::Tag.new(
        name: name,
        from: as_ref(from),
        tagger: tagger,
        data: as_data(data)
      )
      $markmap[as_ref("refs/tags/" + the.name)] ||= []
      $markmap[as_ref("refs/tags/" + the.name)].push pred T.must the.from
      $stream.push the; the
    end

    ##
    # Create a blob with a symbolic +label+ and bearing +data+.
    #
    sig { params(label: String, data: String).returns(Command::Blob) }
    public
    def blob label, data
      mark = mark!
      $blobmap[label] ||= mark
      the = Command::Blob.new mark: mark, data: as_data(data)
      $stream.push the; the
    end

    ##
    # Get a blob from a particular file.
    #
    sig { params(label: String, file: String).returns(Command::Blob) }
    public
    def blob_of label, file
      blob label, File.read(file)
    end

    ##
    # Finish the stream.
    #
    public
    def finish!
      stream  = $stream
      blobmap = $blobmap
      refmap  = $refmap
      markmap = $markmap
      uidmap  = $uidmap
      mark    = $mark

      _reinit_world
      _inner_finish stream, blobmap, refmap, markmap, uidmap, mark
    end

    private
    def _reinit_world
      $stream  = []
      $blobmap = {}
      $refmap  = {}
      $markmap = {}
      $uidmap  = {}
      $mark    = 0
    end

    private
    def _inner_finish stream, blobmap, refmap, markmap, uidmap, mark
      stream.each { |x| puts x }

      {
        blobmap: blobmap,
        refmap:  refmap,
        markmap: markmap,
        uidmap:  uidmap,
        mark:    mark
      }
    end
  end
end
