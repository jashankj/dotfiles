;;; jashankj.util.goops --- helpers for reading Scheme

;;; Commentary:
#|--

Helpers for reading Scheme.

--|#
;;; Code:

(define-module (jashankj util goops)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	pp-goops
	slot:update!
	slot:lset:adjoin!
	make-instance-from-class-ctor-key
  )

  #:use-module (srfi srfi-1)    ;; lists
  #:use-module (srfi srfi-26)   ;; cut

  #:use-module ((guile) #:select (symbol->keyword))
  #:use-module (oop goops)
  #:use-module (ice-9 format)
  #:use-module (ice-9 pretty-print)

  #:use-module (jashankj util dict)
  #:use-module (jashankj util procedure)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (pp-goops obj)
  "Pretty-print OBJ, which may be any `(oop goops)' object."

  (define (pp-slot obj slot)
    (let ([slot-name (slot-definition-name slot)])
      (if (slot-bound? obj slot-name)
          `(,(symbol->keyword slot-name) ,(slot-ref obj slot-name))
          `(,(symbol->keyword slot-name) ,"<unspecified>"))))

  (define (pp-goops-1 obj klass)
    `(make ,(class-name klass)
       ,@(apply append
                (map (lambda (slot) (pp-slot obj slot))
                     (class-direct-slots klass)))
       --and-include--
       ,@(map (lambda (super) (pp-goops-1 obj super))
              (class-direct-supers klass))))

  (pretty-print (pp-goops-1 obj (class-of obj))))

(define (slot:update! obj slot fn)
  (slot-set! obj slot (fn (slot-ref obj slot))))

(define (slot:lset:adjoin! obj slot eq-f upd-f)
  (slot:update! obj slot (lambda (old) (lset-adjoin eq-f old (upd-f old)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. scm:procedure: (make-instance-from-class-ctor-key klass ctor-kw ctor-alist)
   :param klass      <class>:
   :param ctor-kw    <keyword>:
   :param ctor-alist (of <alist> <symbol> #t):

   Given a particular GOOPS class `klass`, which has some number of
   direct slots, and where each of which may have an option designated
   by keyword `ctor-kw` which specifies some `assoc`-able value, which
   we'll call the constructor key, and, if that slot has a constructor
   key, must also have an ``#:init-keyword``:

   Given an association list `ctor-alist` mapping `assoc`-able values to
   some arbitrary value, searches for each of the known constructor keys
   and accumulates them to a call to `make` for the specified `klass`,
   and with the corresponding init-keyword for the slot.

   This gives a relatively low-effort way to deserialise other objects
   into instances of GOOPS classes, so long as one can provide an alist
   describing what the instance's slots should hold.

   Exemplifying::

     (define-class <box> ()
       (cat
        #:init-keyword  #:cat
        #:ctor-key      'cat))

     (make-instance-from-class-ctor-key
       <box>
       #:ctor-key
       '())
     ==> (make <box>)

     (make-instance-from-class-ctor-key
       <box>
       #:ctor-key
       '((cat . "tabby")))
     ==> (make <box> #:cat "tabby")
--|#

(define (make-instance-from-class-ctor-key klass ctor-kw ctor-alist)
  (define (slot-get-options-alist slot)
    (plist->alist (slot-definition-options slot)))

  ;; (define (slot-get-ctor-key slotx ctor-kw)
  ;;   (and=> (assq ctor-kw slotx)
  ;; 	   (compose symbol->string cdr)))

  (define (slot-get-ctor-key slotx ctor-kw)
    (and=> (assq ctor-kw slotx) cdr))

  (define (slot-get-init-keyword slotx)
    (and=> (assq #:init-keyword slotx) cdr))

  (define (slot-make-init-option slot ctor-kw ctor-alist)
    (let* ([slotx    (slot-get-options-alist slot)]
	   [ctor-key (slot-get-ctor-key slotx ctor-kw)]
	   [init-kw  (slot-get-init-keyword slotx)])
      ;; (format #t "slot {~s}: ctor-key {~s}, init-kw {~s}~%"
      ;;            (slot-definition-name slot) ctor-key init-kw)
      (and=> (assoc ctor-key ctor-alist)
	     (compose (cut list init-kw <>) cdr))))

  (let* ([kslots (class-direct-slots klass)]
	 [mkinit (cut slot-make-init-option <> ctor-kw ctor-alist)]
	 [kinits (map mkinit kslots)])
    (apply make `(,klass ,@(apply append (filter identity kinits))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.goops ends here.
