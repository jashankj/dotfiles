#!/bin/sh

exec sed -i -E \
	-e "s@^  #:version \([0-9 ]+\)\$@  #:version ($(date +'%Y %m %d 0'))@" \
	"$@"
