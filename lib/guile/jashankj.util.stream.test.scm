;;; jashankj.util.stream.test --- tests for `jashankj.util.stream'.

;;; Code:

(define-module (jashankj util stream test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util stream)

  #:use-module (srfi srfi-41)   ;; streams
  #:use-module (jashankj util procedure)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.stream.test")

(define (^check-stream f ls it)
  ((compose stream->list (f it) list->stream) ls))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "stream:map-pair"
  (test-equal `()        (^check-stream stream:map-pair `() identity))
  (test-equal `((a . a)) (^check-stream stream:map-pair `(a) identity))
  (test-equal `((1 . 2)) (^check-stream stream:map-pair `(1) (lambda (x) (+ 1 x))))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "stream:push"
  (test-equal `(a)     (^check-stream stream:push `() 'a))
  (test-equal `(a b)   (^check-stream stream:push `(a) 'b))
  (test-equal `(a b c) (^check-stream stream:push `(a b) 'c))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "stream:unshift"
  (test-equal `(a)     (^check-stream stream:unshift `() 'a))
  (test-equal `(a b)   (^check-stream stream:unshift `(b) 'a))
  (test-equal `(a b c) (^check-stream stream:unshift `(b c) 'a))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.stream.test ends here.
