;;; jashankj.util.peg.test --- tests for `jashankj.util.peg'.

;;; Code:

(define-module (jashankj util peg test)
  #:use-module (guile) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util peg)

  #:use-module (ice-9 peg)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.peg.test")

(test-group "peg-match-exactly"

  (define-peg-pattern peg-any body peg-any)

  ;; want one character, got zero
  (test-equal
      #f
    (peg-match-exactly peg-any ""))

  ;; want one character, got two
  (test-equal
      #f
    (peg-match-exactly peg-any "  "))

  ;; want one character, got one
  (let ([result (peg-match-exactly peg-any " ")])
    (test-assert (peg-record? result))
    (test-equal 0   (peg:start result))
    (test-equal 1   (peg:end   result))
    (test-equal " " (peg:substring result))
  )

)

(test-end)

;;; jashankj.util.peg.test ends here.
