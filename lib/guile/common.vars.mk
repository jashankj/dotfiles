#
# Common build rules for all my Guile projects.
#

GUILE3		?= guile3.0
GUILD3		?= guild3.0
TEXI2ANY	?= texi2any

# Where my Guile utility code lives.
MY_GUILE_ROOT	?= ${HOME}/lib/guile

# Paths relative to there:
the.gentexinfo		?= ${MY_GUILE_ROOT}/gentexinfo.ss
the.runtests		?= ${MY_GUILE_ROOT}/runtests.ss
the.gitignore.guile	?= ${MY_GUILE_ROOT}/.gitignore.guile
the.gitignore.docs	?= ${MY_GUILE_ROOT}/.gitignore.docs

GUILE_DIRS	?= ${MY_GUILE_ROOT}

define	__calculate_do_vars
do.guile	:= $${GUILE3} \
			$$(foreach dir, $${GUILE_DIRS}, -L $${dir}) \
			$$(foreach dir, $${GUILE_DIRS}, -C $${dir})
do.guilec	:= $${GUILD3} compile \
			$$(foreach dir, $${GUILE_DIRS}, -L $${dir}) \
			-O2 -W3 --r7rs
do.texi2any	:= $${TEXI2ANY}
endef

project		?= commonguile

.DEFAULT_GOAL	= all
