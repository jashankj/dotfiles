;;; jashankj.util.string.test --- tests for `jashankj.util.string'.

;;; Code:

(define-module (jashankj util string test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util string)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.string.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "gsub"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "string:delete-suffix"
  (test-equal ""   (string:delete-suffix "" ""))
  (test-equal " "  (string:delete-suffix " " ""))
  (test-equal ""   (string:delete-suffix " " " "))
  (test-equal " "  (string:delete-suffix "  " " "))
  (test-equal ""   (string:delete-suffix "  " "  "))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "string:squeeze"
  (test-equal ""      (string:squeeze ""))
  (test-equal " "     (string:squeeze " "))
  (test-equal " "     (string:squeeze "  "))
  (test-equal ". "    (string:squeeze ". "))
  (test-equal ". "    (string:squeeze ".  "))
  (test-equal " . "   (string:squeeze " . "))
  (test-equal " ."    (string:squeeze "  ."))
  (test-equal " . . " (string:squeeze " . . "))
  (test-equal " . . " (string:squeeze "  . . "))
  (test-equal " . . " (string:squeeze " .  . "))
  (test-equal " . . " (string:squeeze " . .  "))
  (test-equal " . . " (string:squeeze "  . .  "))
  (test-equal " . . " (string:squeeze "  .  .  "))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "string:unlines"

  (test-equal
      ""
    (string:unlines))

  (test-equal
      "\n"
    (string:unlines ""))

  (test-equal
      "\n\n"
    (string:unlines "" ""))

  (test-equal
      "\n\n\n"
    (string:unlines "" "\n"))

  (test-equal
      ".\n.\n"
    (string:unlines "." "."))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.string.test ends here.
