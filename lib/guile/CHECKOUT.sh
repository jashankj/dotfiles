#!/usr/bin/env zsh

SRFI=https://github.com/scheme-requests-for-implementation

# zsh SRFI.zsh checkout-list ---
srfis=(
89   113  125  126  128  130  132  143
145  146  151  158  162  167  168  173
180  189  215  
)
# ---

for i in ${(no)srfis}
do
	[ -d third_party/srfi-${i} ] ||
	svn checkout ${SRFI?}/srfi-${i}.git/trunk third_party/srfi-${i}
done

[ -d third_party/chibi ] ||
svn checkout --depth=empty \
	https://github.com/ashinn/chibi-scheme.git/trunk \
	third_party/chibi

[ -d third_party/irregex ] ||
svn checkout \
	https://github.com/ashinn/irregex.git/trunk \
	third_party/irregex
