;;; jashankj.util.sockaddr --- abstraction over socket addressing

;;; Commentary:
#|--

########################################################################
  ``(jashankj util sockaddr)`` --- abstraction over socket addressing
########################################################################

A (hopefully pleasing) abstraction over Guile socket addressing.  I
assume Guile 3 on GNU/Linux, Android/Linux or FreeBSD system (because
that's where I care about these things working).

See ``(jashankj util socket)`` for more on why it's not just SRFI 106.


Compatibility with SRFI 106
------------------------------------

The following are **exactly equivalent to** those defined in SRFI 106.

+ Socket domains ("address families", "protocol families"):

  + `*af-unspec*`: no specific address family;
  + `*af-inet*`: Internet Protocol version 4;
  + `*af-inet6*`: Internet Protocol version 6;

+ Socket types:

  + `*sock-stream*`: sequenced, reliable, two-way, connection-based byte
    streams, with optional out-of-band data channel;

  + `*sock-dgram*`: connectionless, unreliable messages of a fixed
    maximum length ("datagrams");

+ Socket protocols:

  + `*ipproto-ip*`;
  + `*ipproto-tcp*`;
  + `*ipproto-udp*`;


Incompatibility with SRFI 106
------------------------------------

The following are all divergences from SRFI 106.

+ Socket domains ("address families", "protocol families"):

  + added `*pf-unspec*`, `*pf-inet*`, `*pf-inet6*`:
    protocol families corresponding to, respectively,
    `*af-unspec*`, `*af-inet*`, `*af-inet6*`;

  + added `*af-unix*`, `*pf-unix`:
    *nix-domain sockets (from ``AF_UNIX``, ``PF_UNIX``);

+ Socket types:

  + added `*sock-raw*`:
    raw sockets (from ``SOCK_RAW``);

  + added `*sock-rdm*`:
    reliably-delivered-message sockets (from ``SOCK_RDM``);

  + added `*sock-seqpacket*`:
    sequential-packet sockets (from ``SOCK_SEQPACKET``);

+ Socket protocols:

  + added `*ipproto-ipv6*`

--|#
;;; Code:

(define-module (jashankj util sockaddr)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)

  #:re-export (
	*af-unspec* *af-inet* *af-inet6* *af-unix*

	*sock-stream* *sock-dgram*
	*sock-raw* *sock-rdm* *sock-seqpacket*

	*v4addr-any* *v4addr-broadcast* *v4addr-loopback*
	#;*v6addr-any* #;*v6addr-loopback*

	*ipproto-ip* #;*ipproto-ipv6* *ipproto-tcp* *ipproto-udp*

	host/find host/find-by-name host/find-by-address
	host/name host/aliases host/address-type host/addresses

	network/find network/find-by-name network/find-by-address
	network/name network/aliases network/address-type network/net

	protocol/find protocol/find-by-name protocol/find-by-number
	protocol/name protocol/aliases protocol/proto

	service/find service/find-by-name service/find-by-port
	service/name service/aliases service/port service/protocol

	address-info/find
	address-info/flags
	address-info/socket-domain
	address-info/socket-family
	address-info/socket-protocol
	address-info/socket-address
	address-info/canonical-name

	address/number->string
	address/string->number

	socket-address/family socket-address/path
	socket-address/address socket-address/port
	socket-address/flow-info socket-address/scope-id
  )

  #:export (
	socket-domain
	socket-family
	socket-protocol
	socket-address!
  )

  #:use-module
    ((guile)
     #:select (
	(inet-ntop . address/number->string)
	(inet-pton . address/string->number)

	(gethost           . host/find)
	(gethostbyname     . host/find-by-name)
	(gethostbyaddr     . host/find-by-address)
	(hostent:name      . host/name)
	(hostent:aliases   . host/aliases)
	(hostent:addrtype  . host/address-type)
	(hostent:addr-list . host/addresses)

	(getnet          . network/find)
	(getnetbyname    . network/find-by-name)
	(getnetbyaddr    . network/find-by-address)
	(netent:name     . network/name)
	(netent:aliases  . network/aliases)
	(netent:addrtype . network/address-type)
	(netent:net      . network/net)

	(getproto         . protocol/find)
	(getprotobyname   . protocol/find-by-name)
	(getprotobynumber . protocol/find-by-number)
	(protoent:name    . protocol/name)
	(protoent:aliases . protocol/aliases)
	(protoent:proto   . protocol/proto)

	(getserv          . service/find)
	(getservbyname    . service/find-by-name)
	(getservbyport    . service/find-by-port)
	(servent:name     . service/name)
	(servent:aliases  . service/aliases)
	(servent:port     . service/port)
	(servent:proto    . service/protocol)

	(getaddrinfo        . address-info/find)
	(addrinfo:flags     . address-info/flags)
	(addrinfo:fam       . address-info/socket-domain)
	(addrinfo:socktype  . address-info/socket-family)
	(addrinfo:protocol  . address-info/socket-protocol)
	(addrinfo:addr      . address-info/socket-address)
	(addrinfo:canonname . address-info/canonical-name)

	(sockaddr:fam      . socket-address/family)
	(sockaddr:path     . socket-address/path)
	(sockaddr:addr     . socket-address/address)
	(sockaddr:port     . socket-address/port)
	(sockaddr:flowinfo . socket-address/flow-info)
	(sockaddr:scopeid  . socket-address/scope-id)

	(AF_UNSPEC . *af-unspec*)
	(AF_UNIX   . *af-unix*)
	(AF_INET   . *af-inet*)
	(AF_INET6  . *af-inet6*)

	(SOCK_STREAM    . *sock-stream*)
	(SOCK_DGRAM     . *sock-dgram*)
	(SOCK_SEQPACKET . *sock-seqpacket*)
	(SOCK_RAW       . *sock-raw*)
	(SOCK_RDM       . *sock-rdm*)

	(INADDR_ANY       . *v4addr-any*)
	(INADDR_BROADCAST . *v4addr-broadcast*)
	(INADDR_LOOPBACK  . *v4addr-loopback*)
#;	(IN6ADDR_ANY      . *v6addr-any*)
#;	(IN6ADDR_LOOPBACK . *v6addr-loopback*)

	(IPPROTO_IP   . *ipproto-ip*)
#;	(IPPROTO_IPV6 . *ipproto-ipv6*)
#;	(IPPROTO_ICMP . *ipproto-icmp*)
#;	(IPPROTO_RAW  . *ipproto-raw*)
	(IPPROTO_TCP  . *ipproto-tcp*)
	(IPPROTO_UDP  . *ipproto-udp*)

  ))

  #:use-module ((guile) #:select (
	and=>
	make-socket-address
  ))

  ;; For `assertion-violation'.  (We could probably lose that.)
  #:use-module ((rnrs base) #:select (assertion-violation))

  ;; is there a better formatting library (like SRFI 48 but more)?
  ;; SRFI 166 would be nice here, if it weren't a namespace polluter.
  #:use-module (ice-9 format)

  ;; For bitwise-arithmetic operators.
  #:use-module (srfi srfi-60)

  ;; In practice, this could just be SRFI 34 and SRFI 35 calls.
  #:use-module (jashankj util error)
)

(define (lookup who sets name)
  (cond [(assq name sets) => cdr]
        [else (assertion-violation who "no name defined" name)]))

(define (lookup-many who sets . names)
  (define (lookup* name) (lookup who sets name))
  (apply bitwise-ior (map lookup* names)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Socket domains ("address families", "protocol families"):

(define %supported-socket-domains `(
	[unspec  . ,*af-unspec*]
	[inet    . ,*af-inet*]
	[inet6   . ,*af-inet6*]
	[unix    . ,*af-unix*]
))

(define-syntax socket-domain
  (syntax-rules ()
    ((_ name)
     (lookup 'socket-domain %supported-socket-domains 'name))))

(define (check:socket-domain domain)
  (unless (memq domain (map car %supported-socket-domains))
    (croak (¡error!)
           (¡message! "invalid socket-domain")
           (¡irritants! domain))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Socket families ("socket types"):

(define %supported-socket-families `(
	[stream    . ,*sock-stream*]
	[datagram  . ,*sock-dgram*]
	[dgram     . ,*sock-dgram*]
	[raw       . ,*sock-raw*]
	[rdm       . ,*sock-rdm*]
	[seqpacket . ,*sock-seqpacket*]
))

(define-syntax socket-family
  (syntax-rules ()
    ((_ name)
     (lookup 'socket-family %supported-socket-families 'name))))

(define (check:socket-family family)
  (unless (memq family (map car %supported-socket-families))
    (croak (¡error!)
           (¡message! "invalid socket-family")
           (¡irritants! family))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Socket protocols:

(define %supported-socket-protocols `(
	[ip   . ,*ipproto-ip*]
#;	[ipv6 . ,*ipproto-ipv6*]
	[tcp  . ,*ipproto-tcp*]
	[udp  . ,*ipproto-udp*]
))

(define-syntax socket-protocol
  (syntax-rules ()
    ((_ name)
     (lookup 'socket-protocol %supported-socket-protocols 'name))))

(define (check:socket-protocol protocol)
  (unless (memq protocol (map car %supported-socket-protocols))
    (croak (¡error!)
           (¡message! "invalid socket-protocol")
           (¡irritants! protocol))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (eat-host exact addr)
  (cond
   [(assq addr exact) => cdr]
   [(string? addr) (car (host/addresses (host/find addr)))]
   [else addr]))
(define (eat-port port)
  (cond
   [(string? port)
    (service/port (service/find port))]
   [else port]))

(define (make-socket-address-v4 v4addr v4port)
  (define v4-exact
    `((any       . ,*v4addr-any*)
      (broadcast . ,*v4addr-broadcast*)
      (loopback  . ,*v4addr-loopback*)))
  (make-socket-address
   (socket-domain inet)
   (eat-host v4-exact v4addr)
   (eat-port v4port)))

(define (make-socket-address-v6 v6addr v6port)
  (define v6-exact
    `(#|
      (any      . ,*v6addr-any*)
      (loopback . ,*v6addr-loopback*)
      |#))
  (make-socket-address
   (socket-domain inet6)
   (eat-host v6-exact v6addr)
   (eat-port v6port)))

(define (make-socket-address-unix unpath)
  (make-socket-address
   (socket-domain unix)
   unpath))

(define-syntax socket-address!
  (syntax-rules (inet inet6 unix)
    [(_ inet v4addr v4port)
     (make-socket-address-v4 v4addr v4port)]

    [(_ inet6 v6addr v6port)
     (make-socket-address-v6 v6addr v6port)]

    [(_ unix unpath)
     (make-socket-address-unix unpath)]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.sockaddr ends here.
