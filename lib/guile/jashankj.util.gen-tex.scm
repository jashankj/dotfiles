;;; jashankj.util.gen-tex --- helpers for generating TeX

;;; Commentary:
#|--

Functions to help whilst generating TeX

--|#
;;; Code:

(define-module (jashankj util gen-tex)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	tex/math
	tex/bracketed
	tex/⟨-⟩
  )

  #:use-module (srfi srfi-26)   ;; cut
)

(define tex/math (cut string-append "$" <> "$"))

(define (tex/bracketed l r x)
  (string-append "\\left" l " " x "\\right" r))
(define tex/⟨-⟩
  (cut tex/bracketed "\\langle" "\\rangle" <>))

;;; jashankj.util.gen-tex ends here.
