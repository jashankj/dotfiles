;;; jashankj.util.reader --- helpers for reading Scheme.

;;; Commentary:
#|--

Helpers for reading Scheme.

--|#
;;; Code:

(define-module (jashankj util reader)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	read-all-syntax
	read-all-syntax-from-file
  )

  #:use-module (rnrs syntax-case)
  #:use-module (scheme file)
  #:use-module ((guile) #:select (read-syntax))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (read-all-syntax input-filename input-port)
  (let rest ()
    (let ((obj (read-syntax input-port)))
      (if (eof-object? obj) #'()
          (cons (datum->syntax #'input-filename obj) (rest))))))

(define (read-all-syntax-from-file input-filename)
  (call-with-input-file input-filename
    (lambda (input-port)
      (read-all-syntax input-filename input-port))))

;;; jashankj.util.reader ends here.
