;;; jashankj.util.xwfu --- parse ``application/x-www-form-urlencoded``

;;; Commentary:
#|--

########################################################################
``(jashankj util xwfu)`` --- parse ``application/x-www-form-urlencoded``
########################################################################

did you know there's no parser support in Guile for this cursed type?

--|#
;;; Code:

(define-module (jashankj util xwfu)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	*c0-control-pxcs*
	*fragment-pxcs*
	*query-pxcs*
	*special-query-pxcs*
	*path-pxcs*
	*userinfo-pxcs*
	*component-pxcs*
	*xwfu-pxcs*

	application/x-www-form-urlencoded->alist
	alist->application/x-www-form-urlencoded
  )

  #:use-module (ice-9 peg)
  #:use-module (system base compile)
  #:use-module ((web uri) #:select (uri-encode))

  #:use-module ((rnrs arithmetic bitwise)
		#:select (bitwise-arithmetic-shift-left))
  #:use-module (srfi srfi-60)
  #:use-module (srfi srfi-14)   ;; char-sets
  #:use-module (srfi srfi-130)  ;; string-cursors
  #:use-module (chibi match)    ;; (ice-9 match) ;; (srfi srfi-204) ;; match-wcs
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. scm:constant: *c0-control-pxcs*

   Represents the C0 control percent-encode set, defined as the C0
   controls and all code points greater than U+007E (``~``).
--|#
(define *c0-control-pxcs*
  (char-set-union
   (ucs-range->char-set #x0000 #x0020)
   (ucs-range->char-set #x007F #x00FF)))

#|--
.. scm:constant: *fragment-pxcs*

   Represents the fragment percent-encode set, defined as the C0 control
   percent-encode set and U+0020 SPACE, U+0022 (``"``), U+003C (``<``),
   U+003E (``>``), and U+0060 (``\```).
--|#
(define *fragment-pxcs*
  (char-set-adjoin *c0-control-pxcs* #\" #\< #\> #\`))

#|--
.. scm:constant: *query-pxcs*

   Represent the query percent-encode set, defined as the C0 control
   percent-encode set and U+0020 SPACE, U+0022 (``"``), U+0023 (``#``),
   U+003C (``<``), and U+003E (``>``).
--|#
(define *query-pxcs*
  (char-set-adjoin *c0-control-pxcs* #\space #\" #\# #\< #\>))

#|--
.. scm:constant: *special-query-pxcs*

   Represents the special-query percent-encode set, defined as the query
   percent-encode set and U+0027 (``'``).
--|#
(define *special-query-pxcs*  (char-set-adjoin *query-pxcs* #\'))

#|--
.. scm:constant: *path-pxcs*

   Represents the path percent-encode set, defined as the query
   percent-encode set and U+003F (``?``), U+0060 (``\```),
   U+007B (``{``), and U+007D (``}``).
--|#
(define *path-pxcs*  (char-set-adjoin *query-pxcs* #\? #\` #\{ #\}))

#|--
.. scm:constant: *userinfo-pxcs*

   Represents the userinfo percent-encode set, defined as the path
   percent-encode set and
   U+002F (``/``), U+003A (``:``), U+003B (``;``), U+003D (``=``),
   U+0040 (``@``), U+005B (``[``) to U+005E (``^``), inclusive, and
   U+007C (``|``).
--|#
(define *userinfo-pxcs*
  (char-set-union *path-pxcs*
                  (char-set #\/ #\: #\; #\= #\@)
                  (ucs-range->char-set #x005B #x005F)
                  (char-set #\|)))

#|--
.. scm:constant: *component-pxcs*

   Represents the component percent-encode set, defined as the userinfo
   percent-encode set and U+0024 (``$``) to U+0026 (``&``), inclusive,
   U+002B (``+``), and U+002C (``,``).
--|#
(define *component-pxcs*
  (char-set-union *userinfo-pxcs*
                  (ucs-range->char-set #x0024 #x0027)
                  (char-set #\+ #\,)))

#|--
.. scm:constant: *xwfu-pxcs*

   Represents the application/x-www-form-urlencoded percent-encode set,
   defined as the component percent-encode set and U+0021 (``!``),
   U+0027 (``'``) to U+0029 RIGHT PARENTHESIS, inclusive, and
   U+007E (``~``).
--|#
(define *xwfu-pxcs*
  (char-set-union *component-pxcs*
                  (char-set #\!)
                  (ucs-range->char-set #x0027 #x002A)
                  (char-set #\~)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-peg-pattern hexdigit all
  (or (range #\0 #\9) (range #\A #\F) (range #\a #\f)))

(define ->hexdigit
  (match-lambda
    [('hexdigit n)  (string->number n 16)]
    [_  #f]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-peg-pattern percent-encoded all
  (and "%" hexdigit hexdigit))

(define ->percent-encoded
  (match-lambda
    [('percent-encoded "%" msb lsb)
     (string
      (integer->char
       (bitwise-ior
        (bitwise-arithmetic-shift-left (->hexdigit msb) 4)
        (bitwise-arithmetic-shift-left (->hexdigit lsb) 0))))]
    [_  #f]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Brutally broken version:
#; (define-peg-pattern xwfu-byte all (range #\space #\~))

;; Subtly broken version:
#; (define xwfu-byte
  (compile
   (compile-peg-pattern
    `(or ,@(map string
                (char-set->list
                 (char-set-intersection
                  (char-set-complement *xwfu-pxcs*)
                  (ucs-range->char-set #x0000 #x00FF)))))
    'all)))

;; Probably also broken, but seems to work...
(define xwfu-byte
  (compile
   (compile-peg-pattern
    `(or ,@(map string
                (char-set->list
                 (char-set-intersection
                  (char-set-complement
                   (char-set-adjoin *c0-control-pxcs* #\& #\=))
                  (ucs-range->char-set #x0000 #x00FF)))))
    'all)))

(define ->xwfu-byte
  (match-lambda
    [(? string? x)  x]
    [_  #f]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-peg-pattern xwfu-sequence all
  (+ (or percent-encoded xwfu-byte)))

(define ->xwfu-sequence
  (match-lambda
    [('xwfu-sequence xs ...)
     (map (lambda (x) (or (->xwfu-byte x)
                     (->percent-encoded x))) xs)]
    [_  #f]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-peg-pattern xwfu-key   all xwfu-sequence)

(define ->xwfu-key
  (match-lambda
    [('xwfu-key x)  (apply string-append (->xwfu-sequence x))]
    [_  #f]))

(define-peg-pattern xwfu-value all xwfu-sequence)

(define ->xwfu-value
  (match-lambda
    [('xwfu-value x)  (apply string-append (->xwfu-sequence x))]
    [_  #f]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-peg-pattern xwfu-pair all
  (and xwfu-key (? (and "=" xwfu-value))))

(define ->xwfu-pair
  (match-lambda
    [('xwfu-pair key)
     (cons (->xwfu-key key) '())]
    [('xwfu-pair key ("=" value))
     (cons (->xwfu-key key) (->xwfu-value value))]
    [_  #f]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-peg-pattern x-www-form-urlencoded all
  (and xwfu-pair (* (and (ignore "&") x-www-form-urlencoded))))

(define ->x-www-form-urlencoded
  (match-lambda
    [('x-www-form-urlencoded pair)
     (list (->xwfu-pair pair))]
    [('x-www-form-urlencoded pair rest)
     (cons (->xwfu-pair pair)
           (->x-www-form-urlencoded rest))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. scm:procedure: (application/x-www-form-urlencoded->alist string)
   :param <string> string: input data, as a string
   :returns (of <alist> <string> <string>):

   Decode key/value tuples from ``application/x-www-form-urlencoded``.
--|#
(define (application/x-www-form-urlencoded->alist string)
  (->x-www-form-urlencoded
   (peg:tree (match-pattern x-www-form-urlencoded string))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. scm:procedure: (alist->application/x-www-form-urlencoded string)
   :param (of <alist> <string> <string>) alist: input data, as a string
   :returns <string>:

   Encode key/value tuples as ``application/x-www-form-urlencoded``.
--|#
(define (alist->application/x-www-form-urlencoded alist)
  (string-join
   (map
    (match-lambda
      [(k)                     (uri-encode k)]
      [(k . v)  (string-append (uri-encode k) "=" (uri-encode v))])
    alist)
   "&" 'infix))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.xwfu ends here.
