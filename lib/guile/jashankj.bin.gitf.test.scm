;;; jashankj.bin.gitf.test --- tests for `jashankj.bin.gitf'

;;; Code:

(define-module (jashankj bin gitf test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj bin gitf)
  #:use-module (web uri)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.bin.gitf.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "forge:github yshui/picom.git"
  (let ([remote (explode-url "https://github.com/yshui/picom.git")])
    (test-equal "yshui" (remote-user remote))
    (test-equal "picom" (remote-repo remote))
    (test-equal "gh-yshui" (remote-origin remote))
    (test-equal "_github/yshui/picom" (remote-localdir remote))
    (test-equal "picom" (remote-wcdir remote))))

(test-group "forge:gitlab.com qemu-project/qemu"
  (let ([remote (explode-url "https://gitlab.com/qemu-project/qemu")])
    (test-equal "qemu-project" (remote-user remote))
    (test-equal "qemu" (remote-repo remote))
    (test-equal "gl-qemu-project" (remote-origin remote))
    (test-equal "_gitlab/qemu-project/qemu" (remote-localdir remote))
    (test-equal "qemu" (remote-wcdir remote))))

(test-group "forge:gitlab.gnome.org GNOME/glib"
  (let ([remote (explode-url "https://gitlab.gnome.org/GNOME/glib.git")])
    (test-equal "GNOME" (remote-user remote))
    (test-equal "glib" (remote-repo remote))
    (test-equal "gnome-GNOME" (remote-origin remote))
    (test-equal "_gnome/GNOME/glib" (remote-localdir remote))
    (test-equal "glib" (remote-wcdir remote))))

(test-group "forge:gitlab.freedesktop.org fontconfig/fontconfig"
  (let ([remote (explode-url "https://gitlab.freedesktop.org/fontconfig/fontconfig")])
    (test-equal "fontconfig" (remote-user remote))
    (test-equal "fontconfig" (remote-repo remote))
    (test-equal "fdo-fontconfig" (remote-origin remote))
    (test-equal "_fdo/fontconfig/fontconfig" (remote-localdir remote))
    (test-equal "fontconfig" (remote-wcdir remote))))

(test-group "forge:codeberg rgherdt/scheme-json-rpc.git"
  (let ([remote (explode-url "https://codeberg.org/rgherdt/scheme-json-rpc.git")])
    (test-equal "rgherdt" (remote-user remote))
    (test-equal "scheme-json-rpc" (remote-repo remote))
    (test-equal "cb-rgherdt" (remote-origin remote))
    (test-equal "_codeberg/rgherdt/scheme-json-rpc" (remote-localdir remote))
    (test-equal "scheme-json-rpc" (remote-wcdir remote))))

(test-group "forge:srht ~rsl/sphinxcontrib-cldomain"
  (let ([remote (explode-url "https://git.sr.ht/~rsl/sphinxcontrib-cldomain")])
    (test-equal "rsl" (remote-user remote))
    (test-equal "sphinxcontrib-cldomain" (remote-repo remote))
    (test-equal "srht-rsl" (remote-origin remote))
    (test-equal "_srht/rsl/sphinxcontrib-cldomain" (remote-localdir remote))
    (test-equal "sphinxcontrib-cldomain" (remote-wcdir remote))))

(test-group "cgit https://repo.or.cz/docutils.git"
  (let ([remote (explode-url "https://repo.or.cz/docutils.git")])
    (test-equal "docutils" (remote-repo remote))
    (test-equal "orcz" (remote-origin remote))
    (test-equal "_orcz/docutils" (remote-localdir remote))
    (test-equal "docutils" (remote-wcdir remote))))

(test-group "cgit https://repo.or.cz/docutils.git"
  (let ([remote (explode-url "https://repo.or.cz/docutils.git")])
    (test-equal "docutils" (remote-repo remote))
    (test-equal "orcz" (remote-origin remote))
    (test-equal "_orcz/docutils" (remote-localdir remote))
    (test-equal "docutils" (remote-wcdir remote))))

(test-group "cgit https://sourceware.org/git/binutils-gdb.git"
  (let ([remote (explode-url "https://sourceware.org/git/binutils-gdb.git")])
    (test-equal "binutils-gdb" (remote-repo remote))
    (test-equal "sourceware" (remote-origin remote))
    (test-equal "_sourceware/binutils-gdb" (remote-localdir remote))
    (test-equal "binutils-gdb" (remote-wcdir remote))))

(test-group "gnusv gnulib"
  (let ([remote (explode-url "https://git.savannah.gnu.org/git/gnulib.git")])
    (test-equal "gnusv" (remote-origin remote))
    (test-equal "_gnusv/gnulib" (remote-localdir remote))
    (test-equal "gnulib" (remote-wcdir remote))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "commands"
  (let* ([remote     (explode-url "https://git.savannah.gnu.org/git/gnulib.git")]
	 [origin     (remote-origin remote)]
	 [localdir   (remote-localdir remote)]
	 [wcdir      (remote-wcdir remote)]
	 [repo-local (string-append "/src/remote/" localdir ".git")])
    (test-equal
	`("git" "clone" "--bare" "--mirror"
	  "https://git.savannah.gnu.org/git/gnulib.git"
	  "/src/remote/_gnusv/gnulib.git")
      (remote-clone-command remote repo-local))
    (test-equal
	`("git" "-C" "/src/remote/_gnusv/gnulib.git" "fetch")
      (remote-fetch-command remote repo-local))
    (test-equal
	`("git" "clone" "--reference-if-able" "/src/remote/_gnusv/gnulib.git"
	  "--shared" "--origin" "gnusv"
	  "https://git.savannah.gnu.org/git/gnulib.git" "gnulib")
      (local-clone-command remote repo-local origin wcdir))
    (test-equal
	"/src/remote/_gnusv/gnulib.git/.checkouts"
      (checkout-file-of repo-local))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end) ;; jashankj.bin.gitf.test


;;; jashankj.bin.gitf.test ends here.
