;;; jashankj.util.error --- error-generation helpers

;;; Commentary:
#|--

Error-generation helpers.

Choosing ``(rnrs)`` to get ``(rnrs conditions)``.

--|#
;;; Code:

(define-module (jashankj util error)
  #:use-module (rnrs) #:pure #:declarative? #t
  #:version (2024 01 13 0)
  #:re-export (croak! carp!)
  #:export (
	croak carp

	#{¡message!}#
	#{¡warning!}#
	#{¡serious!}#
	#{¡error!}#
	#{¡violation!}#
	#{¡assertion!}#
	#{¡irritants!}#
	#{¡non-continuable!}#
	#{¡restriction!}#
	#{¡lexical!}#
	#{¡syntax!}#
	#{¡undefined!}#
	#{¡invalid-input!}#
	#{¡immutable-object!}#
  )

  #:use-module
    ((rnrs exceptions)
     #:select ((raise             . croak!)
               (raise-continuable . carp!)))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (croak . xs) (croak! (apply condition xs)))
(define (carp  . xs) (carp!  (apply condition xs)))

(define ¡message!         make-message-condition)
(define ¡warning!         make-warning)
(define ¡serious!         make-serious-condition)
(define ¡error!           make-error)
(define ¡violation!       make-violation)
(define ¡assertion!       make-assertion-violation)
(define ¡irritants!       make-irritants-condition)
(define ¡non-continuable! make-non-continuable-violation)
(define ¡restriction!     make-implementation-restriction-violation)
(define ¡lexical!         make-lexical-violation)
(define ¡syntax!          make-syntax-violation)
(define ¡undefined!       make-undefined-violation)
(define (¡invalid-input! input reason)
  (condition
   (¡error!)
   (¡message! (string-append "Invalid input: " reason))
   (¡irritants! input)))
(define (¡immutable-object! input)
  (condition
   (¡restriction!)
   (¡message! "object is immutable")
   (¡irritants! input)))

;;; jashankj.util.error ends here.
