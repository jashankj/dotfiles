;;; jashankj.util.peg --- helpers for `(ice-9 peg)`

;;; Commentary:
#|--

########################################################################
        ``(jashankj util peg)`` --- helpers for ``(ice-9 peg)``
########################################################################

Add some SRFI-115-flavoured keywords; some can be defined in terms of
existing parser implementations in ``(ice-9 peg codegen)``, but some are
also being implemented here from scratch.

-)|#
;;; Code:

(define-module (jashankj util peg)
  #:use-module (guile) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	cg-exactly
	cg-at-least
	cg-repeated

	end-of-string

	lower lower-case
	upper upper-case
	alpha alphabetic
	num   numeric
	alnum alphanum alphanumeric
	punct punctuation
	symbol
	graph graphic
	white space whitespace
	print printing
	cntrl control
	xdigit hex-digit

	peg-match-exactly
  )

  #:use-module (ice-9 peg)
  #:use-module (ice-9 peg codegen)
  #:use-module (system base compile)
  #:use-module (system base pmatch)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Leak some symbols from ``(ice-9 peg codegen)``:

(define-syntax single-filter
  (syntax-rules ()
    "If EXP is a list of one element, return the element.  Otherwise
return EXP."
    ((_ exp)
     (pmatch exp
       ((,elt) elt)
       (,elts elts)))))

(define-syntax push-not-null!
  (syntax-rules ()
    "If OBJ is non-null, push it onto LST, otherwise do nothing."
    ((_ lst obj)
     (if (not (null? obj))
         (push! lst obj)))))

(define-syntax push!
  (syntax-rules ()
    "Push an object onto a list."
    ((_ lst obj)
     (set! lst (cons obj lst)))))

(define baf  (@@ (ice-9 peg codegen) baf))
(define cggr (@@ (ice-9 peg codegen) cggr))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; A factored-out version of many `cg-' functions
;;
;; Carefully picking through the otherwise-identical shapes of `cg-*',
;; `cg-+', `cg-?', `cg-followed-by' and `cg-not-followed-by' suggests
;; one could carefully punch out holes to then make the bounds of the
;; operation a parameter.  (I'm not sure why they haven't; templated
;; macro expansions seems eminently Scheme.)
;;
;; All those functions can be redefined in terms of this expression,
;; setting the `lower-bound' and `upper-bound' parameters ---
;;
;;                      lower-bound             upper-bound
;;      cg-*            #t                      #t
;;      cg-+            #`(> count 1)           #t
;;      cg-?            #t                      #`(< count 1)
;;      cg-exactly      #`(>= count #,#'n)      #`(<= count #,#'n)
;;      cg-at-least     #`(>= count #,#'n)      #t
;;      cg-repeated     #`(>= count #,#'n)      #`(<= count #,#'m)
;;
;; {not-,}followed-by would probably need an additional parameter.
;;

(define (cg-cg lower-bound upper-bound pat accum)
  #`(lambda (str strlen at)
      (let ([body '()])
        (let lp ([end at]
		 [count 0])
          (let* ([match   (#,(compile-peg-pattern pat (baf accum))
			   str strlen end)]
                 [new-end (if match (car match) end)]
                 [count   (if (> new-end end) (1+ count) count)])
            (if (> new-end end)
                (push-not-null! body (single-filter (cadr match))))
            (if (and (> new-end end)
                     #,upper-bound)
                (lp new-end count)
                (let ([success #,lower-bound])
                  #,#`(and success
                           #,(cggr (baf accum) 'cg-body
                                   #'(reverse body) #'new-end)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
| (* <sre> ...)               ; 0 or more matches.
| (zero-or-more <sre> ...)
|#

(add-peg-compiler! 'zero-or-more (@@ (ice-9 peg codegen) cg-*))

#|
| (+ <sre> ...)               ; 1 or more matches.
| (one-or-more <sre> ...)
|#

(add-peg-compiler! 'one-or-more (@@ (ice-9 peg codegen) cg-+))

#|
| (? <sre> ...)               ; 0 or 1 matches.
| (optional <sre> ...)
|#

(add-peg-compiler! 'optional (@@ (ice-9 peg codegen) cg-?))

#|
| (= <n> <sre> ...)           ; <n> matches.
| (exactly <n> <sre> ...)
|#

(define (cg-exactly args accum)
  (syntax-case args ()
    ((n pat)
     (cg-cg #`(>= count #,#'n)
	    #`(<= count #,#'n)
	    #'pat accum))))

(add-peg-compiler! '=       cg-exactly)
(add-peg-compiler! 'exactly cg-exactly)

#|
| (>= <n> <sre> ...)          ; <n> or more matches.
| (at-least <n> <sre> ...)
|#

(define (cg-at-least args accum)
  (syntax-case args ()
    ((n pat)
     (cg-cg #`(>= count #,#'n) #t #'pat accum))))

(add-peg-compiler! '>=       cg-at-least)
(add-peg-compiler! 'at-least cg-at-least)

#|
| (** <n> <m> <sre> ...)      ; <n> to <m> matches.
| (repeated <n> <m> <sre> ...)
|#

(define (cg-repeated args accum)
  (syntax-case args ()
    ((n m pat)
     (cg-cg #`(<= #,#'n count #,#'m) #t #'pat accum))))

(add-peg-compiler! '**       cg-repeated)
(add-peg-compiler! 'repeated cg-repeated)

#|
| (|  <sre> ...)              ; Alternation.
| (or <sre> ...)
|#

#|
| (:   <sre> ...)             ; Sequence.
| (seq <sre> ...)
|#

(add-peg-compiler! ':   (@@ (ice-9 peg codegen) cg-and))
(add-peg-compiler! 'seq (@@ (ice-9 peg codegen) cg-and))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; string assertions

(define-peg-pattern end-of-string none
  (not-followed-by peg-any))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; these aren't all correct, but are close enough for ascii

(define-peg-pattern lower-case body (range #\a #\z))
(define lower lower-case)

(define-peg-pattern upper-case body (range #\A #\Z))
(define upper upper-case)

(define-peg-pattern alphabetic body (or upper-case lower-case))
(define alpha alphabetic)

(define-peg-pattern numeric body (range #\0 #\9))
(define num   numeric)

(define-peg-pattern alphanumeric body (or alphabetic numeric))
(define alphanum alphanumeric)
(define alnum    alphanumeric)

(define-peg-pattern punctuation body (or
	"!"	"\""	"#"	"%"	"&"	"'"	"("	")"
	"*"	","	"-"	"."	"/"	":"	";"	"?"
	"@"	"["	"\\"	"]"	"_"	"{"	"}"
))
(define punct punctuation)

(define-peg-pattern symbol body (or
	"$"
	"+"
	"<"
	"="
	">"
	"^"
	"`"
	"|"
	"~"
))

(define-peg-pattern graphic body (or alphanumeric punctuation symbol))
(define graph graphic)

(define-peg-pattern whitespace body (or " " "\n" "\t" "\r" "\f" "\n"))
(define white whitespace)
(define space whitespace)

(define-peg-pattern printing body (or graphic whitespace))
(define print printing)

(define-peg-pattern control body (range #\0 #\31))
(define cntrl control)

(define-peg-pattern hex-digit body
  (or numeric
      "A" "B" "C" "D" "E" "F"
      "a" "b" "c" "d" "e" "f"))
(define xdigit hex-digit)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (peg-match-exactly nonterm string)
  (let ([length (string-length string)]
	[result (match-pattern nonterm string)])
    (if (and (peg-record? result)
	     (= (peg:start result) 0)
	     (= (peg:end   result) length))
	result
	#f)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.peg ends here.
