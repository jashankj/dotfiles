;;; jashankj.util.gen-tex.test --- tests for `jashankj.util.gen-tex'

;;; Code:

(define-module (jashankj util gen-tex test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util gen-tex)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.gen-tex.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "tex/math"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "tex/bracketed"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "tex/⟨-⟩"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.gen-tex.test ends here.
