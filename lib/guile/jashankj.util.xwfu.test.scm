;;; jashankj.util.xwfu.test --- tests for `jashankj.util.xwfu'.

;;; Code:

(define-module (jashankj util xwfu test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util xwfu)

  #:use-module ((guile) #:select (@ @@))
  #:use-module (ice-9 peg)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.xwfu.test")

(define peg
  (lambda (nonterminal)
    (lambda (string)
      (peg:tree (match-pattern nonterminal string)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "hexdigit"

  (define ^hexdigit (peg (@@ (jashankj util xwfu) hexdigit)))
  (define >hexdigit (@@ (jashankj util xwfu) ->hexdigit))
  (test-equal '(hexdigit "0") (^hexdigit "0"))
  (test-equal #x0 (>hexdigit (^hexdigit "0")))

  (test-equal '(hexdigit "1") (^hexdigit "1"))
  (test-equal #x1 (>hexdigit (^hexdigit "1")))

  (test-equal '(hexdigit "2") (^hexdigit "2"))
  (test-equal #x2 (>hexdigit (^hexdigit "2")))

  (test-equal '(hexdigit "3") (^hexdigit "3"))
  (test-equal #x3 (>hexdigit (^hexdigit "3")))

  (test-equal '(hexdigit "4") (^hexdigit "4"))
  (test-equal #x4 (>hexdigit (^hexdigit "4")))

  (test-equal '(hexdigit "5") (^hexdigit "5"))
  (test-equal (>hexdigit (^hexdigit "5")) #x5)

  (test-equal '(hexdigit "6") (^hexdigit "6"))
  (test-equal #x6 (>hexdigit (^hexdigit "6")))

  (test-equal '(hexdigit "7") (^hexdigit "7"))
  (test-equal #x7 (>hexdigit (^hexdigit "7")))

  (test-equal '(hexdigit "8") (^hexdigit "8"))
  (test-equal #x8 (>hexdigit (^hexdigit "8")))

  (test-equal '(hexdigit "9") (^hexdigit "9"))
  (test-equal #x9 (>hexdigit (^hexdigit "9")))

  (test-equal '(hexdigit "a") (^hexdigit "a"))
  (test-equal #xA (>hexdigit (^hexdigit "a")))

  (test-equal '(hexdigit "A") (^hexdigit "A"))
  (test-equal #xA (>hexdigit (^hexdigit "A")))

  (test-equal '(hexdigit "b") (^hexdigit "b"))
  (test-equal #xB (>hexdigit (^hexdigit "b")))

  (test-equal '(hexdigit "B") (^hexdigit "B"))
  (test-equal #xB (>hexdigit (^hexdigit "B")))

  (test-equal '(hexdigit "c") (^hexdigit "c"))
  (test-equal #xC (>hexdigit (^hexdigit "c")))

  (test-equal '(hexdigit "C") (^hexdigit "C"))
  (test-equal #xC (>hexdigit (^hexdigit "C")))

  (test-equal '(hexdigit "d") (^hexdigit "d"))
  (test-equal #xD (>hexdigit (^hexdigit "d")))

  (test-equal '(hexdigit "D") (^hexdigit "D"))
  (test-equal #xD (>hexdigit (^hexdigit "D")))

  (test-equal '(hexdigit "e") (^hexdigit "e"))
  (test-equal #xE (>hexdigit (^hexdigit "e")))

  (test-equal '(hexdigit "E") (^hexdigit "E"))
  (test-equal #xE (>hexdigit (^hexdigit "E")))

  (test-equal '(hexdigit "f") (^hexdigit "f"))
  (test-equal #xF (>hexdigit (^hexdigit "f")))

  (test-equal '(hexdigit "F") (^hexdigit "F"))
  (test-equal #xF (>hexdigit (^hexdigit "F")))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "percent-encoded"

  (define ^percent-encoded
    (peg (@@ (jashankj util xwfu) percent-encoded)))
  (define >percent-encoded
    (@@ (jashankj util xwfu) ->percent-encoded))

  (test-equal #f (^percent-encoded "a"))
  (test-equal #f (^percent-encoded "%"))
  (test-equal #f (^percent-encoded "%0"))
  (test-equal #f (^percent-encoded "%0 "))

  (test-equal
      '(percent-encoded "%" (hexdigit "2") (hexdigit "0"))
    (^percent-encoded "%20"))
  (test-equal
      " "
    (>percent-encoded (^percent-encoded "%20")))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "xwfu-sequence"

  (define ^xwfu-sequence
    (peg (@@ (jashankj util xwfu) xwfu-sequence)))
  (define >xwfu-sequence
    (@@ (jashankj util xwfu) ->xwfu-sequence))

  ;; yes, the empty string doesn't match
  (test-equal #f (^xwfu-sequence ""))

  (test-equal
      '(xwfu-sequence "a")
    (^xwfu-sequence "a"))

  (test-equal
      '("a")
    (>xwfu-sequence (^xwfu-sequence "a")))

  (test-equal
      '(xwfu-sequence "ab")
    (^xwfu-sequence "ab"))

  (test-equal
      '("ab")
    (>xwfu-sequence (^xwfu-sequence "ab")))

  (test-equal
      '(xwfu-sequence "a"
                      (percent-encoded "%" (hexdigit "2") (hexdigit "0"))
                      "b")
    (^xwfu-sequence "a%20b"))

  (test-equal
      '("a" " " "b")
    (>xwfu-sequence (^xwfu-sequence "a%20b")))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "xwfu-pair"

  (define ^xwfu-pair
    (peg (@@ (jashankj util xwfu) xwfu-pair)))
  (define >xwfu-pair
    (@@ (jashankj util xwfu) ->xwfu-pair))

  (test-equal
      '("a")
    (>xwfu-pair (^xwfu-pair "a")))
  (test-equal
      '("abc")
    (>xwfu-pair (^xwfu-pair "abc")))
  (test-equal
      '("abc" . "d")
    (>xwfu-pair (^xwfu-pair "abc=d")))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "x-www-form-urlencoded"

  (define ^xwfu
    (peg (@@ (jashankj util xwfu) x-www-form-urlencoded)))
  (define >xwfu
    (@@ (jashankj util xwfu) ->x-www-form-urlencoded))

  (test-equal
      '(("a"))
    (>xwfu (^xwfu "a")))
  (test-equal
      '(("abc"))
    (>xwfu (^xwfu "abc")))
  (test-equal
      '(("abc" . "d"))
    (>xwfu (^xwfu "abc=d")))
  (test-equal
      '(("abc") ("d"))
    (>xwfu (^xwfu "abc&d")))
  (test-equal
      '(("abc" . "d") ("c" . "124"))
    (>xwfu (^xwfu "abc=d&c=124")))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "application/x-www-form-urlencoded->alist"

  (test-equal
      '(("abc" . "d") ("c" . "124"))
    (application/x-www-form-urlencoded->alist "abc=d&c=124")
  )

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "alist->application/x-www-form-urlencoded"

  (test-equal
      "abc=d&c=124"
    (alist->application/x-www-form-urlencoded
     '(("abc" . "d") ("c" . "124")))
  )

  (test-equal
      "x%20y=y%20z"
    (alist->application/x-www-form-urlencoded
     '(("x y" . "y z")))
  )

  (test-equal
      "abc&c=124"
    (alist->application/x-www-form-urlencoded
     '(("abc") ("c" . "124")))
  )

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.xwfu.test ends here.
