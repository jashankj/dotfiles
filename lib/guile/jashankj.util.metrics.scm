;;; jashankj.util.metrics --- export metrics to "Prometheus"

;;; Commentary:
#|--

########################################################################
      ``(jashankj util metrics)`` --- export metrics to Prometheus
########################################################################

For various reasons, I like the idea of exporting metrics to Prometheus.
This is a pretty bare-bones (Guile) Scheme client library.

It would be very nice to have a not-Guile-specific implementation...
(I don't believe there's anything that can't be implemented otherwise.)

--|#
;;; Code:

(define-module (jashankj util metrics)
  #:use-module (rnrs) #:pure #:declarative? #t
  #:version (2023 03 23 0)

  #:export (
	<label>
	label!

	<metric>
	metric/set!
	metric/inc!
	metric/dec!

	<metric-counter>
	counter!

	<metric-gauge>
	gauge!

	<metric-histogram>
	histogram!

	<metric-summary>
	summary!

	<metric-untyped>
	untyped!

	render-metrics-to-string
	GET//metrics

	guile:export-metrics!
  )

  #:use-module ((guile) #:select (
	compose
	cond-expand
	const
	use-modules

	gc-stats
	after-gc-hook
	add-hook!	;; XXX not from srfi-173
  ))
  #:use-module (ice-9 format)
  #:use-module (ice-9 regex)
  #:use-module ((ice-9 ports) #:select (with-output-to-string))
  #:use-module (oop goops)
  #:use-module (web response)

  #:use-module (srfi srfi-1)    ;; lists
  #:use-module (srfi srfi-9)    ;; records
  #:use-module (srfi srfi-13)   ;; strings
  #:use-module (srfi srfi-89)   ;; define*
  #:use-module (srfi srfi-128)  ;; comparators
  #:use-module (srfi srfi-130)  ;; string-cursors
  #:use-module (srfi srfi-146)  ;; mapping

  #:use-module (jashankj util error)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--

Labels
====================================

--|#

#|--
.. scm:class:: <label>

   .. scm:slot:: name
   .. scm:slot:: value
--|#
(define-class <label> ()
  (name
	#:getter	label/name
	#:init-keyword	#:name)
  (value
	#:getter	label/value
	#:init-keyword	#:value))

#|--
.. scm:macro:: (label! k v)

   Create an instance of :scm:class:`<label>`
   using the specified values.
--|#
(define-syntax label!
  (syntax-rules ()
    [(_ k v)  (make <label> #:name k #:value v)]))

(define-method (format-label (label <label>))
  (string-append (label/name label) "=\"" (label/value label) "\""))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--

Metrics
====================================

--|#

#|--
.. scm:class:: <metric>

   .. scm:slot:: name
   .. scm:slot:: type
   .. scm:slot:: help
   .. scm:slot:: labels
   .. scm:slot:: value
   .. scm:slot:: timestamp
--|#
(define-class <metric> ()
  (name
	#:getter	metric/name
	#:init-keyword	#:name)
  (type
	#:getter	metric/type
	#:allocation	#:virtual
	#:slot-ref
	(lambda (self)
	  (string-drop-right
	   (string-drop
	    (symbol->string
	     (class-name
	      (class-of
	       self)))
	    8)
	   1))
	#:slot-set!
	(lambda (self new)
	  (croak (¡error!)
		 (¡message! "metric/type is not settable")
		 (¡irritants! #:self self #:new new))))
  (help
	#:getter	metric/help
	#:init-keyword	#:help)
  (labels
	#:getter	metric/labels
	#:init-keyword	#:label
	#:init-value	'())
  (value
	#:getter	metric/value
	#:setter	metric/set!
	#:accessor	metric->value
	#:init-keyword	#:value)
  (timestamp
	#:getter	metric/timestamp
	#:init-keyword	#:timestamp))

#|--
.. scm:procedure: (check-name! name)
   :param <string> name: a string to consider
   :returns: *name*
   :raises: <implementation-restriction-violation> <lexical-violation>

   Check whether the name complies with Prometheus' requirements;
   returns *name* if it does, or raises an error if not.
--|#
(define (check-name! name)
  (unless (valid-name? name)
    (croak (¡restriction!) (¡lexical!)
           (¡message! "invalid metric or label name")
           (¡irritants! name)))
  name)

#|--
.. scm:procedure: (valid-name? str)
   :param <string> str: a string to consider
   :returns: ``#t`` if acceptable, ``#f`` otherwise

   Check whether the name complies with Prometheus' requirements.
--|#
(define (valid-name? str)
  (string-match "[a-zA-Z_:][a-zA-Z0-9_:]*" str))

(define-method (format-metric-labels (metric <metric>))
  (let ([labels (metric/labels metric)])
    (if (not (null? labels))
        (string-append "{" (string-join (map format-label labels) "," 'infix) "}")
        "")))

(define-method (format-metric (metric <metric>) (verbose? <boolean>))
  (with-output-to-string
    (lambda ()
      (when verbose?
        (format #t "# HELP ~a ~a~%" (metric/name metric) (metric/help metric))
        (format #t "# TYPE ~a ~a~%" (metric/name metric) (metric/type metric)))
      (display (metric/name metric))
      (display (format-metric-labels metric))
      (display " ")
      (display (metric/value metric))
      (when (slot-bound? metric 'timestamp)
        (display (metric/timestamp metric)))
      (newline))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. scm:variable: **all-metrics**

   A table of all the metrics that exist in the system;
   a mapping of ``<string>`` metric name to ``<metric>``.
--|#

(define **all-metrics**
  (mapping (make-default-comparator)))


#|--
.. scm:procedure: (find-metric metric-name)
   :param <string> metric-name: the metric name to find
   :returns (or <metric> #f):

   Find the metric with the specified name, or return ``#f``.
--|#
(define (find-metric metric-name)
  (mapping-ref **all-metrics** metric-name (const #f)))

#|--
.. scm:procedure: (export-metric metric)
   :param <metric> metric: the new metric to export

   Add a new metric to the set of all metrics known.
--|#
(define (export-metric metric)
  (set! **all-metrics**
    (mapping-set **all-metrics**
                 (metric/name metric)
                 metric)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-class <metric-counter>   (<metric>))

(define-method (metric/inc! (metric <metric-counter>) (v <number>))
  (metric/set! metric (+ (metric->value metric) v)))
(define-method (metric/inc! (metric <metric-counter>))
  (metric/inc! metric 1))

(define-syntax counter!
  (syntax-rules ()
    [(_ name help labels)
     (or (find-metric name)
         (let ([$m (make <metric-counter>
                     #:name   (check-name! name)
                     #:help   help
                     #:labels labels
                     #:value  0)])
           (export-metric $m)
           $m))]))

(define-method (format-metric (metric <metric-counter>)
                              (verbose? <boolean>))
  (next-method))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-class <metric-gauge>     (<metric>))

(define-syntax gauge!
  (syntax-rules ()
    [(_ name help labels)
     (or (find-metric name)
         (let ([$m (make <metric-gauge>
                     #:name   (check-name! name)
                     #:help   help
                     #:labels labels
                     #:value  0)])
           (export-metric $m)
           $m))]))

(define-method (metric/inc! (metric <metric-gauge>) (v <number>))
  (metric/set! metric (+ (metric->value metric) v)))
(define-method (metric/inc! (metric <metric-gauge>))
  (metric/inc! metric 1))
(define-method (metric/dec! (metric <metric-gauge>) (v <number>))
  (metric/set! metric (- (metric->value metric) v)))
(define-method (metric/dec! (metric <metric-gauge>))
  (metric/inc! metric 1))

(define-method (format-metric (metric <metric-gauge>)
                              (verbose? <boolean>))
  (next-method))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. TODO:: properly implement histograms.
--|#

(define-class <metric-histogram> (<metric>))

(define-syntax histogram!
  (syntax-rules ()
    [(_ name help labels)
     (or (find-metric name)
         (let ([$m (make <metric-histogram>
                     #:name   (check-name! name)
                     #:help   help
                     #:labels labels
                     #:value  0)])
           (export-metric $m)
           $m))]))

(define-method (format-metric (metric <metric-histogram>)
                              (verbose? <boolean>))
  (next-method))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. TODO:: properly implement summaries.
--|#

(define-class <metric-summary>   (<metric>))

(define-syntax summary!
  (syntax-rules ()
    [(_ name help labels)
     (or (find-metric name)
         (let ([$m (make <metric-summary>
                     #:name   (check-name! name)
                     #:help   help
                     #:labels labels
                     #:value  0)])
           (export-metric $m)
           $m))]))

(define-method (format-metric (metric <metric-summary>)
                              (verbose? <boolean>))
  (next-method))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. TODO:: properly implement untyped.
--|#

(define-class <metric-untyped>   (<metric>))

(define-syntax untyped!
  (syntax-rules ()
    [(_ name help labels)
     (or (find-metric name)
         (let ([$m (make <metric-untyped>
                     #:name   (check-name! name)
                     #:help   help
                     #:labels labels)])
           (export-metric $m)
           $m))]))

(define-method (format-metric (metric <metric-untyped>)
                              (verbose? <boolean>))
  (next-method))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (render-metrics-to-string metrics verbose?)
  (with-output-to-string
    (lambda ()
      (mapping-for-each
       (lambda (mname metric)
         (display (format-metric metric verbose?))
         (newline))
       metrics))))

(define (GET//metrics req reqb)
  (values
   (build-response
    #:version '(1 . 0)
    #:code 200 #:reason-phrase "OK"
    #:validate-headers? #t
    #:headers
    `((content-type . (text/plain (version . "0.0.4")))))
   (render-metrics-to-string **all-metrics** #t)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. scm:procedure: (guile:export-metrics!)

   Export metrics available in Guile's :scm:proc:`gc-stats`.
--|#
(define (guile:export-metrics!)
  (define stats (gc-stats))
  (define %descr
    `((gc-time-taken
       . "The time taken by the most recent garbage collection cycle.")
      (heap-size
       . "The number of GC-managed bytes in the heap.")
      (heap-free-size
       . "The lower bound on the number of free bytes in the heap.")
      (heap-total-allocated
       . "The total number of bytes allocated in this process.")
      (heap-allocated-since-gc
       . "The number of bytes allocated since the last GC cycle.")
      (protected-objects
       . "The number of protected objects in this VM.")
      (gc-times
       . "The number of times the garbage collector has run.")))

  ;; See what Guile is exporting, and register it.
  ;; Get an alist of (key . metric).
  (define %stats
    (map
     (lambda (kv)
       (let* ([k-name (car kv)]
              [k_name (string-map
                       (lambda (x) (case x [(#\-) #\_] [else x]))
                       (symbol->string (car kv)))]
              [value (cdr kv)]
              [metric (gauge!
                       (string-append "guile_" k_name)
                       (cdr (assq k-name %descr))
                       '())])
         (metric/set! metric value)
         (cons k-name metric)))
     stats))

  ;; Set up an after-gc-hook to update the metrics.
  (define (update-stats)
    (map
     (lambda (stat) (metric/set! (cdr (assq (car stat) %stats)) (cdr stat)))
     (gc-stats)))

  (add-hook! after-gc-hook update-stats)

  %stats
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.metrics ends here.
