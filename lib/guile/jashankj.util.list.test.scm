;;; jashankj.util.list.test --- tests for `jashankj.util.list'.

;;; Code:

(define-module (jashankj util list test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util list)

  #:use-module (srfi srfi-41)   ;; streams
  #:use-module (jashankj util procedure)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.list.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "cartesian-product"

  (test-equal
      '((1 a) (1 b) (1 c) (2 a) (2 b) (2 c) (3 a) (3 b) (3 c))
    (stream->list
     (cartesian-product
      (list->stream '(1 2 3))
      (list->stream '(a b c)))))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.list.test ends here.
