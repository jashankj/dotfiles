;;; jashankj.util.number.test --- tests for `jashankj.util.number'.

;;; Code:

(define-module (jashankj util number test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util number)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.number.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "integer:as-chunks"
  (test-equal
      `(#b0 #b0 #b0 #b0)
    (integer:as-chunks 0 4 1))

  (test-equal
      `(#b0 #b0 #b0 #b1)
    (integer:as-chunks 1 4 1))

  (test-equal
      `(#b1 #b1 #b1 #b1)
    (integer:as-chunks 15 4 1))

  (test-equal
      `(#b00 #b01)
    (integer:as-chunks 1 2 2))

  (test-equal
      `(#b11 #b00)
    (integer:as-chunks #b1100 2 2))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "number->hex"

  (test-equal  "0" (number->hex #x00))
  (test-equal  "1" (number->hex #x01))
  (test-equal  "2" (number->hex #x02))
  (test-equal  "3" (number->hex #x03))
  (test-equal  "4" (number->hex #x04))
  (test-equal  "5" (number->hex #x05))
  (test-equal  "6" (number->hex #x06))
  (test-equal  "7" (number->hex #x07))
  (test-equal  "8" (number->hex #x08))
  (test-equal  "9" (number->hex #x09))
  (test-equal  "a" (number->hex #x0a))
  (test-equal  "b" (number->hex #x0b))
  (test-equal  "c" (number->hex #x0c))
  (test-equal  "d" (number->hex #x0d))
  (test-equal  "e" (number->hex #x0e))
  (test-equal  "f" (number->hex #x0f))
  (test-equal "10" (number->hex #x10))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "number->HEX"

  (test-equal  "0" (number->HEX #x00))
  (test-equal  "1" (number->HEX #x01))
  (test-equal  "2" (number->HEX #x02))
  (test-equal  "3" (number->HEX #x03))
  (test-equal  "4" (number->HEX #x04))
  (test-equal  "5" (number->HEX #x05))
  (test-equal  "6" (number->HEX #x06))
  (test-equal  "7" (number->HEX #x07))
  (test-equal  "8" (number->HEX #x08))
  (test-equal  "9" (number->HEX #x09))
  (test-equal  "A" (number->HEX #x0a))
  (test-equal  "B" (number->HEX #x0b))
  (test-equal  "C" (number->HEX #x0c))
  (test-equal  "D" (number->HEX #x0d))
  (test-equal  "E" (number->HEX #x0e))
  (test-equal  "F" (number->HEX #x0f))
  (test-equal "10" (number->HEX #x10))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.number.test ends here.
