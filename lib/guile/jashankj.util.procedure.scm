;;; jashankj.util.procedure --- procedure manipulation functions

;;; Commentary:
#|--

Procedure manipulation functions.

--|#
;;; Code:

(define-module (jashankj util procedure)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	bracketed
	compose
	define-match
	identity
	sequential-cwv
	and=>
	|==>|
  )

  #:use-module (rnrs syntax-case)
  #:use-module (chibi match)    ;; (ice-9 match) ;; (srfi srfi-204) ;; match-wcs
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|--
.. scm:procedure: (bracketed before within after)
   :param <procedure> before:
   :param <procedure> within:
   :param <procedure> after:

   Evaluates to the value of `within`; but runs procedure `before`
   before it, and `after` after it (and discards the results of `before`
   and `after`).
--|#
(define (bracketed before within after)
  (before) (define x (within)) (after) x)

#|--
.. scm:procedure: (compose proc . rest)
   :param <procedure> proc:
   :param (of <list> <procedure>) rest:

   Compose `proc` with the procedures in `rest`, such that the last one
   in `rest` is applied first and `proc` last, and return the resulting
   procedure.  The given procedures must have compatible arity.
--|#
(define (compose proc . rest)
  (if (null? rest)
      proc
      (let ((g (apply compose rest)))
        (lambda args
          (call-with-values (lambda () (apply g args)) proc)))))

#|--
.. scm:macro: (define-match name body ...)
   :param name:
   :param body ...:

   Bind `name` as a `match-lambda*` with `body ...` as its claws.
   Racket has something very similar.
--|#
(define-syntax #{define-match}#
  (lambda (x)
    (syntax-case x ()
      ((define-match name body ...)
       #`(define name (match-lambda* body ...))))))

#|--
.. scm:procedure: (identity x)
   :param x:

   The identity function.  What more were you expecting?
--|#
(define (identity x) x)

#|--
.. scm:procedure: (sequential-cwv . procs)
   :param (of <list> <procedure>) procs:

   Apply `call-with-values` sequentially over the list of procedures
   given, passing the result from one to the next.

   In pseudo-Haskell::
     sequential-cwv []       = nil
     sequential-cwv (x:[])   = x
     sequential-cwv (x:y:[]) = c-w-v x y
     sequential-cwv (x:xs)   = c-w-v x (apply sequential-cwv xs)
--|#
(define-match sequential-cwv
  [() *unspecified*]
  [(x) (x)]
  [(x y) (call-with-values x y)]
  [(xs ... y)
   (call-with-values (lambda () (apply sequential-cwv xs)) y)])

#|--
.. scm:procedure: (and=> value procedure)
   :param (or τ #f) value:
   :param (-> τ υ) value:
   :returns (or υ #f):

   When `value` is ``#f``, return ``#f``.
   Otherwise, return ``(procedure value)``.
--|#
(define (and=> value procedure)
  (and value (procedure value)))

;;; jashankj.util.procedure ends here.
