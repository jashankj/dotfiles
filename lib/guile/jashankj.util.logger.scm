;;; jashankj.util.logger --- nice frontend syntax to SRFI 215

;;; Commentary:
#|--

--|#
;;; Code:

(define-module (jashankj util logger)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	register-logger!  LOG

	reasonable-log-callback
	format-log-message
	unroll-log-message
	severity->string

	EMERGENCY ALERT CRITICAL ERROR WARNING NOTICE INFO DEBUG
  )

  #:use-module (rnrs syntax-case)
  #:use-module (srfi srfi-1)    ;; lists
  #:use-module (srfi srfi-19)   ;; time
  #:use-module (srfi srfi-145)  ;; assume
  #:use-module (srfi srfi-215)  ;; logger

  #:use-module (ice-9 format)
  #:use-module (chibi match)    ;; (ice-9 match) ;; (srfi srfi-204) ;; match-wcs
)

;; TODO: check the log-level, I guess?
(define-syntax LOG
  (lambda (x)
    (syntax-case x (EMERGENCY ALERT CRITICAL ERROR WARNING NOTICE INFO DEBUG)
      [(LOG (EMERGENCY fmt-args ...) args ...)
       #`(send-log 0 (format #f fmt-args ...) args ...)]
      [(LOG (ALERT fmt-args ...) args ...)
       #`(send-log 1 (format #f fmt-args ...) args ...)]
      [(LOG (CRITICAL fmt-args ...) args ...)
       #`(send-log 2 (format #f fmt-args ...) args ...)]
      [(LOG (ERROR fmt-args ...) args ...)
       #`(send-log 3 (format #f fmt-args ...) args ...)]
      [(LOG (WARNING fmt-args ...) args ...)
       #`(send-log 4 (format #f fmt-args ...) args ...)]
      [(LOG (NOTICE fmt-args ...) args ...)
       #`(send-log 5 (format #f fmt-args ...) args ...)]
      [(LOG (INFO fmt-args ...) args ...)
       #`(send-log 6 (format #f fmt-args ...) args ...)]
      [(LOG (DEBUG fmt-args ...) args ...)
       #`(send-log 7 (format #f fmt-args ...) args ...)]
      )))

(define (unroll-log-message lsev lmsg lrest logmsg)
  (cond
   [(null? logmsg)
    (values lsev lmsg lrest)]
   [(eq? 'SEVERITY (caar logmsg))
    (unroll-log-message (cdar logmsg) lmsg lrest (cdr logmsg))]
   [(eq? 'MESSAGE (caar logmsg))
    (unroll-log-message lsev (cdar logmsg) lrest (cdr logmsg))]
   [(not (null? logmsg))
    (unroll-log-message lsev lmsg (cons (car logmsg) lrest) (cdr logmsg))]))

(define severity->string
  (match-lambda
    [(or 'EMERGENCY 0) "EMERG"]
    [(or 'ALERT     1) "ALERT"]
    [(or 'CRITICAL  2) "CRIT"]
    [(or 'ERROR     3) "ERROR"]
    [(or 'WARNING   4) "WARN"]
    [(or 'NOTICE    5) "NOTE"]
    [(or 'INFO      6) "INFO"]
    [(or 'DEBUG     7) "DEBUG"]))

(define LOG-LEVELS
  `(EMERGENCY ALERT CRITICAL ERROR WARNING NOTICE INFO DEBUG))

#|--
.. scm:procedure: (format-log-message outport log-sev log-msg log-props)
   :param <output-port> outport:
   :param (or <symbol> <number>) log-sev:
   :param <string> log-msg:
   :param <alist> log-props:

   Format a log message to the output port `outport` from its
   destructured parts: SRFI-215 severity (as symbol or number
   `log-sev`), the message text (as string `log-msg`), and any
   additional fields (as the alist `log-props`).  For example::

     (format-log-message (current-error-port)
       'INFO "Serving on http://localhost:8765/"
       `((URL . "http://localhost:8765/")))

       |- [2023-02-26T01:11:33+1100 INFO]  Serving on http://localhost:8765/
       |-          URL     http://localhost:8765/
--|#
(define (format-log-message outport log-sev log-msg log-props)
  (assume (and (output-port? outport) (output-port-open? outport)))
  (assume (or (memq log-sev LOG-LEVELS) (and (number? log-sev) (<= 0 log-sev 7))))
  (assume (string? log-msg))
  (assume (list? log-props))

  (format
   outport
   "[~a ~a]~/~a~%~:{~/~a~/~a~%~}"
   (date->string (current-date) "~4")
   (severity->string log-sev)
   log-msg
   log-props))

#|--
.. scm:procedure: (reasonable-log-callback logmsg)

   A reasonable implementation of a SRFI-215 logger façade callback.
--|#
(define (reasonable-log-callback logmsg)
  (define-values (log-sev log-msg log-props)
    (unroll-log-message #f #f `() logmsg))
  (format-log-message (current-error-port) log-sev log-msg log-props))

#|--
.. scm:procedure: (register-logger!)

   Configure the SRFI-215 logger façade for `reasonable-log-callback`.
--|#
(define (register-logger!)
  (current-log-callback reasonable-log-callback))

;;; jashankj.util.logger ends here.
