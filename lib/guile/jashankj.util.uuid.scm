;;; jashankj.util.uuid --- universally unique identifiers

;;; Commentary:
#|--

########################################################################
      ``(jashankj util uuid)`` --- universally unique identifiers
########################################################################

This uses libuuid, a part of the ext2fs program suite.

--|#
;;; Code:

(define-module (jashankj util uuid)
  #:use-module (guile) #:pure #:declarative? #t
  #:version (2023 04 29 0)
  #:export (
	uuid/generate
	uuid/generate-random
	uuid/generate-time

	uuid/parse

	uuid/unparse
	uuid/unparse-lower
	uuid/unparse-upper
  )

  #:use-module (oop goops)
  #:use-module (ice-9 format)
  #:use-module (rnrs bytevectors)
  #:use-module (system foreign)
  #:use-module (system foreign-library)
  #:use-module (srfi srfi-11) ;; let-values
  #:use-module (srfi srfi-19)   ;; time
)

(define **libuuid** (load-foreign-library "libuuid.so.1.3.0"))
(define (libuuid-> . xs)
  (apply foreign-library-function **libuuid** xs))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
typedef unsigned char uuid_t[16];
|#

(define (bytevector+pointer len)
  (let ((bv (make-bytevector len 0)))
    (values bv (bytevector->pointer bv))))

(define (make-uuid-bytevector)
  (bytevector+pointer 16))

(define-wrapped-pointer-type <uuid*>
  uuid*?
  uuid*/wrap
  uuid*/unwrap
  (lambda (self oport)
    (format
     oport
     "#<uuid:~x ~a>"
     (pointer-address (uuid*/unwrap self))
     (pointer->bytevector (uuid*/unwrap self) 16 0 'unsigned-char))))


#|
/* UUID Variant definitions */
#define UUID_VARIANT_NCS	0
#define UUID_VARIANT_DCE	1
#define UUID_VARIANT_MICROSOFT	2
#define UUID_VARIANT_OTHER	3

#define UUID_VARIANT_SHIFT	5
#define UUID_VARIANT_MASK     0x7
|#

(define *uuid-variant-ncs*       0)
(define *uuid-variant-dce*       1)
(define *uuid-variant-microsoft* 2)
(define *uuid-variant-other*     3)

(define *uuid-variant-shift*     5)
(define *uuid-variant-mask*    #x7)

#|
/* UUID Type definitions */
#define UUID_TYPE_DCE_NIL    0
#define UUID_TYPE_DCE_TIME   1
#define UUID_TYPE_DCE_SECURITY 2
#define UUID_TYPE_DCE_MD5    3
#define UUID_TYPE_DCE_RANDOM 4
#define UUID_TYPE_DCE_SHA1   5

#define UUID_TYPE_SHIFT      4
#define UUID_TYPE_MASK     0xf
|#

(define *uuid-type-dce-nil*      0)
(define *uuid-type-dce-time*     1)
(define *uuid-type-dce-security* 2)
(define *uuid-type-dce-md5*      3)
(define *uuid-type-dce-random*   4)
(define *uuid-type-dce-sha1*     5)

(define *uuid-type-shift*      4)
(define *uuid-type-mask*     #xf)


#|
#define UUID_STR_LEN 37
|#
(define *uuid-str-len* 37)

#|
/* Allow UUID constants to be defined */
#ifdef __GNUC__
#define UUID_DEFINE(name,u0,u1,u2,u3,u4,u5,u6,u7,u8,u9,u10,u11,u12,u13,u14,u15) \
	static const uuid_t name __attribute__ ((unused)) = {u0,u1,u2,u3,u4,u5,u6,u7,u8,u9,u10,u11,u12,u13,u14,u15}
#else
#define UUID_DEFINE(name,u0,u1,u2,u3,u4,u5,u6,u7,u8,u9,u10,u11,u12,u13,u14,u15) \
	static const uuid_t name = {u0,u1,u2,u3,u4,u5,u6,u7,u8,u9,u10,u11,u12,u13,u14,u15}
#endif
|#

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* clear.c */
extern void uuid_clear(uuid_t uu);
|#

(define %uuid_clear
  (libuuid-> "uuid_clear" #:arg-types '(*) #:return-type void))

(define (uuid*/clear self)
  (%uuid_clear (uuid*/unwrap self)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* compare.c */
extern int uuid_compare(const uuid_t uu1, const uuid_t uu2);
|#

(define %uuid_compare
  (libuuid-> "uuid_compare" #:arg-types '(* *) #:return-type int))

(define (uuid*/compare self other)
  (%uuid_compare (uuid*/unwrap self) (uuid*/unwrap other)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* copy.c */
extern void uuid_copy(uuid_t dst, const uuid_t src);
|#

(define %uuid_copy
  (libuuid-> "uuid_copy" #:arg-types '(* *) #:return-type void))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* gen_uuid.c */
extern void uuid_generate(uuid_t out);
extern void uuid_generate_random(uuid_t out);
extern void uuid_generate_time(uuid_t out);
extern int uuid_generate_time_safe(uuid_t out);

extern void uuid_generate_md5(uuid_t out, const uuid_t ns, const char *name, size_t len);
extern void uuid_generate_sha1(uuid_t out, const uuid_t ns, const char *name, size_t len);
|#

(define (uuid/generate-using f)
  (lambda ()
    (define-values (bv bv*) (make-uuid-bytevector))
    (f bv*)
    bv))

(define %uuid_generate
  (libuuid-> "uuid_generate" #:arg-types '(*) #:return-type void))
(define uuid/generate (uuid/generate-using %uuid_generate))

(define %uuid_generate_random
  (libuuid-> "uuid_generate_random" #:arg-types '(*) #:return-type void))
(define uuid/generate-random (uuid/generate-using %uuid_generate_random))

(define %uuid_generate_time
  (libuuid-> "uuid_generate_time" #:arg-types '(*) #:return-type void))
(define uuid/generate-time (uuid/generate-using %uuid_generate_time))

(define %uuid_generate_time_safe
  (libuuid-> "uuid_generate_time_safe" #:arg-types '(*) #:return-type int))
(define uuid/generate-time-safe (uuid/generate-using %uuid_generate_time_safe))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* isnull.c */
extern int uuid_is_null(const uuid_t uu);
|#

(define %uuid_is_null
  (libuuid-> "uuid_is_null" #:arg-types '(*) #:return-type int))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* parse.c */
extern int uuid_parse(const char *in, uuid_t uu);
extern int uuid_parse_range(const char *in_start, const char *in_end, uuid_t uu);
|#

(define %uuid_parse
  (libuuid-> "uuid_parse" #:arg-types '(* *) #:return-type int))

(define (uuid/parse str)
  (let* ([str* (string->pointer str)])
    (define-values (bv bv*) (make-uuid-bytevector))
    (case (%uuid_parse str* bv*)
      [(0)   bv]
      [else  #f])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* unparse.c */
extern void uuid_unparse(const uuid_t uu, char *out);
extern void uuid_unparse_lower(const uuid_t uu, char *out);
extern void uuid_unparse_upper(const uuid_t uu, char *out);
|#

(define (uuid/unparse-using f)
  (lambda (self)
    (let-values ([(strv str*) (bytevector+pointer *uuid-str-len*)])
      (f (bytevector->pointer self) str*)
      (utf8->string strv))))

(define %uuid_unparse
  (libuuid-> "uuid_unparse" #:arg-types '(* *) #:return-type void))
(define uuid/unparse (uuid/unparse-using %uuid_unparse))

(define %uuid_unparse_lower
  (libuuid-> "uuid_unparse_lower" #:arg-types '(* *) #:return-type void))
(define uuid/unparse-lower (uuid/unparse-using %uuid_unparse_lower))

(define %uuid_unparse_upper
  (libuuid-> "uuid_unparse_upper" #:arg-types '(* *) #:return-type void))
(define uuid/unparse-upper (uuid/unparse-using %uuid_unparse_upper))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* uuid_time.c */
extern time_t uuid_time(const uuid_t uu, struct timeval *ret_tv);
extern int uuid_type(const uuid_t uu);
extern int uuid_variant(const uuid_t uu);
|#

(define %uuid_time
  (libuuid-> "uuid_time" #:arg-types '(* *) #:return-type '*))

(define %uuid_type
  (libuuid-> "uuid_type" #:arg-types '(*) #:return-type int))

(define %uuid_variant
  (libuuid-> "uuid_variant" #:arg-types '(*) #:return-type int))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|
/* predefined.c */
extern const uuid_t *uuid_get_template(const char *alias);
|#

(define %uuid_get_template
  (libuuid-> "uuid_get_template" #:arg-types '(*) #:return-type '*))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.uuid ends here.
