;;; jashankj.util.stream --- improve SRFI-41 streams

;;; Commentary:
#|--

Stream manipulation functions, mainly curried.

--|#
;;; Code:

(define-module (jashankj util stream)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	stream:filter
	stream:fold
	stream:map
	stream:map-pair
	stream:push
	stream:unshift
	stream:zip-2
	stream:xrange

	input-port-lines->stream
	stream->output-port-lines
  )

  #:use-module (scheme write)
  #:use-module (rnrs io ports)
  #:use-module ((ice-9 ports) #:select (with-output-to-port))
  #:use-module (srfi srfi-41)   ;; streams
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define stream:filter       (lambda (f)   (lambda (s) (stream-filter f s))))
(define stream:fold         (lambda (f z) (lambda (s) (stream-fold f z s))))
(define stream:map          (lambda (f)   (lambda  x  (apply stream-map (cons f x)))))
(define stream:map-pair     (lambda (f)   (lambda (s) (stream-map (lambda (x) (cons x (f x))) s))))
(define stream:push         (lambda (it)  (lambda (s) (stream-append s (list->stream (list it))))))
(define stream:unshift      (lambda (it)  (lambda (s) (stream-append (list->stream (list it)) s))))
(define stream:zip-2        (lambda (a)   (lambda (b) (stream-zip a b))))
(define stream:xrange       stream-range)

#|--
.. scm:procedure: (input-port-lines->stream input-port)
   :param <input-port> input-port:
   :returns (of <stream> <string>):

   Lazily read every line on `input-port`, yielding a stream of strings.
   The strings are read with `get-line`; they do not have newlines.
--|#
(define-stream (input-port-lines->stream input-port)
  (let ([line (get-line input-port)])
    (if (eof-object? line)
        stream-null
        (stream-cons line (input-port-lines->stream input-port)))))

#|--
.. scm:procedure: (stream->output-port-lines stream output-port)
   :param (of <stream> <string>) stream:
   :param <output-port> output-port:

   For every string in `stream`, lazily print it, followed by a newline,
   to `output-port`.
--|#
(define (stream->output-port-lines stream output-port)
  (with-output-to-port output-port
    (lambda ()
      (stream-for-each
       (lambda (line)
         (display line)
         (newline))
       stream))))

;;; jashankj.util.stream ends here.
