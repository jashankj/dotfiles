;;; jashankj.util.number --- number manipulation functions

;;; Commentary:
#|--

Number manipulation functions.

--|#
;;; Code:

(define-module (jashankj util number)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	integer:as-chunks
	integer:mask-of-width
	number->hex
	number->HEX
  )

  #:use-module (rnrs arithmetic bitwise)
  #:use-module (srfi srfi-13)   ;; strings
  #:use-module (srfi srfi-26)   ;; cut
  #:use-module (srfi srfi-41)   ;; streams

  #:use-module (jashankj util procedure)
  #:use-module (jashankj util stream)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (integer:as-chunks value n-chunks chunk-width)
  (define (shifted w) (bitwise-arithmetic-shift-right value w))
  (define mask-value  (- (bitwise-arithmetic-shift-left 1 chunk-width) 1))
  (define (masked  n) (bitwise-and n mask-value))
  ((compose
    stream->list
    (stream:map masked)
    (stream:map shifted)
    (stream:map (lambda (i) (* chunk-width i))))
   (stream-range (- n-chunks 1) -1)))

(define (integer:mask-of-width k)
  "Generate a `k'-bit-wide mask from the LSB."
  (- (bitwise-arithmetic-shift-left 1 k) 1))

(define number->hex
  (cut number->string <> 16))
(define number->HEX
  (compose string-upcase number->hex))

;;; jashankj.util.number ends here.
