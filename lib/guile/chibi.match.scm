;; -*- mode: scheme; coding: utf-8; -*-

;;; Commentary:
#|--

Plumb the Wright-Cartwright-Shinn matcher into Guile.
This is mostly the same as guile/modules/ice-9/match.scm,
which is

   Copyright (c) 2010, 2011, 2012, 2020 Free Software Foundation, Inc.
   SPDX-License-Identifier: LGPL-3.0+

But, unlike upstream Guile, the upstream source *is* unmodified...

Compared to Andrew K. Wright's ``match``, this one lacks
``match-define``,
``match:error-control``,
``match:set-error-control``,
``match:error``,
``match:set-error``,
and all structure-related procedures.
Also, `match` doesn't support clauses of the form `(pat => exp)``.

--|#
;;; Code:

(define-module (chibi match)
  #:export (
	match
	match-lambda
	match-lambda*
	match-let
	match-let*
	match-letrec
  )
)

(define (error _ . args)
  (apply throw 'match-error "match" args))

#|--

.. note::

   the regular implementation in ``(ice-9 match)`` uses `struct-ref` and
   therefore only permits numeric references to structure slots --- this
   may be the only correct way to do this, but is certainly frustrating.
   can we abuse `record-accessor` and `record-modifier` to do this?

--|#

(define-syntax slot-ref
  (syntax-rules ()
    ((_ rtd rec n)
     ((record-accessor rtd n) rec))))

(define-syntax slot-set!
  (syntax-rules ()
    ((_ rtd rec n value)
     ((record-modifier rtd n) rec))))

(define-syntax is-a?
  (syntax-rules ()
    ((_ rec rtd)
     (and (struct? rec)
          (eq? (struct-vtable rec) rtd)))))

(include-from-path "chibi.match.impl.scm")
