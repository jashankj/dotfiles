;;;
;;; This file is mostly equal to `third_party/srfi-132/sorting/132.sld',
;;; but includes some tweaked and tuned oddments.
;;;

(define-library (srfi srfi-132)
  (export
   list-sorted?               vector-sorted?
   list-sort                  vector-sort
   list-stable-sort           vector-stable-sort
   list-sort!                 vector-sort!
   list-stable-sort!          vector-stable-sort!
   list-merge                 vector-merge
   list-merge!                vector-merge!
   list-delete-neighbor-dups  vector-delete-neighbor-dups
   list-delete-neighbor-dups! vector-delete-neighbor-dups!
   vector-find-median         vector-find-median!
   vector-select!             vector-separate!
   )

  (import
    (except (scheme base) vector-copy vector-copy!)
    (rename (only (scheme base) vector-copy vector-copy! vector-fill!)
            (vector-copy  r7rs-vector-copy)
            (vector-copy! r7rs-vector-copy!)
            (vector-fill! r7rs-vector-fill!))
    (scheme cxr)
    (only (srfi 27) random-integer))

  (cond-expand
    ((library (rnrs sorting))
     (import (rename (rnrs sorting)
                     (list-sort    r6rs-list-sort)
                     (vector-sort  r6rs-vector-sort)
                     (vector-sort! r6rs-vector-sort!))))
    (else))

  (cond-expand
    ((library (rnrs base))
     (import (only (rnrs base) assert)))
    (else
     (begin
       (define (assert x)
	 (if (not x)
             (error "assertion failure"))))))

  (import (only (guile) include-from-path))

  ;; If the (srfi 132 use-r6rs-sorting) library is defined above,
  ;; we'll use the (rnrs sorting) library for all sorting and trim
  ;; Olin's reference implementation to remove unnecessary code.
  ;; The merge.scm file, for example, extracts the list-merge,
  ;; list-merge!, vector-merge, and vector-merge! procedures from
  ;; Olin's lmsort.scm and vmsort.scm files.

  (cond-expand
    ((library (rnrs sorting))
     (begin
       (include-from-path "third_party/srfi-132/sorting/merge.scm")
       (include-from-path "third_party/srfi-132/sorting/delndups.scm")     ; list-delete-neighbor-dups etc
       (include-from-path "third_party/srfi-132/sorting/sortp.scm")        ; list-sorted?, vector-sorted?
       (include-from-path "third_party/srfi-132/sorting/vector-util.scm")
       (include-from-path "third_party/srfi-132/sorting/sortfaster.scm")))

    (else
     (begin
       (include-from-path "third_party/srfi-132/sorting/delndups.scm")     ; list-delete-neighbor-dups etc
       (include-from-path "third_party/srfi-132/sorting/lmsort.scm")       ; list-merge, list-merge!
       (include-from-path "third_party/srfi-132/sorting/sortp.scm")        ; list-sorted?, vector-sorted?
       (include-from-path "third_party/srfi-132/sorting/vector-util.scm")
       (include-from-path "third_party/srfi-132/sorting/vhsort.scm")
       (include-from-path "third_party/srfi-132/sorting/visort.scm")
       (include-from-path "third_party/srfi-132/sorting/vmsort.scm")       ; vector-merge, vector-merge!
       (include-from-path "third_party/srfi-132/sorting/vqsort2.scm")
       (include-from-path "third_party/srfi-132/sorting/vqsort3.scm")
       (include-from-path "third_party/srfi-132/sorting/sort.scm"))))

  (begin
    (include-from-path "third_party/srfi-132/sorting/select.scm"))

)
