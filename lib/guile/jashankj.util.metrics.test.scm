;;; jashankj.util.metrics.test --- tests for `jashankj.util.metrics'

;;; Code:

(define-module (jashankj util metrics test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util metrics)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.metrics.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "<label>"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "label!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "<metric>"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "metric/set!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "metric/inc!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "metric/dec!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "<metric-counter>"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "counter!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "<metric-gauge>"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "gauge!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "<metric-histogram>"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "histogram!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "<metric-summary>"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "summary!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "<metric-untyped>"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "untyped!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "render-metrics-to-string"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "GET//metrics"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "guile:export-metrics!"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.metrics.test ends here.
