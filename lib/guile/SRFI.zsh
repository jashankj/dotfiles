#!/usr/bin/env zsh

typeset -A known_srfis
known_srfis=(
#	0		1		2		3		.
#	4		5		6		7		.
#	8
#	12
#	16
#	20
#	24
#	28
#	32
#	36
#	40
#	44
#	48
#	52
#	56
#	60
#	64
#	68
#	72
#	76
#	80
#	84
			89	X
#	92
#	96
#	100
#					106	l
#	108
#	112
			113	x
#	116
#	120
			125	x	126	l
	128	l			130	x
	132	l
#	136
							143	x
			145	X	146	x
							151	x
#	152
					158	x
					162	X
							167	X
	168	x
			173	x
#	176
#	180
#	184
			189	x
#	192
#	196
#	200
#	204
#	208
							215	l
#	216
#	220
#	224
#	228
#	232
#	236
#	240
)

coalescable_srfis=()
extractable_srfis=()
symlinkable_srfis=()
for srfi in ${(kon)known_srfis}
do
	[ ${known_srfis[${srfi}]} = "x" ] && coalescable_srfis+=$srfi
	[ ${known_srfis[${srfi}]} = "X" ] && extractable_srfis+=$srfi
	[ ${known_srfis[${srfi}]} = "l" ] && symlinkable_srfis+=$srfi
done

function print-extract-supported()
{
	print "(define *supported* (list ${(on)coalescable_srfis} 162 167))"
}

function print-checkout-list()
{
	print "srfis=("
	sort -n <<< "${(kF)known_srfis}" | rs 0 8
	print ")"
}

function print-makefile-vars()
{
	print "srfis.coalescable\t:= ${(on)coalescable_srfis}"
	print "srfis.extractable\t:= ${(on)extractable_srfis}"
	print "srfis.symlinkable\t:= ${(on)symlinkable_srfis}"
}

case "${1?}" in
	(checkout-list)
		print-checkout-list ;;
	(extract-supported)
		print-extract-supported ;;
	(makefile-vars)
		print-makefile-vars ;;
esac
