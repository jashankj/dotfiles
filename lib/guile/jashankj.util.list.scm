;;; jashankj.util.list --- list manipulation functions

;;; Commentary:
#|--

List manipulation functions.

--|#
;;; Code:

(define-module (jashankj util list)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	cartesian-product
  )

  #:use-module (srfi srfi-1)    ;; lists
  #:use-module (srfi srfi-41)   ;; streams
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (cartesian-product xs ys)
  (stream-of (list x y)
             (x in xs)
             (y in ys)))

;;; jashankj.util.list ends here.
