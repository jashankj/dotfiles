;;; jashankj.util.string --- string manipulation functions

;;; Commentary:
#|--

String manipulation functions.

--|#
;;; Code:

(define-module (jashankj util string)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	gsub
	string:delete-prefix
	string:delete-suffix
	string:join
	string:join-pad-sep
	string:match-all
	string:squeeze
	string:unlines
  )

  #:use-module (srfi srfi-13)   ;; strings
  #:use-module (rx irregex)     ;; (srfi srfi-115)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define gsub
  (lambda (rx . f)
    (lambda (s)
      (apply irregex-replace/all `(,rx ,s ,@f)))))

(define (string:delete-prefix base head)
  (if (and (> (string-length head) 0)
           (string-prefix? head base))
      (string-drop base (string-length head))
      base))

(define (string:delete-suffix base tail)
  (if (and (> (string-length tail) 0)
           (string-suffix? tail base))
      (string-drop-right base (string-length tail))
      base))

(define (string:join sep)   (lambda (ls) (string-join ls sep)))

(define (string:join-pad-sep xs w sep)
  "Given a list of strings `xs', pad them to width `w' with zeroes,
interleave the separator `sep', and form a string of the result."
  (string-join (map (lambda (x) (string-pad x w #\0)) xs) sep))

(define (string:match-all irx str)
  (define (remainder m) (string-drop str (irregex-match-end-index m)))
  (let ([matched (irregex-search irx str 0)])
    (if matched
        (cons matched (string:match-all irx (remainder matched)))
        '())))

(define string:squeeze
  (gsub `(+ " ") " "))

(define (string:unlines . xs)
  (string-join xs "\n" 'suffix))

;;; jashankj.util.string ends here.
