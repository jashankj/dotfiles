;;; jashankj.util.socket --- abstraction over Guile's sockets

;;; Commentary:
#|--

########################################################################
    ``(jashankj util socket)`` --- abstraction over Guile's sockets
########################################################################

A (hopefully pleasing) abstraction over Guile sockets.  I assume Guile 3
on GNU/Linux, Android/Linux or FreeBSD system (because that's where I
care about these things working).

Most socket addressing aspects are in ``(jashankj util sockaddr)``.


Why not SRFI 106?
------------------------------------

This interface is, in fact, somewhat distantly inspired by SRFI 106,
though more of a reaction against its design foibles.  For example,
because we assume Guile, there are symbols that SRFI 106 doesn't export;
and we explicitly inherit a Berkeley sockets interface.

+ Socket domains ("address families", "protocol families"):

  SRFI 106 does not support protocol families (those symbols classically
  prefixed ``PF_``), but technically they should be here too.

  It would be very nice to support the various other common protocols
  supported, such as ``AF_IEEE80211``, ``AF_BLUETOOTH``, ``AF_NATM``,
  but these aren't passed through by Guile, and may have very exciting
  portability problems.

+ :man:`getaddrinfo(3)` support:

  Removed.  I don't much like the way this is exposed from C into
  Scheme (and would have been way more excited if SRFI 106 got this
  *right*).  I also don't like it being exposed *here*, and would
  suggest it should be moved out into its own module.

+ Constructors:

  As noted in a discussion on the SRFI 106 list, "client" and "server"
  sockets are a weird way to express this.  Berkeley sockets have ...
  well, just plain ol' sockets; and only differentiate them by where
  they came from.

  I went looking at SRFI 106 implementations: reading through Ypsilon's
  implementation yielded that there were very ill-specified behaviours.
  This seems quite unnerving; my stance here is that we at least *know*
  Berkeley sockets are cursed, instead of finding the implementation is
  setting who knows what socket options.


Compatibility with SRFI 106
------------------------------------

The following are **exactly equivalent to** those defined in SRFI 106.

+ Message types:

  + `message-type`;
  + `*msg-peek*`;
  + `*msg-oob*`;

+ Shutdown methods:

  + `shutdown-method`;
  + `*shut-rd*`;
  + `*shut-wr*`;
  + `*shut-rdwr*`;


Incompatibility with SRFI 106
------------------------------------

The following are all divergences from SRFI 106.

+ Socket flags:

  + added `*sock-cloexec*`:
    set close-on-exec (from ``SOCK_CLOEXEC``);

  + added `*sock-nonblock*`:
    set non-blocking i/o (from ``SOCK_NONBLOCK``);

+ Constructors:

  + removed `make-client-socket`, `make-server-socket`;
  + added `socket/make', shimming :man:`socket(2)`.

+ Socket operations:

  + `socket-accept`, shimming :man:`accept(2)`, is spelled `socket/accept`;

  + `socket-send`, shimming :man:`send(2)`, is spelled `socket/send`;

  + `socket-recv`, shimming :man:`recv(2)`, is spelled `socket/recv`
    (and uses SRFI 106's very reasonable "provide length" behaviour, not
    Guile's extremely-raw-bits "provide receive buffer" interface leak);

  + `socket-shutdown`, shimming :man:`shutdown(2)`, is spelled `socket/shutdown`;

  + `socket-close`, shimming :man:`close(2)`, is spelled `socket/close`;

+ Port conversion:

  + removed `socket-input-port`, `socket-output-port`:
    Guile's sockets are, by default, "binary input/output ports".

+ Control feature:

  + not implemented: `call-with-port` (mainly because I haven't seen its
    value ... yet)

+ Flag operations:

  + `address-family` is spelled `socket-domain`, and also understands
    ``unix``;

  + `socket-domain` is spelled `socket-family`, and also understands
    names ``dgram`` (as an alternate spelling of ``datagram``), ``raw``,
    ``rdm``, ``seqpacket``

  + `ip-protocol` is spelled `socket-protocol`, and also understands
    ``ipv6``;


Features matching POSIX
------------------------------------

+ Socket options:

  + `*so-acceptconn*`;
  + `*so-broadcast*`;
  + `*so-debug*`;
  + `*so-dontroute*`;
  + `*so-error*`;
  + `*so-keepalive*`;
  + `*so-linger*`;
  + `*so-oobinline*`;
  + `*so-rcvbuf*`;
  + `*so-rcvlowat*`;
  + `*so-rcvtimeo*`;
  + `*so-reuseaddr*`;
  + `*so-reuseport*`;
  + `*so-sndbuf*`;
  + `*so-sndlowat*`;
  + `*so-sndtimeo*`;
  + `*so-style*`;
  + `*so-type*`;

--|#
;;; Code:

(define-module (jashankj util socket)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:re-export (
	*sock-cloexec* *sock-nonblock*
	#;*msg-ctrunc* #;*msg-dontroute* *msg-dontwait* #;*msg-eor*
	*msg-oob* #;*msg-nosignal* *msg-peek* #;*msg-trunc*
	#;*msg-waitall*

	*sol-socket*
	#;*so-acceptconn* *so-broadcast* *so-debug* *so-dontroute*
	*so-error* *so-keepalive* *so-linger* *so-oobinline* *so-rcvbuf*
        #;*so-rcvlowat* #;*so-rcvtimeo* *so-reuseaddr* #;*so-reuseport*
	*so-sndbuf* #;*so-sndlowat* #;*so-sndtimeo* #;*so-style* *so-type*

#;	*tcp-nodelay*
#;	*tcp-cork*
  )

  #:export (
	message-type

	shutdown-method *shut-rd* *shut-wr* *shut-rdwr*

	sockopt

	socket/close
	socket/make
	socket/get-option
	socket/set-option!
	socket/shutdown
	socket/connect
	socket/bind
	socket/listen
	socket/accept
	socket/get-name
	socket/get-peer-name
	socket/recv
	socket/send
;;	socket/recvfrom
	socket/sendto
  )

  #:use-module
    ((guile)
     #:prefix |guile:|
     #:select
     ( accept bind close connect getpeername getsockname getsockopt
       listen recv! recvfrom! send sendto setsockopt shutdown socket ))

  #:use-module
    ((guile)
     #:select (
	(SOCK_CLOEXEC   . *sock-cloexec*)
	(SOCK_NONBLOCK  . *sock-nonblock*)

#;	(MSG_CTRUNC    . *msg-ctrunc*)
#;	(MSG_DONTROUTE . *msg-dontroute*)
#;	(MSG_EOR       . *msg-eor*)
	(MSG_OOB       . *msg-oob*)
#;	(MSG_NOSIGNAL  . *msg-nosignal*)
	(MSG_PEEK      . *msg-peek*)
#;	(MSG_TRUNC     . *msg-trunc*)
#;	(MSG_WAITALL   . *msg-waitall*)
	(MSG_DONTWAIT  . *msg-dontwait*)

#;	(SHUT_RD   . *shut-rd*)
#;	(SHUT_WR   . *shut-wr*)
#;	(SHUT_RDWR . *shut-rdwr*)

	(SOL_SOCKET    . *sol-socket*)
#;	(SO_ACCEPTCONN . *so-acceptconn*) ;; posix !guile
	(SO_BROADCAST  . *so-broadcast*)
	(SO_DEBUG      . *so-debug*)
	(SO_DONTROUTE  . *so-dontroute*)
	(SO_ERROR      . *so-error*)
	(SO_KEEPALIVE  . *so-keepalive*)
	(SO_LINGER     . *so-linger*)
	(SO_OOBINLINE  . *so-oobinline*)
	(SO_RCVBUF     . *so-rcvbuf*)
#;	(SO_RCVLOWAT   . *so-rcvlowat*) ;; posix !guile
#;	(SO_RCVTIMEO   . *so-rcvtimeo*)
	(SO_REUSEADDR  . *so-reuseaddr*)
#;	(SO_REUSEPORT  . *so-reuseport*) ;; linux
	(SO_SNDBUF     . *so-sndbuf*)
#;	(SO_SNDLOWAT   . *so-sndlowat*) ;; posix !guile
#;	(SO_SNDTIMEO   . *so-sndtimeo*)
#;	(SO_STYLE      . *so-style*) ;; guile
	(SO_TYPE       . *so-type*)
  ))

  ;; For `assertion-violation'.  (We could probably lose that.)
  #:use-module ((rnrs base) #:select (assertion-violation))

  ;; is there a better formatting library (like SRFI 48 but more)?
  ;; SRFI 166 would be nice here, if it weren't a namespace polluter.
  #:use-module (ice-9 format)

  ;; In practice, this could just be SRFI 34 and SRFI 35 calls.
  #:use-module (jashankj util error)

  ;; For bitwise-arithmetic operators.
  #:use-module (srfi srfi-60)

  ;; For keyword arguments, supplanting (ice-9 optargs):
  #:use-module ((srfi srfi-89) #:select (lambda* (define* . define+)))

  ;; For proper inhabited/uninhabited value support.
  #:use-module (srfi srfi-189)  ;; either-maybe

  ;; Sockets require awareness of socket-addressing.
  #:use-module (jashankj util sockaddr)

)

(define (lookup who sets name)
  (cond [(assq name sets) => cdr]
        [else (assertion-violation who "no name defined" name)]))

(define (lookup-many who sets . names)
  (define (lookup* name) (lookup who sets name))
  (apply bitwise-ior (map lookup* names)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define %supported-message-types `(
#;	[ctrunc      . ,*msg-ctrunc*]
#;	[no-route    . ,*msg-dontroute*]
#;	[eor         . ,*msg-eor*]
	[oob         . ,*msg-oob*]
#;	[no-signal   . ,*msg-nosignal*]
	[peek        . ,*msg-peek*]
#;	[trunc       . ,*msg-trunc*]
#;	[wait-all    . ,*msg-waitall*]
	[no-wait     . ,*msg-dontwait*]
))

(define-syntax message-type
  (syntax-rules ()
    ((_ names ...)
     (apply lookup-many
            'message-type
            %supported-message-types
            '(names ...)))))

(define (check:message-type msgty)
  (unless (memq msgty (map car %supported-message-types))
    (croak (¡error!)
           (¡message! "invalid message-type")
           (¡irritants! msgty))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define %supported-socket-options `(
#;	[(socket accept-conn)    . (,*sol-socket* . ,*so-acceptconn*)]
	[(socket broadcast)      . (,*sol-socket* . ,*so-broadcast*)]
	[(socket debug)          . (,*sol-socket* . ,*so-debug*)]
	[(socket no-route)       . (,*sol-socket* . ,*so-dontroute*)]
	[(socket error)          . (,*sol-socket* . ,*so-error*)]
	[(socket keepalive)      . (,*sol-socket* . ,*so-keepalive*)]
	[(socket linger)         . (,*sol-socket* . ,*so-linger*)]
	[(socket oob-inline)     . (,*sol-socket* . ,*so-oobinline*)]
	[(socket recv-buf)       . (,*sol-socket* . ,*so-rcvbuf*)]
#;	[(socket recv-low-water) . (,*sol-socket* . ,*so-rcvlowat*)]
#;	[(socket recv-timeout)   . (,*sol-socket* . ,*so-rcvtimeo*)]
	[(socket reuse-addr)     . (,*sol-socket* . ,*so-reuseaddr*)]
#;	[(socket reuse-port)     . (,*sol-socket* . ,*so-reuseport*)]
	[(socket send-buf)       . (,*sol-socket* . ,*so-sndbuf*)]
#;	[(socket send-low-water) . (,*sol-socket* . ,*so-sndlowat*)]
#;	[(socket send-timeout)   . (,*sol-socket* . ,*so-sndtimeo*)]
#;	[(socket style)          . (,*sol-socket* . ,*so-style*)]
	[(socket type)           . (,*sol-socket* . ,*so-type*)]

#;	[(tcp no-delay)          . (,*ipproto-tcp* . ,*tcp-nodelay*)]
#;	[(tcp cork)              . (,*ipproto-tcp* . ,*tcp-cork*)]
))

(define-syntax sockopt
  (syntax-rules ()
    ((_ opt-level opt-name)
     (lookup 'socket-options
             %supported-socket-options
             '(opt-level opt-name)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-syntax shutdown-method
  (syntax-rules (read write)
    [(_ read)  *shut-rd*]
    [(_ write) *shut-wr*]))

(define *shut-rd*   0)
(define *shut-wr*   1)
(define *shut-rdwr* 2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; libguile/filesys.c:

(define (socket/close socket)
  (guile:close socket))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; libguile/socket.c:

(define (socket/make domain family protocol)
  (guile:socket domain family protocol))

;; guile: socketpair

;; guile: getsockopt

(define+ (socket/get-option socket option-level option-name)
  (guile:getsockopt socket option-level option-name))

;; (socket/set-option! SOCKET (OPTLEVEL OPTNAME) OPTVALUE)
(define+ (socket/set-option! socket option option-value)
  (unless (and (pair? option)
               (number? (car option))
               (number? (cdr option)))
    (croak (¡error!)
           (¡message! "invalid socket-option format")
           (¡irritants! option)))
  (define option-level (car option))
  (define option-name  (cdr option))
  (guile:setsockopt socket option-level option-name option-value))

(define (socket/shutdown socket how)
  (guile:shutdown socket how))

(define+ (socket/connect socket sockaddr)
  (guile:connect socket sockaddr))

(define+ (socket/bind socket sockaddr)
  (guile:bind socket sockaddr))

(define+ (socket/listen socket backlog)
  (guile:listen socket backlog))

;; guile: make-socket-address   ==>   (jashankj util sockaddr)

(define+ (socket/accept socket (flags (nothing)))
  (guile:accept socket (if (nothing? flags) 0 flags)))

(define+ (socket/get-name socket)
  (guile:getsockname socket))

(define+ (socket/get-peer-name socket)
  (guile:getpeername socket))

(define+ (socket/recv socket n-bytes (flags (nothing)))
  (let ([buf (make-bytevector n-bytes)])
    (values (guile:recv! socket buf (if (nothing? flags) 0 flags)) buf)))

(define+ (socket/send socket bytev (flags (nothing)))
  (guile:send socket bytev (if (nothing? flags) 0 flags)))

;; guile: recvfrom!

(define+ (socket/sendto socket message sockaddr (flags (nothing)))
  (apply guile:sendto
         `(,socket ,message ,@sockaddr ,(if (nothing? flags) 0 flags))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.socket ends here.
