;;; jashankj.util.dict.test --- tests for `jashankj.util.dict'.

;;; Code:

(define-module (jashankj util dict test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util dict)

  #:use-module (jashankj util procedure)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.dict.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "alist-get"

  (test-equal    #f    (alist-get `() 'a))
  (test-equal '(a . b) (alist-get `((a . b)) 'a))
  (test-equal    #f    (alist-get `((a . b)) 'b))
  (test-equal '(a . b) (alist-get `((a . b) (a . c)) 'a))
  (test-equal '(a . c) (alist-get `((b . b) (a . c)) 'a))
  (test-equal '(b . b) (alist-get `((b . b) (a . c)) 'b))

  (test-equal '("a" . 0) (alist-get `(("a" . 0) (a . 1)) "a"))
  (test-equal '( a  . 1) (alist-get `(("a" . 0) (a . 1)) 'a))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "assoc:ref"

  (test-equal #f (assoc:ref `() 'a))
  (test-equal 'b (assoc:ref `((a . b)) 'a))
  (test-equal #f (assoc:ref `((a . b)) 'b))
  (test-equal 'b (assoc:ref `((a . b) (a . c)) 'a))
  (test-equal 'c (assoc:ref `((b . b) (a . c)) 'a))
  (test-equal 'b (assoc:ref `((b . b) (a . c)) 'b))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "assoc:walk"

  (test-equal
      `()
    (assoc:walk `() identity))

  (test-equal
      `((a . b))
    (assoc:walk `((a . b)) cons))

  (test-equal
      `((a . b) (c . d))
    (assoc:walk `((a . b) (c . d)) cons))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "dict-ref"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "hash-table:merge"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "plist->alist"

  (test-equal '() (plist->alist '()))

  (test-equal
      '((#:k . v))
    (plist->alist '(#:k v)))

  (test-equal
      '((#:k . v)
        (#:l . w))
    (plist->alist '(#:k v #:l w)))

)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.dict.test ends here.
