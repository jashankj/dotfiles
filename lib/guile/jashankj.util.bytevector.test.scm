;;; jashankj.util.bytevector.test --- tests for `jashankj.util.bytevector'

;;; Code:

(define-module (jashankj util bytevector test)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:use-module (srfi srfi-64)   ;; testing
  #:use-module (jashankj util bytevector)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "jashankj.util.bytevector.test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-group "bytevector-hexdump"
  #t
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)

;;; jashankj.util.bytevector.test ends here.
