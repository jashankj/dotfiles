;;; jashankj.bin.gitf --- fetch a Git repository

;;; Commentary:
;; #|--

;; Keep a local bare Git mirror of a repository in ``/src/remote`` and use
;; it as a ``--shared`` source for clones; this makes it easier to find
;; (and cache) Git repositories I use or am interested in, but in which I
;; am not actively developing.

;; --|#
;;; Code:

(define-module (jashankj bin gitf)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2024 01 13 1)

  #:use-module (srfi srfi-26)	;; cut

  #:use-module ((guile)
		#:select
		( define-macro unspecified?
		  exit EXIT_FAILURE system*
		  stat stat:type file-exists?
		  dirname basename canonicalize-path
		  getcwd mkdir open-file))
  #:use-module (ice-9 format)
  #:use-module ((ice-9 ports) #:select (with-output-to-port))
  #:use-module (oop goops)
  #:use-module (web uri)

  #:use-module (chibi match)	;; (ice-9 match) ;; (srfi srfi-204) ;; match-wcs
  #:use-module (rx irregex)	;; (srfi srfi-115)
  #:use-module (jashankj util error)
  #:use-module (jashankj util procedure)
  #:use-module (jashankj util string)

  #:export (
	main
	explode-url
	remote-repo
	remote-user
	remote-origin
	remote-localdir
	remote-wcdir

	remote-clone-command
	remote-fetch-command
	local-clone-command
	checkout-path
	checkout-file-of
  )
)

(define-match main
  [(or ((argv0 url)) ((argv0 url dir)))
   (clone-or-fetch url dir)]

  [_
   (format #t "usage: gitf <url> [<dir>]~&")
   (exit EXIT_FAILURE)])

(define $REPO_BASE "/src/remote")



(define (system! . argv)
  (format #t ":~{ ~a~}~&" argv)
  (apply system* argv))

(define (file-directory? path)
  (eq? 'directory (stat:type (stat path))))

(define (clone-or-fetch url dir)
  (let* ([remote     (explode-url url)]
	 [origin     (remote-origin remote)]
	 [localdir   (remote-localdir remote)]
	 [wcdir      (if (not (unspecified? dir)) dir
			 (remote-wcdir remote))]
	 [repo-local (format #f "~a/~a.git" $REPO_BASE localdir)])
    ;; (format #t "remote:     ~s~&" remote)
    ;; (format #t "origin:     ~s~&" origin)
    ;; (format #t "localdir:   ~s~&" localdir)
    ;; (format #t "wcdir:      ~s~&" wcdir)
    ;; (format #t "repo-local: ~s~&" repo-local)
    (if (not (and (file-exists? repo-local) (file-directory? repo-local)))
	(do-remote-clone! remote repo-local)
	(do-remote-fetch! remote repo-local))
    (do-local-clone! remote repo-local origin wcdir)
    (record-checkout! repo-local wcdir)
  )
)

(define (remote-clone-command remote repo-local)
  `("git" "clone"
    "--bare"
    "--mirror"
    ,(uri->string (remote-uri remote))
    ,repo-local))

(define (make-path path)
  ;; (format #t "~s~&" path)
  (cond
   [(string=? "/" path)  #t]
   [(and (file-exists? path) (file-directory? path))  #t]
   [(not (file-exists? path))
    (make-path (dirname path))
    (mkdir path)]
   [else
    (croak (¡error!)
	   (¡message! "path exists, but is not a directory")
	   (¡irritants! path))]))

(define (do-remote-clone! remote repo-local)
  (make-path repo-local)
  (apply system! (remote-clone-command remote repo-local)))


(define (remote-fetch-command remote repo-local)
  `("git" "-C" ,repo-local "fetch"))

(define (do-remote-fetch! remote repo-local)
  (apply system! (remote-fetch-command remote repo-local)))

(define (local-clone-command remote repo-local origin wcdir)
  `("git" "clone"
    "--reference-if-able" ,repo-local
    "--shared"
    "--origin" ,origin
    ,(uri->string (remote-uri remote))
    ,wcdir))

(define (do-local-clone! remote repo-local origin wcdir)
  (apply system! (local-clone-command remote repo-local origin wcdir)))

(define (checkout-path wcdir)
  (canonicalize-path (string-append (getcwd) "/" wcdir)))
(define (checkout-file-of repo-local)
  (string-append repo-local "/.checkouts"))

(define (record-checkout! repo-local wcdir)
  (with-output-to-port (open-file (checkout-file-of repo-local) "a")
    (lambda () (format #t "~a~&" (checkout-path wcdir)))))



(define (forbidden-slot-set! self val) (croak (¡immutable-object! val)))

(define-class <remote> ()
  (uri
	#:init-keyword	#:uri)
  (uri-path
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref
	(compose
	 split-and-decode-uri-path
	 uri-path
	 (cut slot-ref <> 'uri)))

  (origin-prefix)
  (localdir-prefix)

  (repo)
  (origin)
  (localdir)

  (wcdir
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref	(compose basename (cut slot-ref <> 'localdir)))
)

(define-method (remote-uri      (self <remote>)) (slot-ref self 'uri))
(define-method (remote-repo     (self <remote>)) (slot-ref self 'repo))
(define-method (remote-origin   (self <remote>)) (slot-ref self 'origin))
(define-method (remote-localdir (self <remote>)) (slot-ref self 'localdir))
(define-method (remote-wcdir    (self <remote>)) (slot-ref self 'wcdir))

(define-method (remote-origin-prefix (self <remote>))
  (slot-ref self 'origin-prefix))
(define-method (remote-localdir-prefix (self <remote>))
  (slot-ref self 'localdir-prefix))

(define (slot-ref-user-from-path n)
  (compose
   (cut string:delete-prefix <> "~")
   (cut list-ref <> n)
   (cut slot-ref <> 'uri-path)))

(define (slot-ref-repo-from-path n)
  (compose
   (cut string:delete-suffix <> ".git")
   (cut list-ref <> n)
   (cut slot-ref <> 'uri-path)))



(define-class <remote:forge> (<remote>)
  (user
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref	(slot-ref-user-from-path 0))

  (repo
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref	(slot-ref-repo-from-path 1))

  (origin
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref
	(lambda (self)
	  (format #f "~a-~a"
		  (slot-ref self 'origin-prefix)
		  (slot-ref self 'user))))

  (localdir
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref
	(lambda (self)
	  (format #f "_~a/~a/~a"
		  (slot-ref self 'localdir-prefix)
		  (slot-ref self 'user)
		  (slot-ref self 'repo))))
)

(define-method (remote-user (self <remote:forge>)) (slot-ref self 'user))

(define-macro (define-forge-remote name oprefix ldprefix)
  `(define-class
     ,(string->symbol (format #f "<remote:forge:~a>" name))
     (<remote:forge>)
     (origin-prefix	#:init-value	,oprefix)
     (localdir-prefix	#:init-value    ,ldprefix)))



(define-class <remote:cgit> (<remote>)
  (host
	#:init-keyword	#:host)
  (repo
	#:init-keyword	#:repo)

  (origin
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref	(cut slot-ref <> 'host))

  (localdir
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref
	(lambda (self)
	  (format #f "_~a/~a"
		  (slot-ref self 'host)
		  (slot-ref self 'repo))))
)

(define-macro (define-cgit-remote name nslot)
  `(define-class
     ,(string->symbol (format #f "<remote:cgit:~a>" name))
     (<remote:cgit>)
     (host
	#:init-value	,(symbol->string name))
     (repo
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref	(slot-ref-repo-from-path ,nslot))))



(define-class <remote:gnusv> (<remote>)
  (origin	#:init-value "gnusv")

  (repo
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref
	(lambda (self)
	  ((compose
	    (cut string:delete-suffix <> ".git")
	    (string:join "/")
	    (case (uri-scheme (slot-ref self 'uri))
	      ;; git://git.savannah.gnu.org/gnulib.git
	      [(git)   identity]
	      ;; http://git.savannah.gnu.org/git/gnulib.git
	      [(http)  cdr]
	      [(https) cdr]
	      ;; ssh://git.savannah.gnu.org/srv/git/gnulib.git
	      [(ssh)   cddr])
	    (cut slot-ref <> 'uri-path))
	   self)))

  (localdir
	#:allocation	#:virtual
	#:slot-set!	forbidden-slot-set!
	#:slot-ref
	(lambda (self)
	  (format #f "_~a/~a"
		  (slot-ref self 'origin)
		  (slot-ref self 'repo))))
)



(define-forge-remote github   "gh"    "github")
(define-forge-remote gitlab   "gl"    "gitlab")
(define-forge-remote glgnome  "gnome" "gnome")
(define-forge-remote glfdo    "fdo"   "fdo")
(define-forge-remote srht     "srht"  "srht")
(define-forge-remote codeberg "cb"    "codeberg")

(define-cgit-remote zx2c4      0)
(define-cgit-remote orcz       0)
(define-cgit-remote sourceware 1)

(define (explode-url url)
  (let ([uri (string->uri url)])
    (match (uri-host uri)
      ["github.com"             (make <remote:forge:github>    #:uri uri)]
      ["gitlab.com"             (make <remote:forge:gitlab>    #:uri uri)]
      ["gitlab.gnome.org"       (make <remote:forge:glgnome>   #:uri uri)]
      ["gitlab.freedesktop.org" (make <remote:forge:glfdo>     #:uri uri)]
      ["git.sr.ht"              (make <remote:forge:srht>      #:uri uri)]
      ["codeberg.org"           (make <remote:forge:codeberg>  #:uri uri)]
      ["git.zx2c4.com"          (make <remote:cgit:zx2c4>      #:uri uri)]
      ["repo.or.cz"             (make <remote:cgit:orcz>       #:uri uri)]
      ["sourceware.org"         (make <remote:cgit:sourceware> #:uri uri)]
      [(or "git.savannah.gnu.org"
	   "git.sv.gnu.org")    (make <remote:gnusv>           #:uri uri)]
      [else
       (croak (¡restriction!)
	      (¡message! "unsupported remote URL")
	      (¡irritants! url))])))



;;; jashankj.bin.gitf ends here.
