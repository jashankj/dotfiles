;;; jashankj.util.dict --- dictionary manipulation functions

;;; Commentary:
#|--

Dictionary manipulation functions.

--|#
;;; Code:

(define-module (jashankj util dict)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 04 29 0)
  #:export (
	alist-get
	assoc:ref
	assoc:walk
	dict-ref
	plist->alist
    	hash-table:merge
  )

  #:use-module ((guile) #:select (and=> const keyword?))
  #:use-module (ice-9 format)

  #:use-module (srfi srfi-1)    ;; lists
  #:use-module (srfi srfi-69)   ;; basic-hash-tables
  #:use-module (srfi srfi-89)   ;; define*
  #:use-module (srfi srfi-128)  ;; comparators
  #:use-module (srfi srfi-189)  ;; either-maybe

  #:use-module (chibi match)    ;; (ice-9 match) ;; (srfi srfi-204) ;; match-wcs
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (DEBUG . xs) #t)
;; (apply format `(#t ,@xs)))

(define* (alist-get
	  alist
	  key
	  (cmp (make-default-comparator))
	  (fail (const #f)))
  (DEBUG "alist-get: alist: {~a}~%" alist)
  (DEBUG "~/key: {~a}~%" key)
  (DEBUG "~/cmp: {~a}~%" cmp)
  (DEBUG "~/fail: {~a}~%" fail)
  (define (find ax)
    (DEBUG "alist-get: find: {~a}, have {~a}~%" key ax)
    (cond
     [(and (list? ax)
	   (not (null? ax))
	   (pair? (car ax))
	   (=? cmp (car (car ax)) key))
      (DEBUG "alist-get: find: got! here is {~a}~%" (car ax))
      (car ax)]

     [(and (list? ax)
	   (not (null? ax))
	   (pair? (car ax)))
      (DEBUG "alist-get: find: not got, next~%")
      (find (cdr ax))]

     [else
      (DEBUG "alist-get: find: fail~%")
      (fail)]))
  (find alist))

(define (assoc:ref alist key)
  "Behaves like `assq-ref', but uses `equal?' for key comparison."
  (and=> (alist-get alist key (make-equal-comparator)) cdr))

(define (assoc:walk alist fn)
  (map (lambda (x) (let ([k (car x)] [v (cdr x)]) (fn k v))) alist))

(define* (dict-ref dict key (#:fail fail (const #f)))
  (let ((ref (assq key dict)))
    (if (eq? ref #f)
        (if (procedure? fail) (fail) fail)
        (cdr ref))))

(define (hash-table:merge . xs)
  "Merge hash tables listed in `XS'.  Mappings in later lists take
precedence over mappings in earlier lists."
  (define (merge k v acc) (begin (hash-table-set! acc k v) acc))
  (fold (lambda (ht acc) (hash-table-fold ht merge acc))
        (make-hash-table)
        xs))

(define plist->alist
  (match-lambda
    [`() `()]
    [((? keyword? k) v rest ...)
     (cons (cons k v) (plist->alist rest))]))

;;; jashankj.util.dict ends here.
