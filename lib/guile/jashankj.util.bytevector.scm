;;; jashankj.util.bytevector --- bytevector helpers

;;; Commentary:
#|--

########################################################################
         ``(jashankj util bytevector)`` --- bytevector helpers
########################################################################

--|#
;;; Code:

(define-module (jashankj util bytevector)
  #:use-module (scheme base) #:pure #:declarative? #t
  #:version (2023 03 23 0)
  #:export (
	bytevector-hexdump
  )

  #:use-module (rnrs bytevectors)
; #:use-module (rnrs bytevectors gnu)
  #:use-module (ice-9 format)
  #:use-module ((ice-9 ports) #:select (with-output-to-string))

  #:use-module (srfi srfi-130)  ;; string-cursors

  #:use-module (jashankj util error)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (bytevector-hexdump bv)
  (define *slicew* 16)
  (define bvlen    (bytevector-length bv))

  (define (bytevector-hexdump-one offset bv*)
    (when (> (bytevector-length bv*) 16)
      (croak (¡restriction!)
             (¡message! "bytevector length must be 0 <= x <= 16")
             (¡irritants! bv*)))

    (let ([xs (bytevector->u8-list bv*)])
      (format #t "~8,'0x ~a  |~a|~%"
              offset
              (string-pad-right (format #f "~{ ~2,'0x~}" xs) 48 #\space)
              (apply string (map
                             (lambda (x) (if (<= #x20 x #x7e)
                                        (integer->char x) #\.))
                             xs)))))
  (with-output-to-string
    (lambda ()
      (let roll ([n 0])
        (let* ([offset    (* n *slicew*)]
               [remaining (max 0 (- bvlen offset))])
          (when (<= offset bvlen)
            (bytevector-hexdump-one
             offset
             (bytevector-slice bv offset (min *slicew* remaining))))
          (unless (>= offset bvlen)
            (roll (+ n 1))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; jashankj.util.bytevector ends here.
