{-|
Module:		USock
Description:	Utilities for shell scripts that need sockets.
Stability:	experimental
Portability:	*nix

-}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE NumericUnderscores #-}

module USock
  ( withUnixSocketClient
  , withUnixSocketServer
  , withUnixSocket
  , withUnixSocketAddr, fromUnixSocketAddr
  , withSocketAt
  , serve
  , handleFromUnixPeer
  , getPeerCredential
  )
where

{-& build-depends: base -}
import           Control.Concurrent (ThreadId, forkFinally)
import           Control.Monad (forever)
import qualified Control.Exception as E
import           System.Posix.Types (GroupID, ProcessID, UserID)
{-& build-depends: network -}
import qualified Network.Socket as S

------------------------------------------------------------------------

-- | Connect to a UNIX-domain 'S.Socket' at 'path'; pass to 'continue'.
--   The socket is only guaranteed to be live for 'continue's call.
withUnixSocketClient :: FilePath -> (S.Socket -> IO a) -> IO a
withUnixSocketClient path continue =
  S.withSocketsDo $ withUnixSocket $ \socket -> do
    S.connect socket $ withUnixSocketAddr path
    continue socket

-- | Start a UNIX-domain S.Socket server at 'path'; pass to 'continue'.
--   (The socket is only guaranteed to be live for 'continue's call.)
withUnixSocketServer :: FilePath -> (S.Socket -> IO a) -> IO a
withUnixSocketServer path continue =
  S.withSocketsDo $ withUnixSocket $ \socket -> do
    S.bind socket $ withUnixSocketAddr path
    S.listen socket _nConnections
    continue socket

_nConnections :: Int
_nConnections = 4

-- | Create a UNIX-domain (i.e., 'S.AF_UNIX') 'S.Socket'.
--   The socket is only guaranteed to be live for the inner call.
withUnixSocket :: (S.Socket -> IO a) -> IO a
withUnixSocket = withSocket $ S.socket S.AF_UNIX S.Stream S.defaultProtocol

-- | Make this FilePath a UNIX-domain socket address.
withUnixSocketAddr :: FilePath -> S.SockAddr
withUnixSocketAddr = S.SockAddrUnix

-- | Maybe get a FilePath from a UNIX-domain socket address.
fromUnixSocketAddr :: S.SockAddr -> Maybe FilePath
fromUnixSocketAddr (S.SockAddrUnix fp) = Just fp
fromUnixSocketAddr _                   = Nothing

-- | Create a 'S.Socket' pointed at the provided 'S.AddrInfo'.
--   The socket is only guaranteed to be live for the inner call.
withSocketAt :: S.AddrInfo -> (S.Socket -> IO a) -> IO a
withSocketAt ai = S.openSocket ai `E.bracket` S.close

-- | Create a 'S.Socket'; ensure it is closed after some inner call.
withSocket :: IO S.Socket -> (S.Socket -> IO a) -> IO a
withSocket plumb = plumb `E.bracket` S.close

------------------------------------------------------------------------

_shutdownGraceMillis :: Int
_shutdownGraceMillis = 5_000

serve :: (S.Socket -> S.SockAddr -> IO a) -> S.Socket -> IO b
serve !handler = forever . serveOnce handler

serveOnce :: (S.Socket -> S.SockAddr -> IO a) -> S.Socket -> IO ThreadId
serveOnce !handler !socket = accept' socket $ acceptor handler

accept' :: S.Socket -> ((S.Socket, S.SockAddr) -> IO c) -> IO c
accept' !socket = S.accept socket `E.bracketOnError` (S.close . fst)

acceptor :: (S.Socket -> S.SockAddr -> IO a) -> (S.Socket, S.SockAddr) -> IO ThreadId
acceptor !handler (peer, peerAddr) =
  forkFinally (handler peer peerAddr)
              (const $ S.gracefulClose peer _shutdownGraceMillis)


handleFromUnixPeer
  :: (S.Socket -> S.SockAddr -> Maybe FilePath ->
      Maybe ProcessID -> Maybe UserID -> Maybe GroupID -> IO a)
  -> S.Socket -> S.SockAddr -> IO a
handleFromUnixPeer !handler !peer !peerAddr = do
  let path = fromUnixSocketAddr peerAddr
  (pid, uid, gid) <- getPeerCredential peer
  handler peer peerAddr path pid uid gid

getPeerCredential
  :: S.Socket -> IO (Maybe ProcessID, Maybe UserID, Maybe GroupID)
getPeerCredential s = do
  (pid', uid', gid') <- S.getPeerCredential s
  pure (oi <$> pid', oi <$> uid', oi <$> gid')
  where oi :: Integral o => Num i => o -> i
        oi = fromInteger . toInteger

