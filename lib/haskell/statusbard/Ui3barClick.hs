{-|
Module:		Ui3barClick
Description:	Support glue for the i3bar protocol
Stability:	experimental
Portability:	*nix

-}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Ui3barClick
where

{-& build-depends: text -}
import           Data.Text (Text)

{-& build-depends: aeson -}
import           Data.Aeson

{-& other-modules: UJson -}
{-& build-depends: aeson -}
import           UJson

-- | Click events.
--
--   If enabled i3bar will send you notifications if the user clicks on
--   a block; such notifications might look like, for example:
--
--   @
--   {
--    "name": "ethernet",
--    "instance": "eth0",
--    "button": 1,
--    "modifiers": ["Shift", "Mod1"],
--    "x": 1925,
--    "y": 1400,
--    "relative_x": 12,
--    "relative_y": 8,
--    "output_x": 5,
--    "output_y": 1400,
--    "width": 50,
--    "height": 22
--   }
--   @
class IBarClick self where
  -- | Name of the block, if set
  _name :: self -> Maybe Text

  -- | Instance of the block, if set
  _instance :: self -> Maybe Text

  -- | X11 root window coordinates where the click occurred
  _x, _y :: self -> Int

  -- | X11 button ID (for example 1 to 3 for left\/middle\/right mouse button)
  _button :: self -> Int

  -- | Coordinates where the click occurred, with respect to the top left
  --   corner of the block
  _relative_x, _relative_y :: self -> Int

  -- | Coordinates relative to the current output where the click occurred
  _output_x, _output_y :: self -> Int

  -- | Width and height (in px) of the block
  _width, _height :: self -> Int

  -- | An array of the modifiers active when the click occurred.
  --   The order in which modifiers are listed is not guaranteed.
  _modifiers :: self -> [Text]

instance IBarClick self => ConcoctToJSON self where
  objectTo field fieldP self =
       fieldP "name"       _name       self
    <> fieldP "instance"   _instance   self
    <> field  "x"          _x          self
    <> field  "y"          _y          self
    <> field  "button"     _button     self
    <> field  "relative_x" _relative_x self
    <> field  "relative_y" _relative_y self
    <> field  "output_x"   _output_x   self
    <> field  "output_y"   _output_y   self
    <> field  "width"      _width      self
    <> field  "height"     _height     self
    <> field  "modifiers"  _modifiers  self

clickToJSONValue :: IBarClick self => self -> Value
clickToJSONValue = objectToJSONValue

clickToJSONEncoding :: IBarClick self => self -> Encoding
clickToJSONEncoding = objectToJSONEncoding
