{-|
Module:		StatusbarItem
Description:	fill out my i3 status bar
Stability:	experimental
Portability:	*nix

--}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

{-# OPTIONS_GHC -Wno-unused-imports #-}

module StatusbarItem
  ( StatusbarItem(..)
  , IStatusbarItem(..)
  , makeStatusbarItem )
where

{-& build-depends: base -}
import           Control.Concurrent
import           Data.Functor ((<&>))
import           Data.Text (Text)
import           GHC.Generics
import           GHC.Records
import           System.Timeout
{-& build-depends: stm -}
import           Control.Concurrent.STM (STM, TMVar)
import qualified Control.Concurrent.STM as STM

{-& build-depends: aeson -}
import qualified Data.Aeson as Aeson

{-& other-modules: Ui3barBlock -}
import           Ui3barBlock (IBarBlock)
import qualified Ui3barBlock as IBarBlock

#ifdef WITH_TESTS
{-& build-depends: hspec, hspec-expectations -}
import           Test.Hspec
import           Test.Hspec.QuickCheck
{-& build-depends: HUnit -}
import           Test.HUnit
{-& build-depends: QuickCheck -}
import           Test.QuickCheck
#endif

------------------------------------------------------------------------
-- * Infrastructure:
-- ** Item infrastructure

data StatusbarItem config = StatusbarItem
  { _config :: !config
  , _valueV :: !(TMVar Aeson.Value)
  , _sparkV :: !(TMVar ThreadId)
  }

deriving instance Generic config  => Generic (StatusbarItem config)
deriving instance Eq config       => Eq (StatusbarItem config)

-- | A statusbar Item.
--
--   The definition of an Item shall provide two types:
--
--   * 'config', which describes immutable per-instance configuration,
--     which is required at instance construction time (see 'itemMake'),
--     and
--
--   * 'block', which shall also fulfil 'IBarBlock', which describes the
--     result of a single Item-value update.
--
--   Items shall always have a value to yield, stashed with a concurrent
--   slot (given via 'itemGetValueV'), retrieved via 'itemGetValue'
--
--   Items provide a function that yields a 'IBarBlock' (via 'itemEval').
--
--   Items advise that they should be updated every /interval/ μs
--   (via 'itemGetUpdateIntervalUsec').
--
--   Every /interval/ μs, the Item's IBarBlock computation is invoked.
--   On computation, the Item's 'block' result is converted to a JSON
--   intermediary (c.f., 'Aeson.Value'), and stashed into a concurrent
--   slot.
--
--   Items advise that their updates should conclude within /timeout/ μs
--   (via 'itemGetUpdateTimeoutUsec').  If the item does not yield a
--   value within that time, or the block computation fails for whatever
--   reason, an error value is stashed instead.
--
--   This implementation stashes values in a 'TMVar' slot: fetching the
--   Item value is done via 'STM.readTMVar' of the slot; updating the
--   Item value is done via 'STM.swapTMVar' of the slot.  (The Ruby
--   implementation logically wrapped a mutex around the whole block
--   state hash; this tactic is somewhat less awful.)
--
class IStatusbarItem self config block
    | self -> block, self -> config, config -> self where

  -- | Create an instance of this item, given its configuration object.
  --   **Required.**
  itemMake :: config -> IO self
  -- probably:
  -- itemMake = makeStatusbarItem

  -- | Get the configuration object of this item instance.
  --   **Required.**
  itemGetConfig :: self -> config
  default itemGetConfig :: HasField "_config" self config => self -> config
  itemGetConfig = getField @"_config"
  {-- INLINE itemGetConfig --}

  -- | Get the concurrent-safe slot that stores the JSON intermediate.
  --   **Required.**
  itemGetValueV :: self -> TMVar Aeson.Value
  default itemGetValueV :: HasField "_valueV" self (TMVar Aeson.Value)
                        => self -> TMVar Aeson.Value
  itemGetValueV = getField @"_valueV"
  {-- INLINE itemGetValueV --}

  -- | Get the evaluator thread-id slot.
  --   **Required.**
  itemGetSparkV :: self -> TMVar ThreadId
  default itemGetSparkV :: HasField "_sparkV" self (TMVar ThreadId)
                        => self -> TMVar ThreadId
  itemGetSparkV = getField @"_sparkV"
  {-- INLINE itemGetSparkV --}

  -- | How long, in microseconds since the start of an update, should we
  --   wait to start the next update?  By default, 30s; **overrideable**.
  itemGetUpdateIntervalUsec :: self -> Int
  itemGetUpdateIntervalUsec _ = 30_000_000

  -- | How long, in microseconds, should we wait before deciding the
  --   update attempt has failed?  By default, 10s; **overrideable**.
  itemGetUpdateTimeoutUsec  :: self -> Int
  itemGetUpdateTimeoutUsec _ = 10_000_000

  -- | Generate a new block value for the item, but don't store it.
  --   **Required.**
  itemEval :: IBarBlock block => self -> IO block

  -- | Generate a new Value for the item, but don't store it.
  itemEvalJSON :: IBarBlock block => self -> IO Aeson.Value
  itemEvalJSON self = itemEval self <&> itemBlockToJSON self
  {-- INLINE itemEvalJSON --}

  -- | Convenience wrapper around 'IBarBlock.blockToJSONValue'.
  itemBlockToJSON :: IBarBlock block => self -> block -> Aeson.Value
  itemBlockToJSON _ = IBarBlock.blockToJSONValue
  {-- INLINE itemBlockToJSON --}

  -- | Generate a new Value, enforcing the timeout.
  itemTimedEvalJSON' :: IBarBlock block => self -> IO (Maybe Aeson.Value)
  itemTimedEvalJSON' !self =
    itemGetUpdateTimeoutUsec self `timeout` itemEvalJSON self
  {-- INLINE itemTimedEvalJSON' --}

  -- | Generate a new Value, enforcing the timeout, assuring a value.
  itemEvaluate :: IBarBlock block => self -> IO Aeson.Value
  itemEvaluate !self
      = itemTimedEvalJSON' self
    >>= \case Just val -> return val
              Nothing  -> pure $ itemFailedJSON self "[timed out]"
  {-- INLINE itemEvaluate --}

  -- | Generate a block value for the item having failed,
  --   including some message explaining its failure.
  --   **Required.**
  itemFailed :: IBarBlock block => self -> Text -> block

  -- | Generate a block value for the item having failed,
  --   including some message explaining its failure.
  itemFailedJSON :: IBarBlock block => self -> Text -> Aeson.Value
  itemFailedJSON = itemBlockToJSON =<.<= itemFailed
  {-- INLINE itemFailedJSON --}

  -- | Generate a new value for this item, then store it.
  itemUpdateOnce :: IBarBlock block => self -> IO ()
  itemUpdateOnce self = itemEvaluate self >>= itemWriteValue self
  {-- INLINE itemUpdateOnce --}

  -- | Get the currently-stored value.
  itemReadValue :: self -> IO (Maybe Aeson.Value)
  itemReadValue = itemTryReadSTMV itemGetValueV
  {-- INLINE itemReadValue --}

  -- | Update the currently-stored value.
  itemWriteValue :: self -> Aeson.Value -> IO ()
  itemWriteValue = itemWriteSTMV itemGetValueV
  {-- INLINE itemWriteValue --}

  -- | The item updater loop.
  itemRun :: IBarBlock block => self -> IO ()
  itemRun self
     = itemUpdateOnce self
    >> threadDelay (itemGetUpdateIntervalUsec self)
    >> itemRun self

  -- | Start the item updater thread.
  itemStartThread :: IBarBlock block => self -> IO ()
  itemStartThread !self = forkIO (itemRun self) >>= itemWriteSpark self

  -- | Read the evaluator thread-id.
  itemReadSpark :: self -> IO (Maybe ThreadId)
  itemReadSpark = itemTryReadSTMV itemGetSparkV
  {-- INLINE itemReadSpark --}

  -- | Write the evaluator thread-id.
  itemWriteSpark :: self -> ThreadId -> IO ()
  itemWriteSpark = itemWriteSTMV itemGetSparkV
  {-- INLINE itemWriteSpark --}

itemTryReadSTMV :: (self -> TMVar a) -> self -> IO (Maybe a)
itemTryReadSTMV !stmv = STM.atomically . itemTryReadSTMV' stmv
{-# INLINE itemTryReadSTMV #-}

itemTryReadSTMV' :: (self -> TMVar a) -> self -> STM (Maybe a)
itemTryReadSTMV' !stmv = STM.tryReadTMVar . stmv
{-# INLINE itemTryReadSTMV' #-}

itemWriteSTMV :: (self -> TMVar a) -> self -> a -> IO ()
itemWriteSTMV !stmv !self = STM.atomically . itemWriteSTMV' stmv self
{-# INLINE itemWriteSTMV #-}

itemWriteSTMV' :: (self -> TMVar a) -> self -> a -> STM ()
itemWriteSTMV' !stmv !self = STM.writeTMVar $ stmv self
{-# INLINE itemWriteSTMV' #-}

infixl =<.<=

(=<.<=) :: (self -> q -> r) -> (self -> p -> q) -> self -> p -> r
f =<.<= g = \x -> f x . g x

makeStatusbarItem :: config -> IO (StatusbarItem config)
makeStatusbarItem !_config = do
  _valueV <- STM.atomically STM.newEmptyTMVar
  _sparkV <- STM.atomically STM.newEmptyTMVar
  pure $ StatusbarItem { .. }

------------------------------------------------------------------------
