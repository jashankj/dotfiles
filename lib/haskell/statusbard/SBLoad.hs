{-|
Module:		SBLoad
Description:	fill out my i3 status bar
Stability:	experimental
Portability:	*nix

* load-block: load-average (and runq?)

--}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NumericUnderscores #-}

{-# OPTIONS_GHC -Wno-unused-imports #-}

module SBLoad
  -- ( SBLoad
  -- , SBLoadBlock(..)
  -- , SBLoadConfig(..)
  -- )
where

{-& build-depends: base -}
import           Control.Applicative
import qualified Data.Char as Char
import           Data.Data
import           Data.Maybe (fromMaybe)
import           GHC.Generics
{-& build-depends: bytestring -}
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
{-& build-depends: text -}
import           Data.Text (Text)
import qualified Data.Text as T

{-& build-depends: attoparsec -}
import qualified Data.Attoparsec.ByteString as P

{-& other-modules: Ui3barBlock -}
import           Ui3barBlock (IBarBlock, RGB)
import qualified Ui3barBlock as IBarBlock

import           StatusbarItem

#ifdef WITH_TESTS
{-& build-depends: hspec, hspec-expectations -}
import           Test.Hspec
import           Test.Hspec.QuickCheck
{-& build-depends: HUnit -}
import           Test.HUnit
{-& build-depends: QuickCheck -}
import           Test.QuickCheck
#endif

------------------------------------------------------------------------
-- * Items
-- ** Load

data SBLoadConfig = SBLoadConfig
  { _instance :: !Text
  } deriving (Eq, Data, Generic, Show, Typeable)

data SBLoadBlock = SBLoadBlock
  { _full_text :: !Text
  , _name      :: !Text
  , _instance  :: !Text
  , _color     :: !RGB
  , _markup    :: !Text
  } deriving (Eq, Data, Generic, Show, Typeable)

instance IBarBlock SBLoadBlock where
  _full_text (SBLoadBlock x _ _ _ _) =      x
  _name      (SBLoadBlock _ x _ _ _) = Just x
  _instance  (SBLoadBlock _ _ x _ _) = Just x
  _color     (SBLoadBlock _ _ _ x _) = Just x
  _markup    (SBLoadBlock _ _ _ _ x) = Just x

type SBLoad = StatusbarItem SBLoadConfig

-- instance IStatusbarItem SBLoad SBLoadConfig SBLoadBlock where
--   itemMake = makeStatusbarItem

--   itemGetUpdateIntervalUsec _ = 5_000_000
--   {-- INLINE itemGetUpdateIntervalUsec --}
--   itemGetUpdateTimeoutUsec _ = 1_000_000
--   {-- INLINE itemGetUpdateTimeoutUsec --}

--   itemFailed :: SBLoad -> Text -> SBLoadBlock
--   itemFailed self = undefined -- sbLoadBlockEmit $ itemGetConfig self

--   itemEval :: SBLoad -> IO SBLoadBlock
--   itemEval = runSBLoadBlock . itemGetConfig

-- runSBLoadBlock :: SBLoadConfig -> IO SBLoadBlock
-- runSBLoadBlock !config = do

-- sbLoadBlockEmit :: SBLoadConfig -> Text -> SBLoadBlock
-- sbLoadBlockEmit (SBLoadConfig { _instance }) !full_text =
--   SBLoadBlock
--   { _full_text = full_text
--   , _name      = "loadavg"
--   , _instance  = _instance
--   , _color     = IBarBlock.colourFG
--   , _markup    = IBarBlock.markupPango }


--   , _name      = "loadavg"
--   , _instance  = _instance
--   , _color     = IBarBlock.colourFG
--   , _markup    = IBarBlock.markupPango }

-- data LoadAverage = LoadAverage
--   { _one     :: !Float
--   , _five    :: !Float
--   , _fifteen :: !Float
--   } deriving (Eq, Data, Generic, Show, Typeable)

-- -- getLoadAverage :: IO LoadAverage

------------------------------------------------------------------------
-- *** ... on Linux

-- | From <https://www.kernel.org/doc/html/latest/filesystems/proc.html>:
--
-- > Load average of last 1, 5 & 15 minutes; number of processes
-- > currently runnable (running or on ready queue); total number of
-- > processes in system; last pid created.  All fields are separated by
-- > one space except number of processes currently runnable and total
-- > number of processes in system, which are separated by a slash
-- > (@\/@).  Example: @0.61 0.61 0.55 3/828 22084@
--
-- This structure matches that shape, except stores the load average
-- multiplied by one thousand.
data LoadAverage'Linux = LoadAverage'Linux
  { _one_milli     :: !Int
  , _five_milli    :: !Int
  , _fifteen_milli :: !Int
  , _nRunnable     :: !Int
  , _nProcesses    :: !Int
  , _lastPid       :: !Int
  } deriving (Eq, Data, Generic, Show, Typeable)

getLoadAverage'Linux :: IO (Maybe LoadAverage'Linux)
getLoadAverage'Linux
   =  parseLoadAverage'Linux
  <$> readLoadAverage'Linux "/proc/loadavg"

readLoadAverage'Linux :: FilePath -> IO ByteString
readLoadAverage'Linux = BS.readFile

parseLoadAverage'Linux :: ByteString -> Maybe LoadAverage'Linux
parseLoadAverage'Linux input = maybeEither $ P.parseOnly file input
  where
    -- | parse a Linux @\/proc\/loadavg@ file
    --   "1.90 1.88 2.02 2/2712 2930234"
    file = do
      { one <- lavg; space; five <- lavg; space; fifteen <- lavg; space
      ; qlen <- number; slash; nproc <- number; space; lastpid <- number
      ; linefeed; P.endOfInput
      ; pure $ LoadAverage'Linux one five fifteen qlen nproc lastpid
      }
    -- | extremely load average -- "1.90" => ([1],[9,0])
    lavgx = do { ds <- num; dot; us <- num; pure (ds, us) }
    -- | parse milliloadaverage -- "1.78" => 1780
    lavg = do { (ds, us) <- lavgx; pure $ dec (ds <> lowerN 3 us) }
    -- | most-significant-digit list to integral value -- [1,7,8,0] => 1780
    dec :: (Foldable t, Num n) => t n -> n
    dec = foldl (\n x -> (n * 10) + x) 0
    -- | take the first 'n' digits of 'xs', then follow it with all zeroes.
    lowerN :: Num n => Int -> [n] -> [n]
    lowerN n xs = take n $ xs <> cycle [0]
    -- | a sequence of digits is probably a number
    number = dec <$> num
    -- | a sequence of digits
    num = many digit

    linefeed = P.satisfy (== 0o12)
    space    = P.satisfy (== 0x20)
    dot      = P.satisfy (== 0x2e)
    slash    = P.satisfy (== 0x2f)
    digit :: P.Parser Int
    digit  =  fromIntegral
          <$> flip (-) 0x30
          <$> P.satisfy (\w -> 0x30 <= w && w <= 0x39)

    maybeEither :: Either e x -> Maybe x
    maybeEither (Left  _) = Nothing
    maybeEither (Right x) = Just x

------------------------------------------------------------------------
-- *** ... on FreeBSD (TODO)

------------------------------------------------------------------------
