#!/usr/bin/env runhaskell
{--
:Module:       statusbard.cabal
:Description:  generate @statusbard.cabal@
:Stability:    experimental
:Portability:  GHC
--}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Strict #-}
{-# OPTIONS_GHC -Wall #-}

module Main where

import qualified Data.ByteString                        as BS
import           Data.Either (fromRight)

import           Distribution.CabalSpecVersion          as CSV
import qualified Distribution.Compiler                  as C
import qualified Distribution.FieldGrammar              as FG
import qualified Distribution.FieldGrammar.Parsec       as FGP
import qualified Distribution.Fields.ParseResult        as FPR
import qualified Distribution.Fields.Parser             as F
import qualified Distribution.ModuleName                as MN
import qualified Distribution.PackageDescription.FieldGrammar as PDFG
import           Distribution.PackageDescription.PrettyPrint as PDPP
import qualified Distribution.SPDX.License              as SPDX
import qualified Distribution.SPDX.LicenseExpression    as SPDX
import qualified Distribution.SPDX.LicenseId            as SPDX
import qualified Distribution.Types.BuildInfo           as BI
import qualified Distribution.Types.BuildType           as BT
import qualified Distribution.Types.Dependency          as D
import qualified Distribution.Types.Executable          as E
import qualified Distribution.Types.PackageDescription  as PD
import qualified Distribution.Types.PackageId           as PI
import qualified Distribution.Types.PackageName         as PN
import qualified Distribution.Types.SourceRepo          as S
import qualified Distribution.Types.UnqualComponentName as UCN
import qualified Distribution.Types.Version             as V
import qualified Distribution.Types.VersionRange        as VR
import qualified Distribution.Utils.Path                as Path
import qualified Language.Haskell.Extension             as Hs
-- import qualified Distribution.Types.Benchmark           as B
-- import qualified Distribution.Types.ForeignLib          as FL
-- import qualified Distribution.Types.Library             as L
-- import qualified Distribution.Types.SetupBuildInfo      as SBI
-- import qualified Distribution.Types.TestSuite           as TS
-- import           Distribution.Utils.ShortText (ShortText)
-- import qualified Distribution.Utils.ShortText as ShortText

main :: IO ()
main = sequence executables >>= \x -> emit $ package { PD.executables = x }
  where
    emit = putStrLn . PDPP.showPackageDescription
    executables =
      [ executable "statusbarc" "statusbarc.hs"
      , executable "statusbard" "statusbard.hs" ]

package :: PD.PackageDescription
package =
  PD.emptyPackageDescription
  { PD.specVersion  = CSV.CabalSpecV3_0
  , PD.package      = PI.PackageIdentifier
    { PI.pkgName    = "statusbard"
    , PI.pkgVersion = V.mkVersion [2024, 07, 20, 0] }
  , PD.synopsis     = "my statusbar"
  , PD.stability    = "experimental"
  , PD.pkgUrl       = "https://gitlab.com/jashankj/dotfiles"
  , PD.author       = "Jashank Jeremy; jashank at rulingia dot com dot au"
  , PD.licenseRaw   = Left $ SPDX.License $
    SPDX.ELicenseId SPDX.Apache_2_0 `SPDX.ELicense` Nothing
  , PD.buildTypeRaw = Just BT.Simple
  , PD.sourceRepos =
    [ (S.emptySourceRepo S.RepoHead)
      { S.repoType     = Just $ S.KnownRepoType S.Git
      , S.repoLocation = Just "https://gitlab.com/jashankj/dotfiles.git"
      , S.repoSubdir   = Just "lib/haskell/statusbard" } ]
  }

-- a convenient list of all the "base" dependencies:
everyDep, platformDeps :: [PN.PackageName]
everyDep =
  [ "array"
  , "base"
  , "binary"
  , "bytestring"
  , "containers"
  , "deepseq"
  , "directory"
  , "exceptions"
  , "filepath"
  , "haskeline"
  , "mtl"
  , "parsec"
  , "pretty"
  , "process"
  , "stm"
  , "template-haskell"
  , "terminfo"
  , "text"
  , "time"
  , "transformers"
  , "unix" -- , "Win32"
  , "xhtml"
  ]
platformDeps =
  [ "ghc"
  , "ghc-bignum"
  , "ghc-boot"
  , "ghc-boot-th"
  , "ghc-compact"
  , "ghc-heap"
  , "ghci"
  , "ghc-prim"
  , "hpc"
  , "semaphore-compat"
  , "system-cxx-std-lib"
--, "integer-gmp"
  , "Cabal"
  , "Cabal-syntax"
  ]

executable :: UCN.UnqualComponentName -> FilePath -> IO E.Executable
executable name sourceName = do
  let base = executable' name sourceName [] everyDep -- []
  addenda <- consider sourceName
  return $ base { E.buildInfo = E.buildInfo base <> addenda }

executable' :: UCN.UnqualComponentName -> FilePath
            -> [MN.ModuleName] -> [PN.PackageName] -> E.Executable
executable' name sourceName otherModules buildDepends =
  E.emptyExecutable
  { E.exeName    = name
  , E.modulePath = sourceName
  , E.buildInfo =
    BI.emptyBuildInfo
    { BI.hsSourceDirs = [Path.sameDirectory]
    , BI.otherModules = otherModules
    , BI.defaultLanguage = Just Hs.Haskell2010
    , BI.options = C.PerCompilerFlavor
      [ "-Wall", "-threaded", "-rtsopts", "-with-rtsopts=-N" ]
      []
    , BI.targetBuildDepends = simpleDep `map` buildDepends }
  }
  where simpleDep x = D.mkDependency x VR.anyVersion D.mainLibSet

consider :: FilePath -> IO BI.BuildInfo
consider file =
  go <$> BS.readFile file
  where
  go input =
    case read1 input of
      (l, r) | BS.null l && BS.null r -> mempty
      (l, r) | BS.null l              -> go r
      (l, r) -> runParseBuildInfo (lexCabalSpec l) <> go r

  -- the Cabal format parser goes something like ...
  --
  --   readFields :: ByteString -> Either ParseError [Field Position]
  --   takeFields :: [Field ann] -> (Fields ann, [Field ann])
  --   parseFieldGrammar :: CabalSpecVersion -> FGP.Fields Position
  --                     -> ParsecFieldGrammar s a -> ParseResult a
  --   runParseResult :: ParseResult a
  --                  -> ([PWarning], Either (Version, [PError]) a)
  --
  -- we then just thread the mess, and fold a mempty through it.

  runParseBuildInfo
    = fromRight (error "failed to parse term")
    . snd . FPR.runParseResult . parseBuildInfo
  parseBuildInfo =
    parseCabal CSV.CabalSpecV3_0 PDFG.buildInfoFieldGrammar
  parseCabal specV grammar fields =
    FGP.parseFieldGrammar specV fields grammar
  lexCabalSpec input =
    either (const $ error "failed to lex Cabal spec")
           (fst . FG.takeFields) $
    F.readFields input
  read1
    = BS.breakSubstring "-}"
    . BS.drop 3 . snd
    . BS.breakSubstring "{-&"
