#!/usr/bin/env runhaskell
{-|
Module:		UTimeZone
Description:	@time@, @tz@, now kith.
Stability:	experimental
Portability:	POSIX

How much effort is required to make @%z@ actually work?  Too much.

The @time@ library is somehow both over- /and/ under-engineered,
and is so vastly detached from reality that there is no meaningful
native timezone support.  In practice, this means, "we are too leet
to think the Olson timezone database is relevant to us."

@time@ uses 'ZonedTime' -- a pair of 'LocalTime' and 'TimeZone'.
Great, except there are (functionally) only ten 'TimeZone's that
can be formatted by default; good luck outside them.

Solutions like @tz@ work, but because the (surprisingly elegant)
formatting logic inside @time@ is closed to extension, one cannot
format the 'TZ' object as a timezone.

So, here's a big bag of contortions!

requires build-depends @containers@, @time@, @tz@, @vector@

-}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ParallelListComp #-}

{-# OPTIONS_GHC -Wno-unused-imports #-}

module UTimeZone
where

{-& build-depends: base -}
import           Data.Int
import           GHC.Generics
{-& build-depends: containers -}
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
{-& build-depends: time -}
import           Data.Time (LocalTime, TimeZone, UTCTime, ZonedTime)
import qualified Data.Time as Time
import qualified Data.Time.Clock.System as Time

{-& build-depends: tz -}
import qualified Data.Time.Zones as TZ
import           Data.Time.Zones.Types (TZ)
import qualified Data.Time.Zones.Types as TZ
{-& build-depends: vector -}
import qualified Data.Vector.Generic as V

#ifdef WITH_TESTS
import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.HUnit
import           Test.QuickCheck
#endif

-- | Fake a 'ZonedTime' from a 'UTCTime' and a 'TZ'.
--   Analogous to 'Time.utcToLocalTime'.
zonedFromUtcTz :: UTCTime -> TZ -> ZonedTime
zonedFromUtcTz !utc !tz =
  let local :: LocalTime = TZ.utcToLocalTimeTZ tz utc
      zone  :: TimeZone  = timezoneFromUtcTz utc tz
   in Time.ZonedTime local zone

-- | Fake a 'TimeZone' from a 'UTCTime' and a 'TZ'.
timezoneFromUtcTz :: UTCTime -> TZ -> TimeZone
timezoneFromUtcTz !utc !tz =
  let Tzi {..} = tziGetMap tz `tziChoose` utc
  in Time.TimeZone
     { timeZoneMinutes    = _tzi_offset `div` 60
     , timeZoneSummerOnly = _tzi_isDST
     , timeZoneName       = _tzi_abbrev }

-- | 'TZ' anchors time to the Unix epoch. Sure, fine, whatever.
--   @time@ pronounces this 'Time.SystemTime'.
utcToSystemSeconds :: UTCTime -> Int64
utcToSystemSeconds = Time.systemSeconds . Time.utcToSystemTime

-- | An unpacked version of a single rule of a 'TZ'.
--   Assuming that "time" is given as an integer number of seconds,
--   arbitrarily choosing the Unix epoch as the zero point,
--   we divide the whole range into inclusive-exclusive bands,
--   and in each band we describe the specific rule that applies.
--   Roughly corresponds to Olson tz database rules.
data Tzi = Tzi
  { _tzi_earliest :: !Int64  -- ^ earliest unix-epoch-second, inclusive
  , _tzi_latest   :: !Int64  -- ^ latest unix-epoch-second, exclusive
  , _tzi_offset   :: !Int    -- ^ offset, in seconds, in this timezone
  , _tzi_isDST    :: !Bool   -- ^ is this "daylight time"
  , _tzi_abbrev   :: !String -- ^ textual abbreviation
  } deriving (Eq, Generic, Show)

-- | An unpacked version of all the rules of a 'TZ',
--   keyed by the earliest time of each rule.  (See also 'Tzi'.)
newtype TziMap = TziMap (Map Int64 Tzi)
  deriving (Eq, Generic, Show)

-- | For a particular UTC time, get the 'Tzi' that applies.
tziChoose :: TziMap -> UTCTime -> Tzi
tziChoose !the = tziChoose' the . utcToSystemSeconds

-- | For a particular epoch-anchored second, get the 'Tzi' that applies.
tziChoose' :: TziMap -> Int64 -> Tzi
tziChoose' !(TziMap m) !t =
  case t `M.lookupLT` m of
    Nothing     -> error "TziMap has no ranges; is this timezone valid?"
    Just (_, x) -> x

-- | Unpack a TZ into a TziMap.
tziGetMap :: TZ -> TziMap
tziGetMap !(TZ.TZ {..}) = TziMap $ foldr acc M.empty tzTDI
  where
    acc x@(Tzi {..}) m = M.insert _tzi_earliest x m
    tzTDI = [ Tzi rl rh o d a
            | (rl, rh) <- [ (rl, rh)
                          | rl <- trans
                          | rh <- drop 1 trans ++ [maxBound] ]
            | o        <- V.toList _tzDiffs
            | (d, a)   <- V.toList _tzInfos ]
    trans = V.toList _tzTrans
