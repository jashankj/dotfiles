{-|
Module:		Ui3barHeader
Description:	Support glue for the i3bar protocol
Stability:	experimental
Portability:	*nix

-}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Ui3barHeader
where

{-& build-depends: aeson -}
import           Data.Aeson

{-& other-modules: UJson -}
{-& build-depends: aeson -}
import           UJson

-- | An i3bar header.
class IBarHeader self where
  -- | The version number (as an integer) of the i3bar protocol you will use.
  _version :: self -> Int
  _version _ = 1

  -- | Specify the signal (as an integer) that i3bar should send to
  --   request that you pause your output.  This is used to conserve
  --   battery power when the bar is hidden by not unnecessarily
  --   computing bar updates.  The default value is @SIGSTOP@, which
  --   will unconditionally stop your process.  If this is an issue,
  --   this feature can be disabled by setting the value to 0.
  _stop_signal :: self -> Maybe Int
  _stop_signal _ = Nothing

  -- | Specify to i3bar the signal (as an integer) to send to continue
  --   your processing.  The default value (if none is specified) is
  --   @SIGCONT@.
  _cont_signal :: self -> Maybe Int
  _cont_signal _ = Nothing

  -- | If specified and true i3bar will write an infinite array (same as
  --   above) to your stdin.
  _click_events :: self -> Maybe Bool
  _click_events _ = Nothing

instance IBarHeader ()

instance IBarHeader self => ConcoctToJSON self where
  objectTo field fieldP self =
       field  "version"      _version      self
    <> fieldP "stop_signal"  _stop_signal  self
    <> fieldP "cont_signal"  _cont_signal  self
    <> fieldP "click_events" _click_events self

headerToJSONValue :: IBarHeader self => self -> Value
headerToJSONValue = objectToJSONValue

headerToJSONEncoding :: IBarHeader self => self -> Encoding
headerToJSONEncoding = objectToJSONEncoding
