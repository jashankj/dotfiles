{-|
Module:		UJson
Description:	some ugly JSON magick
Stability:	experimental
Portability:	*nix

-}

{-# LANGUAGE Haskell2010 #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE QuantifiedConstraints #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE RankNTypes #-}

module UJson
where

{-& build-depends: base -}
import           Data.Maybe

{-& build-depends: aeson -}
import           Data.Aeson ( KeyValue((.=)), KeyValueOmit((.?=)), ToJSON
                            , Encoding, Key, Value, pairs, object )

class ConcoctToJSON self where
  objectTo
    :: Semigroup kv
    => {- field -}
       (forall val . ToJSON val -- => KeyValue e kv
        => Key -> (self -> val) -> self -> kv)
    -> {- fieldP -}
       (forall val . ToJSON val -- => KeyValueOmit e kv
        => Key -> (self -> Maybe val) -> self -> kv)
    -> self
    -> kv

  objectToJSONValue :: self -> Value
  objectToJSONValue self = object . catMaybes $ objectTo field fieldP self
    where
      field :: ToJSON val => KeyValue e pair
            => Key -> (self -> val) -> self -> [Maybe pair]
      field label fun self' = [ Just $ label .= fun self' ]
      fieldP :: ToJSON val => KeyValue e pair
             => Key -> (self -> Maybe val) -> self -> [Maybe pair]
      fieldP label fun self' = [ (label .=) <$> fun self' ]

  objectToJSONEncoding :: self -> Encoding
  objectToJSONEncoding self = pairs $ objectTo field fieldP self
    where
      field :: ToJSON val => KeyValue e kv
            => Key -> (self -> val) -> self -> kv
      field  label fun self' = label .=  fun self'
      fieldP :: ToJSON val => KeyValueOmit e kv
             => Key -> (self -> Maybe val) -> self -> kv
      fieldP label fun self' = label .?= fun self'

