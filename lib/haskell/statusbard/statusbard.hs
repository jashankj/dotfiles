#!/usr/bin/env runhaskell
{-|
Module:		statusbard
Description:	fill out my i3 status bar
Stability:	experimental
Portability:	*nix

Based on ``statusbard.rb``.

Blocks we want to support:

* date-block (with custom timezones and custom formats)
* ieee-802-3-block: interface status, addresses, rate
* ieee-802-11-block: interface status, ssid, rate, band, power, rfkill
* battery-block (via upower? directly?): status, source, percent, time-left
* thermal-block: temperature, fan-speed?
* cpufreq-block: frequency, spread?
* load-block: load-average (and runq?)
* todoist-block: current-karma?
* toggl-block?

features:

* UNIX socket (or TCP, in a pinch)
* pidfile
* blocks are non-blocking
* block performance counters
* start blocks only when required

--}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE OverloadedStrings #-}

{-# LANGUAGE BinaryLiterals #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DoAndIfThenElse #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

{--
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE ParallelListComp #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE ViewPatterns #-}
--}

{-# OPTIONS_GHC -Wno-unused-top-binds #-}
{-# OPTIONS_GHC -Wno-unused-imports #-}
{-# OPTIONS_GHC -Wno-missing-signatures #-}

module Main -- (main)
where

{-& build-depends: base -}
import           Control.Concurrent
import           Control.Monad
import           Data.Data
import           Data.Functor ((<&>))
import           Data.Int
import           Data.List
import           Data.Maybe (fromMaybe)
import           Data.Word (Word8)
import           GHC.Generics
import           GHC.Records
import           System.Environment (getEnvironment)
import           System.FilePath ((</>))
import           System.IO
import           System.Posix.Types (GroupID, ProcessID, UserID)
import           System.Timeout
import           Text.Printf
{-& build-depends: bytestring -}
import           Data.ByteString.Lazy (LazyByteString)
import qualified Data.ByteString.Lazy as LBS
{-& build-depends: containers -}
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
{-& build-depends: deepseq -}
import           Control.DeepSeq
{-& build-depends: stm -}
import           Control.Concurrent.STM (STM, TMVar)
import qualified Control.Concurrent.STM as STM
{-& build-depends: text -}
import           Data.Text (Text)
import qualified Data.Text as T
{-& build-depends: time -}
import           Data.Time (LocalTime, TimeZone, UTCTime, ZonedTime)
import qualified Data.Time as Time
import qualified Data.Time.Format as TF
import qualified Data.Time.Clock.System as Time
import qualified Data.Time.Clock.POSIX as Time

{-& build-depends: aeson -}
import qualified Data.Aeson as Aeson
{-& build-depends: formatting -}
import           Formatting ((%))
import qualified Formatting as F
{-& build-depends: hslogger -}
import qualified System.Log.Formatter as L
import qualified System.Log.Handler as L hiding (setLevel)
import qualified System.Log.Handler.Simple as L
import qualified System.Log.Logger as L
{-& build-depends: network -}
import qualified Network.Socket as S
import qualified Network.Socket.ByteString as BS
import qualified Network.Socket.ByteString.Lazy as LBS
{-& build-depends: typed-process -}
{-& build-depends: tz -}
import qualified Data.Time.Zones as TZ
import qualified Data.Time.Zones.All as TZI
import           Data.Time.Zones.Types (TZ)
import qualified Data.Time.Zones.Types as TZ
{-& build-depends: unordered-containers -}

{-& other-modules: Ui3bar -}
import qualified Ui3bar
{-& other-modules: Ui3barBlock -}
import           Ui3barBlock (IBarBlock, RGB)
import qualified Ui3barBlock as IBarBlock
{-& other-modules: Ui3barClick -}
import           Ui3barClick (IBarClick)
import qualified Ui3barClick as IBarClick
{-& other-modules: Ui3barHeader -}
import           Ui3barHeader (IBarHeader)
import qualified Ui3barHeader as IBarHeader
{-& other-modules: UJson -}
{-& build-depends: aeson, text -}
import qualified UJson
{-& other-modules: USock -}
{-& build-depends: network -}
import qualified USock
{-& other-modules: UTimeZone -}
{-& build-depends: containers, time, tz, vector -}
import qualified UTimeZone

import           StatusbarItem
import           SBDate
import           SBLoad

#ifdef WITH_TESTS
{-& build-depends: hspec, hspec-expectations -}
import           Test.Hspec
import           Test.Hspec.QuickCheck
{-& build-depends: HUnit -}
import           Test.HUnit
{-& build-depends: QuickCheck -}
import           Test.QuickCheck
#endif

------------------------------------------------------------------------
-- * Infrastructure:
-- ** Paths

getSocketPath, getPidfilePath :: IO FilePath
getSocketPath  = getHome <&> (</> ".cache" </> "statusbard.sock")
getPidfilePath = getHome <&> (</> ".cache" </> "statusbard.pid")

getHome :: IO FilePath
getHome = getHome' >>=
  \case (Just home) -> pure home
        Nothing     -> fail "`$HOME' is not set"

getHome' :: IO (Maybe FilePath)
getHome' = lookup "HOME" <$> getEnvironment


-- ** Socket driver

serve :: IO ()
serve = getSocketPath >>= serveAt

serveAt :: FilePath -> IO ()
serveAt path = do
  L.debugM "server" $ "listening at " <> show path
  USock.withUnixSocketServer path $ USock.serve handle

handle :: S.Socket -> S.SockAddr -> IO ()
handle = USock.handleFromUnixPeer $ handleWho handle'
  where handleWho continue !peer peerAddr _path pid uid gid = do
          L.debugM "handle" $ "accept from [" <> show pid <> ", " <>
                              show uid <> ":" <> show gid <> "]"
          continue peer peerAddr

handle' :: S.Socket -> S.SockAddr -> IO ()
handle' !peer _peerAddr = do
  got <- BS.recv peer 4096
  L.debugM "handle" $ "recv: " <> show got
  let preamble = i3barPreamble
  L.debugM "handle" $ "send: " <> show preamble
  LBS.sendAll peer preamble
  -- and now the actual cycler
  return ()

-- :: IO [Aeson.Value]

-- ** Main (unimplemented!)

main :: IO ()
#ifndef WITH_TESTS
main = undefined
#else
main = hspec $ sequence_ [spec_main]
spec_main :: SpecWith ()
#endif

initlog :: IO ()
initlog = do
  let hfmt = "[$time] $pid $tid $prio [$loggername] $msg"
  let lfmt = L.simpleLogFormatter hfmt
  hlog <- L.verboseStreamHandler stderr L.DEBUG
  let hlog' = L.setFormatter hlog lfmt
  L.updateGlobalLogger L.rootLoggerName $
    L.setLevel L.DEBUG . L.setHandlers [hlog']


-- ** Bar interaction

i3barPreamble :: LazyByteString
i3barPreamble = theHeader <> "["
  where theHeader = Aeson.encode $ IBarHeader.headerToJSONValue ()

#ifdef WITH_TESTS
spec_main = do
  describe "i3barPreamble" $ do
    it "exactly matches the old i3bar spec" $ do
      i3barPreamble `shouldBe` "{\"version\":1}["
#endif



------------------------------------------------------------------------
-- * Items
-- ** Date

config_dateBlock :: IO SBDate
config_dateBlock
  = itemMake
  $ SBDateConfig
    { _instance = "main"
    , _timezone = Nothing
    , _format = Nothing }

-- blox :: IO [forall item icfg iv. IStatusbarItem item icfg iv => item]
-- blox = do
--   dateBlock <- config_dateBlock
--   return [ dateBlock ]

-- blocks = do
--   dateBlock <- config_dateBlock
--   itemStartThread dateBlock
--   return dateBlock



------------------------------------------------------------------------
-- ** IEEE 802.3 status



------------------------------------------------------------------------
-- ** IEEE 802.11 status



------------------------------------------------------------------------
-- ** Battery status



------------------------------------------------------------------------
-- ** Thermals



------------------------------------------------------------------------
-- ** CPU frequency



------------------------------------------------------------------------
-- ** Load average



------------------------------------------------------------------------
-- ** Todoist



------------------------------------------------------------------------
-- ** Toggl



------------------------------------------------------------------------
