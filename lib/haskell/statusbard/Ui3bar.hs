{-|
Module:		Ui3bar
Description:	Support glue for the i3bar protocol
Stability:	experimental
Portability:	*nix


This document explains the protocol in which i3bar expects its input. It
provides support for colors, urgency, shortening and easy manipulation.


== Rationale for choosing JSON

Before describing the protocol, let’s cover why JSON is a building block of
this protocol.

1. Other bar display programs such as dzen2 or xmobar are using in-band
   signaling: they recognize certain sequences (like ^fg(#330000) in
   your input text).  We would like to avoid that and separate
   information from meta-information.  By information, we mean the
   actual output, like the IP address of your ethernet adapter and by
   meta-information, we mean in which color it should be displayed right
   now.

2. It is easy to write a simple script which manipulates part(s) of the
   input.  Each block of information (like a block for the disk space
   indicator, a block for the current IP address, etc.) can be
   identified specifically and modified in whichever way you like.

3. It remains easy to write a simple script which just suffixes (or
   prefixes) a status line input, because tools like i3status will
   output their JSON in such a way that each line array will be
   terminated by a newline.  Therefore, you are not required to use a
   streaming JSON parser, but you can use any JSON parser and write your
   script in any programming language.  In fact, you can decide to not
   bother with the JSON parsing at all and just inject your output at a
   specific position (beginning or end).

4. Relying on JSON does not introduce any new dependencies.  In fact,
   the IPC interface of i3 also uses JSON, therefore i3bar already
   depends on JSON.

The only point against using JSON is computational complexity.  If that
really bothers you, just use the plain text input format (which i3bar
will continue to support).


== The protocol

The first message of the protocol is a header block, which contains (at
least) the version of the protocol to be used.  In case there are
significant changes (not only additions), the version will be
incremented.  i3bar will still understand the old protocol version, but
in order to use the new one, you need to provide the correct version.
The header block is terminated by a newline and consists of a single
JSON hash:

@
{ "version": 1 }
@

An example with all features:

@
{ "version": 1, "stop_signal": 10, "cont_signal": 12, "click_events": true }
@

(Note that before i3 v4.3 the precise format had to be @{"version":1}@,
byte-for-byte.)

What follows is an infinite array (so it should be parsed by a streaming
JSON parser, but as described above you can go for a simpler solution),
whose elements are one array per status line.  A status line is one unit
of information which should be displayed at a time.  i3bar will not
display any input until the status line is complete.  In each status
line, every block will be represented by a JSON hash:

@
[
 …
 [
  {
   "full_text": "E: 10.0.0.1 (1000 Mbit/s)",
   "color": "#00ff00"
  },
  {
   "full_text": "2012-01-05 20:00:01"
  }
 ],

 [
  {
   "full_text": "E: 10.0.0.1 (1000 Mbit/s)",
   "color": "#00ff00"
  },
  {
   "full_text": "2012-01-05 20:00:02"
  }
 ],
 …
]
@

Please note that this example was pretty printed for human consumption.
i3status and others will output single statuslines in one line, separated by
@\\n@.

You can find an example of a shell script which can be used as your
@status_command@ in the bar configuration at
<https://github.com/i3/i3/blob/next/contrib/trivial-bar-script.sh>

-}

{-# LANGUAGE Haskell2010 #-}
{-# LANGUAGE OverloadedStrings #-}

{-# OPTIONS_GHC -Wno-dodgy-exports #-}
{-# OPTIONS_GHC -Wno-unused-imports #-}

module Ui3bar
  ( module Block
  , module Header
  , module Click )
where

-- import           Data.Text (Text)
-- import qualified Data.Text as T
-- import           GHC.Records

-- import           Data.Aeson

import qualified Ui3barBlock  as Block
import qualified Ui3barHeader as Header
import qualified Ui3barClick  as Click
