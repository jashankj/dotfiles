{-|
Module:		Ui3barBlock
Description:	Support glue for the i3bar protocol
Stability:	experimental
Portability:	*nix

-}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Ui3barBlock
  ( IBarBlock(..)
  , blockToJSONValue, blockToJSONEncoding
  , BarBlock(..), makeBarBlock
  , RGB(..), rgb
  , colourFG, colourBG, colourRed, colourGreen
  , rgbToString, rgbToText
  , markupPango
  )
where

{-& build-depends: base -}
import           Data.Data
import           Data.Word (Word8)
import           GHC.Generics
import           Text.Printf
{-& build-depends: text -}
import           Data.Text (Text)
import qualified Data.Text as T

{-& build-depends: aeson -}
import           Data.Aeson

{-& other-modules: UJson -}
{-& build-depends: aeson -}
import           UJson

-- | An i3bar block.
--
--   If you want to put in your own entries into a block, prefix the key
--   with an underscore (_).  i3bar will ignore all keys it doesn’t
--   understand, and prefixing them with an underscore makes it clear in
--   every script that they are not part of the i3bar protocol.
--
--   *Example*:
--   @
--   {
--    "full_text": "E: 10.0.0.1 (1000 Mbit/s)",
--    "_ethernet_vendor": "Intel"
--   }
--   @
--
--   In the following example, the longest (widest) possible value of the block is
--   used to set the minimum width:
--
--   @
--   {
--    "full_text": "CPU 4%",
--    "min_width": "CPU 100%",
--    "align": "left"
--   }
--   @
--
--   An example of a block which uses all possible entries follows:
--
--   @
--   {
--    "full_text": "E: 10.0.0.1 (1000 Mbit/s)",
--    "short_text": "10.0.0.1",
--    "color": "#00ff00",
--    "background": "#1c1c1c",
--    "border": "#ee0000",
--    "border_top": 1,
--    "border_right": 0,
--    "border_bottom": 3,
--    "border_left": 1,
--    "min_width": 300,
--    "align": "right",
--    "urgent": false,
--    "name": "ethernet",
--    "instance": "eth0",
--    "separator": true,
--    "separator_block_width": 9,
--    "markup": "none"
--   }
--   @
--
class IBarBlock self where
  -- | The @full_text@ will be displayed by i3bar on the status line.
  --   This is the only required key.  If @full_text@ is an empty
  --   string, the block will be skipped.
  _full_text :: self -> Text
  _full_text _ = T.empty
  {-# MINIMAL _full_text #-}

  -- | Where appropriate, the @short_text@ (string) entry should also be
  --   provided.  It will be used in case the status line needs to be
  --   shortened because it uses more space than your screen provides.
  --   For example, when displaying an IPv6 address, the prefix is
  --   usually (!) more relevant than the suffix, because the latter
  --   stays constant when using autoconf, while the prefix changes.
  --   When displaying the date, the time is more important than the
  --   date (it is more likely that you know which day it is than what
  --   time it is).
  _short_text :: self -> Maybe Text
  _short_text _ = Nothing

  -- | To make the current state of the information easy to spot, colors
  --   can be used.  For example, the wireless block could be displayed
  --   in red (using the @color@ (string) entry) if the card is not
  --   associated with any network and in green or yellow (depending on
  --   the signal strength) when it is associated.  Colors are specified
  --   in hex (like in HTML), starting with a leading hash sign.  For
  --   example, @#ff0000@ means red.
  _color :: self -> Maybe RGB
  _color _ = Nothing

  -- | Overrides the background color for this particular block.
  _background :: self -> Maybe RGB
  _background _ = Nothing

  -- | Overrides the border color for this particular block.
  _border :: self -> Maybe RGB
  _border _ = Nothing

  -- | Defines the width (in pixels) of the top border of this block.
  --   Defaults to 1.
  _border_top :: self -> Maybe Int
  _border_top _ = Nothing

  -- | Defines the width (in pixels) of the right border of this block.
  --   Defaults to 1.
  _border_right :: self -> Maybe Int
  _border_right _ = Nothing

  -- | Defines the width (in pixels) of the bottom border of this block.
  --   Defaults to 1.
  _border_bottom :: self -> Maybe Int
  _border_bottom _ = Nothing

  -- | Defines the width (in pixels) of the left border of this block.
  --   Defaults to 1.
  _border_left :: self -> Maybe Int
  _border_left _ = Nothing

  -- | The minimum width of the block.
  --
  --   The value can be an integer, denoting width in pixels.  If the
  --   content of the @full_text@ key take less space than the specified
  --   min_width, the block will be padded to the left and/or the right
  --   side, according to the @align@ key.  This is useful when you want
  --   to prevent the whole status line to shift when value take more or
  --   less space between each iteration.
  --
  --   The value can also be a string.  In this case, the width of the
  --   text given by @min_width@ determines the minimum width of the
  --   block.  This is useful when you want to set a sensible minimum
  --   width regardless of which font you are using, and at what
  --   particular size.
  --
  _min_width :: self -> Maybe (Either Text Int)
  _min_width _ = Nothing

  -- | Align text on the @center@, @right@ or @left@ (default) of the block, when
  --      the minimum width of the latter, specified by the @min_width@ key, is not
  --      reached.
  _align :: self -> Maybe Text
  _align _ = Nothing

  -- | Every block should have a unique @name@ (string) entry so that it
  --   can be easily identified in scripts which process the output.
  --   i3bar completely ignores the name field.
  _name :: self -> Maybe Text
  _name _ = Nothing

  -- | Make sure to also specify an @instance@ (string) entry where
  --   appropriate.  For example, the user can have multiple disk space
  --   blocks for multiple mount points.  i3bar completely ignores the
  --   instance field.
  _instance :: self -> Maybe Text
  _instance _ = Nothing

  -- | A boolean which specifies whether the current value is urgent.
  --   Examples are battery charge values below 1 percent or no more
  --   available disk space (for non-root users).  The presentation of
  --   urgency is up to i3bar.
  _urgent :: self -> Maybe Bool
  _urgent _ = Nothing

  -- | A boolean which specifies whether a separator line should be
  --   drawn after this block.  The default is true, meaning the
  --   separator line will be drawn.  Note that if you disable the
  --   separator line, there will still be a gap after the block, unless
  --   you also use @separator_block_width@.
  _separator :: self -> Maybe Bool
  _separator _ = Nothing

  -- | The amount of pixels to leave blank after the block.  In the
  --   middle of this gap, a separator line will be drawn unless
  --   @separator@ is disabled.  Normally, you want to set this to an
  --   odd value (the default is 9 pixels), since the separator line is
  --   drawn in the middle.
  _separator_block_width :: self -> Maybe Int
  _separator_block_width _ = Nothing

  -- | A string that indicates how the text of the block should be
  --   parsed:
  --
  --   [@"pango"@]: use Pango markup;
  --   [@"none"@]: do not use any markup (the default).
  --
  --   Pango markup only works if you use a Pango font.
  _markup :: self -> Maybe Text
  _markup _ = Nothing


_rgbText :: Functor f => (t -> f RGB) -> t -> f Text
_rgbText get self = rgbToText <$> get self

_colorT, _backgroundT, _borderT :: IBarBlock self => self -> Maybe Text
_colorT      = _rgbText _color
_backgroundT = _rgbText _background
_borderT     = _rgbText _border

instance IBarBlock self => ConcoctToJSON self where
  objectTo field fieldP self =
       field  "full_text"             _full_text             self
    <> fieldP "short_text"            _short_text            self
    <> fieldP "color"                 _colorT                self
    <> fieldP "background"            _backgroundT           self
    <> fieldP "border"                _borderT               self
    <> fieldP "border_top"            _border_top            self
    <> fieldP "border_right"          _border_right          self
    <> fieldP "border_bottom"         _border_bottom         self
    <> fieldP "border_left"           _border_left           self
    <> fieldP "min_width"             _min_width             self
    <> fieldP "align"                 _align                 self
    <> fieldP "name"                  _name                  self
    <> fieldP "instance"              _instance              self
    <> fieldP "urgent"                _urgent                self
    <> fieldP "separator"             _separator             self
    <> fieldP "separator_block_width" _separator_block_width self
    <> fieldP "markup"                _markup                self

blockToJSONValue :: IBarBlock self => self -> Value
blockToJSONValue = objectToJSONValue

blockToJSONEncoding :: IBarBlock self => self -> Encoding
blockToJSONEncoding = objectToJSONEncoding

------------------------------------------------------------------------

-- | A default block implementation that does nothing.
data BarBlock = BarBlock
  { _full_text             :: !Text
  , _short_text            :: !(Maybe Text)
  , _color                 :: !(Maybe RGB)
  , _background            :: !(Maybe RGB)
  , _border                :: !(Maybe RGB)
  , _border_top            :: !(Maybe Int)
  , _border_right          :: !(Maybe Int)
  , _border_bottom         :: !(Maybe Int)
  , _border_left           :: !(Maybe Int)
  , _min_width             :: !(Maybe (Either Text Int))
  , _align                 :: !(Maybe Text)
  , _name                  :: !(Maybe Text)
  , _instance              :: !(Maybe Text)
  , _urgent                :: !(Maybe Bool)
  , _separator             :: !(Maybe Bool)
  , _separator_block_width :: !(Maybe Int)
  , _markup                :: !(Maybe Text)
  } deriving (Eq, Data, Generic, Show, Typeable)

-- no, really, it does nothing
makeBarBlock :: Text -> BarBlock
makeBarBlock ft = BarBlock ft
  Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing
  Nothing Nothing Nothing Nothing Nothing Nothing Nothing Nothing


instance IBarBlock BarBlock where
  _full_text (BarBlock { _full_text = x }) = x
  _short_text (BarBlock { _short_text = x }) = x
  _color (BarBlock { _color = x }) = x
  _background (BarBlock { _background = x }) = x
  _border (BarBlock { _border = x }) = x
  _border_top (BarBlock { _border_top = x }) = x
  _border_right (BarBlock { _border_right = x }) = x
  _border_bottom (BarBlock { _border_bottom = x }) = x
  _border_left (BarBlock { _border_left = x }) = x
  _min_width (BarBlock { _min_width = x }) = x
  _align (BarBlock { _align = x }) = x
  _name (BarBlock { _name = x }) = x
  _instance (BarBlock { _instance = x }) = x
  _urgent (BarBlock { _urgent = x }) = x
  _separator (BarBlock { _separator = x }) = x
  _separator_block_width (BarBlock { _separator_block_width = x }) = x
  _markup (BarBlock { _markup = x }) = x

------------------------------------------------------------------------
-- * Helpers

-- ** Colours

-- | RGB888 colours.
newtype RGB = RGB (Word8, Word8, Word8)
  deriving (Eq, Data, Generic, Show, Typeable)

-- | Convenient 'RGB' constructor.
rgb :: Word8 -> Word8 -> Word8 -> RGB
rgb !r !g !b = RGB (r, g, b)

colourFG, colourBG, colourRed, colourGreen :: RGB
colourFG    = rgb 0xff 0xff 0xff
colourBG    = rgb 0x00 0x00 0x00
colourRed   = rgb 0xff 0x00 0x00
colourGreen = rgb 0x00 0xff 0x00

-- | Stringify 'RGB' colours.
rgbToString :: RGB -> String
rgbToString (RGB (r, g, b)) = printf "#%02x%02x%02x" r g b

-- | Stringify 'RGB' colours (to 'Text').
rgbToText :: RGB -> Text
rgbToText = T.pack . rgbToString


-- ** Markup

-- | Request use of Pango markup.
markupPango :: Text
markupPango = "pango"
