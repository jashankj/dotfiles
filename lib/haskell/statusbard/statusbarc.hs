#!/usr/bin/env runhaskell
{-|
Module:		statusbarc
Description:	render my i3 status bar
Stability:	experimental
Portability:	*nix

Based on ``statusbar.sh``.
-}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Strict #-}

module Main where

{-& build-depends: base -}
import           Control.Concurrent (threadDelay)
import           System.Environment (getArgs, getEnvironment)
{-& build-depends: bytestring -}
import qualified Data.ByteString.Char8     as C
{-& build-depends: filepath -}
import           System.FilePath ((</>))
{-& build-depends: network -}
import qualified Network.Socket.ByteString as S
{-& build-depends: text -}
import qualified Data.Text                 as T
import qualified Data.Text.Encoding        as TE

{-& other-modules: USock -}
import qualified USock

main :: IO ()
main = getArgs >>= main'

main' :: [String] -> IO ()
main' [name] = do
  (Just socketPath) <- getSocketPath
  putStrLn "{\"version\":1}[" >> main'' socketPath name >> putStrLn "]"
main' _ = error "usage: statusbarc <head>"

main'' :: FilePath -> String -> IO ()
main'' path name = do
  USock.withUnixSocketClient path runOnce >> threadDelay 1000000 >> main'' path name
  where runOnce sock = request sock >> receive sock >>= C.putStr
        request sock = S.sendMany sock [TE.encodeUtf8 $ T.pack name, "\n"]
        receive sock = S.recv sock 4096

getSocketPath :: IO (Maybe FilePath)
getSocketPath = do
  home <- lookup "HOME" <$> getEnvironment
  return $ homeRelativePath <$> home
  where homeRelativePath = (</> ".cache" </> "statusbard.sock")
