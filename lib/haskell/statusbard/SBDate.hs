{-|
Module:		SBDate
Description:	fill out my i3 status bar
Stability:	experimental
Portability:	*nix

* date-block (with custom timezones and custom formats)

--}

{-# LANGUAGE Haskell2010 #-}

{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NumericUnderscores #-}

{-# OPTIONS_GHC -Wno-unused-imports #-}

module SBDate
  ( SBDate
  , SBDateBlock(..)
  , SBDateConfig(..)
  )
where

{-& build-depends: base -}
import           Data.Data
import           Data.Maybe (fromMaybe)
import           GHC.Generics
{-& build-depends: text -}
import           Data.Text (Text)
import qualified Data.Text as T
{-& build-depends: time -}
import           Data.Time (UTCTime, ZonedTime)
import qualified Data.Time as Time
import qualified Data.Time.Format as TF

{-& build-depends: tz -}
import qualified Data.Time.Zones as TZ
-- import qualified Data.Time.Zones.All as TZI
import           Data.Time.Zones.Types (TZ)
-- import qualified Data.Time.Zones.Types as TZ

-- {-& other-modules: Ui3barBlock -}
import           Ui3barBlock (IBarBlock, RGB)
import qualified Ui3barBlock as IBarBlock
{-& other-modules: UTimeZone -}
{-& build-depends: containers, time, tz, vector -}
import qualified UTimeZone

import           StatusbarItem

#ifdef WITH_TESTS
{-& build-depends: hspec, hspec-expectations -}
import           Test.Hspec
import           Test.Hspec.QuickCheck
{-& build-depends: HUnit -}
import           Test.HUnit
{-& build-depends: QuickCheck -}
import           Test.QuickCheck
#endif

------------------------------------------------------------------------
-- * Items
-- ** Date

data SBDateConfig = SBDateConfig
  { _instance :: !Text
  , _timezone :: !(Maybe TZ)
  , _format   :: !(Maybe String)
  } deriving (Eq, Data, Generic, Show, Typeable)

data SBDateBlock = SBDateBlock
  { _full_text :: !Text
  , _name      :: !Text
  , _instance  :: !Text
  , _color     :: !RGB
  , _markup    :: !Text
  } deriving (Eq, Data, Generic, Show, Typeable)

instance IBarBlock SBDateBlock where
  _full_text (SBDateBlock x _ _ _ _) =      x
  _name      (SBDateBlock _ x _ _ _) = Just x
  _instance  (SBDateBlock _ _ x _ _) = Just x
  _color     (SBDateBlock _ _ _ x _) = Just x
  _markup    (SBDateBlock _ _ _ _ x) = Just x

type SBDate = StatusbarItem SBDateConfig

instance IStatusbarItem SBDate SBDateConfig SBDateBlock where
  itemMake = makeStatusbarItem

  itemGetUpdateIntervalUsec _ = 1_000_000
  {-- INLINE itemGetUpdateIntervalUsec --}
  itemGetUpdateTimeoutUsec _ = 1_000_000
  {-- INLINE itemGetUpdateTimeoutUsec --}

  itemFailed :: SBDate -> Text -> SBDateBlock
  itemFailed self = sbDateBlockEmit $ itemGetConfig self

  itemEval :: SBDate -> IO SBDateBlock
  itemEval = runSBDateBlock . itemGetConfig

runSBDateBlock :: SBDateConfig -> IO SBDateBlock
runSBDateBlock !config = do
  utcTime         <- Time.getCurrentTime
  zonedTime       <- sbDateIntoZonedTime config utcTime
  let timeString   = sbDateFormatDateTime config zonedTime
  let dateBlock    = sbDateBlockEmit config timeString
  return dateBlock

sbDateIntoZonedTime :: SBDateConfig -> UTCTime -> IO ZonedTime
sbDateIntoZonedTime (SBDateConfig { _timezone }) !currentUTCTime = do
  timezone <- dateDefaultTimezone _timezone
  pure $ currentUTCTime `UTimeZone.zonedFromUtcTz` timezone

sbDateFormatDateTime :: SBDateConfig -> ZonedTime -> Text
sbDateFormatDateTime (SBDateConfig { _format }) !currentZonedTime = do
  let format = dateDefaultFormat _format
  let timeStr = dateFormat format currentZonedTime
  T.pack timeStr

-- | Convenience abbreviation.
dateFormat :: TF.FormatTime time => String -> time -> String
dateFormat = TF.formatTime TF.defaultTimeLocale

sbDateBlockEmit :: SBDateConfig -> Text -> SBDateBlock
sbDateBlockEmit (SBDateConfig { _instance }) !full_text =
  SBDateBlock
  { _full_text = full_text
  , _name      = "date"
  , _instance  = _instance
  , _color     = IBarBlock.colourFG
  , _markup    = IBarBlock.markupPango }

-- | If the configuration doesn't define a timezone, provide one.
dateDefaultTimezone :: Maybe TZ -> IO TZ
dateDefaultTimezone =
  \case Nothing -> TZ.loadLocalTZ
        Just x  -> pure x

-- | If the configuration doesn't define a format, provide one.
dateDefaultFormat :: Maybe String -> String
dateDefaultFormat = fromMaybe "%Y-%m-%d %H:%M:%S %Z %z"


------------------------------------------------------------------------
