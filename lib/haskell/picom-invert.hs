#!/usr/bin/env runhaskell
{--
:Module:	picom-invert
:Description:	use picom(1) to invert colours on a window.
:Stability:	experimental
:Portability:	*nix

<https://www.reddit.com/r/i3wm/comments/kbw3a5/shortcut_for_inverting_a_windows_colors/>

TODO: poke X directly, instead of futzing around with xdotool/xprop.

--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-& build-depends: base, bytestring, typed-process -}
{-& other-modules: UProc -}

module Main where

import           Control.Monad (void)
import           UProc (run, xdotool, xprop)

main :: IO ()
main = do
  window <- getwindowfocus
  status <- getTagInvert window
  case status of
    "TAG_INVERT:  not found."  -> setTagInvert window "1"
    "TAG_INVERT(CARDINAL) = 0" -> setTagInvert window "1"
    "TAG_INVERT(CARDINAL) = 1" -> setTagInvert window "0"
    _ -> error $ "Unknown value of 'TAG_INVERT': " ++ status

getwindowfocus :: IO String
getwindowfocus =
  run $ xdotool ["getwindowfocus"]

getTagInvert :: String -> IO String
getTagInvert window =
  run $ xprop ["-id", window, "8c", "TAG_INVERT"]

setTagInvert :: String -> String -> IO ()
setTagInvert window status =
  void $ run $
  xprop [ "-id", window
        , "-format", "TAG_INVERT", "8c"
        , "-set", "TAG_INVERT", status ]
