#!/usr/bin/env runhaskell
{--
:Module:	explore-zsend
:Description:	examine a ZFS send stream
:Stability:	experimental
:Portability:	*nix
:SPDX-License-Identifier: CDDL-1.0

  Portions derived from OpenZFS.
  Refines structures, enumerations, and constants defined in:
    + //zfs/include/sys/zfs_ioctl.h
      Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights reserved.
      Copyright (c) 2012, 2020 by Delphix. All rights reserved.
      Copyright (c) 2016 RackTop Systems.
      Copyright (c) 2017, Intel Corporation.

ZFS Send Streams, etc..

--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE LambdaCase #-}

module Main
where

{-& build-depends: base -}
import Data.Int
import Data.Word
{-& build-depends: bytestring -}
{-& build-depends: vector -}

{-& build-depends: attoparsec -}
import qualified Data.Attoparsec.ByteString as P
import qualified Data.Attoparsec.Combinators as P
{-& build-depends: vector-sized -}
import qualified Data.Vector.Sized as Sized

type U8  = Word8
type U16 = Word16
type U32 = Word32
type U64 = Word64
type I8  = Int8
type I16 = Int16
type I32 = Int32
type I64 = Int64

main :: IO ()
main = undefined

------------------------------------------------------------------------

_DMU_SUBSTREAM, _DMU_COMPOUNDSTREAM :: U32
_DMU_SUBSTREAM      = 0x1
_DMU_COMPOUNDSTREAM = 0x2


{--

/*
 * Field manipulation macros for the drr_versioninfo field of the
 * send stream header.
 */

/*
 * Header types for zfs send streams.
 */
typedef enum drr_headertype {
	DMU_SUBSTREAM = 0x1,
	DMU_COMPOUNDSTREAM = 0x2
} drr_headertype_t;

#define	DMU_GET_STREAM_HDRTYPE(vi)	BF64_GET((vi), 0, 2)
#define	DMU_SET_STREAM_HDRTYPE(vi, x)	BF64_SET((vi), 0, 2, x)

#define	DMU_GET_FEATUREFLAGS(vi)	BF64_GET((vi), 2, 30)
#define	DMU_SET_FEATUREFLAGS(vi, x)	BF64_SET((vi), 2, 30, x)

/*
 * Feature flags for zfs send streams (flags in drr_versioninfo)
 */

#define	DMU_BACKUP_FEATURE_DEDUP		(1 << 0)
#define	DMU_BACKUP_FEATURE_DEDUPPROPS		(1 << 1)
#define	DMU_BACKUP_FEATURE_SA_SPILL		(1 << 2)
/* flags #3 - #15 are reserved for incompatible closed-source implementations */
#define	DMU_BACKUP_FEATURE_EMBED_DATA		(1 << 16)
#define	DMU_BACKUP_FEATURE_LZ4			(1 << 17)
/* flag #18 is reserved for a Delphix feature */
#define	DMU_BACKUP_FEATURE_LARGE_BLOCKS		(1 << 19)
#define	DMU_BACKUP_FEATURE_RESUMING		(1 << 20)
#define	DMU_BACKUP_FEATURE_REDACTED		(1 << 21)
#define	DMU_BACKUP_FEATURE_COMPRESSED		(1 << 22)
#define	DMU_BACKUP_FEATURE_LARGE_DNODE		(1 << 23)
#define	DMU_BACKUP_FEATURE_RAW			(1 << 24)
#define	DMU_BACKUP_FEATURE_ZSTD			(1 << 25)
#define	DMU_BACKUP_FEATURE_HOLDS		(1 << 26)
/*
 * The SWITCH_TO_LARGE_BLOCKS feature indicates that we can receive
 * incremental LARGE_BLOCKS streams (those with WRITE records of >128KB) even
 * if the previous send did not use LARGE_BLOCKS, and thus its large blocks
 * were split into multiple 128KB WRITE records.  (See
 * flush_write_batch_impl() and receive_object()).  Older software that does
 * not support this flag may encounter a bug when switching to large blocks,
 * which causes files to incorrectly be zeroed.
 *
 * This flag is currently not set on any send streams.  In the future, we
 * intend for incremental send streams of snapshots that have large blocks to
 * use LARGE_BLOCKS by default, and these streams will also have the
 * SWITCH_TO_LARGE_BLOCKS feature set. This ensures that streams from the
 * default use of "zfs send" won't encounter the bug mentioned above.
 */
#define	DMU_BACKUP_FEATURE_SWITCH_TO_LARGE_BLOCKS (1 << 27)
/* flag #28 is reserved for a Nutanix feature */
/*
 * flag #29 is the last unused bit. It is reserved to indicate a to-be-designed
 * extension to the stream format which will accomodate more feature flags.
 * If you need to add another feature flag, please reach out to the OpenZFS
 * community, e.g., on GitHub or Slack.
 */

/*
 * Mask of all supported backup features
 */
#define	DMU_BACKUP_FEATURE_MASK	(DMU_BACKUP_FEATURE_SA_SPILL | \
    DMU_BACKUP_FEATURE_EMBED_DATA | DMU_BACKUP_FEATURE_LZ4 | \
    DMU_BACKUP_FEATURE_RESUMING | DMU_BACKUP_FEATURE_LARGE_BLOCKS | \
    DMU_BACKUP_FEATURE_COMPRESSED | DMU_BACKUP_FEATURE_LARGE_DNODE | \
    DMU_BACKUP_FEATURE_RAW | DMU_BACKUP_FEATURE_HOLDS | \
    DMU_BACKUP_FEATURE_REDACTED | DMU_BACKUP_FEATURE_SWITCH_TO_LARGE_BLOCKS | \
    DMU_BACKUP_FEATURE_ZSTD)

/* Are all features in the given flag word currently supported? */
#define	DMU_STREAM_SUPPORTED(x)	(!((x) & ~DMU_BACKUP_FEATURE_MASK))

typedef enum dmu_send_resume_token_version {
	ZFS_SEND_RESUME_TOKEN_VERSION = 1
} dmu_send_resume_token_version_t;

/*
 * The drr_versioninfo field of the dmu_replay_record has the
 * following layout:
 *
 *	64	56	48	40	32	24	16	8	0
 *	+-------+-------+-------+-------+-------+-------+-------+-------+
 *	|		reserved	|        feature-flags	    |C|S|
 *	+-------+-------+-------+-------+-------+-------+-------+-------+
 *
 * The low order two bits indicate the header type: SUBSTREAM (0x1)
 * or COMPOUNDSTREAM (0x2).  Using two bits for this is historical:
 * this field used to be a version number, where the two version types
 * were 1 and 2.  Using two bits for this allows earlier versions of
 * the code to be able to recognize send streams that don't use any
 * of the features indicated by feature flags.
 */

#define	DMU_BACKUP_MAGIC 0x2F5bacbacULL

/*
 * Send stream flags.  Bits 24-31 are reserved for vendor-specific
 * implementations and should not be used.
 */
#define	DRR_FLAG_CLONE		(1<<0)
#define	DRR_FLAG_CI_DATA	(1<<1)
/*
 * This send stream, if it is a full send, includes the FREE and FREEOBJECT
 * records that are created by the sending process.  This means that the send
 * stream can be received as a clone, even though it is not an incremental.
 * This is not implemented as a feature flag, because the receiving side does
 * not need to have implemented it to receive this stream; it is fully backwards
 * compatible.  We need a flag, though, because full send streams without it
 * cannot necessarily be received as a clone correctly.
 */
#define	DRR_FLAG_FREERECORDS	(1<<2)
/*
 * When DRR_FLAG_SPILL_BLOCK is set it indicates the DRR_OBJECT_SPILL
 * and DRR_SPILL_UNMODIFIED flags are meaningful in the send stream.
 *
 * When DRR_FLAG_SPILL_BLOCK is set, DRR_OBJECT records will have
 * DRR_OBJECT_SPILL set if and only if they should have a spill block
 * (either an existing one, or a new one in the send stream).  When clear
 * the object does not have a spill block and any existing spill block
 * should be freed.
 *
 * Similarly, when DRR_FLAG_SPILL_BLOCK is set, DRR_SPILL records will
 * have DRR_SPILL_UNMODIFIED set if and only if they were included for
 * backward compatibility purposes, and can be safely ignored by new versions
 * of zfs receive.  Previous versions of ZFS which do not understand the
 * DRR_FLAG_SPILL_BLOCK will process this record and recreate any missing
 * spill blocks.
 */
#define	DRR_FLAG_SPILL_BLOCK	(1<<3)

/*
 * flags in the drr_flags field in the DRR_WRITE, DRR_SPILL, DRR_OBJECT,
 * DRR_WRITE_BYREF, and DRR_OBJECT_RANGE blocks
 */
#define	DRR_CHECKSUM_DEDUP	(1<<0) /* not used for SPILL records */
#define	DRR_RAW_BYTESWAP	(1<<1)
#define	DRR_OBJECT_SPILL	(1<<2) /* OBJECT record has a spill block */
#define	DRR_SPILL_UNMODIFIED	(1<<2) /* SPILL record for unmodified block */

#define	DRR_IS_DEDUP_CAPABLE(flags)	((flags) & DRR_CHECKSUM_DEDUP)
#define	DRR_IS_RAW_BYTESWAPPED(flags)	((flags) & DRR_RAW_BYTESWAP)
#define	DRR_OBJECT_HAS_SPILL(flags)	((flags) & DRR_OBJECT_SPILL)
#define	DRR_SPILL_IS_UNMODIFIED(flags)	((flags) & DRR_SPILL_UNMODIFIED)

/* deal with compressed drr_write replay records */
#define	DRR_WRITE_COMPRESSED(drrw)	((drrw)->drr_compressiontype != 0)
#define	DRR_WRITE_PAYLOAD_SIZE(drrw) \
	(DRR_WRITE_COMPRESSED(drrw) ? (drrw)->drr_compressed_size : \
	(drrw)->drr_logical_size)
#define	DRR_SPILL_PAYLOAD_SIZE(drrs) \
	((drrs)->drr_compressed_size ? \
	(drrs)->drr_compressed_size : (drrs)->drr_length)
#define	DRR_OBJECT_PAYLOAD_SIZE(drro) \
	((drro)->drr_raw_bonuslen != 0 ? \
	(drro)->drr_raw_bonuslen : P2ROUNDUP((drro)->drr_bonuslen, 8))

/*
 * zfs ioctl command structure
 */

--}

type C_dmu_objset_type_t = U32

_MAXNAMELEN = 256

data C_drr_begin =
  C_drr_begin
  { _drr_magic         :: !U64
  , _drr_versioninfo   :: !U64
  , _drr_creation_time :: !U64
  , _drr_type          :: !C_dmu_objset_type_t
  , _drr_flags         :: !U32
  , _drr_toguid        :: !U64
  , _drr_fromguid      :: !U64
  , _drr_toname        :: !(Sized.Vector 256 U8)
  } deriving (Eq, Show)



{--

typedef struct dmu_replay_record {
	enum {
		DRR_BEGIN, DRR_OBJECT, DRR_FREEOBJECTS,
		DRR_WRITE, DRR_FREE, DRR_END, DRR_WRITE_BYREF,
		DRR_SPILL, DRR_WRITE_EMBEDDED, DRR_OBJECT_RANGE, DRR_REDACT,
		DRR_NUMTYPES
	} drr_type;
	U32 drr_payloadlen;
	union {
		struct drr_begin drr_begin;
		struct drr_end {
			zio_cksum_t drr_checksum;
			U64 drr_toguid;
		} drr_end;
		struct drr_object {
			U64 drr_object;
			dmu_object_type_t drr_type;
			dmu_object_type_t drr_bonustype;
			U32 drr_blksz;
			U32 drr_bonuslen;
			U8 drr_checksumtype;
			U8 drr_compress;
			U8 drr_dn_slots;
			U8 drr_flags;
			U32 drr_raw_bonuslen;
			U64 drr_toguid;
			/* only (possibly) nonzero for raw streams */
			U8 drr_indblkshift;
			U8 drr_nlevels;
			U8 drr_nblkptr;
			U8 drr_pad[5];
			U64 drr_maxblkid;
			/* bonus content follows */
		} drr_object;
		struct drr_freeobjects {
			U64 drr_firstobj;
			U64 drr_numobjs;
			U64 drr_toguid;
		} drr_freeobjects;
		struct drr_write {
			U64 drr_object;
			dmu_object_type_t drr_type;
			U32 drr_pad;
			U64 drr_offset;
			U64 drr_logical_size;
			U64 drr_toguid;
			U8 drr_checksumtype;
			U8 drr_flags;
			U8 drr_compressiontype;
			U8 drr_pad2[5];
			/* deduplication key */
			ddt_key_t drr_key;
			/* only nonzero if drr_compressiontype is not 0 */
			U64 drr_compressed_size;
			/* only nonzero for raw streams */
			U8 drr_salt[ZIO_DATA_SALT_LEN];
			U8 drr_iv[ZIO_DATA_IV_LEN];
			U8 drr_mac[ZIO_DATA_MAC_LEN];
			/* content follows */
		} drr_write;
		struct drr_free {
			U64 drr_object;
			U64 drr_offset;
			U64 drr_length;
			U64 drr_toguid;
		} drr_free;
		struct drr_write_byref {
			/* where to put the data */
			U64 drr_object;
			U64 drr_offset;
			U64 drr_length;
			U64 drr_toguid;
			/* where to find the prior copy of the data */
			U64 drr_refguid;
			U64 drr_refobject;
			U64 drr_refoffset;
			/* properties of the data */
			U8 drr_checksumtype;
			U8 drr_flags;
			U8 drr_pad2[6];
			ddt_key_t drr_key; /* deduplication key */
		} drr_write_byref;
		struct drr_spill {
			U64 drr_object;
			U64 drr_length;
			U64 drr_toguid;
			U8 drr_flags;
			U8 drr_compressiontype;
			U8 drr_pad[6];
			/* only nonzero for raw streams */
			U64 drr_compressed_size;
			U8 drr_salt[ZIO_DATA_SALT_LEN];
			U8 drr_iv[ZIO_DATA_IV_LEN];
			U8 drr_mac[ZIO_DATA_MAC_LEN];
			dmu_object_type_t drr_type;
			/* spill data follows */
		} drr_spill;
		struct drr_write_embedded {
			U64 drr_object;
			U64 drr_offset;
			/* logical length, should equal blocksize */
			U64 drr_length;
			U64 drr_toguid;
			U8 drr_compression;
			U8 drr_etype;
			U8 drr_pad[6];
			U32 drr_lsize; /* uncompressed size of payload */
			U32 drr_psize; /* compr. (real) size of payload */
			/* (possibly compressed) content follows */
		} drr_write_embedded;
		struct drr_object_range {
			U64 drr_firstobj;
			U64 drr_numslots;
			U64 drr_toguid;
			U8 drr_salt[ZIO_DATA_SALT_LEN];
			U8 drr_iv[ZIO_DATA_IV_LEN];
			U8 drr_mac[ZIO_DATA_MAC_LEN];
			U8 drr_flags;
			U8 drr_pad[3];
		} drr_object_range;
		struct drr_redact {
			U64 drr_object;
			U64 drr_offset;
			U64 drr_length;
			U64 drr_toguid;
		} drr_redact;

		/*
		 * Note: drr_checksum is overlaid with all record types
		 * except DRR_BEGIN.  Therefore its (non-pad) members
		 * must not overlap with members from the other structs.
		 * We accomplish this by putting its members at the very
		 * end of the struct.
		 */
		struct drr_checksum {
			U64 drr_pad[34];
			/*
			 * fletcher-4 checksum of everything preceding the
			 * checksum.
			 */
			zio_cksum_t drr_checksum;
		} drr_checksum;
	} drr_u;
} dmu_replay_record_t;

/* diff record range types */
typedef enum diff_type {
	DDR_NONE = 0x1,
	DDR_INUSE = 0x2,
	DDR_FREE = 0x4
} diff_type_t;

/*
 * The diff reports back ranges of free or in-use objects.
 */
typedef struct dmu_diff_record {
	U64 ddr_type;
	U64 ddr_first;
	U64 ddr_last;
} dmu_diff_record_t;

--}
