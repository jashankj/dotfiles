#!/usr/bin/env runhaskell
{--
:Module:	pdump-explore
:Description:	examine an Emacs portable-dumper file
:Stability:	experimental
:Portability:	*nix
:SPDX-License-Identifier: GPL-3.0-or-later

  Portions derived from GNU Emacs, which is copyright
  Copyright (C) Free Software Foundation, Inc.
  Contains constants defined in:
    + //emacs/src/pdumper.c
      Copyright (C) 2018-2022 Free Software Foundation, Inc.
    + //emacs/src/pdumper.h
      Copyright (C) 2016, 2018-2022 Free Software Foundation, Inc.

It seems there isn't any simple tool to dumps out pdmp files!

--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE LambdaCase #-}
{-& build-depends: base, bytestring -}
{-& build-depends: attoparsec, bytestring-conversion, mmap -}
{-& other-modules: PDumper -}

module Main
where

import           Control.Exception
import qualified Data.Attoparsec.ByteString as P
import           Data.Bits
import           Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import           Data.Int
import           Data.Word
import           Foreign.ForeignPtr
import           Foreign.Marshal
import           Foreign.Ptr
import           Foreign.StablePtr
import           Foreign.Storable
import           System.Environment
import           System.FilePath
import           System.IO.MMap

import qualified PDumper

main :: IO ()
main = getArgs >>= \case
  [dumpfile] -> main' dumpfile
  _ -> fail "usage: pdump-explore <emacs-dump-file.pdmp>"

main' :: FilePath -> IO ()
main' dumpfile = do
  fptr <- mmapFileByteString dumpfile Nothing
  putStrLn $ show $ P.parseOnly (PDumper.undump :: P.Parser PDumper.DumpHeader) fptr
