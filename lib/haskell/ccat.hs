#!/usr/bin/env runhaskell
{--
:Module:	ccat
:Description:	cat with comments.
:Stability:	experimental
:Portability:	*nix

Based on ``ccat`` (previously Shell + Sed).
--}

{-# LANGUAGE ApplicativeDo #-}
{-& build-depends: base, bytestring -}
{-& other-modules: UIO -}

module Main where

import           Control.Monad (unless)
import           Data.Char (isSpace)
import           System.Environment (getArgs)
import           System.IO (IOMode(ReadMode), openFile, stdin, stdout)

-- import           Control.Monad (forM_)
-- import qualified Data.ByteString.Lazy as LBS
-- import           Text.RE.TDFA
-- import           Text.RE.Tools

import qualified UIO

main :: IO ()
main = getArgs >>= main'
  where main' []     = UIO.interactMP stdin stdout ccat
        main' (x:xs) = do
          fh <- x `openFile` ReadMode
          UIO.interactMP fh stdout ccat
          unless (null xs) $ main' xs

ccat :: String -> IO (Maybe String)
ccat x =
  let x' = stripLeadingSpace . stripHashComment $ x
  in pure $ if null x' then Nothing else Just x'
  where
  stripHashComment = takeWhile (/= '#')
  stripLeadingSpace = dropWhile isSpace

-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE QuasiQuotes #-}
-- {- other-modules: regex #-}
--
-- main = getArgs >>= main'
--   where
--     main' [] =                  sed ccat "-" "-"
--     main' xs = forM_ xs $ \f -> sed ccat  f  "-"
--
-- ccat :: Edits IO RE LBS.ByteString
-- ccat =
--   Select
--     [ Template $ SearchReplace [reBS|#.*|] mempty
--     , LineEdit [reBS|^[[:space:]]*$|] $ \_ _ -> pure Delete ]
