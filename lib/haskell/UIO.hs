{--
:Module:	UIO
:Description:  Utilities for various simple scripts.
:Stability:	experimental
:Portability:	*nix
--}

{-# LANGUAGE LambdaCase #-}

module UIO
  ( interact, interactM, interactMP
  , loadTable
  )
where

import           Prelude hiding (interact)

import           Data.Functor
import           Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as C
import           System.IO (Handle, hGetLine, hIsEOF, hPutStrLn)

interact :: Handle -> Handle -> (String -> String) -> IO ()
interact hi ho f = interactM hi ho (pure . f)

interactM :: Handle -> Handle -> (String -> IO String) -> IO ()
interactM hi ho f = interactMP hi ho $ fmap Just . f

interactMP :: Handle -> Handle -> (String -> IO (Maybe String)) -> IO ()
interactMP hi ho f =
  hIsEOF hi >>= \case
    True  -> pure ()
    False -> hGetLine hi >>= f >>= (\case
      Just x  -> hPutStrLn ho x
      Nothing -> pure ()) >> interactMP hi ho f

-- | Load tabular data from 'fh', using 'fs' as the field separator and
--   'rs' as the record separator.
loadTable :: Char -> Char -> Handle -> IO [[ByteString]]
loadTable fs rs fh =
  C.hGetContents fh <&> map (C.split fs) . C.split rs
