{--
:Module:       UProc
:Description:  utilities for invoking subprocesses
:Stability:    experimental
:Portability:  *nix

--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}

module UProc
  ( proc, run
  , xdotool, xprop, xrandr
  )
where

import           Control.Monad (guard)
import qualified Data.ByteString.Lazy.Char8 as LC
import           System.Process.Typed

xdotool, xprop, xrandr :: [String] -> ProcessConfig () () ()
xdotool = proc "xdotool"
xprop   = proc "xprop"
xrandr  = proc "xrandr"

run :: ProcessConfig i o e -> IO String
run k = do
  (es, ret') <- readProcessStdout k
  guard (es == ExitSuccess)
  case lines . LC.unpack $ ret' of
    (x:_) -> pure x
    []    -> fail "no input?"
