#!/usr/bin/env runhaskell
{--
:Module:	update-signal
:Description:	fetch and install a new version of Signal
:Stability:	experimental
:Portability:	*nix

--}

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-& build-depends: base, bytestring, text, debian, req -}

module Main where

import           Control.Monad.IO.Class
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as C
import           Data.Either
import           Data.Functor
import           Data.List
import           Data.Maybe
import qualified Data.Text as T
import           Data.Text (Text)
import           System.FilePath

import qualified Debian.Apt.Index as Index
import           Debian.Control hiding (Control, Paragraph, Field)
import           Debian.Control (Field'(Field))
import           Debian.Version
import           Debian.Version.Text ()
import           Network.HTTP.Req

type Control   = Control' T.Text
type Paragraph = Paragraph' T.Text
type Field     = Field' T.Text

{-- Be nice if we could just use:

import           Debian.Sources
source = parseSourceLine []
  "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main"

--}

_pkg, _arch, _deb_dist, _deb_comp :: Text
_pkg     = "signal-desktop"
_arch     = "amd64"
_deb_dist = "xenial"
_deb_comp = "main"

_deb_uri, f_release, f_package :: Url 'Https
_deb_uri  = https "updates.signal.org" /: "desktop" /: "apt"
f_release = _deb_uri /: "dists" /: _deb_dist /: "InRelease"
f_package = _deb_uri /: "dists" /: _deb_dist /:
            _deb_comp /: (T.concat ["binary-", _arch]) /: "Packages"

main :: IO ()
main = runReq defaultHttpConfig $ do
  idx <- loadIndex . responseBody <$>
         req GET f_package NoReqBody lbsResponse mempty
  let pkg  = indexSelect idx
  let file = snd . fromJust $ lookupP' "Filename" pkg
  deb <- responseBody <$>
         req GET (_deb_uri /: file) NoReqBody lbsResponse mempty
  let out = takeFileName $ T.unpack file
  liftIO $ BS.writeFile out deb
  liftIO $ putStrLn out

  -- bsdtar xOf "${file}" data.tar.xz |
  -- sudo bsdtar -C / -xvf - opt/Signal

loadIndex :: C.ByteString -> [Paragraph]
loadIndex idx
   = unControl
  <$> fromRight undefined
   $ Index.controlFromIndex Index.Uncompressed "-" idx

pickNewest :: [Paragraph] -> Paragraph
pickNewest = last . sortOn xversion
  where
    xversion = parseDebianVersion' . snd . fromJust . lookupP' "Version"

indexSelect :: [Paragraph] -> Paragraph
indexSelect
  = pickNewest
  . indexSelect' "Package" ((== _pkg) . snd)
  . indexSelect' "Architecture" ((== _arch) . snd)

indexSelect' :: String -> ((Text, Text) -> Bool) -> [Paragraph] -> [Paragraph]
indexSelect' fld cond =
  filter $
  \par -> isJust $ lookupP' fld par <&> cond

lookupP' :: String -> Paragraph -> Maybe (Text, Text)
lookupP' fieldname paragraph =
  lookupP fieldname paragraph >>=
  \case { Field (k, v) -> Just (k, stripWS v); _ -> Nothing }
