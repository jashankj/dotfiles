#
# NAME: J::Proc --- thin convenience hack veneer over IPC::Open3
# SYNOPSIS:
#
#	use FindBin;
#	use lib "$FindBin::Bin/../lib/perl";
#	use J::Proc;
#	$J::Proc::VERBOSE_IO = 0;
#
#	my $z3 = J::Proc->spawn('z3', '-in');
#	$z3->send(...);
#	$z3->recv(...); $z3->getline;
#	$z3->close_in; $z3->close_out; $z3->close_err;
#	$z3->waitpid;
#
#-
# SPDX-License-Identifier: BSD-2-Clause
# Copyright (c) 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
#

package J::Proc;

use v5.34;
use strict;
use warnings;

use Exporter 'import';
our $VERSION   = '0.1.0';
our @EXPORT    = qw(spawn);
our @EXPORT_OK = qw($VERBOSE_IO);

use Carp;

use IO::Handle;
use IPC::Open3;
use Symbol 'gensym';

our $VERBOSE_IO = 0;

sub spawn
{
	my $class = shift;
	my $self = {};
	bless $self, $class;
	return $self->_spawn(@_);
}

sub _spawn
{
	my $self = shift;
	$self->{iam} = $_[0];
	$self->{pid} = open3(
		$self->{in},
		$self->{out},
		$self->{err} = gensym,
		@_
	);

	foreach ($self->{in}, $self->{out}, $self->{err}) {
		$_->binmode(':unix');
		$_->autoflush(1); # but still not enough!
	}

	$self
}

sub _io_log
{
	return unless $VERBOSE_IO;
	my $self = shift;
	my $dir  = shift;
	my $ident = sprintf '[%s %s]', $self->{iam}, $dir;
	carp "$ident @_";
}

sub send
{
	my $self = shift;
	$self->_io_log('0>', @_);
	$self->{in}->print(@_);
	$self->{in}->flush;
}

sub recv
{
	my $self = shift;
	my $l = '';
	$self->{out}->read($l, 4096);
	$self->_io_log('1<', $l);
	$l
}

sub getline
{
	my $self = shift;
	my $l = $self->{out}->getline;
	$self->_io_log('1<', (defined($l) ? $l : '<undef>'));
	$l
}

sub close_in
{
	my $self = shift;
	$self->_io_log('0:', 'close');
	$self->{in}->close;
}

sub close_out
{
	my $self = shift;
	$self->_io_log('1:', 'close');
	$self->{out}->close;
}

sub close_err
{
	my $self = shift;
	$self->_io_log('2:', 'close');
	$self->{err}->close;
}

sub waitpid
{
	my $self = shift;
	waitpid $self->{pid}, 0;
	$? >> 8
}

1;
