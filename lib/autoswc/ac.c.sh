#!/hint/sh
# Autoconf hints for `autoconf-2.71/autoconf/c.m4'.

ac_cv_c_compiler_gnu=${ac_cv_c_compiler_gnu=yes}
ac_cv_cxx_compiler_gnu=${ac_cv_cxx_compiler_gnu=yes}
ac_cv_objc_compiler_gnu=${ac_cv_objc_compiler_gnu=yes}
ac_cv_objcxx_compiler_gnu=${ac_cv_objcxx_compiler_gnu=yes}

ac_cv_c_backslash_a=${ac_cv_c_backslash_a=yes}
ac_cv_c_bigendian=${ac_cv_c_bigendian=no}
ac_cv_c_char_unsigned=${ac_cv_c_char_unsigned=no}
ac_cv_c_const=${ac_cv_c_const=yes}
ac_cv_c_flexmember=${ac_cv_c_flexmember=yes}
ac_cv_c_inline=${ac_cv_c_inline=inline}
ac_cv_c_restrict=${ac_cv_c_restrict=__restrict__}
ac_cv_c_stringize=${ac_cv_c_stringize=yes}
ac_cv_c_typeof=${ac_cv_c_typeof=typeof}
ac_cv_c_vararrays=${ac_cv_c_vararrays=yes}
ac_cv_c_volatile=${ac_cv_c_volatile=yes}
ac_cv_prog_CPP=${ac_cv_prog_CPP="$CC -E"}
ac_cv_prog_cc_stdc=${ac_cv_prog_cc_stdc=}
ac_cv_prog_gcc=${ac_cv_prog_gcc=yes}
ac_cv_prog_gcc_traditional=${ac_cv_prog_gcc_traditional=no}
ac_cv_prog_cc_c11=${ac_cv_prog_cc_c11=}
ac_cv_prog_cc_c89=${ac_cv_prog_cc_c89=}
ac_cv_prog_cc_c99=${ac_cv_prog_cc_c99=}

ac_cv_prog_CXXCPP=${ac_cv_prog_CXXCPP="$CXX -E"}
ac_cv_prog_cxx_g=${ac_cv_prog_cxx_g=yes}
ac_cv_prog_cxx_stdcxx=${ac_cv_prog_cxx_stdcxx=}
#ac_cv_prog_cxx_c_o=${ac_cv_prog_cxx_c_o=yes}
ac_cv_prog_gxx=${ac_cv_prog_gxx=yes}
