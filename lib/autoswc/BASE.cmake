# My preferred build environment.

set(CMAKE_C_COMPILER "/usr/lib64/ccache/clang" CACHE FILEPATH "" FORCE)
set(CMAKE_C_FLAGS "-g -O2 -flto" CACHE STRING "" FORCE)

set(CMAKE_CXX_COMPILER "/usr/lib64/ccache/clang++" CACHE FILEPATH "" FORCE)
set(CMAKE_CXX_FLAGS "-g -O2 -flto" CACHE STRING "" FORCE)

set(CMAKE_LINKER "/usr/bin/ld.lld" CACHE FILEPATH "" FORCE)
set(CMAKE_EXE_LINKER_FLAGS "-g -O2 -flto -fuse-ld=lld" CACHE STRING "" FORCE)
set(CMAKE_SHARED_LINKER_FLAGS "-g -O2 -flto -fuse-ld=lld" CACHE STRING "" FORCE)

set(CMAKE_ADDR2LINE "/usr/bin/llvm-addr2line" CACHE FILEPATH "" FORCE)
set(CMAKE_AR "/usr/bin/llvm-ar" CACHE FILEPATH "" FORCE)
set(CMAKE_AS "/usr/bin/llvm-as" CACHE FILEPATH "" FORCE)
set(CMAKE_CXXFILT "/usr/bin/llvm-cxxfilt" CACHE FILEPATH "" FORCE)
set(CMAKE_DLLTOOL "/usr/bin/llvm-dlltool" CACHE FILEPATH "" FORCE)
set(CMAKE_DWP "/usr/bin/llvm-dwp" CACHE FILEPATH "" FORCE)
set(CMAKE_NM "/usr/bin/llvm-nm" CACHE FILEPATH "" FORCE)
set(CMAKE_OBJCOPY "/usr/bin/llvm-objcopy" CACHE FILEPATH "" FORCE)
set(CMAKE_OBJDUMP "/usr/bin/llvm-objdump" CACHE FILEPATH "" FORCE)
set(CMAKE_RANLIB "/usr/bin/llvm-ranlib" CACHE FILEPATH "" FORCE)
set(CMAKE_READELF "/usr/bin/llvm-readelf" CACHE FILEPATH "" FORCE)
set(CMAKE_SIZE "/usr/bin/llvm-size" CACHE FILEPATH "" FORCE)
set(CMAKE_STRIP "/usr/bin/llvm-strip" CACHE FILEPATH "" FORCE)
set(CMAKE_WINDRES "/usr/bin/llvm-windres" CACHE FILEPATH "" FORCE)

set(CMAKE_SHELL "/bin/dash" CACHE FILEPATH "" FORCE)

set(CMAKE_AWK "/usr/bin/gawk" CACHE FILEPATH "" FORCE)
set(CMAKE_M4 "/usr/bin/m4" CACHE FILEPATH "" FORCE)

set(CMAKE_MITSCHEME_INTERPRETER "/opt/local/bin/mit-scheme" CACHE FILEPATH "" FORCE)
set(CMAKE_INSTALL_DO_STRIP OFF CACHE BOOL "" FORCE)
set(BUILD_SHARED_LIBS ON CACHE BOOL "" FORCE)
set(CMAKE_SKIP_RPATH ON CACHE BOOL "" FORCE)
set(CMAKE_USE_RELATIVE_PATHS ON CACHE BOOL "" FORCE)
