#!/usr/bin/env zsh
# Write out my preferred build environment ---
# I want LLVM + Clang.

exec 3>BASE.sh
exec 4>BASE.cmake

function s() { print -u3 "$@" }
function c() { print -u4 "$@" }
function cv() {
	case "$2" in
		(STRING|FILEPATH)
			c "set($1 \"$3\" CACHE $2 \"\" FORCE)"
			;;
		(*)
			c "set($1 $3 CACHE $2 \"\" FORCE)"
			;;
	esac
}

s "#!/hint/sh"
s "# My preferred build environment."
s ''

c "# My preferred build environment."
c ''

# -D CMAKE_BUILD_TYPE=Production \

ccache=/usr/lib64/ccache

########################################################################
# Clang and LLVM toolchain:

s "CC='ccache clang'"
cv CMAKE_C_COMPILER FILEPATH "$ccache/clang"
s "CFLAGS='-g -O2 -flto'"
cv CMAKE_C_FLAGS STRING '-g -O2 -flto'
s ''
c ''

s "CXX='ccache clang++'"
cv CMAKE_CXX_COMPILER FILEPATH "$ccache/clang++"
s "CXXFLAGS='-g -O2 -flto'"
cv CMAKE_CXX_FLAGS STRING '-g -O2 -flto'
s ''
c ''

s 'LD=ld.lld'
cv CMAKE_LINKER FILEPATH $(which ld.lld)
s "LDFLAGS='-g -O2 -flto -fuse-ld=lld'"
cv CMAKE_EXE_LINKER_FLAGS STRING '-g -O2 -flto -fuse-ld=lld'
cv CMAKE_SHARED_LINKER_FLAGS STRING '-g -O2 -flto -fuse-ld=lld'
s ''
c ''

s 'ADDR2LINE=llvm-addr2line'
cv CMAKE_ADDR2LINE FILEPATH $(which llvm-addr2line)

s 'AR=llvm-ar'
cv CMAKE_AR FILEPATH $(which llvm-ar)

s 'AS=llvm-as'
cv CMAKE_AS FILEPATH $(which llvm-as)

s 'CXXFILT=llvm-cxxfilt'
cv CMAKE_CXXFILT FILEPATH $(which llvm-cxxfilt)

s 'DLLTOOL=llvm-dlltool'
cv CMAKE_DLLTOOL FILEPATH $(which llvm-dlltool)

s 'DWP=llvm-dwp'
cv CMAKE_DWP FILEPATH $(which llvm-dwp)

s 'NM=llvm-nm'
cv CMAKE_NM FILEPATH $(which llvm-nm)

s 'OBJCOPY=llvm-objcopy'
cv CMAKE_OBJCOPY FILEPATH $(which llvm-objcopy)

s 'OBJDUMP=llvm-objdump'
cv CMAKE_OBJDUMP FILEPATH $(which llvm-objdump)

s 'RANLIB=llvm-ranlib'
cv CMAKE_RANLIB FILEPATH $(which llvm-ranlib)

s 'READELF=llvm-readelf'
cv CMAKE_READELF FILEPATH $(which llvm-readelf)

s 'SIZE=llvm-size'
cv CMAKE_SIZE FILEPATH $(which llvm-size)

s 'STRIP=llvm-strip'
cv CMAKE_STRIP FILEPATH $(which llvm-strip)

s 'WINDRES=llvm-windres'
cv CMAKE_WINDRES FILEPATH $(which llvm-windres)

s ''
c ''

########################################################################
# other odds and ends

s 'SHELL=/bin/dash'
s 'CONFIGURE_SHELL=/bin/dash'
cv CMAKE_SHELL FILEPATH /bin/dash

s ''
c ''

s 'AWK=gawk'
cv CMAKE_AWK FILEPATH $(which gawk)
s 'M4=m4'
cv CMAKE_M4 FILEPATH $(which m4)

s ''
c ''

s 'MIT_SCHEME_EXE=/opt/local/bin/mit-scheme'
cv CMAKE_MITSCHEME_INTERPRETER FILEPATH "/opt/local/bin/mit-scheme"

########################################################################
cv CMAKE_INSTALL_DO_STRIP	BOOL OFF
cv BUILD_SHARED_LIBS		BOOL ON
cv CMAKE_SKIP_RPATH		BOOL ON
cv CMAKE_USE_RELATIVE_PATHS	BOOL ON
