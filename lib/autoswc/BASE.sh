#!/hint/sh
# My preferred build environment.

CC='ccache clang'
CFLAGS='-g -O2 -flto'

CXX='ccache clang++'
CXXFLAGS='-g -O2 -flto'

LD=ld.lld
LDFLAGS='-g -O2 -flto -fuse-ld=lld'

ADDR2LINE=llvm-addr2line
AR=llvm-ar
AS=llvm-as
CXXFILT=llvm-cxxfilt
DLLTOOL=llvm-dlltool
DWP=llvm-dwp
NM=llvm-nm
OBJCOPY=llvm-objcopy
OBJDUMP=llvm-objdump
RANLIB=llvm-ranlib
READELF=llvm-readelf
SIZE=llvm-size
STRIP=llvm-strip
WINDRES=llvm-windres

SHELL=/bin/dash
CONFIGURE_SHELL=/bin/dash

AWK=gawk
M4=m4

MIT_SCHEME_EXE=/opt/local/bin/mit-scheme
