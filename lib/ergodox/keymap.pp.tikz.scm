;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; TeX/TikZ pretty printer

(define-module (keymap pp tikz)
  #:use-module (ice-9 format) ;; srfi/48 ;; format-intermediate
  #:use-module (ice-9 match) ;;  racket/match
  #:use-module (srfi srfi-13)   ;; strings
;;#:use-module racket/port
;;#:use-module racket/trace

  #:use-module (keymap util)
  #:use-module (keymap chars)

  #:export
  ( keymap-->tex
    keymap-->tikzpicture ))

(define (keymap-->tex caption)
  (λ keys
    (string-append
     "\\input keymap--common.tex\n"
     (latex/env "document" #f
      (apply (keymap-->tikzpicture caption) keys))
     )))

(define keymap-tikzpicture-options
  (unlines
   ""
   "  help lines/.style = { xstep = 1cm, ystep = 1cm, line width=0.1pt, gray },"
   "  k/.style = {"
   "    font = {\\sffamily},"
   ;; Positioning#: use a corner as a reference frame.
   "    anchor = north west,"
   ;; Always draw the key box.
   "    draw, line width = 1pt,"
   ;; Allow the box to be as tightly packed as possible.
   "    inner sep = 0pt,"
   ;; Set minimum dimensions.
   "    minimum width = 1cm,"
   "    minimum height = 1cm,"
   "  },"
   "  W/.style = { k, minimum width  = 2cm },"
   "  T/.style = { k, minimum height = 2cm },"
   "  t/.style = { k, minimum height = 1.5cm },"))

(define (keymap-->tikzpicture caption)
  (lambda keys
    (apply
     keymap-->tikzpicture-do
     (cons caption (map key-->tex keys)))))

(define (keymap-->tikzpicture-do
         caption
         k00 k01 k02 k03 k04 k05 k06
         k10 k11 k12 k13 k14 k15 k16
         k20 k21 k22 k23 k24 k25
         k30 k31 k32 k33 k34 k35 k36
         k40 k41 k42 k43 k44
             k55 k56
                 k54
         k53 k52 k51
         k07 k08 k09 k0A k0B k0C k0D
         k17 k18 k19 k1A k1B k1C k1D
             k28 k29 k2A k2B k2C k2D
         k37 k38 k39 k3A k3B k3C k3D
                 k49 k4A k4B k4C k4D
         k57 k58
         k59
         k5C k5B k5A)

  (latex/env "center" #f
    (latex/env "tikzpicture" keymap-tikzpicture-options
      (draw-left
       k00 k01 k02 k03 k04 k05 k06
       k10 k11 k12 k13 k14 k15 k16
       k20 k21 k22 k23 k24 k25
       k30 k31 k32 k33 k34 k35 k36
       k40 k41 k42 k43 k44)
      (draw-lthumb
       k55 k56 k54 k53 k52 k51)
      (draw-right
       k07 k08 k09 k0A k0B k0C k0D
       k17 k18 k19 k1A k1B k1C k1D
           k28 k29 k2A k2B k2C k2D
       k37 k38 k39 k3A k3B k3C k3D
               k49 k4A k4B k4C k4D)
      (draw-rthumb k57 k58 k59 k5C k5B k5A)
    )
    (string-append "\n\\vspace*{4cm}\\par{ " caption " }")
  ))

(define (draw-left
         k00 k01 k02 k03 k04 k05 k06
         k10 k11 k12 k13 k14 k15 k16
         k20 k21 k22 k23 k24 k25
         k30 k31 k32 k33 k34 k35 k36
         k40 k41 k42 k43 k44)
  (tikz/scope
   "shift = {(0, 3)}" ;; "transform canvas = {shift = {(0, 3)}}"
   (draw-left-k0 k00 k01 k02 k03 k04 k05 k06)
   (draw-left-k1 k10 k11 k12 k13 k14 k15 k16)
   (draw-left-k2 k20 k21 k22 k23 k24 k25)
   (draw-left-k3 k30 k31 k32 k33 k34 k35 k36)
   (draw-left-k4 k40 k41 k42 k43 k44)))

(define (draw-left-k0 k00 k01 k02 k03 k04 k05 k06)
  (kdraw-wide-then `(0  4) k00 k01 k02 k03 k04 k05 k06))
(define (draw-left-k1 k10 k11 k12 k13 k14 k15 k16)
  (string-append
   (kdraw-wide-then `(0  3) k10 k11 k12 k13 k14 k15)
   (draw `(@ (7  3) ,(node-mid k16)))))
(define (draw-left-k2 k20 k21 k22 k23 k24 k25)
  (kdraw-wide-then `(0  2) k20 k21 k22 k23 k24 k25))
(define (draw-left-k3 k30 k31 k32 k33 k34 k35 k36)
  (string-append
   (kdraw-wide-then `(0  1) k30 k31 k32 k33 k34 k35)
   (draw `(@ (7 1.5) ,(node-mid k36)))))
(define (draw-left-k4 k40 k41 k42 k43 k44)
  (kdraw* `(1  0) k40 k41 k42 k43 k44))

(define (draw-lthumb k55 k56 k54 k53 k52 k51)
  (tikz/scope
   "transform canvas = {shift = {(4.5, 1)}, rotate = -30}"
   (draw `(@ (1.0  2.0) ,(node-key  k55)))
   (draw `(@ (2.0  2.0) ,(node-key  k56)))
   (draw `(@ (2.0  1.0) ,(node-key  k54)))
   (draw `(@ (0.0  1.0) ,(node-tall k53)))
   (draw `(@ (1.0  1.0) ,(node-tall k52)))
   (draw `(@ (2.0  0.0) ,(node-key  k51)))
   ))

(define (draw-right
         k07 k08 k09 k0A k0B k0C k0D
         k17 k18 k19 k1A k1B k1C k1D
             k28 k29 k2A k2B k2C k2D
         k37 k38 k39 k3A k3B k3C k3D
                 k49 k4A k4B k4C k4D)
  (tikz/scope
   "shift = {(9, 3)}" ;; "transform canvas = {shift = {(9, 3)}}"
   (draw-right-k0  k07 k08 k09 k0A k0B k0C k0D)
   (draw-right-k1  k17 k18 k19 k1A k1B k1C k1D)
   (draw-right-k2      k28 k29 k2A k2B k2C k2D)
   (draw-right-k3  k37 k38 k39 k3A k3B k3C k3D)
   (draw-right-k4          k49 k4A k4B k4C k4D)))


(define (draw-right-k0  k07 k08 k09 k0A k0B k0C k0D)
  (string-append
   (kdraw*  `(0  4) k07 k08 k09 k0A k0B k0C)
   (draw `(@ (6  4) ,(node-wide k0D)))))
(define (draw-right-k1  k17 k18 k19 k1A k1B k1C k1D)
  (string-append
   (draw `(@ (0  3) ,(node-mid k17)))
   (kdraw*  `(1  3)     k18 k19 k1A k1B k1C)
   (draw `(@ (6  3) ,(node-wide k1D)))))
(define (draw-right-k2      k28 k29 k2A k2B k2C k2D)
  (string-append
   (kdraw*  `(1  2)     k28 k29 k2A k2B k2C)
   (draw `(@ (6  2) ,(node-wide k2D)))))
(define (draw-right-k3  k37 k38 k39 k3A k3B k3C k3D)
  (string-append
   (draw `(@ (0 1.5) ,(node-mid k37)))
   (kdraw*  `(1  1)     k38 k39 k3A k3B k3C)
   (draw `(@ (6  1)  ,(node-wide k3D)))))
(define (draw-right-k4          k49 k4A k4B k4C k4D)
  (kdraw* `(2  0) k49 k4A k4B k4C k4D))

(define (draw-rthumb k57 k58 k59 k5C k5B k5A)
  (tikz/scope
   "transform canvas = {shift = {(9.9, -0.5)}, rotate = 30}"
   (draw `(@ (0.0  2.0) ,(node-key  k57)))
   (draw `(@ (1.0  2.0) ,(node-key  k58)))
   (draw `(@ (0.0  1.0) ,(node-key  k59)))
   (draw `(@ (0.0  0.0) ,(node-key  k5C)))
   (draw `(@ (1.0  1.0) ,(node-tall k5B)))
   (draw `(@ (2.0  1.0) ,(node-tall k5A)))
   ))

#|
,r (submod (file "keymap.scm") keymap/util)
,r (submod (file "keymap.scm") keymap/chars)
,r (submod (file "keymap.scm") keymap/pp/tex)
,r (submod (file "keymap.scm") keymap/pp/c)
,r (submod (file "keymap.scm") keymap)
,r (submod (file "keymap.scm") keymap/layouts)
(display (with-keymap-do keymap:dvorak-base (keymap-->tikzpicture "\\texttt{L\\\\_DVORAK\\\\_BASE}")))
|#

(define (tikz/scope opts . rest)
  (apply latex/env `(scope ,opts ,@rest)))

(define (latex/env name opts . body)
  (let ((o (open-output-string)))
    (format o "\\begin{~a}" name)
    (when opts
      (format o "[~a]" opts))
    (format o "~%")
    (match body
      ((xs ...)      (format o "~a" (apply unlines xs)))
      ((? string? _) (format o "~a" body)))
    (format o "~%")
    (format o "\\end{~a}" name)
    (get-output-string o)))

(define (kdraw* pt . ks)
  (apply
   draw
   `(( @ ,pt ,(node-key (car ks)))
     ,@(kdraw--rest (cdr ks))
     )))
(define (kdraw-wide-then pt k0 . ks)
  (apply
   draw
   `(( @ ,pt                   ,(node-wide k0))
     (++ (2  0)                ,(node-key  (car ks)))
     ,@(kdraw--rest (cdr ks)))))
(define (kdraw--rest ks)
  (map (lambda (k) `(++ (1  0) ,(node-key k))) ks))

(define (draw . rest)
  (define (point where)
    (string-append "(" (string-join (map number->string where) ", ") ")"))
  (define cmd
    (match-lambda*
      (('@ (where ...) what)
       (string-append (point where) " " what))
      (('++ (where ...) what)
       (string-append "++ " (point where) " " what))))
  (define (pad-cmd x) (string-append "  " (apply cmd x)))
  (apply unlines `("\\draw" ,@(map pad-cmd rest) ";")))

(define (node-wide inner)
  (string-append "node[W] {" inner "}"))
(define (node-key  inner)
  (string-append "node[k] {" inner "}"))
(define (node-mid  inner)
  (string-append "node[t] {" inner "}"))
(define (node-tall inner)
  (string-append "node[T] {" inner "}"))
