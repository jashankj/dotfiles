;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Low-level utility functions.

(define-module (keymap util)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 format) ;; (srfi/48) ;; format-intermediate
  #:use-module (ice-9 match)  ;; (racket/match)
  #:use-module (srfi srfi-41)   ;; streams

  #:export
  ( keyterm->string
    number->hex
    number->HEX
    dict-ref
    cartesian-product
    stream:map
    stream:filter
    exclusive-range
    tex/math
    tex/bracketed
    tex/⟨-⟩ )

  #:use-module ((jashankj util dict)   #:select (plist->alist))
  #:use-module ((jashankj util string) #:select ((string:unlines . unlines)))
  #:re-export
  ( plist->alist
    unlines )
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define keyterm->string
  (match-lambda
    ((? string? it) it)
    (#f "")
    (lst (format #f "~s" lst))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (number->hex x)
  (number->string x 16))
(define number->HEX
  (compose string-upcase number->hex))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Compatibility with Racket:

(define* (dict-ref dict key #:optional (fail (const #f)))
  (let ((ref (assq key dict)))
    (if (eq? ref #f)
        (if (procedure? fail) (fail) fail)
        (cdr ref))))

(define (cartesian-product xs ys)
  (stream-of (list x y)
             (x in xs)
             (y in ys)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Functor-esque glue on streams.

(define ((stream:map    f) s)
  (stream-map    f s))
(define ((stream:filter f) s)
  (stream-filter f s))
(define exclusive-range stream-range)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TeX miscellanea.

(define (tex/math x)
  (string-append "$" x "$"))
(define (tex/bracketed l r x)
  (string-append "\\left" l " " x "\\right" r))
(define (tex/⟨-⟩ x)
  (tex/bracketed "\\langle" "\\rangle" x))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
