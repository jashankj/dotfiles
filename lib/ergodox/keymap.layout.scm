;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Keyboard layouts:

(define-module (keymap layout)
  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 format) ;; srfi/48 ;; format-intermediate
  #:use-module (ice-9 local-eval)
  #:use-module (ice-9 match)  ;; racket/match
  #:use-module (srfi srfi-1)    ;; lists
  #:use-module (srfi srfi-41)   ;; streams

  #:use-module (keymap chars)
  #:use-module (keymap util)

  #:export
  ( make-keymap! ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define* (format-key #:rest xs)
  (apply string-append "k" (map number->HEX xs)))

(define extant-keys
  ((compose
    stream->list
    (stream:filter
     (match-lambda
       ((or 'k26 'k45 'k46 'k27 'k47 'k48 'k50 'k5D) #f)
       (_ #t)))
    (stream:map string->symbol)
    (stream:map (match-lambda ((r c) (format-key r c)))))
   (cartesian-product
    (stream-range #x0 #x6)
    (stream-range #x0 #xE))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define make-keymap!
  (local-eval
   `(lambda* (#:key ,@(append-map (lambda (ksym) `((,ksym #t))) extant-keys))
      ;; (lambda* (#:key (k00 #t) (k01 #t) ...)  ...)
      (map
       (match-lambda
         ((k ks ...)
          (cons k (map
                   (match-lambda
                     ((? symbol? x)  (char-table-get* x))
                     ((? char?   x)  (char-table-get* x))
                     (#t             (char-table-get* #t))
                     (#f             (char-table-get* #f))
                     ((? list?   x)  (plist->alist    x)))
                   ks))))
       `((#:left
          ,k00 ,k01 ,k02 ,k03 ,k04 ,k05 ,k06
          ,k10 ,k11 ,k12 ,k13 ,k14 ,k15 ,k16
          ,k20 ,k21 ,k22 ,k23 ,k24 ,k25
          ,k30 ,k31 ,k32 ,k33 ,k34 ,k35 ,k36
          ,k40 ,k41 ,k42 ,k43 ,k44)
         (#:lthumb ,k55 ,k56
                  ,k54
                  ,k53 ,k52 ,k51)
         (#:right
          ,k07 ,k08 ,k09 ,k0A ,k0B ,k0C ,k0D
          ,k17 ,k18 ,k19 ,k1A ,k1B ,k1C ,k1D
          ,k28 ,k29 ,k2A ,k2B ,k2C ,k2D
          ,k37 ,k38 ,k39 ,k3A ,k3B ,k3C ,k3D
          ,k49 ,k4A ,k4B ,k4C ,k4D)
         (#:rthumb
          ,k57 ,k58
          ,k59
          ,k5C ,k5B ,k5A))))
   (the-environment)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
