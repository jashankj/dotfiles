;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; C layout printer

(define-module (keymap pp c)
  #:use-module (ice-9 format) ;; srfi/48 ;; format-intermediate
  #:use-module (ice-9 match)  ;; (racket/match)
  #:use-module (srfi srfi-13)   ;; strings
  #:use-module (srfi srfi-41)   ;; streams

  #:use-module (keymap chars)
  #:use-module (keymap util)

  #:export
  ( generate:keymap.c
    keymap-->cdef ))

(define (generate:keymap.c keymaps)

  ;; extract just the bits we want:
  (define (keymaps-extract-1 keymap)
    (cons
     (car keymap)
     (assq-ref (plist->alist (cdr keymap)) #:c-symbol)))
  (define keymaps-extract
    (map keymaps-extract-1 keymaps))

  (string-append
   (unlines
    "/*"
    " * jashankj@'s keyboard configuration: Ergodox EZ"
    " */"
    ""
    "#include QMK_KEYBOARD_H"
    "#include \"version.h\""
    ""
    "enum layouts {")
   (format #f "~/~a = 0,~%~{~/~a,~%~}"
           (cdar keymaps-extract)
           (map cdr (cdr keymaps-extract)))
   (unlines
    "};"
    ""
    "const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] ="
    "{")
   (format #f "~{#include \"./~a.h\"~%~}"
           (map car keymaps-extract))
   (unlines
    "};"
    ""
    "// (setq-local indent-tabs-mode t tab-width 8 c-basic-offset 8)")))

(define (keymap-->cdef name)
  (lambda keys
    (apply
     keymap-->cdef-do
     (cons name (map key-->c keys)))))

(define (keymap-->cdef-do
         name
         k00 k01 k02 k03 k04 k05 k06
         k10 k11 k12 k13 k14 k15 k16
         k20 k21 k22 k23 k24 k25
         k30 k31 k32 k33 k34 k35 k36
         k40 k41 k42 k43 k44
             k55 k56
                 k54
         k53 k52 k51
         k07 k08 k09 k0A k0B k0C k0D
         k17 k18 k19 k1A k1B k1C k1D
             k28 k29 k2A k2B k2C k2D
         k37 k38 k39 k3A k3B k3C k3D
                 k49 k4A k4B k4C k4D
         k57 k58
         k59
         k5C k5B k5A)
  (format
   #f "[~a] = LAYOUT_ergodox(~%~:@{~k~%~}),~%"
   name
   ;; Left:
   `("~8t~a"  (,(keymap--row->cdef k00 k01 k02 k03 k04 k05 k06)))
   `("~8t~a"  (,(keymap--row->cdef k10 k11 k12 k13 k14 k15 k16)))
   `("~8t~a"  (,(keymap--row->cdef k20 k21 k22 k23 k24 k25)))
   `("~8t~a"  (,(keymap--row->cdef k30 k31 k32 k33 k34 k35 k36)))
   `("~8t~a"  (,(keymap--row->cdef k40 k41 k42 k43 k44)))
   ;; Left-Thumb:
   `("~24t~a" (,(keymap--row->cdef     k55 k56)))
   `("~40t~a" (,(keymap--row->cdef         k54)))
   `("~8t~a"  (,(keymap--row->cdef k53 k52 k51)))
   ;; Right:
   `("~8t~a"  (,(keymap--row->cdef k07 k08 k09 k0A k0B k0C k0D)))
   `("~8t~a"  (,(keymap--row->cdef k17 k18 k19 k1A k1B k1C k1D)))
   `("~8t~a"  (,(keymap--row->cdef #f  k28 k29 k2A k2B k2C k2D)))
   `("~8t~a"  (,(keymap--row->cdef k37 k38 k39 k3A k3B k3C k3D)))
   `("~8t~a"  (,(keymap--row->cdef #f  #f  k49 k4A k4B k4C k4D)))
   ;; Right-Thumb:
   `("~8t~a"  (,(keymap--row->cdef k57 k58)))
   `("~8t~a"  (,(keymap--row->cdef k59)))
   `("~8t~a"  (,(string-trim-right
                 (keymap--row->cdef k5C k5B k5A)
                 (lambda (x) (eqv? x #\,)))))))

(define (keymap--row->cdef . keys)
  (apply format #f "~:@{~vt~a~}" '()
         (stream->list
          (stream-zip
           (stream-map (lambda (n) (* n 16))
                       (stream-range 0 (length keys)))
           (stream-map keymap--key->cdef
                       (list->stream keys))))))

(define (keymap--key->cdef key)
  ((compose
    (lambda (x) (if (string= x "") x (string-append x ",")))
    keyterm->string)
   key))
