;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Keyboard tables:

(define-module (keymap chars)
;;#:use-module (racket/dict)
  #:use-module (ice-9 format) ;; srfi/48 ;; format-intermediate
  #:use-module (ice-9 match)  ;; racket/match
  #:use-module (srfi srfi-41)   ;; streams

  #:use-module (keymap util)

  #:export
  ( char-table-get*
    char-table-get
    key-->tex
    key-->c ))

(define (tex//kp x)
  (string-append "{\\tiny\\rmfamily\\scshape kp}" x))

(define hardwired-chars
  (let ((KP tex//kp))
    `(
	(NO		#:c   KC_NO
			#:tex "")
	(#f		#:c   KC_NO
			#:tex "")
	(#t		#:c   _______
			#:tex "~")
	(#\newline	#:c   KC_ENTER
			#:tex "\\kEnterB")
	(#\033		#:c   KC_ESCAPE
			#:tex "\\kEscape")
	(#\backspace	#:c   KC_BACKSPACE
			#:tex "\\kBackspace")
	(Delete		#:c   KC_DELETE
			#:tex "\\kDelete")
	(#\tab		#:c   KC_TAB
			#:tex "\\kTab")
	(#\space	#:c   KC_SPACE
			#:tex "~")
	(#\-		#:c   KC_MINUS
			#:tex "--~---")
	(#\=		#:c   KC_EQUAL
			#:tex "+ =")
	(#\[		#:c   KC_LBRC ;; KC_LEFT_BRACKET
			#:tex "[ \\{")
	(#\]		#:c   KC_RBRC ;; KC_RIGHT_BRACKET
			#:tex "] \\}")
	(#\\		#:c   KC_BACKSLASH
			#:tex "\\textbackslash\\ |")
	(#\;		#:c   KC_SEMICOLON
			#:tex "; :")
	(#\'		#:c   KC_QUOTE
			#:tex "' \"")
	(#\`		#:c   KC_GRAVE
			#:tex "\\textasciigrave\\ ~")
	(#\,		#:c   KC_COMMA
			#:tex ", <")
	(#\.		#:c   KC_DOT
			#:tex ". >")
	(#\/		#:c   KC_SLASH
			#:tex "/ ?")

	(Menu		#:c   KC_APP ;; KC_MENU
			#:tex "\\kAlt")
	(PrintScr	#:c   KC_PRINT_SCREEN
			#:tex "\\kPrintSc")
	(ScrollLk	#:c   KC_SCROLL_LOCK
			#:tex "\\kScrollLk")
	(Pause		#:c   KC_PAUSE
			#:tex "\\kPause")
	(Insert		#:c   KC_INSERT
			#:tex "\\kInsert")
	(Home		#:c   KC_HOME
			#:tex "\\kHome")
	(End		#:c   KC_END
			#:tex "\\kEnd")
	(PgUp		#:c   KC_PAGE_UP
			#:tex "\\kPageUp")
	(PgDown		#:c   KC_PAGE_DOWN
			#:tex "\\kPageDown")
	(Right		#:c   KC_RIGHT
			#:tex "→")
	(Left		#:c   KC_LEFT
			#:tex "←")
	(Up		#:c   KC_UP
			#:tex "↑")
	(Down		#:c   KC_DOWN
			#:tex "↓")

	(NumLock	#:c   KC_NUM_LOCK
			#:tex "\\kNumLock")
	(KP/		#:c   KC_KP_SLASH
			#:tex ,(KP "/"))
	(KP*		#:c   KC_KP_ASTERISK
			#:tex ,(KP "*"))
	(KP-		#:c   KC_KP_MINUS
			#:tex ,(KP "--"))
	(KP+		#:c   KC_KP_PLUS
			#:tex ,(KP "+"))
	(KPEnter	#:c   KC_KP_ENTER
			#:tex "\\kEnterC")
	(KP.		#:c   KC_KP_DOT
			#:tex ,(KP "."))

	(LCtrl		#:c   KC_LEFT_CTRL
			#:tex "\\kCtrlA")
	(RCtrl		#:c   KC_RIGHT_CTRL
			#:tex "\\kCtrlA")
	(LShift		#:c   KC_LEFT_SHIFT
			#:tex "\\kShift")
	(RShift		#:c   KC_RIGHT_SHIFT
			#:tex "\\kShift")
	(LAlt		#:c   KC_LEFT_ALT
			#:tex "\\kOption")
	(RAlt		#:c   KC_RIGHT_ALT
			#:tex "\\kOption")
	(LGUI		#:c   KC_LEFT_GUI
			#:tex "\\kGUI")
	(RGUI		#:c   KC_RIGHT_GUI
			#:tex "\\kGUI")

;;	(Sleep		#:c   KC_SYSTEM_SLEEP)
;;	(Wake		#:c   KC_SYSTEM_WAKE)
;;	(VolMute	#:c   KC_AUDIO_MUTE)
;;	(VolUp		#:c   KC_AUDIO_VOL_UP)
;;	(VolDown	#:c   KC_AUDIO_VOL_DOWN)
;;	(MxNext		#:c   KC_MEDIA_NEXT_TRACK)
;;	(MxPrev		#:c   KC_MEDIA_PREV_TRACK)
;;	(MxStop		#:c   KC_MEDIA_STOP)
;;	(MxPlay		#:c   KC_MEDIA_PLAY_PAUSE)
;;	(MxFwd		#:c   KC_MEDIA_FAST_FORWARD)
;;	(MxRew		#:c   KC_MEDIA_REWIND)
;;	(Eject		#:c   KC_MEDIA_EJECT)
;;	(Bright+	#:c   KC_BRIGHTNESS_UP)
;;	(Bright-	#:c   KC_BRIGHTNESS_DOWN)
	)))

(define (char-tablegen)
  (define (stream-range* from to)
    (stream-range from (+ to 1)))
  (define (stream-char-range* lo hi)
    (stream-range* (char->integer lo) (char->integer hi)))
  (define stream-A-Z (stream-char-range* #\A #\Z))
  (define stream-0-9 (stream-char-range* #\0 #\9))

  (define (alisti-of x)
    `(,x
      #:c   ,(string->symbol (format #f "KC_~a" x))
      #:tex                 ,(format #f "~a" x)))
  (define (alisti-of-char x)
    (alisti-of (integer->char x)))

  (define stream-KP-keys
    (stream-map
     (lambda (x)
       (let ((a       (format #f "~a" x))
             (kp-a    (format #f "KP_~a" x))
             (kc-kp-a (format #f "KC_KP_~a" x)))
         `(,(string->symbol kp-a)
           #:c   ,(string->symbol kc-kp-a)
           #:tex ,(tex//kp  a))))
     (stream-range* 0 9)))

  (define (make-f-key-symbol n)
    (string->symbol (format #f "F~a" n)))
  (define stream-F-keys
    (stream-map make-f-key-symbol (stream-range* 1 20)))

  (define streams
    (list
     (stream-map alisti-of-char stream-A-Z)
     (stream-map alisti-of-char stream-0-9)
     (stream-map alisti-of      stream-F-keys)
     stream-KP-keys
     (list->stream hardwired-chars)))

  (stream->list (apply stream-append streams)))

(define char-table (char-tablegen))

(define (char-table-get* ch)
  (match (dict-ref char-table ch #f)
    ((plist ...) (plist->alist plist))
    (#f #f)))
(define (char-table-get ch out)
  (match (char-table-get* ch)
    ((alist ...) (dict-ref alist out #f))
    (#f #f)))

(define (key-->tex key) (dict-ref key #:tex " "))
(define (key-->c   key) (dict-ref key #:c   "_______"))
