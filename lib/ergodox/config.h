/*
 * Mostly tweaks to docs/feature_tap_dance.
 * See docs/tap_hold#permissive-hold
 * See docs/tap_hold#tapping-term
 */
#define	PERMISSIVE_HOLD

#undef	TAPPING_TERM
#define	TAPPING_TERM 2000
