#
# QMK firmware configuration for an Ergodox EZ.
#

RGB_MATRIX_ENABLE	= yes

MOUSEKEY_ENABLE		= no
EXTRAKEY_ENABLE		= yes
COMMAND_ENABLE		= yes
NKRO_ENABLE		= yes
SWAP_HANDS_ENABLE	= no

LTO_ENABLE		= yes
#
#    text	   data	    bss	    dec	    hex	filename
#       0	  27914	      0	  27914	   6d0a	(LTO_ENABLE=no)
#  * The firmware size is fine - 27914/32256 (86%, 4342 bytes free)
#
#    text	   data	    bss	    dec	    hex	filename
#       0	  24328	      0	  24328	   5f08	(LTO_ENABLE=yes)
#  * The firmware size is fine - 24328/32256 (75%, 7928 bytes free)
#
