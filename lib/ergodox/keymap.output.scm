;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-module (keymap output)
  #:use-module (keymap util)
  #:use-module (keymap chars)
  #:use-module (keymap layout)
  #:use-module (keymap pp c)
  #:use-module (keymap pp tex)
  #:use-module (keymap pp tikz)

  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 format) ;; srfi/48 ;; format-intermediate
  #:use-module (ice-9 match)  ;; (require racket/match)

  #:export
  ( output-do
    output:keymap.c
    output:keymap--common.tex
    output:keymaps
    output:keymap-1
    with-keymap-do
    output:keymap_⟨-⟩.h
    output:keymap_⟨-⟩.tex ))

;; Override the default with-output-to-file.
(define* (with-output-to-file file thunk)
  (format (current-output-port) "~a~%" file)
  ((@ (ice-9 ports) with-output-to-file) file thunk))

(define (output:keymap.c *keymaps*)
  (with-output-to-file "keymap.c"
    (lambda () (display (generate:keymap.c *keymaps*)))))

(define (output:keymap--common.tex *keymaps*)
  (with-output-to-file "keymap--common.tex"
    generate:keymap--common.tex))

(define* (output:keymaps *keymaps*)
  (for-each output:keymap-1 *keymaps*))

(define (output-do *keymaps*)
  (output:keymap.c           *keymaps*)
  (output:keymap--common.tex *keymaps*)
  (output:keymaps            *keymaps*))

(define (output:keymap-1 keymap)
  (let* ((name     (car keymap))
         (props    (cdr keymap))
         (c-out    (format #f "~a.h"   name))
         (tex-out  (format #f "~a.tex" name)))

    (with-output-to-file c-out
      (apply output:keymap_⟨-⟩.h props))
    (with-output-to-file tex-out
      (apply output:keymap_⟨-⟩.tex props))))

(define (with-keymap-do keymap f)
  "Bind all the keys in KEYMAP, and pass them all to procedure F."
  (match keymap
    ((('#:left
       k00 k01 k02 k03 k04 k05 k06
       k10 k11 k12 k13 k14 k15 k16
       k20 k21 k22 k23 k24 k25
       k30 k31 k32 k33 k34 k35 k36
       k40 k41 k42 k43 k44)
      ('#:lthumb
           k55 k56
               k54
       k53 k52 k51)
      ('#:right
       k07 k08 k09 k0A k0B k0C k0D
       k17 k18 k19 k1A k1B k1C k1D
           k28 k29 k2A k2B k2C k2D
       k37 k38 k39 k3A k3B k3C k3D
               k49 k4A k4B k4C k4D)
      ('#:rthumb
       k57 k58
       k59
       k5C k5B k5A))
     (f k00 k01 k02 k03 k04 k05 k06 k10 k11 k12 k13 k14 k15 k16 k20 k21
        k22 k23 k24 k25 k30 k31 k32 k33 k34 k35 k36 k40 k41 k42 k43 k44
        k55 k56 k54 k53 k52 k51 k07 k08 k09 k0A k0B k0C k0D k17 k18 k19
        k1A k1B k1C k1D k28 k29 k2A k2B k2C k2D k37 k38 k39 k3A k3B k3C
        k3D k49 k4A k4B k4C k4D k57 k58 k59 k5C k5B k5A))))

(define* ((output:keymap_⟨-⟩.h   #:key from c-symbol tex-caption))
  (display (with-keymap-do from (keymap-->cdef c-symbol))))

(define* ((output:keymap_⟨-⟩.tex #:key from c-symbol tex-caption))
  (display (with-keymap-do from (keymap-->tex tex-caption))))
