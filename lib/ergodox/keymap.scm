#!/usr/bin/env guile \
-e '(@ (keymap) main)' -s
!#

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-module (keymap)
  #:use-module (keymap util)
  #:use-module (keymap chars)
  #:use-module (keymap layout)
  #:use-module (keymap output)

  #:export (main))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (keymap:qd-base f)
  ;; Layer-switching keys.
  (define k06
    `( #:c "MO(L_FNKEYS0)"
       #:tex ,(tex/math (tex/⟨-⟩ "F_0"))))
  (define k07
    `( #:c "MO(L_FNKEYS1)"
       #:tex ,(tex/math (tex/⟨-⟩ "F_1"))))
  (define k4D
    `( #:c "MO(L_RAINBOWS)"
       #:tex ,(tex/math (tex/⟨-⟩ "f"))))

  ;; Space-cadet keys.
  (define k20
    `( #:c "CTL_T(KC_ESC)"
       #:tex "\\kCtrlA\\ \\kEscape"))
  (define k30
    `( #:c "KC_LSPO" ;; "LSFT_T(KC_LPRN)"
       #:tex "( \\kShift"))
  (define k3D
    `( #:c "KC_RSPC" ;; "RSFT_T(KC_RPRN)"
       #:tex "\\kShift )"))

  (make-keymap!
	;; LEFT:
	#:k00 #\=		#:k01 #\1		#:k02 #\2		#:k03 #\3		#:k04 #\4		#:k05 #\5		#:k06 k06
	#:k10 #\tab		#:k11 (f #\Q #\')	#:k12 (f #\W #\,)	#:k13 (f #\E #\.)	#:k14 (f #\R #\P)	#:k15 (f #\T #\Y)	#:k16 #f
	#:k20 k20		#:k21 (f #\A #\A)	#:k22 (f #\S #\O)	#:k23 (f #\D #\E)	#:k24 (f #\F #\U)	#:k25 (f #\G #\I)
	#:k30 k30		#:k31 (f #\Z #\;)	#:k32 (f #\X #\Q)	#:k33 (f #\C #\J)	#:k34 (f #\V #\K)	#:k35 (f #\B #\X)	#:k36 #f
	#:k40 #f		#:k41 #\`		#:k42 (f #\' #\\)	#:k43 `Left		#:k44 `Right

	;; LTHUMB:
				#:k55 `LCtrl		#:k56 `LAlt
							#:k54 `Home
	#:k53 #\backspace	#:k52 `Delete		#:k51 `End

	;; RIGHT:
	#:k07 k07		#:k08 #\6		#:k09 #\7		#:k0A #\8		#:k0B #\9		#:k0C #\0		#:k0D #\-
	#:k17 `PrintScr		#:k18 (f #\Y #\F)	#:k19 (f #\U #\G)	#:k1A (f #\I #\C)	#:k1B (f #\O #\R)	#:k1C (f #\P #\L)	#:k1D (f #\\ #\/)
				#:k28 (f #\H #\D)	#:k29 (f #\J #\H)	#:k2A (f #\K #\T)	#:k2B (f #\L #\N)	#:k2C (f #\; #\S)	#:k2D (f #\' #\\)
	#:k37 `Menu		#:k38 (f #\N #\B)	#:k39 (f #\M #\M)	#:k3A (f #\, #\W)	#:k3B (f #\. #\V)	#:k3C (f #\/ #\Z)	#:k3D k3D
							#:k49 `Up		#:k4A `Down		#:k4B #\[		#:k4C #\]		#:k4D k4D

	;; RTHUMB:
	#:k57 `LGUI		#:k58 `RCtrl
	#:k59 `PgUp
	#:k5C `PgDown		#:k5B #\newline		#:k5A #\space
  ))

(define keymap:qwerty-base
  (keymap:qd-base (lambda (q d) q)))
(define keymap:dvorak-base
  (keymap:qd-base (lambda (q d) d)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define keymap:fnkeys0
  (make-keymap!
   #:k01 `F1 #:k02 `F2 #:k03 `F3 #:k04 `F4 #:k05 `F5
   #:k08 `F6 #:k09 `F7 #:k0A `F8 #:k0B `F9 #:k0C `F10
   #:k17 `ScrollLk
   #:k37 `NumLock
   #:k19 `KP_7  #:k1A `KP_8  #:k1B `KP_9
   #:k29 `KP_4  #:k2A `KP_5  #:k2B `KP_6
   #:k39 `KP_1  #:k3A `KP_2  #:k3B `KP_3
   #:k38 `KP_0
   #:k4B `KP.  #:k4C `KPEnter
   #:k57 `KP/
   #:k59 `KP*
   #:k5C `KP-
   #:k5B `KP+
   #:k5A `KPEnter
   ))

(define keymap:fnkeys1
  (make-keymap!
   #:k01 `F11 #:k02 `F12 #:k03 `F13 #:k04 `F14 #:k05 `F15
   #:k08 `F16 #:k09 `F17 #:k0A `F18 #:k0B `F19 #:k0C `F20))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define keymap:rainbows
  (let ((angled  (compose tex/math tex/⟨-⟩))
        (rgbprop
         (lambda (x) (tex/math
                 (string-append
                  "\\underset"
                  "{{\\textrm{\\textsc{rgb}}}}"
                  "{" x "}")))))
    (make-keymap!
     #:k11 `( #:c "DF(L_QWERTY_BASE)"  #:tex ,(angled "\\mathbb{Q}"))
     #:k21 `( #:c "DF(L_DVORAK_BASE)"  #:tex ,(angled "\\mathbb{D}"))
     #:k49 `( #:c "RGB_VAD"  #:tex ,(rgbprop "v^{-}"))
     #:k4A `( #:c "RGB_VAI"  #:tex ,(rgbprop "v^{+}"))
     #:k4B `( #:c "RGB_HUD"  #:tex ,(rgbprop "h^{-}"))
     #:k4C `( #:c "RGB_HUI"  #:tex ,(rgbprop "h^{+}"))
     #:k38 `( #:c "RGB_TOG"  #:tex ,(rgbprop "*"))
     #:k39 `( #:c "RGB_M_P"  #:tex ,(rgbprop "{\\small\\textsf{pl}}"))
     #:k3A `( #:c "RGB_M_B"  #:tex ,(rgbprop "{\\small\\textsf{br}}"))
     #:k3B `( #:c "RGB_M_R"  #:tex ,(rgbprop "{\\small\\textsf{r}}"))
     #:k3C `( #:c "RGB_M_SW" #:tex ,(rgbprop "{\\small\\textsf{sw}}"))
     #:k2C `( #:c "RGB_M_SN" #:tex ,(rgbprop "{\\small\\textsf{sn}}"))
     #:k2B `( #:c "RGB_M_K"  #:tex ,(rgbprop "{\\small\\textsf{kn}}"))
     #:k2A `( #:c "RGB_M_X"  #:tex ,(rgbprop "{\\small\\textsf{xm}}"))
     #:k29 `( #:c "RGB_M_G"  #:tex ,(rgbprop "{\\small\\textsf{gr}}"))
     #:k28 `( #:c "RGB_M_T"  #:tex ,(rgbprop "{\\small\\textsf{t}}"))
     )))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define* (main #:rest rest)
  (output-do
   `((keymap_dvorak_base
      #:from        ,keymap:dvorak-base
      #:c-symbol    L_DVORAK_BASE
      #:tex-caption "\\texttt{L\\_DVORAK\\_BASE}")
     (keymap_qwerty_base
      #:from        ,keymap:qwerty-base
      #:c-symbol    L_QWERTY_BASE
      #:tex-caption "\\texttt{L\\_QWERTY\\_BASE}")
     (keymap_fnkeys0
      #:from        ,keymap:fnkeys0
      #:c-symbol    L_FNKEYS0
      #:tex-caption "\\texttt{L\\_FNKEYS0}")
     (keymap_fnkeys1
      #:from        ,keymap:fnkeys1
      #:c-symbol    L_FNKEYS1
      #:tex-caption "\\texttt{L\\_FNKEYS1}")
     (keymap_rainbows
      #:from        ,keymap:rainbows
      #:c-symbol    L_RAINBOWS
      #:tex-caption "\\texttt{L\\_RAINBOWS}"))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
