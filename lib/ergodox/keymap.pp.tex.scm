;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Common keymap glue for TeX.

(define-module (keymap pp tex)
  #:use-module (ice-9 format) ;; srfi/48 ;; format-intermediate
  #:use-module (ice-9 match)  ;; racket/match
  #:use-module (ice-9 unicode)
  #:use-module (srfi srfi-13)   ;; strings
  #:use-module (srfi srfi-41)   ;; streams
;;#:use-module racket/port
;;#:use-module racket/trace

  #:use-module (keymap util)

  #:export
  ( generate:keymap--common.tex ))

(define (generate:keymap--common.tex)
  (display keymap-common-header-1)
  (display (make-character-table-defs))
  (display keymap-common-header-2))

(define *the-character-table*
  `((	#x21E4	Backtab		Inter)
    (	#x21E5	Tab		Inter)
    (	#x21E7	Shift		Inter)
    (	#x21EA	CapsLock	Inter)
    (	#x21ED	NumLock		DejaVuSans)
    (	#x21F1	Home		DejaVuSans)
    (	#x21F2	End		DejaVuSans)
    (	#x21F3	ScrollLk	DejaVuSans)
    (	#x2303	CtrlA		Inter)
    (	#x2BB9	CtrlB		NewCMMath)
    (	#x2388	CtrlC	        NewCMMath)
    (	#x2325	Option		Inter)
    (	#x2387	Alt		Inter)
    (	#x2318	GUI		Inter)
    (	#x2324	EnterA		DejaVuSans)
    (	#x2326	Delete		Inter)
    (	#x232B	Backspace	Inter)
    (	#x238B	Escape		Inter)
    (	#x2397	PageUp		NewCMMath)
    (	#x2398	PageDown	NewCMMath)
    (	#x2399	PrintSc		NewCMMath)
    (	#x2380	Insert		Inter)
    (	#x2389	Pause		NewCMMath)
    (	#x238A	Break		NewCMMath)
    (	#x2384	Compose		NewCMMath)
    (	#x23CE	EnterB		Inter)
    (	#x2386	EnterC		NewCMMath)
    (	#x23CF	Eject		Inter)
    (	#x2B90	EnterD		NewCMMath)
    (	#x2327	Clear		Inter)
    (	#x23E9	FwdSeek		NewCMMath)
    (	#x23EA	RevSeek		NewCMMath)
    (	#x23ED	FwdSkip		NewCMMath)
    (	#x23EE	RevSkip		NewCMMath)
    (	#x23EF	PlayPause 	NewCMMath)
    (	#x23F9	Stop      	NewCMMath)
    (	#x23FA	Record		NewCMMath)))

(define keymap-common-header-1
  (unlines
   "\\documentclass[12pt]{article}"

   "\\usepackage{fontspec}"
   "% \\setsansfont{DejaVuSans}"
   "% \\setsansfont{DejaVuSansMono}"
   "  \\setsansfont{AlgolRevived}"
   "% \\setsansfont{Inter}"

   "\\newfontfamily\\ffDejaVuSansMono{DejaVuSansMono}"
   "\\newfontfamily\\ffDejaVuSans{DejaVuSans}"
   "\\newfontfamily\\ffInter{Inter}"
   "\\newfontfamily\\ffNewCMMath{NewCMMath-Regular}[Scale=1.5]"
   ))

(define keymap-common-header-2
  (unlines
   "\\usepackage{polyglossia}"
   "\\setdefaultlanguage[variant=australian]{english}"

   "\\usepackage{geometry}"
   "\\geometry{verbose, a5paper, landscape, margin=1cm}"
   "\\pagestyle{empty}"

   "\\setlength{\\parskip}{\\smallskipamount}"
   "\\setlength{\\parindent}{0pt}"

   "\\usepackage{amsmath}"
   "\\usepackage{amssymb}"

   "\\usepackage{xcolor}"
   "\\usepackage{tikz}"
   ))

(define* (format-one-character key val #:optional (font #f))
  (let* ((key*      (integer->char key))
         (khex      (string-pad (number->HEX key) 4 #\0))
         (val-def   (format #f "\\newcommand{\\k~a}" val))
         (val-body  (format #f "\\char\"~a" khex))
         (kname     (char->formal-name key*))
         (comment   (format #f "% U+~a ~c ~a" khex key* kname)))
    (if font
        (format #f "~40a~a~%  {{\\ff~a~a}}~%"
                val-def comment font val-body)
        (format #f "~40a~a~%"
                (string-append val-def "{" val-body "}") comment))))

(define (make-character-table-defs)
  ((compose
    string-concatenate
    stream->list
    stream-append
    (stream:map (lambda (x) (apply format-one-character x)))
    list->stream)
   *the-character-table*))

(define (make-demo-block)
  ((compose
    string-concatenate
    stream->list
    stream-append
    (stream:map (lambda (x) (format #f "\\k~a{}~%" (cadr x))))
    list->stream)
   *the-character-table*))
