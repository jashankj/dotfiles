;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Low-level utility functions.

(define-module (keymap util test)
  #:use-module (srfi srfi-64)   ;; testing

  #:use-module (keymap util)
  #:use-module (srfi srfi-41)   ;; streams
)

(test-begin "keymap util test")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "keyterm->string")

(test-equal ""      (keyterm->string #f))

(test-equal ""      (keyterm->string ""))

(test-equal "xyzzy" (keyterm->string "xyzzy"))

(test-equal "()"    (keyterm->string `()))

(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "number->hex")

(test-equal  "0" (number->hex #x00))
(test-equal  "1" (number->hex #x01))
(test-equal  "2" (number->hex #x02))
(test-equal  "3" (number->hex #x03))
(test-equal  "4" (number->hex #x04))
(test-equal  "5" (number->hex #x05))
(test-equal  "6" (number->hex #x06))
(test-equal  "7" (number->hex #x07))
(test-equal  "8" (number->hex #x08))
(test-equal  "9" (number->hex #x09))
(test-equal  "a" (number->hex #x0a))
(test-equal  "b" (number->hex #x0b))
(test-equal  "c" (number->hex #x0c))
(test-equal  "d" (number->hex #x0d))
(test-equal  "e" (number->hex #x0e))
(test-equal  "f" (number->hex #x0f))
(test-equal "10" (number->hex #x10))

(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "number->HEX")

(test-equal  "0" (number->HEX #x00))
(test-equal  "1" (number->HEX #x01))
(test-equal  "2" (number->HEX #x02))
(test-equal  "3" (number->HEX #x03))
(test-equal  "4" (number->HEX #x04))
(test-equal  "5" (number->HEX #x05))
(test-equal  "6" (number->HEX #x06))
(test-equal  "7" (number->HEX #x07))
(test-equal  "8" (number->HEX #x08))
(test-equal  "9" (number->HEX #x09))
(test-equal  "A" (number->HEX #x0a))
(test-equal  "B" (number->HEX #x0b))
(test-equal  "C" (number->HEX #x0c))
(test-equal  "D" (number->HEX #x0d))
(test-equal  "E" (number->HEX #x0e))
(test-equal  "F" (number->HEX #x0f))
(test-equal "10" (number->HEX #x10))

(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "dict-ref")
(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "cartesian-product")

(test-equal
    '((1 a) (1 b) (1 c) (2 a) (2 b) (2 c) (3 a) (3 b) (3 c))
  (stream->list
   (cartesian-product
    (list->stream '(1 2 3))
    (list->stream '(a b c)))))

(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "stream:map")
(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "stream:filter")
(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-begin "exclusive-range")
(test-end)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(test-end)
