divert(-1)
dnl um4l -- a terrible UML generator with Graphviz and M4
dnl
dnl I drive this with a wrapper script, `um4l', which does:
dnl
dnl     m4 ~/lib/m4/uml.dot.m4 "$1" \
dnl     | dot -Tpdf -o "${1%.um4l}.pdf"
dnl
dnl ## Directives
dnl ### Start and End
dnl  - `UMLBegin': start a UML diagram.
dnl  - `UMLEnd': end a UML diagram.
dnl
dnl ### Entities
dnl  - `Interface (<name>, <methods>)': define an interface
dnl  - `Enum (<name>, <variants>)': define an enumeration
dnl  - `Class (<name>, <fields>, <methods>)': define a class
dnl
dnl ### Members
dnl Signatures have the form `ident:ty'.
dnl
dnl #### Fields
dnl  - `PublField (<name>, <type>)': a public field
dnl  - `ProtField (<name>, <type>)': a protected field
dnl  - `Field (<name>, <type>)': a private field
dnl
dnl #### Methods
dnl  - `Constructor (<args>)': declare a constructor
dnl  - `Method (<name>, <args>, <returns>)': declare a public method
dnl  - `ProtMethod (<name>, <args>, <returns>)': declare a protected method
dnl  - `PrivMethod (<name>, <args>, <returns>)': declare a private method
dnl
dnl ### Relations
dnl  - `Extends (<subclass>, <superclass>)': define an extends relation
dnl  - `Implements (<implementor>, <interface>)': define an implements relation
dnl  - `Aggregates (<class>, <contained>)': note that a class contains many instances
dnl  - `Composes (<class>, <contained>)': note that a class contains one instance
dnl
dnl  - `AssociatedWith (<x>, <y>)': shares a `uses' relation with another instance
dnl  - `DependsOn (<x>, <y>)': who knows, i don't know
dnl
dnl ## Example
dnl
dnl ```
dnl UMLBegin
dnl
dnl Class(`Greeter', `
dnl     Field(`greeting', `String'),
dnl     Field(`greetCount', `int')',
dnl `
dnl     Constructor(`'),
dnl     Constructor(`greeting:String'),
dnl     Method(`getGreeting', `', `String'),
dnl     Method(`setGreeting', `greet:String', `void'),
dnl     Method(`getGreetCount', `', `int'),
dnl     Method(`greet', `f:OutputStream', `void')
dnl ');
dnl
dnl Class(`LoudGreeter', `
dnl     Field(`loudGreetCount', `int')',
dnl `
dnl     Method(`getLoudGreetCount', `', `int'),
dnl     Method(`getLoudGreeting', `', `String'),
dnl     Method(`greetLoudly', `f:OutputStream', `void')
dnl ');
dnl Extends(`LoudGreeter', `Greeter');
dnl
dnl UMLEnd
dnl ```
dnl
dnl 2018-04-28    Jashank Jeremy <jashankj@cse.unsw.edu.au>
dnl 2019-06-11    Jashank Jeremy <jashankj@cse.unsw.edu.au>

define(`UMLBegin', `digraph "uml" {
    rankdir=TB;
    node [fontname="Roboto", shape="record"];
    edge [fontname="Roboto"];
')
define(`UMLEnd', `}')
define(`PublField', ``+ $1: $2'')
define(`ProtField', ``# $1: $2'')
define(`Field', ``– $1: $2'')

define(`Constructor', ``+ «constructor»($1)'')
define(`Method', ``+ $1($2): $3'')
define(`ProtMethod', ``# $1($2): $3'')
define(`PrivMethod', ``– $1($2): $3'')

define(`_ClassInner',
`ifelse(`$#', `0', `',
        `$#', `1', ``$1\l'',
        ``$1\l'_ClassInner(shift($@))')')

define(`Interface', ``$1 [label="{«interface»\n\N\n|'_ClassInner($2)`}"]'')
define(`Enum', ``$1 [label="{«enumeration»\n\N\n|'_ClassInner($2)`}"]'')
define(`Class', ``$1 [label="{\N\n|'_ClassInner($2)`|'_ClassInner($3)`}"]'')

define(`AssociatedWith', `$1 -> $2 [arrowhead="open", style="solid"]')
define(`DependsOn', `$1 -> $2 [arrowhead="open", style="dashed"]')
define(`Aggregates', `$1 -> $2 [arrowtail="ediamond", arrowhead="none", dir="back", style="solid"]')
define(`Composes', `$1 -> $2 [arrowtail="diamond", arrowhead="none", dir="back", style="solid"]')

define(`Extends', `$2 -> $1 [arrowtail="empty", arrowhead="none", dir="back", style="solid"]')
define(`Implements', `$2 -> $1 [arrowtail="empty", arrowhead="none", dir="back", style="dashed"]')

divert(1)dnl
