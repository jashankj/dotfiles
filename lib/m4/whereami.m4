divert(`-1')dnl
dnl ####################################################################
dnl Some handy macros for making code portable between machines.

define(__hostenv__, esyscmd(`hostenv | tr -d "\n"'))

define(Djaenelle, ifelse(__hostenv__, jaenelle, 1, -1))
define(Dalyzon,   ifelse(__hostenv__, alyzon, 1, -1))
define(Dinara,    ifelse(__hostenv__, inara, 1, -1))
define(Dmenolly,  ifelse(__hostenv__, menolly, 1, -1))
define(Dlisbon,   ifelse(__hostenv__, lisbon, 1, -1))

define(Dwedge,    ifelse(__hostenv__, wedge, 1, -1))
define(Dyarn,     ifelse(__hostenv__, yarn-ev, 1, -1))

define(Dcse,      ifelse(__hostenv__, cse, 1, -1))

divert(`0')dnl
