From 98ef89eeeaebf5e8e46e9edc027931cf0020298a Mon Sep 17 00:00:00 2001
From: Jashank Jeremy <jashank@rulingia.com.au>
Date: Mon, 5 Apr 2021 16:48:15 +1000
Subject: [PATCH] {LOCAL} Allow the use of `libprofiler' to collect profile
 data.

The Emacs profiler binds the `SIGPROF' signal, which breaks `gperftools'
in a default configuration.  The easiest way to stop this happening is
apparently to undefine the symbol, which turns off the Emacs profiler.

To allow "continuous pprof"-style operation, every N garbage collection
cycles (currently N=16) we restart the profiler, which flushes out the
profile to disk.  Otherwise, we can only usefully review profiling data
after the Emacs process has gone away.
---
 src/alloc.c     | 47 +++++++++++++++++++++++++++++++++++++++++++++++
 src/callproc.c  |  4 ++++
 src/profiler.c  |  4 ++++
 src/sysdep.c    |  3 +++
 src/syssignal.h |  4 ++++
 5 files changed, 62 insertions(+)

diff --git a/src/alloc.c b/src/alloc.c
index 5ad8097394..3171eaf738 100644
--- a/src/alloc.c
+++ b/src/alloc.c
@@ -639,6 +639,48 @@ mmap_lisp_allowed_p (void)
    running finalizers.  */
 struct Lisp_Finalizer doomed_finalizers;
 
+
+
+/* Ask `libprofiler' to dump profile data. */
+#include <gperftools/profiler.h>
+#include <stdio.h>
+#include <stdlib.h>
+#include <unistd.h>
+
+static bool     gperf_cpu_enabled_p = true;
+static char    *gperf_cpu_profile_dir = NULL;
+static char     gperf_cpu_file[PATH_MAX];
+static unsigned gperf_cpu_n_profiles = 0;
+
+static void
+update_profile_name (unsigned int n)
+{
+  snprintf (gperf_cpu_file, PATH_MAX,
+	    "%s/emacs.%d.profile.%u",
+	    gperf_cpu_profile_dir, getpid (), n);
+}
+
+static void
+restart_profiler (void)
+{
+  // fast-path:
+  if (! gperf_cpu_enabled_p) return;
+
+  // first-run:  enabled, dir = NULL
+  // disables if CPUPROFILE_DIR is unset
+  if (gperf_cpu_enabled_p && !gperf_cpu_profile_dir)
+    if ((gperf_cpu_profile_dir = getenv ("CPUPROFILE_DIR")) == NULL)
+      {
+	gperf_cpu_enabled_p = false;
+	return;
+      }
+
+  if (ProfilingIsEnabledForAllThreads ())
+    ProfilerStop ();
+  update_profile_name (gperf_cpu_n_profiles++);
+  ProfilerStart (gperf_cpu_file);
+}
+
 
 /************************************************************************
 				Malloc
@@ -6226,6 +6268,9 @@ garbage_collect (void)
       if (tot_after < tot_before)
 	malloc_probe (min (tot_before - tot_after, SIZE_MAX));
     }
+
+  if (gcs_done % 16 == 0)
+    restart_profiler (); /* ergh, but no easy better place */
 }
 
 DEFUN ("garbage-collect", Fgarbage_collect, Sgarbage_collect, 0, 0, "",
@@ -7556,6 +7601,8 @@ init_alloc (void)
 {
   Vgc_elapsed = make_float (0.0);
   gcs_done = 0;
+
+  restart_profiler ();
 }
 
 void
diff --git a/src/callproc.c b/src/callproc.c
index 66a35d9f33..51ee39ed4f 100644
--- a/src/callproc.c
+++ b/src/callproc.c
@@ -86,6 +86,10 @@ #define _P_NOWAIT 1	/* from process.h */
 #include "nsterm.h"
 #endif
 
+#ifdef SIGPROF
+#undef SIGPROF
+#endif
+
 /* Pattern used by call-process-region to make temp files.  */
 static Lisp_Object Vtemp_file_name_pattern;
 
diff --git a/src/profiler.c b/src/profiler.c
index 31a46d1b5e..27c21f1adb 100644
--- a/src/profiler.c
+++ b/src/profiler.c
@@ -23,6 +23,10 @@ Copyright (C) 2012-2022 Free Software Foundation, Inc.
 #include "systime.h"
 #include "pdumper.h"
 
+#ifdef SIGPROF
+#undef SIGPROF
+#endif
+
 /* Return A + B, but return the maximum fixnum if the result would overflow.
    Assume A and B are nonnegative and in fixnum range.  */
 
diff --git a/src/sysdep.c b/src/sysdep.c
index 1e630835ad..180d018964 100644
--- a/src/sysdep.c
+++ b/src/sysdep.c
@@ -2036,6 +2036,9 @@ init_signals (void)
   sigaction (SIGSYS, &thread_fatal_action, 0);
 #endif
   sigaction (SIGTERM, &process_fatal_action, 0);
+#ifdef SIGPROF
+#undef SIGPROF
+#endif
 #ifdef SIGPROF
   signal (SIGPROF, SIG_IGN);
 #endif
diff --git a/src/syssignal.h b/src/syssignal.h
index 07055c04be..2250c9bcc8 100644
--- a/src/syssignal.h
+++ b/src/syssignal.h
@@ -47,6 +47,10 @@ #define SIGEV_SIGNAL SIGEV_SIGNAL
 # define HAVE_ITIMERSPEC
 #endif
 
+#ifdef SIGPROF
+#undef SIGPROF
+#endif
+
 #if (defined SIGPROF && !defined PROFILING \
      && (defined HAVE_SETITIMER || defined HAVE_ITIMERSPEC))
 # define PROFILER_CPU_SUPPORT
-- 
2.34.1

