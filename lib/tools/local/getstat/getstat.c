/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2018-2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

/**
 * @file	~/src/local/getstat/getstat.c
 * @brief	on a FreeBSD machine, grab some statistics
 * @author	Jashank Jeremy &lt;jashank at rulingia.com.au&gt;
 * @date	2018-10-13
 */

#include <sys/cdefs.h>
#include <sys/types.h>
#include <sys/param.h>

#include <sys/resource.h>
#include <sys/sysctl.h>
#include <sys/vmmeter.h>
#include <vm/vm_param.h>

#include <err.h>
#include <libxo/xo.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>

#define xsysctl(mib,destp,destlenp) \
	sysctl (mib##_mib, mib##_len, (destp), (destlenp), NULL, 0)

struct lavg { double one, five, fifteen; };

struct state {
	bool show_runq;
	bool show_lavg;
	bool show_freq;
	bool one_line;
	size_t n_freqs;
	unsigned *freqs;

	bool show_temp;
	size_t n_temps;
	unsigned *temps;
};

static void parse_args (struct state *s, int *argc, char ***argv);

static char *get_time (char *, size_t);
static int16_t get_runq_len (void);
static struct lavg get_loadavg (void);
static u_int get_cpu_freq (u_int);
static int get_cpu_temp (u_int);

static void usage (void) __dead2;

int
main (int argc, char **argv)
{
	struct state s = {
		.one_line  = false,
		.show_runq = false,
		.show_lavg = false,
		.show_freq = false, .n_freqs = 0, .freqs = NULL,
		.show_temp = false, .n_temps = 0, .temps = NULL,
	};

	parse_args (&s, &argc, &argv);

	xo_open_container_h (NULL, "statistics");
	xo_open_list ("points");

	do {
		char time_buf[9] = {};
		struct lavg lavg = get_loadavg ();

		xo_open_instance ("point");
		if (s.one_line) xo_emit ("\r");
		xo_emit ("{:time/%s}", get_time (time_buf, 9));

		if (s.show_runq)
			xo_emit ("{P: }{:runq/%2hd}", get_runq_len ());

		if (s.show_lavg)
			xo_emit (
				"{P: }{:load-average-1/%.2lf}"
				"{P: }{:load-average-5/%.2lf}"
				"{P: }{:load-average-15/%.2lf}",
				lavg.one, lavg.five, lavg.fifteen);

		if (s.show_freq) {
			xo_open_list ("frequencies");
			for (u_int i = 0; i < s.n_freqs; i++)
				xo_emit (
					"{P: }{:frequency-MHz/%4u}",
					get_cpu_freq (s.freqs[i]));
			xo_close_list ("frequencies");
		}

		if (s.show_temp) {
			xo_open_list ("frequencies");
			for (u_int i = 0; i < s.n_temps; i++)
				xo_emit (
					"{P: }{:temperature-C/%2d}",
					get_cpu_temp (s.temps[i]));
			xo_close_list ("frequencies");
		}

		if (! s.one_line) xo_emit ("\n");
		if (  s.one_line) xo_flush ();
		xo_close_instance ("point");
	} while (sleep (1) == 0);

	xo_close_list ("points");
	xo_close_container_h (NULL, "statistics");

	return EXIT_SUCCESS;
}

static void
parse_args (struct state *s, int *argc, char ***argv)
{
	*argc = xo_parse_args (*argc, *argv);
	if (argc < 0) err (EX_SOFTWARE, "xo_parse_args");

	int ch;
	while ((ch = getopt (*argc, *argv, "rqlt:f:")) != -1) {
		switch (ch) {
		case 'r': s->one_line  = true; break;
		case 'q': s->show_runq = true; break;
		case 'l': s->show_lavg = true; break;

		case 't':
			s->show_temp = true;
			s->n_temps++;
			s->temps = realloc (s->temps, s->n_temps * sizeof (unsigned));
			s->temps[s->n_temps - 1] = strtoul (optarg, NULL, 10);
			break;

		case 'f':
			s->show_freq = true;
			s->n_freqs++;
			s->freqs = realloc (s->freqs, s->n_freqs * sizeof (unsigned));
			s->freqs[s->n_freqs - 1] = strtoul (optarg, NULL, 10);
			break;

		default:
			usage ();
		}
	}
}

static char *
get_time (char *buf, size_t bufsiz)
{
	time_t t = time (NULL);

	struct tm tm;
	localtime_r (&t, &tm);

	strftime (buf, bufsiz, "%H:%M:%S", &tm);

	return buf;
}

static int16_t
get_runq_len (void)
{
	int   vmtotal_mib[] = { CTL_VM, VM_TOTAL };
	u_int vmtotal_len   = nitems (vmtotal_mib);

	struct vmtotal vmtotal;
	memset (&vmtotal, 0, sizeof (vmtotal));
	size_t vmtotal_reslen = sizeof (vmtotal);
	if (xsysctl (vmtotal, &vmtotal, &vmtotal_reslen) != 0)
		err (EX_OSERR, "couldn't get vm.vmtotal");

	return vmtotal.t_rq;
}

static struct lavg
get_loadavg (void)
{
	int   loadavg_mib[] = { CTL_VM, VM_LOADAVG };
	u_int loadavg_len   = nitems (loadavg_mib);

	struct loadavg loadavg;
	memset (&loadavg, 0, sizeof (loadavg));
	size_t loadavg_reslen = sizeof (loadavg);

	if (xsysctl (loadavg, &loadavg, &loadavg_reslen) != 0)
		err (EX_OSERR, "couldn't get vm.loadavg");

	return (struct lavg) {
		.one     = (double)loadavg.ldavg[0] / (double)loadavg.fscale,
		.five    = (double)loadavg.ldavg[1] / (double)loadavg.fscale,
		.fifteen = (double)loadavg.ldavg[2] / (double)loadavg.fscale
	};
}

static u_int
get_cpu_freq (u_int ncpu)
{
	u_int freq;
	size_t freq_len = sizeof (freq);
	char mib[32] = {};
	snprintf (mib, 32, "dev.cpu.%d.freq", ncpu);

	if (sysctlbyname (mib, &freq, &freq_len, NULL, 0) != 0)
		err (EX_OSERR, "couldn't get %s", mib);

	return freq;
}

static int
get_cpu_temp (u_int ncpu)
{
	u_int temp;
	size_t temp_len = sizeof (temp);
	char mib[32] = {};
	snprintf (mib, 32, "dev.cpu.%d.temperature", ncpu);

	if (sysctlbyname (mib, &temp, &temp_len, NULL, 0) != 0)
		err (EX_OSERR, "couldn't get %s", mib);

	// deciKelvin?
	return (int) (((double)temp / 10.0) - 273.15);
}

static void
usage (void)
{
	fputs ("usage: getstat [-rql] [-t cpu] [-f cpu]\n", stderr);
	exit (EX_USAGE);
}
