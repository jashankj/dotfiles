/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2018-22 Jashank Jeremy <jashank at rulingia.com.au>
 */

/*!
 * @file	~/src/local/lan/lan.rs
 * @brief	Solver for 'Countdown' games.
 * @author	Jashank Jeremy <jashank at rulingia.com.au>
 */

#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]

use std::{
	cmp::Eq,
	collections::{HashMap, HashSet, BTreeMap, BTreeSet},
	fmt::{self, Debug},
	fs::File,
	hash::Hash,
	io::{BufReader, prelude::*},
	ops,
	path::PathBuf, borrow::Borrow,
};

// use std::rc::Rc as R;
use std::sync::Arc as R;

use bitvec::prelude::*;
use clap::{ArgEnum, Parser, Subcommand};
use color_eyre::eyre::{Result, eyre};
use itertools::Itertools;
use rand::prelude::*;
use rayon::prelude::*;

////////////////////////////////////////////////////////////////////////

type Dictx = R<dyn Dict>;
trait Dict: Send + Sync {
	fn load_dict () -> Result<Self> where Self: Sized;
	fn is_word (&self, word: &str) -> bool;
}

type LettersOut = BTreeMap<usize, Vec<String>>;

const WORDS_FILE: &'static str = "/usr/share/dict/words";


////////////////////////////////////////////////////////////////////////

/// Play a game of "Countdown".
/// Or, for Australians, "Letters and Numbers".
#[derive(Clone, Debug, Parser, PartialEq)]
struct Opt {
	#[clap(long, arg_enum, default_value = "vec")]
	use_dict: DictOpt,
	#[clap(subcommand)]
	command: Cmd,
}

#[derive(Clone, Debug, PartialEq, Subcommand)]
enum Cmd {
	/// Play a letters round.
	Letters {
		#[clap(name = "letters")]
		word: String,
	},

	/// Play a numbers round.
	Numbers {
		a: usize,
		b: usize,
		c: usize,
		d: usize,
		e: usize,
		f: usize,
		target: usize,

		#[clap(long, default_value = "0")]
		range: isize
	},

	/// Play a conundrum (or a teaser).
	#[clap(alias = "teaser")]
	Conundrum {
		#[clap(name = "letters")]
		word: String,
	},

	/// Check if a word is present in the dictionary.
	/// Intended for micro-benchmarking.
//	#[clap(hidden)]
	DictCheck {
		word: String,
	},

	/// Do nothing (but do dictionary initialisation)
//	#[clap(hidden)]
	Nop,
}

////////////////////////////////////////////////////////////////////////

fn main() -> Result<()>
{
	let opt = Opt::parse();
	let dict = opt.use_dict.create()?;

	use Cmd::*;
	match opt.command {
		Letters { word } => {
			for (len, words) in letters(dict, &word) {
				println!("{}\t{}", len, words.join(" "))
			}
		},

		Numbers { a, b, c, d, e, f, target, range } => {
			for (op, x) in numbers(a, b, c, d, e, f, target, range) {
				println!("{} == {}", x, op);
			}
		},

		Conundrum { word } => {
			for word in conundrum(dict, &word) {
				println!("{}", word);
			}
		},

		DictCheck { word } => {
			println!("{} => {:?}", &word, dict.is_word(&word)); },

		Nop => {}
	}

	Ok(())
}

////////////////////////////////////////////////////////////////////////

/*

letters :: Dict -> [L] -> [(Int, [String])]
letters dict str@[_a, _b, _c, _d, _e, _f, _g, _h, _i]
  = groupify
  . groupBy (\x y -> lengthOrd x y == EQ)
  . sortBy lengthOrd
  . filter (isWord dict)
  . quicknub . countdownify $ str
  where
    lengthOrd = \x -> \y -> compare (length x) (length y)
    groupify  = map (\xs -> (length $ head xs, xs))
letters _ _ = error "bad input"

 */

fn letters (dict: Dictx, chars: &str) -> LettersOut
{
	let chars: Vec<u8> =
		chars.bytes().collect();
	let shuf =
		countdownify(&chars);
	println!("{:?}", &shuf);

	let xs: Vec<String> =
		shuf
		.into_par_iter()
		.map(|x| unsafe { String::from_utf8_unchecked(x) })
		.filter(|x| dict.is_word(x))
		.collect();
	println!("{:?}", &xs);

	group_by_len(xs)
}

fn group_by_len(xs: Vec<String>) -> LettersOut
{
	let mut out = LettersOut::new();
	for x in xs {
		let wl = out.entry(x.len()).or_insert_with(Vec::new);
		wl.push(x);
	}

	for (len, wl) in &mut out { wl.sort(); }
	out
}


////////////////////////////////////////////////////////////////////////

/*
numbers :: (N, N, N, N, N, N) -> N -> [(Ops, N)]
numbers (a,b,c,d,e,f) target = hits
  where
    hits = [ (x, y) | (x, (Just y)) <- okevals, y `elem` nearTarget ]

    nearTarget = [ (target - targetrange) .. (target + targetrange) ]
    targetrange = 0

    okevals = filter (isJust . snd) evalops
    evalops = map (\x -> (x, eval x)) ops
    ops  = concatMap (insertop) seqs
    seqs = countdownify nums
    nums = [a, b, c, d, e, f]
*/

type N = usize;

fn numbers (
	a: N, b: N, c: N, d: N, e: N, f: N,
	target: N, range: isize
) -> Vec<(RN, N)>
{
	assert!(1 <= target && target <= 999);

	fn k (n: N) -> RN { R::new(Op::K(n)) }

	let mut ops = HashSet::new();
	for xs in countdownify(&[k(a), k(b), k(c), k(d), k(e), k(f)]) {
		ops.extend(insertop(&xs));
	}

	ops.into_par_iter()
		.map(|op| { let x = op.eval(); (op, x) })
		.filter(|(op, x)| x.is_some())
		.map(|(op, x)| (op, x.unwrap()))
		.filter(|(op, x)| near_target(target, range, *x))
    .collect()
}

fn near_target (target: N, range: isize, x: N) -> bool
{
	let target_min = (target as isize) - range;
	let target_max = (target as isize) + range;

	target_min <= (x as isize) && (x as isize) <= target_max
}

#[test]
fn test_near_target()
{
	assert!(  near_target(100, 0, 100));
	assert!(! near_target( 99, 0, 100));
	assert!(  near_target( 99, 1, 100));
	assert!(! near_target(101, 0, 100));
	assert!(  near_target(101, 1, 100));
}

#[derive(Clone, Debug, Hash, Eq)]
enum Op {
	K (N),
	Add (R<Op>, R<Op>),
	Sub (R<Op>, R<Op>),
	Mul (R<Op>, R<Op>),
	Div (R<Op>, R<Op>),
}

impl Op {
	fn eval (&self) -> Option<N>
	{
		use crate::Op::*;
		match self {
			K(n)      => Some(*n),
			Add(a, b) => Op::eval_both(a, b).map(|(a,b)| a + b),
			Sub(a, b) => Op::eval_both(a, b).map(|(a,b)| a - b),
			Mul(a, b) => Op::eval_both(a, b).map(|(a,b)| a * b),
			Div(a, b) => Op::eval_both(a, b).and_then(Op::eval_div)
		}
	}

	fn eval_both (a: &Op, b: &Op) -> Option<(N, N)>
	{
		a.eval().and_then(|a|
		b.eval().and_then(|b|
			Some((a, b))))
	}

	fn eval_div ((a, b): (N, N)) -> Option<N>
	{
		match (a, b) {
			(_, 0) | (_, 1)     => None,  // don't divide by 0 or 1
			(_, _) if !dvd(a,b) => None,  // CDN rules say exact divison only
			(a, b)              => Some(a / b)
		}
	}
}

#[inline(always)]
fn dvd<T> (a: T, b: T) -> bool
where T: std::ops::Rem<Output = N>,
{
	(a % b) == 0
}

impl PartialEq for Op {
    fn eq (&self, other: &Op) -> bool
	{
		use crate::Op::*;
		// commutativity-aware equality prunes
		// massive repetition out of the output
		// `doNumbers'` mechanism,
		// with a well-placed call to `nub`
		match (self, other) {
			(K (k1), K (k2)) => k1 == k2,
			(Add (a1, b1), Add (a2, b2)) => coeq (a1, b1, a2, b2),
			(Mul (a1, b1), Mul (a2, b2)) => coeq (a1, b1, a2, b2),
			(Sub (a1, b1), Sub (a2, b2)) => a1 == a2 && b1 == b2,
			(Div (a1, b1), Div (a2, b2)) => a1 == a2 && b1 == b2,
			(_, _) => false
		}
	}
}

impl fmt::Display for Op {
    fn fmt (&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
	{
		use crate::Op::*;
		match self {
			K (n) => write!(f, "{}", n),
			Add (a, b) => write!(f, "({} + {})", a, b),
			Sub (a, b) => write!(f, "({} - {})", a, b),
			Mul (a, b) => write!(f, "({} * {})", a, b),
			Div (a, b) => write!(f, "({} / {})", a, b),
		}
	}
}

fn coeq<T> (a1: T, b1: T, a2: T, b2: T) -> bool
where T: PartialEq
{
	(a1 == a2 && b1 == b2) ||
	(a1 == b2 && a2 == b1)
}

type RN = R<Op>;

#[allow(non_snake_case)]
fn insertop (ns: &[RN]) -> Vec<RN>
{
	fn k   (n: N)         -> RN { R::new(Op::K(n)) }
	fn Add (a: RN, b: RN) -> RN { R::new(Op::Add(a, b)) }
	fn Sub (a: RN, b: RN) -> RN { R::new(Op::Sub(a, b)) }
	fn Mul (a: RN, b: RN) -> RN { R::new(Op::Mul(a, b)) }
	fn Div (a: RN, b: RN) -> RN { R::new(Op::Div(a, b)) }

	if ns.is_empty() { return vec![] }
	let x = ns[0].clone();
	if ns.len() == 1 { return vec![ x ] }
	assert!(ns.len() >= 2);
	let (_, xs) = ns.split_first().unwrap();

	insertop(xs).iter().map(|xs| {
		vec![
			Add(x.clone(), xs.clone()), Add(xs.clone(), x.clone()),
			Sub(x.clone(), xs.clone()), Sub(xs.clone(), x.clone()),
			Mul(x.clone(), xs.clone()), Mul(xs.clone(), x.clone()),
			Div(x.clone(), xs.clone()), Div(xs.clone(), x.clone()),
		]
	})
		.concat().into_iter()
		.collect()
}

/*
insertop :: [N] -> [Ops]
insertop []     = []
insertop (x:[]) = [K x]
insertop (x_:xs) =
  concatMap (\xs' ->
               [ Add x xs', Add xs' x
               , Sub x xs', Sub xs' x
               , Mul x xs', Mul xs' x
               , Div x xs', Div xs' x
               ]) $ insertop xs
  where x = K x_

countdownify :: [a] -> [[a]]
countdownify = concatMap (permutations) . subsequences

quicknub :: Ord a => [a] -> [a]
quicknub = Set.toList . Set.fromList
*/


////////////////////////////////////////////////////////////////////////

/*
conundrum :: Dict -> [L] -> [String]
conundrum dict word
  = filter (isWord dict) . quicknub . permutations $ word
*/

fn conundrum (dict: Dictx, word: &str) -> Vec<String>
{
	word
		.bytes()
		.permutations(word.len())
    .collect::<HashSet<_>>().into_par_iter() // faster
		.map(|x| unsafe { String::from_utf8_unchecked(x) })
		.filter(|x| dict.is_word(x))
		.collect()
}


////////////////////////////////////////////////////////////////////////

type P<T> = HashSet<Vec<T>>;

fn nub<T> (mut xs: Vec<T>) -> Vec<T>
where T: Eq + Hash
{
	xs
		.drain(..).collect::<HashSet<_>>()
		.drain().collect()
}

use itertools::{Permutations, Powerset};
use std::slice::Iter;
struct Countdownify<'a, T> {
	xs:    &'a [T],
	powerset:  Option<Powerset<Iter<'a, T>>>,
	ps_curr: Option<Vec<&'a T>>,
	ps_curr_i: Option<Iter<'a, &'a T>>,
	permutes: Option<Permutations<std::option::IntoIter<Vec<&'a T>>>>
}

impl<'a, T> Countdownify<'a, T>
where T: Clone + Debug + Eq + Hash
{
	fn new (xs: &'a [T]) -> Countdownify<'a, T>
	{
		Countdownify {
			xs,
			powerset:  None,
			ps_curr:   None,
			ps_curr_i: None,
			permutes:  None
		}
	}

	fn pow_init (&mut self)
	{
		if self.powerset.is_none() {
			self.powerset = Some(self.xs.iter().powerset());
		}
	}

	fn pow_next (&mut self)
	{
		self.pow_init();
		if let Some(pow) = &mut self.powerset {
			self.ps_curr = pow.next();
		} else {
			unreachable!()
		}
	}
}

impl<'a, T> Iterator for Countdownify<'a, T>
where T: Clone + Debug + Eq + Hash
{
	type Item = T;

	fn next (&mut self) -> Option<Self::Item>
	{
		self.pow_init();
		if self.permutes.is_none() {
			self.pow_next();
			if self.ps_curr.is_none() { return None }
			let ps_curr = self.ps_curr.as_ref();
			let xlen    = ps_curr.len();
		}

        todo!()
    }
}


fn countdownify<'a, T> (xs: &'a [T]) -> Vec<Vec<T>>
where T: Clone + Debug + Eq + Hash
{
	Countdownify::countdownify_vec(xs)
}

impl<'a, T> Countdownify<'a, T>
where T: Clone + Debug + Eq + Hash
{
	fn countdownify_vec (xs: &'a [T]) -> Vec<Vec<T>>
	{
		let mut out: Vec<Vec<T>> =
			Vec::with_capacity(2usize.pow(xs.len() as u32) * 4);
		xs.iter();
		for subseqs in xs.iter().powerset() {
			let xlen = subseqs.len();
			for shuf in subseqs.into_iter().permutations(xlen) {
				out.push(shuf.iter().map(|x| (*x).clone()).collect_vec());
			}
		}
		out
	}

	fn countdownify_by_item (xs: &'a [T]) -> Vec<Vec<T>>
	{
		let mut out: P<T> = HashSet::new();
		for subseqs in xs.iter().powerset() {
			let xlen = subseqs.len();
			for shuf in subseqs.into_iter().permutations(xlen) {
				out.insert(shuf.iter().map(|x| (*x).clone()).collect_vec());
			}
		}
		out.drain().collect()
	}

	fn countdownify_by_index (xs: &'a [T]) -> Vec<Vec<T>>
	{
		let mut out: P<T> = HashSet::new();
		for subseqs in (0..xs.len()).powerset() {
			let xlen = subseqs.len();
			for shuf in subseqs.into_iter().permutations(xlen) {
				let mut this = vec![];
				for i in shuf { this.push(xs[i].clone()); }
				out.insert(this);
			}
		}
		out.drain().collect()
	}
}

/*

[] => []
[a] => [[a]]
[a,b] => [[a,b], [b,a]]
[a,b,c] => [a,b,c],[a,c,b]


[a,b,c] =>
  [ []
  , [a], [b], [c]
  , [a,b], [b,a], [a,c], [c,a], [b,c], [c,b],
  , [a,b,c], [a,c,b], [b,a,c], [b,c,a], [c,a,b], [c,b,a]
  ]

*/


////////////////////////////////////////////////////////////////////////

#[derive(ArgEnum, Clone, Debug, PartialEq)]
enum DictOpt {
	/// Use a `Vec<String>` for the dictionary.
	#[clap(name="vec")]
	UseVec,
	/// Use a `BTreeSet<String>` for the dictionary.
	#[clap(name="btree-set")]
	UseBTreeSet,
	/// Use a `HashSet<String>` for the dictionary.
	#[clap(name="hash-set")]
	UseHashSet,
}

impl DictOpt {
	fn create(&self) -> Result<Dictx>
	{
		use DictOpt::*;
		match self {
			UseVec =>
				Ok(R::new(
					<Vec<String> as Dict>::load_dict()?)),
			UseBTreeSet =>
				Ok(R::new(
					<BTreeSet<String> as Dict>::load_dict()?)),
			UseHashSet =>
				Ok(R::new(
					<HashSet<String> as Dict>::load_dict()?)),
		}
	}
}


//
// Benchmark #1: lan --use-dict vec nop
//   Time (mean ± σ):      35.7 ms ±   1.3 ms    [User: 26.7 ms, System: 9.4 ms]
//   Range (min … max):    33.2 ms …  41.0 ms    71 runs
//
// Benchmark #3: lan --use-dict vec dict-check alpha
//   Time (mean ± σ):      35.5 ms ±   1.5 ms    [User: 26.7 ms, System: 9.1 ms]
//   Range (min … max):    33.2 ms …  42.5 ms    66 runs
// Benchmark #4: lan --use-dict vec dict-check zulu
//   Time (mean ± σ):      36.7 ms ±   1.1 ms    [User: 29.1 ms, System: 7.9 ms]
//   Range (min … max):    34.6 ms …  38.9 ms    65 runs
//
impl Dict for Vec<String> {
	fn load_dict () -> Result<Self>
	{
		let mut this = Self::new();
		load_dict_inner(&mut |it| { this.push(it); })?;
		Ok(this)
	}

	fn is_word (&self, word: &str) -> bool
	{
		self.contains(&word.to_owned())
	}
}


//
// Benchmark #5: lan --use-dict btree-set nop
//   Time (mean ± σ):     121.6 ms ±   3.3 ms    [User: 113.9 ms, System: 7.8 ms]
//   Range (min … max):   115.5 ms … 130.5 ms    23 runs
//
// Benchmark #7: lan --use-dict btree-set dict-check alpha
//   Time (mean ± σ):     121.4 ms ±   2.7 ms    [User: 108.3 ms, System: 12.8 ms]
//   Range (min … max):   115.9 ms … 125.9 ms    22 runs
// Benchmark #8: lan --use-dict btree-set dict-check zulu
//   Time (mean ± σ):     120.4 ms ±   3.5 ms    [User: 108.3 ms, System: 12.2 ms]
//   Range (min … max):   112.8 ms … 125.3 ms    22 runs
//
impl Dict for BTreeSet<String> {
	fn load_dict () -> Result<Self>
	{
		let mut this = Self::new();
		load_dict_inner(&mut |it| { this.insert(it); })?;
		Ok(this)
	}

	fn is_word (&self, word: &str) -> bool
	{
		self.contains(&word.to_owned())
	}
}


//
// Benchmark #9: lan --use-dict hash-set nop
//   Time (mean ± σ):     131.2 ms ±   4.5 ms    [User: 113.9 ms, System: 17.3 ms]
//   Range (min … max):   123.0 ms … 140.1 ms    22 runs
//
// Benchmark #11: lan --use-dict hash-set dict-check alpha
//   Time (mean ± σ):     134.2 ms ±   6.4 ms    [User: 117.7 ms, System: 16.1 ms]
//   Range (min … max):   122.8 ms … 141.8 ms    20 runs
// Benchmark #12: lan --use-dict hash-set dict-check zulu
//   Time (mean ± σ):     134.7 ms ±   5.7 ms    [User: 113.9 ms, System: 20.3 ms]
//   Range (min … max):   124.4 ms … 147.7 ms    20 runs
//
impl Dict for HashSet<String> {
	fn load_dict () -> Result<Self>
	{
		let mut this = Self::new();
		load_dict_inner(&mut |it| { this.insert(it); })?;
		Ok(this)
	}

	fn is_word (&self, word: &str) -> bool
	{
		self.contains(&word.to_owned())
	}
}


fn load_dict_inner (insert_fn: &mut dyn FnMut(String) -> ()) -> Result<()>
{
	let f = File::open(WORDS_FILE)?;
	let r = BufReader::new(f);
	for l in r.lines() { insert_fn(l?); }
	Ok(())
}


////////////////////////////////////////////////////////////////////////
