/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

use z3::*;
use std::collections::BTreeMap;

#[tracing::instrument]
pub(super) fn run()
{
	let xxs = [
		['P', 'E', 'R'],
		['N', 'H', 'U'],
		['G', 'Y', 'T'],
		['A', 'O', 'M']
	];

	nytsolv::solver::run_with_z3(|ctx, solv| {
		let (cs, cc, _ct) = setup_char_sort(&ctx);
		let bigram_fn = setup_bigram_fn(&ctx, &cs);
		setup_bigram_axioms(&solv, &bigram_fn, &cc, &xxs);
		let trigram_fn = setup_trigram_fn(&ctx, &cs);
		setup_trigram_axioms(&ctx, &solv, &cs, &bigram_fn, &trigram_fn);

		use SatResult::*;
		match solv.check() {
			SatResult::Unknown => {
				println!("unknown");
				println!("{}", solv.get_reason_unknown().unwrap());
			},
			SatResult::Unsat => {
				println!("unsat");
				println!("{:#?}", solv.get_unsat_core());
			},
			SatResult::Sat => {
				println!("sat");
				println!("{:#?}", solv.get_model().unwrap());

//				Some(read_model(&cc, &grid, &solv.get_model().unwrap()))
			},
		}
	})
}

#[tracing::instrument]
fn setup_char_sort (ctx: &Context) ->
	(Sort<'_>, BTreeMap<char, FuncDecl<'_>>, Vec<FuncDecl<'_>>)
{
	let syms: Vec<_> = ('A'..'Z')
		.map(String::from)
		.map(Symbol::from)
		.collect();
	let (cs, cc, ct) = Sort::enumeration(ctx, "Char".into(), &syms);
	(cs, ('A'..'Z').into_iter().zip(cc).collect(), ct)
}

#[tracing::instrument]
fn setup_bigram_fn<'ctx> (ctx: &'ctx Context, cs: &Sort<'ctx>)
	-> FuncDecl<'ctx>
{
	FuncDecl::new(ctx, "bigram", &[&cs, &cs], &Sort::bool(ctx))
}

#[tracing::instrument]
fn setup_bigram_axioms<'ctx> (
	solv: &Solver<'ctx>,
	func: &FuncDecl<'ctx>,
	cc:   &BTreeMap<char, FuncDecl<'_>>,
	xxs:  &[[char; 3]; 4]
)
{
	let ch = |x| cc.get(x).unwrap().apply(&[]);
	let bigram_app =
		|t, u| func.apply(&[&ch(t), &ch(u)]).as_bool().unwrap();

	for (a, j, k) in itertools::iproduct!(0..4, 0..3, 0..3) {
		solv.assert(&bigram_app(&xxs[a][j], &xxs[a][k]).not());
	}


	for (a, ai, b, bj) in itertools::iproduct!(0..4, 0..3, 0..4, 0..3) {
		if a == b { continue }
		println!("(assert (bigram {} {}))", xxs[a][ai], xxs[b][bj]);
		solv.assert(&bigram_app(&xxs[a][ai], &xxs[b][bj]));
	}
}

#[tracing::instrument]
fn setup_trigram_fn<'ctx> (ctx: &'ctx Context, cs: &Sort<'ctx>)
	-> FuncDecl<'ctx>
{
	FuncDecl::new(ctx, "trigram", &[&cs, &cs, &cs], &Sort::bool(ctx))
}

#[tracing::instrument]
fn setup_trigram_axioms<'ctx> (
	ctx: &'ctx Context,
	solv: &Solver<'ctx>,
	cs: &Sort<'ctx>,
	bigram_fn: &FuncDecl<'ctx>,
	trigram_fn: &FuncDecl<'ctx>,
)
{
	use z3::ast::{Ast, Bool, Datatype, forall_const};
	let pat = || {
		let t = Datatype::fresh_const(ctx, "trgm", cs);
		let tp = Pattern::new(ctx, &[&t]);
		(t, tp)
	};

	let (t, tp) = pat();
	let (u, up) = pat();
	let (v, vp) = pat();
	solv.assert(&forall_const(
		ctx, &[], &[&tp, &up, &vp],
		&Bool::and(ctx, &[
			&bigram_fn.apply(&[&t, &u]).as_bool().unwrap(),
			&bigram_fn.apply(&[&u, &v]).as_bool().unwrap(),
		])._eq(
			&trigram_fn.apply(&[&t, &u, &v]).as_bool().unwrap()
		)
	));
}
