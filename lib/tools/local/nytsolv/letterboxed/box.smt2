;; 	      P   E   R
;; 	  ┌───o───o───o───┐
;; 	  │               │
;; 	A o               o N
;; 	  │               │
;; 	O o               o H
;; 	  │               │
;; 	M o               o U
;; 	  │               │
;; 	  └───o───o───o───┘
;; 	      G   Y   T
;;
;; t IN { P, E, R }  &&  u IN { N, H, U, G, Y, T, A, O, M }  &&  bigram t u

(set-option :produce-assignments true)
(set-option :produce-models      true)
(set-option :produce-proofs      true)
(set-option :produce-unsat-cores true)

(declare-datatype
 Char
 ((A) (B) (C) (D) (E) (F) (G) (H) (I)
  (J) (K) (L) (M) (N) (O) (P) (Q) (R)
  (S) (T) (U) (V) (W) (X) (Y) (Z)))


(declare-fun bigram  (Char Char)      Bool)

(assert (not (bigram P P)))
(assert (not (bigram P E)))
(assert (not (bigram P R)))
(assert (not (bigram E P)))
(assert (not (bigram E E)))
(assert (not (bigram E R)))
(assert (not (bigram R P)))
(assert (not (bigram R E)))
(assert (not (bigram R R)))
(assert (not (bigram N N)))
(assert (not (bigram N H)))
(assert (not (bigram N U)))
(assert (not (bigram H N)))
(assert (not (bigram H H)))
(assert (not (bigram H U)))
(assert (not (bigram U N)))
(assert (not (bigram U H)))
(assert (not (bigram U U)))
(assert (not (bigram G G)))
(assert (not (bigram G Y)))
(assert (not (bigram G T)))
(assert (not (bigram Y G)))
(assert (not (bigram Y Y)))
(assert (not (bigram Y T)))
(assert (not (bigram T G)))
(assert (not (bigram T Y)))
(assert (not (bigram T T)))
(assert (not (bigram A A)))
(assert (not (bigram A O)))
(assert (not (bigram A M)))
(assert (not (bigram O A)))
(assert (not (bigram O O)))
(assert (not (bigram O M)))
(assert (not (bigram M A)))
(assert (not (bigram M O)))
(assert (not (bigram M M)))

(assert (and
	 (bigram P N) (bigram P H) (bigram P U)
	 (bigram P G) (bigram P Y) (bigram P T)
	 (bigram P A) (bigram P O) (bigram P M)
	 (bigram E N) (bigram E H) (bigram E U)
	 (bigram E G) (bigram E Y) (bigram E T)
	 (bigram E A) (bigram E O) (bigram E M)
	 (bigram R N) (bigram R H) (bigram R U)
	 (bigram R G) (bigram R Y) (bigram R T)
	 (bigram R A) (bigram R O) (bigram R M)
	 (bigram N P) (bigram N E) (bigram N R)
	 (bigram N G) (bigram N Y) (bigram N T)
	 (bigram N A) (bigram N O) (bigram N M)
	 (bigram H P) (bigram H E) (bigram H R)
	 (bigram H G) (bigram H Y) (bigram H T)
	 (bigram H A) (bigram H O) (bigram H M)
	 (bigram U P) (bigram U E) (bigram U R)
	 (bigram U G) (bigram U Y) (bigram U T)
	 (bigram U A) (bigram U O) (bigram U M)
	 (bigram G P) (bigram G E) (bigram G R)
	 (bigram G N) (bigram G H) (bigram G U)
	 (bigram G A) (bigram G O) (bigram G M)
	 (bigram Y P) (bigram Y E) (bigram Y R)
	 (bigram Y N) (bigram Y H) (bigram Y U)
	 (bigram Y A) (bigram Y O) (bigram Y M)
	 (bigram T P) (bigram T E) (bigram T R)
	 (bigram T N) (bigram T H) (bigram T U)
	 (bigram T A) (bigram T O) (bigram T M)
	 (bigram A P) (bigram A E) (bigram A R)
	 (bigram A N) (bigram A H) (bigram A U)
	 (bigram A G) (bigram A Y) (bigram A T)
	 (bigram O P) (bigram O E) (bigram O R)
	 (bigram O N) (bigram O H) (bigram O U)
	 (bigram O G) (bigram O Y) (bigram O T)
	 (bigram M P) (bigram M E) (bigram M R)
	 (bigram M N) (bigram M H) (bigram M U)
	 (bigram M G) (bigram M Y) (bigram M T)
	 ))
;	(declare-fun trigram (Char Char Char) Bool)
;	(assert
;	 (forall ((t Char) (u Char) (v Char))
;		 (= (and (bigram t u) (bigram u v))
;		    (trigram t u v))))

(check-sat)
(get-model)
