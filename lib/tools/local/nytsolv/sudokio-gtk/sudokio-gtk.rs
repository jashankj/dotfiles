/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(unused_imports)]

use color_eyre::eyre::{Result, eyre};

fn main () -> Result<()>
{
	color_eyre::install()?;

	Ok(())
}

