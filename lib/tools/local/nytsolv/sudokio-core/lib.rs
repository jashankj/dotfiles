/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

////////////////////////////////////////////////////////////////////////

pub mod pp;

////////////////////////////////////////////////////////////////////////

macro_rules! defcell {
	( $( $id:ident / $n:literal ),+ ) =>
	{
		#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
		pub enum Cell {
			$( $id ),+
		}

		impl From<u8> for Cell
		{
			fn from (n: u8) -> Cell
			{
				use Cell::*;
				match n {
					$( $n => return $id, )+
					_ => unimplemented!()
				}
			}
		}

		impl From<Cell> for u8
		{
			fn from (c: Cell) -> u8
			{
				use Cell::*;
				match c { $( $id => $n ),+ }
			}
		}

		impl From<Cell> for char
		{
			fn from (c: Cell) -> char
			{
				char::from_digit(u8::from(c) as u32, 10).unwrap()
			}
		}

		impl From<Cell> for &'static str
		{
			fn from (c: Cell) -> &'static str
			{
				use Cell::*;
				match c { $( $id => stringify!($n) ),+ }
			}
		}
	}
}

defcell! {
	N1 / 1, N2 / 2, N3 / 3,
	N4 / 4, N5 / 5, N6 / 6,
	N7 / 7, N8 / 8, N9 / 9
}

pub trait Cellish:
	Copy + Clone + std::fmt::Debug + std::cmp::PartialEq
{
	fn to_string (self) -> String;
}

impl Cellish for Cell
{
	fn to_string (self) -> String
	{
		format!("{}", char::from(self))
	}
}

impl Cellish for Option<Cell>
{
	fn to_string (self) -> String
	{
		match self {
			None    => String::from(" "),
			Some(x) => x.to_string(),
		}
	}
}

pub const N: usize = 9;

#[repr(transparent)]
#[derive(Debug, PartialEq)]
pub struct GSudoku<T> (pub [[T; N]; N])
	where T: std::fmt::Debug + std::cmp::PartialEq;

#[derive(Debug)]
pub struct GRow<T> (pub [T; N]);


impl<T: std::fmt::Debug + std::cmp::PartialEq> GSudoku<T>
{
	pub fn cell_ref (i: usize, j: usize) -> [char; 3]
	{
		assert! (i < N && j < N);
		let i: char = char::from_digit(i as u32, 10).unwrap();
		let j: char = char::from_digit(j as u32, 10).unwrap();
		['c', i, j]
	}
}

impl<T: Cellish> GSudoku<T>
{
	pub fn format (&self) -> String
	{
		self.invoke::<T>(None)
	}

	pub fn format_whole<U: Cellish> (&self, that: &GSudoku<U>) -> String
	{
		self.invoke(Some(that))
	}

	pub fn invoke<U: Cellish> (&self, that: Option<&GSudoku<U>>) -> String
	{
		crate::pp::sudoku(self, that)
	}
}

pub type SudokuPartial = GSudoku<Option<Cell>>;
pub type Sudoku        = GSudoku<Cell>;

////////////////////////////////////////////////////////////////////////

#[cfg(test)]
pub fn test_vector_0() -> (SudokuPartial, Sudoku)
{
	use std::option::Option::None as X;
	use crate::Cell::*;
	let n1 = Some(N1); let n2 = Some(N2); let n3 = Some(N3);
	let n4 = Some(N4); let n5 = Some(N5); let n6 = Some(N6);
	let n7 = Some(N7); let n8 = Some(N8); let n9 = Some(N9);

	let s = GSudoku([
		[ X,  X,  X,   X, n1,  X,   X,  X,  X],
		[ X, n3, n4,  n2,  X,  X,   X,  X,  X],
		[ X,  X,  X,  n7, n5,  X,   X,  n6, X],

		[n5, n1,  X,   X,  X, n8,   X, n3,  X],
		[ X, n6,  X,   X,  X,  X,  n7, n1,  X],
		[ X,  X, n9,   X,  X,  X,   X,  X,  X],

		[n9,  X,  X,  n4,  X, n6,  n3,  X,  X],
		[ X,  X, n1,   X, n3,  X,   X,  X,  X],
		[ X, n2,  X,   X,  X,  X,  n9, n5,  X],
	]);

	let t = GSudoku([
		[N6, N7, N5,  N8, N1, N3,  N4, N9, N2],
		[N1, N3, N4,  N2, N6, N9,  N5, N8, N7],
		[N8, N9, N2,  N7, N5, N4,  N1, N6, N3],

		[N5, N1, N7,  N6, N4, N8,  N2, N3, N9],
		[N4, N6, N3,  N5, N9, N2,  N7, N1, N8],
		[N2, N8, N9,  N3, N7, N1,  N6, N4, N5],

		[N9, N5, N8,  N4, N2, N6,  N3, N7, N1],
		[N7, N4, N1,  N9, N3, N5,  N8, N2, N6],
		[N3, N2, N6,  N1, N8, N7,  N9, N5, N4],
	]);

	(s, t)
}

////////////////////////////////////////////////////////////////////////
