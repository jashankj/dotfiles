/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

////////////////////////////////////////////////////////////////////////

use super::{Sudoku, SudokuPartial, GSudoku, GRow, Cellish};

use itertools::Itertools;
use owo_colors::OwoColorize;

#[allow(non_upper_case_globals)]
mod q {
	macro_rules! Q {
		( $id:ident => $x:expr ) => {
			pub(super) const $id : char = $x ;
		}
	}

	Q!(HH => '━'); Q!(hh => '─');
	Q!(VV => '┃'); Q!(vv => '│');
	Q!(TL => '┏'); Q!(Tc => '┯'); Q!(TC => '┳'); Q!(TR => '┓');
	Q!(rL => '┠'); Q!(rc => '┼'); Q!(rC => '╂'); Q!(rR => '┨');
	Q!(RL => '┣'); Q!(Rc => '┿'); Q!(RC => '╋'); Q!(RR => '┫');
	Q!(BL => '┗'); Q!(Bc => '┷'); Q!(BC => '┻'); Q!(BR => '┛');
}

pub(super) fn sudoku<T: Cellish, U: Cellish> (o: &GSudoku<T>, i: Option<&GSudoku<U>>) -> String
{
	let row: fn(_, _, _) -> String =
		if i.is_some() { row_answer } else { row_normal };

	[
		rule_header(),

		row(0, o, i),
		rule_minor(),
		row(1, o, i),
		rule_minor(),
		row(2, o, i),

		rule_major(),

		row(3, o, i),
		rule_minor(),
		row(4, o, i),
		rule_minor(),
		row(5, o, i),

		rule_major(),

		row(6, o, i),
		rule_minor(),
		row(7, o, i),
		rule_minor(),
		row(8, o, i),

		rule_footer()
	]
		.join("\n")
}

fn row_normal<T: Cellish, U: Cellish> (n: usize, o: &GSudoku<T>, _i: Option<&GSudoku<U>>) -> String
{
	row_normal_inner(GRow(o.0[n]))
}

fn row_normal_inner<T: Cellish> (a: GRow<T>) -> String
{
	row_inner(a.0.map(T::to_string).map(pad).into_iter())
}

fn row_answer<T: Cellish, U: Cellish> (n: usize, o: &GSudoku<T>, i: Option<&GSudoku<U>>) -> String
{
	row_answer_inner(GRow(o.0[n]), GRow(i.unwrap().0[n]))
}

fn row_answer_inner<T: Cellish, U: Cellish> (GRow(o): GRow<T>, GRow(i): GRow<U>) -> String
{
	row_inner(
		i.into_iter().zip(o.into_iter())
			.map(|(ix, ox)| {
				let ix = ix.to_string();
				let ox = ox.to_string();
				let s = pad(ix.clone());
				match ix == ox {
					false => format!("{}", s.bold().red()),
					true  => s,
				}
			})
	)
}

fn row_inner<T: Iterator<Item = String>> (a: T) -> String
{
	use q::{vv, VV};
	[VV, vv, vv, VV, vv, vv, VV, vv, vv, VV]
		.into_iter()
		.map(String::from)
		.interleave(a)
		.collect()
}

fn pad (x: String) -> String
{
	use std::iter::once;
	let sp = String::from(' ');
	once(sp.clone())
		.chain(once(x))
		.chain(once(sp))
		.collect()
}

fn rule_header () -> String
{
	use q::{HH, TL, Tc, TC, TR};
	unruled(x_N(HH, 3), [TL, Tc, Tc, TC, Tc, Tc, TC, Tc, Tc, TR])
}

fn rule_minor () -> String
{
	use q::{hh, rL, rc, rC, rR};
	unruled(x_N(hh, 3), [rL, rc, rc, rC, rc, rc, rC, rc, rc, rR])
}

fn rule_major () -> String
{
	use q::{HH, RL, Rc, RC, RR};
	unruled(x_N(HH, 3), [RL, Rc, Rc, RC, Rc, Rc, RC, Rc, Rc, RR])
}

fn rule_footer () -> String
{
	use q::{HH, BL, Bc, BC, BR};
	unruled(x_N(HH, 3), [BL, Bc, Bc, BC, Bc, Bc, BC, Bc, Bc, BR])
}

#[allow(unstable_name_collisions)]
fn unruled (x: String, y: [char; 10]) -> String
{
	y
		.into_iter()
		.map(String::from)
		.intersperse(x)
		.collect()
}

fn x_N<T: Clone, U: FromIterator<T>> (x: T, n: usize) -> U
{
	std::iter::once(x).cycle().take(n).collect()
}

////////////////////////////////////////////////////////////////////////

#[test]
fn try_pretty()
{
	let (input, expect) = crate::test_vector_0();

	println!("{}", input.format());
	println!("{}", input.format_whole(&expect));
}
