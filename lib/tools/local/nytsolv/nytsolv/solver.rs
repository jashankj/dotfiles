/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2022 Jashank Jeremy <jashank@rulingia.com.au>
 */

use z3::*;

pub fn run_with_z3<F, T> (inner: F) -> T
where F: Fn(&z3::Context, &z3::Solver<'_>) -> T
{
	let cfg = Config::new();
	let ctx = Context::new(&cfg);
	let solv = Solver::new(&ctx);
	let _opts = setup_options(&ctx);
	inner(&ctx, &solv)
}

pub fn setup_options (ctx: &Context) -> Params<'_>
{
	let mut opts = Params::new(ctx);
	opts.set_bool("produce-assignments", true);
	opts.set_bool("produce-models",      true);
	opts.set_bool("produce-proofs",      true);
	opts.set_bool("produce-unsat-cores", true);
	opts
}
