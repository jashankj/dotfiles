/*-
 * SPDX-License-Identifier: Apache-2.0
 * Copyright 2022 Jashank Jeremy <jashank at rulingia dot com dot au>
 */

//!
//! hola
//!

#![allow(dead_code, unused_imports)]

use std::{
	collections::HashMap,
	fmt::{self, Debug},
	path::{PathBuf, Path},
	rc::Rc,
	sync::Arc,
	time::Duration
};

use async_trait::async_trait;
use chrono::prelude::*;
use color_eyre::eyre::{Result, WrapErr, bail, eyre};
use lazy_static::lazy_static;
use serde::{Serialize, Deserialize};
use tokio::{
	fs,
	io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt},
	net::{TcpListener, UnixListener}
};
use tracing::{trace, debug, info, warn, error, event, span};


////////////////////////////////////////////////////////////////////////

// ########################################################################
// # A mapping from a client name to a list of blocks to render to them.
// #
// # ORDER :: Map<Symbol, [Symbol]>
// ORDER = {
//   # alyzon LHS screen: task status
//   alyzon_left: %i[
//     todoist
//     loadavg
//     date
//   ],

//   # alyzon RHS screen: system status
//   alyzon_right: %i[
//     ifwatch_bridge0
//     ifwatch_em0
//     ifwatch_msk0
//     ifwatch_tun0
//     thermal
//     cpufreq
//     loadavg
//     date
//   ],

//   # jaenelle
//   jaenelle: %i[
//     ifwatch_bond0
//     ifwatch_enp0s25
//     wlwatch_wlp3s0
//     ifwatch_bnep0
//     ifwatch_tun0
//     ifwatch_tun1
//     battery
//     thermal_jaenelle
//     cpufreq_jaenelle
//     loadavg
//     date
//   ],

//   # lisbon --- TODO add ifwatch/wlwatch
//   lisbon: %i[
//     ifwatch_bond0
//     ifwatch_enp2s0f0
//     wlwatch_wlp3s0
//     battery
//     thermal_lisbon
//     cpufreq_lisbon
//     loadavg
//     date
//   ],

//   # menolly
//   menolly: %i[
//     ifwatch_eth0
//     thermal_menolly
//     cpufreq_menolly
//     loadavg
//     date
//   ],

//   # wedge
//   wedge: %i[
//     ifwatch_eno1
//     thermal_wedge
//     cpufreq_wedge
//     loadavg
//     date
//   ],

//   # yarn
//   yarn: %i[
//     wlwatch_wlo0
//     thermal_yarn
//     cpufreq_yarn
//     battery
//     loadavg
//     date
//   ],
// }.freeze

// USE_DBUS   = true
// USE_NOTIFY = false
// NOTIFY_ON  = :wedge
// TIMEOUT    = 1.0
// DEBUG      = 'TIMEOUT' # true

lazy_static! {
	static ref XDG_BASE_DIRS: xdg::BaseDirectories =
		xdg::BaseDirectories::with_prefix("statusbard").unwrap();
}

#[tracing::instrument]
fn get_pid_file_path () -> Result<PathBuf> // SBD_PIDFILE
{
	Ok(XDG_BASE_DIRS.place_runtime_file("statusbard.pid")?)
}

#[tracing::instrument]
fn get_unix_socket_path () -> Result<PathBuf> // SBD_SOCKET
{
	Ok(XDG_BASE_DIRS.place_runtime_file("statusbard.sock")?)
}

// NaN = Float::NAN

// ########################################################################
// # Debugging configuration

// def whinge(from, text)
//   return unless DEBUG
//   if DEBUG.is_a?(TrueClass) ||
//      (DEBUG.is_a?(Regexp) && DEBUG =~ from) ||
//      (DEBUG.is_a?(String) && DEBUG == from)
//     puts "[#{Time.now.strftime('%F %T')} whinge:#{from}] #{text}"
//   end
// end

// def grumble(from, text)
//   puts "[#{Time.now.strftime('%F %T')} #{from}] #{text}"
// end

// ########################################################################
// # Colour names
// #
// # COLOURS :: Map<Symbol, String>
// COLORS = {
//   fg: '#ffffff',
//   bg: '#000000',
//   red: '#ff0000',
//   green: '#00ff00'
// }.freeze

// ########################################################################
// # Useful symbol names from FontAwesome.
// #
// # SYM :: Map<Symbol, String>
// SYM = {
//   lock: "\u{f023}",
//   info_circle: "\u{f05a}",
//   unlock: "\u{f09c}",
//   rss: "\u{f09e}",
//   tachometer: "\u{f0e4}",
//   sitemap: "\u{f0e8}",
//   caret_square_o_down: "\u{f150}",
//   caret_square_o_up: "\u{f151}",
//   plug: "\u{f1e6}",
//   wifi: "\u{f1eb}",
//   battery_4: "\u{f240}",
//   battery_3: "\u{f241}",
//   battery_2: "\u{f242}",
//   battery_1: "\u{f243}",
//   battery_0: "\u{f244}",
//   pause_circle: "\u{f28b}",
//   bluetooth: "\u{f293}"
// }.to_a.map do |x|
//   k, v = x
//   [k, '<span face="FontAwesome">' + v + '</span>']
// end.to_h.freeze

// ########################################################################
// # DBus support.

// NOTIFICATIONS_SVC = -'org.freedesktop.Notifications'
// NOTIFICATIONS_OBJ = -'/org/freedesktop/Notifications'
// NOTIFICATIONS_IFX = -'org.freedesktop.Notifications'

// # A shim to make life less terrible while interacting with DBus.
// class DBarf
//   def initialize(bus)
//     @bus = bus
//     @services = {}
//     @objects = {}
//     @interfaces = {}
//   end

//   def get_service(svc, id)
//     @services[svc] ||= @bus[id]
//   end

//   def service(svc)
//     @services[svc]
//   end

//   def get_object(svc, obj, id)
//     objkey = _key [svc, obj]
//     @objects[objkey] ||= @services[svc].object id
//     @objects[objkey].introspect
//     @objects[objkey]
//   end

//   def object(svc, obj)
//     @objects[_key([svc, obj])]
//   end

//   def get_interface(svc, obj, ifx, id)
//     objkey = _key [svc, obj]
//     ifxkey = _key [objkey, ifx]
//     @interfaces[ifxkey] ||= @objects[objkey][id]
//   end

//   def interface(svc, obj, ifx)
//     @interfaces[_key([svc, obj, ifx])]
//   end

//   private def _key(els)
//     els.map(&:to_s).join('__').to_sym
//   end
// end

// if USE_DBUS
//   whinge 'DBarf', 'connecting to system dbus... '
//   SYS_BUS = DBarf.new DBus.system_bus
// end

// ########################################################################

// # notification queue
// class Notifier < DBus::Object
//   def initialize(id)
//     super id

//     @id = 0
//     @mutex = Mutex.new
//     @queue = []
//   end

//   IFX_NOTIFY_PARAMS = [
//     'in app_name:s',
//     'in replaces_id:u',
//     'in app_icon:s',
//     'in summary:s',
//     'in body:s',
//     'in actions:av',
//     'in hints:{sv}',
//     'in action_timeout:i'
//   ].freeze

//   IFX_GSI_PARAMS = [
//     'out name:s',
//     'out vendor:s',
//     'out version:s',
//     'out spec_version:s'
//   ].freeze

//   dbus_interface NOTIFICATIONS_IFX do
//     dbus_method :GetCapabilities do
//       ['body']
//     end

//     dbus_method :Notify, IFX_NOTIFY_PARAMS.join(', ') \
//     do |app_name, _replaces_id, _app_icon, summary, body, _actions, _hints, _action_timeout|
//       whinge 'Notifier', "enqueuing new message #{id}"
//       @mutex.synchronize do
//         @queue << {
//           app_name: app_name,
//           summary: summary,
//           body: body.strip,
//           id: @id
//         }
//         @id += 1
//       end
//     end

//     dbus_method :CloseNotification, 'in id:u' \
//     do |id|
//       grumble 'Notifier', "Close #{id}"
//     end

//     dbus_method :GetServerInformation, IFX_GSI_PARAMS.join(', ') \
//     do
//       ['statusbar.rb', 'Rulingia', '$Revision: 1.30 $', '1.2']
//     end
//   end

//   def empty?
//     @mutex.synchronize { @queue.empty? }
//   end

//   def shift
//     @mutex.synchronize { @queue.shift }
//   end

//   class << self
//     def blockify(x)
//       prefix =
//         SYM[:info_circle] + ' ' +
//         x[:app_name] + ': ' +
//         x[:summary]
//       {
//         name: 'notification',
//         full_text: (prefix + ' | ' + x[:body]),
//         short_text: prefix,
//         markup: 'pango'
//       }
//     end
//   end
// end

// def start_notifier
//   whinge 'Notifier', 'launching notifier... '
//   sess_bus = DBus.session_bus
//   begin
//     service = sess_bus.request_service NOTIFICATIONS_SVC
//     notifier = Notifier.new NOTIFICATIONS_OBJ
//     whinge 'Notifier', notifier.inspect
//     service.export notifier

//     Thread.new do
//       dloop = DBus::Main.new
//       dloop << sess_bus
//       dloop.run
//     end
//   rescue DBus::NameRequestError
//     notifier = nil
//   end
//   whinge 'Notifier', 'ok'

//   notifier
// end

// NOTIFIER = USE_NOTIFY ? start_notifier : nil

// ########################################################################
// # Block processes.

// # date_block :: () -> (() -> IO Map)
// def date_block
//   lambda {
//     s = Time.now.strftime('%Y-%m-%d %H:%M:%S')
//     {
//       name: 'date',
//       full_text: s,
//       short_text: s[-8..-1],
//       color: COLORS[:fg],
//       markup: 'pango'
//     }
//   }
// end

// def toggl_block
//   lambda {
//     # TODO: talk to Toggl directly
//     res = `~jashank/bin/toggl now 2>&1`.lines.map(&:strip).first
//     res = '[toggl unavailable]' if res == 'Sent: None'

//     {
//       name: 'toggl',
//       full_text: res,
//       color: COLORS[:fg],
//       markup: 'pango'
//     }
//   }
// end

// def todoist_block
//   lambda {
//     # TODO: talk to Todoist directly
//     karma =
//       begin
//         `todoist-karma`.chomp
//       rescue StandardError
//         ''
//       end

//     {
//       name: 'todoist',
//       full_text: karma,
//       markup: 'pango'
//     }
//   }
// end

// ########################################################################
// # Network interfaces

// # ifwatch_block
// #   :: String
// #   -> ( Symbol | [Symbol] )
// #   -> Bool?
// #   -> (() -> IO Map)
// def ifwatch_block(iface, sym, long = true)
//   lambda {
//     ipa, status =
//       case RbConfig::CONFIG['host_os']
//       when /freebsd/ then ifwatch_freebsd iface
//       when /linux/   then ifwatch_linux iface
//       else                ['aowijf', false]
//       end

//     if sym.is_a? Array
//     then up_sym, down_sym = sym
//     else up_sym = down_sym = sym
//     end

//     ipa ||= ''

//     {
//       name: 'ifwatch_' + iface,
//       full_text:
//         SYM[status ? up_sym : down_sym] +
//           (long ? ' ' + ipa : ''),
//       color: COLORS[status ? :green : :red],
//       markup: 'pango'
//     }
//   }
// end

// # ifwatch_linux :: String -> IO (String | nil)
// def ifwatch_linux_get_carrier(iface)
//   sysfs_dir = File.join '', 'sys', 'class', 'net', iface
//   carrier_f = File.join sysfs_dir, 'carrier'
//   return nil unless File.exist? carrier_f
//   IO.read(carrier_f).strip.to_i
// end

// # ifwatch_linux :: String -> IO (String, Bool)
// def ifwatch_linux(iface)
//   whinge 'ifwatch.linux', "checking interface #{iface}"

//   carrier = ifwatch_linux_get_carrier iface
//   if carrier
//     whinge 'ifwatch.linux', "#{iface} is probably up"
//     # TODO: interact with netlink directly --- libmnl?
//     ipa = `ip addr show #{iface} 2>/dev/null`
//     ipa = ipa.lines.grep(/inet /).map(&:strip)
//     ipa = if ipa.nil? || ipa.empty?
//           then '(!v4)'
//           else ipa.first.split(' ')[1]
//           end
//     whinge 'ifwatch.linux', "#{iface} is on #{ipa}"
//   end
//   [ipa, carrier == 1]
// end

// # ifwatch_freebsd :: String -> IO (String, Bool)
// def ifwatch_freebsd(iface)
//   ## TODO: do this using `sysctl(2)' interface
//   ipl = `ifconfig #{iface} 2>/dev/null`
//   if (status = !(ipl =~ /<UP,.*RUNNING.*>/).nil?)
//     ipa = ipl.lines.grep(/inet /).map(&:strip)
//     ipa = if ipa.nil? || ipa.empty?
//           then '(!v4)'
//           else ipa.first.split(' ')[1]
//           end
//   end
//   [ipa, status]
// end

// ########################################################################
// # Wireless interfaces.

// def wlwatch_block(iface)
//   lambda {
//     result =
//       case RbConfig::CONFIG['host_os']
//       when /linux/ then wlwatch_linux_nl80211 iface
//       else              [false, 'aowijf']
//       end

//     text = SYM[:wifi]

//     if result[:status]
//       text += format(
//         ' %s (%dMb; %d/%d dBm %.03fG)',
//         result[:essid],
//         result[:bitrate],
//         result[:rxpower],
//         result[:txpower],
//         result[:freq] / 1000.0,
//       )
//     end

//     whinge "wlwatch_#{iface}", result.inspect
//     whinge "wlwatch_#{iface}", text.inspect

//     color =
//       if    result[:killed] then :red
//       elsif result[:status] then :green
//       else                       :fg
//       end

//     {
//       name: "wlwatch_#{iface}",
//       full_text: text,
//       color: COLORS[color],
//       markup: 'pango'
//     }
//   }
// end

// # wlwatch_linux_nl80211 :: String -> IO Map
// #
// # Thanks to the wonders of modern technology, I now have to make *two*
// # executable invocations per cycle, because `iwconfig' is too useful.
// # Even better, I have to screen-scrape a tool that says not to!  Yay!
// #
// def wlwatch_linux_nl80211(iface)
//   ## TODO: retrieve this data by chatting to nl80211 ourselves?
//   dev_link = map_of_lines_kv(`iw dev #{iface} link`.lines[1..], sep: ':')
//   dev_info = map_of_lines_kv(`iw dev #{iface} info`.lines[..9])

//   {
//     killed:  wlwatch_linux_get_rfkill,
//     status:  (dev_info['type']       || '') == 'managed',
//     essid:   cleanup_essid(dev_info['ssid']),
//     freq:    (dev_link['freq']       || '').to_f, # MHz
//     bitrate: (dev_link['tx bitrate'] || '').split.first.to_f,
//     txpower: (dev_info['txpower']    || '').split.first.to_f,
//     rxpower: (dev_link['signal']     || '').split.first.to_f,
//   }
// end

// #
// # wlp3s0    IEEE 802.11  ESSID:"xxxxxx"
// #           Mode:Managed  Frequency:5.5 GHz  Access Point: XX:XX:XX:XX:XX:XX
// #           Bit Rate=866.7 Mb/s   Tx-Power=22 dBm
// #           Retry short limit:7   RTS thr:off   Fragment thr:off
// #           Power Management:on
// #           Link Quality=62/70  Signal level=-48 dBm
// #           Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
// #           Tx excessive retries:0  Invalid misc:1736   Missed beacon:0
// #
// # wlwatch_linux_iwconfig :: String -> IO Map
// def wlwatch_linux_iwconfig(iface)
//   # TODO: use `iwconfig'
//   values =
//     `iwconfig #{iface}`
//     .split(/\s{2,}/)
//     .map { |i| i.split(/[:=]/, 2) }
//     .find_all { |x| x.length == 2 }
//     .to_h

//   {
//     killed:  wlwatch_linux_get_rfkill,
//     status:  values['Access Point'] != ' Not-Associated',
//     essid:   cleanup_essid(values['ESSID']),
//     freq:    (values['Frequency']    || '').split.first.to_f * 1000.0, # GHz
//     bitrate: (values['Bit Rate']     || '').split.first.to_f,
//     txpower: (values['Tx-Power']     || '').split.first.to_f,
//     rxpower: (values['Signal level'] || '').split.first.to_f
//   }
// end

// def wlwatch_linux_get_rfkill
//   ## TODO: do this better
//   kills = JSON.parse `rfkill -J`
//   if kills.class == Hash and kills.has_key? ''
//     kills = kills['']
//   elsif kills.class == Hash and kills.has_key? 'rfkilldevices'
//     kills = kills['rfkilldevices']
//   end
//   wlan_kill = kills.find { |k| k['type'] == 'wlan' }
//   (wlan_kill['soft'] == 'blocked' || wlan_kill['hard'] == 'blocked')
// end

// def map_of_lines_kv(lines, sep: ' ')
//   lines
//     .map(&:strip)
//     .map { |x| x.split(sep, 2).map(&:strip) }
//     .filter { |x| x.length == 2 }
//     .to_h
// end

// def cleanup_essid(essid)
//   CGI.escapeHTML(
//     (essid || '')
//       .gsub(/^["']/, '')
//       .gsub(/["']$/, '')
//   )
// end

// ########################################################################
// # Battery functions.
// #
// # We ask UPower, via D-Bus, for this information.

// UPOWER_SVC        = -'org.freedesktop.UPower'
// UPOWER_OBJ_BAT0   = -'/org/freedesktop/UPower/devices/battery_BAT0'
// UPOWER_IFX_DEVICE = -'org.freedesktop.UPower.Device'

// # battery_block :: () -> (() -> IO Map)
// def battery_block
//   lambda {
//     # https://upower.freedesktop.org/docs/Device.html#Device:State
//     SYS_BUS.get_service :upower, UPOWER_SVC
//     SYS_BUS.get_object :upower, :bat0, UPOWER_OBJ_BAT0
//     SYS_BUS.get_interface :upower, :bat0, :device, UPOWER_IFX_DEVICE
//     ifx = SYS_BUS.interface :upower, :bat0, :device
//     acad = [1, 4].include? ifx['State']
//     pc = ifx['Energy'] / ifx['EnergyFull']

//     sym = battery_symbol acad, pc
//     col = battery_colour acad, pc

//     time_h, time_m = \
//       battery_time acad ? ifx['TimeToFull'] : ifx['TimeToEmpty']

//     {
//       name: 'battery',
//       full_text:
//         format(
//           '%s %.01f%% (%d:%02d)',
//           SYM[sym], pc * 100.0, time_h, time_m
//         ),
//       color: COLORS[col],
//       markup: 'pango'
//     }
//   }
// end

// def battery_symbol(acad, percent)
//   if acad then :plug
//   else
//     case percent
//     when 0.8..1.0 then :battery_4
//     when 0.6..0.8 then :battery_3
//     when 0.4..0.6 then :battery_2
//     when 0.2..0.4 then :battery_1
//     else               :battery_0
//     end
//   end
// end

// def battery_colour(acad, percent)
//   if acad then :fg
//   elsif percent <= 0.2 then :red
//   else :green
//   end
// end

// def battery_time(time)
//   [
//     (time / 3600),
//     (time % 3600) / 60
//   ]
// end

// ########################################################################
// # Thermal functions.

// # thermal_block :: Int? -> Float? -> IO Map
// def thermal_block(nzones = 4, high_threshold = 55.0)
//   lambda {
//     temp, rpm =
//       case RbConfig::CONFIG['host_os']
//       when /freebsd/ then thermal_freebsd nzones
//       when /linux/   then thermal_linux nzones
//       else                [NaN, nil]
//       end
//     text  = format('%.01f°C', temp)
//     text += format(' (%d R)', rpm) if rpm

//     {
//       name: 'thermal',
//       full_text: text,
//       color: COLORS[temp >= high_threshold ? :red : :fg],
//       markup: 'pango'
//     }
//   }
// end

// # thermal_freebsd :: Int -> IO (Float, Int)
// def thermal_freebsd(ncpu)
//   mibs = (0...ncpu).map { |i| "dev.cpu.#{i}.temperature" }
//   ## TODO: do this using `sysctl(2)' interface
//   therms =
//     `sysctl -n #{mibs.join(' ')}`
//     .lines
//     .map(&:strip)
//     .map { |i| i.gsub(/C$/, '') }
//     .map(&:to_f)
//   temp = therms.reduce(:+) / therms.length
//   [temp, nil]
// end

// def thermal_linux_tphwmon
//   tphwmon    = Dir.glob '/sys/devices/platform/thinkpad_hwmon/hwmon/hwmon*'
//   return nil if tphwmon.empty?
//   hwmon, _   = tphwmon
//   temp1_path = File.join hwmon, 'temp1_input'
//   temp1      = File.read(temp1_path).strip.to_f / 1000
//   fan1_path  = File.join hwmon, 'fan1_input'
//   fan1       = File.read(fan1_path).strip.to_f
//   [temp1, fan1]
// end

// # thermal_linux :: Int -> IO (Float, Int)
// def thermal_linux(nzones)
//   if xs = thermal_linux_tphwmon
//     whinge 'thermal.linux', "using tphwmon, got #{xs}"
//     return xs
//   end

//   whinge 'thermal.linux', 'yes, hello'
//   therms = (0...nzones).pmap do |i|
//     whinge 'thermal.linux', "thermal zone #{i}"
//     path = "/sys/devices/virtual/thermal/thermal_zone#{i}/temp"
//     File.open(path).each_line.first.chomp.to_f / 1000
//   end
//   temp = therms.reduce(:+) / therms.length
//   whinge 'thermal.linux', "found #{therms.length} zones, giving #{temp}"

//   rpm =
//     if    File.exist? '/proc/acpi/ibm/fan' then fanrpm_ibm
//     elsif File.exist? '/proc/i8k'          then fanrpm_dell
//     else  nil
//     end

//   [temp, rpm]
// end

// def fanrpm_ibm
//   begin
//     fanstat = File.open('/proc/acpi/ibm/fan').each_line
//     rpm = fanstat.grep(/^speed/).first.split("\t")[2].to_i
//     whinge 'fanrpm.ibm', "fan at #{rpm}"
//     rpm
//   rescue
//     whinge 'fanrpm.ibm', 'cannot read fan speed'
//   end
// end

// def fanrpm_dell
//   begin
//     fanstat = File.open('/proc/i8k').each_line
//     rpm = fanstat.first.split[7].to_i
//     whinge 'fanrpm.dell', "fan at #{rpm}"
//     rpm
//   rescue
//     whinge 'fanrpm.dell', 'cannot read fan speed'
//   end
// end

// ########################################################################
// # System core frequency functions.

// # cpufreq_block :: Int? -> Float? -> (() -> IO Map)
// def cpufreq_block(ncpu = 1, threshold = 3.3e+9)
//   lambda {
//     raw_freq =
//       case RbConfig::CONFIG['host_os']
//       when /freebsd/ then cpufreq_freebsd ncpu
//       when /linux/   then cpufreq_linux ncpu
//       else                NaN
//       end

//     freq, units = \
//       if    raw_freq >= 1e+9 then [raw_freq.to_f / 1e+9, 'G']
//       elsif raw_freq >= 1e+6 then [raw_freq.to_f / 1e+6, 'M']
//       elsif raw_freq >= 1e+3 then [raw_freq.to_f / 1e+3, 'k']
//       else                        [raw_freq, 'Hz']
//       end

//     str =
//       format(
//         units == 'G' ? '%s %.1f%s' : '%s %d%s',
//         SYM[:tachometer], freq, units
//       )

//     {
//       name: 'cpufreq',
//       full_text: str,
//       color: COLORS[raw_freq >= threshold ? :red : :fg],
//       markup: 'pango',
//       # force the width to be consistent
//       min_width: "#{SYM[:tachometer]} 888M",
//       align: 'center'
//     }
//   }
// end

// # cpufreq_freebsd :: Int -> IO Float
// def cpufreq_freebsd(ncpu)
//   mibs = (0...ncpu).map { |i| "dev.cpu.#{i}.freq" }
//   ## TODO: do this using `sysctl(2)' interface
//   freqs =
//     `sysctl -n #{mibs.join(' ')}`
//     .lines
//     .map(&:strip)
//     .map(&:to_f)

//   # FreeBSD reports in MHz by default.
//   1e+6 * (freqs.reduce(:+) / freqs.length)
// end

// # cpufreq_linux :: Int -> IO Float
// def cpufreq_linux(ncpu)
//   raw_freqs = (0...ncpu).pmap do |i|
//     path = "/sys/devices/system/cpu/cpu#{i}/cpufreq/scaling_cur_freq"
//     File.open(path).each_line.first.chomp.to_i
//   end

//   # Linux reports in kHz by default.
//   1e+3 * (raw_freqs.reduce(:+) / raw_freqs.length)
// end

// ########################################################################
// # System load average.

// # loadavg_block :: () -> (() -> IO Map)
// def loadavg_block
//   lambda {
//     one, five, fifteen =
//       case RbConfig::CONFIG['host_os']
//       when /freebsd/ then loadavg_freebsd
//       when /linux/   then loadavg_linux
//       else                [NaN, NaN, NaN]
//       end
//     {
//       name: 'loadavg',
//       full_text: format('%.02f:%.02f:%.02f', one, five, fifteen),
//       color: COLORS[one >= 4 ? :red : :green],
//       markup: 'pango'
//     }
//   }
// end

// # loadavg_freebsd :: () -> IO (Float, Float, Float)
// def loadavg_freebsd
//   ## TODO: do this using `sysctl(2)' interface
//   `sysctl -n vm.loadavg`
//     .chomp.split(' ', 5)[1..3].map(&:to_f)
// end

// # loadavg_linux :: () -> IO (Float, Float, Float)
// def loadavg_linux
//   IO.read('/proc/loadavg').strip
//     .split(' ', 5)[0..2].map(&:to_f)
// end

// ########################################################################
// # Block list!

// # Block
// #   { interval: f32
// #   , timeout:  f32
// #   , proc:     blockfn
// #   , mutex:    Mutex
// #   , painted:  bool
// #   , value:    BlockValue }
// # BLOCKS :: Map<Symbol, Block>
// BLOCKS = {
//   date: {
//     interval: 1,
//     proc: date_block
//   },

//   ifwatch_bridge0: {
//     interval: 5,
//     proc: ifwatch_block('bridge0', :sitemap)
//   },

//   ifwatch_bond0: {
//     interval: 5,
//     proc: ifwatch_block('bond0', :sitemap)
//   },

//   ifwatch_bnep0: {
//     interval: 5,
//     proc: ifwatch_block('bnep0', :bluetooth, false)
//   },

//   ifwatch_em0: {
//     interval: 5,
//     proc: ifwatch_block('em0', %i[caret_square_o_up caret_square_o_down], false)
//   },

//   ifwatch_enp0s25: {
//     interval: 5,
//     proc: ifwatch_block('enp0s25', %i[caret_square_o_up caret_square_o_down], false)
//   },

//   ifwatch_eno1: {
//     interval: 5,
//     proc: ifwatch_block('eno1', %i[caret_square_o_up caret_square_o_down], false)
//   },

//   ifwatch_enp2s0f0: {
//     interval: 5,
//     proc: ifwatch_block('enp2s0f0', %i[caret_square_o_up caret_square_o_down], false)
//   },

//   ifwatch_eth0: {
//     interval: 5,
//     proc: ifwatch_block('eth0', %i[caret_square_o_up caret_square_o_down], false)
//   },

//   ifwatch_msk0: {
//     interval: 5,
//     proc: ifwatch_block('msk0', %i[caret_square_o_up caret_square_o_down], false)
//   },

//   ifwatch_tun0: {
//     interval: 5,
//     proc: ifwatch_block('tun0', %i[lock unlock], false)
//   },

//   ifwatch_tun1: {
//     interval: 5,
//     proc: ifwatch_block('tun1', %i[lock unlock], false)
//   },

//   ifwatch_tap0: {
//     interval: 5,
//     proc: ifwatch_block('tap0', %i[lock unlock], false)
//   },

//   wlwatch_wlp3s0: {
//     interval: 5,
//     proc: wlwatch_block('wlp3s0')
//   },

//   wlwatch_wlo1: {
//     interval: 5,
//     proc: wlwatch_block('wlo1')
//   },

//   battery: {
//     interval: 30,
//     proc: battery_block
//   },

//   thermal_jaenelle: {
//     interval: 5,
//     proc: thermal_block(2, 60.0)
//   },

//   thermal_lisbon: {
//     interval: 5,
//     proc: thermal_block(1, 51.0)
//   },

//   thermal_menolly: {
//     interval: 5,
//     proc: thermal_block(1, 65.0)
//   },

//   thermal_wedge: {
//     interval: 5,
//     proc: thermal_block(4, 50.0)
//   },

//   thermal_yarn: {
//     interval: 5,
//     proc: thermal_block(7, 40.0)
//   },

//   loadavg: {
//     interval: 5,
//     proc: loadavg_block
//   },

//   cpufreq_jaenelle: {
//     interval: 2,
//     proc: cpufreq_block(4, 2.6e+9)
//   },

//   cpufreq_lisbon: {
//     interval: 2,
//     proc: cpufreq_block(16, 3.1e+9) ### TODO check
//   },

//   cpufreq_menolly: {
//     interval: 2,
//     proc: cpufreq_block(1, 800e+6)
//   },

//   cpufreq_wedge: {
//     interval: 2,
//     proc: cpufreq_block(12, 4.6e+9)
//   },

//   cpufreq_yarn: {
//     interval: 2,
//     proc: cpufreq_block(8, 1.7e+9)
//   },

//   # toggl: {
//   #   interval: 30,
//   #   proc: toggl_block
//   # },

//   todoist: {
//     interval: 60,
//     timeout: 5.0,
//     proc: todoist_block
//   }
// }.freeze


////////////////////////////////////////////////////////////////////////
// The server loop /////////////////////////////////////////////////////

#[tracing::instrument]
async fn start_server () -> Result<()>
{
	debug!(target: "start_server", "starting server...");
	let server = create_unix_server().await?;

	debug!(target: "start_server", "listening...");
	loop {
		let (client, _) = server.accept().await?;

		tokio::spawn(async move {
			// handle_request(client).await
			// reap
			todo!()
		});
	}
}

// def reap
//   Process.waitpid(-1, Process::WNOHANG) rescue Errno::ECHILD
// end


// # Request handling #####################################################

// def handle_request(client)
//   request = client.gets.strip.to_sym
//   unless ORDER.key? request
//     client.puts '[]'
//     client.close
//     return
//   end

//   handle_request_for(client, request)
// end

// def handle_request_for(client, block_group)
//   if USE_NOTIFY and !NOTIFIER.nil? and NOTIFY_ON == block_group
//     handle_request_as_notifier(client) and return
//   end

//   handle_request_as_statusbar(client, block_group)
// end

// def handle_request_as_notifier(client)
//   return nil if NOTIFIER.empty?

//   whinge 'REQUEST', 'notifications available, flushing one'

//   curr = NOTIFIER.shift
//   whinge 'REQUEST', curr.inspect

//   client.puts [Notifier.blockify(curr)].to_json + ','
//   sleep 3

//   client.close

//   return curr
// end

// def handle_request_as_statusbar(client, block_group)
//   blocks = ORDER[block_group]

//   whinge 'REQUEST', 'getting blocks ' + blocks.join(',') +
//                     ' to satisfy ' + block_group.to_s
//   whinge 'REQUEST', are_blocks_alive(blocks)

//   start_blocks blocks

//   res = nil # save the result
//   benches = {} # save timing data
//   bx = Benchmark.measure do
//     res, benches = get_block_values(blocks)
//     client.puts res.to_json + ','
//     client.close
//   end

//   slow_report bx, block_group, res, benches if bx.real >= 0.9
// end


// # Block manangement ####################################################

// def start_all_blocks!
//   grumble 'main', 'starting blocks... '
//   updater_blocks = BLOCKS.keys
//   updater_blocks.each do |block|
//     start_block block
//   end
//   whinge 'main', 'blocks started'
// end

// def start_blocks(blocks)
//   blocks.each do |block|
//     next unless BLOCKS.key?(block)
//     next if BLOCKS[block].key?(:mutex) and not BLOCKS[block][:mutex].nil?
//     grumble 'main', 'starting block ' + block.to_s
//     start_block block
//   end
// end

// def start_block(block)
//   whinge block.to_s, 'starting'
//   BLOCKS[block][:mutex] = Mutex.new
//   BLOCKS[block][:updater] = Thread.new { block_updater block }
// end

// def block_updater(block)
//   loop do
//     BLOCKS[block][:mutex].synchronize do
//       whinge block, 'updating'
//       timeout = TIMEOUT
//       timeout = BLOCKS[block][:timeout] if BLOCKS[block].key?(:timeout)

//       status = Timeout.timeout(timeout) do
//         BLOCKS[block][:val] = BLOCKS[block][:proc].call
//       end rescue Timeout::Error

//       if status == Timeout::Error
//         whinge 'TIMEOUT', "#{block} timed out after #{timeout}!"
//         BLOCKS[block][:val] = { full_text: '[timed out]' }
//       end
//       BLOCKS[block][:painted] = false
//     end
//     whinge block, "sleeping for #{BLOCKS[block][:interval]}"
//     sleep BLOCKS[block][:interval]
//   end
// end

// def are_blocks_alive(blocks)
//   blocks.map do |block|
//     '{' + block.to_s + ': ' +
//       case
//       when ! BLOCKS.key?(block)          then 'NOBLOCK'
//       when ! BLOCKS[block].key?(:mutex)  then 'NOMUTEX'
//       when BLOCKS[block][:mutex].nil?    then 'NOMUTEX'
//       when BLOCKS[block][:mutex].locked? then 'LOCKED'
//       else                                    'OK'
//       end + '}'
//   end.join(', ')
// end

// def get_block_values(blocks)
//   benches = {}
//   res = blocks.pmap do |block|
//     next unless BLOCKS.key? block
//     next if BLOCKS[block].key?(:mutex) and BLOCKS[block][:mutex].nil?
//     rx = nil
//     benches[block] = Benchmark.measure do
//       rx = BLOCKS[block][:mutex].synchronize do
//         BLOCKS[block][:painted] = true
//         BLOCKS[block][:val]
//       end
//     end
//     rx
//   end.compact

//   [res, benches]
// end


////////////////////////////////////////////////////////////////////////
// Socket management ///////////////////////////////////////////////////

#[tracing::instrument]
async fn create_tcp_server () -> Result<TcpListener>
{
	Ok(TcpListener::bind("127.0.0.1:2018").await?)
}

#[tracing::instrument]
async fn create_unix_server () -> Result<UnixListener>
{
	let socket = get_unix_socket_path()?;
	debug!(target: "create_unix_server", "socket: {:?}", &socket);

	if socket.exists() {
		debug!(target: "create_unix_server", "hm, socket exist, let's get rid of it.");
		fs::remove_file(&socket).await?
	}

	Ok(UnixListener::bind(socket)?)
}


////////////////////////////////////////////////////////////////////////
// PID-file management /////////////////////////////////////////////////

#[tracing::instrument]
async fn create_pidfile () -> Result<()>
{
	let pidfile = get_pid_file_path()?;
	if pidfile_live_p(&pidfile).await? {
		bail!("{:?} exists; cowardly refusing to continue", &pidfile)
	}

	fs::write(
		&pidfile,
		format!("{}\n", std::process::id())
	).await?;
	Ok(())
}

#[tracing::instrument]
async fn pidfile_live_p<P> (pidfile: P) -> Result<bool>
where P: AsRef<Path> + Debug
{
	use nix::{
		libc::pid_t,
		unistd::Pid,
		errno::Errno::{EPERM, ESRCH},
		sys::signal::{kill, Signal::SIGCONT}
	};
	let pidfile = pidfile.as_ref();
	if !pidfile.exists() { return Ok(false); }

	debug!(target: "pidfile_live_p", "pidfile exists");
	let pid_str = read_one_line_file(pidfile).await?;
	let pid     = usize::from_str_radix(&pid_str, 10)?;
	let pid     = Pid::from_raw(pid as pid_t);
	debug!(target: "pidfile_live_p",
		   "our pidfile suggests we are/were process {pid}");

	match kill(pid, Some(SIGCONT)) {
		Err(EPERM) => {
			debug!(target: "pidfile_live_p", "process {pid} exists but not owned by us");
			return Ok(false);
		}
		Err(ESRCH) => {
			debug!(target: "pidfile_live_p", "process {pid} doesn't exist");
			return Ok(false);
		}
		_ => {}
	}

	debug!(target: "pidfile_live_p", "process {pid} seems to exist");
	Ok(true)
}


// # Performance reporting ################################################

// $slow = Mutex.new
// def slow_report(b, req, res, bx)
//   $slow.synchronize do
//     key = "req.#{req}"
//     grumble key, format('took %.02fs to answer!', b.real)
//     grumble key, crunch(res).join(' | ')

//     width = bx.keys.map(&:length).max + 4
//     grumble key, ''.ljust(width) + Benchmark::CAPTION

//     bx.keys.sort { |x, y| bx[x].real <=> bx[y].real }
//       .each do |block|
//         grumble key, "#{block}: ".ljust(width) +
//                      bx[block].format(Benchmark::FORMAT)
//     end
//   end
// end

// def crunch(blocks)
//   blocks.map do |block|
//     block[:full_text].gsub(/<[^>]+>/, '')
//   end
// end

////////////////////////////////////////////////////////////////////////

#[tracing::instrument]
#[tokio::main]
async fn main () -> Result<()>
{
	color_eyre::install()?;
	env_logger::init();

	create_pidfile().await?;

	// Signal.trap('HUP') { Pry::ColorPrinter.pp BLOCKS }
	start_server().await
}


////////////////////////////////////////////////////////////////////////

/// When a file contains only one line, read that and return it.
#[tracing::instrument]
async fn read_one_line_file<P> (path: P) -> Result<String>
where P: AsRef<Path> + Debug
{
	use tokio::fs::read_to_string;

	let     path     = path.as_ref();
	let     contents = read_to_string(&path).await
		.wrap_err_with(|| format!("failed to read {:?}", &path))?;
	let mut x_lines  = contents.lines();
	let     x_first  = x_lines.next()
		.ok_or_else(|| eyre!("no lines found in {:?}", &path))?;
	Ok(x_first.to_string())
}

////////////////////////////////////////////////////////////////////////
