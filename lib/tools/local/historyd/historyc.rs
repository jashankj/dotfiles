extern crate chrono;
extern crate structopt;
extern crate tokio;


use std::convert::Infallible;
use std::ffi::CStr;
use std::fmt::{self, Display};
use std::net::SocketAddr;
use std::num::ParseIntError;
use std::os::unix;
use std::str::FromStr;

use historyd::*;

use chrono::prelude::*;
use color_eyre::eyre::Result as EResult;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;
// use tokio::prelude::*;
use structopt::StructOpt;
use whoami;

////////////////////////////////////////////////////////////////////////

#[paw::main]
#[tokio::main]
async fn main (args: Historyc) -> EResult<()>
{
	color_eyre::install()?;

	let req = match &args {
		Historyc::SessionStart { shell } =>
			Request::SessionStart {
				start_time: Local::now(),
				hostname: whoami::hostname(),
				username: whoami::username(),
				shell: shell.clone(),
				shell_pid: unix::process::parent_id(),
			},

		Historyc::SessionEnd { session } =>
			Request::SessionEnd {
				session_id: *session,
				end_time: Local::now(),
			},

		Historyc::History {
			session, shell_sequence, shell_level,
			use_tty, tty, use_euid, euid,
			time_started, time_finished,
			return_value, pipe_status,
			use_cwd, working_dir: cwd,
			command
		} => {
			let tty  = Perhaps::new(*use_tty,  tty.clone());
			let euid = Perhaps::new(*use_euid, euid.clone());
			let cwd  = Perhaps::new(*use_cwd,  cwd.clone());
			Request::History {
				session_id:     *session,
				shell_level:    *shell_level,
				shell_sequence: *shell_sequence,
				tty:            tty.get().map(|x| x.0.clone()),
				euid:           euid.get().map(|x| x.0),
				time_started:   *time_started,
				time_finished:  *time_finished,
				return_value:   *return_value,
				pipe_status:    pipe_status.clone(),
				working_dir:    cwd.get().map(|x| x.0.clone()),
				command:        command.clone()
			}
		}
	};

	let addr = REMOTE_ADDR.parse::<SocketAddr>()?;
	let mut sock = TcpStream::connect(&addr).await?;

	let buf_to = serde_cbor::to_vec(&req).unwrap();
	sock.write_all(&buf_to).await?;

	sock.shutdown().await?;

	let mut buf_from = Vec::new();
	sock.read_to_end(&mut buf_from).await?;
	let from: Response = serde_cbor::from_slice(&buf_from).unwrap();

	println!(
		"{:?}",
		match from {
			Response::Session { session_id } => session_id,
			Response::History { history_id } => history_id
		}
	);

	Ok(())
}

////////////////////////////////////////////////////////////////////////

#[derive(Debug, StructOpt)]
#[structopt(name = "historyc")]
enum Historyc {
	/// Start an interactive session.
	#[structopt(name = "session-start")]
	SessionStart {
		/// the 'shell' being used run
		/// (which may be a shell,
		/// but could also be Emacs, Wily, etc.)
		shell: String,
	},

	/// End a session.
	#[structopt(name = "session-end")]
	SessionEnd {
		/// the session to end
		#[structopt(long = "session")]
		session: usize,
	},

	/// Log a command.
	#[structopt(name = "history")]
	History {
		/// the session to log this command to
		#[structopt(long = "session", env = "HISTORYC_SESSION_ID")]
		session: usize,
		/// the shell sequence number
		#[structopt(long = "shell-seq")]
		shell_sequence: Option<usize>,
		/// the level of the shell
		#[structopt(long = "shell-level")]
		shell_level: Option<usize>,
		/// use current (pseudo-)terminal.
		#[structopt(long = "use-tty")]
		use_tty: bool,
		/// the (pseudo-)terminal name
		#[structopt(long = "tty", required_unless("use-tty"))]
		tty: Option<TTY>,
		/// use current EUID
		#[structopt(long = "use-euid")]
		use_euid: bool,
		/// the (pseudo-)terminal name
		#[structopt(long = "euid", required_unless("use-euid"))]
		euid: Option<EUID>,
		/// the time this command started
		#[structopt(long = "start-time")]
		time_started: DateTime<Local>,
		/// the time this command finished
		#[structopt(long = "end-time")]
		time_finished: DateTime<Local>,
		/// the whole return value
		#[structopt(long = "return-value")]
		return_value: u32,
		/// the whole return value
		#[structopt(long = "pipe-status")]
		pipe_status: Vec<u32>,
		/// use current working directory
		#[structopt(long = "use-cwd")]
		use_cwd: bool,
		/// the working directory this command was started in
		#[structopt(long = "directory", required_unless("use-cwd"))]
		working_dir: Option<WorkingDir>,
		/// the command that was run
		command: String,
	}
}

////////////////////////////////////////////////////////////////////////

#[derive(Clone, Debug)]
enum Perhaps<T> {
	None,
	Guess,
	Given(T)
}

impl<T: Default> Perhaps<T> {
	fn new (guess: bool, val: Option<T>) -> Self {
		match (guess, val) {
			(false, Some(x)) => Perhaps::Given(x),
			(true,  None)    => Perhaps::Guess,
			(false, None)    => Perhaps::None,
			_                => unreachable!()
		}
	}

	fn get (self) -> Option<T>
	{
		match self {
			Perhaps::None     => None,
			Perhaps::Guess    => Some(Default::default()),
			Perhaps::Given(x) => Some(x)
		}
	}
}

////////////////////////////////////////////////////////////////////////

macro_rules! wrapper_ty {
	{ $name:ident , $inner:ident , $err:ident , $default:block } => {
		#[derive(Clone, Debug)]
		struct $name ($inner);

		impl ::std::str::FromStr for $name {
			type Err = $err;
			fn from_str (src: &str)
				-> ::std::result::Result<Self, Self::Err>
			{
				Ok($name($inner::from_str(src)?))
			}
		}

		impl ::std::default::Default for $name {
			fn default () -> Self
				$default
		}

		impl ::std::fmt::Display for $name {
			fn fmt (&self, f: &mut ::std::fmt::Formatter<'_>)
				-> ::std::fmt::Result
			{
				write!(f, "{}", self.0)
			}
		}
	}
}

wrapper_ty! { TTY, String, Infallible,
	{
		// pub unsafe extern "C" fn ttyname(fd: c_int) -> *mut c_char
		TTY (
			unsafe {
				CStr::from_ptr(libc::ttyname(0 as libc::c_int))
			}.to_string_lossy().into_owned()
		)
	}
}

wrapper_ty! { EUID, u32, ParseIntError,
	{
		// pub unsafe extern "C" fn geteuid() -> uid_t (=u32)
		EUID(unsafe { libc::geteuid() })
	}
}

wrapper_ty! { WorkingDir, String, Infallible,
	{
		WorkingDir(
			std::env::current_dir().unwrap()
				.to_string_lossy().into_owned()
		)
	}
}
