extern crate chrono;
extern crate rusqlite;
extern crate tokio;

use std::convert::TryInto;
use std::env;
// use std::fmt::{self, Display, Formatter};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};

use historyd::*;

use rusqlite::{params, Connection};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};
// use tokio::prelude::*;
// use tokio::prelude::future::ok;
// use chrono::prelude::*;
use color_eyre::eyre::{Result, WrapErr};

const SCHEMA: &'static str = r##"
CREATE TABLE sessions (
	id          INTEGER PRIMARY KEY,
	start_time  TEXT,
	end_time    TEXT,
	hostname    TEXT,
	username    TEXT,
	shell       TEXT,
	shell_pid   INTEGER
);

CREATE TABLE history_entries (
	session_id     INTEGER REFERENCES session (id),
	sequence       INTEGER PRIMARY KEY,
	shell_sequence INTEGER,
	shell_level    INTEGER,
	tty            TEXT,
	euid           TEXT,
	time_started   TEXT,
	time_finished  TEXT,
	return_value   INTEGER,
	pipe_status    TEXT,
	working_dir    TEXT,
	command        TEXT,
	CONSTRAINT he_sess_seq UNIQUE (session_id, sequence)
);
"##;

const DO_SESSION_START: &'static str = r##"
INSERT INTO sessions (start_time, hostname, username, shell, shell_pid)
	VALUES (?, ?, ?, ?, ?);
"##;

const DO_SESSION_END: &'static str = r##"
UPDATE sessions
	SET end_time = ?
	WHERE id = ?;
"##;

const DO_HISTORY: &'static str = r##"
INSERT INTO history_entries (
	session_id, shell_sequence, shell_level, tty, euid,
	time_started, time_finished,
	return_value, pipe_status, working_dir,
	command
)
	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
"##;

#[tokio::main]
async fn main() -> Result<()>
{
    color_eyre::install()?;
	let dbconn = Arc::new(Mutex::new(db_connect()?));

	let address = REMOTE_ADDR.parse::<SocketAddr>()?;
	let service = TcpListener::bind(&address).await
		.wrap_err_with(|| format!("While trying to bind {:?}:", &address))?;

	loop {
		let (socket, _) = service.accept().await
			.wrap_err("While accepting a connection:")?;

		let dbconn = dbconn.clone();
		tokio::spawn(async move {
			handle_connection(socket, dbconn).await
		});
	}
}

async fn handle_connection (mut sock: TcpStream, conn: Arc<Mutex<Connection>>)
	-> Result<()>
{
	let mut buf_from = Vec::new();
	sock.read_to_end(&mut buf_from).await
		.wrap_err("While receiving from client:")?;

	let req = serde_cbor::from_slice(&buf_from)
		.wrap_err("While parsing client request:")?;

	let res = handle_request(req, conn)
		.wrap_err("While computing response to client:")?;

	let buf_to = serde_cbor::to_vec(&res)
		.wrap_err("While preparing client response:")?;

	sock.write_all(&buf_to).await
		.wrap_err("While sending to client:")?;

	Ok(())
}

fn handle_request (req: Request, conn: Arc<Mutex<Connection>>)
	-> Result<Response>
{
	let conn = conn.clone();
	let conn = conn.lock().unwrap();

	use historyd::Request::*;

	match req {
		SessionStart { start_time, hostname, username, shell, shell_pid } => {
			let mut query = conn.prepare_cached(DO_SESSION_START)
				.wrap_err("While preparing 'session-start' query:")?;

			let rowid = query.insert(params![
				start_time.to_rfc3339(),
				hostname, username,
				shell, shell_pid.to_string()
			]).wrap_err("While running 'session-start' query:")?;

			Ok(Response::Session {
				session_id: rowid.try_into().unwrap()
			})
		}

		SessionEnd { session_id, end_time } => {
			let mut query = conn.prepare_cached(DO_SESSION_END)
				.wrap_err("While preparing 'session-end' query:")?;

			query.execute(params![
				end_time.to_rfc3339(),
				session_id.to_string()
			]).wrap_err("While running 'session-end' query:")?;

			Ok(Response::Session { session_id })
		}

		History {
			session_id, shell_sequence, shell_level,
			tty, euid, time_started, time_finished,
			return_value, pipe_status, working_dir,
			command
		} => {
			let mut query = conn.prepare_cached(DO_HISTORY)
				.wrap_err("While preparing 'history' query:")?;

			let pipe_status: Option<String> = pipe_status.iter()
				.map(ToString::to_string)
				.reduce(|acc, x| {
					format!("{},{}", acc, x)
				});

			let rowid = query.insert(params![
				session_id,
				shell_sequence,
				shell_level,
				tty,
				euid,
				time_started.timestamp_millis(),
				time_finished.timestamp_millis(),
				return_value,
				pipe_status,
				working_dir,
				command
			]).wrap_err("While running 'history' query:")?;

			Ok(Response::History {
				history_id: rowid.try_into().unwrap()
			})
		}
	}
}

fn db_connect() -> Result<Connection>
{
	use rusqlite::OpenFlags;
	use rusqlite::Error::QueryReturnedNoRows;

	let conn = Connection::open_with_flags(
		get_database_path(),
		OpenFlags::SQLITE_OPEN_READ_WRITE |
		OpenFlags::SQLITE_OPEN_CREATE
	).wrap_err("While connecting to database:")?;

	match conn.query_row(
		"SELECT COUNT(id) FROM sessions GROUP BY id",
		[],
		|_| Ok(())
	) {
		Ok(_) | Err(QueryReturnedNoRows) => (),

		// if there's no database, give it some database.
		Err(_) => {
			println!("historyd: creating database {:?}", get_database_path());
			conn.execute_batch(SCHEMA)
				.wrap_err("While creating database schema:")
				.unwrap();
		}
	};

	conn.prepare_cached(DO_SESSION_START)
		.wrap_err("While preparing 'session-start' query:")?;
	conn.prepare_cached(DO_SESSION_END)
		.wrap_err("While preparing 'session-end' query:")?;
	conn.prepare_cached(DO_HISTORY)
		.wrap_err("While preparing 'history' query:")?;

	Ok(conn)
}

fn get_database_path() -> PathBuf
{
	let home = env::var_os("HOME").unwrap();
	let mut buf = PathBuf::from(home);
	buf.push(".historyd.db");
	buf
}
