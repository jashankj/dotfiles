# -*- makefile-bsdmake -*-

#-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2022 Jashank Jeremy <jashank@rulingia.com.au>
#

CC	= clang
CFLAGS	+= \
	-O2 -g \
	-fno-common \
	-fPIE \
	-std=gnu11

PKGCONFIGS	+= libbsd-overlay
# bsd/{ee,grdc}:
PKGCONFIGS	+= ncursesw tinfo
# bsd/factor:
PKGCONFIGS	+= libcrypto

__pkgconf	!= command -v pkgconf || true
__pkg_config	!= command -v pkg-config || true
.if ${__pkgconf} != ""
PKGCONFIG	?= ${__pkgconf}
.elif ${__pkg_config} != ""
PKGCONFIG	?= ${__pkg_config}
.else
.warn "Neither 'pkgconf' nor 'pkg-config' was found."
.endif

.for pkg in ${PKGCONFIGS}
CFLAGS.${pkg:C/^lib//}	!= ${PKGCONFIG} --cflags ${pkg}
LDFLAGS.${pkg:C/^lib//}	!= ${PKGCONFIG} --libs   ${pkg}
.endfor

.if ${OS} == "Linux"
CFLAGS	+= -include ${.CURDIR}/../../bsd/portable/shim-param.h
CFLAGS	+= -include ${.CURDIR}/../../bsd/portable/shim-cdefs.h
CFLAGS	+= ${CFLAGS.bsd-overlay}
LDFLAGS	+= ${LDFLAGS.bsd-overlay}
.endif

.for lib in ${LIBADD}
CFLAGS	+= ${CFLAGS.${lib}}
LDFLAGS	+= ${LDFLAGS.${lib}}
.endfor

USER		?= jashank
GROUP		?= jeremy

ROOT_USER	?= root
ROOT_GROUP	?= wheel

PREFIX		?= ${HOME}
BINOWN		?= jashank
BINGRP		?= jeremy
BINDIR		?= ${PREFIX}/bin/${OSARCH}
LIBDIR		?= ${PREFIX}/lib/${OSARCH}
SHLIBDIR	?= ${LIBDIR}
INCLUDEDIR	?= ${PREFIX}/include
INCDIR		?= ${INCLUDEDIR}
MANDIR		?= ${PREFIX}/share/man
MANTARGET	?= man
DOCDIR		?= ${PREFIX}/share/doc
NLSDIR		?= ${PREFIX}/share/nls
