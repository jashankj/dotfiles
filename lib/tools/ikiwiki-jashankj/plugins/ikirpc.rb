#!/usr/bin/env ruby
# ikirpc.rb --- XML-RPC shim for IkiWiki plugins in Ruby

require 'rubygems'
require 'xmlrpc/parser'
require 'xmlrpc/client'
require 'xmlrpc/server'

require 'sigdump/setup'
DEBUG_IKIRPC = false

########################################################################

module XMLRPC
  class IOServer < XMLRPC::BasicServer
    include XMLRPC::ParserWriterChooseMixin

    def initialize io_in, io_out, *a
      @in  = io_in
      @out = io_out
      super(*a)
    end

    def rpc method, *args
      request = create.methodCall method, *args
      send request
      response = recv
      parser.parseMethodResponse response
    end

    def serve
      catch :exit_serve do
        loop do
          request = recv
          next unless request
          response = process request
          send response
        end
      end
    end

    private def recv
      data = recv_in_line
      return nil if data[0...15] != "Content-length:"
      length = data[16...(data.index("\n"))].to_i
      recv_in_line # drop blank line between
      data = recv_in length
      recv_in_line # drop blank following
      data
    end

    private def recv_in_line
      begin
        report_input @in.readline
      rescue EOFError
        throw :exit_serve
      end
    end

    private def recv_in n
      report_input @in.read n
    end

    private def send data
      send_out format("Content-length: %d\n\n%s\n", data.bytesize, data)
      @out.flush
    end

    private def send_out data
      data.lines.each do |x|
        @out.print report_output x
      end
    end

    private def report_input data
      $stderr.puts "<<< #{data}" if DEBUG_IKIRPC
      data
    end

    private def report_output data
      $stderr.puts ">>> #{data}" if DEBUG_IKIRPC
      data
    end
  end

  class STDIOServer < IOServer
    def initialize *a
      super $stdin, $stdout, *a
    end
  end


  class Proxy
    def initialize service
      @service = service
    end

    def method_missing name, *args
      @service.rpc(name.to_s, *args)
    end
  end
end

########################################################################

module IkiWiki
  class PluginProxy
    attr_reader :service

    def ikiwiki_id
      self.class.name
        .delete_prefix('IkiWiki::Plugin::')
        .downcase
    end

    def initialize
      @service = XMLRPC::STDIOServer.new
      @proxy = XMLRPC::Proxy.new(@service)
      @id = ikiwiki_id
      @hooks ||= []
      @funcs ||= []
      @imported ||= false

      @service.add_handler('import') do |*|
        begin
          @hooks.each {|hook| @proxy.hook(*(hook.to_a.flatten)) }
          @funcs.each {|hook| @proxy.inject(*(hook.to_a.flatten)) }
          @imported = true
        rescue Exception => e
          $stderr.puts e
        end
      end
    end

    def serve; @service.serve; end

    def hook(type, name: nil, id: @id, last: false)
      @imported ||= false
      raise "already imported" if @imported
      name ||= type.to_sym
      @service.add_handler name.to_s do |*args|
        __send__ name.to_sym, **arglist_unfurl(*args)
      end
      @hooks ||= []
      @hooks += [{ id: id, type: type, call: name, last: last }]
    end

    def inject(rname, name: nil, memoize: true)
      @imported ||= false
      raise "already imported" if @imported
      name ||= rname.to_sym
      @service.add_handler name.to_s do |*args|
        $stderr.puts("inject:#{rname} / #{name} -- #{args.inspect}")
        __send__ name.to_sym, **arglist_unfurl(*args)
      end
      @funcs ||= []
      @funcs += [{ name: rname, call: name, memoize: memoize }]
    end

    class Var0Proxy
      def initialize(proxy); @proxy = proxy; @var1 = {}; end
      def [] hash; @var1[hash] ||= Var1Proxy.new(@proxy, hash); end
    end

    class Var1Proxy
      def initialize(proxy, hash); @proxy = proxy; @hash = hash; end
      def []  key;        @proxy.getvar @hash, key;         end
      def []= key, value; @proxy.setvar @hash, key, value;  end
    end

    class State0Proxy
      def initialize(proxy); @proxy = proxy; @state1 = {}; end
      def [] page; @state1[page] ||= State1Proxy.new(@proxy, page); end
    end

    class State1Proxy
      def initialize(proxy, page); @proxy = proxy; @page = page; @state2 = {}; end
      def [] id; @state2[id] ||= State2Proxy.new(@proxy, @page, id); end
    end

    class State2Proxy
      def initialize(proxy, page, id); @proxy = proxy; @page = page; @id = id; end
      def []  key;        @proxy.getstate @page, @id, key;         end
      def []= key, value; @proxy.setstate @page, @id, key, value;  end
    end


    def argv;     @proxy.getargv;   end
    def argv= x;  @proxy.setargv x; end
    def var;   @var   ||= Var0Proxy.new @proxy; end
    def state; @state ||= State0Proxy.new @proxy; end
    def pagespec_match? spec
      @proxy.pagespec_match spec
    end


    private def arglist_unfurl *args
      args
        .each_slice(2)
        .map { |k,v| [k.to_sym, v] }
        .to_h
    end
  end
end


########################################################################
