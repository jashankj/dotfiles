#!/usr/bin/perl
# Markdown markup language
package IkiWiki::Plugin::markdown;

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
	hook(type => "checkconfig", id => "markdown", call => \&checkconfig);
	hook(type => "getsetup", id => "markdown", call => \&getsetup);
	hook(type => "htmlize", id => "markdown", call => \&htmlize, longname => "Markdown");
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => 1, # format plugin
			section => "format",
		},
}

sub checkconfig () {
#	$config{mdwn_footnotes} = 1 unless defined $config{mdwn_footnotes};
#	$config{mdwn_alpha_lists} = 0 unless defined $config{mdwn_alpha_lists};
}

use IO::Handle;
use IPC::Open2;

my $markdown_sub;
sub htmlize (@) {
	my %params = @_;
	my $content = $params{content};

	if (! defined $markdown_sub) {
		$markdown_sub = sub {
			my ($kd_out, $kd_in);
			my $pid = open2 $kd_out, $kd_in, "kramdown";
			$kd_in->write(shift);
			$kd_in->close;
			my $res = (join '', $kd_out->getlines);
			$kd_out->close;
			waitpid $pid, 0;
			return $res;
		}
	}

	require Encode;
	# Workaround for perl bug (#376329)
	$content = Encode::encode_utf8 $content;
	eval { $content = &$markdown_sub($content) };
	print STDERR $@ if $@;
	$content = Encode::decode_utf8 $content;

	return $content;
}

1
