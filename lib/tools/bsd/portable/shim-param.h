#define MAXBSIZE        65536           /* must be power of 2 */
#define SPECNAMELEN     255             /* max length of devicename */

#define nitems(x)       (sizeof((x)) / sizeof((x)[0]))
