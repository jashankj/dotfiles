diff --git c/src/Xmobar/Plugins/Monitors/Batt.hs i/src/Xmobar/Plugins/Monitors/Batt.hs
index 52600d8..65eccff 100644
--- c/src/Xmobar/Plugins/Monitors/Batt.hs
+++ i/src/Xmobar/Plugins/Monitors/Batt.hs
@@ -23,13 +23,16 @@ import Xmobar.Plugins.Monitors.Batt.Common (BattOpts(..)
 import Xmobar.Plugins.Monitors.Common
 import System.Console.GetOpt
 
-#if defined(freebsd_HOST_OS)
-import qualified Xmobar.Plugins.Monitors.Batt.FreeBSD as MB
+#ifdef UPOWER
+import qualified Xmobar.Plugins.Monitors.Batt.UPower as MB
 #else
+# if defined(freebsd_HOST_OS)
+import qualified Xmobar.Plugins.Monitors.Batt.FreeBSD as MB
+# else
 import qualified Xmobar.Plugins.Monitors.Batt.Linux as MB
+# endif
 #endif
 
-
 defaultOpts :: BattOpts
 defaultOpts = BattOpts
   { onString = "On"
diff --git c/src/Xmobar/Plugins/Monitors/Batt/UPower.hs i/src/Xmobar/Plugins/Monitors/Batt/UPower.hs
new file mode 100644
index 0000000..ac5412f
--- /dev/null
+++ i/src/Xmobar/Plugins/Monitors/Batt/UPower.hs
@@ -0,0 +1,108 @@
+{-# LANGUAGE LambdaCase #-}
+{-# LANGUAGE OverloadedStrings #-}
+
+module Xmobar.Plugins.Monitors.Batt.UPower (readBatteries)
+where
+
+
+import Xmobar.Plugins.Monitors.Batt.Common ( BattOpts(..)
+                                           , Result(..)
+                                           , Status(..))
+
+import           Data.Int
+import qualified Data.Map as Map
+import           Data.Word
+import           GHC.Float
+
+import           DBus
+import qualified DBus.Client as DC
+
+upower_svc        = busName_       "org.freedesktop.UPower"
+upower_obj_bat0   = objectPath_    "/org/freedesktop/UPower/devices/battery_BAT0"
+upower_ifx_device = interfaceName_ "org.freedesktop.UPower.Device"
+
+readBatteries :: BattOpts -> [String] -> IO Result
+readBatteries opts _ = readBatteries'
+
+readBatteries' :: IO Result
+readBatteries' = do
+  bus <- DC.connectSystem
+  reply' <- DC.getAllPropertiesMap bus $
+           (methodCall upower_obj_bat0 upower_ifx_device (memberName_ $ "Get"))
+           { methodCallDestination = Just upower_svc }
+  case reply' of
+    Left  _     -> return $ NA
+    Right reply -> do
+      let percent = unwrapO $ v2f32 =<< Map.lookup "Percentage"  reply
+      let power   = unwrapO $ v2f32 =<< Map.lookup "EnergyRate"  reply
+      let time =
+            let tFull  = unwrapO $ v2i64 =<< Map.lookup "TimeToFull" reply
+                tEmpty = unwrapO $ v2i64 =<< Map.lookup "TimeToEmpty" reply
+            in max tFull tEmpty
+      let status  = upower2Status $ unwrapO $ v2u32 =<< Map.lookup "State" reply
+      return $ Result percent power (i2n time) status
+  where
+    v2f32 :: Variant -> Maybe Float
+    v2f32 v = fromVariant v >>= pure . double2Float
+    v2i64 :: Variant -> Maybe Int64
+    v2i64 v = fromVariant v
+    v2u32 :: Variant -> Maybe Word32
+    v2u32 v = fromVariant v
+    i2n = fromIntegral . toInteger -- ew
+
+    unwrapO (Just x) = x
+    unwrapO        _ = error "unwrapO of Nothing"
+
+    upower2Status = \case
+      0 -> Unknown
+      1 -> Charging
+      2 -> Discharging
+      3 -> Discharging -- Empty
+      4 -> Full
+      5 -> Idle -- PendingCharge
+      6 -> Idle -- PendingDischarge
+      _ -> undefined
+
+
+{--
+ -- From jashankj/dotfiles/bin/statusbard.rb::
+
+
+  # Battery functions.
+  #
+  # We ask UPower, via D-Bus, for this information.
+
+  UPOWER_SVC        = -'org.freedesktop.UPower'
+  UPOWER_OBJ_BAT0   = -'/org/freedesktop/UPower/devices/battery_BAT0'
+  UPOWER_IFX_DEVICE = -'org.freedesktop.UPower.Device'
+
+  # battery_block :: () -> (() -> IO Map)
+  def battery_block
+    lambda {
+      # https://upower.freedesktop.org/docs/Device.html#Device:State
+      SYS_BUS.get_service :upower, UPOWER_SVC
+      SYS_BUS.get_object :upower, :bat0, UPOWER_OBJ_BAT0
+      SYS_BUS.get_interface :upower, :bat0, :device, UPOWER_IFX_DEVICE
+      ifx = SYS_BUS.interface :upower, :bat0, :device
+      acad = [1, 4].include? ifx['State']
+      pc = ifx['Energy'] / ifx['EnergyFull']
+
+      sym = battery_symbol acad, pc
+      col = battery_colour acad, pc
+
+      time_h, time_m = \
+        battery_time acad ? ifx['TimeToFull'] : ifx['TimeToEmpty']
+
+      {
+        name: 'battery',
+        full_text:
+          format(
+            '%s %.01f%% (%d:%02d)',
+            SYM[sym], pc * 100.0, time_h, time_m
+          ),
+        color: COLORS[col],
+        markup: 'pango'
+      }
+    }
+  end
+--}
diff --git c/xmobar.cabal i/xmobar.cabal
index 51cbd14..4cfdef8 100644
--- c/xmobar.cabal
+++ i/xmobar.cabal
@@ -68,6 +68,10 @@ flag with_mpris
   description: MPRIS v1, v2 support.
   default: False
 
+flag with_upower
+  description: Use UPower for battery statistics.
+  default: False
+
 flag with_dbus
   description: Publish a service on the session bus for controlling xmobar.
   default: False
@@ -278,6 +282,11 @@ library
        other-modules: Xmobar.System.DBus
        cpp-options: -DDBUS
 
+    if flag(with_upower) || flag(all_extensions)
+       build-depends: dbus >= 1
+       other-modules: Xmobar.Plugins.Monitors.Batt.UPower
+       cpp-options: -DUPOWER
+
     if flag(with_xpm) || flag(all_extensions)
        extra-libraries: Xpm
        other-modules: Xmobar.X11.XPMFile
