// helpers for SPIM-targeted C

#ifndef __SPIM__H_
#define __SPIM__H_

#define PRINT_INT_SYSCALL       1
#define PRINT_FLOAT_SYSCALL     2
#define PRINT_DOUBLE_SYSCALL    3
#define PRINT_STRING_SYSCALL    4
#define READ_INT_SYSCALL        5
#define READ_FLOAT_SYSCALL      6
#define READ_DOUBLE_SYSCALL     7
#define READ_STRING_SYSCALL     8
#define SBRK_SYSCALL            9
#define EXIT_SYSCALL            10
#define PRINT_CHARACTER_SYSCALL 11
#define READ_CHARACTER_SYSCALL  12
#define OPEN_SYSCALL            13
#define READ_SYSCALL            14
#define WRITE_SYSCALL           15
#define CLOSE_SYSCALL           16
#define EXIT2_SYSCALL           17

#define str_(x) #x
#define str(x) str_(x)

static inline void __spim_print_int (int n)
{
	asm volatile (
		"li   $v0, " str(PRINT_INT_SYSCALL) "\n\t"
		"move $a0, %0\n\t"
		"syscall\n\t"
		: // no returns
		: "r" (n)
		: "a0", "v0");
}

static inline void __spim_print_string (const char *s)
{
	asm volatile (
		"li   $v0, " str(PRINT_STRING_SYSCALL) "\n\t"
		"move $a0, %0\n\t"
		"syscall\n\t"
		: // no returns
		: "r" (s)
		: "a0", "v0");
}

static inline int __spim_read_int (void)
{
	int r;
	asm volatile (
		"li   $v0, " str(READ_INT_SYSCALL) "\n\t"
		"syscall\n\t"
		"move %0, $v0\n\t"
		: "=r" (r)
		: // no params
		: "a0", "v0");
	return r;
}

static inline void __spim_read_string (char *buf, int len)
{
	asm volatile (
		"li   $v0, " str(READ_STRING_SYSCALL) "\n\t"
		"move %0, $a0\n\t"
		"move %1, $a1\n\t"
		"syscall\n\t"
		: "+m" (buf)
		: "r" (buf), "r" (len)
		: "a0", "a1", "v0", "memory");
}

static inline void __spim_sbrk (int brk)
{
	asm volatile (
		"li   $v0, " str(SBRK_SYSCALL) "\n\t"
		"move %0, $a0\n\t"
		"syscall\n\t"
		: // no returns
		: "r" (brk)
		: "a0", "v0", "memory");
}

static inline  __attribute__((noreturn)) void __spim_exit  (void)
{
	asm volatile (
		"li   $v0, " str(EXIT_SYSCALL) "\n\t"
		"syscall\n\t"
		: // no returns
		: // no params
		: "v0");
	__builtin_unreachable();
}

static inline void __spim_exit2 (int code)
{
	asm volatile (
		"li   $v0, " str(EXIT2_SYSCALL) "\n\t"
		"move %0, $a0\n\t"
		"syscall\n\t"
		: // no returns
		: "r" (code)
		: "a0", "v0");
}

static inline void __spim_putchar (int c)
{
	asm volatile (
		"li   $v0, " str(PRINT_CHARACTER_SYSCALL) "\n\t"
		"move %0, $a0\n\t"
		"syscall\n\t"
		: // no returns
		: "r" (c)
		: "a0", "v0");
}

static inline int __spim_getchar (void)
{
	int c;
	asm volatile (
		"li   $v0, " str(READ_CHARACTER_SYSCALL) "\n\t"
		"syscall\n\t"
		"move %0, $v0\n\t"
		: "=r" (c)
		: // no params
		: "a0", "v0");
	return c;
}

static inline int __spim_open (const char *filename, int flags, int mode)
{
	int fd;
	asm volatile (
		"li   $v0, " str(OPEN_SYSCALL) "\n\t"
		"move $a0, %0\n\t"
		"move $a1, %1\n\t"
		"move $a2, %2\n\t"
		"syscall\n\t"
		"move %0, $v0\n\t"
		: "=r" (fd)
		: "r" (filename), "r" (flags), "r" (mode)
		: "a0", "a1", "a2", "v0");
	return fd;
}

static inline int __spim_read (int fd, void *buf, int len)
{
	int res;
	asm volatile (
		"li   $v0, " str(READ_SYSCALL) "\n\t"
		"move $a0, %0\n\t"
		"move $a1, %1\n\t"
		"move $a2, %2\n\t"
		"syscall\n\t"
		"move %0, $v0\n\t"
		: "=r" (res)
		: "r" (fd), "r" (buf), "r" (len)
		: "a0", "a1", "a2", "v0", "memory");
	return res;
}

static inline int __spim_write (int fd, const void *buf, int len)
{
	int res;
	asm volatile (
		"li   $v0, " str(WRITE_SYSCALL) "\n\t"
		"move $a0, %0\n\t"
		"move $a1, %1\n\t"
		"move $a2, %2\n\t"
		"syscall\n\t"
		"move %0, $v0\n\t"
		: "=r" (res)
		: "r" (fd), "r" (buf), "r" (len)
		: "a0", "a1", "a2", "v0");
	return res;
}

static inline void __spim_close (int fd)
{
	asm volatile (
		"li   $v0, " str(CLOSE_SYSCALL) "\n\t"
		"move $a0, %0\n\t"
		"syscall\n\t"
		: // no returns
		: "r" (fd)
		: "a0", "v0");
}

#undef str
#undef str_

#endif // !defined (__SPIM__H_)
