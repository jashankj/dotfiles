((c-mode
  . ((c-file-style . "BSD")
     (c-block-comment-is-default . nil)
     (indent-tabs-mode . t)
     (tab-width . 4)
     (c-basic-offset . 4)
     ))
 (c++-mode
  . ((c-file-style . "BSD")
     (indent-tabs-mode . t)
     (tab-width . 4)
     (c-basic-offset . 4)
     ))
 (glsl-mode
  . ((c-file-style . "BSD")
     (indent-tabs-mode . t)
     (tab-width . 4)
     (c-basic-offset . 4)
     ))
 (java-mode
  . ((c-file-style . "BSD")
     (indent-tabs-mode . t)
     (tab-width . 4)
     (c-basic-offset . 4)
     ))

 (rust-mode
  . ((indent-tabs-mode . t)
     (tab-width . 4)
     ))

 (cperl-mode
  . ((cperl-indent-level . 4)
     (indent-tabs-mode . t)
     (tab-width . 4)
     ))

 (python-mode
  . ((python-indent-offset . 4)
     (tab-width . 4)
     (indent-tabs-mode . t)
     ))

 (shell-script-mode
  . ((indent-tabs-mode . t)
     (tab-width . 4)
     ))

 (js2-mode
  . ((js2-basic-offset . 4)
     (tab-width . 4)
     (indent-tabs-mode . t)
     (fill-column . 72)
     ))
 (coffee-mode
  . ((indent-tabs-mode . nil)
     (tab-width . 8)
     (coffee-tab-width . 2)
     ))

 (org-mode
  . ((indent-tabs-mode . t)
     (tab-width . 4)
     ))
 )
