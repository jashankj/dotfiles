#!/bin/sh

cat <<__EOF__
<?xml version="1.0" encoding="utf-8" ?>
<manifest>
  <include name="repos.xml" />

__EOF__

ls -1d */.git |
LC_ALL=C sort -b |
grep -v hol-mode.el |
while read i
do
	origin="$(git -C "$i" remote show)"
	name="$(git -C "$i" remote show -n "${origin}" \
		| grep 'Fetch URL: ' \
		| awk '{print $3}' \
		| sed -E '
			s@^https?://code\.orgmode\.org/@@;
			s@^https?://git\.code\.sf\.net/p/@@;
			s@^https?://git\.flintfam\.org/swf-projects/@@;
			s@^https?://git\.savannah\.gnu\.org/git/@@;
			s@^https?://git\.savannah\.nongnu\.org/git/@@;
			s@^https?://git\.\.nongnu\.org/git/@@;
			s@^https?://git\.notmuchmail\.org/git/@@;
			s@^https?://github\.com/@@;
			s@^git\@github\.com:@@;
			s@^https?://gitlab\.com/@@;
			s@^https?://mumble\.net/~campbell/git/@@;
			s@^https?://depp\.brause\.cc/@@;
			s@\.git$@@;
			s@paredit$@paredit.git@;
			s@nov\.el$@nov.el.git@
		')"
	rev="$(git -C "$i" rev-parse HEAD)"
	echo "  <extend-project name=\"${name}\" revision=\"${rev}\" />"
done

cat <<__EOF__
</manifest>
__EOF__
