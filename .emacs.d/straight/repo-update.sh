#!/bin/sh
exec repo sync \
	--no-manifest-update \
	--jobs=8 \
	--manifest-name=repos.xml "$@"
