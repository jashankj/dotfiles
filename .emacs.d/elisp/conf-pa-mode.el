;;; conf-pa-mode.el --- simple mode for editing PulseAudio config  -*- lexical-binding: t; -*-

;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright (c) 2022 Jashank Jeremy.

;; Author:	Jashank Jeremy <jashank@rulingia.com.au>
;; Keywords:	conf pulseaudio

;; This file is not part of GNU Emacs.

;;; Commentary:

;; A brute-force keyword-smashing PulseAudio script-editing mode.

;;; Code:

(require 'conf-mode)

(defconst conf-pa--commands-list
  '("help" "list-modules" "list-cards" "list-sinks" "list-sources"
    "list-clients" "list-sink-inputs" "list-source-outputs" "stat"
    "info" "ls" "list" "load-module" "unload-module" "describe-module"
    "set-sink-volume" "set-source-volume" "set-sink-mute"
    "set-source-mute" "set-sink-input-volume" "set-source-output-volume"
    "set-sink-input-mute" "set-source-output-mute" "set-default-sink"
    "set-default-source" "set-card-profile" "set-sink-port"
    "set-source-port" "set-port-latency-offset" "suspend-sink"
    "suspend-source" "suspend" "move-sink-input" "move-source-output"
    "update-sink-proplist" "update-source-proplist"
    "update-sink-input-proplist" "update-source-output-proplist"
    "list-samples" "play-sample" "remove-sample" "load-sample"
    "load-sample-lazy" "load-sample-dir-lazy" "kill-client"
    "kill-sink-input" "kill-source-output" "set-log-level"
    "set-log-meta" "set-log-target" "set-log-time" "set-log-backtrace"
    "play-file" "dump" "dump-volumes" "shared" "send-message" "exit")
  "A list of commands known by PulseAudio.")

(defconst conf-pa-keywords-regexp
  (rx-to-string `(or ,@conf-pa--directives-list
		     ,@conf-pa--commands-list)))


(defconst conf-pa--directives-list
  '(".include"
    ".fail" ".nofail"
    ".ifexists" ".else" ".endif")
  "A list of the \"meta directives\" known by PulseAudio.")

(defconst conf-pa-font-lock-keywords
  `(;; Directives ("meta directives"):
    (,(rx-to-string
       `(seq line-start
	     (0+ space)
	     (group-n 1 (or ,@conf-pa--directives-list))
	     (opt
	      (1+ space)
	      (group-n 2 (1+ (or (syntax word) ?- ?.))))))
     (1 'font-lock-keyword-face)
     (2 'font-lock-variable-name-face nil t))
    ;; Commands:
    (,(rx-to-string
       `(seq line-start
	     (0+ space)
	     (group-n 1 (or ,@conf-pa--commands-list))
	     (opt
	      (1+ space)
	      (group-n 2 (1+ (or (syntax word) ?-))))))
     (1 'font-lock-function-name-face)
     (2 'font-lock-variable-name-face nil t))))

(define-derived-mode conf-pa-mode conf-mode "Conf[PulseAudio]"
  (conf-mode-initialize "#" 'conf-pa-font-lock-keywords)
  (setq-local conf-assignment-sign nil))

(provide 'conf-pa-mode)

;;; conf-pa-mode.el ends here
