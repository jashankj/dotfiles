;; this file contains the colour set that defined Lunarized.

;; Copyright (C) 2011-2016 Bozhidar Batsov

;; Author: Bozhidar Batsov <bozhidar@batsov.com>
;; Author: Thomas Frössman <thomasf@jossystem.se>
;; Author: Jashank Jeremy <jashank@rulingia.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; <snip>
;;; Setup Start
(defmacro lunarized-with-color-variables (variant &rest body)
  (declare (indent defun))
  `(let* ((class '((class color) (min-colors 89)))
          (light-class (append '((background light)) class))
          (dark-class (append '((background dark)) class))
          (variant ,variant)

          (s-base03    (if lunarized-termcolors "black"        "#280100"))
          (s-base02    (if lunarized-termcolors "black"        "#340707"))
          ;; emphasized content
          (s-base01    (if lunarized-termcolors "brightgreen"  "#5d4346"))
          ;; primary content
          (s-base00    (if lunarized-termcolors "brightyellow" "#826664"))
          (s-base0     (if lunarized-termcolors "white"        "#958384"))
          ;; comments
          (s-base1     (if lunarized-termcolors "white"        "#a09294"))
          ;; background highlight light
          (s-base2     (if lunarized-termcolors "red"          "#d4deed"))
          ;; background light
          (s-base3     (if lunarized-termcolors "brightred"    "#e3eefd"))

          ;; Lunarized accented colors
          (yellow    (if lunarized-termcolors "yellow"        "#b58900"))
          (orange    (if lunarized-termcolors "brightred"     "#cb4b16"))
          (red       (if lunarized-termcolors "red"           "#dc322f"))
          (magenta   (if lunarized-termcolors "magenta"       "#d33682"))
          (violet    (if lunarized-termcolors "brightmagenta" "#6c71c4"))
          (blue      (if lunarized-termcolors "blue"          "#268bd2"))
          (cyan      (if lunarized-termcolors "cyan"          "#2aa198"))
          (green     (if lunarized-termcolors "green"         "#859900"))

          ;; Darker and lighter accented colors
          ;; Only use these in exceptional circumstances!
          (yellow-d  "#7B6000")
          (yellow-l  "#DEB542")
          (orange-d  "#8B2C02")
          (orange-l  "#F2804F")
          (red-d     "#990A1B")
          (red-l     "#FF6E64")
          (magenta-d "#93115C")
          (magenta-l "#F771AC")
          (violet-d  "#3F4D91")
          (violet-l  "#9EA0E5")
          (blue-d    "#00629D")
          (blue-l    "#69B7F0")
          (cyan-d    "#00736F")
          (cyan-l    "#69CABF")
          (green-d   "#546E00")
          (green-l   "#B4C342")

          ;; Lunarized palette names, use these instead of -fg -bg...
          (base0 (if (eq variant 'light) s-base00 s-base0))
          (base00 (if (eq variant 'light) s-base0 s-base00))
          (base1 (if (eq variant 'light) s-base01 s-base1))
          (base01 (if (eq variant 'light) s-base1 s-base01))
          (base2 (if (eq variant 'light) s-base02 s-base2))
          (base02 (if (eq variant 'light) s-base2 s-base02))
          (base3 (if (eq variant 'light) s-base03 s-base3))
          (base03 (if (eq variant 'light) s-base3 s-base03))

          ;; Line drawing color
          ;;
          ;; NOTE only use this for very thin lines that are hard to see using base02, in low
          ;; color displayes base02 might be used instead
          (s-line (if (eq variant 'light) s-base2 s-base02))
          ;(s-line (if (eq variant 'light) "#cccec4" "#284b54"))

          ;; Light/Dark adaptive higher/lower contrast accented colors
          ;;
          ;; NOTE Only use these in exceptional cirmumstances!
          (yellow-hc (if (eq variant 'light) yellow-d yellow-l))
          (yellow-lc (if (eq variant 'light) yellow-l yellow-d))
          (orange-hc (if (eq variant 'light) orange-d orange-l))
          (orange-lc (if (eq variant 'light) orange-l orange-d))
          (red-hc (if (eq variant 'light) red-d red-l))
          (red-lc (if (eq variant 'light) red-l red-d))
          (magenta-hc (if (eq variant 'light) magenta-d magenta-l))
          (magenta-lc (if (eq variant 'light) magenta-l magenta-d))
          (violet-hc (if (eq variant 'light) violet-d violet-l))
          (violet-lc (if (eq variant 'light) violet-l violet-d))
          (blue-hc (if (eq variant 'light) blue-d blue-l))
          (blue-lc (if (eq variant 'light) blue-l blue-d))
          (cyan-hc (if (eq variant 'light) cyan-d cyan-l))
          (cyan-lc (if (eq variant 'light) cyan-l cyan-d))
          (green-hc (if (eq variant 'light) green-d green-l))
          (green-lc (if (eq variant 'light) green-l green-d))

          ;; customize based face properties
          (s-maybe-bold (if lunarized-use-less-bold
                            'unspecified 'bold))
          (s-maybe-italic (if lunarized-use-more-italic
                              'italic 'normal))
          (s-variable-pitch (if lunarized-use-variable-pitch
                                'variable-pitch 'default))
          (s-fringe-bg (if lunarized-distinct-fringe-background
                           base02 base03))
          (s-fringe-fg base01)

          (s-header-line-fg (if lunarized-high-contrast-mode-line
                                base1 base0))
          (s-header-line-bg (if lunarized-high-contrast-mode-line
                                base02 base03))
          (s-header-line-underline (if lunarized-high-contrast-mode-line
                                       nil base02))

          (s-mode-line-fg (if lunarized-high-contrast-mode-line
                              base03 base0))
          (s-mode-line-bg (if lunarized-high-contrast-mode-line
                              base0 base02))
          (s-mode-line-underline (if lunarized-high-contrast-mode-line
                                     nil s-line))

          (s-mode-line-buffer-id-fg (if lunarized-high-contrast-mode-line
                                        'unspecified base1))
          (s-mode-line-inactive-fg (if lunarized-high-contrast-mode-line
                                       base0 base01))
          (s-mode-line-inactive-bg (if lunarized-high-contrast-mode-line
                                       base02 base03))
          (s-mode-line-inactive-bc (if lunarized-high-contrast-mode-line
                                       base02 base02))
          )
     ,@body))
;; <snip>
