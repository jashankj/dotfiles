;; curry-compose.el --- `curry', `rcurry', and `compose'. -*- lexical-binding: t -*-

;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright 2013, Eric Schulte.
;; Copyright 2022, Jashank Jeremy.

;; Author: Eric Schulte <https://github.com/eschulte/>
;; URL:    <https://gist.github.com/eschulte/6167923>

;; Mucked around with by Jashank Jeremy <https://jashankj.space/>.

;;; Commentary:

;; Allows for more compact anonymous functions.
;; The following examples demonstrate the usage.
;;
;;     ;; partial application with `curry'
;;     (mapcar (» #'+ 2) '(1 2 3 4)) ; => (3 4 5 6)
;;
;;     ;; alternate order of arguments with `rcurry'
;;     (mapcar (« #'- 1) '(1 2 3 4)) ; => (0 1 2 3)
;;
;;     ;; function composition with `compose'
;;     (mapcar (∘ #'list (» #'* 2)) '(1 2 3 4)) ; => ((2) (4) (6) (8))

;; jashankj notes: `cl' is deprecated; `lexical-let' is obsolete.
;; However: they're still present, still work, and adding a well-placed
;; `require' makes this code all behave as expected.

;;; Code:

;;;; Function definitions:
(defsubst curry (function &rest arguments)
  "Partial application of FUNCTION to some ARGUMENTS.

For example,
  \(mapcar \(curry #'+ 2\) \='\(1 2 3 4\)\)
would evaluate to
  \(3 4 5 6\)"
  (let ((function function)
        (arguments arguments))
    (lambda (&rest more) (apply function (append arguments more)))))

(defsubst rcurry (function &rest arguments)
  "Reversed partial application of FUNCTION to some ARGUMENTS.

For example,
  \(mapcar \(« #\='- 1) \='\(1 2 3 4\)\)
would evaluate to
  \(0 1 2 3\)"
  (let ((function function)
        (arguments arguments))
    (lambda (&rest more) (apply function (append more arguments)))))

(defsubst compose (function &rest more-functions)
  "Composition of FUNCTION and MORE-FUNCTIONS.

For example,
  \(mapcar \(compose #'list \(curry #'* 2\)\) '\(1 2 3 4\)\)
would evaluate to
  \(\(2\) \(4\) \(6\) \(8\)\)"
  (cl-reduce
   (lambda (f g)
     (let ((f f) (g g))
       (lambda (&rest arguments)
         (funcall f (apply g arguments)))))
   more-functions
   :initial-value function))

;;;; Compact display:
(defun pretty-curry-compose ()
  "Configure compact pretty-printing for curry-compose primitives."
  (mapc (lambda (pair)
          (let ((regexp (car pair))
                (symbol (cdr pair)))
            (font-lock-add-keywords
	     'emacs-lisp-mode
             `((,regexp
                (0 (progn (compose-region (match-beginning 1) (match-end 1)
					  ,symbol)
                          nil)))))))
        '(("(\\(compose\\)[ \t\n\r]" . ?\∘)
          ("(\\(curry\\)[ \t\n\r]" . ?\»)
          ("(\\(rcurry\\)[ \t\n\r]" . ?\«))))
(add-to-list 'emacs-lisp-mode-hook 'pretty-curry-compose)

;;;; Color these functions like keywords
(font-lock-add-keywords
 'emacs-lisp-mode
 '(("(\\(compose\\)[ \t\n\r]" 1 font-lock-keyword-face)
   ("(\\(curry\\)[ \t\n\r]" 1 font-lock-keyword-face)
   ("(\\(rcurry\\)[ \t\n\r]" 1 font-lock-keyword-face)))

(provide 'curry-compose)
;;; curry-compose.el ends here
