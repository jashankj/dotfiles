((org-mode
  .
  ((indent-tabs-mode . nil)
   (org-imenu-depth . 5)
   (org-src-fontify-natively . nil)
   (org-todo-keyword-faces . (("DISABLED" . org-todo)))
   (rainbow-delimiters-mode . t)
   (tab-width . 8))))
