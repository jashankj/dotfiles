#!/usr/bin/env python3

"""
Reads ``PACKAGES.toml``, and installs or updates relevant packages.

``PACKAGES.toml`` is a dictionary in three levels::

  group:
	operator:
	  package:
		[version: str]
		[operator: str]

"""

import argparse
import os
import pathlib
from pathlib import Path
import pprint
import subprocess
import sys
import tomllib
import typing

type PackageName = str
type Operator    = str
type GroupName   = str
type Package     = dict[Operator, str]
type Packages    = dict[PackageName, Package]
type Operables   = dict[Operator, Packages]
type Operable    = dict[GroupName, Operables]
type Config      = Operable

# all possible operators
_OPERATORS: set[Operator] = {
	# 'cabal',
	# 'cpanm',
	# 'crate',
	# # 'deb',
	'gem',
	# 'gomod',
	# 'npm',
	# 'opam',
	# 'pacman',
	# 'pip',
	'rpm',
}

def _make_argument_parser() -> argparse.ArgumentParser:
	argp = argparse.ArgumentParser()
	for op in _OPERATORS:
		argp.add_argument(
			f'--use-{op}',
			action = argparse.BooleanOptionalAction,
		)
	argp.add_argument('group', type = str, nargs = 1)
	return argp

def load_config(file: str) -> Config:
	with open(file, 'rb') as f:
		return tomllib.load(f)

def path_of(envvar: str, default: str) -> Path:
	if val := os.environ.get(envvar, None):
		return Path(val)
	return Path(default).expanduser()

_GEM_HOME: Path = path_of('GEM_HOME', '~/.gem/ruby/current')

def cmd_dnf5_install(name: PackageName, pkg: Package) -> list[str]:
	return ['sudo', 'dnf5', 'install', name]

def permitted_operator_p(args: argparse.Namespace, oper: Operator) -> bool:
	return f'use_{oper}' in args and vars(args)[f'use_{oper}']

def sidestep(args: argparse.Namespace, name: PackageName, pkg: Package,
			 rpm_guess: str | None = None, deb_guess: str | None = None) -> list[str] | None:
	if 'use_rpm' in args and args.use_rpm and 'rpm' in pkg:
		if type(pkg['rpm']) == str:
			name = pkg['rpm']
		elif pkg['rpm']:
			name = rpm_guess or name
		return cmd_dnf5_install(name, pkg)
	if 'use_deb' in args and args.use_deb and 'deb' in pkg:
		# TODO
		assert False, "unimplemented"
	return None

def op_gem(args: argparse.Namespace, name: PackageName, pkg: Package) -> list[str]:
	# `gem` supports sidestepping to `rpm`, `deb`:
	return sidestep(
		args, name, pkg,
		rpm_guess = f'rubygems({name})',
		deb_guess = None
	) or [
		'gem', 'install',
		'--verbose',
		'--no-user-install',
		f'--install-dir={_GEM_HOME}',
		name,
		*(['--version', pkg['version']] if 'version' in pkg else [])
	]

def op_cargo(args: argparse.Namespace, name: PackageName, pkg: Package) -> list[str]:
	# `cargo` supports sidestepping to `rpm`, `deb`:
	return sidestep(args, name, pkg) or [
		'cargo', 'install',
		name
	]

def op_cabal(args: argparse.Namespace, name: PackageName, pkg: Package) -> list[str]:
	# `cabal` supports sidestepping to `rpm`, `deb`:
	return sidestep(args, name, pkg) or [
		'cabal', 'install',
		'--verbose',
		'--overwrite-policy=always',
		'--user',
		name
	]

def op_pip(args: argparse.Namespace, name: PackageName, pkg: Package) -> list[str]:
	# `pip` supports sidestepping to `rpm`, `deb`:
	return sidestep(args, name, pkg) or [
		'python3', '-m',
		'pip', 'install',
		'--user',
		'--upgrade',
		name
	]

def op_cpanm(args: argparse.Namespace, name: PackageName, pkg: Package) -> list[str]:
	# `cpanm` supports sidestepping to `rpm`, `deb`:
	return sidestep(args, name, pkg) or [
		'cpanm',
		'--local-lib-contained', '~/perl5',
		'--prompt',
		name
	]

_OPS = {
	'cabal': op_cabal,
	'cargo': op_cargo,
	'cpanm': op_cpanm,
	'gem': op_gem,
	'pip': op_pip,
}

def main(argv: list[str]) -> int:
	args: argparse.Namespace = _make_argument_parser().parse_args(argv[1:])
	pprint.pp(args)

	configuration = load_config('PACKAGES.toml')
	group = args.group[0]

	if group not in configuration:
		raise Exception(f"unknown group: {repr(group)}")
	assert group in configuration

	oper_packages = configuration[group]
	pprint.pp(oper_packages)

	for (operator, packages) in oper_packages.items():
		for (package, override) in packages.items():
			cmd = _OPS[operator](args, package, override)
			print(cmd)
			subprocess.run(cmd)

	return 0

if __name__ == '__main__':
	sys.exit(main(sys.argv))
