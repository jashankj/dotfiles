package Repo;

use v5;
use strict;
use warnings;
use feature 'say';
use feature 'switch';
no warnings 'experimental::smartmatch';

use Benchmark ':hireswallclock';
use Carp;
use Class::Struct;
use Data::Dumper;
use File::chdir;
use File::Spec;
use YAML::XS;

struct 'Repo' => [
	'section'     => '$',       # String; section name
	'path'        => '$',       # Path; filesystem path relative to /src
	'vcs'         => '$',       # [ git | git+bare | ... ]
	'owner'       => '$',       # Option<String>
	'origin'      => '$',       # Option<URI>
	'description' => '$',       # Option<String>

	'indexed'     => '$',       # Option<bool>, default false; run indexing
	'updated'     => '$',       # Option<bool>, default true; run updates

	'update'      => 'Repo::Update', # Option<RepoUpdate>
	'post_update' => '$',       # Option<String>; post-update hook.
	'build'       => '$',       # Option<String>; a build script.
];

struct 'Repo::Update' => [
	'git' => 'Repo::Update::git',
	'hg'  => 'Repo::Update::hg',
];

struct 'Repo::Update::git' => [
	'args'       => '@',
	'rebase'     => '$',
	'submodules' => '$',
	'remote'     => '$',
	'branch'     => '$',
];

struct 'Repo::Update::hg' => [
	'mq' => '$',
];

########################################################################

our $SRCDIR    = '/src';
our $CONFIG    = File::Spec->catfile($SRCDIR, '.bin', 'REPOSITORIES.yaml');
our $LOCKDIR   = File::Spec->catfile($SRCDIR, '.bin', 'locks');

our $REPO_URL  = 'ssh://alyzon.rulingia.com.au//src/';
our $CGIT_URL  = 'http://alyzon.rulingia.com.au/srcgit/';
our $HGWEB_URL = 'http://alyzon.rulingia.com.au/srchg/';
our $BARE_URL  = 'http://alyzon.rulingia.com.au/src/';

our $QUIET_JOBS = not -t STDOUT;

sub load_all {
	map { Repo->new(%$_) }
		@{YAML::XS::LoadFile $Repo::CONFIG};
}

sub realpath { File::Spec->catfile($SRCDIR, shift->path) }


########################################################################

sub do_update {
	my $self  = shift;
	if ($self->is_locked) {
		say "=== ", $self->path, ": ignored (locked)";
		return;
	}

	if (defined $self->updated and not $self->updated) {
		say "=== ", $self->path, ": ignored (disabled)";
		return;
	}

	$self->do_lock;
	say "=== ", $self->path, ": updating";
	my $t0 = Benchmark->new;
	given ($self->vcs) {
		$self->do_update_git        when 'git';
		$self->do_update_git_bare   when 'git+bare';
		$self->do_update_hg         when 'hg';
		$self->do_update_fossil     when 'fossil';
		$self->do_update_darcs      when 'darcs';

		$self->do_update_svn        when 'svn';
		$self->do_update_cvs        when 'cvs';

		$self->do_update_grepo      when 'grepo';
		$self->do_update_gclient    when 'gclient';
		$self->do_update_gjiri      when 'gjiri';

		default {
			carp $self->path.": don't know how to update";
		}
	}
	my $t1 = Benchmark->new;
	$self->do_unlock;
	my $td = timediff $t1, $t0;
	say "\tdone in " . timestr($td) . "\n";
}

# Repo#do_update_git
#   update this Git repository; replaces `do_git_tree'.
sub do_update_git {
	my $self = shift;
	my $cfg = $self->update->git
		if $self->update and $self->update->git;
	my $rebase = (not $cfg) or ($cfg and $cfg->rebase);
	my $submod = (not $cfg) or ($cfg and $cfg->submodules);

	my @cmd = (
		'git',
		'-C', '/src/'.$self->path,
		'pull',
	);

	push @cmd, '--rebase'             if $rebase;
	push @cmd, '--recurse-submodules' if $submod;
	push @cmd, '--quiet'              if $QUIET_JOBS;
	if ($cfg and $cfg->remote) {
		push @cmd, $cfg->remote;
		push @cmd, $cfg->branch if $cfg->branch;
	}
	push @cmd, @{$cfg->args} if $cfg and $cfg->args;

	system @cmd
}

# Repo#do_update_git_bare
#   update this bare Git repository.
sub do_update_git_bare {
	my $self = shift;
	carp $self->path.": cannot currently update bare Git repos";
}

# Repo#do_update_hg
#   update this Mercurial repository.
sub do_update_hg {
	my $self = shift;
	local $CWD = $self->realpath;

	my $cfg = $self->update->hg
		if $self->update and $self->update->hg;
	system 'hg', 'qpop', '--all' if $cfg and $cfg->mq;

	my @cmd = qw(hg pull --update);
	push @cmd, '--quiet' if $QUIET_JOBS;

	system @cmd
}

# Repo#do_update_fossil
#   update this Fossil repository.
sub do_update_fossil {
	my $self = shift;
	local $CWD = $self->realpath;

	my @fossil = ('fossil');
	push @fossil, '--quiet' if $QUIET_JOBS;

	system @fossil, 'pull' and
	system @fossil, 'update'
}

# Repo#do_update_darcs
#   update this Darcs repository.
sub do_update_darcs {
	my $self = shift;
	local $CWD = $self->realpath;

	my @cmd = qw(darcs pull);
	push @cmd, '--quiet' if $QUIET_JOBS;
	system @cmd
}


# Repo#do_update_svn
#   update this Subversion checkout.
sub do_update_svn {
	my $self = shift;
	local $CWD = $self->realpath;

	my @cmd = qw(svn update);
	push @cmd, '--quiet' if $QUIET_JOBS;

	system @cmd
}

# Repo#do_update_cvs
#   update this CVS checkout.
sub do_update_cvs {
	my $self = shift;
	local $CWD = $self->realpath;

	my @cmd = (
		'cvs', '-R', ($QUIET_JOBS ? '-Q' : '-q'),
		'update', '-P', '-d', '.'
	);
	system @cmd
}


# Repo#do_update_grepo
#   update this Google `repo' depots (Android, seL4, etc.)
sub do_update_grepo {
	my $self = shift;
	local $CWD = $self->realpath;

	my @cmd = qw(repo sync);
	push @cmd, '--quiet' if $QUIET_JOBS;

	system @cmd;
}

# Repo#do_update_gclient
#   update this Google `gclient' depots (Chromium)
sub do_update_gclient {
	my $self = shift;
	carp $self->path.": cannot currently update Google \`gclient' depots";
}

# Repo#do_update_gjiri
#   update this Google `jiri' depots (Fuchsia)
sub do_update_gjiri {
	my $self = shift;
	carp $self->path.": cannot currently update Google \`jiri' depots";
}


########################################################################

# -"build")
# -	if [ "X$2" = "X" ]
# -	then
# -		echo "builders:" $(cd "$S/.bin/builders" && ls)
# -	else
# -		do_tree_build "$2"
# -	fi
# -	;;


########################################################################

sub do_vacuum {
	my $self  = shift;
	if ($self->is_locked) {
		say "=== ", $self->path, ": ignored";
		return;
	}

	$self->do_lock;
	say "=== ", $self->path, ": vacuuming";
	my $t0 = Benchmark->new;
	given ($self->vcs) {
		$self->do_vacuum_git        when 'git';
		$self->do_vacuum_git        when 'git+bare';
		$self->do_vacuum_fossil     when 'fossil';

		default {
			carp $self->path.": don't know how to vacuum ".$self->vcs;
		}
	}
	my $t1 = Benchmark->new;
	$self->do_unlock;
	my $td = timediff $t1, $t0;
	say "\tdone in " . timestr($td);
}

# Repo#do_vacuum_git
#   run garbage collection, etc. on this Git repository.
sub do_vacuum_git {
	my $self = shift;
	my @git = ('git', '-C', '/src/'.$self->path);

	system @git, 'gc', '--auto' and
	system @git, 'repack', '-A', '-d'
}

# Repo#do_vacuum_fossil
#   run garbage collection, etc. on this Fossil repository.
sub do_vacuum_fossil {
	my $self = shift;
	local $CWD = $self->realpath;
	my @cmd = (
		'fossil', 'rebuild',
		'--analyse',
		'--cluster',
		'--index',
		'--vacuum',
	);

	system @cmd
}


########################################################################

sub do_index {
	my $self = shift;
	unless ($self->indexed) {
		say "=== ", $self->path, ": ignored (disabled)";
		return;
	}

	$self->do_lock;
	say "=== ", $self->path, ": indexing";
	my $t0 = Benchmark->new;
	system 'cindex', $self->realpath;
	my $t1 = Benchmark->new;
	$self->do_unlock;
	my $td = timediff $t1, $t0;
	say "\tdone in " . timestr($td) . "\n";
}

# -"index")
# -	echo "=== rebuilding csearch index..."
# -	for p in $(cat $S/.bin/indexpaths)
# -	do
# -		$S/.bin/cindex ${p}
# -	done
# -	;;
# -
# -"reindex")
# -	if [ -e $CSEARCHINDEX ]
# -	then
# -		echo "=== rebuilding csearch index..."
# -		$S/.bin/cindex -list | sed -e 's/^/	/g'
# -		$S/.bin/cindex
# -	else
# -		echo "!!! no search index exists. run 'src index' instead."
# -	fi
# -	;;
# -
# -"grep")
# -	shift
# -	exec $S/.bin/csearch "$@"
# -	;;


########################################################################

# Repo#lockfile
#   return the path of the lockfile
sub lockfile {
	my $self  = shift;
	my $lockf = $self->path;
	$lockf =~ s@/@__@g;
	File::Spec->catfile($LOCKDIR, $lockf)
}

# Repo#is_locked
#   return true if the repository is locked.
sub is_locked {
	my $self = shift;
	-e $self->lockfile
}

# Repo#do_lock
#   set the lock on this repository
sub do_lock {
	my $self = shift;
	croak $self->path.": already locked" if $self->is_locked;

	open my $f, '>', $self->lockfile
		or croak $self->path.": cannot lock: $?";
	$f->print("$$\n");
	$f->close;
}

# Repo#do_unlock
#   remove the lock on this repository
sub do_unlock {
	my $self = shift;
	croak $self->path.": not locked" unless $self->is_locked;
	unlink $self->lockfile
		or croak $self->path.": cannot unlock: $?";
}


########################################################################

sub make_cgit_repo {
	my $self = shift;
	return
	unless $self->vcs eq 'git'
		or $self->vcs eq 'git+bare';

	say 'repo.url=',       $self->path;
	given ($self->vcs) {
		say 'repo.path=/src/', $self->path          when 'git+bare';
		say 'repo.path=/src/', $self->path, '/.git' when 'git';
	}
	say 'repo.section=',   $self->section;
	say 'repo.owner=',     $self->owner if $self->owner;
	say 'repo.clone-url=', join ' ', $self->cgit_clone_urls;
	say 'repo.desc=',      $self->cgit_description;
	say '';
}

sub local_clone_url { $REPO_URL . shift->path }
sub cgit_clone_urls {
	my $self = shift;
	my @urls;
	push @urls, $self->local_clone_url;
	push @urls, $self->origin if $self->origin;
	@urls
}

sub cgit_description {
	my $self = shift;
	my $descr = '';
	$descr .= $self->description . ' ' if $self->description;
	$descr .= $self->cgit_descr_mirror if $self->origin;
	$descr
}

sub cgit_descr_mirror {
	$_ = shift->origin;
	s#https://github\.com/#github:#;
	s#git\@github\.com:#github:/#;
	s#https://gitlab\.com/#gitlab:#;
	s#git\@gitlab\.com:#gitlab:/#;
	'[mirror of ' . $_ . ']'
}


########################################################################

sub make_hgwebrc {
	my $self = shift;
	return unless $self->vcs eq 'hg';
	say sprintf '/%s = /src/%s', $self->path, $self->path
}


########################################################################

my $reverse_repos;

sub make_reverse_repos {
	$reverse_repos = {};
	foreach my $repo (Repo::load_all) {
		$reverse_repos->{ $repo->path } = $repo;
	}
}

sub url_for {
	my $path = shift;
	make_reverse_repos unless $reverse_repos;

	$path =~ s@\A/src/@@;
	my ($vol, $dir, $file, @dirs, $repo);
	($vol, $dir, $file) = File::Spec->splitpath($path);
	       @dirs        = File::Spec->splitdir($dir);
	foreach my $i (reverse 0..$#dirs) {
		my $repodir = File::Spec->catdir(@dirs[0..$i]);
		next unless exists $reverse_repos->{$repodir};
		$repo = $reverse_repos->{$repodir};
		$file = File::Spec->catfile(@dirs[($i + 1) .. ($#dirs - 1)], $file);
	}

	return $BARE_URL . $path unless $repo;

	if ($repo and $repo->vcs eq 'git' or $repo->vcs eq 'git+bare') {
		$CGIT_URL  . $repo->path . '/tree/' . $file
	} elsif ($repo and $repo->vcs eq 'hg') {
		$HGWEB_URL . $repo->path . '/file/tip/' . $file
	} else {
		$BARE_URL  . $path
	}
}


########################################################################

1;

__END__;
