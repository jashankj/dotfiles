#!/usr/local/bin/perl
########################################################################
# codesearch driver
#
#     /
#     /q?q=<text>
#     /_os.xml

use v5;
use strict;
use warnings;
use utf8;
use feature 'unicode_strings';
binmode STDOUT, ':utf8';

use Benchmark ':hireswallclock';
use Cwd;
use Data::Dumper;
use Encode qw{encode decode};
use HTTP::Message;
use IPC::Run3;
use MIME::Base64;
use URI::Escape;

use lib '/src/.bin';
use lib '/home/jashank/perl5/lib/perl5';
use Repo;

my $DEBUG = 0;

sub route($$$);
sub tmpl($;%);
sub escaped($);
sub trimmed($);
sub say;
sub __404();

my $BASE         = 'http://alyzon.rulingia.com.au/srcsearch';

my $CGREP        = '/home/jashank/bin.freebsd-amd64/cgrep';
my $CINDEX       = '/home/jashank/bin.freebsd-amd64/cindex';
my $CSEARCH      = '/home/jashank/bin.freebsd-amd64/csearch';
my $CSEARCHINDEX = '/src/.csearchindex';

sub csearch_home();
sub csearch_csearch($);
sub csearch_opensearch_xml();

########################################################################
# the most hopeless templating system ... in the world.

my %TEMPLATES = ();
{
	my $capture = '';
	while (<DATA>) {
		chomp;
		if (/^%% (.*)$/) { $capture = $1; next; }
		$TEMPLATES{$capture} = '' unless exists $TEMPLATES{$capture};
		$TEMPLATES{$capture} .= "$_\n";
	}
};

sub tmpl($;%) {
	my ($tmpl, %opts) = @_;
	$tmpl = $TEMPLATES{$tmpl};
	$opts{'BASE'} = $BASE;
	for my $k (keys %opts) {
		my $v = $opts{$k};
		$tmpl =~ s#\{\{\s?$k\s?\}\}#$v#gs;
	}
	$tmpl
}

sub escaped($) {
	($_) = @_;
	s/&/&amp;/mg; s/</&lt;/mg; s/>/&gt;/mg;
	return $_
}

sub trimmed($) {
	($_) = @_;
	s/^  //mg;
	return $_
}

########################################################################
# the most miserable application engine ... in the world.

my $STATUS = undef;
my $CONTENT_TYPE = 'text/html';
my $OUTPUT_BUFFER = '';
sub say { $OUTPUT_BUFFER .= join '', @_; }
sub __404() {
	$STATUS = '404 Not Found';
	say tmpl 'header';
	say tmpl '404';
	say tmpl 'footer';
	goto finish;
}

########################################################################
# the worst application router ... in the world.

sub route($$$) {
	my ($method, $path, $query) = @_;

	if ($method eq 'GET' and $path =~ m#^/?$#) {
		csearch_home;

	} elsif ($method eq 'GET' and $path =~ m#^/q$# and $query =~ m#^q=(.+)$#) {
		csearch_csearch uri_unescape $1;

	} elsif ($method eq 'GET' and $path =~ m#^/_os\.xml$#) {
		csearch_opensearch_xml;

	} else {
		__404;
	}
}

########################################################################

sub csearch_home() {
	say tmpl 'header';
	say tmpl 'home';
	say tmpl 'footer';
}

########################################################################

sub csearch_csearch($) {
	my ($query) = @_;
	say tmpl 'header';

	my $results = {};
	my $n = 0;
	$ENV{'CSEARCHINDEX'} = $CSEARCHINDEX;
	my $t0 = Benchmark->new;
	run3 [$CSEARCH, '-n', $query], \undef, sub {
		my $line = shift;
		my ($file, $linum, $match) = split /:/, $line, 3;
		$results->{$file} = {} unless exists $results->{$file};
		$results->{$file}->{$linum} = $match;
		$n++;
	};
	my $t1 = Benchmark->new;
	my $t  = timediff $t1, $t0;

	sub mark_match($$) {
		my $query = shift;
		my $text  = shift;
		$text =~ m{$query};
		$` . "<mark>" . $& . "</mark>" . $';
	}

	say tmpl 'results_begin',
		n => $n,
		t => sprintf('%5.03f', $t->real);
	foreach my $file (sort keys %$results) {
		say tmpl 'result_file_begin',
			file => $file,
			url  => Repo::url_for($file);
		foreach my $linum (sort { $a <=> $b } keys %{ $results->{$file} }) {
			say tmpl 'result_file_code',
			  linum  => $linum,
			  result => mark_match $query, escaped $results->{$file}->{$linum};
		}
		say tmpl 'result_file_end';
	}
	say tmpl 'results_end';

	say tmpl 'footer';
}

########################################################################

sub csearch_opensearch_xml() {
	$CONTENT_TYPE = 'application/opensearchdescription+xml';
	say tmpl 'opensearch-description';
}

########################################################################
my $method = 'GET';
my $path   = '';
my $query  = '';
if (exists $ENV{'REQUEST_METHOD'} and exists $ENV{'PATH_INFO'} and exists $ENV{'QUERY_STRING'}) {
	$method = $ENV{'REQUEST_METHOD'};
	$path   = $ENV{'PATH_INFO'};
	$query  = $ENV{'QUERY_STRING'};
} elsif (scalar @ARGV == 1) {
	$DEBUG  = 1;
	$method = 'GET';
	$path   = $ARGV[0];
} elsif (scalar @ARGV == 2) {
	$DEBUG  = 1;
	$method = $ARGV[0];
	$path   = $ARGV[1];
} elsif (scalar @ARGV == 3) {
	$DEBUG  = 1;
	$method = $ARGV[0];
	$path   = $ARGV[1];
	$query  = $ARGV[2];
}

$path =~ s#/$##;
route $method, $path, $query;

finish:
print "Status: $STATUS\n" if defined $STATUS;
print "Content-type: $CONTENT_TYPE; charset=utf-8\n\n";
print $OUTPUT_BUFFER;
1;

########################################################################
########################################################################
########################################################################

__END__;
%% 404
<div class="alert alert-danger w-100 mx-auto text-center">
  <h4>Not found</h4>
  You've stumbled off the trail!
</div>

%% header
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="stake" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="codesearch" />
  <link rel="search" type="application/opensearchdescription+xml"
        title="csearch" href="{{BASE}}/_os.xml" />

  <title>codesearch</title>
  <style type="text/css">
    html {
      height: 100% }
    code {
      white-space: pre }
    body {
      margin: 0;
      height: 100%;
      display: grid;
      grid-template-rows: 5rem auto 5rem;
      grid-template-columns: auto;
      grid-template-areas: "header" "main" "footer" }

    header {
      grid-area: header;
      background-color: #f0f0f0;
      padding-bottom: 1rem;
      text-align: center }
    main {
      grid-area: main;
      align-self: center;
      width: 100%;
      max-width: 60rem;
      margin: 0 auto;
      padding: 0 1rem; }
    footer {
      grid-area: footer;
      background-color: #c0c0c0;
      margin-top: 0.75rem;
      padding: 0.75rem 0;
      text-align: center }

    form {
      padding-top: 2rem;
      text-align: center;
      font-size: 125% }

    ul.results {
      padding: 0;
      list-style-type: none; }
    ul.results > li {
      padding-bottom: 2ex; }
    ul.results > li > ol > li::marker {
      font-size: 80%; }

  </style>
</head>
<body>
<header>
<h1>codesearch</h1>
</header>
<main>

%% home
<form method="GET" action="{{BASE}}/q">
<input type="text" name="q" id="q" required="1" placeholder="query"/>
<input type="submit" value="csearch" />
</form>

%% results_begin
<p><small>{{n}} results in {{t}}s</small></p>
<ul class="results">

%% result_file_begin
<li><a href="{{url}}"><code>{{file}}</code></a><ol>

%% result_file_code
<li value="{{linum}}"><code>{{result}}</code></li>

%% result_file_end
</ol></li>

%% results_end
</ul>

%% footer
</main>

<footer>
<p><small>
  codesearch.cgi | <a href="https://github.com/google/codesearch">google/codesearch</a>
</small></p>
</footer>
</body>
</html>

%% opensearch-description
<?xml version="1.0" encoding="UTF-8" ?>
<OpenSearchDescription
    xmlns="http://a9.com/-/spec/opensearch/1.1/"
    xmlns:moz="http://www.mozilla.org/2006/browser/search/">
  <ShortName>csearch</ShortName>
  <Description>codesearch in alyzon:/src</Description>
  <InputEncoding>UTF-8</InputEncoding>
  <Url type="text/html" template="{{BASE}}/q">
    <Param name="q" value="{searchTerms}" />
  </Url>
  <Url rel="self" type="application/opensearchdescription+xml" template="{{BASE}}/_os.xml" />
  <moz:SearchForm>{{BASE}}</moz:SearchForm>
</OpenSearchDescription>
