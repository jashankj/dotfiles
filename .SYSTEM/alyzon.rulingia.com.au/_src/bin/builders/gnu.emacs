v=
llvm_v=80
ld_flags="-fuse-ld=lld"

conf_flags=(
    --prefix=/src/gnu/emacs
    --with-x-toolkit=lucid # gtk is broke
    --with-xft
    --with-modules
    --with-wide-int
    --without-libsystemd
)

c_oflags="-O0" # "-O2"
c_gflags="-g3 -fno-omit-frame-pointer -fno-optimize-sibling-calls"
conf_flags+=--enable-checking='yes,glyphs'
conf_flags+=--enable-check-lisp-object-type

cd $S/gnu/emacs

# clean
mv obj${v} obj.x && mkdir -p obj${v}
(rm -rf obj.x) &
cd $S/gnu/emacs/src${v} && \
git clean -ffdx

set -e

./autogen.sh
cd ../obj${v} && \
env CANNOT_DUMP=yes \
    PATH=/usr/local/llvm${llvm_v}/bin:$PATH \
    ../src${v}/configure -C ${conf_flags} \
        CC=clang CXX=clang++ \
        AR=llvm-ar RANLIB=llvm-ranlib \
        CFLAGS="${c_oflags} ${c_gflags}" \
        LDFLAGS="${ld_flags} ${c_oflags} ${c_gflags}" \
        PKG_CONFIG=pkgconf MAKE=gmake \
        CONFIG_SITE=/src/freebsd/ports/Templates/config.site \
        lt_cv_sys_max_cmd_len=262144
gmake -Otarget -j4
gmake install
