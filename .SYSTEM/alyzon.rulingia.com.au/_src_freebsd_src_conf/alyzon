# alyzon: ThinkCentre M83, FreeBSD/amd64
#
# based on
#   stable/10/sys/amd64/conf/GENERIC r284665

cpu		HAMMER
ident		alyzon

makeoptions	DEBUG=-g		# Build kernel with gdb(1) debug symbols
makeoptions	WITH_CTF=1		# Run ctfconvert(1) for DTrace support

options 	SCHED_ULE		# ULE scheduler
options 	PREEMPTION		# Enable kernel thread preemption
options 	INET			# InterNETworking
options 	INET6			# IPv6 communications protocols
options 	IPSEC			# IP (v4/v6) securityy
options 	TCP_OFFLOAD		# TCP offload
options 	SCTP			# Stream Control Transmission Protocol
options 	FFS			# Berkeley Fast Filesystem
options 	SOFTUPDATES		# Enable FFS soft updates support
options 	UFS_ACL			# Support for access control lists
options 	UFS_DIRHASH		# Improve performance on big directories
options 	UFS_GJOURNAL		# Enable gjournal-based UFS journaling
options 	QUOTA			# Enable disk quotas for UFS
options 	MD_ROOT			# MD is a potential root device
options 	NFSCL			# New Network Filesystem Client
options 	NFSD			# New Network Filesystem Server
options 	NFSLOCKD		# Network Lock Manager
options 	NFS_ROOT		# NFS usable as /, requires NFSCL
options 	MSDOSFS			# MSDOS Filesystem
options 	CD9660			# ISO 9660 Filesystem
options 	PROCFS			# Process filesystem (requires PSEUDOFS)
options 	PSEUDOFS		# Pseudo-filesystem framework
options 	GEOM_PART_GPT		# GUID Partition Tables.
options 	GEOM_LABEL		# Provides labelization
options 	COMPAT_FREEBSD32	# Compatible with i386 binaries
options 	COMPAT_FREEBSD4		# Compatible with FreeBSD4
options 	COMPAT_FREEBSD5		# Compatible with FreeBSD5
options 	COMPAT_FREEBSD6		# Compatible with FreeBSD6
options 	COMPAT_FREEBSD7		# Compatible with FreeBSD7
options 	COMPAT_FREEBSD9		# Compatible with FreeBSD9
options 	COMPAT_FREEBSD10	# Compatible with FreeBSD10
options 	SCSI_DELAY=5000		# Delay (in ms) before probing SCSI
options 	KTRACE			# ktrace(1) support
options 	STACK			# stack(9) support
options 	SYSVSHM			# SYSV-style shared memory
options 	SYSVMSG			# SYSV-style message queues
options 	SYSVSEM			# SYSV-style semaphores
options 	_KPOSIX_PRIORITY_SCHEDULING # POSIX P1003_1B real-time extensions
options 	PRINTF_BUFR_SIZE=128	# Prevent printf output being interspersed.
options 	KBD_INSTALL_CDEV	# install a CDEV entry in /dev
options 	HWPMC_HOOKS		# Necessary kernel hooks for hwpmc(4)
options 	AUDIT			# Security event auditing
options 	CAPABILITY_MODE		# Capsicum capability mode
options 	CAPABILITIES		# Capsicum capabilities
options 	MAC			# TrustedBSD MAC Framework
options 	KDTRACE_FRAME		# Ensure frames are compiled in
options 	KDTRACE_HOOKS		# Kernel DTrace hooks
options 	DDB_CTF			# Kernel ELF linker loads CTF data
options 	INCLUDE_CONFIG_FILE	# Include this file in kernel
options 	RACCT			# Resource accounting framework
options 	RACCT_DEFAULT_TO_DISABLED # Set kern.racct.enable=0 by default
options 	RCTL			# Resource limits

# Debugging support.  Always need this:
options 	KDB			# Enable kernel debugger support.
options 	KDB_TRACE		# Print a stack trace for a panic.

# Make an SMP-capable kernel by default
options 	SMP			# Symmetric MultiProcessor Kernel
options 	DEVICE_NUMA		# I/O Device Affinity

# CPU frequency control
device		cpufreq
device 		coretemp
device		cpuctl

# Bus support.
device		acpi
options 	ACPI_DMAR
device		pci
options 	PCI_HP			# PCI-Express native HotPlug
options		PCI_IOV			# PCI SR-IOV support

# ATA controllers
device		ahci			# AHCI-compatible SATA controllers

# ATA/SCSI peripherals
device		scbus			# SCSI bus (required for ATA/SCSI)
device		da			# Direct Access (disks)
device		sa			# Sequential Access (tape etc)
device		cd			# CD
device		pass			# Passthrough device (direct ATA/SCSI access)
#device		ses			# Enclosure Services (SES and SAF-TE)
#device		ctl			# CAM Target Layer

# atkbdc0 controls both the keyboard and the PS/2 mouse
device		atkbdc			# AT keyboard controller
device		atkbd			# AT keyboard
device		psm			# PS/2 mouse

device		kbdmux			# keyboard multiplexer

device		vga			# VGA video card driver
options 	VESA			# Add support for VESA BIOS Extensions (VBE)

device		splash			# Splash screen and screen saver support

# syscons is the default console driver, resembling an SCO console
device		sc
options 	SC_PIXEL_MODE		# add support for the raster text mode

# vt is the new video console driver
device		vt
device		vt_efifb

device		agp			# support several AGP chipsets

# Serial (COM) ports
device		uart			# Generic UART driver

# PCI Ethernet NICs.
device		em			# Intel PRO/1000 Gigabit Ethernet Family
device		mii
device		mii_bitbang
device		e1000phy		# Marvell 88E1000 1000/100/10-BT
device		msk			# Marvell/SysKonnect Yukon II Gigabit Ethernet
device		netmap			# netmap(4) support

# Pseudo devices.
device		crypto			# Crypto core
device		cryptodev		# Crypto pseudo-dev
device		loop			# Network loopback
device		random			# Entropy device
device		rdrand_rng		# Intel Bull Mountain RNG
device		tpm			# Trusted Platform Module
device		aesni			# AES-NI OpenCrypto
device		ether			# Ethernet support
device		vlan			# 802.1Q VLAN support
device		tun			# Packet tunnel.
device		md			# Memory "disks"
device		gif			# IPv6 and IPv4 tunneling
device		firmware		# firmware assist module

# The `bpf' device enables the Berkeley Packet Filter.
# Be aware of the administrative consequences of enabling this!
# Note that 'bpf' is required for DHCP.
device		bpf			# Berkeley packet filter

# USB support
options 	USB_DEBUG		# enable debug msgs
device		uhci			# UHCI PCI->USB interface
device		ohci			# OHCI PCI->USB interface
device		ehci			# EHCI PCI->USB interface (USB 2.0)
device		xhci			# XHCI PCI->USB interface (USB 3.0)
device		usb			# USB Bus (required)
device		ukbd			# Keyboard
device		umass			# Disks/Mass storage - Requires scbus and da

# Sound support
device		sound			# Generic sound driver (required)
device		snd_hda			# Intel High Definition Audio

