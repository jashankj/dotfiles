#
# inara -- FreeBSD/amd64 diskless on a Thinkpad Mini 10
#

cpu		HAMMER
ident		inara

makeoptions	DEBUG=-g		# Build kernel with gdb(1) debug symbols
#makeoptions	WITH_CTF=1		# Run ctfconvert(1) for DTrace support

options 	SCHED_ULE		# ULE scheduler
#options 	NUMA			# Non-Uniform Memory Architecture support
options 	PREEMPTION		# Enable kernel thread preemption
#options 	VIMAGE			# Subsystem virtualization, e.g. VNET
options 	INET			# InterNETworking
options 	INET6			# IPv6 communications protocols
options 	IPSEC			# IP (v4/v6) security
options 	IPSEC_SUPPORT		# Allow kldload of ipsec and tcpmd5
options 	TCP_OFFLOAD		# TCP offload
options 	TCP_BLACKBOX		# Enhanced TCP event logging
options 	TCP_HHOOK		# hhook(9) framework for TCP
options		TCP_RFC7413		# TCP Fast Open
options 	SCTP			# Stream Control Transmission Protocol
options 	FFS			# Berkeley Fast Filesystem
options 	SOFTUPDATES		# Enable FFS soft updates support
options 	UFS_ACL			# Support for access control lists
options 	UFS_DIRHASH		# Improve performance on big directories
options 	UFS_GJOURNAL		# Enable gjournal-based UFS journaling
options 	QUOTA			# Enable disk quotas for UFS
options 	MD_ROOT			# MD is a potential root device
options 	NFSCL			# Network Filesystem Client
#options 	NFSD			# Network Filesystem Server
options 	NFSLOCKD		# Network Lock Manager
options 	NFS_ROOT		# NFS usable as /, requires NFSCL
options 	TMPFS			#Efficient memory filesystem
options 	MSDOSFS			# MSDOS Filesystem
#options 	CD9660			# ISO 9660 Filesystem
options 	PROCFS			# Process filesystem (requires PSEUDOFS)
options 	PSEUDOFS		# Pseudo-filesystem framework
#options 	FUSE			#FUSE support module
options 	GEOM_PART_GPT		# GUID Partition Tables.
#options 	GEOM_RAID		# Soft RAID functionality.
options 	GEOM_LABEL		# Provides labelization
options 	EFIRT			# EFI Runtime Services support
options 	COMPAT_CLOUDABI32
options 	COMPAT_CLOUDABI64
options 	COMPAT_FREEBSD32	# Compatible with i386 binaries
options 	COMPAT_FREEBSD4		# Compatible with FreeBSD4
options 	COMPAT_FREEBSD5		# Compatible with FreeBSD5
options 	COMPAT_FREEBSD6		# Compatible with FreeBSD6
options 	COMPAT_FREEBSD7		# Compatible with FreeBSD7
options 	COMPAT_FREEBSD9		# Compatible with FreeBSD9
options 	COMPAT_FREEBSD10	# Compatible with FreeBSD10
options 	COMPAT_FREEBSD11	# Compatible with FreeBSD11
options 	COMPAT_LINUXKPI		# Compatible with Linux KPI
options 	SCSI_DELAY=5000		# Delay (in ms) before probing SCSI
options 	KTRACE			# ktrace(1) support
options 	STACK			# stack(9) support
options 	SYSVSHM			# SYSV-style shared memory
options 	SYSVMSG			# SYSV-style message queues
options 	SYSVSEM			# SYSV-style semaphores
options 	_KPOSIX_PRIORITY_SCHEDULING # POSIX P1003_1B real-time extensions
options 	PRINTF_BUFR_SIZE=128	# Prevent printf output being interspersed.
options 	KBD_INSTALL_CDEV	# install a CDEV entry in /dev
options 	HWPMC_HOOKS		# Necessary kernel hooks for hwpmc(4)
options 	AUDIT			# Security event auditing
options 	CAPABILITY_MODE		# Capsicum capability mode
options 	CAPABILITIES		# Capsicum capabilities
options 	MAC			# TrustedBSD MAC Framework
#options 	KDTRACE_FRAME		# Ensure frames are compiled in
#options 	KDTRACE_HOOKS		# Kernel DTrace hooks
#options 	DDB_CTF			# Kernel ELF linker loads CTF data
options 	INCLUDE_CONFIG_FILE	# Include this file in kernel
#options 	RACCT			# Resource accounting framework
#options 	RACCT_DEFAULT_TO_DISABLED # Set kern.racct.enable=0 by default
#options 	RCTL			# Resource limits

# Debugging support.  Always need this:
options 	KDB			# Enable kernel debugger support.
options 	KDB_TRACE		# Print a stack trace for a panic.

# Kernel dump features.
#options 	EKCD			# Support for encrypted kernel dumps
#options 	GZIO			# gzip-compressed kernel and user dumps
options 	ZSTDIO			# zstd-compressed kernel and user dumps
#options 	NETDUMP			# netdump(4) client support
device		xz

# Make an SMP-capable kernel by default
options 	SMP			# Symmetric MultiProcessor Kernel
options 	EARLY_AP_STARTUP

# CPU frequency control
device		cpufreq
device		coretemp
device		cpuctl

# Bus support.
device		acpi
options 	ACPI_DMAR
#device		acpi_wmi
device		acpi_ibm
device		acpi_video
device		pci
#options 	PCI_HP			# PCI-Express native HotPlug
options		PCI_IOV			# PCI SR-IOV support

# ATA controllers
device		ahci			# AHCI-compatible SATA controllers
device		ata			# Legacy ATA/SATA controllers

# ATA/SCSI peripherals
device		scbus			# SCSI bus (required for ATA/SCSI)
device		ch			# SCSI media changers
device		da			# Direct Access (disks)
device		sa			# Sequential Access (tape etc)
device		cd			# CD
device		pass			# Passthrough device (direct ATA/SCSI access)

# atkbdc0 controls both the keyboard and the PS/2 mouse
device		atkbdc			# AT keyboard controller
device		atkbd			# AT keyboard
device		psm			# PS/2 mouse

device		kbdmux			# keyboard multiplexer

device		vga			# VGA video card driver
options 	VESA			# Add support for VESA BIOS Extensions (VBE)

device		splash			# Splash screen and screen saver support

# syscons is the default console driver, resembling an SCO console
device		sc
options 	SC_PIXEL_MODE		# add support for the raster text mode

# vt is the new video console driver
device		vt
device		vt_vga

#device		agp			# support several AGP chipsets

# Power management support (see NOTES for more options)
#device		apm

# PCI Ethernet NICs that use the common MII bus controller code.
device  	mii		# Minimal MII support
device  	mii_bitbang	# Common module for bit-bang'ing the MII
device  	rgephy		# RealTek 8169S/8110S/8211B/8211C
device		re		# RealTek 8139C+/8169/8169S/8110S

# Wireless NIC cards
device		wlan			# 802.11 support
options 	IEEE80211_DEBUG		# enable debug msgs
options 	IEEE80211_AMPDU_AGE	# age frames in AMPDU reorder q's
options 	IEEE80211_SUPPORT_MESH	# enable 802.11s draft support
device		wlan_wep		# 802.11 WEP support
device		wlan_ccmp		# 802.11 CCMP support
device		wlan_tkip		# 802.11 TKIP support
device		wlan_amrr		# AMRR transmit rate control algorithm
device		iwn			# Intel 4965/1000/5000/6000 wireless NICs.

# Pseudo devices.
device		crypto			# core crypto support
device		loop			# Network loopback
device		random			# Entropy device
#device		rdrand_rng		# Intel Bull Mountain RNG
device		ether			# Ethernet support
device		vlan			# 802.1Q VLAN support
device		tuntap			# Packet tunnel.
device		md			# Memory "disks"
device		firmware		# firmware assist module

# The `bpf' device enables the Berkeley Packet Filter.
# Be aware of the administrative consequences of enabling this!
# Note that 'bpf' is required for DHCP.
device		bpf			# Berkeley packet filter

# USB support
options 	USB_DEBUG		# enable debug msgs
device		uhci			# UHCI PCI->USB interface
device		ohci			# OHCI PCI->USB interface
device		ehci			# EHCI PCI->USB interface (USB 2.0)
device		xhci			# XHCI PCI->USB interface (USB 3.0)
device		usb			# USB Bus (required)
device		ukbd			# Keyboard
device		umass			# Disks/Mass storage - Requires scbus and da

# Sound support
device		sound			# Generic sound driver (required)
device		snd_hda			# Intel High Definition Audio

# MMC/SD
device		mmc			# MMC/SD bus
device		mmcsd			# MMC/SD memory card
device		sdhci			# Generic PCI SD Host Controller

# Netmap provides direct access to TX/RX rings on supported NICs
device		netmap			# netmap(4) support

# evdev interface
options 	EVDEV_SUPPORT		# evdev support in legacy drivers
device		evdev			# input event device support
device		uinput			# install /dev/uinput cdev

# System Management Bus support is provided by the 'smbus' device.
device		smbus		# Bus support, required for smb below.
device		smb
device		iicbus		# Bus support, required for ic/iic/iicsmb below.
device		iic
device		iicsmb		# smb over i2c bridge

# Enable netgraph for Bluetooth, Ethernet, and PPPoE
options		NETGRAPH
options		NETGRAPH_BLUETOOTH
options		NETGRAPH_BLUETOOTH_BT3C
options		NETGRAPH_BLUETOOTH_HCI
options		NETGRAPH_BLUETOOTH_L2CAP
options		NETGRAPH_BLUETOOTH_SOCKET
options		NETGRAPH_BLUETOOTH_UBT
options		NETGRAPH_ETHER
options		NETGRAPH_PPP
options		NETGRAPH_PPPOE
