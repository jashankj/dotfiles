#
# GENERIC -- Generic kernel configuration file for FreeBSD/i386
#
# For more information on this file, please read the config(5) manual page,
# and/or the handbook section on Kernel Configuration Files:
#
#    https://www.FreeBSD.org/doc/en_US.ISO8859-1/books/handbook/kernelconfig-config.html
#
# The handbook is also available locally in /usr/share/doc/handbook
# if you've installed the doc distribution, otherwise always see the
# FreeBSD World Wide Web server (https://www.FreeBSD.org/) for the
# latest information.
#
# An exhaustive list of options and more detailed explanations of the
# device lines is also present in the ../../conf/NOTES and NOTES files.
# If you are in doubt as to the purpose or necessity of a line, check first
# in NOTES.
#
# $FreeBSD$

cpu		I486_CPU
cpu		I586_CPU
cpu		I686_CPU
ident		GENERIC

makeoptions	DEBUG=-g		# Build kernel with gdb(1) debug symbols
makeoptions	WITH_CTF=1		# Run ctfconvert(1) for DTrace support

options 	SCHED_ULE		# ULE scheduler
options 	PREEMPTION		# Enable kernel thread preemption
options 	VIMAGE			# Subsystem virtualization, e.g. VNET
options 	INET			# InterNETworking
options 	INET6			# IPv6 communications protocols
options 	IPSEC			# IP (v4/v6) security
options 	IPSEC_SUPPORT		# Allow kldload of ipsec and tcpmd5
options 	TCP_HHOOK		# hhook(9) framework for TCP
options 	TCP_OFFLOAD		# TCP offload
options 	SCTP			# Stream Control Transmission Protocol
options 	FFS			# Berkeley Fast Filesystem
options 	SOFTUPDATES		# Enable FFS soft updates support
options 	UFS_ACL			# Support for access control lists
options 	UFS_DIRHASH		# Improve performance on big directories
options 	UFS_GJOURNAL		# Enable gjournal-based UFS journaling
options 	QUOTA			# Enable disk quotas for UFS
options 	MD_ROOT			# MD is a potential root device
options 	NFSCL			# Network Filesystem Client
options 	NFSLOCKD		# Network Lock Manager
options 	NFS_ROOT		# NFS usable as /, requires NFSCL
options 	MSDOSFS			# MSDOS Filesystem
options 	CD9660			# ISO 9660 Filesystem
options 	PROCFS			# Process filesystem (requires PSEUDOFS)
options 	PSEUDOFS		# Pseudo-filesystem framework
options 	GEOM_RAID		# Soft RAID functionality.
options 	GEOM_LABEL		# Provides labelization
options 	COMPAT_FREEBSD4		# Compatible with FreeBSD4
options 	COMPAT_FREEBSD5		# Compatible with FreeBSD5
options 	COMPAT_FREEBSD6		# Compatible with FreeBSD6
options 	COMPAT_FREEBSD7		# Compatible with FreeBSD7
options 	COMPAT_FREEBSD9		# Compatible with FreeBSD9
options 	COMPAT_FREEBSD10	# Compatible with FreeBSD10
options 	COMPAT_FREEBSD11	# Compatible with FreeBSD11
options 	SCSI_DELAY=5000		# Delay (in ms) before probing SCSI
options 	KTRACE			# ktrace(1) support
options 	STACK			# stack(9) support
options 	SYSVSHM			# SYSV-style shared memory
options 	SYSVMSG			# SYSV-style message queues
options 	SYSVSEM			# SYSV-style semaphores
options 	_KPOSIX_PRIORITY_SCHEDULING # POSIX P1003_1B real-time extensions
options 	PRINTF_BUFR_SIZE=128	# Prevent printf output being interspersed.
options 	KBD_INSTALL_CDEV	# install a CDEV entry in /dev
options 	HWPMC_HOOKS		# Necessary kernel hooks for hwpmc(4)
options 	AUDIT			# Security event auditing
options 	CAPABILITY_MODE		# Capsicum capability mode
options 	CAPABILITIES		# Capsicum capabilities
options 	MAC			# TrustedBSD MAC Framework
options 	KDTRACE_HOOKS		# Kernel DTrace hooks
options 	DDB_CTF			# Kernel ELF linker loads CTF data
options 	INCLUDE_CONFIG_FILE	# Include this file in kernel
options 	RACCT			# Resource accounting framework
options 	RACCT_DEFAULT_TO_DISABLED # Set kern.racct.enable=0 by default
options 	RCTL			# Resource limits

# Debugging support.  Always need this:
options 	KDB			# Enable kernel debugger support.
options 	KDB_TRACE		# Print a stack trace for a panic.

# Kernel dump features.
options 	EKCD			# Support for encrypted kernel dumps
options 	GZIO			# gzip-compressed kernel and user dumps
options 	ZSTDIO			# zstd-compressed kernel and user dumps
options 	NETDUMP			# netdump(4) client support

# To make an SMP kernel, the next two lines are needed
options 	SMP			# Symmetric MultiProcessor Kernel
device		apic			# I/O APIC
options 	EARLY_AP_STARTUP

# CPU frequency control
device		cpufreq

# Bus support.
device		acpi
device		pci
options		PCI_IOV			# PCI SR-IOV support

# Floppy drives
device		fdc

# ATA controllers
device		ahci			# AHCI-compatible SATA controllers
device		ata			# Legacy ATA/SATA controllers

# ATA/SCSI peripherals
device		scbus			# SCSI bus (required for ATA/SCSI)
device		ch			# SCSI media changers
device		da			# Direct Access (disks)
device		sa			# Sequential Access (tape etc)
device		cd			# CD
device		pass			# Passthrough device (direct ATA/SCSI access)
device		ses			# Enclosure Services (SES and SAF-TE)

# atkbdc0 controls both the keyboard and the PS/2 mouse
device		atkbdc			# AT keyboard controller
device		atkbd			# AT keyboard
device		psm			# PS/2 mouse

device		kbdmux			# keyboard multiplexer

device		vga			# VGA video card driver
options 	VESA			# Add support for VESA BIOS Extensions (VBE)

device		splash			# Splash screen and screen saver support

# syscons is the default console driver, resembling an SCO console
device		sc
options 	SC_PIXEL_MODE		# add support for the raster text mode

# vt is the new video console driver
device		vt
device		vt_vga

device		agp			# support several AGP chipsets

# Serial (COM) ports
device		uart			# Generic UART driver

# Parallel port
device		ppc
device		ppbus			# Parallel port bus (required)
device		lpt			# Printer
device		ppi			# Parallel port interface device

# PCI Ethernet NICs that use the common MII bus controller code.
# NOTE: Be sure to keep the 'device miibus' line in order to use these NICs!
device		miibus			# MII bus support
device		vr			# VIA Rhine, Rhine II

# Pseudo devices.
device		crypto			# core crypto support
device		loop			# Network loopback
device		random			# Entropy device
device		padlock_rng		# VIA Padlock RNG
device		ether			# Ethernet support
device		vlan			# 802.1Q VLAN support
device		tuntap			# Packet tunnel.
device		md			# Memory "disks"
device		gif			# IPv6 and IPv4 tunneling
device		firmware		# firmware assist module

# The `bpf' device enables the Berkeley Packet Filter.
# Be aware of the administrative consequences of enabling this!
# Note that 'bpf' is required for DHCP.
device		bpf			# Berkeley packet filter

# USB support
options 	USB_DEBUG		# enable debug msgs
device		uhci			# UHCI PCI->USB interface
device		ohci			# OHCI PCI->USB interface
device		ehci			# EHCI PCI->USB interface (USB 2.0)
device		usb			# USB Bus (required)
device		ukbd			# Keyboard
device		umass			# Disks/Mass storage - Requires scbus and da

# Sound support
device		sound			# Generic sound driver (required)
device		snd_cmi			# CMedia CMI8338/CMI8738
device		snd_csa			# Crystal Semiconductor CS461x/428x
device		snd_emu10kx		# Creative SoundBlaster Live! and Audigy
device		snd_es137x		# Ensoniq AudioPCI ES137x
device		snd_hda			# Intel High Definition Audio
device		snd_ich			# Intel, NVidia and other ICH AC'97 Audio
device		snd_via8233		# VIA VT8233x Audio

# evdev interface
options 	EVDEV_SUPPORT		# evdev support in legacy drivers
device		evdev			# input event device support
device		uinput			# install /dev/uinput cdev
