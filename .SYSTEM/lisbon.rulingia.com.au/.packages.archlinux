#% ccat ~/.packages.archlinux | pacman -S

# A base Arch installation:
  base
  base-devel
  linux
    linux-firmware

# jashankj-base:
  zsh
  mg
  sudo
  tmux
  rsync
  bind-tools
  mutt
  weechat
  links
  openvpn
  lsof
  wget
  curl
  lftp
  yubikey-manager
  duplicity

# Arch base toolchain supplement
  openssh
  nfs-utils
    rpcbind
    nfsidmap
  inetutils
  dhclient
  man-pages
    man-db
  sssd
    nss_ldap
    pam_ldap
  ntp
  git
  rrdtool
  m4
  mlocate
  busybox

# jashankj-devel:
  erlang
  mono mono-addins mono-tools
  dotnet-host dotnet-runtime dotnet-sdk dotnet-targeting-pack
  squeak-vm
  shellcheck
  dash

  # jashankj-devel-arm:
    aarch64-linux-gnu-binutils aarch64-linux-gnu-gcc
    arm-none-eabi-binutils     arm-none-eabi-gcc
  # jashankj-devel-avr:
    avrdude
    avr-binutils avr-gcc
  # jashankj-devel-c:
    clang llvm lld lldb openmp
    valgrind
    gdb
  # jashankj-devel-db:
    postgresql postgresql-docs postgis
    sqlite sqlite-doc sqlite-tcl
  # jashankj-devel-edit:
    emacs
    editorconfig-core-c
    global
    ctags
  # jashankj-devel-fpga:
    kicad
      kicad-library
      kicad-library-3d
    #/ gnucap
    pulseview
      sigrok-cli
      sigrok-firmware-fx2lafw
    gtkwave
    ghdl-llvm #/ ghdl-gcc ghdl-mcode
    verilator
    iverilog
    #/ tkgate
    #/ yosys-git
      icestorm-git
        arachne-pnr-git
        nextpnr-git
        trellis
    magic-git
  # jashankj-devel-java:
    jdk-openjdk
      jre-openjdk
      jre-openjdk-headless
      openjdk-doc
      openjdk-src
    jdk11-openjdk
      jre11-openjdk
      jre11-openjdk-headless
      openjdk11-doc
      openjdk11-src
    jdk8-openjdk
      jre8-openjdk
      jre8-openjdk-headless
      openjdk8-doc
      openjdk8-src
    eclipse-ecj
    icedtea-web
    ant
    maven
    beanshell
    jasmin
    #/ jogl
  # jashankj-devel-math:
    gnuplot
    sagemath
    coq
    agda
      agda-stdlib
    z3
    yices
    cvc4
    polyml
    scala
  # jashankj-devel-mips:
    uboot-tools
  # jashankj-devel-perl:
  # jashankj-devel-ruby:
    ruby
    jruby
    rubygems
    mruby
  # jashankj-devel-text:
    texlive-installer
    lyx
    texmacs
    pandoc
    groff
    aspell
      aspell-en
    hunspell
      hunspell-en_AU
      hunspell-en_GB
      hunspell-en_US
    mythes-en
  # jashankj-devel-vcs:
    git
      gitflow-avh
      git-annex
      git-lfs
      hub
    rcs
    cvs
    mercurial
    subversion
    darcs
    fossil
    meld
  # jashankj-devel-xml:
    docbook-dsssl
      openjade
    docbook-sgml
    docbook-xml
    docbook-xsl
    docbook5-xml
    xmlstarlet

# jashankj-server:
  # jashankj-server-http:
    apache
    php php-apache
  # jashankj-server-lp:
    cups
      cups-filters
      cups-pdf
    foomatic-db-engine
      foomatic-db
  # jashankj-server-mta:
    postfix

# jashankj-gui:
  xorg-server
    xorg-server-xephyr
    xorg-server-xnest
  xorg-docs
  xorg-apps
    xorg-xman
    xorg-xdm
    xorg-xclock
  i3-wm
    perl-anyevent-i3
  picom
  redshift
  dmenu
  scrot
  xclip
  xcape
  xss-lock
    xscreensaver
  rofi
  workrave
  xfce4-notifyd
  inkscape
  netsurf
  dillo
  xfce4-terminal
  thunar
  synergy
  tigervnc
  rdesktop

  # gui, fonts ---
  xorg-fonts
    xorg-fonts-100dpi
    xorg-fonts-75dpi
    #/ xorg-fonts-alias
    xorg-fonts-cyrillic
    xorg-fonts-misc
    xorg-fonts-type1
    ttf-fira-code
    ttf-fira-mono otf-fira-mono
    ttf-fira-sans otf-fira-sans
    ttf-roboto
    ttf-ibm-plex

# jashankj-media:
  eog feh
  #/ xv
  evince xpdf okular
  gimp dia fontforge digikam shotwell #/ xfig
  ardour
  pulseaudio paprefs pavucontrol
  pulseaudio-alsa
  pulseaudio-bluetooth bluez
  pulseaudio-zeroconf
  ffmpeg mpv mlt kdenlive

# alyzon-inspired misc:
  dmidecode
  libva
    libva-mesa-driver
    libva-vdpau-driver
  radeontool
  xf86-video-amdgpu
    clinfo
      ocl-icd
      opencl-headers
      opencl-mesa
  vulkan-tools
    amdvlk
    vulkan-radeon
    vulkan-mesa-layers

  gnome-terminal
  libreoffice-fresh
    libreoffice-fresh-en-gb
  gitg
  zim
  #/ tomboy
  unison
  ntfs-3g
  smartmontools

iftop
x11-ssh-askpass
syslinux

mkinitcpio mkinitcpio-nfs-utils

# menolly:
# xf86-video-openchrome
# unichrome-dri


ed
efibootmgr
linux-headers
otf-font-awesome
ruby-dbus
openbsd-netcat

networkmanager
networkmanager-openconnect
networkmanager-openvpn
networkmanager-qt
nm-connection-editor
libnm libnm-glib libnma
openresolv

acpilight
iw
net-tools
wireless_tools
strace
ruby-ffi
firewalld
firewall-applet python-pyqt5
bluez-utils blueman
fprintd
dunst

zsh-autosuggestions
zsh-completions
arandr
wireshark-cli
wireshark-qt
tcpdump
ifenslave
autofs
jemalloc
bmake
tree
opam
go
devtools
intltool

mips-harvard-os161-binutils mips-harvard-os161-gdb mips-harvard-os161-gcc48
wakatime rescuetime2
etc-update ddate lab json-sh jasmin screenkey redo-python
xmoto

teams
discord_arch_electron
signal-desktop
jitsi-meet-desktop

zoom

autoconf-archive
gitflow-avh
w3m
cmake
python-pip
python-lxml
python-libarchive-c
python-ply
python-ordered-set
python-hypothesis
python-beautifulsoup4
ninja
qemu-arch-extra
qemu-block-iscsi
qemu-block-rbd
edk2-shell
efitools
bridge-utils
powertop
thermald
cpupower
turbostat

ccache distcc

gtk-engines breeze-gtk

mesa-demos
fwupd
sloccount

bc kmod libelf pahole cpio perl tar xz xmlto python-sphinx python-sphinx_rtd_theme graphviz imagemagick git

alsamixer
alsa-utils

sysstat
lshw
usbutils

ghidra

unrtf
catdoc
abiword

s-tui
noweb

npm
doxygen
xdotool
nbd
