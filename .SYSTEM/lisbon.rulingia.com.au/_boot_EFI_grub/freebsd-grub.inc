########################################################################

menuentry "[lisbon] FreeBSD" --class freebsd --class os {
	set	be="freebsd"
	set	kern="kernel"
	set	kopts="" # -s -v
	set	root=($zpool)/ROOT/$be/@
	set	kernel=$root/boot/${kern}

        echo	":: loading ${kernel}/kernel ... "
        kfreebsd	$kernel/kernel ${kopts}

        echo	":: loading $root/boot/device.hints ... "
        kfreebsd_loadenv $root/boot/device.hints

        echo	":: loading $root/boot/zfs/zpool.cache ..."
        kfreebsd_module	$root/boot/zfs/zpool.cache type=/boot/zfs/zpool.cache

	set	_zfs_klds="opensolaris zfs"
	set	_wlan_klds="wlan wlan_ccmp wlan_tkip wlan_wep"
	set	_drm_klds="iicbus iic iicbb drm ttm amdkfd amdgpu"
	set	_fs_klds="tmpfs nullfs fdescfs fuse"
	set	_other_klds="linux_common linux"
	set	klds="${_zfs_klds} ${_drm_klds} ${_usb_klds} ${_fs_klds} ${_other_klds}"

        for kld in \
		opensolaris zfs \
		wlan wlan_ccmp wlan_tkip wlan_wep if_iwm \
		iicbus iic iicbb \
		linuxkpi linuxkpi_gplv2 debugfs \
		drm ttm amdkfd amdgpu \
		tmpfs nullfs fdescfs fuse \
		linux_common linux linux64
        do
                echo	":: loading ${kernel}/${kld}.ko ..."
                kfreebsd_module_elf ${kernel}/${kld}.ko
        done

        set	kFreeBSD.console="efi,vidconsole"
        set	kFreeBSD.kern.vty="vt"
        set	kFreeBSD.vfs.root.mountfrom="zfs:lisbon/ROOT/${be}"

        echo	":: booting."
        echo	""
}

########################################################################
