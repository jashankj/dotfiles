########################################################################

menuentry	'[lisbon] Fedora Linux rawhide (linux-5.18.0-60.fc37.x86_64)' \
	--class fedora --class gnu-linux --class gnu --class os \
{
	load_linux fedora \
		boot/vmlinuz-5.18.0-60.fc37.x86_64 \
		boot/initramfs-5.18.0-60.fc37.x86_64.img \
		security=selinux
}

menuentry	'[lisbon] Fedora Linux rawhide (linux-5.17.6-300.fc36.x86_64)' \
	--class fedora --class gnu-linux --class gnu --class os \
{
	load_linux fedora \
		boot/vmlinuz-5.17.6-300.fc36.x86_64 \
		boot/initramfs-5.17.6-300.fc36.x86_64.img \
		security=selinux
}

menuentry	'[lisbon] Fedora Linux rawhide (linux-5.17.5-300.fc36.x86_64)' \
	--class fedora --class gnu-linux --class gnu --class os \
{
	load_linux fedora \
		boot/vmlinuz-5.17.5-300.fc36.x86_64 \
		boot/initramfs-5.17.5-300.fc36.x86_64.img \
		security=selinux
}

########################################################################
