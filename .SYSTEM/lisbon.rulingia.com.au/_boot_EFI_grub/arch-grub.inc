########################################################################

menuentry	'[lisbon] Arch Linux rolling (linux)' \
	--class arch --class gnu-linux --class gnu --class os \
{
	load_linux arch \
		boot/vmlinuz-linux \
		boot/initramfs-linux.img
}

menuentry	'[lisbon] Arch Linux rolling (linux), fallback' \
	--class arch --class gnu-linux --class gnu --class os \
{
	load_linux arch \
		boot/vmlinuz-linux \
		boot/initramfs-linux-fallback.img
}

menuentry	'[lisbon] Arch Linux rolling (linux-lts)' \
	--class arch --class gnu-linux --class gnu --class os \
{
	load_linux arch \
		boot/vmlinuz-linux-lts \
		boot/initramfs-linux-lts.img
}

menuentry	'[lisbon] Arch Linux rolling (linux-lts), fallback' \
	--class arch --class gnu-linux --class gnu --class os \
{
	load_linux arch \
		boot/vmlinuz-linux-lts \
		boot/initramfs-linux-lts-fallback.img
}

########################################################################
