REL = ARGV.shift
V   = ARGV.shift
e = 0

ARGF.each_line do |l|
  l.gsub!('$releasever', REL)
  case l
  when /^\[pgdg-/     then e = 1; print l
  when /^\[pgdg#{V}/  then e = 1; print l
  when /^\[/          then e = 0; print l
  when /^enabled\s*=\s*1/ then puts "enabled=#{e}"; next
  when /^gpgkey=/ then puts 'gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-pgdg-fedora-all'
  else print l
  end
end
