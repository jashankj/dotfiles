config_opts['dnf.conf'] += '''
[copr:jashankj:localpkgs]
name=copr:jashankj/localpkgs
baseurl=https://download.copr.fedorainfracloud.org/results/jashankj/localpkgs/fedora-rawhide-$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://download.copr.fedorainfracloud.org/results/jashankj/localpkgs/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
'''
