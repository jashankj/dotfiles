import collections
from collections.abc import Generator
from pathlib import Path
import tomllib
from typing import Any, IO, Callable

from talon import resource

from .user_settings import SETTINGS_DIR


def transform_table(table: dict[str, str | list[str]]) \
        -> Generator[tuple[str, str], None, None]:
    """
    Transform a dictionary of terms to utterance lists to a form that
    can become part of a ``talon.Context.lists``.
    """

    for key, phrases in table.items():
        if isinstance(phrases, list):
            for phrase in phrases:
                yield phrase, key
        else:
            yield phrases, key


def transform_polytable(table: dict[str, dict[str, list[str]]], subkey: str) \
        -> Generator[tuple[str, str], None, None]:
    """
    Transform a dictionary of terms to utterance groups to a form
    that can be used in a ``talon.Context.lists``.
    """

    for key, subtable in table.items():
        for phrase in subtable[subkey]:
            yield phrase, key


# XXX determine the right type for CallbackT!
CallbackT = Callable[[Any], None]
DecoratorT = Callable[[CallbackT], CallbackT]

def watch(filename: str) -> DecoratorT:
    """
    From ``filename``, import tables.
    """
    assert filename.endswith(".toml")
    path = SETTINGS_DIR / filename

    def decorator(fn: CallbackT) -> CallbackT:
        @resource.watch(str(path), mode='rb')
        def on_update(f):
            data = tomllib.load(f)
            fn(data)

    return decorator
