new line: "\n"
(da dash | double dash): "--"
triple quote: "'''"
triple grave: "```"
(da dot | dot dot | dotdot): ".."
ellipsis: "..."
(crack | comma and | spamma): ", "
(stab | arrow): "->"
(fat arrow | fat comma | dub arrow): "=>"

# Insert delimiter pairs
<user.delimiter_pair>: user.delimiter_pair_insert(delimiter_pair)

# Wrap selection with delimiter pairs
<user.delimiter_pair> that: user.delimiter_pair_wrap_selection(delimiter_pair)
