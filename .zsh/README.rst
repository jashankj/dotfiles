========================================================================
                        zsh configuration notes
========================================================================

what's this? a zsh user who *doesn't* use Oh My Zsh?


from :man:`zsh(1)` --- specifically, ``Zsh/files.yo`` ---

   .. rubric:: STARTUP/SHUTDOWN FILES

   Commands are first read from ``/etc/zsh/zshenv``;
   this cannot be overridden.
   [...]
   Commands are then read from ``$ZDOTDIR/.zshenv``.
   If the shell is a login shell,
   commands are read from ``/etc/zsh/zprofile``
   and then ``$ZDOTDIR/.zprofile``.
   Then, if the shell is interactive,
   commands are read from ``/etc/zsh/zshrc``
   and then ``$ZDOTDIR/.zshrc``.
   Finally, if the shell is a login shell,
   ``/etc/zsh/zlogit`` and ``$ZDOTDIR/.zlogin`` are read.
   [...]
   When a login shell exits,
   the files ``$ZDOTDIR/.zlogout``
   and then ``/etc/zsh/zlogout`` are read.

In the directories ``env.d``, ``profile.d``, ``rc.d``,
``login.d``, and ``logout.d`` are files that are run in turn
during each of the noted script-reading stages.
Ordering is calculated with :man:`rcorder(8)`.


env.d notes
------------------------------------------------

``DECLARE``:
   By ``DECLARE``, we have finished any up-front configuration.

``ACCEPT``:
   By ``ACCEPT``, we have loaded any external (i.e., system-specific)
   configuration, which might overwrite global state.

``ASSERT``:
   By ``ASSERT``, we have merged together our declared configuration
   with any weird external state that has been set, and we then assert
   our view of the world.

   ASSERT ties together loads of variables.


beware of slow code in env.d ---
every invocation of the shell hits it,
and that can seriously add up!
notable previous offenders:

+ Perl ``local::lib``
+ TeX Live platform detection

