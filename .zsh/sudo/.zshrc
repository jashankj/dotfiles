source ~root/.zsh.helper; try_load_zrcfile zshrc

# Patch rc.d/ZFPATH
if [ ! -z "${try_ZDOTDIR}" ]
then
	fpath=(
		"${try_ZDOTDIR}/.zsh/completions"
		"${try_ZDOTDIR}/.zsh/functions"
		"${fpath[@]}"
	)
fi
