source ~root/.zsh.helper; try_load_zrcfile zshenv

# Patch around env.d/ASSERT
if [ ! -z "${try_ZDOTDIR}" ]
then
	BINPATH="${try_ZDOTDIR}/bin:${try_ZDOTDIR}/bin/$OSARCH:${PATH}"
	PATH="${BINPATH}"
	declared_BINPATH="${BINPATH}"
	declared_PATH="${BINPATH}"
fi
