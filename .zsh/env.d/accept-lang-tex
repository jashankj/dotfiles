# -*- shell-script -*-
# PROVIDE: accept-lang-tex
# REQUIRE: DECLARE declare-osarch
# BEFORE:  ACCEPT
#
# TeX > system-wide TeXlive

. "$ZDOTDIR/.zsh/functions/dir_exists_p"
. "$ZDOTDIR/.zsh/functions/link_exists_p"

texlive=/usr/local/texlive

if link_exists_p "${texlive}/current"
then
	export TEXMFLOCAL="${texlive}/texmf-local"
	export TEXMFSYSVAR="${texlive}/current/texmf-var"
	export TEXMFSYSCONFIG="${texlive}/current/texmf-config"

	# Calculating the TeX Live platform is slow (~400ms on lisbon)
	# so make a guess first based on $ARCHOS; and, if it doesn't
	# exist, then fall back to the full calculation.
	texlive_platform="$ARCHOS"
	if ! dir_exists_p "${texlive}/current/bin/${texlive_platform}"
	then
		texlive_platform="$("${texlive}/current/install-tl" --print-platform)"
	fi

	decl_binpath=("${texlive}/current/bin/${texlive_platform}" "${decl_binpath[@]}")
	decl_manpath+="${texlive}/current/texmf-dist/doc/man"
	decl_infopath+="${texlive}/current/texmf-dist/doc/info"
fi
