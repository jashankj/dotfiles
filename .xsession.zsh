#!/usr/bin/env zsh
# jashank's xinitrc

# exec >>(timestamped | tee -a ~/.xprofile-errors) 2>&1
set -x


# Use rcorder(8), or its cached output, to load scripts.
. "$HOME/.zsh/functions/source_rcorder"
source_rcorder xsession "$HOME/.xsession.d"


# OK... fire up a window manager.

if cse_vlab_p
then
	startxfce4

elif santiago_p
then
	gnome-session

elif elspeth_p && ! have_command_p i3
then
	/opt/X11/bin/quartz-wm

else
	i3

fi
