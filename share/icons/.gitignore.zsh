#!/usr/bin/env zsh
# Generate `~/share/icons/.gitignore'.

print '/.gitignore'
print '!/.gitignore.zsh'

# <https://specifications.freedesktop.org/icon-theme-spec/>
themes=(
	# The blessed:
	hicolor
)

# <https://specifications.freedesktop.org/icon-theme-spec/>
sizes=(
	16x16
	24x24
	32x32
	48x48
	64x64
	128x128
	256x256
	512x512
	scalable
)

# <https://specifications.freedesktop.org/icon-naming-spec/>
contexts=(
	actions
	animations
	apps
	categories
	devices
	emblems
	emotes
	ntl
	mimetypes
	places
	status
)

for theme in "${themes[@]}"
do
	for size in "${sizes[@]}"
	do
		for context in "${contexts[@]}"
		do
			printf '/%s/%s/%s/*.???\n' \
			       "${theme}" \
			       "${size}" \
			       "${context}"
		done
	done
done
