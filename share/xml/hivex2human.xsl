<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">

  <xsl:template name="at-node" match="node">
    <xsl:param name="path" />
    <xsl:value-of select="concat($path, '/', @name, '&#xa;')" />

    <xsl:call-template name="top">
      <xsl:with-param name="path" select="concat($path, '/', @name)" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="at-value" match="value">
    <xsl:param name="path" />
    <xsl:value-of
	xml:space="preserve"
	select="concat($path, '/', @key, ' = ', @type, ',', @encoding, ':', @value, '&#xa;')" />
  </xsl:template>

  <xsl:template name="top" match="/hive/node[@root='1']">
    <xsl:param name="path">^</xsl:param>

    <xsl:apply-templates select="value">
      <xsl:with-param name="path" select="$path" />
    </xsl:apply-templates>

    <xsl:apply-templates select="node">
      <xsl:with-param name="path" select="$path" />
    </xsl:apply-templates>
  </xsl:template>

</xsl:stylesheet>
