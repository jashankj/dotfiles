set-default-mode fill
set-default-mode indent
set-fill-column 72
auto-execute *.c c-mode
column-number-mode
